  INSERT INTO MOE_SYSTEM_SETTING ("PARAM_NAME","PARAM_NAME_DESC","PARAM_VALUE","PARAM_VALUE_TYPE","PARAM_POSSIBLE_VALUE","PARAM_LEVEL","PARAM_DISPLAY","CREATE_BY","CREATE_DTM","UPDATE_BY","UPDATE_DTM") VALUES ('saam_alert_desc_hlab1502_positive_code','SAAM Alert Description HALB 1502 Positive Code','A0017',null,null,0,'N','admin',sysdate,'admin',sysdate);
  INSERT INTO MOE_SYSTEM_SETTING ("PARAM_NAME","PARAM_NAME_DESC","PARAM_VALUE","PARAM_VALUE_TYPE","PARAM_POSSIBLE_VALUE","PARAM_LEVEL","PARAM_DISPLAY","CREATE_BY","CREATE_DTM","UPDATE_BY","UPDATE_DTM") VALUES ('saam_alert_desc_hlab1502_negative_code','SAAM Alert Description HALB 1502 Negative Code','A0043',null,null,0,'N','admin',sysdate,'admin',sysdate);

  -- //@UNDO
DELETE FROM MOE_SYSTEM_SETTING WHERE PARAM_NAME = 'saam_alert_desc_hlab1502_positive_code';
DELETE FROM MOE_SYSTEM_SETTING WHERE PARAM_NAME = 'saam_alert_desc_hlab1502_negative_code';

