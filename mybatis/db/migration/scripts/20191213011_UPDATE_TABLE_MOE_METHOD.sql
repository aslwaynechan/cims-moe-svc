UPDATE MOE_METHOD SET METHOD_NAME = 'hk.health.moe.service.MoeDrugService.getPatientPrescriptionByOrderNo' WHERE METHOD_ID = 2;
UPDATE MOE_METHOD SET METHOD_NAME = 'hk.health.moe.service.MoeDrugService.getPatientOrderList' WHERE METHOD_ID = 3;
UPDATE MOE_METHOD SET METHOD_NAME = 'hk.health.moe.service.MoeDrugService.getPatientOrderDetail' WHERE METHOD_ID = 4;
UPDATE MOE_METHOD SET METHOD_NAME = 'hk.health.moe.service.MoeDrugService.getPatientOrderLogByPeriod' WHERE METHOD_ID = 16;
INSERT INTO MOE_METHOD(METHOD_ID,METHOD_NAME) VALUES(18,'hk.health.moe.service.MoeDrugService.deletePatientOrderDetail');


-- //@UNDO
UPDATE MOE_METHOD SET METHOD_NAME = 'hk.gov.ehr.model.moe.webservice.MoeDrugService.getPatientPrescriptionByOrderNo' WHERE METHOD_ID = 2;
UPDATE MOE_METHOD SET METHOD_NAME = 'hk.gov.ehr.model.moe.webservice.MoeDrugService.getPatientOrderList' WHERE METHOD_ID = 3;
UPDATE MOE_METHOD SET METHOD_NAME = 'hk.gov.ehr.model.moe.webservice.MoeDrugService.getPatientOrderDetail' WHERE METHOD_ID = 4;
UPDATE MOE_METHOD SET METHOD_NAME = 'hk.gov.ehr.model.moe.webservice.MoeDrugService.getPatientOrderLogByPeriod' WHERE METHOD_ID = 16;
DELETE FROM MOE_METHOD WHERE METHOD_ID = 18 AND METHOD_NAME = 'hk.health.moe.service.MoeDrugService.deletePatientOrderDetail';



