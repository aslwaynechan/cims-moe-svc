package hk.health.moe.facade;

import hk.health.moe.pojo.dto.inner.InnerSaveDrugDto;

public interface SaDrugLocalFacade {

    void saveDrugDetail(InnerSaveDrugDto dto) throws Exception;

    void deleteDrugDetail(String localDrudId) throws Exception;

}
