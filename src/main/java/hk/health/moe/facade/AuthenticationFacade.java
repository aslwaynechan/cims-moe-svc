/*********************************************************************
* Copyright (c) eHR HK Limited 2013. 
* All Rights Reserved.
* 
*********************************************************************
* PROJECT NAME    :  eHR Adaptation
*
* MODULE NAME     :  Medication Order Entry (MOE)
*
* PROGRAM NAME    :  AuthenticationFacade.java
*
* PURPOSE         :  Specify the methods that classes must implement.
*
**********************************************************************
* VERSION         :   1.01.001
* REF NO          :  
* DATE CREATED    :   7 June 2013
* 
*********************************************************************
*
* CHANGES         :
* REF NO           DATE        DETAIL
* ------           ----        ------
*
*********************************************************************
*/
package hk.health.moe.facade;

import hk.health.moe.pojo.dto.MoeUserSettingDto;
import org.springframework.security.access.ConfigAttribute;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public interface AuthenticationFacade {

	 Map<String, Collection<ConfigAttribute>> getOperationMethodMap();

	 HashMap<String, MoeUserSettingDto> getUserSetting(String loginId);
}
