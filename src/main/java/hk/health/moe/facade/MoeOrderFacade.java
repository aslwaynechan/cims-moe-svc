package hk.health.moe.facade;

import hk.health.moe.pojo.dto.webservice.OrderListResponse;
import hk.health.moe.pojo.dto.webservice.PrescriptionDto;
import hk.health.moe.pojo.dto.webservice.PrescriptionResponse;
import hk.health.moe.pojo.dto.webservice.ResponseBean;
import hk.health.moe.pojo.dto.MoeEhrOrderDto;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.pojo.po.MoeEhrOrderLogPo;
import hk.health.moe.pojo.po.MoeEhrOrderPo;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.List;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/7/30
 */
public interface MoeOrderFacade {

    List<MoeEhrOrderDto> getMoeOrder(UserDto userDto, int withinMonths, String prescType, String hospcode) throws Exception;

    List<MoeEhrOrderDto> getMoeOrder(UserDto userDto, String prescType, String hospcode) throws Exception;

    MoeEhrOrderPo addMoeOrder(MoeEhrOrderPo savePo, UserDto userDto) throws Exception;

    MoeEhrOrderPo updateMoeOrder(MoeEhrOrderPo savePo, MoeEhrOrderLogPo saveLogPo, UserDto userDto) throws Exception;

    PrescriptionDto getMoeOrder(long ordNo, String hospcode) throws Exception;

    OrderListResponse getMoeOrder(String hospCode, String patientkey, String episodeNo, String episodeType, XMLGregorianCalendar startSearchDate,
                                  XMLGregorianCalendar endSearchDate) throws Exception;

    PrescriptionResponse getMoeOrder(String hospCode, String patientKey,
                                     String episodeNo, String episodeType, String loginId) throws Exception;
    PrescriptionResponse getPatientOrderLog(String patientKey,
                                            XMLGregorianCalendar searchStartDate,
                                            XMLGregorianCalendar searchEndDate,
                                            String uploadMode) throws Exception;

    ResponseBean deleteMoeOrderByPatientRefKey(String patientRefKey, long ordNo, UserDto userDto) throws Exception;
}
