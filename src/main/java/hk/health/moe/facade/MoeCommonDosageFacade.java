package hk.health.moe.facade;

import hk.health.moe.pojo.dto.inner.InnerRelatedDrugDosageLocalDrug;
import hk.health.moe.pojo.dto.inner.InnerSaveCommonDosageDto;
import hk.health.moe.pojo.po.MoeCommonDosagePo;
import hk.health.moe.pojo.po.MoeDrugLocalPo;

import java.util.List;

public interface MoeCommonDosageFacade {

    void saveCommonDosage(List<InnerSaveCommonDosageDto> commonDosageDtos, List<InnerRelatedDrugDosageLocalDrug> relatedDrugDosageLocalDrugs, boolean isNewADrug, MoeDrugLocalPo dataPo) throws Exception;

    // Simon 20190912 for drug maintenance start--
    List<MoeCommonDosagePo> getCommonDosage(boolean isNewADrug, String localDrugId, String hkRegNo) throws Exception;
    //Simon 20190912 for drug maintenance start end--

    // Simon 20191014 start--
    void deleteMoeCommonDosageByLocalDrugId(String localDrugId) throws Exception;
    //Simon 20191014 end--

}
