package hk.health.moe.facade;

import hk.health.moe.pojo.dto.inner.InnerSaveDrugDto;

public interface MoeDrugToSaamFacade {

    void saveDrugDetail(InnerSaveDrugDto dto) throws Exception;

    void deleteDrugDetail(String localDrugId) throws Exception;

}
