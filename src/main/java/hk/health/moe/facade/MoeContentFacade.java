package hk.health.moe.facade;

public interface MoeContentFacade {

    void setContent() throws Exception;

    void setTempContent() throws Exception;

    void refreshSystemSetting() throws Exception;

}
