package hk.health.moe.facade;

import hk.health.moe.pojo.bo.CoreObjectBo;
import hk.health.moe.pojo.bo.ReportDataValueBo;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.pojo.po.MoeOrderPo;
import hk.health.moe.restservice.bean.saam.MoePatientSummaryDto;

import java.util.Date;

public interface ReportDataFacade {

    ReportDataValueBo reportForPrescription(MoeOrderPo moeOrderPo, UserDto userDto,
                                            CoreObjectBo coreObject, Date curDateTime,
                                            MoePatientSummaryDto patientSummaryDto, boolean failToCallSaam) throws Exception;

}
