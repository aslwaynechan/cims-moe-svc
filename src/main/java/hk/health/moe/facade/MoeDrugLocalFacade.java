package hk.health.moe.facade;

import hk.health.moe.pojo.dto.inner.InnerRelatedDrugDosageLocalDrug;
import hk.health.moe.pojo.dto.inner.InnerSaveDrugDto;
import hk.health.moe.pojo.po.MoeDrugLocalPo;

import java.util.List;

public interface MoeDrugLocalFacade {

    MoeDrugLocalPo saveDrugDetail(InnerSaveDrugDto dto, List<InnerRelatedDrugDosageLocalDrug> relatedDrugDosageLocalDrugs) throws Exception;

    // Simon 20191014 start--
    boolean deleteDrugs(String localDrugId) throws Exception;
    boolean deleteDrug(String localDrugId) throws Exception;
    //Simon 20191014 end--
}
