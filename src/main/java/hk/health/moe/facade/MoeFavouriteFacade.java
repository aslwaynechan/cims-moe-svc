package hk.health.moe.facade;

import hk.health.moe.pojo.dto.MoeMyFavouriteHdrDto;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.pojo.po.MoeMyFavouriteHdrPo;

import java.util.List;

public interface MoeFavouriteFacade {

    List<MoeMyFavouriteHdrDto> getMyFavourite(String userId, UserDto userDto) throws Exception;

    List<MoeMyFavouriteHdrDto> getDepartmentalFavourite(UserDto userDto) throws Exception;

    List<MoeMyFavouriteHdrDto> orderMyFavouritesDetail(List<MoeMyFavouriteHdrDto> favs, final UserDto userDto, boolean isDepartment) throws Exception;

    MoeMyFavouriteHdrDto orderMyFavouriteDetail(MoeMyFavouriteHdrDto fav, final UserDto userDto, boolean isDepartment) throws Exception;

    MoeMyFavouriteHdrDto deleteMyFavourite(String createUserId, MoeMyFavouriteHdrDto hdr) throws Exception;
    MoeMyFavouriteHdrDto deleteMyFavouriteDetail(String createUserId, MoeMyFavouriteHdrDto hdr) throws Exception;

    //List<MoeMyFavouriteHdrDto> deleteMyFavourite(String createUserId, List<MoeMyFavouriteHdrDto> hdr) throws Exception;
    List<MoeMyFavouriteHdrDto> sortMyFavourites(List<MoeMyFavouriteHdrDto> hdrs,final UserDto userDto, boolean isDepartment)throws Exception;

    MoeMyFavouriteHdrDto saveMyFavourite(MoeMyFavouriteHdrDto fav, final UserDto userDto, boolean isDepartment) throws Exception;
    MoeMyFavouriteHdrDto updateMyFavourite(MoeMyFavouriteHdrDto fav, final UserDto userDto, boolean isDepartment) throws Exception;

    // Ricci 20191108 start --
    MoeMyFavouriteHdrPo saveDepartFavourite(MoeMyFavouriteHdrDto saveDto, UserDto user) throws Exception;
    // Ricci 20191108 end --

}
