package hk.health.moe.facade.Impl;

import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.facade.MoeOrderFacade;
import hk.health.moe.pojo.dto.MoeEhrOrderDto;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.pojo.dto.webservice.OrderListResponse;
import hk.health.moe.pojo.dto.webservice.PrescriptionDto;
import hk.health.moe.pojo.dto.webservice.PrescriptionResponse;
import hk.health.moe.pojo.dto.webservice.ResponseBean;
import hk.health.moe.pojo.po.MoeEhrOrderLogPo;
import hk.health.moe.pojo.po.MoeEhrOrderPo;
import hk.health.moe.pojo.po.MoeMedProfilePo;
import hk.health.moe.pojo.po.MoeOrderLogPoPK;
import hk.health.moe.pojo.po.MoeOrderPo;
import hk.health.moe.pojo.po.MoePatientCasePo;
import hk.health.moe.pojo.po.MoePatientPo;
import hk.health.moe.repository.MoeEhrOrderLogRepository;
import hk.health.moe.repository.MoeEhrOrderRepository;
import hk.health.moe.repository.MoeOrderLogRepository;
import hk.health.moe.repository.MoeOrderRepository;
import hk.health.moe.service.MoeContentService;
import hk.health.moe.util.DateUtil;
import hk.health.moe.util.DtoMapUtil;
import hk.health.moe.util.EhrWsDtoMapUtil;
import hk.health.moe.util.Formatter;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.MoeWsDtoMapUtil;
import hk.health.moe.util.OrderLogUtil;
import hk.health.moe.util.StringUtil;
import hk.health.moe.util.XmlGregorianCalendarConversionUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.Response;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/7/30
 */
@Service
public class MoeOrderFacadeImpl implements MoeOrderFacade {

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;
    @Autowired
    private MoeEhrOrderRepository moeEhrOrderRepository;
    @Autowired
    private MoeOrderRepository moeOrderRepository;
    @Autowired
    private MoeOrderLogRepository moeOrderLogRepository;
    @Autowired
    private MoeEhrOrderLogRepository moeEhrOrderLogRepository;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    private MoeContentService moeContentService;

    @Override
    @Transactional(readOnly = true)
    public List<MoeEhrOrderDto> getMoeOrder(UserDto userDto, int withinMonths, String prescType, String hospcode) throws Exception {
        String patientKey = userDto.getMoePatientKey();
        if (StringUtil.stripToEmpty(patientKey).length() == 0) {
            //eric 20191224 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("moeOrder.patientNotExist"));
            throw new MoeServiceException(ResponseCode.SpecialMessage.MOEORDER_PATIENT_NOT_EXIST.getResponseCode(),
                    ResponseCode.SpecialMessage.MOEORDER_PATIENT_NOT_EXIST.getResponseMessage());
            //eric 20191224 end--
        }
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, withinMonths * -1);
        Date afterDate = cal.getTime();
        List<MoeEhrOrderPo> orders = null;
        if (ServerConstant.PRES_TYPE_ALL.equals(prescType)) {
            if (hospcode == null) {
                orders = moeEhrOrderRepository.findAllByPatient(patientKey, afterDate, ServerConstant.ORDER_STATUS_CREATE);
            } else {
                orders = moeEhrOrderRepository.findAllByPatient(patientKey, afterDate, ServerConstant.ORDER_STATUS_CREATE, hospcode);
            }
        } else {
            if (hospcode == null) {
                orders = moeEhrOrderRepository.findAllByPatientAndPrescType(patientKey, afterDate, prescType, ServerConstant.ORDER_STATUS_CREATE);
            } else {
                orders = moeEhrOrderRepository.findAllByPatientAndPrescType(patientKey, afterDate, prescType, ServerConstant.ORDER_STATUS_CREATE, hospcode);
            }
        }
        //Simon 20190816 start-- remove current order of the patient by caseNo
        List<MoeEhrOrderPo> byPatientKeyAndOrderNo = moeEhrOrderRepository.findByPatientKeyAndOrderNo(userDto.getMoePatientKey(), userDto.getOrderNum());
        orders.removeAll(byPatientKeyAndOrderNo);
        //Simon 20190816 start-- remove current order of the patient by caseNo
        List<MoeEhrOrderDto> ehrOrders = DtoMapUtil.convertMoeEhrOrderDtos(orders, false, userDto);
        return ehrOrders;
    }

    @Override
    @Transactional(readOnly = true)
    public List<MoeEhrOrderDto> getMoeOrder(UserDto userDto, String prescType, String hospcode) throws Exception {
        String patientKey = userDto.getMoePatientKey();
        if (StringUtil.stripToEmpty(patientKey).length() == 0) {
            throw new MoeServiceException(localeMessageSourceUtil.getMessage("moeOrder.patientNotExist"));
        }
        List<MoeEhrOrderPo> orders = null;
        if (ServerConstant.PRES_TYPE_ALL.equals(prescType)) {
            if (hospcode == null) {
                orders = moeEhrOrderRepository.findAllByPatient(patientKey, ServerConstant.ORDER_STATUS_CREATE);
            } else {
                orders = moeEhrOrderRepository.findAllByPatient(patientKey, ServerConstant.ORDER_STATUS_CREATE, hospcode);
            }
        } else {
            if (hospcode == null) {
                orders = moeEhrOrderRepository.findAllByPatientAndPrescType(patientKey, prescType, ServerConstant.ORDER_STATUS_CREATE);
            } else {
                orders = moeEhrOrderRepository.findAllByPatientAndPrescType(patientKey, prescType, ServerConstant.ORDER_STATUS_CREATE, hospcode);
            }
        }
        //Simon 20190816 start-- remove current order of the patient by caseNo
        List<MoeEhrOrderPo> byPatientKeyAndOrderNo = moeEhrOrderRepository.findByPatientKeyAndOrderNo(userDto.getMoePatientKey(), userDto.getOrderNum());
        orders.removeAll(byPatientKeyAndOrderNo);
        //Simon 20190816 start-- remove current order of the patient by caseNo
        List<MoeEhrOrderDto> ehrOrders = DtoMapUtil.convertMoeEhrOrderDtos(orders, false, userDto);
        return ehrOrders;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public MoeEhrOrderPo addMoeOrder(MoeEhrOrderPo savePo, UserDto userDto) throws Exception {
        moeOrderRepository.save(savePo.getMoeOrder());
        moeEhrOrderRepository.save(savePo);

        // Chris 20190808 Gen RefNo and Set RefNo  -Start
        //generate RefNo
        String refNo = Formatter.getRefNo(savePo.getMoeOrder().getOrdNo(), savePo.getMoeOrder().getVersion());

        savePo.getMoeOrder().setRefNo(refNo);

        // Log Order
        MoeEhrOrderLogPo orderLog = OrderLogUtil.convertMoeEhrOrderLog(savePo, userDto, ServerConstant.ORDER_TRANSACTION_TYPE_INSERT, true);

        moeOrderLogRepository.save(orderLog.getMoeOrderLog());
        moeEhrOrderLogRepository.save(orderLog);
        // Chris 20190808 Gen RefNo and Set RefNo  -End

        return savePo;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public MoeEhrOrderPo updateMoeOrder(MoeEhrOrderPo savePo, MoeEhrOrderLogPo saveLogPo, UserDto userDto) throws Exception {
        moeOrderRepository.save(savePo.getMoeOrder());
        savePo = moeEhrOrderRepository.save(savePo);

        moeOrderLogRepository.save(saveLogPo.getMoeOrderLog());
        moeEhrOrderLogRepository.save(saveLogPo);

        return savePo;
    }


    // Simon 20191105 for webservice start--
    @Override
    @Transactional(readOnly = true)
    public PrescriptionDto getMoeOrder(long ordNo, String hospcode) throws Exception {
        if (StringUtil.stripToEmpty(hospcode).length() == 0 || ordNo <= 0) {
            return null;
        }

        MoeEhrOrderPo order = moeEhrOrderRepository.findByHospCodeAndOrdNo(hospcode, ordNo);

        PrescriptionDto dto = null;
        if (order != null) {
            dto = MoeWsDtoMapUtil.convertMoeEhrOrderDto(order);
        }

        return dto;
    }

    @Override
    @Transactional(readOnly = true)
    public OrderListResponse getMoeOrder(String hospCode, String patientKey, String episodeNo, String episodeType, XMLGregorianCalendar startSearchDate,
                                         XMLGregorianCalendar endSearchDate) {

        Date startDate = DateUtil.clearTime(XmlGregorianCalendarConversionUtil.asDate(startSearchDate));
        Date endDate = DateUtil.clearTime(XmlGregorianCalendarConversionUtil.asDate(endSearchDate));


        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, -3);
        Date searchFromDate = cal.getTime();

        List<MoeEhrOrderPo> moeEhrOrderList = null;
        if (StringUtils.isBlank(episodeNo)) {
            if ("A".equals(episodeType)) {
                if (startDate != null && endDate != null) {
                    moeEhrOrderList = moeEhrOrderRepository.findAllByHospcodeAndPatientRefKeyAndOrdEndDate(hospCode, patientKey, startDate, endDate);
                } else {
                    moeEhrOrderList = moeEhrOrderRepository.findAllByHospcodeAndMoePatient(hospCode, patientKey, searchFromDate);
                }
            } else {
                if (startDate != null && endDate != null) {
                    moeEhrOrderList = moeEhrOrderRepository.findAllByHospcodeAndPatientRefKeyAndPrescTypeAndOrdEndDate(hospCode, patientKey, episodeType, startDate, endDate);
                } else {
                    moeEhrOrderList = moeEhrOrderRepository.findAllByHospcodeAndPatientRefKeyAndPrescType(hospCode, patientKey, episodeType, searchFromDate);
                }
            }
        } else {
            if (episodeType.equals("A")) {
                if (startDate != null && endDate != null) {
                    moeEhrOrderList = moeEhrOrderRepository.findAllByEpisodeNoAndOrderEndDate(hospCode, patientKey, episodeNo, startDate, endDate);
                } else {
                    moeEhrOrderList = moeEhrOrderRepository.findAllByEpisodeNo(hospCode, patientKey, episodeNo, searchFromDate);
                }

            } else {
                if (startDate != null && endDate != null) {
                    moeEhrOrderList = moeEhrOrderRepository.findAllByEpisodeNoAndPrescTypeAndOrdEndDate(hospCode, patientKey, episodeNo, episodeType, startDate, endDate);
                } else {
                    moeEhrOrderList = moeEhrOrderRepository.findAllByEpisodeNoAndPrescType(hospCode, patientKey, episodeNo, episodeType, searchFromDate);
                }
            }
        }

        OrderListResponse response = MoeWsDtoMapUtil.convertMoeEhrOrderDtos(moeEhrOrderList);
        return response;
    }

    @Transactional
    @Override
    public PrescriptionResponse getMoeOrder(String hospCode, String patientKey,
                                            String episodeNo, String episodeType, String loginId) throws Exception {

//        List<MoeEhrOrderPo> orderList = moeEhrOrderDao.getPatientOrderDetail(hospCode, patientKey, episodeNo, episodeType, loginId);
        List<MoeEhrOrderPo> orderList = null;
        if (StringUtils.isBlank(loginId)) {
            if (episodeType.equals("A")) {
                orderList = moeEhrOrderRepository.findPatientOrderDetail(hospCode, patientKey, episodeNo);
            } else {
                orderList = moeEhrOrderRepository.findPatientOrderDetailByPrescType(hospCode, patientKey, episodeNo, episodeType);
            }
        } else {
            if (episodeType.equals("A")) {
                orderList = moeEhrOrderRepository.findPatientOrderDetailByLoginId(loginId, hospCode, patientKey, episodeNo);
            } else {
                orderList = moeEhrOrderRepository.findPatientOrderDetailByLoginIdAndPrescType(loginId, hospCode, patientKey, episodeNo, episodeType);
            }
        }
        PrescriptionResponse prescriptionResponse = new PrescriptionResponse();
        for (MoeEhrOrderPo order : orderList) {
            prescriptionResponse.getPrescriptionDto().add(MoeWsDtoMapUtil.convertMoeEhrOrderDto(order));
        }
        return prescriptionResponse;
    }

    @Transactional
    @Override
    public PrescriptionResponse getPatientOrderLog(String patientKey, XMLGregorianCalendar searchStartDate, XMLGregorianCalendar searchEndDate, String uploadMode) {
        if (patientKey == null)
            return null;
        Date startDate = XmlGregorianCalendarConversionUtil.asDate(searchStartDate);
        Date endDate = XmlGregorianCalendarConversionUtil.asDate(searchEndDate);

//        List<MoeEhrOrderLogPo> orderList = moeEhrOrderLogDao.getPatientOrder(patientKey, startDate, endDate, uploadMode);
        List<MoeEhrOrderLogPo> orderList = null;

        StringBuilder sql = new StringBuilder("SELECT " +
                "distinct o2.ord_no,o2.ref_no,o2.last_Upd_Date  " +
                "FROM " +
                "moe_ehr_order_log o " +
                "INNER JOIN moe_order_log o2 ON o.ord_No = o.ord_No  " +
                "AND o.REF_NO = o2.REF_NO " +
                "INNER JOIN moe_med_profile_log p ON o.ord_No = p.ord_No  " +
                "AND o.ref_no = p.ref_no " +
                "INNER JOIN moe_ehr_med_profile_log pl ON p.ord_No = pl.ord_no  " +
                "AND p.hospcode = pl.hospcode  " +
                "AND p.CMS_ITEM_NO = pl.CMS_ITEM_NO  " +
                "AND p.ref_no = pl.ref_no " +
                "inner join moe_patient pa on o.moe_patient_key = pa.moe_patient_key WHERE ");
        if ("DM".equalsIgnoreCase(uploadMode)) {
            sql.append(" pl.trx_Type <> 'D' AND ");
        }

        if (searchStartDate != null && searchEndDate != null) {
            sql.append(" pa.patient_ref_key = :patientKey" +
                    " and o2.last_Upd_Date >= :searchStartDate and o2.last_Upd_Date <= :searchEndDate" +
                    " and o2.last_Upd_Date in (select max(last_Upd_Date) from Moe_Order_Log where ord_no = o.ord_no and last_Upd_Date >= :searchStartDate and last_Upd_Date <= :searchEndDate)" +
                    " and o2.version in (select max(version) from Moe_Order_Log where ord_No = o.ord_No and last_Upd_Date = o2.last_Upd_Date)" +
                    " order by o2.last_Upd_Date desc");
            Map<String, Object> paraMap = new HashMap<>();
            paraMap.put("patientKey", patientKey);
            paraMap.put("searchStartDate", startDate);
            paraMap.put("searchEndDate", endDate);
            orderList = namedParameterJdbcTemplate.query(sql.toString(), paraMap, new BeanPropertyRowMapper<>(MoeEhrOrderLogPo.class));
        } else if (searchStartDate != null || searchEndDate != null) {
            String searchDateString = "";
            String searchDateString2 = "";
            //Date date = startDate;
            Map<String, Object> paraMap = new HashMap<>();
            if (searchStartDate != null) {
                searchDateString = " and o2.last_Upd_Date >= :searchStartDate";
                searchDateString2 = " and last_Upd_Date >= :searchStartDate";
                paraMap.put("searchStartDate", startDate);
            } else {
                //date = endDate;
                searchDateString = " and o2.last_Upd_Date <= :searchEndDate";
                searchDateString2 = " and last_Upd_Date <= :searchEndDate ";
                paraMap.put("searchEndDate", endDate);
            }
            sql.append(" pa.patient_ref_key = :patientKey" +
                    searchDateString +
                    " and o2.last_Upd_Date in (select max(last_Upd_Date) from Moe_Order_Log where ord_No = o.ord_No " + searchDateString2 + " )" +
                    " and o2.version in (select max(version) from Moe_Order_Log where ord_No = o.ord_No and last_Upd_Date = o2.last_Upd_Date)" +
                    " order by o2.last_Upd_Date desc");

            paraMap.put("patientKey", patientKey);
            orderList = namedParameterJdbcTemplate.query(sql.toString(), paraMap, new BeanPropertyRowMapper<>(MoeEhrOrderLogPo.class));
        } else {
            sql.append(" pa.patient_ref_key = :patientKey" +
                    " and o2.last_Upd_Date in (select max(last_Upd_Date) from Moe_Order_Log where ord_No = o.ord_No)" +
                    " and o2.version in (select max(version) from Moe_Order_Log where ord_No = o.ord_No and last_Upd_Date = o2.last_Upd_Date)" +
                    " order by o2.last_Upd_Date desc");
            Map<String, Object> paraMap = new HashMap<>();
            paraMap.put("patientKey", patientKey);
            orderList = namedParameterJdbcTemplate.query(sql.toString(), paraMap, new BeanPropertyRowMapper<>(MoeEhrOrderLogPo.class));
        }
        PrescriptionResponse prescriptionResponse = new PrescriptionResponse();

        List<MoeOrderLogPoPK> pkList = new ArrayList<>();
        MoeOrderLogPoPK pk = null;
        for (MoeEhrOrderLogPo order : orderList) {
            pk = new MoeOrderLogPoPK();
            pk.setOrdNo(order.getOrdNo());
            pk.setRefNo(order.getRefNo());
            pkList.add(pk);
        }

        orderList = moeEhrOrderLogRepository.findAllById(pkList);
        for (MoeEhrOrderLogPo order : orderList) {
            prescriptionResponse.getPrescriptionDto().add(EhrWsDtoMapUtil.convertMoeEhrOrderDto(order, uploadMode));
        }
        return prescriptionResponse;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public ResponseBean deleteMoeOrderByPatientRefKey(String patientRefKey, long ordNo, final UserDto userDto) throws Exception {
        ResponseBean response = new ResponseBean();
        if (StringUtil.stripToEmpty(patientRefKey).length() == 0 || ordNo <= 0 || userDto == null) {
            response.setResponseErrorCode(ServerConstant.WARN_INVALID_PARAM);
            return response;
        }
        MoeEhrOrderPo order = moeEhrOrderRepository.findByPatientRefKeyAndOrderNo(patientRefKey, ordNo);
        if (order == null) {
//            LOGGER.warn("Cannot find order["+ordNo+"] for patient["+patientRefKey+"]");
            response.setResponseErrorCode(ServerConstant.WARN_INVALID_PARAM);
            return response;
        }

        setDeleteInfo(order, userDto);
        order.getMoeOrder().setRefNo(Formatter.getRefNo(order.getMoeOrder().getOrdNo(), order.getMoeOrder().getVersion() + 1));
//        moeEhrOrderDao.merge(order);
        moeEhrOrderRepository.save(order);
        // Log Order
        MoeEhrOrderLogPo orderLog = OrderLogUtil.convertMoeEhrOrderLog(order, userDto, ServerConstant.ORDER_TRANSACTION_TYPE_DELETE, true);
//        moeEhrOrderLogDao.insert(orderLog);
//        MoeOrderLogPo po = orderLog.getMoeOrderLog();
//        po.setMoeEhrOrderLog(orderLog);
        moeOrderLogRepository.saveAndFlush(orderLog.getMoeOrderLog());
        moeEhrOrderLogRepository.saveAndFlush(orderLog);
        // Ricci 20191106 start -- remove redis cache
//        String redisKey = String.format(ServerConstant.MOE_CACHE_ORDER_PREFIX,
//                userDto.getMrnPatientIdentity(), userDto.getMrnPatientEncounterNum());
//        moeContentService.deleteOrder(redisKey);

        response.setResponseErrorCode(ServerConstant.SUCCESS_ERRCODE);
        return response;
    }

    private void setDeleteInfo(MoeEhrOrderPo order, final UserDto userDto) {
        Date now = new Date();

        updateOrder(order, userDto, now, false);

        order.getMoeOrder().setOrdStatus("CO");
        for (MoeMedProfilePo profile : order.getMoeOrder().getMoeMedProfiles()) {
            profile.setItemStatus("D");
        }
    }

    private void updateOrder(MoeEhrOrderPo order, final UserDto userDto, final Date now, boolean setCreateInfo) {
        updateOrder(null, order, userDto, now, setCreateInfo);
    }

    private void updateOrder(final MoeEhrOrderPo oorder, MoeEhrOrderPo order, final UserDto userDto, final Date now, boolean setCreateInfo) {
        if (setCreateInfo) {
            order.setHospcode(userDto.getHospitalCd());
            MoePatientPo patient = new MoePatientPo();
            patient.setMoePatientKey(userDto.getMoePatientKey());
            order.setMoePatient(patient);
            MoePatientCasePo patientCase = new MoePatientCasePo();
            patientCase.setMoeCaseNo(userDto.getMoeCaseNo());
            order.setMoePatientCase(patientCase);
            order.setCreateUserId(userDto.getLoginId());
            order.setCreateUser(userDto.getLoginName());
            order.setCreateHosp(userDto.getHospitalCd());
            order.setCreateRank(userDto.getUserRankCd());
            order.setCreateRankDesc(userDto.getUserRankDesc());
        }
        order.setUpdateUserId(userDto.getLoginId());
        order.setUpdateUser(userDto.getLoginName());
        order.setUpdateHosp(userDto.getHospitalCd());
        order.setUpdateRank(userDto.getUserRankCd());
        order.setUpdateRankDesc(userDto.getUserRankDesc());

        MoeOrderPo moeOrder = order.getMoeOrder();
        moeOrder.setLastUpdDate(now);
        if (userDto.getActionCd() != null && userDto.getActionCd().equalsIgnoreCase(ServerConstant.MODE_EDIT_WITH_REMARK)) {
            order.setRemarkHosp(userDto.getHospitalCd());
            order.setRemarkRank(userDto.getUserRankCd());
            order.setRemarkRankDesc(userDto.getUserRankDesc());
            order.setRemarkUser(userDto.getLoginName());
            order.setRemarkUserId(userDto.getLoginId());

            if (oorder != null) {
                order.setUpdateUserId(oorder.getUpdateUserId());
                order.setUpdateUser(oorder.getUpdateUser());
                order.setUpdateHosp(oorder.getUpdateHosp());
                order.setUpdateRank(oorder.getUpdateRank());
                order.setUpdateRankDesc(oorder.getUpdateRankDesc());
                //moeOrder.setLastUpdDate(oorder.getMoeOrder().getLastUpdDate());
                moeOrder.setLastUpdBy(oorder.getMoeOrder().getLastUpdBy());
            } else {
                // Delete order
                //moeOrder.setLastUpdDate(now);
                moeOrder.setLastUpdBy(userDto.getLoginId());
            }
        } else {
            order.setUpdateUserId(userDto.getLoginId());
            order.setUpdateUser(userDto.getLoginName());
            order.setUpdateHosp(userDto.getHospitalCd());
            order.setUpdateRank(userDto.getUserRankCd());
            order.setUpdateRankDesc(userDto.getUserRankDesc());

            order.setRemarkHosp(null);
            order.setRemarkRank(null);
            order.setRemarkRankDesc(null);
            order.setRemarkUser(null);
            order.setRemarkUserId(null);

            //moeOrder.setLastUpdDate(now);
            moeOrder.setLastUpdBy(userDto.getLoginId());
        }
        if (setCreateInfo) {
            moeOrder.setHospcode(userDto.getHospitalCd());
            moeOrder.setPatHospcode(userDto.getHospitalCd());
            if (moeOrder.getBackDate() != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
                try {
                    moeOrder.setOrdDate(sdf.parse(moeOrder.getBackDate()));
                } catch (ParseException e) {

                }
            } else {
                moeOrder.setOrdDate(now);
            }
            moeOrder.setOrdType("P");
            moeOrder.setOrdStatus("O");
            moeOrder.setMoCode(userDto.getLoginId());
            moeOrder.setCaseNo(userDto.getMoeCaseNo());
            moeOrder.setIpasWard(userDto.getWard());
            moeOrder.setBedNo(userDto.getBedNum());
            moeOrder.setSpecialty(userDto.getSpec());
            if (userDto.getSourceSystem() != null) {
                moeOrder.setOrdSubtype(ServerConstant.SOURCE_SYSTEM_ORD_SUBTYPE_MAP.get(userDto.getSourceSystem()));
            }
            if (moeOrder.getBackDate() != null) {
                moeOrder.setOrdSubtype("B");
            }
        }
    }

    //Simon 20191105 end--
}
