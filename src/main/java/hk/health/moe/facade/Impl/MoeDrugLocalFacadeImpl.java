package hk.health.moe.facade.Impl;

import hk.health.moe.common.MoeCommonHelper;
import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.facade.MoeCommonDosageFacade;
import hk.health.moe.facade.MoeDrugBySpecialtyFacade;
import hk.health.moe.facade.MoeDrugLocalFacade;
import hk.health.moe.facade.MoeDrugTherapeuticLocalFacade;
import hk.health.moe.facade.MoeDrugToSaamFacade;
import hk.health.moe.facade.SaDrugLocalFacade;
import hk.health.moe.pojo.dto.MoeDrugAliasNameLocalDto;
import hk.health.moe.pojo.dto.MoeDrugLocalDto;
import hk.health.moe.pojo.dto.MoeDrugOrdPropertyLocalDto;
import hk.health.moe.pojo.dto.MoeDrugStrengthLocalDto;
import hk.health.moe.pojo.dto.inner.InnerRelatedDrugDosageLocalDrug;
import hk.health.moe.pojo.dto.inner.InnerSaveDrugDto;
import hk.health.moe.pojo.po.MoeDrugAliasNameLocalLogPo;
import hk.health.moe.pojo.po.MoeDrugAliasNameLocalPo;
import hk.health.moe.pojo.po.MoeDrugLocalLogPo;
import hk.health.moe.pojo.po.MoeDrugLocalPo;
import hk.health.moe.pojo.po.MoeDrugOrdPropertyLocLogPo;
import hk.health.moe.pojo.po.MoeDrugOrdPropertyLocalPo;
import hk.health.moe.pojo.po.MoeDrugStrengthLocalLogPo;
import hk.health.moe.pojo.po.MoeDrugStrengthLocalPo;
import hk.health.moe.pojo.po.MoeDrugStrengthPo;
import hk.health.moe.pojo.po.MoeMyFavouriteDetailPo;
import hk.health.moe.pojo.po.MoeMyFavouriteHdrPo;
import hk.health.moe.repository.MoeDrugAliasNameLocalLogRepository;
import hk.health.moe.repository.MoeDrugAliasNameLocalRepository;
import hk.health.moe.repository.MoeDrugLocalLogRepository;
import hk.health.moe.repository.MoeDrugLocalRepository;
import hk.health.moe.repository.MoeDrugOrdPropertyLocLogRepository;
import hk.health.moe.repository.MoeDrugOrdPropertyLocalRepository;
import hk.health.moe.repository.MoeDrugStrengthLocalLogRepository;
import hk.health.moe.repository.MoeDrugStrengthLocalRepository;
import hk.health.moe.repository.MoeDrugStrengthRepository;
import hk.health.moe.repository.MoeMyFavouriteHdrRepository;
import hk.health.moe.service.impl.BaseServiceImpl;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util2.Formatter;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class MoeDrugLocalFacadeImpl extends BaseServiceImpl implements MoeDrugLocalFacade {

    @Autowired
    private MoeDrugLocalRepository moeDrugLocalRepository;

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private MoeCommonDosageFacade moeCommonDosageFacade;

    @Autowired
    private MoeMyFavouriteHdrRepository moeMyFavouriteHdrRepository;

    @Autowired
    private MoeDrugStrengthLocalRepository moeDrugStrengthLocalRepository;

    @Autowired
    private MoeDrugStrengthLocalLogRepository moeDrugStrengthLocalLogRepository;

    @Autowired
    private MoeDrugLocalLogRepository moeDrugLocalLogRepository;

    @Autowired
    private MoeDrugAliasNameLocalRepository moeDrugAliasNameLocalRepository;

    @Autowired
    private MoeDrugAliasNameLocalLogRepository moeDrugAliasNameLocalLogRepository;

    @Autowired
    private MoeDrugOrdPropertyLocalRepository moeDrugOrdPropertyLocalRepository;

    @Autowired
    private MoeDrugOrdPropertyLocLogRepository moeDrugOrdPropertyLocLogRepository;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    // Simon 20191014 start--
    @Autowired
    private SaDrugLocalFacade saDrugLocalFacade;

    @Autowired
    private MoeDrugToSaamFacade moeDrugToSaamFacade;

    @Autowired
    private MoeDrugBySpecialtyFacade moeDrugBySpecialtyFacade;

    @Autowired
    private MoeDrugTherapeuticLocalFacade moeDrugTherapeuticLocalFacade;
    //Simon 20191014 end--

    //Chris 20200109  -Start
    @Autowired
    private MoeDrugStrengthRepository moeDrugStrengthRepository;
    //Chris 20200109  -End


    @Override
    public MoeDrugLocalPo saveDrugDetail(InnerSaveDrugDto dto, List<InnerRelatedDrugDosageLocalDrug> relatedDrugDosageLocalDrugs) throws Exception {

        if (!dto.isNewDrugRecord()) {
            updateMyFavourite(dto.getMoeDrugLocal(), dto.getMoeDrugStrengthLocal(), dto.getMoeDrugOrdPropertyLocal(), dto.isLocalDrugOnly());
        }

        // Update DrugDetail Page Records
        if (dto.isLocalDrugOnly() || dto.isNewDrugRecord()) {
            MoeDrugLocalPo savePo = null;
            //upsertMoeDrugStrengthLocal(dto.getMoeDrugStrengthLocal());
            upsertMoeDrugStrengthLocal(dto.getMoeDrugStrengthLocal(), dto.getHkRegNo());
            savePo = upsertMoeDrugLocal(dto.getMoeDrugLocal());
            upsertMoeDrugAliasNameLocal(dto.getMoeDrugAliasNameLocalList(), savePo);
            upsertMoeDrugOrdPropertyLocal(dto.getMoeDrugOrdPropertyLocal(), savePo, relatedDrugDosageLocalDrugs);

            return savePo;
        } else {
            MoeDrugLocalPo dataPo = mapper.map(dto.getMoeDrugLocal(), MoeDrugLocalPo.class);
            //upsertMoeDrugStrengthLocal(dto.getMoeDrugStrengthLocal());
            upsertMoeDrugStrengthLocal(dto.getMoeDrugStrengthLocal(), dto.getHkRegNo());
            updateBaseUnit(dto.getMoeDrugLocal(), relatedDrugDosageLocalDrugs);
            upsertMoeDrugOrdPropertyLocal(dto.getMoeDrugOrdPropertyLocal(), dataPo, relatedDrugDosageLocalDrugs);

            return dataPo;
        }
    }

    private void updateMyFavourite(MoeDrugLocalDto drug, MoeDrugStrengthLocalDto strength, MoeDrugOrdPropertyLocalDto ordProperty, boolean isLocalDrugOnly) throws Exception {
        if (drug == null || strength == null || ordProperty == null) {
            //eric 20191223 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.error"));
            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage());
            //eric 20191223 end--
        }

        String localDrugId = drug.getLocalDrugId();
        if (StringUtils.isEmpty(localDrugId)) {
            //eric 20191223 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.error"));
            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage());
            //eric 20191223 end--
        }

        MoeDrugLocalPo moeDrugLocal = null;
        MoeDrugStrengthLocalPo moeDrugStrengthLocal = null;
        MoeDrugOrdPropertyLocalPo moeDrugOrdPropertyLocal = null;
        Optional<MoeDrugLocalPo> optionalMoeDrugLocalPo = moeDrugLocalRepository.findById(localDrugId);
        if (optionalMoeDrugLocalPo.isPresent()) {
            moeDrugLocal = optionalMoeDrugLocalPo.get();
            moeDrugStrengthLocal = moeDrugLocal.getMoeDrugStrengths().iterator().next();
            moeDrugOrdPropertyLocal = moeDrugLocal.getMoeDrugOrdPropertyLocal();
        }

        if (moeDrugLocal != null) {
            // check if auto-update field is updated
            boolean updateMyFavourite = false;
            if (drug.getTerminologyName() == null) {
                //LOGGER.debug("Update My Favourite trigger by Drug Maintenance - Local Drug");
                if ((moeDrugLocal.getTradeName() == null && drug.getTradeName() != null)
                        || (moeDrugLocal.getTradeName() != null && drug.getTradeName() == null)
                        || (moeDrugLocal.getTradeName() != null && drug.getTradeName() != null
                        && !moeDrugLocal.getTradeName().equals(drug.getTradeName())))
                    updateMyFavourite = true;
                else if (moeDrugLocal.getVtm() != null
                        && !moeDrugLocal.getVtm().equals(drug.getVtm()) ||
                        drug.getVtm() != null && !drug.getVtm().equals(moeDrugLocal.getVtm()))
                    updateMyFavourite = true;
                else if ((moeDrugStrengthLocal.getStrength() == null && strength.getStrength() != null)
                        || (moeDrugStrengthLocal.getStrength() != null && strength.getStrength() == null)
                        || (moeDrugStrengthLocal.getStrength() != null && strength.getStrength() != null
                        && !moeDrugStrengthLocal.getStrength().equals(strength.getStrength())))
                    updateMyFavourite = true;
                else if ((moeDrugStrengthLocal.getStrengthLevelExtraInfo() == null && strength.getStrengthLevelExtraInfo() != null)
                        || (moeDrugStrengthLocal.getStrengthLevelExtraInfo() != null && strength.getStrengthLevelExtraInfo() == null)
                        || (moeDrugStrengthLocal.getStrengthLevelExtraInfo() != null && strength.getStrengthLevelExtraInfo() != null
                        && !moeDrugStrengthLocal.getStrengthLevelExtraInfo().equals(strength.getStrengthLevelExtraInfo())))
                    updateMyFavourite = true;
                else if ((moeDrugLocal.getRouteId() == null && drug.getRouteId() != null)
                        || (moeDrugLocal.getRouteId() != null && drug.getRouteId() == null)
                        || (moeDrugLocal.getRouteId() != null && drug.getRouteId() != null
                        && !moeDrugLocal.getRouteId().equals(drug.getRouteId())))
                    updateMyFavourite = true;
                else if ((moeDrugLocal.getRouteEng() == null && drug.getRouteEng() != null)
                        || (moeDrugLocal.getRouteEng() != null && drug.getRouteEng() == null)
                        || (moeDrugLocal.getRouteEng() != null && drug.getRouteEng() != null
                        && !moeDrugLocal.getRouteEng().equals(drug.getRouteEng())))
                    updateMyFavourite = true;
                else if ((moeDrugLocal.getFormId() == null && drug.getFormId() != null)
                        || (moeDrugLocal.getFormId() != null && drug.getFormId() == null)
                        || (moeDrugLocal.getFormId() != null && drug.getFormId() != null
                        && !moeDrugLocal.getFormId().equals(drug.getFormId())))
                    updateMyFavourite = true;
                else if ((moeDrugLocal.getFormEng() == null && drug.getFormEng() != null)
                        || (moeDrugLocal.getFormEng() != null && drug.getFormEng() == null)
                        || (moeDrugLocal.getFormEng() != null && drug.getFormEng() != null
                        && !moeDrugLocal.getFormEng().equals(drug.getFormEng())))
                    updateMyFavourite = true;
                else if ((moeDrugLocal.getDoseFormExtraInfoId() == null && drug.getDoseFormExtraInfoId() != null)
                        || (moeDrugLocal.getDoseFormExtraInfoId() != null && drug.getDoseFormExtraInfoId() == null)
                        || (moeDrugLocal.getDoseFormExtraInfoId() != null && drug.getDoseFormExtraInfoId() != null
                        && !moeDrugLocal.getDoseFormExtraInfoId().equals(drug.getDoseFormExtraInfoId())))
                    updateMyFavourite = true;
                else if ((moeDrugLocal.getDoseFormExtraInfo() == null && drug.getDoseFormExtraInfo() != null)
                        || (moeDrugLocal.getDoseFormExtraInfo() != null && drug.getDoseFormExtraInfo() == null)
                        || (moeDrugLocal.getDoseFormExtraInfo() != null && drug.getDoseFormExtraInfo() != null
                        && !moeDrugLocal.getDoseFormExtraInfo().equals(drug.getDoseFormExtraInfo())))
                    updateMyFavourite = true;
                else if ((moeDrugStrengthLocal.getAmp() == null && strength.getAmp() != null)
                        || (moeDrugStrengthLocal.getAmp() != null && strength.getAmp() == null)
                        || (moeDrugStrengthLocal.getAmp() != null && strength.getAmp() != null
                        && !moeDrugStrengthLocal.getAmp().equals(strength.getAmp())))
                    updateMyFavourite = true;
            } else if ("HKCTT".equals(drug.getTerminologyName())) { // && name_type = 'N'
                //LOGGER.debug("Update My Favourite trigger by Drug Maintenance HKMTT");
                if ((moeDrugOrdPropertyLocal.getTradeNameAlias() == null && ordProperty.getTradeNameAlias() != null)
                        || (moeDrugOrdPropertyLocal.getTradeNameAlias() != null && ordProperty.getTradeNameAlias() == null)
                        || (moeDrugOrdPropertyLocal.getTradeNameAlias() != null && ordProperty.getTradeNameAlias() != null
                        && !moeDrugOrdPropertyLocal.getTradeNameAlias().equals(ordProperty.getTradeNameAlias())))
                    updateMyFavourite = true;
            }

            if (updateMyFavourite) {
				
				/*
				MoeDrugStrengthLocalPo moeDrugStrengthLocal = null;
				if(moeDrugLocal.getMoeDrugStrengths().size() > 0) {
					moeDrugStrengthLocal = moeDrugLocal.getMoeDrugStrengths().iterator().next();
				} else {
					moeDrugStrengthLocal = new MoeDrugStrengthLocalPo();
				}
				*/

                // get my favourite
                List<MoeMyFavouriteHdrPo> myFavouriteHdrs = null;
                //local drug
                if (isLocalDrugOnly) {
                    //LOGGER.debug("updateMyFavourite = true, get my fav -local");
                    myFavouriteHdrs = moeMyFavouriteHdrRepository.findAllByLocalDrug(moeDrugLocal.getTradeName(),
                            moeDrugLocal.getVtm(),
                            moeDrugStrengthLocal.getStrength(),
                            moeDrugStrengthLocal.getStrengthLevelExtraInfo(),
                            Formatter.nullToZero(moeDrugLocal.getFormId()),
                            moeDrugLocal.getFormEng(),
                            moeDrugLocal.getDoseFormExtraInfo(),
                            Formatter.nullToZero(moeDrugLocal.getDoseFormExtraInfoId()),
                            moeDrugLocal.getRouteEng(),
                            Formatter.nullToZero(moeDrugLocal.getRouteId()),
                            moeDrugStrengthLocal.getAmp());
                }
                //mtt drug
                else {
                    if (MoeCommonHelper.isFlagFalse(moeDrugLocal.getStrengthCompulsory())) {
                        //LOGGER.debug("updateMyFavourite = true, get my fav -StrengthCompulsory: N, from " + moeDrugOrdPropertyLocal.getTradeNameAlias() + " to " + ordProperty.getTradeNameAlias());
                        myFavouriteHdrs = moeMyFavouriteHdrRepository.findAllByMttDrugN(moeDrugLocal.getTradeName(),
                                moeDrugLocal.getVtm(),
                                Formatter.nullToZero(moeDrugLocal.getFormId()),
                                moeDrugLocal.getFormEng(),
                                moeDrugLocal.getDoseFormExtraInfo(),
                                Formatter.nullToZero(moeDrugLocal.getDoseFormExtraInfoId()),
                                moeDrugLocal.getRouteEng(),
                                Formatter.nullToZero(moeDrugLocal.getRouteId()),
                                moeDrugOrdPropertyLocal.getTradeNameAlias());
                    } else if (MoeCommonHelper.isFlagTrue(moeDrugLocal.getStrengthCompulsory())) {
                        //LOGGER.debug("updateMyFavourite = true, get my fav -StrengthCompulsory: Y, from " + moeDrugOrdPropertyLocal.getTradeNameAlias() + " to " + ordProperty.getTradeNameAlias());
                        if (drug.getVtm().indexOf("+") > 0) {
                            myFavouriteHdrs = moeMyFavouriteHdrRepository.findAllByMttDrugY(moeDrugLocal.getTradeName(),
                                    moeDrugLocal.getVtm(),
                                    moeDrugStrengthLocal.getStrength(),
                                    moeDrugStrengthLocal.getStrengthLevelExtraInfo(),
                                    Formatter.nullToZero(moeDrugLocal.getFormId()),
                                    moeDrugLocal.getFormEng(),
                                    moeDrugLocal.getDoseFormExtraInfo(),
                                    Formatter.nullToZero(moeDrugLocal.getDoseFormExtraInfoId()),
                                    moeDrugLocal.getRouteEng(),
                                    Formatter.nullToZero(moeDrugLocal.getRouteId()),
                                    (moeDrugOrdPropertyLocal.getTradeNameAlias() == null ?
                                            moeDrugStrengthLocal.getAmp() : "[" + moeDrugOrdPropertyLocal.getTradeNameAlias() + "] " + moeDrugStrengthLocal.getAmp()),
                                    moeDrugOrdPropertyLocal.getTradeNameAlias());
                        } else {
                            myFavouriteHdrs = moeMyFavouriteHdrRepository.findAllBySingleMttDrugY(moeDrugLocal.getTradeName(),
                                    moeDrugLocal.getVtm(),
                                    moeDrugStrengthLocal.getStrength(),
                                    moeDrugStrengthLocal.getStrengthLevelExtraInfo(),
                                    Formatter.nullToZero(moeDrugLocal.getFormId()),
                                    moeDrugLocal.getFormEng(),
                                    moeDrugLocal.getDoseFormExtraInfo(),
                                    Formatter.nullToZero(moeDrugLocal.getDoseFormExtraInfoId()),
                                    moeDrugLocal.getRouteEng(),
                                    Formatter.nullToZero(moeDrugLocal.getRouteId()),
                                    moeDrugOrdPropertyLocal.getTradeNameAlias());
                        }
                    }
                }

                // update my favourite
                if (myFavouriteHdrs != null && myFavouriteHdrs.size() > 0) {
                    //LOGGER.debug("Update My Favourite trigger by Drug Maintenance: Size of myFavouriteHdr:" + myFavouriteHdr.size());
                    if (isLocalDrugOnly) {
                        for (MoeMyFavouriteHdrPo myFavourite : myFavouriteHdrs) {
                            for (MoeMyFavouriteDetailPo detail : myFavourite.getMoeMyFavouriteDetails()) {
                                detail.setTradeName(drug.getTradeName());
                                detail.setVtm(drug.getVtm());
                                detail.setPreparation(strength.getStrength());
                                detail.setStrengthLevelExtraInfo(strength.getStrengthLevelExtraInfo());
                                detail.setFormId(drug.getFormId());
                                detail.setFormEng(drug.getFormEng());
                                detail.setDoseFormExtraInfo(drug.getDoseFormExtraInfo());
                                detail.setDoseFormExtraInfoId(drug.getDoseFormExtraInfoId());
                                detail.setDrugRouteEng(drug.getRouteEng());
                                detail.setDrugRouteId(drug.getRouteId());
                                detail.setScreenDisplay(strength.getAmp());
                            }
                        }
                        //LOGGER.debug("Update My Favourite trigger by Drug Maintenance - Local Drug");
                    } else {
                        for (MoeMyFavouriteHdrPo myFavourite : myFavouriteHdrs) {
                            for (MoeMyFavouriteDetailPo detail : myFavourite.getMoeMyFavouriteDetails()) {
                                detail.setAliasName(ordProperty.getTradeNameAlias());
                                if (drug.getVtm().indexOf("+") > 0) {
                                    //multiple ingredient product
                                    //LOGGER.debug("updateMyFavourites Trade Name Alias by Drug Maintenance - HKMTT - multiple ingredient product");
                                    if (ordProperty.getTradeNameAlias() != null) {
                                        detail.setScreenDisplay("[" + ordProperty.getTradeNameAlias() + "] " + strength.getAmp());
                                    } else {
                                        detail.setScreenDisplay(strength.getAmp());
                                        detail.setNameType("V");
                                    }
                                } else {
                                    //single ingredient product
                                    //LOGGER.debug("updateMyFavourites Trade Name Alias by Drug Maintenance - HKMTT - single ingredient product");
                                    if (ordProperty.getTradeNameAlias() != null) {
                                        detail.setScreenDisplay("[" + ordProperty.getTradeNameAlias() + "] " + drug.getTradeName() + " (" + drug.getVtm() + ")");
                                    } else {
                                        detail.setScreenDisplay(drug.getTradeName() + " (" + drug.getVtm() + ")");
                                        detail.setNameType("V");
                                    }
                                }
                            }
                        }
                    }
                    moeMyFavouriteHdrRepository.saveAll(myFavouriteHdrs);
                } else {
                    //LOGGER.debug("Update My Favourite trigger by Drug Maintenance: no myFavourite records.");
                }
            }
        }
    }

    //Chris 20200109 For find moeDrugStrength by HkRegNo  -Start
    //private void upsertMoeDrugStrengthLocal(MoeDrugStrengthLocalDto saveDto) throws Exception {
    private void upsertMoeDrugStrengthLocal(MoeDrugStrengthLocalDto saveDto, String hkRegNo) throws Exception {
    //Chris 20200109 For find moeDrugStrength by HkRegNo  -End

        Date now = new Date();
        if (StringUtils.isEmpty(saveDto.getVmp())) {
            saveDto.setVmp(null);
        }
        MoeDrugStrengthLocalPo savePo = mapper.map(saveDto, MoeDrugStrengthLocalPo.class);
        // Init Update Info
        savePo.setUpdateHosp(getUserDto().getHospitalCd());
        savePo.setUpdateRank(getUserDto().getUserRankCd());
        savePo.setUpdateRankDesc(getUserDto().getUserRankDesc());
        savePo.setUpdateUser(getUserDto().getLoginName());
        savePo.setUpdateUserId(getUserDto().getLoginId());
        savePo.setUpdateDtm(now);

        if (moeDrugStrengthLocalRepository.existsById(saveDto.getLocalDrugId())) {
            MoeDrugStrengthLocalDto createInfo = moeDrugStrengthLocalRepository.findCreateInfoByLocalDrugId(saveDto.getLocalDrugId());
            // Update Record

            //Chris 20200109 For Supplement the VMP_ID from MOE_DRUG_STRENGTH table -Start
            if (saveDto.getVmpId() == null && StringUtils.isNotBlank(hkRegNo)) {
                List<MoeDrugStrengthPo> moeDrugStrengthPoList = moeDrugStrengthRepository.findByHkRegNo(hkRegNo);
                if (moeDrugStrengthPoList !=null && moeDrugStrengthPoList.size() > 0) {
                    Integer vmpId = moeDrugStrengthPoList.get(0).getVmpId() == null ? null : moeDrugStrengthPoList.get(0).getVmpId().intValue();
                    vmpId = vmpId == 0 ? null : vmpId;
                    //Supplement vmpId from moe_drug_strength
                    saveDto.setVmpId(vmpId);
                }
            }
            //Chris 20200109 For Supplement the VMP_ID from MOE_DRUG_STRENGTH table -End

            // Copy Create Info
            savePo.setCreateHosp(createInfo.getCreateHosp());
            savePo.setCreateDtm(createInfo.getCreateDtm());
            savePo.setCreateRank(createInfo.getCreateRank());
            savePo.setCreateRankDesc(createInfo.getCreateRankDesc());
            savePo.setCreateUser(createInfo.getCreateUser());
            savePo.setCreateUserId(createInfo.getCreateUserId());

            moeDrugStrengthLocalRepository.save(savePo);
            moeDrugStrengthLocalLogRepository.save(new MoeDrugStrengthLocalLogPo(ServerConstant.RECORD_UPDATE, savePo));
        } else {
            // New Record
            savePo.setCreateHosp(getUserDto().getHospitalCd());
            savePo.setCreateRank(getUserDto().getUserRankCd());
            savePo.setCreateRankDesc(getUserDto().getUserRankDesc());
            savePo.setCreateUser(getUserDto().getLoginName());
            savePo.setCreateUserId(getUserDto().getLoginId());
            savePo.setCreateDtm(now);

            moeDrugStrengthLocalRepository.save(savePo);
            moeDrugStrengthLocalLogRepository.save(new MoeDrugStrengthLocalLogPo(ServerConstant.RECORD_INSERT, savePo));
        }
    }

    private MoeDrugLocalPo upsertMoeDrugLocal(MoeDrugLocalDto saveDto) throws Exception {

        Date now = new Date();
        MoeDrugLocalPo savePo = mapper.map(saveDto, MoeDrugLocalPo.class);

        savePo.setUpdateHosp(getUserDto().getHospitalCd());
        savePo.setUpdateRank(getUserDto().getUserRankCd());
        savePo.setUpdateRankDesc(getUserDto().getUserRankDesc());
        savePo.setUpdateUser(getUserDto().getLoginName());
        savePo.setUpdateUserId(getUserDto().getLoginId());
        savePo.setUpdateDtm(now);

        if (moeDrugLocalRepository.existsById(saveDto.getLocalDrugId())) {
            // Update Record
            MoeDrugLocalDto dataDto = moeDrugLocalRepository.findCreateInfoByLocalDrugId(saveDto.getLocalDrugId());
            // Copy Create Info
            savePo.setCreateHosp(dataDto.getCreateHosp());
            savePo.setCreateDtm(now);
            savePo.setCreateRank(dataDto.getCreateRank());
            savePo.setCreateHosp(dataDto.getCreateHosp());
            savePo.setCreateRankDesc(dataDto.getCreateRankDesc());
            savePo.setCreateUser(dataDto.getCreateUser());
            savePo.setCreateUserId(dataDto.getCreateUserId());

            moeDrugLocalRepository.save(savePo);
            moeDrugLocalLogRepository.save(new MoeDrugLocalLogPo(ServerConstant.RECORD_UPDATE, savePo));
        } else {
            // New Record

            String hkRegNo = saveDto.getHkRegNo();
            if (StringUtils.isEmpty(hkRegNo)) {
                hkRegNo = UUID.randomUUID().toString();
                hkRegNo = StringUtils.remove(hkRegNo, "-");
                savePo.setHkRegNo(hkRegNo);
            }

            savePo.setCreateHosp(getUserDto().getHospitalCd());
            savePo.setCreateRank(getUserDto().getUserRankCd());
            savePo.setCreateRankDesc(getUserDto().getUserRankDesc());
            savePo.setCreateUser(getUserDto().getLoginName());
            savePo.setCreateUserId(getUserDto().getLoginId());
            savePo.setCreateDtm(now);

            moeDrugLocalRepository.save(savePo);
            moeDrugLocalLogRepository.save(new MoeDrugLocalLogPo(ServerConstant.RECORD_INSERT, savePo));
        }

        return savePo;
    }

    private void upsertMoeDrugAliasNameLocal(List<MoeDrugAliasNameLocalDto> saveDtoList, MoeDrugLocalPo moeDrugLocalPo) throws Exception {

        List<MoeDrugAliasNameLocalPo> oriList = moeDrugAliasNameLocalRepository.findByHkRegNo(moeDrugLocalPo.getHkRegNo());

        Comparator<MoeDrugAliasNameLocalPo> aliasNameIdASC = new Comparator<MoeDrugAliasNameLocalPo>() {
            @Override
            public int compare(MoeDrugAliasNameLocalPo arg0, MoeDrugAliasNameLocalPo arg1) {
                return arg0.getAliasNameId().compareTo(arg1.getAliasNameId());
            }
        };

        Comparator<MoeDrugAliasNameLocalDto> aliasNameIdASC_ = new Comparator<MoeDrugAliasNameLocalDto>() {
            @Override
            public int compare(MoeDrugAliasNameLocalDto arg0, MoeDrugAliasNameLocalDto arg1) {
                return arg0.getAliasNameId().compareTo(arg1.getAliasNameId());
            }
        };

        Collections.sort(saveDtoList, aliasNameIdASC_);
        Collections.sort(oriList, aliasNameIdASC);

        int start = 0;
        int size = oriList.size();
        MoeDrugAliasNameLocalPo savePo = null;
        List<MoeDrugAliasNameLocalPo> deletePos = new ArrayList<>();
        List<MoeDrugAliasNameLocalPo> upsertPos = new ArrayList<>();

        List<MoeDrugAliasNameLocalLogPo> logPos = new ArrayList<>();

        for (MoeDrugAliasNameLocalDto saveDto : saveDtoList) {
            savePo = mapper.map(saveDto, MoeDrugAliasNameLocalPo.class);

            // Trim To Null For aliasNameId
            savePo.setAliasNameId(StringUtils.trimToNull(savePo.getAliasNameId()));
            savePo.setHkRegNo(moeDrugLocalPo.getHkRegNo());

            // Init Create & Update info
            savePo.setCreateDtm(moeDrugLocalPo.getCreateDtm());
            savePo.setCreateHosp(moeDrugLocalPo.getCreateHosp());
            savePo.setCreateRank(moeDrugLocalPo.getCreateRank());
            savePo.setCreateRankDesc(moeDrugLocalPo.getCreateRankDesc());
            savePo.setCreateUser(moeDrugLocalPo.getCreateUser());
            savePo.setCreateUserId(moeDrugLocalPo.getCreateUserId());

            savePo.setUpdateDtm(moeDrugLocalPo.getUpdateDtm());
            savePo.setUpdateHosp(moeDrugLocalPo.getUpdateHosp());
            savePo.setUpdateRank(moeDrugLocalPo.getUpdateRank());
            savePo.setUpdateRankDesc(moeDrugLocalPo.getUpdateRankDesc());
            savePo.setUpdateUser(moeDrugLocalPo.getUpdateUser());
            savePo.setUpdateUserId(moeDrugLocalPo.getUpdateUserId());

            MoeDrugAliasNameLocalPo curRecord = null;
            Integer compare = null;
            for (int i = start; i < size; i++) {
                curRecord = oriList.get(i);
                compare = curRecord.getAliasNameId().compareTo(savePo.getAliasNameId());
                if (compare < 0) {
                    deletePos.add(curRecord);
                    logPos.add(new MoeDrugAliasNameLocalLogPo(ServerConstant.RECORD_DELETE, curRecord));
                    //moeDrugAliasNameLocalRepository.delete(curRecord);
                    //moeDrugAliasNameLocalLogRepository.save(new MoeDrugAliasNameLocalLogPo(ServerConstant.RECORD_DELETE, curRecord));
                } else if (compare == 0) {
                    start = i + 1;
                    break;
                } else if (compare > 0) {
                    start = i;
                    compare = null;
                    break;
                }
            }

            if (compare == null) {
                // New Record
                savePo.setAliasNameId(null);
                upsertPos.add(savePo);
                logPos.add(new MoeDrugAliasNameLocalLogPo(ServerConstant.RECORD_INSERT, savePo));
                moeDrugAliasNameLocalRepository.save(savePo);
                moeDrugAliasNameLocalLogRepository.save(new MoeDrugAliasNameLocalLogPo(ServerConstant.RECORD_INSERT, savePo));
            } else if (compare == 0) {
                upsertPos.add(savePo);
                logPos.add(new MoeDrugAliasNameLocalLogPo(ServerConstant.RECORD_UPDATE, savePo));
                moeDrugAliasNameLocalRepository.save(savePo);
                moeDrugAliasNameLocalLogRepository.save(new MoeDrugAliasNameLocalLogPo(ServerConstant.RECORD_UPDATE, savePo));
            }
        }

        if (start < size) {
            for (int i = start; i < size; i++) {
                deletePos.add(oriList.get(i));
                logPos.add(new MoeDrugAliasNameLocalLogPo(ServerConstant.RECORD_DELETE, oriList.get(i)));
                moeDrugAliasNameLocalRepository.delete(oriList.get(i));
                moeDrugAliasNameLocalLogRepository.save(new MoeDrugAliasNameLocalLogPo(ServerConstant.RECORD_DELETE, oriList.get(i)));
            }
        }
    }

    private void upsertMoeDrugOrdPropertyLocal(MoeDrugOrdPropertyLocalDto saveDto, MoeDrugLocalPo moeDrugLocalPo, List<InnerRelatedDrugDosageLocalDrug> relatedDrugDosageLocalDrugs) throws Exception {

        Date now = new Date();
        MoeDrugOrdPropertyLocalPo savePo = mapper.map(saveDto, MoeDrugOrdPropertyLocalPo.class);

        // Init Update Info
        savePo.setUpdateHosp(getUserDto().getHospitalCd());
        savePo.setUpdateRank(getUserDto().getUserRankCd());
        savePo.setUpdateRankDesc(getUserDto().getUserRankDesc());
        savePo.setUpdateUser(getUserDto().getLoginName());
        savePo.setUpdateUserId(getUserDto().getLoginId());
        savePo.setUpdateDtm(now);

        if (moeDrugOrdPropertyLocalRepository.existsById(saveDto.getLocalDrugId())) {
            MoeDrugOrdPropertyLocalDto createInfo = moeDrugOrdPropertyLocalRepository.findCreateInfoAndMaximumDurationByLocalDrugId(saveDto.getLocalDrugId());

            // Copy Create Info
            savePo.setCreateHosp(createInfo.getCreateHosp());
            savePo.setCreateDtm(now);
            savePo.setCreateRank(createInfo.getCreateRank());
            savePo.setCreateRankDesc(createInfo.getCreateRankDesc());
            savePo.setCreateUser(createInfo.getCreateUser());
            savePo.setCreateUserId(createInfo.getCreateUserId());

            moeDrugOrdPropertyLocalRepository.save(savePo);
            moeDrugOrdPropertyLocLogRepository.saveAndFlush(new MoeDrugOrdPropertyLocLogPo(ServerConstant.RECORD_UPDATE, savePo));

            Long oriMaxDuration = createInfo.getMaximumDuration();
            oriMaxDuration = oriMaxDuration == null ? 0 : oriMaxDuration;
            Long newMaxDuration = savePo.getMaximumDuration();
            newMaxDuration = newMaxDuration == null ? 0 : newMaxDuration;
            if (oriMaxDuration.compareTo(newMaxDuration) != 0L) {
                considerUpdateOtherRecords(savePo, moeDrugLocalPo);
            }

        } else {

            // Init Create Info
            savePo.setCreateHosp(getUserDto().getHospitalCd());
            savePo.setCreateRank(getUserDto().getUserRankCd());
            savePo.setCreateRankDesc(getUserDto().getUserRankDesc());
            savePo.setCreateUser(getUserDto().getLoginName());
            savePo.setCreateUserId(getUserDto().getLoginId());
            savePo.setCreateDtm(now);

            moeDrugOrdPropertyLocalRepository.saveAndFlush(savePo);
            moeDrugOrdPropertyLocLogRepository.saveAndFlush(new MoeDrugOrdPropertyLocLogPo(ServerConstant.RECORD_INSERT, savePo));
        }

        // Update maximum duration for related drugs
        Map<String, Long> versionMap = new HashMap<>();
        if (relatedDrugDosageLocalDrugs != null && relatedDrugDosageLocalDrugs.size() > 0) {
            List<String> relatedDrugDosageLocalDrugKeys = new ArrayList<>();
            for (InnerRelatedDrugDosageLocalDrug relatedDrugDosageLocalDrug : relatedDrugDosageLocalDrugs) {
                if (StringUtils.isNotBlank(relatedDrugDosageLocalDrug.getLocalDrugId())) {
                    relatedDrugDosageLocalDrugKeys.add(relatedDrugDosageLocalDrug.getLocalDrugId());
                    versionMap.put(relatedDrugDosageLocalDrug.getLocalDrugId(), relatedDrugDosageLocalDrug.getVersion());
                }
            }
            List<MoeDrugOrdPropertyLocalPo> relatedDrugOrdPropertyLocalPos = moeDrugOrdPropertyLocalRepository.listLocalDrugByLocalDrugIds(relatedDrugDosageLocalDrugKeys);
            for (MoeDrugOrdPropertyLocalPo oriRelatedPo : relatedDrugOrdPropertyLocalPos) {
                // Update Record
                MoeDrugOrdPropertyLocalDto saveRelatedDto = mapper.map(oriRelatedPo, MoeDrugOrdPropertyLocalDto.class);
                //saveRelatedDto.setVersion(versionMap.get(saveRelatedDto.getLocalDrugId()));   //TODO

                saveRelatedDto.setMaximumDuration(savePo.getMaximumDuration());
                saveRelatedDto.setUpdateUserId(savePo.getUpdateUserId());
                saveRelatedDto.setUpdateUser(savePo.getUpdateUser());
                saveRelatedDto.setUpdateRank(savePo.getUpdateRank());
                saveRelatedDto.setUpdateRankDesc(savePo.getUpdateRankDesc());
                saveRelatedDto.setUpdateHosp(savePo.getUpdateHosp());
                saveRelatedDto.setUpdateDtm(now);
                saveRelatedDto.setTradeNameAlias(savePo.getTradeNameAlias() != null ? savePo.getTradeNameAlias().trim() : null);
                saveRelatedDto.setSpecifyQuantityFlag(savePo.getSpecifyQuantityFlag());
                saveRelatedDto.setDrugCategoryId(savePo.getDrugCategoryId());

                MoeDrugOrdPropertyLocalPo saveRelatedPo = mapper.map(saveRelatedDto, MoeDrugOrdPropertyLocalPo.class);
                moeDrugOrdPropertyLocalRepository.save(saveRelatedPo);
                moeDrugOrdPropertyLocLogRepository.save(new MoeDrugOrdPropertyLocLogPo(ServerConstant.RECORD_UPDATE, saveRelatedPo));
            }

        }
    }

    private void considerUpdateOtherRecords(MoeDrugOrdPropertyLocalPo moeDrugOrdPropertyLocalPo, MoeDrugLocalPo moeDrugLocalPo) {

        Date now = new Date();
        boolean isNeedUpdateOthers = MoeCommonHelper.isFlagFalse(moeDrugLocalPo.getStrengthCompulsory());
        if (isNeedUpdateOthers) {
            String tradeNameValue = moeDrugLocalPo.getTradeName() == null ? "#" : moeDrugLocalPo.getTradeName();
            String vtmValue = moeDrugLocalPo.getVtm() == null ? "#" : moeDrugLocalPo.getVtm();
            Long routeIdValue = moeDrugLocalPo.getRouteId() == null ? 0 : moeDrugLocalPo.getRouteId();
            Long formIdValue = moeDrugLocalPo.getFormId() == null ? 0 : moeDrugLocalPo.getFormId();
            String doseFormExtraInfoValue = moeDrugLocalPo.getDoseFormExtraInfo() == null ? "#" : moeDrugLocalPo.getDoseFormExtraInfo();

            List<MoeDrugOrdPropertyLocalPo> moeDrugOrdPropertyLocalList = moeDrugOrdPropertyLocalRepository.getRelatedDrugOrdPropertyLocal(tradeNameValue,
                    vtmValue,
                    routeIdValue,
                    formIdValue,
                    doseFormExtraInfoValue);
            boolean isMoeDrugOrdPropertyLocalListValid = moeDrugOrdPropertyLocalList != null && !moeDrugOrdPropertyLocalList.isEmpty();
            if (isMoeDrugOrdPropertyLocalListValid) {
                for (MoeDrugOrdPropertyLocalPo record : moeDrugOrdPropertyLocalList) {
                    if (!record.getLocalDrugId().equalsIgnoreCase(moeDrugOrdPropertyLocalPo.getLocalDrugId())) {
                        record.setMaximumDuration(moeDrugOrdPropertyLocalPo.getMaximumDuration());
                        record.setUpdateUserId(moeDrugOrdPropertyLocalPo.getUpdateUserId());
                        record.setUpdateUser(moeDrugOrdPropertyLocalPo.getUpdateUser());
                        record.setUpdateRank(moeDrugOrdPropertyLocalPo.getUpdateRank());
                        record.setUpdateRankDesc(moeDrugOrdPropertyLocalPo.getUpdateRankDesc());
                        record.setUpdateHosp(moeDrugOrdPropertyLocalPo.getUpdateHosp());
                        record.setUpdateDtm(now);

                        moeDrugOrdPropertyLocalRepository.save(record);
                        moeDrugOrdPropertyLocLogRepository.save(new MoeDrugOrdPropertyLocLogPo(ServerConstant.RECORD_UPDATE, record));
                    }
                }
            }
        }
    }

    private void updateBaseUnit(MoeDrugLocalDto dataDto, List<InnerRelatedDrugDosageLocalDrug> relatedDrugDosageLocalDrugs) throws Exception {

        Date now = new Date();
        Optional<MoeDrugLocalPo> optionalMoeDrugLocalPo = moeDrugLocalRepository.findById(dataDto.getLocalDrugId());
        if (optionalMoeDrugLocalPo.isPresent()) {
            MoeDrugLocalPo oriPo = optionalMoeDrugLocalPo.get();
            MoeDrugLocalDto saveDto = mapper.map(oriPo, MoeDrugLocalDto.class);

            // Set Update Data
            saveDto.setBaseUnit(dataDto.getBaseUnit());
            saveDto.setBaseUnitId(dataDto.getBaseUnitId());

            // Set Vesion
            saveDto.setVersion(dataDto.getVersion());

            // Init Update Info
            saveDto.setUpdateHosp(getUserDto().getHospitalCd());
            saveDto.setUpdateRank(getUserDto().getUserRankCd());
            saveDto.setUpdateRankDesc(getUserDto().getUserRankDesc());
            saveDto.setUpdateUser(getUserDto().getLoginName());
            saveDto.setUpdateUserId(getUserDto().getLoginId());
            saveDto.setUpdateDtm(now);

            MoeDrugLocalPo savePo = mapper.map(saveDto, MoeDrugLocalPo.class);
            moeDrugLocalRepository.save(savePo);
            moeDrugLocalLogRepository.save(new MoeDrugLocalLogPo(ServerConstant.RECORD_UPDATE, savePo));
        }
        // Ricci 20191028 end --


        // update base unit of related drugs
        for (InnerRelatedDrugDosageLocalDrug relatedDrugDosageLocalDrug : relatedDrugDosageLocalDrugs) {
            String localDrugId = relatedDrugDosageLocalDrug.getLocalDrugId();
            if (StringUtils.isNotBlank(localDrugId)) {
                Optional<MoeDrugLocalPo> optionalMoeDrugLocalRelatedPo = moeDrugLocalRepository.findById(localDrugId);
                if (optionalMoeDrugLocalRelatedPo.isPresent()) {
                    MoeDrugLocalPo oriRelatedPo = optionalMoeDrugLocalRelatedPo.get();
                    MoeDrugLocalDto saveRelatedDto = mapper.map(oriRelatedPo, MoeDrugLocalDto.class);

                    saveRelatedDto.setBaseUnit(dataDto.getBaseUnit());
                    saveRelatedDto.setBaseUnitId(dataDto.getBaseUnitId());
                    saveRelatedDto.setVersion(relatedDrugDosageLocalDrug.getVersion());
                    saveRelatedDto.setUpdateHosp(getUserDto().getHospitalCd());
                    saveRelatedDto.setUpdateRank(getUserDto().getUserRankCd());
                    saveRelatedDto.setUpdateRankDesc(getUserDto().getUserRankDesc());
                    saveRelatedDto.setUpdateUser(getUserDto().getLoginName());
                    saveRelatedDto.setUpdateUserId(getUserDto().getLoginId());
                    saveRelatedDto.setUpdateDtm(now);

                    MoeDrugLocalPo saveRelatedPo = mapper.map(saveRelatedDto, MoeDrugLocalPo.class);
                    moeDrugLocalRepository.save(saveRelatedPo);
                    moeDrugLocalLogRepository.saveAndFlush(new MoeDrugLocalLogPo(ServerConstant.RECORD_UPDATE, saveRelatedPo));
                } else {
                    //eric 20191223 start--
//                    throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.error"));
                    throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),
                            ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage());
                    //eric 20191223 end--
                }
            }
        }
    }

/*    private void updateBaseUnit(MoeDrugLocalPo local) {
        String sql = "update Moe_Drug_Local set base_Unit_Id=:baseUnitId, base_Unit=:baseUnit where local_Drug_Id=:localDrugId";
        Map<String, Object> paraMap = new HashMap<>();
        paraMap.put("baseUnitId", local.getBaseUnitId());
        paraMap.put("baseUnit", local.getBaseUnit());
        paraMap.put("localDrugId", local.getLocalDrugId());
        namedParameterJdbcTemplate.update(sql, paraMap);
    }*/


    // Simon 20191014 start--

    /**
     * PURPOSE         : Delete Drug Information
     *
     * @param localDrugId the id to be deleted
     * @return boolean true when delete success
     */
    @Override
    public boolean deleteDrug(String localDrugId) throws MoeServiceException {

        // get and delete Moe Drug Local
        MoeDrugLocalPo moeDrugLocal = null;
        Optional<MoeDrugLocalPo> byId = moeDrugLocalRepository.findById(localDrugId);
        if (byId.isPresent())
            moeDrugLocal = byId.get();
        if (moeDrugLocal == null) {
            return false;
        }
        String hkRegNo = moeDrugLocal.getHkRegNo();

        // get and delete Strength
        MoeDrugStrengthLocalPo moeDrugStrength = null;
        Optional<MoeDrugStrengthLocalPo> byId1 = moeDrugStrengthLocalRepository.findById(localDrugId);
        if (byId1.isPresent()) {
            moeDrugStrength = byId1.get();
        }
        if (moeDrugStrength != null) {
            moeDrugStrengthLocalRepository.delete(moeDrugStrength);
            moeDrugStrengthLocalLogRepository.save(new MoeDrugStrengthLocalLogPo(ServerConstant.RECORD_DELETE, moeDrugStrength));
        }

        // get and delete Alias Names
        List<MoeDrugAliasNameLocalPo> moeDrugAliasNameList = moeDrugAliasNameLocalRepository.findByHkRegNo(hkRegNo);
        if (moeDrugAliasNameList != null && moeDrugAliasNameList.size() > 0) {
            for (MoeDrugAliasNameLocalPo po : moeDrugAliasNameList) {
                moeDrugAliasNameLocalRepository.delete(po);
                moeDrugAliasNameLocalLogRepository.save(new MoeDrugAliasNameLocalLogPo(ServerConstant.RECORD_DELETE, po));
            }
        }
        // get and delete Order Property
        MoeDrugOrdPropertyLocalPo moeDrugOrderProperty = null;
        Optional<MoeDrugOrdPropertyLocalPo> byId4 = moeDrugOrdPropertyLocalRepository.findById(localDrugId);
        if (byId4.isPresent()) {
            moeDrugOrderProperty = byId4.get();
        }
        moeDrugOrdPropertyLocalRepository.delete(moeDrugOrderProperty);
        moeDrugOrdPropertyLocLogRepository.save(new MoeDrugOrdPropertyLocLogPo(ServerConstant.RECORD_DELETE, moeDrugOrderProperty));

        moeDrugLocalRepository.delete(moeDrugLocal);
        moeDrugLocalLogRepository.saveAndFlush(new MoeDrugLocalLogPo(ServerConstant.RECORD_DELETE, moeDrugLocal));

        return true;
    }

    @Override
    public boolean deleteDrugs(String localDrugId) throws Exception {
        saDrugLocalFacade.deleteDrugDetail(localDrugId);
        moeDrugToSaamFacade.deleteDrugDetail(localDrugId);
        //localDrugId not found
        if (!deleteDrug(localDrugId)) {
            //eric 20191223 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("records.notExisted"));
            throw new MoeServiceException(ResponseCode.SpecialMessage.RECORDS_NOT_EXISTED.getResponseCode(),ResponseCode.SpecialMessage.RECORDS_NOT_EXISTED.getResponseMessage());
            //eric 20191223 end--
        }
        moeCommonDosageFacade.deleteMoeCommonDosageByLocalDrugId(localDrugId);
        moeDrugBySpecialtyFacade.deleteDrugBySpecialty(localDrugId);
        moeDrugTherapeuticLocalFacade.deleteDrugByTherapeuticLocal(localDrugId);
        return true;
    }
    //Simon 20191014 end--


}
