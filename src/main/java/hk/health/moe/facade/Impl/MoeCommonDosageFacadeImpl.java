package hk.health.moe.facade.Impl;

import hk.health.moe.common.MoeCommonHelper;
import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.ParamException;
import hk.health.moe.exception.RecordException;
import hk.health.moe.facade.MoeCommonDosageFacade;
import hk.health.moe.pojo.dto.DrugInfoDto;
import hk.health.moe.pojo.dto.FieldErrorDto;
import hk.health.moe.pojo.dto.MoeCommonDosageDto;
import hk.health.moe.pojo.dto.inner.InnerRelatedDrugDosageLocalDrug;
import hk.health.moe.pojo.dto.inner.InnerSaveCommonDosageDto;
import hk.health.moe.pojo.po.MoeCommonDosageLogPo;
import hk.health.moe.pojo.po.MoeCommonDosagePo;
import hk.health.moe.pojo.po.MoeCommonDosageTypePo;
import hk.health.moe.pojo.po.MoeDrugLocalPo;
import hk.health.moe.pojo.po.MoeFreqPo;
import hk.health.moe.pojo.po.MoeRegimenPo;
import hk.health.moe.pojo.po.MoeRoutePo;
import hk.health.moe.repository.MoeCommonDosageLogRepository;
import hk.health.moe.repository.MoeCommonDosageRepository;
import hk.health.moe.repository.MoeCommonDosageTypeRepository;
import hk.health.moe.repository.MoeDrugLocalRepository;
import hk.health.moe.repository.MoeFreqRepository;
import hk.health.moe.repository.MoeRegimenRepository;
import hk.health.moe.repository.MoeRouteRepository;
import hk.health.moe.service.impl.BaseServiceImpl;
import hk.health.moe.util.DtoUtil;
import hk.health.moe.util.Formatter;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.ValidateUtil;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class MoeCommonDosageFacadeImpl extends BaseServiceImpl implements MoeCommonDosageFacade {

    @Autowired
    private MoeCommonDosageRepository moeCommonDosageRepository;

    @Autowired
    private MoeCommonDosageLogRepository moeCommonDosageLogRepository;

    @Autowired
    private MoeRegimenRepository moeRegimenRepository;

    @Autowired
    private MoeFreqRepository moeFreqRepository;

    @Autowired
    private MoeRouteRepository moeRouteRepository;

    @Autowired
    private MoeCommonDosageTypeRepository moeCommonDosageTypeRepository;

    @Autowired
    private MoeDrugLocalRepository moeDrugLocalRepository;

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    @Autowired
    private ValidateUtil validateUtil;

    @Autowired
    private DozerBeanMapper mapper;

    @Override
    public void saveCommonDosage(List<InnerSaveCommonDosageDto> commonDosageDtos, List<InnerRelatedDrugDosageLocalDrug> relatedDrugDosageLocalDrugs, boolean isNewDrug, MoeDrugLocalPo dataPo) throws Exception {

        String localDrugId = dataPo.getLocalDrugId();
        String hkRegNo = dataPo.getHkRegNo();

        List<MoeCommonDosagePo> insertMoeCommonDosages = new ArrayList<>();
        List<MoeCommonDosagePo> deleteMoeCommonDosages = new ArrayList<>();
        List<MoeCommonDosagePo> updateMoeCommonDosages = new ArrayList<>();

        List<Long> deleteMoeCommonDosagesIds = new ArrayList<>();
        for (InnerSaveCommonDosageDto dto : commonDosageDtos) {
            if (ServerConstant.RECORD_DELETE.equalsIgnoreCase(dto.getActionType())) {
                deleteMoeCommonDosagesIds.add(dto.getMoeCommonDosage().getCommonDosageId());
            }
        }
        if (deleteMoeCommonDosagesIds.size() == 0) {
            deleteMoeCommonDosagesIds.add(0L);
        }

        for (InnerSaveCommonDosageDto dto : commonDosageDtos) {
            MoeCommonDosageDto moeCommonDosageDto = dto.getMoeCommonDosage();

            Long commonDosageId = moeCommonDosageDto.getCommonDosageId() == null ? 0L : moeCommonDosageDto.getCommonDosageId();
            // filter delete record first
            if (ServerConstant.RECORD_DELETE.equalsIgnoreCase(dto.getActionType())) {
                List<MoeCommonDosagePo> moeCommonDosageList = deleteCommonDosage(moeCommonDosageDto);
                deleteMoeCommonDosages.addAll(moeCommonDosageList);
            } else {
                checkCommonDosageDuplicate(commonDosageDtos, dto);
                if (commonDosageId == 0L) {
                    // new record
                    List<MoeCommonDosagePo> moeCommonDosageList = addCommonDosage(moeCommonDosageDto, localDrugId, hkRegNo, deleteMoeCommonDosagesIds);
                    insertMoeCommonDosages.addAll(moeCommonDosageList);
                } else {
                    // update record
                    List<MoeCommonDosagePo> moeCommonDosageList = updateCommonDosage(moeCommonDosageDto);
                    updateMoeCommonDosages.addAll(moeCommonDosageList);
                }
            }
        }


        // Delete related drug for add new HKCTT common dosage
        if (relatedDrugDosageLocalDrugs != null && relatedDrugDosageLocalDrugs.size() != 0 && isNewDrug) {
            List<String> relatedDrugDosageLocalDrugIds = new ArrayList<>();
            for (InnerRelatedDrugDosageLocalDrug relatedDrugDosageLocalDrug : relatedDrugDosageLocalDrugs) {
                relatedDrugDosageLocalDrugIds.add(relatedDrugDosageLocalDrug.getLocalDrugId());
            }

            List<MoeCommonDosagePo> deleteCommonDosageList = moeCommonDosageRepository.listByLocalDrugIds(relatedDrugDosageLocalDrugIds);
            deleteMoeCommonDosages.addAll(deleteCommonDosageList);
        }

        if (insertMoeCommonDosages != null) {
            for (MoeCommonDosagePo moeCommonDosage : insertMoeCommonDosages) {
                moeCommonDosageRepository.save(moeCommonDosage);
                MoeCommonDosageLogPo moeCommonDosageLog = new MoeCommonDosageLogPo(ServerConstant.RECORD_INSERT, moeCommonDosage);
                moeCommonDosageLogRepository.save(moeCommonDosageLog);
            }
        }

        if (deleteMoeCommonDosages != null) {
            for (MoeCommonDosagePo moeCommonDosage : deleteMoeCommonDosages) {
                moeCommonDosageRepository.delete(moeCommonDosage);
                MoeCommonDosageLogPo moeCommonDosageLog = new MoeCommonDosageLogPo(ServerConstant.RECORD_DELETE, moeCommonDosage);
                moeCommonDosageLogRepository.save(moeCommonDosageLog);
            }
        }

        if (updateMoeCommonDosages != null) {
            for (MoeCommonDosagePo moeCommonDosage : updateMoeCommonDosages) {
                moeCommonDosageRepository.save(Formatter.emptyToNull(moeCommonDosage));
                MoeCommonDosageLogPo moeCommonDosageLog = new MoeCommonDosageLogPo(ServerConstant.RECORD_UPDATE, moeCommonDosage);
                moeCommonDosageLogRepository.save(moeCommonDosageLog);
            }
        }
    }

    public List<MoeCommonDosagePo> addCommonDosage(MoeCommonDosageDto dtoRecord, String localDrugId, String hkRegNo, List<Long> deleteMoeCommonDosagesIds) throws Exception {

        BigDecimal dosageValue = dtoRecord.getDosage() == null ? new BigDecimal(0) : dtoRecord.getDosage();
        String dosageUnitValue = StringUtils.isEmpty(dtoRecord.getDosageUnit()) ? "#" : dtoRecord.getDosageUnit();
        String localDrugIdValue = StringUtils.isEmpty(dtoRecord.getLocalDrugId()) ? "#" : dtoRecord.getLocalDrugId();
        Long freq1 = dtoRecord.getFreq1() != null ? dtoRecord.getFreq1() : 0L;

        List<MoeCommonDosagePo> result = new ArrayList<>();

        Long isExistence = moeCommonDosageRepository.checkExistence(dtoRecord.getSuspend(),
                dtoRecord.getStrCommonDosageType(),
                dosageValue,
                dosageUnitValue,
                dtoRecord.getFreqId(),
                dtoRecord.getPrn(),
                dtoRecord.getHkRegNo(),
                localDrugIdValue,
                dtoRecord.getRouteEng(),
                freq1,
                deleteMoeCommonDosagesIds
        );

        if (isExistence > 0) {
            //eric 20191223 start--
//            throw new ParamException(localeMessageSourceUtil.getMessage("records.alreadyExist"));
            throw new ParamException(ResponseCode.SpecialMessage.RECORDS_ALREADY_EXIST.getResponseCode(),ResponseCode.SpecialMessage.RECORDS_ALREADY_EXIST.getResponseMessage());
            //eric 20191223 end--
        }
        /*if (list != null && (!list.isEmpty())) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i) != null
                        && dtoRecord.getFreq1() != null
                        && list.get(i).longValue() != dtoRecord.getFreq1().longValue()) {
                    continue;
                }

                throw new ParamException(localeMessageSourceUtil.getMessage("records.alreadyExist"));
            }
        }*/

        // prepare new record
        MoeCommonDosagePo commonDosagePo = new MoeCommonDosagePo();
        buildCommonDosage(commonDosagePo, dtoRecord);

        Date d = new Date();
        //commonDosagePo.setHkRegNo(hkRegNo);
        //commonDosagePo.setLocalDrugId(localDrugId);
        commonDosagePo.setDosage(dtoRecord.getDosage());
        commonDosagePo.setDosageUnitId(dtoRecord.getDosageUnitId());
        commonDosagePo.setDosageUnit(dtoRecord.getDosageUnit());
        commonDosagePo.setFreq1(dtoRecord.getFreq1());
        commonDosagePo.setPrn(dtoRecord.getPrn());
        commonDosagePo.setSupplFreqId(0L);
        commonDosagePo.setSupplFreqEng("");
        commonDosagePo.setSupplFreq1(0L);
        commonDosagePo.setSupplFreq2(0L);
        commonDosagePo.setOwner(getUserDto().getLoginId());
        commonDosagePo.setSuspend(dtoRecord.getSuspend());
        commonDosagePo.setCreateUserId(getUserDto().getLoginId());
        commonDosagePo.setCreateUser(getUserDto().getLoginName());
        commonDosagePo.setCreateHosp(getUserDto().getHospitalCd());
        commonDosagePo.setCreateRank(getUserDto().getUserRankCd());
        commonDosagePo.setCreateRankDesc(getUserDto().getUserRankDesc());
        commonDosagePo.setCreateDtm(d);
        commonDosagePo.setUpdateUserId(getUserDto().getLoginId());
        commonDosagePo.setUpdateUser(getUserDto().getLoginName());
        commonDosagePo.setUpdateHosp(getUserDto().getHospitalCd());
        commonDosagePo.setUpdateRank(getUserDto().getUserRankCd());
        commonDosagePo.setUpdateRankDesc(getUserDto().getUserRankDesc());
        commonDosagePo.setUpdateDtm(d);

        List<DrugInfoDto> localDrugIdList = getSameTypeLocalDrugIdList(localDrugId, hkRegNo);

        // prepare return result
        if (!localDrugIdList.isEmpty()) {
            for (DrugInfoDto record : localDrugIdList) {
                commonDosagePo.setLocalDrugId(record.getLocalDrugId());
                commonDosagePo.setHkRegNo(record.getHkRegNo());
                result.add(Formatter.emptyToNull(new MoeCommonDosagePo(commonDosagePo)));
            }
        }

        return result;
    }

    public List<MoeCommonDosagePo> deleteCommonDosage(MoeCommonDosageDto moeCommonDosageDto) {

        List<MoeCommonDosagePo> result = new ArrayList<MoeCommonDosagePo>();

        MoeCommonDosagePo oriPo = null;
        Optional<MoeCommonDosagePo> optionalMoeCommonDosagePo = moeCommonDosageRepository.findById(moeCommonDosageDto.getCommonDosageId());
        if (optionalMoeCommonDosagePo.isPresent()) {
            oriPo = optionalMoeCommonDosagePo.get();
            List<DrugInfoDto> drugInfoDtoList = getSameTypeLocalDrugIdList(oriPo.getLocalDrugId(), oriPo.getHkRegNo());
            if (drugInfoDtoList != null && drugInfoDtoList.size() > 0) {
                List<String> localDrugIdList = new ArrayList<String>();
                for (DrugInfoDto record : drugInfoDtoList) {
                    localDrugIdList.add(record.getLocalDrugId());
                }

                String dosageType = oriPo.getMoeCommonDosageType().getCommonDosageType();
                BigDecimal dosage = oriPo.getDosage() != null ? oriPo.getDosage() : new BigDecimal(0);
                Long freqId = oriPo.getMoeFreq().getFreqId();
                String prn = oriPo.getPrn();
                Long freq1 = oriPo.getFreq1() != null ? oriPo.getFreq1() : 0L;
                String routeEng = oriPo.getRouteEng();
                result = moeCommonDosageRepository.findMoeCommonDosageWithSameMoeDrug(localDrugIdList, dosageType, dosage, freqId, prn, freq1, routeEng);
            }
        }

        return result;
    }

    public List<MoeCommonDosagePo> updateCommonDosage(MoeCommonDosageDto dataDto) throws Exception {


        List<MoeCommonDosagePo> result = new ArrayList<>();

        Optional<MoeCommonDosagePo> optionalMoeCommonDosagePo = moeCommonDosageRepository.findById(dataDto.getCommonDosageId());
        if (optionalMoeCommonDosagePo.isPresent()) {
            MoeCommonDosagePo dataPo = optionalMoeCommonDosagePo.get();

            List<DrugInfoDto> drugInfoDTOList = getSameTypeLocalDrugIdList(dataPo.getLocalDrugId(), dataPo.getHkRegNo());
            List<String> localDrugIdList = new ArrayList<>();
            for (DrugInfoDto record : drugInfoDTOList) {
                localDrugIdList.add(record.getLocalDrugId());
            }

            String dosageType = dataPo.getMoeCommonDosageType().getCommonDosageType();
            BigDecimal dosage = dataPo.getDosage() != null ? dataPo.getDosage() : new BigDecimal(0);
            Long freqId = dataPo.getMoeFreq().getFreqId();
            String prn = dataPo.getPrn();
            Long freq1 = dataPo.getFreq1() != null ? dataPo.getFreq1() : 0L;
            String routeEng = StringUtils.isNotBlank(dataPo.getRouteEng()) ? dataPo.getRouteEng() : "#";

            List<MoeCommonDosagePo> moeCommonDosageList = moeCommonDosageRepository.findMoeCommonDosageWithSameMoeDrug(localDrugIdList, dosageType, dosage, freqId, prn, freq1, routeEng);
            for (MoeCommonDosagePo oriPo : moeCommonDosageList) {
                MoeCommonDosageDto saveDto = mapper.map(oriPo, MoeCommonDosageDto.class);

                if (dataDto.getCommonDosageId().equals(oriPo.getCommonDosageId())) {
                    // Handle current record data
                    saveDto.setVersion(dataDto.getVersion());
                }
                MoeCommonDosagePo savePo = mapper.map(saveDto, MoeCommonDosagePo.class);

                // Set Data
                buildCommonDosage(savePo, dataDto);
                savePo.setSuspend(dataDto.getSuspend());
                savePo.setDosage(dataDto.getDosage());
                savePo.setDosageUnit(dataDto.getDosageUnit());
                savePo.setDosageUnitId(dataDto.getDosageUnitId());
                savePo.setFreq1(dataDto.getFreq1());
                savePo.setPrn(dataDto.getPrn());

                // Init Update Info
                savePo.setUpdateUserId(getUserDto().getLoginId());
                savePo.setUpdateUser(getUserDto().getLoginName());
                savePo.setUpdateHosp(getUserDto().getHospitalCd());
                savePo.setUpdateRank(getUserDto().getUserRankCd());
                savePo.setUpdateRankDesc(getUserDto().getUserRankDesc());

                result.add(Formatter.emptyToNull(savePo));
            }
        }

        return result;
    }

    private List<DrugInfoDto> getSameTypeLocalDrugIdList(String localDrugId, String hkRegNo) {

        List<DrugInfoDto> localDrugIdList = new ArrayList<DrugInfoDto>();
        String tradeName = null;
        String vtm = null;
        Long routeId = 0L;
        Long formId = 0L;
        Long doseFormExtraInfoId = 0L;

        if (StringUtils.isBlank(localDrugId)) {
            return localDrugIdList;
        }

        MoeDrugLocalPo moeDrugLocal = null;
        Optional<MoeDrugLocalPo> optionalMoeDrugLocalPo = moeDrugLocalRepository.findById(localDrugId);
        if (optionalMoeDrugLocalPo.isPresent()) {
            moeDrugLocal = optionalMoeDrugLocalPo.get();
            String strengthCompulsory = moeDrugLocal.getStrengthCompulsory();

            // if strenghtCompulsory <> 'N', no need to update other drug
            if (!MoeCommonHelper.isFlagFalse(strengthCompulsory)) {
                localDrugIdList.add(new DrugInfoDto(localDrugId, hkRegNo));
                return localDrugIdList;
            }
            tradeName = moeDrugLocal.getTradeName();
            vtm = moeDrugLocal.getVtm();
            routeId = moeDrugLocal.getRouteId();
            formId = moeDrugLocal.getFormId();
            doseFormExtraInfoId = moeDrugLocal.getDoseFormExtraInfoId();

            String tradeNameValue = StringUtils.isEmpty(tradeName) ? "#" : tradeName;
            String vtmValue = StringUtils.isEmpty(vtm) ? "#" : vtm;
            Long routeIdValue = routeId == null ? 0L : routeId;
            Long formIdValue = formId == null ? 0L : formId;
            Long doseFormExtraInfoIdValue = doseFormExtraInfoId == null ? 0 : doseFormExtraInfoId;

            return moeDrugLocalRepository.findSameTypeLocalDrugId(tradeNameValue, vtmValue, routeIdValue, formIdValue, doseFormExtraInfoIdValue);
        } else {
            return localDrugIdList;
        }
    }

    public List<MoeCommonDosagePo> getCommonDosage(boolean isNewADrug, String localDrugId, String hkRegNo) {

        boolean localDrugIdValid = StringUtils.isNotEmpty(localDrugId);
        boolean hkRegNoValid = StringUtils.isNotEmpty(hkRegNo);

        if (!localDrugIdValid) {
            return null;
        }

        List<MoeCommonDosagePo> returnRecords = new ArrayList<>();

        if (isNewADrug && hkRegNoValid) {
            int relatedDrugExist = moeDrugLocalRepository.checkRelatedDrugExist(hkRegNo, localDrugId);
            if (relatedDrugExist > 0) {
                returnRecords = moeCommonDosageRepository.findAllRelatedCommonDosageByHkRegNo(hkRegNo, localDrugId);
            } else {
                returnRecords = moeCommonDosageRepository.findByHkRegNo(hkRegNo);
            }
        } else if (isNewADrug && !hkRegNoValid) {
            //--- do nothing ---
        } else {
            returnRecords = moeCommonDosageRepository.findAllByLocalDrugId(localDrugId);
        }

        return returnRecords;
    }

    private MoeCommonDosagePo buildCommonDosage(MoeCommonDosagePo commonDosagePo, MoeCommonDosageDto dtoRecord) throws Exception {

        List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();

        if (StringUtils.isBlank(dtoRecord.getPrn())) {
            //eric 20191219 start--
//            throw new ParamException(localeMessageSourceUtil.getMessage("param.wrongInputSubmission"), true,
//                    validateUtil.buildFieldErrorList("Prn", ServerConstant.FIELD_ERROR_NOT_NULL, fieldErrorDtos));
            throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage(),
                    true, validateUtil.buildFieldErrorList("Prn", ServerConstant.FIELD_ERROR_NOT_NULL, fieldErrorDtos));
            //eric 20191219 end--
        }

        if (StringUtils.isBlank(dtoRecord.getSuspend())) {
            //eric 20191219 start--
//            throw new ParamException(localeMessageSourceUtil.getMessage("param.wrongInputSubmission"), true,
//                    validateUtil.buildFieldErrorList("Suspend", ServerConstant.FIELD_ERROR_NOT_NULL, fieldErrorDtos));
            throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage(),
                    true,validateUtil.buildFieldErrorList("Suspend", ServerConstant.FIELD_ERROR_NOT_NULL, fieldErrorDtos));
            //eric 20191219 end--
        }

        MoeRegimenPo moeRegimen = null;
        if (StringUtils.isNotBlank(dtoRecord.getRegimenType())) {
            moeRegimen = DtoUtil.REGIMEN_ID_MAP.get(dtoRecord.getRegimenType());
            if (moeRegimen != null) {
                commonDosagePo.setMoeRegimen(moeRegimen);
            } else {
                //eric 20191219 start--
//                throw new ParamException(localeMessageSourceUtil.getMessage("param.wrongInputSubmission"), true,
//                        validateUtil.buildFieldErrorList("RegimenType", ServerConstant.FIELD_ERROR_NOT_NULL, fieldErrorDtos));
                throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage(),
                        true, validateUtil.buildFieldErrorList("RegimenType", ServerConstant.FIELD_ERROR_NOT_NULL, fieldErrorDtos));
                //eric 20191219 end--
            }
        } else {
            //eric 20191223 start--
            throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage(), true,
                    validateUtil.buildFieldErrorList("RegimenType", ServerConstant.FIELD_ERROR_NOT_NULL, fieldErrorDtos));
            //eric 20191223 end--
        }

        MoeFreqPo moeFreq = null;
        if (dtoRecord.getFreqId() != null) {
            moeFreq = DtoUtil.FREQ_ID_MAP.get(dtoRecord.getFreqId());
            if (moeFreq != null) {
                commonDosagePo.setMoeFreq(moeFreq);
                commonDosagePo.setFreqCode(moeFreq.getFreqCode());
                commonDosagePo.setFreqEng(moeFreq.getFreqEng());
            } else {
                //eric 20191223 start--
                throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage(), true,
                        validateUtil.buildFieldErrorList("FreqId", ServerConstant.FIELD_ERROR_NOT_NULL, fieldErrorDtos));
                //eric 20191223 end--
            }
        } else {
            //eric 20191223 start--
            throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage(), true,
                    validateUtil.buildFieldErrorList("FreqId", ServerConstant.FIELD_ERROR_NOT_NULL, fieldErrorDtos));
            //eric 20191223 end--
        }

        MoeCommonDosageTypePo moeCommonDosageType = null;
        if (StringUtils.isNotBlank(dtoRecord.getStrCommonDosageType())) {
            moeCommonDosageType = DtoUtil.COMMON_DOSAGE_TYPE_ID_MAP.get(dtoRecord.getStrCommonDosageType());
            if (moeCommonDosageType != null) {
                commonDosagePo.setMoeCommonDosageType(moeCommonDosageType);
            } else {
                //eric 20191223 start--
                throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage(), true,
                        validateUtil.buildFieldErrorList("DosageType", ServerConstant.FIELD_ERROR_NOT_NULL, fieldErrorDtos));
                //eric 20191223 end--
            }
        } else {
            //eric 20191223 start--
            throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage(), true,
                    validateUtil.buildFieldErrorList("DosageType", ServerConstant.FIELD_ERROR_NOT_NULL, fieldErrorDtos));
            //eric 20191223 end--
        }

        MoeRoutePo moeRoute = null;
        if (dtoRecord.getRouteId() != null) {
            moeRoute = DtoUtil.ROUTE_ID_MAP.get(dtoRecord.getRouteId());
            if (moeRoute != null) {
                commonDosagePo.setRouteId(moeRoute.getRouteId());
                commonDosagePo.setRouteEng(moeRoute.getRouteEng());
            }
        }

        return commonDosagePo;
    }

    // Simon 20191014 start--
    @Override
    public void deleteMoeCommonDosageByLocalDrugId(String localDrugId) throws Exception {

        List<MoeCommonDosagePo> moeCommonDosageList = moeCommonDosageRepository.findAllByLocalDrugId(localDrugId);
        boolean isMoeCommonDosageListValid = moeCommonDosageList != null && !moeCommonDosageList.isEmpty();

        if (isMoeCommonDosageListValid) {
            for (MoeCommonDosagePo record : moeCommonDosageList) {
                moeCommonDosageRepository.delete(record);
                moeCommonDosageLogRepository.saveAndFlush(new MoeCommonDosageLogPo(ServerConstant.RECORD_DELETE, record));
            }
        }

    }
    //Simon 20191014 end--

    private void checkCommonDosageDuplicate(List<InnerSaveCommonDosageDto> commonDosageDtos, InnerSaveCommonDosageDto target) throws Exception {

        BigDecimal zero = new BigDecimal(0);
        MoeCommonDosageDto source = target.getMoeCommonDosage();

        for (InnerSaveCommonDosageDto innerSaveCommonDosageDto : commonDosageDtos) {
            if (innerSaveCommonDosageDto != target && !ServerConstant.RECORD_DELETE.equalsIgnoreCase(innerSaveCommonDosageDto.getActionType())) {
                MoeCommonDosageDto record = innerSaveCommonDosageDto.getMoeCommonDosage();

                String recordDosageType = StringUtils.isNotBlank(record.getStrCommonDosageType()) ? record.getStrCommonDosageType() : "";
                BigDecimal recordDosageValue = record.getDosage() != null ? record.getDosage() : zero;
                Long recordDosageUnitId = record.getDosageUnitId() != null ? record.getDosageUnitId() : 0L;
                Long recordDosageFreqId = record.getFreqId() != null ? record.getFreqId() : 0L;
                Long recordDosageRoute = record.getRouteId() != null ? record.getRouteId() : 0L;
                String recordDosagePrn = StringUtils.isNotBlank(record.getPrn()) ? record.getPrn() : "";
                Long recordFreq1 = record.getFreq1() != null ? record.getFreq1() : 0L;
                String recordSuspend = StringUtils.isNotBlank(record.getSuspend()) ? record.getSuspend() : "";

                String sourceDosageType = StringUtils.isNotBlank(source.getStrCommonDosageType()) ? source.getStrCommonDosageType() : "";
                BigDecimal sourceDosageValue = source.getDosage() != null ? source.getDosage() : zero;
                Long sourceDosageUnitId = source.getDosageUnitId() != null ? source.getDosageUnitId() : 0L;
                Long sourceDosageFreqId = source.getFreqId() != null ? source.getFreqId() : 0L;
                Long sourceDosageRoute = source.getRouteId() != null ? source.getRouteId() : 0L;
                String sourceDosagePrn = StringUtils.isNotBlank(source.getPrn()) ? source.getPrn() : "";
                Long sourceFreq1 = source.getFreq1() != null ? source.getFreq1() : 0L;
                String sourceSuspend = StringUtils.isNotBlank(source.getSuspend()) ? source.getSuspend() : "";

                if (recordDosageType.equalsIgnoreCase(sourceDosageType)
                        && recordDosageValue.compareTo(sourceDosageValue) == 0
                        && recordDosageUnitId.compareTo(sourceDosageUnitId) == 0
                        && recordDosageFreqId.compareTo(sourceDosageFreqId) == 0
                        && recordDosageRoute.compareTo(sourceDosageRoute) == 0
                        && recordDosagePrn.equalsIgnoreCase(sourceDosagePrn)
                        && recordFreq1.compareTo(sourceFreq1) == 0
                        && recordSuspend.equalsIgnoreCase(sourceSuspend)) {
                    //eric 20191223 start--
//                    throw new ParamException(localeMessageSourceUtil.getMessage("records.alreadyExist"));
                    throw new ParamException(ResponseCode.SpecialMessage.RECORDS_ALREADY_EXIST.getResponseCode(),
                            ResponseCode.SpecialMessage.RECORDS_ALREADY_EXIST.getResponseMessage());
                    //eric 20191223 end--
                }
            }
        }
    }
}
