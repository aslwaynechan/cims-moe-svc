package hk.health.moe.facade.Impl;

import hk.health.moe.common.ServerConstant;
import hk.health.moe.facade.MoeDrugGenericNameLocalFacade;
import hk.health.moe.pojo.dto.MoeDrugLocalDto;
import hk.health.moe.pojo.dto.inner.InnerSaveDrugDto;
import hk.health.moe.pojo.po.MoeDrugGenericNameLocalPo;
import hk.health.moe.pojo.po.MoeDrugStrengthPo;
import hk.health.moe.repository.MoeDrugGenericNameLocalRepository;
import hk.health.moe.repository.MoeDrugStrengthRepository;
import hk.health.moe.service.impl.BaseServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class MoeDrugGenericNameLocalFacadeImpl extends BaseServiceImpl implements MoeDrugGenericNameLocalFacade {

    @Autowired
    private MoeDrugGenericNameLocalRepository moeDrugGenericNameLocalRepository;

    @Autowired
    private MoeDrugStrengthRepository moeDrugStrengthRepository;

    @Override
    public boolean checkIsMoeDrugGenericNameLocalExist(InnerSaveDrugDto dto) throws Exception {

        boolean isExist = ServerConstant.DEFAULT_VALUE_FALSE;

        if (dto.getMoeDrugLocal().getStrengthCompulsory().equalsIgnoreCase("N")) {
            //StrengthCompulsory = 'N'

            Long vtmRouteFormId = getVtmRouteFormIdFromInnerSaveDrugDto(dto);

            if (vtmRouteFormId != null) {
                Integer vtmRouteFormIdExist = moeDrugGenericNameLocalRepository.isVtmRouteFormIdExist(vtmRouteFormId);
                isExist = vtmRouteFormIdExist != null && vtmRouteFormIdExist > 0;
            }

        } else {
            //StrengthCompulsory = 'Y'

            Long vmpId = getVmpIdFromInnerSaveDrugDto(dto);

            if (vmpId != null) {
                Integer vmpIdExist = moeDrugGenericNameLocalRepository.isVmpIdExist(vmpId);
                isExist = vmpIdExist != null && vmpIdExist > 0;
            }

        }

        return isExist;

    }


    //Get VtmRouteFormId from InnerSaveDrugDto
    private Long getVtmRouteFormIdFromInnerSaveDrugDto(InnerSaveDrugDto dto) {

        Long vtmRouteFormId = null;

        if (dto.getMoeDrugLocal() != null) {
            vtmRouteFormId = dto.getMoeDrugLocal().getVtmRouteFormId();
        }

        if (vtmRouteFormId == null || vtmRouteFormId <= 0L) {
            return null;
        }

        return vtmRouteFormId;

    }


    //Get VmpId from InnerSaveDrugDto
    private Long getVmpIdFromInnerSaveDrugDto(InnerSaveDrugDto dto) {

        Long vmpId = null;
        Integer localVmpId = null;

        if (dto.getMoeDrugStrengthLocal() != null) {
            localVmpId = dto.getMoeDrugStrengthLocal().getVmpId();
        }

        if (localVmpId != null && localVmpId != 0) {
            vmpId = localVmpId.longValue();
        } else if (StringUtils.isNotBlank(dto.getHkRegNo())) {      //If can't get vmpId in moe_drug_strength_local, try to get it from moe_drug_strength by HkRegNo
            List<MoeDrugStrengthPo> moeDrugStrengthPoList = moeDrugStrengthRepository.findByHkRegNo(dto.getHkRegNo());
            if (moeDrugStrengthPoList != null && moeDrugStrengthPoList.size() > 0) {
                vmpId = moeDrugStrengthPoList.get(0).getVmpId();
            }
        }

        if (vmpId == null || vmpId <= 0L) {
            return null;
        }

        return vmpId;

    }


    @Override
    public void addGenericNameLocalRecord(InnerSaveDrugDto dto) throws Exception {

        Date now = new Date();
        MoeDrugGenericNameLocalPo savePo = new MoeDrugGenericNameLocalPo();

        MoeDrugLocalDto moeDrugLocalDto = dto.getMoeDrugLocal();
        String strengthCompulsory = moeDrugLocalDto.getStrengthCompulsory();
        Long vtmRouteFormId = moeDrugLocalDto.getVtmRouteFormId();
        Integer getVmpId = dto.getMoeDrugStrengthLocal().getVmpId();
        Long vmpId = null;

        if (getVmpId != null && getVmpId != 0) {
            vmpId = getVmpId.longValue();
        } else if (strengthCompulsory.equalsIgnoreCase("Y")  && (getVmpId == null || getVmpId == 0)) {

            //get from moe_drug_strength
            if (StringUtils.isNotBlank(dto.getHkRegNo()) && dto.getMoeDrugStrengthLocal().getAmpId() != null && dto.getMoeDrugStrengthLocal().getAmpId() != 0) {
                MoeDrugStrengthPo moeDrugStrengthPo = moeDrugStrengthRepository.findByHkRegNoAndAmpId(dto.getHkRegNo(), dto.getMoeDrugStrengthLocal().getAmpId().longValue());
                if (moeDrugStrengthPo != null && moeDrugStrengthPo.getVmpId() != null && moeDrugStrengthPo.getVmpId() != 0L) {
                    vmpId = moeDrugStrengthPo.getVmpId();
                }

            }

        }

        //Check
        if (strengthCompulsory.equalsIgnoreCase("N")  && (vtmRouteFormId == null || vtmRouteFormId == 0L)) {
            System.out.println("Strength Compulsory = \"N\" and vtmRouteFormId is null, will not create a new record for Generic Name Level.");
            return;
        } else if (strengthCompulsory.equalsIgnoreCase("Y") && (vmpId == null || vmpId == 0L)) {
            System.out.println("Strength Compulsory = \"Y\" and vmpId is null, will not create a new record for Generic Name Level.");
            return;
        }

        String suffix = strengthCompulsory.equalsIgnoreCase("Y") ? String.valueOf(vmpId) : String.valueOf(vtmRouteFormId);

        //TODO Primary Key
        savePo.setLocalDrugId(strengthCompulsory + suffix);
        savePo.setVtm(moeDrugLocalDto.getVtm());
        savePo.setFormId(moeDrugLocalDto.getFormId());
        savePo.setFormEng(moeDrugLocalDto.getFormEng());
        savePo.setDoseFormExtraInfoId(moeDrugLocalDto.getDoseFormExtraInfoId());
        savePo.setDoseFormExtraInfo(moeDrugLocalDto.getDoseFormExtraInfo());
        savePo.setRouteId(moeDrugLocalDto.getRouteId());
        savePo.setRouteEng(moeDrugLocalDto.getRouteEng());
        savePo.setVtmId(moeDrugLocalDto.getVtmId());
        savePo.setVtmRouteId(moeDrugLocalDto.getVtmRouteId());
        savePo.setVtmRouteFormId(moeDrugLocalDto.getVtmRouteFormId());
        savePo.setLegalClassId(moeDrugLocalDto.getLegalClassId());
        savePo.setLegalClass(moeDrugLocalDto.getLegalClass());
        savePo.setStrengthCompulsory(strengthCompulsory);
        savePo.setVmpId(vmpId);
        savePo.setVmp(dto.getMoeDrugStrengthLocal().getVmp());

        savePo.setStrength(dto.getMoeDrugStrengthLocal().getStrength());
        savePo.setStrengthLevelExtraInfo(dto.getMoeDrugStrengthLocal().getStrengthLevelExtraInfo());

        savePo.setPrescribeUnit(moeDrugLocalDto.getPrescribeUnit());
        savePo.setPrescribeUnitId(moeDrugLocalDto.getPrescribeUnitId());
        savePo.setBaseUnit(moeDrugLocalDto.getBaseUnit());
        savePo.setBaseUnitId(moeDrugLocalDto.getBaseUnitId());
        savePo.setDispenseUnit(moeDrugLocalDto.getDispenseUnit());
        savePo.setDispenseUnitId(moeDrugLocalDto.getDispenseUnitId());

        savePo.setCreateUserId(getUserDto().getLoginId());
        savePo.setCreateUser(getUserDto().getLoginName());
        savePo.setCreateHosp(getUserDto().getHospitalCd());
        savePo.setCreateRank(getUserDto().getUserRankCd());
        savePo.setCreateRankDesc(getUserDto().getUserRankDesc());
        savePo.setCreateDtm(now);

        savePo.setUpdateUserId(getUserDto().getLoginId());
        savePo.setUpdateUser(getUserDto().getLoginName());
        savePo.setUpdateHosp(getUserDto().getHospitalCd());
        savePo.setUpdateRank(getUserDto().getUserRankCd());
        savePo.setUpdateRankDesc(getUserDto().getUserRankDesc());
        savePo.setUpdateDtm(now);

        moeDrugGenericNameLocalRepository.save(savePo);

    }


    @Override
    public void deleteGenericNameLocalRecord(InnerSaveDrugDto dto) throws Exception {

        MoeDrugLocalDto moeDrugLocalDto = dto.getMoeDrugLocal();
        String strengthCompulsory = moeDrugLocalDto.getStrengthCompulsory();

        if (strengthCompulsory.equalsIgnoreCase("Y")) {
            //StrengthCompulsory = 'Y'

            Long vmpId = getVmpIdFromInnerSaveDrugDto(dto);

            if (vmpId != null) {
                List<MoeDrugGenericNameLocalPo> moeDrugGenericNameLocalPoList = moeDrugGenericNameLocalRepository.findByVmpId(vmpId);

                if (moeDrugGenericNameLocalPoList != null && moeDrugGenericNameLocalPoList.size() == 1) {
                    moeDrugGenericNameLocalRepository.delete(moeDrugGenericNameLocalPoList.get(0));
                    System.out.println("Delete Generic Name Level record:" + strengthCompulsory + vmpId);
                }
            }

        } else  {
            //StrengthCompulsory = 'N'

            Long vtmRouteFormId = getVtmRouteFormIdFromInnerSaveDrugDto(dto);

            if (vtmRouteFormId != null) {
                List<MoeDrugGenericNameLocalPo> moeDrugGenericNameLocalPoList = moeDrugGenericNameLocalRepository.findByVtmRouteFormId(vtmRouteFormId);

                if (moeDrugGenericNameLocalPoList != null && moeDrugGenericNameLocalPoList.size() == 1) {
                    moeDrugGenericNameLocalRepository.delete(moeDrugGenericNameLocalPoList.get(0));
                    System.out.println("Delete Generic Name Level record:" + strengthCompulsory + vtmRouteFormId);
                }
            }

        }

    }


}
