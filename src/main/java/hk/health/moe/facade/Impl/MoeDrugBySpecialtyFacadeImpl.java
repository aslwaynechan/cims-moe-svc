package hk.health.moe.facade.Impl;

import hk.health.moe.common.ServerConstant;
import hk.health.moe.facade.MoeDrugBySpecialtyFacade;
import hk.health.moe.pojo.dto.MoeDrugBySpecialtyDto;
import hk.health.moe.pojo.dto.inner.InnerSaveDrugBySpecialtyDto;
import hk.health.moe.pojo.po.MoeDrugBySpecialtyLogPo;
import hk.health.moe.pojo.po.MoeDrugBySpecialtyPo;
import hk.health.moe.repository.MoeDrugBySpecialtyLogRepository;
import hk.health.moe.repository.MoeDrugBySpecialtyRepository;
import hk.health.moe.service.impl.BaseServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MoeDrugBySpecialtyFacadeImpl extends BaseServiceImpl implements MoeDrugBySpecialtyFacade {

    @Autowired
    private MoeDrugBySpecialtyRepository moeDrugBySpecialtyRepository;

    @Autowired
    private MoeDrugBySpecialtyLogRepository moeDrugBySpecialtyLogRepository;

    @Autowired
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public void saveDrugBySpecialty(List<InnerSaveDrugBySpecialtyDto> drugBySpecialtyItems) throws Exception {

        List<MoeDrugBySpecialtyPo> insertSpecialtyList = new ArrayList<>();
        List<MoeDrugBySpecialtyPo> deleteSpecialtyList = new ArrayList<>();

        for (InnerSaveDrugBySpecialtyDto dto : drugBySpecialtyItems) {
            MoeDrugBySpecialtyDto moeDrugBySpecialtyDto = dto.getMoeDrugBySpecialty();
            String actionType = StringUtils.isBlank(dto.getActionType()) ? "" : dto.getActionType().toUpperCase();
            if (StringUtils.isNotBlank(moeDrugBySpecialtyDto.getLocalDrugId())) {
                switch (actionType) {
                    case ServerConstant.RECORD_DELETE:
                        deleteSpecialtyList.add(dozerBeanMapper.map(moeDrugBySpecialtyDto, MoeDrugBySpecialtyPo.class));
                        break;
                    case ServerConstant.RECORD_INSERT:
                        insertSpecialtyList.add(addSpecialty(moeDrugBySpecialtyDto));
                        break;
                    default:
                        break;
                }
            }
        }

        if (insertSpecialtyList != null) {
            for (MoeDrugBySpecialtyPo specialty : insertSpecialtyList) {
                moeDrugBySpecialtyRepository.save(specialty);
                MoeDrugBySpecialtyLogPo moeDrugBySpecialtyLog = new MoeDrugBySpecialtyLogPo(ServerConstant.RECORD_INSERT, specialty);
                moeDrugBySpecialtyLogRepository.save(moeDrugBySpecialtyLog);
            }
        }

        if (deleteSpecialtyList != null) {
            for (MoeDrugBySpecialtyPo specialty : deleteSpecialtyList) {
                moeDrugBySpecialtyRepository.delete(specialty);
                MoeDrugBySpecialtyLogPo moeDrugBySpecialtyLog = new MoeDrugBySpecialtyLogPo(ServerConstant.RECORD_DELETE, specialty);
                moeDrugBySpecialtyLogRepository.save(moeDrugBySpecialtyLog);
            }
        }
    }

    @Override
    public List<MoeDrugBySpecialtyDto> getDrugBySpecialtyByLocalDrugId(String localDrugId, String hospitalCode) throws Exception {

        List<MoeDrugBySpecialtyDto> result = new ArrayList<MoeDrugBySpecialtyDto>();

        List<MoeDrugBySpecialtyPo> moeDrugBySpecialtyList = null;

        if (hospitalCode == null
                || hospitalCode.isEmpty()
                || hospitalCode.equalsIgnoreCase("LOCALEMR")) {
            // get all same local Drug ID record of drug by specialty
            moeDrugBySpecialtyList = moeDrugBySpecialtyRepository.findByLocalDrugId(localDrugId);

        } else {
            // only get same local Drug ID and hospitalCode record of drug by specialty
            moeDrugBySpecialtyList = moeDrugBySpecialtyRepository.findByLocalDrugIdHospCode(localDrugId, hospitalCode);

        }

        if (moeDrugBySpecialtyList != null) {
            for (MoeDrugBySpecialtyPo record : moeDrugBySpecialtyList) {
                result.add(dozerBeanMapper.map(record, MoeDrugBySpecialtyDto.class));
            }
        }

        return result;
    }


    public MoeDrugBySpecialtyPo addSpecialty(MoeDrugBySpecialtyDto saveDto) throws Exception {

        MoeDrugBySpecialtyPo savePo = dozerBeanMapper.map(saveDto, MoeDrugBySpecialtyPo.class);
        savePo.setCreateUserId(getUserDto().getLoginId());
        savePo.setCreateUser(getUserDto().getLoginName());
        savePo.setCreateHosp(getUserDto().getHospitalCd());
        savePo.setCreateRank(getUserDto().getUserRankCd());
        savePo.setCreateRankDesc(getUserDto().getUserRankDesc());

        return savePo;
    }

    // Simon 20191014 start--
    @Override
    public boolean deleteDrugBySpecialty(String localDrugId) throws Exception {

        List<MoeDrugBySpecialtyPo> moeDrugBySpecialtyList = moeDrugBySpecialtyRepository.findByLocalDrugId(localDrugId);
        if (moeDrugBySpecialtyList != null) {
            for (MoeDrugBySpecialtyPo moeDrugBySpecialty : moeDrugBySpecialtyList) {
                moeDrugBySpecialtyRepository.delete(moeDrugBySpecialty);
                moeDrugBySpecialtyLogRepository.saveAndFlush(new MoeDrugBySpecialtyLogPo(ServerConstant.RECORD_DELETE, moeDrugBySpecialty));
            }
        }

        return false;
    }
    //Simon 20191014 end--
}
