package hk.health.moe.facade.Impl;

import hk.health.moe.common.MoeCommonHelper;
import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.ConcurrentException;
import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.exception.ParamException;
import hk.health.moe.exception.RecordException;
import hk.health.moe.facade.MoeFavouriteFacade;
import hk.health.moe.pojo.dto.MoeMedProfileDto;
import hk.health.moe.pojo.dto.MoeMyFavouriteHdrDto;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.pojo.po.MoeMyFavouriteDetailPo;
import hk.health.moe.pojo.po.MoeMyFavouriteHdrPo;
import hk.health.moe.pojo.po.MoeMyFavouriteMultDosePo;
import hk.health.moe.repository.MoeMyFavouriteDetailRepository;
import hk.health.moe.repository.MoeMyFavouriteHdrRepository;
import hk.health.moe.service.MoeContentService;
import hk.health.moe.util.DtoMapUtil;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.StringUtil;
import hk.health.moe.util.SystemSettingUtil;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class MoeFavouriteFacadeImpl implements MoeFavouriteFacade {

    public final static Logger logger = LoggerFactory.getLogger(MoeFavouriteFacadeImpl.class);

    @Autowired
    private MoeMyFavouriteHdrRepository moeMyFavouriteHdrRepository;

//    @Autowired
//    private SystemSettingUtil systemSettingUtil;

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;
    @Autowired
    private MoeMyFavouriteDetailRepository moeMyFavouriteDetailRepository;
    @Autowired
    private DozerBeanMapper dozerBeanMapper;
    @Autowired
    private MoeContentService moeContentService;

    @Override
    public List<MoeMyFavouriteHdrDto> getMyFavourite(String userId, UserDto userDto) throws Exception {
        String myFavSpecialty = userDto.getSpec();
        if (StringUtil.stripToEmpty(userId).length() == 0) {
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("param.Invalid"));
            throw new MoeServiceException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 end--
        }
        List<MoeMyFavouriteHdrPo> hdrs = null;
        if (SystemSettingUtil.isSettingEnabled(ServerConstant.ENABLE_ASSIGN_SPECIALTY_INTO_MY_FAVOURITE)) {
            hdrs = moeMyFavouriteHdrRepository.findAllByUserId(userId, StringUtil.nullToEmpty(myFavSpecialty));
        } else {
            hdrs = moeMyFavouriteHdrRepository.findAllByUserId(userId);
        }
        try {
            hdrs = sortMyFavourites(hdrs);
        } catch (Exception e) {
            logger.warn(localeMessageSourceUtil.getMessage("myfavourite.sort.error"));
        }
        String enableHkmttIndicator = SystemSettingUtil.getParamValue("enable_my_favourite_hkmtt_update_indicator");
        Integer updateIndicator = null;
        if (SystemSettingUtil.getParamValue("my_favourite_hkmtt_update_indicator") != null) {
            updateIndicator = Integer.parseInt(SystemSettingUtil.getParamValue("my_favourite_hkmtt_update_indicator"));
        }
        return DtoMapUtil.convertMoeMyFavouriteHdrDtos(hdrs, enableHkmttIndicator, updateIndicator, userDto);
    }

    @Override
    public List<MoeMyFavouriteHdrDto> getDepartmentalFavourite(UserDto userDto) throws Exception {
        String userSpecialty = userDto.getSpec();
        if (StringUtil.stripToEmpty(userSpecialty).length() == 0) {
            throw new MoeServiceException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        }

        // Ricci 20191107 start -- support cache
        List<MoeMyFavouriteHdrDto> result = new ArrayList<>();
        List<MoeMyFavouriteHdrPo> hdrs = null;
        Set<String> excludeFavIdSet = new HashSet<>();
        List<MoeMyFavouriteHdrPo> cacheDtoList = moeContentService.listMyFavourite();
        if (cacheDtoList != null && cacheDtoList.size() != 0) {
            for (MoeMyFavouriteHdrPo cacheDto : cacheDtoList) {
                if (StringUtils.isNotBlank(cacheDto.getMyFavouriteId())) {
                    excludeFavIdSet.add(cacheDto.getMyFavouriteId());
                }
            }
        }

        if (excludeFavIdSet.size() != 0) {
            hdrs = moeMyFavouriteHdrRepository.findAllByUserSpecialtyExcludeIdSet(userSpecialty, excludeFavIdSet);
            hdrs.addAll(cacheDtoList);
        } else {
            hdrs = moeMyFavouriteHdrRepository.findAllByUserSpecialty(userSpecialty);
            if (cacheDtoList != null) {
                hdrs.addAll(cacheDtoList);
            }
        }
        // Ricci 20191107 end --

        try {
            if (hdrs != null && hdrs.size() > 0) {
                hdrs = sortMyFavourites(hdrs);
            }
        } catch (Exception e) {
            logger.warn("Fail to sort my favourite for user specialty " + userSpecialty + ".");
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.sort.error"));
        }

        String enableHkmttIndicator = SystemSettingUtil.getParamValue("enable_my_favourite_hkmtt_update_indicator");
        Integer updateIndicator = null;
        if (SystemSettingUtil.getParamValue("my_favourite_hkmtt_update_indicator") != null) {
            updateIndicator = Integer.parseInt(SystemSettingUtil.getParamValue("my_favourite_hkmtt_update_indicator"));
        }

        result.addAll(DtoMapUtil.convertMoeMyFavouriteHdrDtos(hdrs, enableHkmttIndicator, updateIndicator, userDto));
        return result;
    }

    private List<MoeMyFavouriteHdrPo> sortMyFavourites(List<MoeMyFavouriteHdrPo> hdrs) throws Exception {
        if (hdrs == null || hdrs.size() <= 1) {
            return hdrs;
        }
        MoeMyFavouriteHdrPo cache = null;
        List<MoeMyFavouriteHdrPo> sorted = new ArrayList<>(hdrs.size());
        HashMap<String, MoeMyFavouriteHdrPo> map = new HashMap<>();
        for (MoeMyFavouriteHdrPo hdr : hdrs) {
            if (StringUtils.isNotBlank(hdr.getMyFavouriteId())) {
                map.put(hdr.getFrontMyFavouriteId(), hdr);
            } else {
                cache = hdr;    // For cache which has not save into DB .. only support one cache
            }
        }
        if (!map.isEmpty()) {
            int loopSize = hdrs.size();
            if (cache != null) {
                loopSize--; // reduce cache
            }
            MoeMyFavouriteHdrPo nextHdr = map.get(ServerConstant.MY_FAVOURITED_HEAD_ID);
            sorted.add(nextHdr);
            while (sorted.size() < loopSize) {
                nextHdr = map.get(nextHdr.getMyFavouriteId());
                if (nextHdr == null) {
                    //eric 20191219 start--
//                    throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.sort.error"));
                    throw new MoeServiceException(ResponseCode.SpecialMessage.MY_FAVOURITE_SORT_ERROR.getResponseCode(),
                            ResponseCode.SpecialMessage.MY_FAVOURITE_SORT_ERROR.getResponseMessage());

                    //eric 20191219 end--
                }
                sorted.add(nextHdr);
            }
        }
        if (cache != null) {
            sorted.add(cache);
        }

        return sorted;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public List<MoeMyFavouriteHdrDto> orderMyFavouritesDetail(List<MoeMyFavouriteHdrDto> favs, final UserDto userDto, boolean isDepartment) throws Exception {
        if (favs == null || favs.size() == 0) {
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("param.Invalid"));
            throw new MoeServiceException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 end--
        }
        List<MoeMyFavouriteHdrPo> hdrs = DtoMapUtil.convertMoeMyFavouriteHdrs(favs, false);
        HashMap<String, Long> myFavouriteList = new HashMap<String, Long>();
        List<Object[]> object = new ArrayList<>();
        if (isDepartment) {
            object = moeMyFavouriteHdrRepository.getDeptMyFavouriteVersionMapping(userDto.getSpec());
        } else {
            object = moeMyFavouriteHdrRepository.getMyFavouriteVersionMapping(hdrs.get(0).getCreateUserId());
        }
        for (Object[] myFavourite : object) {
            myFavouriteList.put((String) myFavourite[0], (Long) myFavourite[1]);
        }

        for (MoeMyFavouriteHdrPo hdr : hdrs) {
            if (myFavouriteList.get(hdr.getMyFavouriteId()) == null) {
                //eric 20191219 start--
//                throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.deletedByOthers"));
                throw new MoeServiceException(ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseCode(),
                        ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseMessage());
                //eric 20191219 end--
            }
            if (myFavouriteList.get(hdr.getMyFavouriteId()).longValue() != hdr.getVersion().longValue()) {
                //eric 20191219 start--
//                throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.UpdatedByOthers"));
                throw new MoeServiceException(ResponseCode.SpecialMessage.MY_FAVOURITE_UPDATED_BY_OTHERS.getResponseCode(),
                        ResponseCode.SpecialMessage.MY_FAVOURITE_UPDATED_BY_OTHERS.getResponseMessage());
                //eric 20191219 end--
            }
            updateFavUserInfo(hdr, userDto, isDepartment);
        }
        arrangeMyFavourites(hdrs);
        moeMyFavouriteHdrRepository.saveAll(hdrs);
        // response.setResponseErrorCode(ServerConstant.SUCCESS_ERRCODE);
        String enableHkmttIndicator = SystemSettingUtil.getParamValue("enable_my_favourite_hkmtt_update_indicator");
        Integer updateIndicator = null;
        if (SystemSettingUtil.getParamValue("my_favourite_hkmtt_update_indicator") != null) {
            updateIndicator = Integer.parseInt(SystemSettingUtil.getParamValue("my_favourite_hkmtt_update_indicator"));
        }
        return DtoMapUtil.convertMoeMyFavouriteHdrDtos(hdrs, enableHkmttIndicator, updateIndicator, userDto);

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public MoeMyFavouriteHdrDto orderMyFavouriteDetail(MoeMyFavouriteHdrDto fav, final UserDto userDto, boolean isDepartment) throws Exception {
        if (fav == null) {
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("param.Invalid"));
            throw new MoeServiceException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 end--
        }
//        MoeMyFavouriteHdrPo hdr = DtoMapUtil.convertMoeMyFavouriteHdr(fav, false);
        //List<MoeMyFavouriteHdrPo> hdrs = moeMyFavouriteHdrRepository.getByMyFavouriteId(fav.getMyFavouriteId());
        List<MoeMyFavouriteHdrPo> listById = moeMyFavouriteHdrRepository.getAllByMyFavouriteId(fav.getMyFavouriteId());

        if (listById == null || listById.size() == 0)
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("param.Invalid"));
            throw new MoeServiceException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        //eric 20191219 end--
        MoeMyFavouriteHdrPo hdr = listById.get(0);
        if (hdr.getVersion().longValue() != fav.getVersion().longValue())
            //eric 20191219 start--
//            throw new ConcurrentException(localeMessageSourceUtil.getMessage("records.concurrentErrors"));
            throw new ConcurrentException(ResponseCode.SpecialMessage.RECORDS_CONCURRENT_ERRORS.getResponseCode(),
                    ResponseCode.SpecialMessage.RECORDS_CONCURRENT_ERRORS.getResponseMessage());
            //eric 20191219 end--
        updateFavUserInfo(hdr, userDto, isDepartment);
        if (hdr.getMoeMyFavouriteDetails().size() != fav.getMoeMedProfiles().size())
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("param.Invalid"));
            throw new MoeServiceException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        //eric 20191219 start--
        int count = 0;
        Set<MoeMyFavouriteDetailPo> targetSet = new LinkedHashSet<>();
        for (MoeMyFavouriteDetailPo po : hdr.getMoeMyFavouriteDetails()) {
            MoeMyFavouriteDetailPo tempPo = dozerBeanMapper.map(po, MoeMyFavouriteDetailPo.class);
            Set<MoeMyFavouriteMultDosePo> doseSet = new LinkedHashSet<>();
            tempPo.setItemNo(fav.getMoeMedProfiles().get(count++).getOrgItemNo());
            for (MoeMyFavouriteMultDosePo dosePo : po.getMoeMyFavouriteMultDoses()) {
                MoeMyFavouriteMultDosePo tempDosePo = dozerBeanMapper.map(dosePo, MoeMyFavouriteMultDosePo.class);
                tempDosePo.setMoeMyFavouriteDetail(tempPo);
                doseSet.add(tempDosePo);
            }
            tempPo.setMoeMyFavouriteMultDoses(doseSet);
            targetSet.add(tempPo);
        }
        hdr.getMoeMyFavouriteDetails().clear();
        hdr.getMoeMyFavouriteDetails().addAll(targetSet);
        moeMyFavouriteHdrRepository.save(hdr);
        return fav;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public MoeMyFavouriteHdrDto deleteMyFavourite(String createUserId, MoeMyFavouriteHdrDto hdr) throws Exception {
        if (StringUtil.stripToEmpty(createUserId).length() == 0 ||
                StringUtil.stripToEmpty(hdr.getMyFavouriteId()).length() == 0 || StringUtil.stripToEmpty(hdr.getFrontMyFavouriteId()).length() == 0) {
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("param.Invalid"));
            throw new MoeServiceException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 start--
        }
        List<MoeMyFavouriteHdrPo> listById = moeMyFavouriteHdrRepository.getByMyFavouriteId(hdr.getMyFavouriteId());
        if (listById == null || listById.size() == 0)
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.deletedByOthers"));
            throw new MoeServiceException(ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseCode(),
                    ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseMessage());
            //eric 20191219 start--
        MoeMyFavouriteHdrPo target = listById.get(0);
        // Simon 20190724 start--comment
//        if (target == null) {
//            LOGGER.warn("My Favourite deleted by others["+hdr.getMyFavouriteId()+"]");
//            response.setResponseErrorCode(ServerConstant.DB_FAIL_RECORD_DELETED_BY_OTHERS);
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.sort.deletedByOthers"));
//        }
        // Simon 20190724 end--comment
        if (hdr.getVersion().longValue() != target.getVersion().longValue()) {
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.UpdatedByOthers"));
            throw new MoeServiceException(ResponseCode.SpecialMessage.MY_FAVOURITE_UPDATED_BY_OTHERS.getResponseCode(),
                    ResponseCode.SpecialMessage.MY_FAVOURITE_UPDATED_BY_OTHERS.getResponseMessage());
            //eric 20191219 end--
        }
        moeMyFavouriteHdrRepository.delete(target);
        // Simon 20190724 start--comment
//        if(success == 1) {
//            MoeMyFavouriteHdr hdr1 = moeMyFavouriteHdrDao.findByFrontMyFavouriteId(hdr.getMyFavouriteId());
//            if(hdr1 != null) {
//                hdr1.setFrontMyFavouriteId(target.getFrontMyFavouriteId());
//                moeMyFavouriteHdrDao.update(hdr1);
//            }
//        }
//
//        response.setResponseErrorCode(ServerConstant.SUCCESS_ERRCODE);
        // Simon 20190724 end--comment

        // Simon 20190723 start--add for sort my favourite list before delete
        List<MoeMyFavouriteHdrPo> backFavourites = moeMyFavouriteHdrRepository.getByMyFrontFavouriteId(hdr.getMyFavouriteId());
        if (backFavourites != null && backFavourites.size() > 0 && backFavourites.get(0) != null) {
            MoeMyFavouriteHdrPo backFavourite = backFavourites.get(0);
            backFavourite.setFrontMyFavouriteId(hdr.getFrontMyFavouriteId());
            moeMyFavouriteHdrRepository.save(backFavourite);
        }
        // Simon 20190723 end--add for sort my favourite list before delete
        return hdr;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public MoeMyFavouriteHdrDto deleteMyFavouriteDetail(String createUserId, MoeMyFavouriteHdrDto hdr) throws Exception {
        if (StringUtil.stripToEmpty(createUserId).length() == 0 ||
                StringUtil.stripToEmpty(hdr.getMyFavouriteId()).length() == 0) {
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("param.Invalid"));
            throw new MoeServiceException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 end--
        }
        // Simon 20190724 start--comment
//      if (target == null) {
//            LOGGER.warn("My Favourite deleted by others["+hdr.getMyFavouriteId()+"]");
//            response.setResponseErrorCode(ServerConstant.DB_FAIL_RECORD_DELETED_BY_OTHERS);
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.sort.deletedByOthers"));
//        }
//        if (hdr.getVersion() != target.getVersion()) {
//            LOGGER.warn("My Favourite updated by others["+hdr.getMyFavouriteId()+"]");
//           response.setResponseErrorCode(ServerConstant.DB_FAIL_RECORD_UPDATED_BY_OTHERS);
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.sort.UpdatedByOthers"));
//        }
        // Simon 20190724 end--comment
        List<MoeMyFavouriteHdrPo> hdrPos = moeMyFavouriteHdrRepository.getAllByMyFavouriteId(hdr.getMyFavouriteId());
        if (hdrPos == null || hdrPos.size() == 0)
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.deletedByOthers"));
            throw new MoeServiceException(ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseCode(),
                    ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseMessage());
        //eric 20191219 start--
        MoeMyFavouriteHdrPo hdrPo = hdrPos.get(0);
        if (hdrPo.getVersion().longValue() != hdr.getVersion().longValue())
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.UpdatedByOthers"));
            throw new MoeServiceException(ResponseCode.SpecialMessage.MY_FAVOURITE_UPDATED_BY_OTHERS.getResponseCode(),
                    ResponseCode.SpecialMessage.MY_FAVOURITE_UPDATED_BY_OTHERS.getResponseMessage());
        //eric 20191219 end--
        List<MoeMyFavouriteDetailPo> beforeDeleteDetailPo = moeMyFavouriteDetailRepository.findByMyFavouriteIdOrderByItemNoAsc(hdr.getMyFavouriteId());
        if (beforeDeleteDetailPo == null || beforeDeleteDetailPo.size() == 0) {
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.deletedByOthers"));
            throw new MoeServiceException(ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseCode(),
                    ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseMessage());
            //eric 20191219 start--
        } else if (beforeDeleteDetailPo.size() == 1 && hdrPo.getMyFavouriteName() == null) {
            //if the drug set with one drug and null my favourite name is deleted. the drug set is deleted also
            MoeFavouriteFacade currentProxy = (MoeFavouriteFacade) AopContext.currentProxy();
            return currentProxy.deleteMyFavourite(createUserId, hdr);
        }
        Set<MoeMyFavouriteDetailPo> detailPos = new LinkedHashSet<>();
        MoeMyFavouriteHdrPo newPo = new MoeMyFavouriteHdrPo();
        dozerBeanMapper.map(hdrPo, newPo);
        long newItemNo = 1;
        for (MoeMyFavouriteDetailPo detailPo : newPo.getMoeMyFavouriteDetails()) {
            if (detailPo.getItemNo() == hdr.getMoeMedProfiles().get(0).getOrgItemNo())
                continue;
            detailPo.setItemNo(newItemNo++);
            detailPos.add(detailPo);
        }
        newPo.getMoeMyFavouriteDetails().clear();
        newPo.getMoeMyFavouriteDetails().addAll(detailPos);
        moeMyFavouriteHdrRepository.save(newPo);
        return hdr;
    }

    // Simon 20190726 start--comment
//    @Override
//    @Transactional(propagation = Propagation.REQUIRES_NEW)
//    public List<MoeMyFavouriteHdrDto> deleteMyFavourite(String createUserId, List<MoeMyFavouriteHdrDto> list) throws Exception {
////        ResponseBean response = new ResponseBean();
//        List<MoeMyFavouriteHdrDto> hdrs = new ArrayList<>();
//        for (MoeMyFavouriteHdrDto hdr : list) {
//            hdrs.add(deleteMyFavourite(createUserId, hdr));
//        }
////        response.setResponseErrorCode(ServerConstant.SUCCESS_ERRCODE);
//        return hdrs;
//    }
    // Simon 20190726 end--comment

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public MoeMyFavouriteHdrDto saveMyFavourite(MoeMyFavouriteHdrDto fav, final UserDto userDto, boolean isDepartment) throws Exception {
        if (fav == null /*|| fav.getMyFavouriteId() != null*/) {
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("param.Invalid"));
            throw new MoeServiceException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 end--
        }
        MoeMyFavouriteHdrPo hdr = null;
        try {
            hdr = DtoMapUtil.convertMoeMyFavouriteHdrForPersistence(fav, false);
        } catch (NullPointerException nullException) {
            //eric 20191219 start--
//            throw new ParamException(localeMessageSourceUtil.getMessage("param.Invalid"));
            throw new MoeServiceException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 end--
        }
        updateFavUserInfo(hdr, userDto, isDepartment);
        List<MoeMyFavouriteHdrPo> userHdrs;
        if (isDepartment) {
            userHdrs = moeMyFavouriteHdrRepository.findAllByUserSpecialty(hdr.getUserSpecialty());
        } else {
            userHdrs = moeMyFavouriteHdrRepository.findByUserId(hdr.getCreateUserId());
        }
        if (userHdrs != null && userHdrs.size() > 0) {
            try {
                if (userHdrs != null && userHdrs.size() > 0)
                    userHdrs = sortMyFavourites(userHdrs);
            } catch (Exception e) {
                arrangeMyFavourites(userHdrs);
//                throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.deletedByOthers"));
            }
            if (StringUtils.isBlank(fav.getMyFavouriteId())) {
                hdr.setFrontMyFavouriteId(userHdrs.get(userHdrs.size() - 1).getMyFavouriteId());
            }
        } else {
            hdr.setFrontMyFavouriteId(ServerConstant.MY_FAVOURITED_HEAD_ID);
        }
        updateFavUserInfo(hdr, userDto, isDepartment);
        hdr = moeMyFavouriteHdrRepository.save(hdr);
        String enableHkmttIndicator = SystemSettingUtil.getParamValue("enable_my_favourite_hkmtt_update_indicator");
        Integer updateIndicator = null;
        if (SystemSettingUtil.getParamValue("my_favourite_hkmtt_update_indicator") != null) {
            updateIndicator = Integer.parseInt(SystemSettingUtil.getParamValue("my_favourite_hkmtt_update_indicator"));
        }
        return DtoMapUtil.convertMoeMyFavouriteHdrDto(hdr, enableHkmttIndicator, updateIndicator, userDto);
    }

    private void updateFavUserInfo(MoeMyFavouriteHdrPo hdr, final UserDto userDto, boolean isDepartment) {
        Date now = new Date();
        if (hdr.getMyFavouriteId() == null) {
            if (hdr.getCreateUserId() == null) {
                hdr.setCreateUserId(userDto.getLoginId());
                hdr.setCreateUser(userDto.getLoginName());
            }
            hdr.setCreateHosp(userDto.getHospitalCd());
            hdr.setCreateRank(userDto.getUserRankCd());
            hdr.setCreateRankDesc(userDto.getUserRankDesc());
            hdr.setCreateDate(new Time(now.getTime()));
        }
        hdr.setUpdateUserId(userDto.getLoginId());
        hdr.setUpdateUser(userDto.getLoginName());
        hdr.setUpdateHosp(userDto.getHospitalCd());
        hdr.setUpdateRank(userDto.getUserRankCd());
        hdr.setUpdateRankDesc(userDto.getUserRankDesc());
        hdr.setUpdateDate(new Time(now.getTime()));
        if (isDepartment) {
            hdr.setUserSpecialty(userDto.getSpec());
        }

        if (hdr.getMoeMyFavouriteDetails() != null) {
            for (MoeMyFavouriteDetailPo detail : hdr.getMoeMyFavouriteDetails()) {
                if (detail.getCreateUserId() == null) {
                    detail.setCreateUserId(userDto.getLoginId());
                    detail.setCreateUser(userDto.getLoginName());
                    detail.setCreateHosp(userDto.getHospitalCd());
                    detail.setCreateRank(userDto.getUserRankCd());
                    detail.setCreateRankDesc(userDto.getUserRankDesc());

                    detail.setUpdateUserId(userDto.getLoginId());
                    detail.setUpdateUser(userDto.getLoginName());
                    detail.setUpdateHosp(userDto.getHospitalCd());
                    detail.setUpdateRank(userDto.getUserRankCd());
                    detail.setUpdateRankDesc(userDto.getUserRankDesc());
                }
            }
        }
    }

    private void arrangeMyFavourites(List<MoeMyFavouriteHdrPo> hdrs) {
        if (hdrs != null && hdrs.size() > 0) {
            hdrs.get(0).setFrontMyFavouriteId(ServerConstant.MY_FAVOURITED_HEAD_ID);
            for (int i = 1; i < hdrs.size(); i++) {
                hdrs.get(i).setFrontMyFavouriteId(hdrs.get(i - 1).getMyFavouriteId());
            }
        }
    }

    private void copyFavUserInfo(final MoeMyFavouriteHdrPo header, MoeMyFavouriteHdrPo hdr) {
        hdr.setCreateUserId(header.getCreateUserId());
        hdr.setCreateUser(header.getCreateUser());
        hdr.setCreateHosp(header.getCreateHosp());
        hdr.setCreateRank(header.getCreateRank());
        hdr.setCreateRankDesc(header.getCreateRankDesc());
        hdr.setCreateDate(header.getCreateDate());

        if (hdr.getMoeMyFavouriteDetails() != null) {
            for (MoeMyFavouriteDetailPo detail : hdr.getMoeMyFavouriteDetails()) {
                detail.setCreateUserId(header.getCreateUserId());
                detail.setCreateUser(header.getCreateUser());
                detail.setCreateHosp(header.getCreateHosp());
                detail.setCreateRank(header.getCreateRank());
                detail.setCreateRankDesc(header.getCreateRankDesc());
            }
        }
    }


    @Transactional
    @Override
    // Simon 20190726 start--for sort my favourite only
    public List<MoeMyFavouriteHdrDto> sortMyFavourites(List<MoeMyFavouriteHdrDto> hdrs, final UserDto userDto, boolean isDepartment) throws Exception {
        if (hdrs == null || hdrs.size() == 0
                || StringUtil.stripToEmpty(hdrs.get(0).getCreateUserId()).length() == 0) {
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("param.Invalid"));
            throw new MoeServiceException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 end--
        }
        List<MoeMyFavouriteHdrPo> byUserId = null;
        if (isDepartment) {
            byUserId = moeMyFavouriteHdrRepository.findAllByUserSpecialty(userDto.getSpec());
        } else {
            byUserId = moeMyFavouriteHdrRepository.findByUserId(hdrs.get(0).getCreateUserId());
        }
        if (byUserId == null || byUserId.size() == 0)
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("records.notExisted"));
            throw new MoeServiceException(ResponseCode.SpecialMessage.RECORDS_NOT_EXISTED.getResponseCode(),
                    ResponseCode.SpecialMessage.RECORDS_NOT_EXISTED.getResponseMessage());
           //eric 20191219 end--
        if (byUserId.size() != hdrs.size())
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("param.Invalid"));
            throw new MoeServiceException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        //eric 20191219 end--
        HashMap<String, Long> myFavouriteList = new LinkedHashMap<String, Long>();
        List<Object[]> object = new ArrayList<>();
        if (isDepartment) {
            object = moeMyFavouriteHdrRepository.getDeptMyFavouriteVersionMapping(userDto.getSpec());
        } else {
            object = moeMyFavouriteHdrRepository.getMyFavouriteVersionMapping(hdrs.get(0).getCreateUserId());
        }
        for (Object[] myFavourite : object) {
            myFavouriteList.put((String) myFavourite[0], (Long) myFavourite[1]);
        }

        Set frontFavIdSet = new LinkedHashSet();
        for (MoeMyFavouriteHdrDto hdr : hdrs) {
            if (myFavouriteList.get(hdr.getMyFavouriteId()) == null) {
                //eric 20191219 start--
//                throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.deletedByOthers"));
                throw new MoeServiceException(ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseCode(),
                        ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseMessage());
                //eric 20191219 end--
            }
            if (myFavouriteList.get(hdr.getMyFavouriteId()).longValue() != hdr.getVersion().longValue()) {
                //eric 20191219 start--
//                throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.UpdatedByOthers"));
                throw new MoeServiceException(ResponseCode.SpecialMessage.MY_FAVOURITE_UPDATED_BY_OTHERS.getResponseCode(),
                        ResponseCode.SpecialMessage.MY_FAVOURITE_UPDATED_BY_OTHERS.getResponseMessage());
                //eric 20191219 end--
            }
            frontFavIdSet.add(hdr.getFrontMyFavouriteId());
        }
        //Simon 20190812 --start add for checking same front favourite id
        if (frontFavIdSet.size() != hdrs.size())
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("param.Invalid"));
            throw new MoeServiceException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        //eric 20191219 end--
        //Simon 20190812 --end add for checking same front favourite id
        HashMap<String, MoeMyFavouriteHdrPo> poMap = new LinkedHashMap<>();
        for (MoeMyFavouriteHdrPo po : byUserId) {
            poMap.put(po.getMyFavouriteId(), po);
        }
        for (MoeMyFavouriteHdrDto hdr : hdrs) {
            if (myFavouriteList.get(hdr.getMyFavouriteId()) == null) {
                //eric 20191219 start--
//                throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.deletedByOthers"));
                throw new MoeServiceException(ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseCode(),
                        ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseMessage());
                //eric 20191219 end--
            }
            if (myFavouriteList.get(hdr.getMyFavouriteId()).longValue() != hdr.getVersion().longValue()) {
                //eric 20191219 start--
//                throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.UpdatedByOthers"));
                throw new MoeServiceException(ResponseCode.SpecialMessage.MY_FAVOURITE_UPDATED_BY_OTHERS.getResponseCode(),
                        ResponseCode.SpecialMessage.MY_FAVOURITE_UPDATED_BY_OTHERS.getResponseMessage());
                //eric 20191219 end--
            }
            poMap.get(hdr.getMyFavouriteId()).setFrontMyFavouriteId(hdr.getFrontMyFavouriteId());
        }
        List<MoeMyFavouriteHdrPo> savePos = new ArrayList<>();
        for (Map.Entry<String, MoeMyFavouriteHdrPo> entry : poMap.entrySet()) {
            MoeMyFavouriteHdrPo tempPo = entry.getValue();
            updateFavUserInfo(tempPo, userDto, isDepartment);
            savePos.add(tempPo);
        }
//        arrangeMyFavourites(savePos);
        moeMyFavouriteHdrRepository.saveAll(savePos);
        return hdrs;
    }
    // Simon 20190726 end--for sort myfavourite only

    @Transactional(rollbackFor = Exception.class)
    @Override
    public MoeMyFavouriteHdrDto updateMyFavourite(MoeMyFavouriteHdrDto fav, final UserDto userDto, boolean isDepartment) throws Exception {
        if (fav == null) {
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("param.Invalid"));
            throw new MoeServiceException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 end--
        }
        MoeMyFavouriteHdrPo hdr = DtoMapUtil.convertMoeMyFavouriteHdrForPersistence(fav, false);
        //for update operation
        List<MoeMyFavouriteHdrPo> listById = moeMyFavouriteHdrRepository.getAllByMyFavouriteId(hdr.getMyFavouriteId());
        if (listById == null || listById.size() == 0) {
            //eric 20191219 start--
//                throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.deletedByOthers"));
            throw new MoeServiceException(ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseCode(),
                    ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseMessage());
            //eric 20191219 end--
        }
        MoeMyFavouriteHdrPo header = listById.get(0);
        if (header == null) {
            //eric 20191219 start--
//                throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.deletedByOthers"));
            throw new MoeServiceException(ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseCode(),
                    ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseMessage());
            //eric 20191219 end--
        }
        if (header.getVersion().longValue() != hdr.getVersion().longValue()) {
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.UpdatedByOthers"));
            throw new MoeServiceException(ResponseCode.SpecialMessage.MY_FAVOURITE_UPDATED_BY_OTHERS.getResponseCode(),
                    ResponseCode.SpecialMessage.MY_FAVOURITE_UPDATED_BY_OTHERS.getResponseMessage());
            //eric 20191219 end--
        }
        Set<MoeMyFavouriteDetailPo> moeMyFavouriteDetails = header.getMoeMyFavouriteDetails();
        Iterator<MoeMyFavouriteDetailPo> iterator = hdr.getMoeMyFavouriteDetails().iterator();
        MoeMyFavouriteDetailPo addDetail = null;
        while (iterator.hasNext()) {
            addDetail = iterator.next();
            addDetail.setItemNo(Long.parseLong(Integer.valueOf(moeMyFavouriteDetails.size() + 1).toString()));
            moeMyFavouriteDetails.add(addDetail);
        }
        updateFavUserInfo(header, userDto, isDepartment);
        moeMyFavouriteHdrRepository.save(header);
        String enableHkmttIndicator = SystemSettingUtil.getParamValue("enable_my_favourite_hkmtt_update_indicator");
        Integer updateIndicator = null;
        if (SystemSettingUtil.getParamValue("my_favourite_hkmtt_update_indicator") != null) {
            updateIndicator = Integer.parseInt(SystemSettingUtil.getParamValue("my_favourite_hkmtt_update_indicator"));
        }
        return DtoMapUtil.convertMoeMyFavouriteHdrDto(hdr, enableHkmttIndicator, updateIndicator, userDto);
    }

    // Ricci 20191108 start -- for isDepartment
    @Override
    @Transactional(rollbackFor = Exception.class)
    public MoeMyFavouriteHdrPo saveDepartFavourite(MoeMyFavouriteHdrDto saveDto, UserDto user) throws Exception {
        Date now = new Date();
        if (StringUtils.isNotBlank(saveDto.getMyFavouriteId())) {
            MoeMyFavouriteHdrPo hdrUserInfo = moeMyFavouriteHdrRepository.findCreateInfoByMyFavouriteId(saveDto.getMyFavouriteId());
            Set<MoeMyFavouriteDetailPo> detailUserInfoList = moeMyFavouriteDetailRepository.findCreateInfoByMyFavouriteId(saveDto.getMyFavouriteId());
            if (hdrUserInfo != null) {
                hdrUserInfo.setMoeMyFavouriteDetails(detailUserInfoList);
                resumeDepartFav(saveDto, hdrUserInfo, user, now);
            } else {
                //eric 20191219 start--
//                throw new ParamException(localeMessageSourceUtil.getMessage("myfavourite.deletedByOthers"));
                throw new ParamException(ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseCode(),
                        ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseMessage());
                //eric 20191219 end--
            }
        } else {
            // check if has other record
            List<MoeMyFavouriteHdrPo> sortList = moeMyFavouriteHdrRepository.findAllByUserSpecialty(user.getSpec());
            if (sortList != null && sortList.size() > 0) {
                try {
                    sortList = sortMyFavourites(sortList);
                } catch (Exception e) {
                    arrangeMyFavourites(sortList);
                }
                saveDto.setFrontMyFavouriteId(sortList.get(sortList.size() - 1).getMyFavouriteId());
            } else {
                saveDto.setFrontMyFavouriteId(ServerConstant.MY_FAVOURITED_HEAD_ID);
            }
            saveDto.setVersion(null);
            resumeDepartFav(saveDto, null, user, now);
        }
        MoeMyFavouriteHdrPo hdr = DtoMapUtil.convertMoeMyFavouriteHdrForPersistence(saveDto, false);
        hdr.setUserSpecialty(user.getSpec());

        return moeMyFavouriteHdrRepository.save(hdr);
    }
    // Ricci 20191108 end --


    // Ricci 20191110 start --
    private void resumeDepartFav(MoeMyFavouriteHdrDto newHdr, MoeMyFavouriteHdrPo hdrUserInfo, UserDto user, Date now) throws Exception {
        MoeCommonHelper.buildUserInfo(newHdr, hdrUserInfo, user, now);
        List<MoeMedProfileDto> newDetails = newHdr.getMoeMedProfiles();

        if (newDetails != null && newDetails.size() != 0) {
            if (null == hdrUserInfo) {
                for (MoeMedProfileDto newDetail : newDetails) {
                    MoeCommonHelper.buildUserInfo(newDetail.getMoeEhrMedProfile(), user, user, now);
                    newDetail.setOrgItemNo(0L);
                }
            } else {
                if (hdrUserInfo.getMoeMyFavouriteDetails() != null && hdrUserInfo.getMoeMyFavouriteDetails().size() > 0) {
                    // If has update data ... prepare resume
                    MoeMyFavouriteDetailPo resumeDetail;
                    Map<Long, MoeMyFavouriteDetailPo> oldDetailMap = new HashMap<>();
                    for (MoeMyFavouriteDetailPo oldDetail : hdrUserInfo.getMoeMyFavouriteDetails()) {
                        oldDetailMap.put(oldDetail.getItemNo(), oldDetail);
                    }
                    for (MoeMedProfileDto newDetail : newDetails) {
                        if (null == newDetail.getOrgItemNo() || 0L == newDetail.getOrgItemNo().longValue()) {
                            MoeCommonHelper.buildUserInfo(newDetail.getMoeEhrMedProfile(), user, user, now);
                        } else {
                            resumeDetail = oldDetailMap.get(newDetail.getOrgItemNo());
                            if (resumeDetail != null) {
                                MoeCommonHelper.buildUserInfo(newDetail.getMoeEhrMedProfile(), resumeDetail, user, now);
                            } else {
                                //eric 20191219 start--
//                                throw new ParamException(localeMessageSourceUtil.getMessage("param.Invalid"));
                                throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
                                //eric 20191219 end--
                            }
                        }
                    }
                }
            }
        }
    }
    // Ricci 20191110 end --

}
