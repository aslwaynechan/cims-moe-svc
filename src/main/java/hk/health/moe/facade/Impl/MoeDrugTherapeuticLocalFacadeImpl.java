package hk.health.moe.facade.Impl;

import hk.health.moe.common.MoeCommonHelper;
import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.facade.MoeDrugTherapeuticLocalFacade;
import hk.health.moe.pojo.dto.MoeDrugTherapeuticLocalDto;
import hk.health.moe.pojo.dto.inner.InnerSaveDrugDto;
import hk.health.moe.pojo.po.MoeDrugTherapeuticLocalLogPo;
import hk.health.moe.pojo.po.MoeDrugTherapeuticLocalPo;
import hk.health.moe.pojo.po.MoeDrugTherapeuticLocalPoPK;
import hk.health.moe.repository.MoeDrugTherapeuticLocalLogRepository;
import hk.health.moe.repository.MoeDrugTherapeuticLocalRepository;
import hk.health.moe.util.Formatter;
import hk.health.moe.util.SystemSettingUtil;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class MoeDrugTherapeuticLocalFacadeImpl implements MoeDrugTherapeuticLocalFacade {

    @Autowired
    private MoeDrugTherapeuticLocalRepository moeDrugTherapeuticLocalRepository;

    @Autowired
    private MoeDrugTherapeuticLocalLogRepository moeDrugTherapeuticLocalLogRepository;

    @Autowired
    private DozerBeanMapper mapper;

    @Override
    public void saveDrugByTherapeuticLocal(InnerSaveDrugDto dto) throws Exception {
        String enableLocalTherapeuticClass = SystemSettingUtil.getParamValue(ServerConstant.PARAM_NAME_ENABLE_LOCAL_THERAPEUTIC_CLASS);

        if (enableLocalTherapeuticClass != null && ServerConstant.DB_FLAG_TRUE.equalsIgnoreCase(enableLocalTherapeuticClass)) {
            String localDrugId = dto.getLocalDrugId();
            List<MoeDrugTherapeuticLocalDto> moeDrugTherapeuticLocalDtoList = dto.getMoeTherapeuticLocals();

            List<MoeDrugTherapeuticLocalPo> moeDrugTherapeuticLocalList = moeDrugTherapeuticLocalRepository.findByLocalDrugIdOrderByRank(localDrugId);
            if (moeDrugTherapeuticLocalList != null && !moeDrugTherapeuticLocalList.isEmpty()) {
                for (Iterator<MoeDrugTherapeuticLocalPo> therapeuticLocalIter = moeDrugTherapeuticLocalList.listIterator(); therapeuticLocalIter.hasNext(); ) {
                    MoeDrugTherapeuticLocalPo moeDrugTherapeuticLocal = therapeuticLocalIter.next();

                    for (Iterator<MoeDrugTherapeuticLocalDto> therapeuticLocalDtoIter = moeDrugTherapeuticLocalDtoList.listIterator(); therapeuticLocalDtoIter.hasNext(); ) {
                        MoeDrugTherapeuticLocalDto moeDrugTherapeuticLocalDto = therapeuticLocalDtoIter.next();

                        if (moeDrugTherapeuticLocal.getRank().equals(moeDrugTherapeuticLocalDto.getRank())) {
                            if ((MoeCommonHelper.isSame(moeDrugTherapeuticLocal.getTherapeuticClassCode(), moeDrugTherapeuticLocalDto.getTherapeuticClassCode())) &&
                                    (MoeCommonHelper.isSame(moeDrugTherapeuticLocal.getTherapeuticClassDescription(), moeDrugTherapeuticLocalDto.getTherapeuticClassDescription()))) {
                                // Same Therapeutic Code and Desc
                            } else {
                                // Diff Therapeutic Code and Desc
                                // Update
                                MoeDrugTherapeuticLocalPo updatedMoeDrugTherapeuticLocal = mapper.map(moeDrugTherapeuticLocalDto, MoeDrugTherapeuticLocalPo.class);
                                upsertMoeDrugTherapeuticLocal(updatedMoeDrugTherapeuticLocal);
                            }

                            therapeuticLocalIter.remove();
                            therapeuticLocalDtoIter.remove();
                        }
                    }
                }

                if (moeDrugTherapeuticLocalList != null && !moeDrugTherapeuticLocalList.isEmpty()) {
                    // Delete
                    for (int i = 0; i < moeDrugTherapeuticLocalList.size(); i++) {
                        MoeDrugTherapeuticLocalPo moeDrugTherapeuticLocal = moeDrugTherapeuticLocalList.get(i);
                        deleteMoeDrugTherapeuticLocal(moeDrugTherapeuticLocal);
                    }
                }

                if (moeDrugTherapeuticLocalDtoList != null && !moeDrugTherapeuticLocalDtoList.isEmpty()) {
                    // New
                    for (int i = 0; i < moeDrugTherapeuticLocalDtoList.size(); i++) {
                        MoeDrugTherapeuticLocalDto moeDrugTherapeuticLocalDto = moeDrugTherapeuticLocalDtoList.get(i);

                        MoeDrugTherapeuticLocalPo moeDrugTherapeuticLocal = mapper.map(moeDrugTherapeuticLocalDto, MoeDrugTherapeuticLocalPo.class);
                        upsertMoeDrugTherapeuticLocal(moeDrugTherapeuticLocal);
                    }
                }
            } else {
                // New
                if (moeDrugTherapeuticLocalDtoList != null && !moeDrugTherapeuticLocalDtoList.isEmpty()) {
                    for (int i = 0; i < moeDrugTherapeuticLocalDtoList.size(); i++) {
                        MoeDrugTherapeuticLocalDto moeDrugTherapeuticLocalDto = moeDrugTherapeuticLocalDtoList.get(i);

                        MoeDrugTherapeuticLocalPo moeDrugTherapeuticLocal = mapper.map(moeDrugTherapeuticLocalDto, MoeDrugTherapeuticLocalPo.class);
                        upsertMoeDrugTherapeuticLocal(moeDrugTherapeuticLocal);
                    }
                }
            }
        } else {
            return;
        }
    }

    private void upsertMoeDrugTherapeuticLocal(MoeDrugTherapeuticLocalPo record) throws Exception {
        String localDrugId = null;
        Long rank = -1L;

        if (record != null && record.getLocalDrugId() != null && record.getRank() != null) {
            localDrugId = record.getLocalDrugId();
            rank = record.getRank();
        }

        if (StringUtils.isEmpty(localDrugId) || (rank == -1)) {
            //eric 20191223 start--
//            throw new MoeServiceException("Data Problem(MoeDrugTherapeuticLocalId(" + localDrugId + "," + rank + ") is NULL)");
            throw new MoeServiceException(ResponseCode.SpecialMessage.DATA_PROBLEM.getResponseCode(),
                    ResponseCode.SpecialMessage.DATA_PROBLEM.getResponseMessage()+"(MoeDrugTherapeuticLocalId(" + localDrugId + "," + rank + ") is NULL)");
            //eric 20191223 end--
        }

        String therapeuticClassCode = record.getTherapeuticClassCode();
        String therapeuticClassDescription = record.getTherapeuticClassDescription();

        if (StringUtils.isEmpty(therapeuticClassCode) || StringUtils.isEmpty(therapeuticClassDescription)) {
            //eric 20191223 start--
//            throw new MoeServiceException("Data Problem(therapeuticClass is NULL)");
            throw new MoeServiceException(ResponseCode.SpecialMessage.DATA_PROBLEM.getResponseCode(),
                    ResponseCode.SpecialMessage.DATA_PROBLEM.getResponseMessage()+"(therapeuticClass is NULL)");
            //eric 20191223 end--
        }

        MoeDrugTherapeuticLocalPoPK moeDrugTherapeuticLocalId = new MoeDrugTherapeuticLocalPoPK();
        moeDrugTherapeuticLocalId.setLocalDrugId(localDrugId);
        moeDrugTherapeuticLocalId.setRank(rank);

        MoeDrugTherapeuticLocalPo updatedTherapeuticLocal = null;
        Optional<MoeDrugTherapeuticLocalPo> optionalMoeDrugTherapeuticLocalPo = moeDrugTherapeuticLocalRepository.findById(moeDrugTherapeuticLocalId);
        if (optionalMoeDrugTherapeuticLocalPo.isPresent()) {
            updatedTherapeuticLocal = optionalMoeDrugTherapeuticLocalPo.get();
        }
        if (updatedTherapeuticLocal != null) {
            // Update Record
            updatedTherapeuticLocal.setTherapeuticClassCode(therapeuticClassCode);
            updatedTherapeuticLocal.setTherapeuticClassDescription(therapeuticClassDescription);

            updatedTherapeuticLocal.setUpdateUserId(record.getUpdateUserId());
            updatedTherapeuticLocal.setUpdateUser(record.getUpdateUser());
            updatedTherapeuticLocal.setUpdateRank(record.getUpdateRank());
            updatedTherapeuticLocal.setUpdateRankDesc(record.getUpdateRankDesc());
            updatedTherapeuticLocal.setUpdateHosp(record.getUpdateHosp());
            updatedTherapeuticLocal.setUpdateDtm(new Date());

            moeDrugTherapeuticLocalRepository.save(Formatter.emptyToNull(updatedTherapeuticLocal));

            MoeDrugTherapeuticLocalLogPo moeDrugTherapeuticLocalLog = new MoeDrugTherapeuticLocalLogPo(ServerConstant.RECORD_UPDATE, updatedTherapeuticLocal);
            moeDrugTherapeuticLocalLogRepository.save(moeDrugTherapeuticLocalLog);
        } else {
            // New Record
            record.setCreateDtm(new Date());
            record.setUpdateDtm(new Date());

            moeDrugTherapeuticLocalRepository.save(Formatter.emptyToNull(record));

            MoeDrugTherapeuticLocalLogPo moeDrugTherapeuticLocalLog = new MoeDrugTherapeuticLocalLogPo(ServerConstant.RECORD_INSERT, record);
            moeDrugTherapeuticLocalLogRepository.save(moeDrugTherapeuticLocalLog);
        }
    }

    private void deleteMoeDrugTherapeuticLocal(MoeDrugTherapeuticLocalPo record) throws Exception {
        String localDrugId = null;
        Long rank = -1L;

        if (record != null && record.getLocalDrugId() != null && record.getRank() != null) {
            localDrugId = record.getLocalDrugId();
            rank = record.getRank();
        }

        if (StringUtils.isEmpty(localDrugId) || (rank == -1)) {
            //eric 20191223 start--
//            throw new MoeServiceException("Data Problem(MoeDrugTherapeuticLocalId(" + localDrugId + "," + rank + ") is NULL)");
            throw new MoeServiceException(ResponseCode.SpecialMessage.DATA_PROBLEM.getResponseCode(),
                    ResponseCode.SpecialMessage.DATA_PROBLEM.getResponseMessage()+"(MoeDrugTherapeuticLocalId(" + localDrugId + "," + rank + ") is NULL)");
            //eric 20191223 end--
        }

        moeDrugTherapeuticLocalRepository.delete(record);

        MoeDrugTherapeuticLocalLogPo moeDrugTherapeuticLocalLog = new MoeDrugTherapeuticLocalLogPo(ServerConstant.RECORD_DELETE, record);
        moeDrugTherapeuticLocalLogRepository.save(moeDrugTherapeuticLocalLog);
    }

    // Simon 20191014 start--
    @Override
    public boolean deleteDrugByTherapeuticLocal(String localDrugId) throws Exception {
        //No checking on enable_local_therapeutic_class, TherapeuticLocalClass can be deleted when user delete the local_drug

        List<MoeDrugTherapeuticLocalPo> moeDrugTherapeuticLocalList = moeDrugTherapeuticLocalRepository.findByLocalDrugIdOrderByRank(localDrugId);
        if (moeDrugTherapeuticLocalList != null && !moeDrugTherapeuticLocalList.isEmpty()) {
            for (int i = 0; i < moeDrugTherapeuticLocalList.size(); i++) {
                MoeDrugTherapeuticLocalPo moeDrugTherapeuticLocal = moeDrugTherapeuticLocalList.get(i);
                moeDrugTherapeuticLocalRepository.delete(moeDrugTherapeuticLocal);
                moeDrugTherapeuticLocalLogRepository.save(new MoeDrugTherapeuticLocalLogPo(ServerConstant.RECORD_DELETE, moeDrugTherapeuticLocal));
            }
        }

        return true;
    }
    //Simon 20191014 end--

}
