/*********************************************************************
* Copyright (c) eHR HK Limited 2013. 
* All Rights Reserved.
* 
*********************************************************************
* PROJECT NAME    :  eHR Adaptation
*
* MODULE NAME     :  Medication Order Entry (MOE)
*
* PROGRAM NAME    :  AuthenticationFacadeImpl.java
*
* PURPOSE         :  Core business logic
*
**********************************************************************
* VERSION         :   1.01.001
* REF NO          :  
* DATE CREATED    :   7 June 2013
* 
*********************************************************************
*
* CHANGES         :
* REF NO           DATE        DETAIL
* ------           ----        ------
*
*********************************************************************
*/
package hk.health.moe.facade.Impl;

import hk.health.moe.facade.AuthenticationFacade;
import hk.health.moe.pojo.dto.MoeUserSettingDto;
import hk.health.moe.pojo.po.MoeMethodPo;
import hk.health.moe.pojo.po.MoeOperationPo;
import hk.health.moe.pojo.po.MoeUserSettingPo;
import hk.health.moe.repository.MoeMethodRepository;
import hk.health.moe.repository.MoeUserSettingRepository;
import hk.health.moe.util.DtoMapUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service("authenticationFacade")
@Transactional(readOnly=true)
public class AuthenticationFacadeImpl implements AuthenticationFacade {


	@Autowired
	private MoeMethodRepository moeMethodRepository;
	@Autowired
	private MoeUserSettingRepository moeUserSettingRepository;
	@Override
	public Map<String, Collection<ConfigAttribute>> getOperationMethodMap() {
		
		List<MoeMethodPo> list = moeMethodRepository.findAll();
		Map<String, Collection<ConfigAttribute>> methodMap = new HashMap<String, Collection<ConfigAttribute>>();
		for (MoeMethodPo method : list) {
			List<ConfigAttribute> configList = new ArrayList<ConfigAttribute>();
			Set<MoeOperationPo> operationList = method.getOperations();
			for(MoeOperationPo operation : operationList) {
				configList.add(new SecurityConfig(operation.getOperationName()));
			}
			methodMap.put(method.getMethodName(), configList);
		}
		
		return methodMap;
	}
	
	@Override
	public HashMap<String, MoeUserSettingDto> getUserSetting(String loginId) {
		HashMap<String, MoeUserSettingDto> smap = new HashMap<String, MoeUserSettingDto>(1);
		
		List<MoeUserSettingPo> list = moeUserSettingRepository.findByLoginId(loginId);
		if(list != null) {
			for(MoeUserSettingPo s : list) {
				smap.put(s.getParamName(), DtoMapUtil.convertMoeUserSettingDto(s));
			}
		}
		return smap;
	}

}
