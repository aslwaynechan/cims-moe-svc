package hk.health.moe.facade.Impl;

import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.facade.SaDrugLocalFacade;
import hk.health.moe.pojo.dto.MoeDrugLocalDto;
import hk.health.moe.pojo.dto.inner.InnerSaveDrugDto;
import hk.health.moe.pojo.po.SaDrugLocalLogPo;
import hk.health.moe.pojo.po.SaDrugLocalPo;
import hk.health.moe.repository.SaDrugLocalLogRepository;
import hk.health.moe.repository.SaDrugLocalRepository;
import hk.health.moe.util2.DtoMapUtil;
import hk.health.moe.util.LocaleMessageSourceUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class SaDrugLocalFacadeImpl implements SaDrugLocalFacade {

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    @Autowired
    private SaDrugLocalRepository saDrugLocalRepository;

    @Autowired
    private SaDrugLocalLogRepository saDrugLocalLogRepository;

    @Override
    public void saveDrugDetail(InnerSaveDrugDto dto) throws Exception {
        if (dto == null) {
            return;
        }

        // MoeDrugLocal
        MoeDrugLocalDto moeDrugLocalDto = dto.getMoeDrugLocal();
        SaDrugLocalPo saDrugLocal = null;
        if (moeDrugLocalDto != null) {
            saDrugLocal = DtoMapUtil.convertSaDrugLocal(moeDrugLocalDto);
            if (saDrugLocal.getHkRegNo().length() > 10) {
                saDrugLocal.setHkRegNo(null);
            }
            if (dto.isAddToSAAM()) {
                upsertMoeDrugLocal(saDrugLocal);
            } else {
                deleteDrugDetail(moeDrugLocalDto.getLocalDrugId());
            }
        }
    }

    private String upsertMoeDrugLocal(SaDrugLocalPo record) throws Exception {

        if (record == null) {
            //eric 20191227 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.error"));
            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage());
            //eric 20191227 end--
        }

        String localDrugId = record.getAllergenId();
        if (StringUtils.isEmpty(localDrugId)) {
            //eric 20191227 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.error"));
            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage());
            //eric 20191227 end--
        }

        SaDrugLocalPo saDrugLocal = null;
        Optional<SaDrugLocalPo> optionalSaDrugLocalPo = saDrugLocalRepository.findById(localDrugId);
        if (optionalSaDrugLocalPo.isPresent()) {
            saDrugLocal = optionalSaDrugLocalPo.get();
        }

        if (saDrugLocal != null) {
            // Update Current Record

            // Trade Name
            saDrugLocal.setTradeName(record.getTradeName());

            // Generic Name
            saDrugLocal.setVtm(record.getVtm());

            // FormerBan
            saDrugLocal.setFormerBan(record.getFormerBan());

            // Abbreviation
            saDrugLocal.setAbbreviation(record.getAbbreviation());

            // Other Name
            saDrugLocal.setOtherName(record.getOtherName());

            // GenericProductIndicator
            saDrugLocal.setGenericProductInd(record.getGenericProductInd());

            // Update Date
            saDrugLocal.setUpdateDtm(new Date());

            saDrugLocalLogRepository.save(new SaDrugLocalLogPo(ServerConstant.RECORD_UPDATE, saDrugLocal));

            return saDrugLocal.getHkRegNo();
        } else {

            // New Record
/*			String hkRegNo = record.getHkRegNo();
			if (StringUtils.isEmpty(hkRegNo)) {
				hkRegNo = UUID.randomUUID().toString();
				hkRegNo = StringUtils.remove(hkRegNo, "-");
				record.setHkRegNo(hkRegNo);
			}
*/
            // Update Date
            record.setUpdateDtm(new Date());

            saDrugLocalRepository.save(record);
            saDrugLocalLogRepository.save(new SaDrugLocalLogPo(ServerConstant.RECORD_INSERT, record));

            return "";

        }
    }

    @Override
    public void deleteDrugDetail(String localDrudId) throws Exception {
        Optional<SaDrugLocalPo> optionalSaDrugLocalPo = saDrugLocalRepository.findById(localDrudId);
        SaDrugLocalPo saDrugLocalToDelete = null;
        if (optionalSaDrugLocalPo.isPresent()) {
            saDrugLocalToDelete = optionalSaDrugLocalPo.get();
        }
        if (saDrugLocalToDelete != null) {
            saDrugLocalRepository.delete(saDrugLocalToDelete);
            saDrugLocalLogRepository.save(new SaDrugLocalLogPo(ServerConstant.RECORD_DELETE, saDrugLocalToDelete));
        }
    }

}
