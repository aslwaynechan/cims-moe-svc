package hk.health.moe.facade.Impl;

import hk.health.moe.common.DrugType;
import hk.health.moe.common.MoeCommonHelper;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.facade.MoeContentFacade;
import hk.health.moe.pojo.dto.MoeActionStatusDto;
import hk.health.moe.pojo.dto.MoeDrugCategoryDto;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.MoeDrugGenericNameLocalDto;
import hk.health.moe.pojo.dto.MoeDrugOrderPropertyDto;
import hk.health.moe.pojo.dto.MoeHospitalSettingDto;
import hk.health.moe.pojo.dto.MoeRegimenUnitDto;
import hk.health.moe.pojo.dto.MoeSiteDto;
import hk.health.moe.pojo.dto.MoeSupplFreqDto;
import hk.health.moe.pojo.dto.MoeSystemSettingDto;
import hk.health.moe.pojo.dto.PreparationDto;
import hk.health.moe.pojo.po.MoeActionStatusPo;
import hk.health.moe.pojo.po.MoeBaseUnitPo;
import hk.health.moe.pojo.po.MoeBaseUnitTermIdMapPo;
import hk.health.moe.pojo.po.MoeClinicalAlertRulePo;
import hk.health.moe.pojo.po.MoeCommonDosageTypePo;
import hk.health.moe.pojo.po.MoeConjunctionTermIdMapPo;
import hk.health.moe.pojo.po.MoeDoseFormExtraInfoPo;
import hk.health.moe.pojo.po.MoeDrugAliasNameLocalPo;
import hk.health.moe.pojo.po.MoeDrugBySpecialtyPo;
import hk.health.moe.pojo.po.MoeDrugCategoryPo;
import hk.health.moe.pojo.po.MoeDrugGenericNameLocalPo;
import hk.health.moe.pojo.po.MoeDrugLocalPo;
import hk.health.moe.pojo.po.MoeDrugStrengthLocalPo;
import hk.health.moe.pojo.po.MoeDrugStrengthPo;
import hk.health.moe.pojo.po.MoeDurationTermIdMapPo;
import hk.health.moe.pojo.po.MoeDurationUnitMultiplierPo;
import hk.health.moe.pojo.po.MoeFormPo;
import hk.health.moe.pojo.po.MoeFormRouteMapPo;
import hk.health.moe.pojo.po.MoeFreqLocalPo;
import hk.health.moe.pojo.po.MoeFreqPo;
import hk.health.moe.pojo.po.MoeFreqTermIdMapPo;
import hk.health.moe.pojo.po.MoeHospitalSettingPo;
import hk.health.moe.pojo.po.MoeLegalClassPo;
import hk.health.moe.pojo.po.MoePrescribeUnitTermIdMapPo;
import hk.health.moe.pojo.po.MoeRegimenPo;
import hk.health.moe.pojo.po.MoeRegimenUnitPo;
import hk.health.moe.pojo.po.MoeRoutePo;
import hk.health.moe.pojo.po.MoeRouteSiteMapPo;
import hk.health.moe.pojo.po.MoeRouteTermIdMapPo;
import hk.health.moe.pojo.po.MoeSitePo;
import hk.health.moe.pojo.po.MoeSiteTermIdMapPo;
import hk.health.moe.pojo.po.MoeSupplFreqPo;
import hk.health.moe.pojo.po.MoeSupplFreqTermIdMapPo;
import hk.health.moe.pojo.po.MoeSystemSettingPo;
import hk.health.moe.repository.BaseRepository;
import hk.health.moe.repository.MoeActionStatusRepository;
import hk.health.moe.repository.MoeBaseUnitRepository;
import hk.health.moe.repository.MoeClinicalAlertRuleRepository;
import hk.health.moe.repository.MoeCommonDosageTypeRepository;
import hk.health.moe.repository.MoeDoseFormExtraInfoRepository;
import hk.health.moe.repository.MoeDrugAliasNameLocalRepository;
import hk.health.moe.repository.MoeDrugBySpecialtyRepository;
import hk.health.moe.repository.MoeDrugCategoryRepository;
import hk.health.moe.repository.MoeDrugGenericNameLocalRepository;
import hk.health.moe.repository.MoeDrugLocalRepository;
import hk.health.moe.repository.MoeDrugOrdPropertyLocalRepository;
import hk.health.moe.repository.MoeDrugRepository;
import hk.health.moe.repository.MoeDrugStrengthLocalRepository;
import hk.health.moe.repository.MoeDrugStrengthRepository;
import hk.health.moe.repository.MoeDurationUnitMultiplierRepository;
import hk.health.moe.repository.MoeFormRepository;
import hk.health.moe.repository.MoeFormRouteMapRepository;
import hk.health.moe.repository.MoeFreqLocalRepository;
import hk.health.moe.repository.MoeFreqRepository;
import hk.health.moe.repository.MoeLegalClassRepository;
import hk.health.moe.repository.MoeRegimenRepository;
import hk.health.moe.repository.MoeRegimenUnitRepository;
import hk.health.moe.repository.MoeRouteRepository;
import hk.health.moe.repository.MoeRouteSiteMapRepository;
import hk.health.moe.repository.MoeSiteRepository;
import hk.health.moe.repository.MoeSupplFreqRepository;
import hk.health.moe.repository.SystemSettingRepository;
import hk.health.moe.util.DrugSearchEngine;
import hk.health.moe.util.DtoMapUtil;
import hk.health.moe.util.DtoUtil;
import hk.health.moe.util.Formatter;
import hk.health.moe.util.SystemSettingUtil;
import hk.health.moe.util.TempDtoUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Service
public class MoeContentFacadeImpl implements MoeContentFacade {

    @Autowired
    private MoeDrugLocalRepository moeDrugLocalRepository;

    @Autowired
    private MoeDrugRepository moeDrugRepository;

    @Autowired
    private MoeFreqLocalRepository moeFreqLocalRepository;

    @Autowired
    private MoeFreqRepository moeFreqRepository;

    @Autowired
    private MoeDrugStrengthLocalRepository moeDrugStrengthLocalRepository;

    @Autowired
    private MoeSupplFreqRepository moeSupplFreqRepository;

    @Autowired
    private MoeFormRouteMapRepository moeFormRouteMapRepository;

    @Autowired
    private MoeDrugAliasNameLocalRepository moeDrugAliasNameLocalRepository;

    @Autowired
    private MoeActionStatusRepository moeActionStatusRepository;

    @Autowired
    private MoeSiteRepository moeSiteRepository;

    @Autowired
    private MoeRouteRepository moeRouteRepository;

    @Autowired
    private MoeRegimenRepository moeRegimenRepository;

    @Autowired
    private MoeRegimenUnitRepository moeRegimenUnitRepository;

    @Autowired
    private MoeRouteSiteMapRepository moeRouteSiteMapRepository;

    @Autowired
    private MoeBaseUnitRepository moeBaseUnitRepository;

    @Autowired
    private MoeFormRepository moeFormRepository;

    @Autowired
    private MoeDrugOrdPropertyLocalRepository moeDrugOrdPropertyLocalRepository;

    @Autowired
    private MoeClinicalAlertRuleRepository moeClinicalAlertRuleRepository;

    @Autowired
    private MoeDurationUnitMultiplierRepository moeDurationUnitMultiplierRepository;

    @Autowired
    private MoeDoseFormExtraInfoRepository moeDoseFormExtraInfoRepository;

    @Autowired
    private MoeDrugBySpecialtyRepository moeDrugBySpecialtyRepository;

    @Autowired
    private MoeLegalClassRepository moeLegalClassRepository;

    @Autowired
    private MoeDrugCategoryRepository moeDrugCategoryRepository;

    @Autowired
    private BaseRepository baseRepository;

    @Autowired
    private MoeCommonDosageTypeRepository moeCommonDosageTypeRepository;

    @Autowired
    private SystemSettingRepository systemSettingRepository;

    //Chris 20200110  -Start
    @Autowired
    private MoeDrugGenericNameLocalRepository moeDrugGenericNameLocalRepository;

    @Autowired
    private MoeDrugStrengthRepository moeDrugStrengthRepository;
    //Chris 20200110  -False

    @Override
    @Transactional(readOnly = true)
    public void setContent() throws Exception {
        setStrengthContent();
        setDuplicateBasicNames();
        setFreqContent();
        setActionStatusContent();
        setSupplFreqContent();
        setSiteContent();
        setRouteContent();
        setRegimenContent();
        setRegimenUnitContent();
        setFormRouteMapContent();
        setRouteSiteMapContent();
        setBaseUnit();
        setForm();
        setDurationUnitContent();
        setMoeClinicalAlertRuleContent();
        setMoeDrugOrderPropertyContent();
        setDoseFormExtraInfoContent();
        setDrugBySpecialtyContent();
        setLegalClass();
        setDrugContent();
        setDrugCategoryContent();
        setTermIdMap();
        setSystemSetting();
        setHospitalSetting();
        // Ricci 20190822 for moe_drug_server start --
        setCommonDosageType();
        // Ricci 20190822 for moe_drug_server end --
        //setLogPath();
        //setSystemMessage();

        //Chris 20200110  Set Parent Drug of Generic Name Level  -Start
        setGenericNameContent();
        //Chris 20200110  Set Parent Drug of Generic Name Level  -End
    }

    @Override
    @Transactional(readOnly = true)
    public void setTempContent() throws Exception {
        setTempStrengthContent();
        setTempDuplicateBasicNames();
        setTempFreqContent();
        setTempActionStatusContent();
        setTempSupplFreqContent();
        setTempSiteContent();
        setTempRouteContent();
        setTempRegimenContent();
        setTempRegimenUnitContent();
        setTempFormRouteMapContent();
        setTempRouteSiteMapContent();
        setTempBaseUnit();
        setTempForm();
        setTempDurationUnitContent();
        setTempMoeClinicalAlertRuleContent();
        setTempMoeDrugOrderPropertyContent();
        setTempDoseFormExtraInfoContent();
        setTempDrugBySpecialtyContent();
        setTempLegalClass();
        setTempDrugContent();
        setTempDrugCategoryContent();
        setTempTermIdMap();

        // Ricci 20190823 moe_drug_server start --
        setTempCommonDosageType();
        // Ricci 20190823 moe_drug_server end --

        //Chris 20200110  Set Parent Drug of Generic Name Level  -Start
        setTempGenericNameContent();
        //Chris 20200110  Set Parent Drug of Generic Name Level  -End
    }

    @Override
    @Transactional(readOnly = true)
    public void refreshSystemSetting() throws Exception {
        setSystemSetting();
        setHospitalSetting();
        //setLogPath();
        //setSystemMessage();
    }

    // Ricci 20190722 start --

    /**
     * for setContent() --
     */
    private void setStrengthContent() {
        List<Object[]> list = moeDrugStrengthLocalRepository.getAllNonStrengthCompulsory();
        addPreparationContent(list, true);
    }

    private void setDuplicateBasicNames() {
        if (!DtoUtil.DUP_BASIC_NAMES.isEmpty()) return;
        List<Object[]> results = moeFreqLocalRepository.getAllTradeNameAliasNamePairs(ServerConstant.ALIAS_NAME_TYPE_ABB, ServerConstant.ALIAS_NAME_TYPE_OTHER);
        if (results != null) {
            Map<String, String> map = new HashMap<String, String>();
            for (Object[] c : results) { // c0 - trade name, c1 - other name/abbreviation, c2 - vtm
                String tradeName = String.valueOf(c[0]);
                String alias = String.valueOf(c[1]);
                String vtm2 = String.valueOf(c[2]);
                String vtm = map.get(tradeName + alias);
                if (vtm != null && !vtm.equalsIgnoreCase(vtm2)) {
                    DtoUtil.DUP_BASIC_NAMES.add(tradeName + alias);
                } else if (vtm == null) {
                    map.put(tradeName + alias, vtm2);
                }
            }
        }
    }

    private void setFreqContent() {

        if (DtoUtil.FREQS_LOCAL.isEmpty()) {
            List<MoeFreqLocalPo> list = moeFreqLocalRepository.getAllOrderByRank();
            int orderCount = 1;
            for (MoeFreqLocalPo freq : list) {
                DtoUtil.FREQS_LOCAL.add(DtoMapUtil.convertMoeFreqDto(freq));
                DtoUtil.FREQ_ORDER.put(freq.getFreqEng(), orderCount++);
            }
        }

        if (DtoUtil.FREQS.isEmpty()) {
            List<MoeFreqPo> list = moeFreqRepository.findAll();
            for (MoeFreqPo freq : list) {
                DtoUtil.FREQS.add(DtoMapUtil.convertMoeFreqDto(freq));
                DtoUtil.FREQ_ID_MAP.put(freq.getFreqId(), freq);
            }
        }
    }

    private void setSupplFreqContent() {

        if (DtoUtil.REGIMEN_TYPE_MAP.size() == 0) {
            List<MoeSupplFreqPo> list = moeSupplFreqRepository.getAllOrderByTypeAndRank();
            List<MoeSupplFreqDto> supplFreqList = new ArrayList<MoeSupplFreqDto>();

            String prevRegimenType = "-";
            String currentRegimenType = "-";
            int count = 1;

            for (MoeSupplFreqPo supplFreq : list) {
                if (count != 1) {
                    currentRegimenType = supplFreq.getMoeRegimen().getRegimenType();
                }
                if (prevRegimenType.equalsIgnoreCase(currentRegimenType)) {
                    supplFreqList.add(DtoMapUtil.convertMoeSupplFreqDto(supplFreq));
                }
                if (!prevRegimenType.equalsIgnoreCase(currentRegimenType)) {
                    DtoUtil.REGIMEN_TYPE_MAP.put(prevRegimenType, supplFreqList);
                    supplFreqList = new ArrayList<MoeSupplFreqDto>();
                    supplFreqList.add(DtoMapUtil.convertMoeSupplFreqDto(supplFreq)); // add back the different one
                }
                prevRegimenType = supplFreq.getMoeRegimen().getRegimenType();
                count++;
            }
            if (list.size() > 0 && DtoUtil.REGIMEN_TYPE_MAP.get(currentRegimenType) == null) {
                DtoUtil.REGIMEN_TYPE_MAP.put(currentRegimenType, supplFreqList);
            }
        }
    }

    private void setFormRouteMapContent() {
        if (!DtoUtil.FORM_ROUTE_MAP.isEmpty()) return;

        List<MoeFormRouteMapPo> list = moeFormRouteMapRepository.findAll();
        for (MoeFormRouteMapPo fr : list) {
            String key = fr.getMoeForm().getFormEng() + " " + fr.getMoeRoute().getRouteEng();
            DtoUtil.FORM_ROUTE_MAP.put(key, fr.getDisplayRoute());
        }
    }

    private void setDrugContent() {

        List<MoeDrugLocalPo> locals = moeDrugLocalRepository.getAllDistinctBasicAttributes();
        HashMap<String, HashSet<MoeDrugAliasNameLocalPo>> map = new HashMap<>();
        List<MoeDrugAliasNameLocalPo> aliasNames = moeDrugAliasNameLocalRepository.findAll();
        for (MoeDrugAliasNameLocalPo alias : aliasNames) {
            HashSet<MoeDrugAliasNameLocalPo> set = map.get(alias.getHkRegNo());
            if (set == null) {
                set = new HashSet<>();
            }
            set.add(alias);
            map.put(alias.getHkRegNo(), set);
        }

        for (MoeDrugLocalPo local : locals) {
            HashSet<MoeDrugAliasNameLocalPo> set = map.get(local.getHkRegNo());
            if (set != null) {
                local.setMoeDrugAliasNames(set);
            } else {
                local.setMoeDrugAliasNames(new HashSet<>());
            }
        }

        List<String> suspendDrugs = moeDrugLocalRepository.getAllSuspendedDrugs("Y");
        for (MoeDrugLocalPo drug : locals) {
            for (MoeDrugStrengthLocalPo strength : drug.getMoeDrugStrengths()) {
                if (suspendDrugs.contains(drug.getLocalDrugId())) {
                    addSuspendDrug(drug, strength);
                } else {
                    addExistDrug(drug, strength);

                    //Chris 20200113  For Supplement VmpId from moe_drug_strength  -Start
                    MoeDrugStrengthPo moeDrugStrengthPo = null;
                    if (strength.getVmpId() == null || strength.getVmpId() == 0L) {
                        if (StringUtils.isNotBlank(drug.getHkRegNo()) && strength.getAmpId() != null && strength.getAmpId() != 0L) {
                            moeDrugStrengthPo = moeDrugStrengthRepository.findByHkRegNoAndAmpId(drug.getHkRegNo(), strength.getAmpId());
                        }
                    }
                    //List<MoeDrugDto> dtos = DtoMapUtil.convertMoeDrugDTOWithAllStrengths(drug, strength);
                    List<MoeDrugDto> dtos = DtoMapUtil.convertMoeDrugDTOWithAllStrengths(drug, strength, moeDrugStrengthPo);
                    //Chris 20200113  For Supplement VmpId from moe_drug_strength  -End
                    addParentDrugs(dtos);

                    if (drug.getMoeCommonDosages() != null) {
                        dtos = DtoMapUtil.convertMoeDrugDTOWithAllCommonDosage(drug, strength);
                        addDosageDrugs(dtos);
                    }
                }
            }
        }
        sortDrugContent();

        if (DtoUtil.DRUGS_BY_HKREGNO.size() > 0) {
            return;
        }
        List<MoeDrugDto> drugList = moeDrugRepository.findHkRegNoAndVtm();
        for (MoeDrugDto drug : drugList) {
            DtoUtil.DRUGS_BY_HKREGNO.put(drug.getHkRegNo(), drug.getVtm());
        }

    }

    private void setActionStatusContent() {

        if (DtoUtil.ACTIONS_STATUS_LIST.isEmpty()) {
            List<MoeActionStatusPo> list = moeActionStatusRepository.getAllOrderByRank();
            for (MoeActionStatusPo actionStatus : list) {
                DtoUtil.ACTIONS_STATUS_LIST.add(DtoMapUtil.convertMoeActionStatusDto(actionStatus));
            }
            for (MoeActionStatusDto moeActionStatusDto : DtoUtil.ACTIONS_STATUS_LIST) {
                DtoUtil.ACTIONS_STATUS_MAP.put(moeActionStatusDto.getActionStatusType().toUpperCase(), moeActionStatusDto);
            }

        }
    }

    private void setSiteContent() {
        if (DtoUtil.ROUTE_MAP.size() == 0) {
            List<MoeSitePo> list = moeSiteRepository.getAllOrderByRankAndSite();

            for (MoeSitePo site : list) {
                MoeSiteDto dto = DtoMapUtil.convertMoeSiteDto(site);
                DtoUtil.SITE_LIST.add(dto);
            }
        }
    }

    private void setRouteContent() {
        if (DtoUtil.ROUTE_LIST.isEmpty()) {
            List<MoeRoutePo> list = moeRouteRepository.getAllOrderByRank();
            for (MoeRoutePo route : list) {
                DtoUtil.ROUTE_LIST.add(DtoMapUtil.convertMoeRouteDto(route));
                DtoUtil.ROUTE_ID_MAP.put(route.getRouteId(), route);
            }
        }
    }

    private void setRegimenContent() {
        if (DtoUtil.REGIMENT_LIST.isEmpty()) {
            List<MoeRegimenPo> list = moeRegimenRepository.getAllOrderByRank();
            for (MoeRegimenPo regimen : list) {
                DtoUtil.REGIMENT_LIST.add(DtoMapUtil.convertMoeRegimenDto(regimen));
                DtoUtil.REGIMEN_ID_MAP.put(regimen.getRegimenType(), regimen);
            }
        }
    }

    private void setRegimenUnitContent() {
        if (DtoUtil.REGIMENT_UNIT_MAP.isEmpty()) {
            List<MoeRegimenUnitPo> list = moeRegimenUnitRepository.getAllOrderByRank();
            for (MoeRegimenUnitPo unit : list) {
                List<MoeRegimenUnitDto> dtos = DtoUtil.REGIMENT_UNIT_MAP.get(unit.getRegimenType());
                if (dtos == null) {
                    dtos = new ArrayList<MoeRegimenUnitDto>();
                }
                dtos.add(DtoMapUtil.convertMoeRegimenUnitDto(unit));
                DtoUtil.REGIMENT_UNIT_MAP.put(unit.getRegimenType(), dtos);
            }
        }
    }

    private void setRouteSiteMapContent() {
        if (!DtoUtil.ROUTE_SITE_MAP.isEmpty()) return;

        List<MoeRouteSiteMapPo> list = moeRouteSiteMapRepository.findAllOrderByRouteAndRank();
        for (MoeRouteSiteMapPo rs : list) {
            String key = rs.getMoeRoute().getRouteEng() + " " + rs.getMoeSite().getSiteEng();
            DtoUtil.ROUTE_SITE_MAP.put(key, rs.getDisplayRoute());

            List<MoeSiteDto> siteList = DtoUtil.ROUTE_MAP.get(rs.getMoeRoute().getRouteEng());
            if (siteList == null) {
                siteList = new ArrayList<MoeSiteDto>();
            }
            siteList.add(DtoMapUtil.convertMoeSiteDto(rs.getMoeSite()));
            DtoUtil.ROUTE_MAP.put(rs.getMoeRoute().getRouteEng(), siteList);
        }
    }

    private void setBaseUnit() {
        if (DtoUtil.BASE_UNIT_LIST.isEmpty()) {
            List<MoeBaseUnitPo> list = moeBaseUnitRepository.findAllBaseUnit(ServerConstant.BASE_UNIT_TRUE);
            for (MoeBaseUnitPo baseUnit : list) {
                DtoUtil.BASE_UNIT_LIST.add(DtoMapUtil.convertMoeBaseUnitDto(baseUnit));
                if ("dose".equals(baseUnit.getBaseUnit())) {
                    DtoUtil.setBaseUnitDoseId(baseUnit.getBaseUnitId());
                }
            }
        }
        if (DtoUtil.PRESCRIBE_UNIT_LIST.isEmpty()) {
            List<MoeBaseUnitPo> list = moeBaseUnitRepository.findAllPrescribeUnit(ServerConstant.BASE_UNIT_TRUE);
            for (MoeBaseUnitPo baseUnit : list) {
                DtoUtil.PRESCRIBE_UNIT_LIST.add(DtoMapUtil.convertMoeBaseUnitDto(baseUnit));
            }
        }
        if (DtoUtil.DISPENSE_UNIT_LIST.isEmpty()) {
            List<MoeBaseUnitPo> list = moeBaseUnitRepository.findAllDispenseUnit(ServerConstant.BASE_UNIT_TRUE);
            for (MoeBaseUnitPo baseUnit : list) {
                DtoUtil.DISPENSE_UNIT_LIST.add(DtoMapUtil.convertMoeBaseUnitDto(baseUnit));
            }
        }
    }

    private void setForm() {
        if (DtoUtil.FORM_LIST.isEmpty()) {
            List<MoeFormPo> list = moeFormRepository.findAll();
            for (MoeFormPo form : list) {
                DtoUtil.FORM_LIST.add(DtoMapUtil.convertMoeFormDto(form));
            }
        }
    }

    private void setDurationUnitContent() {
        if (!DtoUtil.DURATION_UNIT_MAP.isEmpty()) return;

        List<MoeDurationUnitMultiplierPo> list = moeDurationUnitMultiplierRepository.findAll();
        for (MoeDurationUnitMultiplierPo du : list) {
            DtoUtil.DURATION_UNIT_MAP.put(du.getDurationUnit(), du.getDurationMultiplier());
            DtoUtil.DURATION_UNIT_DESC_MAP.put(du.getDurationUnit(), du.getDurationUnitDesc());
            // Ricci 20190719 start --
            DtoUtil.DURATION_UNIT_LIST.add(DtoMapUtil.convertMoeDurationUnitDto(du));
            // Ricci 20190719 end --
        }
    }

    private void setMoeClinicalAlertRuleContent() {
        if (!DtoUtil.CLINICAL_ALERT_MAP.isEmpty()) return;

        List<MoeClinicalAlertRulePo> list = moeClinicalAlertRuleRepository.findAll();
        for (MoeClinicalAlertRulePo rule : list) {
            DtoUtil.CLINICAL_ALERT_MAP.put(rule.getVtm(), DtoMapUtil.convertMoeClinicalAlertRuleDto(rule));
        }
    }

    private void setMoeDrugOrderPropertyContent() {
        List<Object[]> list = moeDrugOrdPropertyLocalRepository.getMinDosageAndMaxDurationByVtmRouteForm();
        // strength compulsory, trade name, vtm , route , form, form extra, strength, min, max
        for (Object[] objs : list) {
            String key;
            boolean isMutiIngr = MoeDrugDto.isMultiIngr(String.valueOf(objs[2]));
            boolean isStrengthCompulsory = String.valueOf(objs[0]).equals("Y");
            if (isStrengthCompulsory) {
                StringBuilder sb = new StringBuilder();
                if (objs[1] != null) sb.append(String.valueOf(objs[1]));
                if (objs[2] != null) sb.append(" " + String.valueOf(objs[2]));
                if (objs[3] != null) sb.append(" " + String.valueOf(objs[3]));
                if (objs[4] != null) sb.append(" " + String.valueOf(objs[4]));
                if (objs[5] != null) sb.append(" " + String.valueOf(objs[5]));
                if (objs[6] != null) sb.append(" " + String.valueOf(objs[6]));
                key = sb.toString();
            } else {
                StringBuilder sb = new StringBuilder();
                if (objs[1] != null) sb.append(String.valueOf(objs[1]));
                if (objs[2] != null) sb.append(" " + String.valueOf(objs[2]));
                if (objs[3] != null) sb.append(" " + String.valueOf(objs[3]));
                if (objs[4] != null) sb.append(" " + String.valueOf(objs[4]));
                if (objs[5] != null) sb.append(" " + String.valueOf(objs[5]));
                key = sb.toString();
            }

            Double dosage = null;
            Long duration = null;
            String specifyQuantityFlag = null;
            if (objs[7] != null) {
                dosage = ((BigDecimal) objs[7]).doubleValue();
            }
            if (objs[8] != null) {
                duration = (Long) objs[8];
            }
            if (objs[9] != null) {
                specifyQuantityFlag = (String) objs[9];
            }

            if (!isStrengthCompulsory) {
                if (dosage != null) {
                    List<Double> dosages = DtoUtil.MIN_DOSAGE_MAP.get(key);
                    if (dosages == null) {
                        dosages = new ArrayList<Double>();
                        DtoUtil.MIN_DOSAGE_MAP.put(key, dosages);
                    }
                    if (!dosages.contains(dosage)) {
                        dosages.add(dosage);
                    }
                }
                if (duration != null) {
                    Long maxDuration = DtoUtil.MAX_DURATION_MAP.get(key);
                    if (maxDuration == null || duration < maxDuration) {
                        DtoUtil.MAX_DURATION_MAP.put(key, duration);
                    }
                }
                if (specifyQuantityFlag != null && "Y".equalsIgnoreCase(specifyQuantityFlag)) {
                    DtoUtil.SPECIFY_QUANTITY_FLAG.add(key);
                }
            } else {
                if (isMutiIngr) {
                    DtoUtil.MULT_DRUG_ORDER_PROPERTY_MAP.put(key, new MoeDrugOrderPropertyDto(dosage, duration));
                } else {
                    DtoUtil.SINGLE_DRUG_ORDER_PROPERTY_MAP.put(key, new MoeDrugOrderPropertyDto(dosage, duration));
                }
                if (specifyQuantityFlag != null && "Y".equalsIgnoreCase(specifyQuantityFlag)) {
                    DtoUtil.STRENGTH_COMP_SPECIFY_QUANTITY_FLAG.add(key);
                }
            }
        }
    }

    private void setDoseFormExtraInfoContent() {
        List<MoeDoseFormExtraInfoPo> list = moeDoseFormExtraInfoRepository.findAll();
        for (MoeDoseFormExtraInfoPo info : list) {
            DtoUtil.DOSE_FORM_EXTRA_INFO_LIST.add(info);
        }
    }

    private void setDrugBySpecialtyContent() {
        List<MoeDrugBySpecialtyPo> list = moeDrugBySpecialtyRepository.findAll();
        for (MoeDrugBySpecialtyPo spec : list) {
            String key = spec.getHospcode() + " " + spec.getUserSpecialty();
            List<String> hkregs = DtoUtil.HKREG_BY_SPECIALTY.get(key);
            if (hkregs == null) {
                hkregs = new ArrayList<String>();
            }
            hkregs.add(spec.getLocalDrugId());
            DtoUtil.HKREG_BY_SPECIALTY.put(key, hkregs);
            DtoUtil.EXIST_SPECIALTY_DRUGS.put(spec.getLocalDrugId(), new ArrayList<String>());
        }

        if (SystemSettingUtil.isSettingEnabled("enable_drug_by_specialty_batch")) {
            setNSDrugBySpecialty(moeDrugLocalRepository.findBySpecialtyDrug());
        } else {// for DH case, to minimize the sql fetch time
            List<Object[]> list2 = moeDrugBySpecialtyRepository.findAllHospcodeSpecialty();
            if (list2 != null) {
                for (Object[] o : list2) {
                    setNSDrugBySpecialty(moeDrugLocalRepository.findBySpecialtyDrug(String.valueOf(o[0]), String.valueOf(o[1])));
                }
            }
        }
    }

    private void setLegalClass() {
        if (!DtoUtil.LEGAL_CLASS_MAP.isEmpty()) return;

        List<MoeLegalClassPo> list = moeLegalClassRepository.findAll();
        for (MoeLegalClassPo l : list) {
            DtoUtil.LEGAL_CLASS_MAP.put(l.getLegalClass(), l.getLegalClassId());
            if ("Y".equals(l.getDangerousDrug())) {
                DtoUtil.DANGEROUS_DRUG_LEGAL_CLASS.add(l.getLegalClass());
            }
        }
    }

    private void setDrugCategoryContent() {
        List<MoeDrugCategoryPo> list = moeDrugCategoryRepository.findAll();
        for (MoeDrugCategoryPo category : list) {
            String key = category.getDrugCategoryId();
            MoeDrugCategoryDto moeDrugCategoryDto = DtoMapUtil.convertMoeDrugCategoryDto(category);
            DtoUtil.DRUG_CATEGORY_MAP.put(key, moeDrugCategoryDto);
        }
    }

    private void setNSDrugBySpecialty(List<Object[]> result) {
        if (result != null) {
            for (Object[] os : result) {
                // hospcode + user_specialty
                String key = os[0] + " " + os[1];
                // trade_name + vtm + route_eng + form_eng
                String drugValue = os[2] + " " + os[3] + " " + os[4] + " " + os[5];
                // dose_form_extra_info
                if (os[6] != null) {
                    drugValue += " " + os[6];
                }
                // modu
                if (os[7] != null) {
                    drugValue += " " + os[7];
                }
                drugValue += " N";
                List<String> specDrugValues = DtoUtil.EXIST_NS_SPECIALTY_DRUGS.get(key);
                if (specDrugValues == null) {
                    specDrugValues = new ArrayList<String>();
                    DtoUtil.EXIST_NS_SPECIALTY_DRUGS.put(key, specDrugValues);
                }
                specDrugValues.add(drugValue);
            }
        }
    }

    private void setTermIdMap() {
        List<MoeBaseUnitTermIdMapPo> baseUnitTermIdMap = baseRepository.findAll(MoeBaseUnitTermIdMapPo.class);
        for (MoeBaseUnitTermIdMapPo entry : baseUnitTermIdMap) {
            DtoUtil.BASE_UNIT_TERM_ID_MAP.put(entry.getBaseUnit(), entry);
        }
        List<MoePrescribeUnitTermIdMapPo> prescribeUnitTermIdMap = baseRepository.findAll(MoePrescribeUnitTermIdMapPo.class);
        for (MoePrescribeUnitTermIdMapPo entry : prescribeUnitTermIdMap) {
            DtoUtil.PRESCRIBE_UNIT_TERM_ID_MAP.put(entry.getPrescribeUnit(), entry);
        }
        List<MoeConjunctionTermIdMapPo> conjunctionTermIdMap = baseRepository.findAll(MoeConjunctionTermIdMapPo.class);
        for (MoeConjunctionTermIdMapPo entry : conjunctionTermIdMap) {
            DtoUtil.CONJUNCTION_TERM_ID_MAP.put(entry.getOrderLineType(), entry);
        }
        List<MoeDurationTermIdMapPo> durationTermIdMap = baseRepository.findAll(MoeDurationTermIdMapPo.class);
        for (MoeDurationTermIdMapPo entry : durationTermIdMap) {
            DtoUtil.DURATION_TERM_ID_MAP.put(entry.getDurationUnit(), entry);
        }
        List<MoeFreqTermIdMapPo> freqTermIdMap = baseRepository.findAll(MoeFreqTermIdMapPo.class);
        for (MoeFreqTermIdMapPo entry : freqTermIdMap) {
            String freq1 = entry.getFreq1() != null ? entry.getFreq1().toString() : "";
            DtoUtil.FREQ_TERM_ID_MAP.put(entry.getFreqCode() + " " + freq1, entry);
        }
        List<MoeRouteTermIdMapPo> routeTermIdMap = baseRepository.findAll(MoeRouteTermIdMapPo.class);
        for (MoeRouteTermIdMapPo entry : routeTermIdMap) {
            DtoUtil.ROUTE_TERM_ID_MAP.put(entry.getRouteEng(), entry);
        }
        List<MoeSiteTermIdMapPo> siteTermIdMap = baseRepository.findAll(MoeSiteTermIdMapPo.class);
        for (MoeSiteTermIdMapPo entry : siteTermIdMap) {
            DtoUtil.SITE_TERM_ID_MAP.put(entry.getSiteEng(), entry);
        }
        List<MoeSupplFreqTermIdMapPo> supplFreqTermIdMap = baseRepository.findAll(MoeSupplFreqTermIdMapPo.class);
        for (MoeSupplFreqTermIdMapPo entry : supplFreqTermIdMap) {
            String supplFreqCode = entry.getSupplFreqCode() != null ? entry.getSupplFreqCode() : "";
            String supplFreqText = "";
            if ("on odd / even days".equals(supplFreqCode)) {
                supplFreqText = entry.getSupplFreqDescEng() != null ? entry.getSupplFreqDescEng() : "";
            }
            String freq1 = entry.getSupFreq1() != null ? entry.getSupFreq1().toString() : "";
            String freq2 = entry.getSupFreq2() != null ? entry.getSupFreq2().toString() : "";
            String dow = entry.getDayOfWeek() != null ? entry.getDayOfWeek() : "";
            DtoUtil.SUPPL_FREQ_TERM_ID_MAP.put(supplFreqCode + " " + supplFreqText + " " + freq1 + " " + freq2 + " " + dow, entry);
        }
    }

    private void addPreparationContent(List<Object[]> list, boolean isLocal) {
        for (Object[] objs : list) {
            //String vtm = (isLocal || isProduction)?String.valueOf(objs[0]):lookupDict(String.valueOf(objs[0]));
            StringBuilder sb = new StringBuilder();
            if (objs[0] != null) sb.append(String.valueOf(objs[0]));
            if (objs[1] != null) sb.append(" " + String.valueOf(objs[1]));
            if (objs[2] != null) sb.append(" " + String.valueOf(objs[2]));
            if (objs[3] != null) sb.append(" " + String.valueOf(objs[3]));
            if (objs[8] != null) sb.append(" " + String.valueOf(objs[8]));
            String key = sb.toString();
            if (objs[4] != null) {
                PreparationDto str = new PreparationDto();
                str.setStrength(String.valueOf(objs[4]));
                if (objs[5] != null) {
                    str.setStrengthLevelExtraInfo(String.valueOf(objs[5]));
                }
                if (objs[6] != null) {
                    str.setAmpId(Long.valueOf(String.valueOf(objs[6])));
                }
                if (objs[7] != null) {
                    str.setLocalDrugId(String.valueOf(objs[7]));
                }
                List<PreparationDto> strs = DtoUtil.PREPARATION_MAP.get(key);
                if (strs == null) {
                    strs = new ArrayList<>();
                }
                strs.add(str);
                DtoUtil.PREPARATION_MAP.put(key, strs);
            }
        }
    }

    private void addSuspendDrug(MoeDrugLocalPo drug, MoeDrugStrengthLocalPo strength) {
        StringBuilder sb = new StringBuilder();
        StringBuilder sbWithStrength = new StringBuilder();

        sb.append(drug.getTradeName());
        sb.append(" " + drug.getVtm());
        sb.append(" " + drug.getRouteEng());
        sb.append(" " + drug.getFormEng());
        if (drug.getDoseFormExtraInfo() != null) {
            sb.append(" " + drug.getDoseFormExtraInfo());
        }
        if (drug.getPrescribeUnit() != null) {
            sb.append(" " + drug.getPrescribeUnit());
        }

        sbWithStrength.append(sb.toString());

        // Add suspend Drug
        if (!MoeCommonHelper.isFlagTrue(drug.getStrengthCompulsory())) {
            sb.append(" " + drug.getStrengthCompulsory());
            DtoUtil.SUSPEND_DRUGS_MAP.add(sb.toString());
        }

        if (MoeDrugDto.isMultiIngr(drug.getVtm()) && strength.getAmp() != null) {
            sbWithStrength.append(" " + strength.getAmp());
        }

        if (strength.getStrength() != null) {
            sbWithStrength.append(" " + strength.getStrength());
        }
        if (strength.getStrengthLevelExtraInfo() != null) {
            sbWithStrength.append(" " + strength.getStrengthLevelExtraInfo());
        }

        sbWithStrength.append(" " + drug.getStrengthCompulsory());

        DtoUtil.SUSPEND_DRUGS_MAP.add(sbWithStrength.toString());
    }

    private void addExistDrug(MoeDrugLocalPo drug, MoeDrugStrengthLocalPo strength) {
        StringBuilder sb = new StringBuilder();
        StringBuilder sbWithStrength = new StringBuilder();
        StringBuilder sbWithTradeAlias = new StringBuilder();
        StringBuilder sbWithTradeAliasWithStrength = new StringBuilder();

        sb.append(drug.getTradeName());
        sb.append(" " + drug.getVtm());
        sb.append(" " + drug.getRouteEng());
        sb.append(" " + drug.getFormEng());
        if (drug.getDoseFormExtraInfo() != null) {
            sb.append(" " + drug.getDoseFormExtraInfo());
        }
        if (drug.getPrescribeUnit() != null) {
            sb.append(" " + drug.getPrescribeUnit());
        }

        sbWithTradeAlias.append(sb.toString());
        sbWithTradeAliasWithStrength.append(sb.toString());

        if (MoeCommonHelper.isFlagTrue(drug.getStrengthCompulsory())) {
            if (MoeDrugDto.isMultiIngr(drug.getVtm()) && strength.getAmp() != null) {
                if (drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() != null) {
                    sbWithTradeAlias.append(" [" + drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() + "]");
                    sbWithTradeAliasWithStrength.append(" [" + drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() + "]");
                }
            }
            drug.setTradeVtmRouteFormId(strength.getAmpId());
        }

        sbWithStrength.append(sb.toString());

        if (drug.getTerminologyName() == null || MoeDrugDto.isMultiIngr(drug.getVtm()) && strength.getAmp() != null) {
            sbWithTradeAliasWithStrength.append(" " + strength.getAmp());
            sbWithStrength.append(" " + strength.getAmp());
        }

        if (strength.getStrength() != null) {
            sbWithTradeAliasWithStrength.append(" " + strength.getStrength());
            sbWithStrength.append(" " + strength.getStrength());
        }
        if (strength.getStrengthLevelExtraInfo() != null) {
            sbWithTradeAliasWithStrength.append(" " + strength.getStrengthLevelExtraInfo());
            sbWithStrength.append(" " + strength.getStrengthLevelExtraInfo());
        }

        sb.append(" " + drug.getStrengthCompulsory());
        sbWithStrength.append(" " + drug.getStrengthCompulsory());
        sbWithTradeAlias.append(" " + drug.getStrengthCompulsory());
        sbWithTradeAliasWithStrength.append(" " + drug.getStrengthCompulsory());

        if (!MoeCommonHelper.isFlagTrue(drug.getStrengthCompulsory())) {
            MoeDrugLocalPo cloneDrug = DtoMapUtil.createMoeDrugLocalForKey(drug);
            cloneDrug.setTradeVtmRouteFormId(strength.getAmpId());
            DtoUtil.EXIST_DRUGS_MAP_WITH_STRENGTH.put(sbWithStrength.toString(), cloneDrug);
        }
        // Add Exist Danger Drug
        if (drug.getLegalClass() != null && DtoUtil.DANGEROUS_DRUG_LEGAL_CLASS.contains(drug.getLegalClass())) {
            DtoUtil.EXIST_DANGER_DRUGS.add(sbWithTradeAlias.toString());
            DtoUtil.EXIST_DANGER_DRUGS.add(sbWithTradeAliasWithStrength.toString());
        }

        // Add Exist Drug
        if (!MoeCommonHelper.isFlagTrue(drug.getStrengthCompulsory())) {
            DtoUtil.EXIST_DRUGS_MAP.put(sb.toString(), drug);
            for (MoeDrugAliasNameLocalPo local : drug.getMoeDrugAliasNames()) {
                DtoUtil.EXIST_DRUGS_WITH_ALIAS_LIST.add(sb.toString() + " " + local.getAliasName());
            }
            DtoUtil.EXIST_DRUGS_MAP_WITH_STRENGTH.put(sb.toString(), drug);
            // Add Exist Drug with Trade Name Alias
            if (drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() != null) {
                DtoUtil.EXIST_DRUGS_WITH_TRADE_ALIAS_MAP.put(drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() + " " + sbWithTradeAlias.toString(), drug);
            }
            // Add Exist Danger Drug
            if (drug.getLegalClass() != null && DtoUtil.DANGEROUS_DRUG_LEGAL_CLASS.contains(drug.getLegalClass())) {
                DtoUtil.EXIST_DANGER_DRUGS.add(sb.toString());
            }
        }

        DtoUtil.EXIST_DRUGS_MAP.put(sbWithStrength.toString(), drug);
        for (MoeDrugAliasNameLocalPo local : drug.getMoeDrugAliasNames()) {
            DtoUtil.EXIST_DRUGS_WITH_ALIAS_LIST.add(sbWithStrength.toString() + " " + local.getAliasName());
        }
        // Add Exist Drug with Trade Name Alias
        if (drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() != null) {
            DtoUtil.EXIST_DRUGS_WITH_TRADE_ALIAS_MAP.put(drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() + " " + sbWithTradeAliasWithStrength.toString(), drug);
        }

        // Add Specialty Drug
        if (DtoUtil.EXIST_SPECIALTY_DRUGS.get(drug.getLocalDrugId()) != null) {
            if (drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() != null) {
                DtoUtil.EXIST_SPECIALTY_DRUGS.get(drug.getLocalDrugId()).add(sbWithTradeAliasWithStrength.toString());
            }
            DtoUtil.EXIST_SPECIALTY_DRUGS.get(drug.getLocalDrugId()).add(sbWithStrength.toString());
        }
    }

    private void addParentDrugs(List<MoeDrugDto> dtos) {
        for (MoeDrugDto dto : dtos) {
            if (dto.getTerminologyName() == null) {
                dto.setDisplayNameType(ServerConstant.ALIAS_NAME_TYPE_LOCAL_DRUG);
            }
            Formatter.setDisplayAttributes(dto);
            if (DtoUtil.DRUGS_BY_DRUGNAME.get(dto.getShortName()) == null) {
                DtoUtil.DRUGS_BY_DRUGNAME.put(dto.getShortName(), dto);
            }
            addDrugsByType(DtoUtil.DRUGS_BY_TYPE, dto);
        }
    }

    private void addDrugsByType(Map<String, List<MoeDrugDto>> map, MoeDrugDto dto) {
        String aliasNameType = dto.getDisplayNameType();
        if (dto.getTerminologyName() == null) {    //NONMTT
            DtoUtil.addDtoToMap(map, DrugType.NONMTT.toString(), dto);
        } else if (MoeCommonHelper.isAliasNameTypeVtm(aliasNameType)) {
            if (MoeDrugDto.isMultiIngr(dto.getVtm())) {    //MULTIINGR
                DtoUtil.addDtoToMap(map, DrugType.MULTIINGR.toString(), dto);
            } else if (MoeCommonHelper.isFlagTrue(dto.getGenericIndicator())) {    //GENERIC
                DtoUtil.addDtoToMap(map, DrugType.GENERIC.toString(), dto);
            } else {    //VTM & TRADENAME
                if (dto.getVtm() != null) {
                    DtoUtil.addDtoToMap(map, DrugType.VTM.toString(), dto);
                }
                if (dto.getTradeName() != null) {
                    DtoUtil.addDtoToMap(map, DrugType.TRADENAME.toString(), dto);
                }
            }
        } else if (MoeCommonHelper.isAliasNameTypeBan(aliasNameType)) {
            DtoUtil.addDtoToMap(map, DrugType.BAN.toString(), dto);
        } else if (MoeCommonHelper.isAliasNameTypeAbb(aliasNameType)) {
            DtoUtil.addDtoToMap(map, DrugType.ABB.toString(), dto);
        } else if (MoeCommonHelper.isAliasNameTypeOther(aliasNameType)) {
            DtoUtil.addDtoToMap(map, DrugType.OTHER.toString(), dto);
        } else if (MoeCommonHelper.isAliasNameTypeTradeNameAlias(aliasNameType)) {
            DtoUtil.addDtoToMap(map, DrugType.TRADENAMEALIAS.toString(), dto);
        }
    }

    private void addDosageDrugs(List<MoeDrugDto> dtos) {
        for (MoeDrugDto dto : dtos) {
            if (dto.getTerminologyName() == null) {
                dto.setDisplayNameType(ServerConstant.ALIAS_NAME_TYPE_LOCAL_DRUG);
            }
            Formatter.setDisplayAttributes(dto);
            addDrugsByType(DtoUtil.DOSAGE_DRUGS_BY_TYPE, dto);
        }
    }

    private void sortDrugContent() {
        for (DrugType type : DrugType.values()) {
            DrugSearchEngine.DrugSearchType comparatorType = DrugSearchEngine.DrugSearchType.ASSORTED;
            if (type.equals(DrugType.TRADENAME)) {
                comparatorType = DrugSearchEngine.DrugSearchType.TRADENAME;
            } else if (type.equals(DrugType.GENERIC)) {
                comparatorType = DrugSearchEngine.DrugSearchType.GENERIC;
            } else if (type.equals(DrugType.NONMTT)) {
                comparatorType = DrugSearchEngine.DrugSearchType.NONMTT;
            }

            List<MoeDrugDto> drugs = DtoUtil.DRUGS_BY_TYPE.get(type.toString());
            if (drugs != null) {
                Collections.sort(drugs, DrugSearchEngine.COMPARATOR_BY_TYPE.get(comparatorType));
            }

            drugs = DtoUtil.DOSAGE_DRUGS_BY_TYPE.get(type.toString());
            if (drugs != null) {
                Collections.sort(drugs, DrugSearchEngine.COMPARATOR_BY_TYPE.get(comparatorType));
            }
        }
    }

    // Ricci 20190722 end --

    // Ricci 20190722 start --

    /**
     * for setTempContent() --
     */
    private void setTempStrengthContent() {
        List<Object[]> list = moeDrugStrengthLocalRepository.getAllNonStrengthCompulsory();
        addTempPreparationContent(list, true);
    }

    private void setTempDuplicateBasicNames() {
        if (!TempDtoUtil.DUP_BASIC_NAMES.isEmpty()) return;
        List<Object[]> results = moeFreqLocalRepository.getAllTradeNameAliasNamePairs(ServerConstant.ALIAS_NAME_TYPE_ABB, ServerConstant.ALIAS_NAME_TYPE_OTHER);
        if (results != null) {
            Map<String, String> map = new HashMap<String, String>();
            for (Object[] c : results) { // c0 - trade name, c1 - other name/abbreviation, c2 - vtm
                String tradeName = String.valueOf(c[0]);
                String alias = String.valueOf(c[1]);
                String vtm2 = String.valueOf(c[2]);
                String vtm = map.get(tradeName + alias);
                if (vtm != null && !vtm.equalsIgnoreCase(vtm2)) {
                    TempDtoUtil.DUP_BASIC_NAMES.add(tradeName + alias);
                } else if (vtm == null) {
                    map.put(tradeName + alias, vtm2);
                }
            }
        }
    }

    private void setTempFreqContent() {

        if (TempDtoUtil.FREQS_LOCAL.isEmpty()) {
            List<MoeFreqLocalPo> list = moeFreqLocalRepository.getAllOrderByRank();
            int orderCount = 1;
            for (MoeFreqLocalPo freq : list) {
                TempDtoUtil.FREQS_LOCAL.add(DtoMapUtil.convertMoeFreqDto(freq));
                TempDtoUtil.FREQ_ORDER.put(freq.getFreqEng(), orderCount++);
            }
        }

        if (TempDtoUtil.FREQS.isEmpty()) {
            List<MoeFreqPo> list = moeFreqRepository.findAll();
            for (MoeFreqPo freq : list) {
                TempDtoUtil.FREQS.add(DtoMapUtil.convertMoeFreqDto(freq));
                TempDtoUtil.FREQ_ID_MAP.put(freq.getFreqId(), freq);
            }
        }
    }

    private void setTempSupplFreqContent() {

        if (TempDtoUtil.REGIMEN_TYPE_MAP.size() == 0) {
            List<MoeSupplFreqPo> list = moeSupplFreqRepository.getAllOrderByTypeAndRank();
            List<MoeSupplFreqDto> supplFreqList = new ArrayList<MoeSupplFreqDto>();

            String prevRegimenType = "-";
            String currentRegimenType = "-";
            int count = 1;

            for (MoeSupplFreqPo supplFreq : list) {
                if (count != 1) {
                    currentRegimenType = supplFreq.getMoeRegimen().getRegimenType();
                }
                if (prevRegimenType.equalsIgnoreCase(currentRegimenType)) {
                    supplFreqList.add(DtoMapUtil.convertMoeSupplFreqDto(supplFreq));
                }
                if (!prevRegimenType.equalsIgnoreCase(currentRegimenType)) {
                    TempDtoUtil.REGIMEN_TYPE_MAP.put(prevRegimenType, supplFreqList);
                    supplFreqList = new ArrayList<MoeSupplFreqDto>();
                    supplFreqList.add(DtoMapUtil.convertMoeSupplFreqDto(supplFreq)); // add back the different one
                }
                prevRegimenType = supplFreq.getMoeRegimen().getRegimenType();
                count++;
            }
            if (list.size() > 0 && TempDtoUtil.REGIMEN_TYPE_MAP.get(currentRegimenType) == null) {
                TempDtoUtil.REGIMEN_TYPE_MAP.put(currentRegimenType, supplFreqList);
            }
        }
    }

    private void setTempFormRouteMapContent() {
        if (!TempDtoUtil.FORM_ROUTE_MAP.isEmpty()) return;

        List<MoeFormRouteMapPo> list = moeFormRouteMapRepository.findAll();
        for (MoeFormRouteMapPo fr : list) {
            String key = fr.getMoeForm().getFormEng() + " " + fr.getMoeRoute().getRouteEng();
            TempDtoUtil.FORM_ROUTE_MAP.put(key, fr.getDisplayRoute());
        }
    }

    private void setTempDrugContent() {

        List<MoeDrugLocalPo> locals = moeDrugLocalRepository.getAllDistinctBasicAttributes();
        HashMap<String, HashSet<MoeDrugAliasNameLocalPo>> map = new HashMap<>();
        List<MoeDrugAliasNameLocalPo> aliasNames = moeDrugAliasNameLocalRepository.findAll();
        for (MoeDrugAliasNameLocalPo alias : aliasNames) {
            HashSet<MoeDrugAliasNameLocalPo> set = map.get(alias.getHkRegNo());
            if (set == null) {
                set = new HashSet<>();
            }
            set.add(alias);
            map.put(alias.getHkRegNo(), set);
        }

        for (MoeDrugLocalPo local : locals) {
            HashSet<MoeDrugAliasNameLocalPo> set = map.get(local.getHkRegNo());
            if (set != null) {
                local.setMoeDrugAliasNames(set);
            } else {
                local.setMoeDrugAliasNames(new HashSet<>());
            }
        }

        List<String> suspendDrugs = moeDrugLocalRepository.getAllSuspendedDrugs("Y");
        for (MoeDrugLocalPo drug : locals) {
            for (MoeDrugStrengthLocalPo strength : drug.getMoeDrugStrengths()) {
                if (suspendDrugs.contains(drug.getLocalDrugId())) {
                    addTempSuspendDrug(drug, strength);
                } else {
                    addTempExistDrug(drug, strength);

                    //Chris 20200113  For Supplement VmpId from moe_drug_strength  -Start
                    MoeDrugStrengthPo moeDrugStrengthPo = null;
                    if (strength.getVmpId() == null || strength.getVmpId() == 0L) {
                        if (StringUtils.isNotBlank(drug.getHkRegNo()) && strength.getAmpId() != null && strength.getAmpId() != 0L) {
                            moeDrugStrengthPo = moeDrugStrengthRepository.findByHkRegNoAndAmpId(drug.getHkRegNo(), strength.getAmpId());
                        }
                    }
                    //List<MoeDrugDto> dtos = DtoMapUtil.convertMoeDrugDTOWithAllStrengths(drug, strength);
                    List<MoeDrugDto> dtos = DtoMapUtil.convertMoeDrugDTOWithAllStrengths(drug, strength, moeDrugStrengthPo);
                    //Chris 20200113  For Supplement VmpId from moe_drug_strength  -End
                    //TODO Add Temp Parent Drugs & Drug by Tyep
                    addTempParentDrugs(dtos);

                    if (drug.getMoeCommonDosages() != null) {
                        dtos = DtoMapUtil.convertMoeDrugDTOWithAllCommonDosage(drug, strength);
                        //TODO Add Dosage Drugs
                        addTempDosageDrugs(dtos);
                    }
                }
            }
        }
        sortTempDrugContent();

        if (TempDtoUtil.DRUGS_BY_HKREGNO.size() > 0) {
            return;
        }
        List<MoeDrugDto> drugList = moeDrugRepository.findHkRegNoAndVtm();
        for (MoeDrugDto drug : drugList) {
            TempDtoUtil.DRUGS_BY_HKREGNO.put(drug.getHkRegNo(), drug.getVtm());
        }

    }

    private void setTempActionStatusContent() {

        if (TempDtoUtil.ACTIONS_STATUS_LIST.isEmpty()) {
            List<MoeActionStatusPo> list = moeActionStatusRepository.getAllOrderByRank();
            for (MoeActionStatusPo actionStatus : list) {
                TempDtoUtil.ACTIONS_STATUS_LIST.add(DtoMapUtil.convertMoeActionStatusDto(actionStatus));
            }
            for (MoeActionStatusDto moeActionStatusDto : TempDtoUtil.ACTIONS_STATUS_LIST) {
                TempDtoUtil.ACTIONS_STATUS_MAP.put(moeActionStatusDto.getActionStatusType().toUpperCase(), moeActionStatusDto);
            }

        }
    }

    private void setTempSiteContent() {
        if (TempDtoUtil.ROUTE_MAP.size() == 0) {
            List<MoeSitePo> list = moeSiteRepository.getAllOrderByRankAndSite();

            for (MoeSitePo site : list) {
                MoeSiteDto dto = DtoMapUtil.convertMoeSiteDto(site);
                TempDtoUtil.SITE_LIST.add(dto);
            }
        }
    }

    private void setTempRouteContent() {
        if (TempDtoUtil.ROUTE_LIST.isEmpty()) {
            List<MoeRoutePo> list = moeRouteRepository.getAllOrderByRank();
            for (MoeRoutePo route : list) {
                TempDtoUtil.ROUTE_LIST.add(DtoMapUtil.convertMoeRouteDto(route));
                TempDtoUtil.ROUTE_ID_MAP.put(route.getRouteId(), route);
            }
        }
    }

    private void setTempRegimenContent() {
        if (TempDtoUtil.REGIMENT_LIST.isEmpty()) {
            List<MoeRegimenPo> list = moeRegimenRepository.getAllOrderByRank();
            for (MoeRegimenPo regimen : list) {
                TempDtoUtil.REGIMENT_LIST.add(DtoMapUtil.convertMoeRegimenDto(regimen));
                TempDtoUtil.REGIMEN_ID_MAP.put(regimen.getRegimenType(), regimen);
            }
        }
    }

    private void setTempRegimenUnitContent() {
        if (TempDtoUtil.REGIMENT_UNIT_MAP.isEmpty()) {
            List<MoeRegimenUnitPo> list = moeRegimenUnitRepository.getAllOrderByRank();
            for (MoeRegimenUnitPo unit : list) {
                List<MoeRegimenUnitDto> dtos = TempDtoUtil.REGIMENT_UNIT_MAP.get(unit.getRegimenType());
                if (dtos == null) {
                    dtos = new ArrayList<MoeRegimenUnitDto>();
                }
                dtos.add(DtoMapUtil.convertMoeRegimenUnitDto(unit));
                TempDtoUtil.REGIMENT_UNIT_MAP.put(unit.getRegimenType(), dtos);
            }
        }
    }

    private void setTempRouteSiteMapContent() {
        if (!TempDtoUtil.ROUTE_SITE_MAP.isEmpty()) return;

        List<MoeRouteSiteMapPo> list = moeRouteSiteMapRepository.findAllOrderByRouteAndRank();
        for (MoeRouteSiteMapPo rs : list) {
            String key = rs.getMoeRoute().getRouteEng() + " " + rs.getMoeSite().getSiteEng();
            TempDtoUtil.ROUTE_SITE_MAP.put(key, rs.getDisplayRoute());

            List<MoeSiteDto> siteList = TempDtoUtil.ROUTE_MAP.get(rs.getMoeRoute().getRouteEng());
            if (siteList == null) {
                siteList = new ArrayList<MoeSiteDto>();
            }
            siteList.add(DtoMapUtil.convertMoeSiteDto(rs.getMoeSite()));
            TempDtoUtil.ROUTE_MAP.put(rs.getMoeRoute().getRouteEng(), siteList);
        }
    }

    private void setTempBaseUnit() {
        if (TempDtoUtil.BASE_UNIT_LIST.isEmpty()) {
            List<MoeBaseUnitPo> list = moeBaseUnitRepository.findAllBaseUnit(ServerConstant.BASE_UNIT_TRUE);
            for (MoeBaseUnitPo baseUnit : list) {
                TempDtoUtil.BASE_UNIT_LIST.add(DtoMapUtil.convertMoeBaseUnitDto(baseUnit));
                if ("dose".equals(baseUnit.getBaseUnit())) {
                    TempDtoUtil.setBaseUnitDoseId(baseUnit.getBaseUnitId());
                }
            }
        }
        if (TempDtoUtil.PRESCRIBE_UNIT_LIST.isEmpty()) {
            List<MoeBaseUnitPo> list = moeBaseUnitRepository.findAllPrescribeUnit(ServerConstant.BASE_UNIT_TRUE);
            for (MoeBaseUnitPo baseUnit : list) {
                TempDtoUtil.PRESCRIBE_UNIT_LIST.add(DtoMapUtil.convertMoeBaseUnitDto(baseUnit));
            }
        }
        if (TempDtoUtil.DISPENSE_UNIT_LIST.isEmpty()) {
            List<MoeBaseUnitPo> list = moeBaseUnitRepository.findAllDispenseUnit(ServerConstant.BASE_UNIT_TRUE);
            for (MoeBaseUnitPo baseUnit : list) {
                TempDtoUtil.DISPENSE_UNIT_LIST.add(DtoMapUtil.convertMoeBaseUnitDto(baseUnit));
            }
        }
    }

    private void setTempForm() {
        if (TempDtoUtil.FORM_LIST.isEmpty()) {
            List<MoeFormPo> list = moeFormRepository.findAll();
            for (MoeFormPo form : list) {
                TempDtoUtil.FORM_LIST.add(DtoMapUtil.convertMoeFormDto(form));
            }
        }
    }

    private void setTempDurationUnitContent() {
        if (!TempDtoUtil.DURATION_UNIT_MAP.isEmpty()) return;

        List<MoeDurationUnitMultiplierPo> list = moeDurationUnitMultiplierRepository.findAll();
        for (MoeDurationUnitMultiplierPo du : list) {
            TempDtoUtil.DURATION_UNIT_MAP.put(du.getDurationUnit(), du.getDurationMultiplier());
            TempDtoUtil.DURATION_UNIT_DESC_MAP.put(du.getDurationUnit(), du.getDurationUnitDesc());
            // Ricci 20190719 start --
            TempDtoUtil.DURATION_UNIT_LIST.add(DtoMapUtil.convertMoeDurationUnitDto(du));
            // Ricci 20190719 end --
        }
    }

    private void setTempMoeClinicalAlertRuleContent() {
        if (!TempDtoUtil.CLINICAL_ALERT_MAP.isEmpty()) return;

        List<MoeClinicalAlertRulePo> list = moeClinicalAlertRuleRepository.findAll();
        for (MoeClinicalAlertRulePo rule : list) {
            TempDtoUtil.CLINICAL_ALERT_MAP.put(rule.getVtm(), DtoMapUtil.convertMoeClinicalAlertRuleDto(rule));
        }
    }

    private void setTempMoeDrugOrderPropertyContent() {
        List<Object[]> list = moeDrugOrdPropertyLocalRepository.getMinDosageAndMaxDurationByVtmRouteForm();
        // strength compulsory, trade name, vtm , route , form, form extra, strength, min, max
        for (Object[] objs : list) {
            String key;
            boolean isMutiIngr = MoeDrugDto.isMultiIngr(String.valueOf(objs[2]));
            boolean isStrengthCompulsory = String.valueOf(objs[0]).equals("Y");
            if (isStrengthCompulsory) {
                StringBuilder sb = new StringBuilder();
                if (objs[1] != null) sb.append(String.valueOf(objs[1]));
                if (objs[2] != null) sb.append(" " + String.valueOf(objs[2]));
                if (objs[3] != null) sb.append(" " + String.valueOf(objs[3]));
                if (objs[4] != null) sb.append(" " + String.valueOf(objs[4]));
                if (objs[5] != null) sb.append(" " + String.valueOf(objs[5]));
                if (objs[6] != null) sb.append(" " + String.valueOf(objs[6]));
                key = sb.toString();
            } else {
                StringBuilder sb = new StringBuilder();
                if (objs[1] != null) sb.append(String.valueOf(objs[1]));
                if (objs[2] != null) sb.append(" " + String.valueOf(objs[2]));
                if (objs[3] != null) sb.append(" " + String.valueOf(objs[3]));
                if (objs[4] != null) sb.append(" " + String.valueOf(objs[4]));
                if (objs[5] != null) sb.append(" " + String.valueOf(objs[5]));
                key = sb.toString();
            }

            Double dosage = null;
            Long duration = null;
            String specifyQuantityFlag = null;
            if (objs[7] != null) {
                dosage = ((BigDecimal) objs[7]).doubleValue();
            }
            if (objs[8] != null) {
                duration = (Long) objs[8];
            }
            if (objs[9] != null) {
                specifyQuantityFlag = (String) objs[9];
            }

            if (!isStrengthCompulsory) {
                if (dosage != null) {
                    List<Double> dosages = TempDtoUtil.MIN_DOSAGE_MAP.get(key);
                    if (dosages == null) {
                        dosages = new ArrayList<Double>();
                        TempDtoUtil.MIN_DOSAGE_MAP.put(key, dosages);
                    }
                    if (!dosages.contains(dosage)) {
                        dosages.add(dosage);
                    }
                }
                if (duration != null) {
                    Long maxDuration = TempDtoUtil.MAX_DURATION_MAP.get(key);
                    if (maxDuration == null || duration < maxDuration) {
                        TempDtoUtil.MAX_DURATION_MAP.put(key, duration);
                    }
                }
                if (specifyQuantityFlag != null && "Y".equalsIgnoreCase(specifyQuantityFlag)) {
                    TempDtoUtil.SPECIFY_QUANTITY_FLAG.add(key);
                }
            } else {
                if (isMutiIngr) {
                    TempDtoUtil.MULT_DRUG_ORDER_PROPERTY_MAP.put(key, new MoeDrugOrderPropertyDto(dosage, duration));
                } else {
                    TempDtoUtil.SINGLE_DRUG_ORDER_PROPERTY_MAP.put(key, new MoeDrugOrderPropertyDto(dosage, duration));
                }
                if (specifyQuantityFlag != null && "Y".equalsIgnoreCase(specifyQuantityFlag)) {
                    TempDtoUtil.STRENGTH_COMP_SPECIFY_QUANTITY_FLAG.add(key);
                }
            }
        }
    }

    private void setTempDoseFormExtraInfoContent() {
        List<MoeDoseFormExtraInfoPo> list = moeDoseFormExtraInfoRepository.findAll();
        for (MoeDoseFormExtraInfoPo info : list) {
            TempDtoUtil.DOSE_FORM_EXTRA_INFO_LIST.add(info);
        }
    }

    private void setTempDrugBySpecialtyContent() {
        List<MoeDrugBySpecialtyPo> list = moeDrugBySpecialtyRepository.findAll();
        for (MoeDrugBySpecialtyPo spec : list) {
            String key = spec.getHospcode() + " " + spec.getUserSpecialty();
            List<String> hkregs = TempDtoUtil.HKREG_BY_SPECIALTY.get(key);
            if (hkregs == null) {
                hkregs = new ArrayList<String>();
            }
            hkregs.add(spec.getLocalDrugId());
            TempDtoUtil.HKREG_BY_SPECIALTY.put(key, hkregs);
            TempDtoUtil.EXIST_SPECIALTY_DRUGS.put(spec.getLocalDrugId(), new ArrayList<String>());
        }

        if (SystemSettingUtil.isSettingEnabled("enable_drug_by_specialty_batch")) {
            setTempNSDrugBySpecialty(moeDrugLocalRepository.findBySpecialtyDrug());
        } else {// for DH case, to minimize the sql fetch time
            List<Object[]> list2 = moeDrugBySpecialtyRepository.findAllHospcodeSpecialty();
            if (list2 != null) {
                for (Object[] o : list2) {
                    setTempNSDrugBySpecialty(moeDrugLocalRepository.findBySpecialtyDrug(String.valueOf(o[0]), String.valueOf(o[1])));
                }
            }
        }
    }

    private void setTempLegalClass() {
        if (!TempDtoUtil.LEGAL_CLASS_MAP.isEmpty()) return;

        List<MoeLegalClassPo> list = moeLegalClassRepository.findAll();
        for (MoeLegalClassPo l : list) {
            TempDtoUtil.LEGAL_CLASS_MAP.put(l.getLegalClass(), l.getLegalClassId());
            if ("Y".equals(l.getDangerousDrug())) {
                TempDtoUtil.DANGEROUS_DRUG_LEGAL_CLASS.add(l.getLegalClass());
            }
        }
    }

    private void setTempDrugCategoryContent() {
        List<MoeDrugCategoryPo> list = moeDrugCategoryRepository.findAll();
        for (MoeDrugCategoryPo category : list) {
            String key = category.getDrugCategoryId();
            MoeDrugCategoryDto moeDrugCategoryDto = DtoMapUtil.convertMoeDrugCategoryDto(category);
            TempDtoUtil.DRUG_CATEGORY_MAP.put(key, moeDrugCategoryDto);
        }
    }

    private void setTempNSDrugBySpecialty(List<Object[]> result) {
        if (result != null) {
            for (Object[] os : result) {
                // hospcode + user_specialty
                String key = os[0] + " " + os[1];
                // trade_name + vtm + route_eng + form_eng
                String drugValue = os[2] + " " + os[3] + " " + os[4] + " " + os[5];
                // dose_form_extra_info
                if (os[6] != null) {
                    drugValue += " " + os[6];
                }
                // modu
                if (os[7] != null) {
                    drugValue += " " + os[7];
                }
                drugValue += " N";
                List<String> specDrugValues = TempDtoUtil.EXIST_NS_SPECIALTY_DRUGS.get(key);
                if (specDrugValues == null) {
                    specDrugValues = new ArrayList<String>();
                    TempDtoUtil.EXIST_NS_SPECIALTY_DRUGS.put(key, specDrugValues);
                }
                specDrugValues.add(drugValue);
            }
        }
    }

    private void setTempTermIdMap() {
        List<MoeBaseUnitTermIdMapPo> baseUnitTermIdMap = baseRepository.findAll(MoeBaseUnitTermIdMapPo.class);
        for (MoeBaseUnitTermIdMapPo entry : baseUnitTermIdMap) {
            TempDtoUtil.BASE_UNIT_TERM_ID_MAP.put(entry.getBaseUnit(), entry);
        }
        List<MoePrescribeUnitTermIdMapPo> prescribeUnitTermIdMap = baseRepository.findAll(MoePrescribeUnitTermIdMapPo.class);
        for (MoePrescribeUnitTermIdMapPo entry : prescribeUnitTermIdMap) {
            TempDtoUtil.PRESCRIBE_UNIT_TERM_ID_MAP.put(entry.getPrescribeUnit(), entry);
        }
        List<MoeConjunctionTermIdMapPo> conjunctionTermIdMap = baseRepository.findAll(MoeConjunctionTermIdMapPo.class);
        for (MoeConjunctionTermIdMapPo entry : conjunctionTermIdMap) {
            TempDtoUtil.CONJUNCTION_TERM_ID_MAP.put(entry.getOrderLineType(), entry);
        }
        List<MoeDurationTermIdMapPo> durationTermIdMap = baseRepository.findAll(MoeDurationTermIdMapPo.class);
        for (MoeDurationTermIdMapPo entry : durationTermIdMap) {
            TempDtoUtil.DURATION_TERM_ID_MAP.put(entry.getDurationUnit(), entry);
        }
        List<MoeFreqTermIdMapPo> freqTermIdMap = baseRepository.findAll(MoeFreqTermIdMapPo.class);
        for (MoeFreqTermIdMapPo entry : freqTermIdMap) {
            String freq1 = entry.getFreq1() != null ? entry.getFreq1().toString() : "";
            TempDtoUtil.FREQ_TERM_ID_MAP.put(entry.getFreqCode() + " " + freq1, entry);
        }
		/*List<MoePrnTermIdMap> prnTermIdMap = simpleDao.findAll(MoePrnTermIdMap.class);
		for(MoePrnTermIdMap entry : prnTermIdMap) {
			TempDtoUtil.PRN_TERM_ID_MAP.put(entry.getPrn(), entry);
		}*/
        List<MoeRouteTermIdMapPo> routeTermIdMap = baseRepository.findAll(MoeRouteTermIdMapPo.class);
        for (MoeRouteTermIdMapPo entry : routeTermIdMap) {
            TempDtoUtil.ROUTE_TERM_ID_MAP.put(entry.getRouteEng(), entry);
        }
        List<MoeSiteTermIdMapPo> siteTermIdMap = baseRepository.findAll(MoeSiteTermIdMapPo.class);
        for (MoeSiteTermIdMapPo entry : siteTermIdMap) {
            TempDtoUtil.SITE_TERM_ID_MAP.put(entry.getSiteEng(), entry);
        }
        List<MoeSupplFreqTermIdMapPo> supplFreqTermIdMap = baseRepository.findAll(MoeSupplFreqTermIdMapPo.class);
        for (MoeSupplFreqTermIdMapPo entry : supplFreqTermIdMap) {
            String supplFreqCode = entry.getSupplFreqCode() != null ? entry.getSupplFreqCode() : "";
            String supplFreqText = "";
            if ("on odd / even days".equals(supplFreqCode)) {
                supplFreqText = entry.getSupplFreqDescEng() != null ? entry.getSupplFreqDescEng() : "";
            }
            String freq1 = entry.getSupFreq1() != null ? entry.getSupFreq1().toString() : "";
            String freq2 = entry.getSupFreq2() != null ? entry.getSupFreq2().toString() : "";
            String dow = entry.getDayOfWeek() != null ? entry.getDayOfWeek() : "";
            TempDtoUtil.SUPPL_FREQ_TERM_ID_MAP.put(supplFreqCode + " " + supplFreqText + " " + freq1 + " " + freq2 + " " + dow, entry);
        }
    }

    private void addTempPreparationContent(List<Object[]> list, boolean isLocal) {
        for (Object[] objs : list) {
            //String vtm = (isLocal || isProduction)?String.valueOf(objs[0]):lookupDict(String.valueOf(objs[0]));
            StringBuilder sb = new StringBuilder();
            if (objs[0] != null) sb.append(String.valueOf(objs[0]));
            if (objs[1] != null) sb.append(" " + String.valueOf(objs[1]));
            if (objs[2] != null) sb.append(" " + String.valueOf(objs[2]));
            if (objs[3] != null) sb.append(" " + String.valueOf(objs[3]));
            if (objs[8] != null) sb.append(" " + String.valueOf(objs[8]));
            String key = sb.toString();
            if (objs[4] != null) {
                PreparationDto str = new PreparationDto();
                str.setStrength(String.valueOf(objs[4]));
                if (objs[5] != null) {
                    str.setStrengthLevelExtraInfo(String.valueOf(objs[5]));
                }
                if (objs[6] != null) {
                    str.setAmpId(Long.valueOf(String.valueOf(objs[6])));
                }
                if (objs[7] != null) {
                    str.setLocalDrugId(String.valueOf(objs[7]));
                }
                List<PreparationDto> strs = TempDtoUtil.PREPARATION_MAP.get(key);
                if (strs == null) {
                    strs = new ArrayList<>();
                }
                strs.add(str);
                TempDtoUtil.PREPARATION_MAP.put(key, strs);
            }
        }
    }

    private void addTempSuspendDrug(MoeDrugLocalPo drug, MoeDrugStrengthLocalPo strength) {
        StringBuilder sb = new StringBuilder();
        StringBuilder sbWithStrength = new StringBuilder();

        sb.append(drug.getTradeName());
        sb.append(" " + drug.getVtm());
        sb.append(" " + drug.getRouteEng());
        sb.append(" " + drug.getFormEng());
        if (drug.getDoseFormExtraInfo() != null) {
            sb.append(" " + drug.getDoseFormExtraInfo());
        }
        if (drug.getPrescribeUnit() != null) {
            sb.append(" " + drug.getPrescribeUnit());
        }

        sbWithStrength.append(sb.toString());

        // Add suspend Drug
        if (!MoeCommonHelper.isFlagTrue(drug.getStrengthCompulsory())) {
            sb.append(" " + drug.getStrengthCompulsory());
            TempDtoUtil.SUSPEND_DRUGS_MAP.add(sb.toString());
        }

        if (MoeDrugDto.isMultiIngr(drug.getVtm()) && strength.getAmp() != null) {
            sbWithStrength.append(" " + strength.getAmp());
        }

        if (strength.getStrength() != null) {
            sbWithStrength.append(" " + strength.getStrength());
        }
        if (strength.getStrengthLevelExtraInfo() != null) {
            sbWithStrength.append(" " + strength.getStrengthLevelExtraInfo());
        }

        sbWithStrength.append(" " + drug.getStrengthCompulsory());

        TempDtoUtil.SUSPEND_DRUGS_MAP.add(sbWithStrength.toString());
    }

    private void addTempExistDrug(MoeDrugLocalPo drug, MoeDrugStrengthLocalPo strength) {
        StringBuilder sb = new StringBuilder();
        StringBuilder sbWithStrength = new StringBuilder();
        StringBuilder sbWithTradeAlias = new StringBuilder();
        StringBuilder sbWithTradeAliasWithStrength = new StringBuilder();

        sb.append(drug.getTradeName());
        sb.append(" " + drug.getVtm());
        sb.append(" " + drug.getRouteEng());
        sb.append(" " + drug.getFormEng());
        if (drug.getDoseFormExtraInfo() != null) {
            sb.append(" " + drug.getDoseFormExtraInfo());
        }
        if (drug.getPrescribeUnit() != null) {
            sb.append(" " + drug.getPrescribeUnit());
        }

        sbWithTradeAlias.append(sb.toString());
        sbWithTradeAliasWithStrength.append(sb.toString());

        if (MoeCommonHelper.isFlagTrue(drug.getStrengthCompulsory())) {
            if (MoeDrugDto.isMultiIngr(drug.getVtm()) && strength.getAmp() != null) {
                if (drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() != null) {
                    sbWithTradeAlias.append(" [" + drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() + "]");
                    sbWithTradeAliasWithStrength.append(" [" + drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() + "]");
                }
            }
            drug.setTradeVtmRouteFormId(strength.getAmpId());
        }

        sbWithStrength.append(sb.toString());

        if (drug.getTerminologyName() == null || MoeDrugDto.isMultiIngr(drug.getVtm()) && strength.getAmp() != null) {
            sbWithTradeAliasWithStrength.append(" " + strength.getAmp());
            sbWithStrength.append(" " + strength.getAmp());
        }

        if (strength.getStrength() != null) {
            sbWithTradeAliasWithStrength.append(" " + strength.getStrength());
            sbWithStrength.append(" " + strength.getStrength());
        }
        if (strength.getStrengthLevelExtraInfo() != null) {
            sbWithTradeAliasWithStrength.append(" " + strength.getStrengthLevelExtraInfo());
            sbWithStrength.append(" " + strength.getStrengthLevelExtraInfo());
        }

        sb.append(" " + drug.getStrengthCompulsory());
        sbWithStrength.append(" " + drug.getStrengthCompulsory());
        sbWithTradeAlias.append(" " + drug.getStrengthCompulsory());
        sbWithTradeAliasWithStrength.append(" " + drug.getStrengthCompulsory());

        if (!MoeCommonHelper.isFlagTrue(drug.getStrengthCompulsory())) {
            MoeDrugLocalPo cloneDrug = DtoMapUtil.createMoeDrugLocalForKey(drug);
            cloneDrug.setTradeVtmRouteFormId(strength.getAmpId());
            TempDtoUtil.EXIST_DRUGS_MAP_WITH_STRENGTH.put(sbWithStrength.toString(), cloneDrug);
        }
        // Add Exist Danger Drug
        if (drug.getLegalClass() != null && TempDtoUtil.DANGEROUS_DRUG_LEGAL_CLASS.contains(drug.getLegalClass())) {
            TempDtoUtil.EXIST_DANGER_DRUGS.add(sbWithTradeAlias.toString());
            TempDtoUtil.EXIST_DANGER_DRUGS.add(sbWithTradeAliasWithStrength.toString());
        }

        // Add Exist Drug
        if (!MoeCommonHelper.isFlagTrue(drug.getStrengthCompulsory())) {
            TempDtoUtil.EXIST_DRUGS_MAP.put(sb.toString(), drug);
            for (MoeDrugAliasNameLocalPo local : drug.getMoeDrugAliasNames()) {
                TempDtoUtil.EXIST_DRUGS_WITH_ALIAS_LIST.add(sb.toString() + " " + local.getAliasName());
            }
            TempDtoUtil.EXIST_DRUGS_MAP_WITH_STRENGTH.put(sb.toString(), drug);
            // Add Exist Drug with Trade Name Alias
            if (drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() != null) {
                TempDtoUtil.EXIST_DRUGS_WITH_TRADE_ALIAS_MAP.put(drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() + " " + sbWithTradeAlias.toString(), drug);
            }
            // Add Exist Danger Drug
            if (drug.getLegalClass() != null && TempDtoUtil.DANGEROUS_DRUG_LEGAL_CLASS.contains(drug.getLegalClass())) {
                TempDtoUtil.EXIST_DANGER_DRUGS.add(sb.toString());
            }
        }

        TempDtoUtil.EXIST_DRUGS_MAP.put(sbWithStrength.toString(), drug);
        for (MoeDrugAliasNameLocalPo local : drug.getMoeDrugAliasNames()) {
            TempDtoUtil.EXIST_DRUGS_WITH_ALIAS_LIST.add(sbWithStrength.toString() + " " + local.getAliasName());
        }
        // Add Exist Drug with Trade Name Alias
        if (drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() != null) {
            TempDtoUtil.EXIST_DRUGS_WITH_TRADE_ALIAS_MAP.put(drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() + " " + sbWithTradeAliasWithStrength.toString(), drug);
        }

        // Add Specialty Drug
        if (TempDtoUtil.EXIST_SPECIALTY_DRUGS.get(drug.getLocalDrugId()) != null) {
            if (drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() != null) {
                TempDtoUtil.EXIST_SPECIALTY_DRUGS.get(drug.getLocalDrugId()).add(sbWithTradeAliasWithStrength.toString());
            }
            TempDtoUtil.EXIST_SPECIALTY_DRUGS.get(drug.getLocalDrugId()).add(sbWithStrength.toString());
        }
    }

    private void addTempParentDrugs(List<MoeDrugDto> dtos) {
        for (MoeDrugDto dto : dtos) {
            if (dto.getTerminologyName() == null) {
                dto.setDisplayNameType(ServerConstant.ALIAS_NAME_TYPE_LOCAL_DRUG);
            }
            Formatter.setDisplayAttributes(dto);
            if (TempDtoUtil.DRUGS_BY_DRUGNAME.get(dto.getShortName()) == null) {
                //TODO  Add Parent Drug by Type with shortName
                TempDtoUtil.DRUGS_BY_DRUGNAME.put(dto.getShortName(), dto);
            }
            //TODO  Add Drug by Type
            addTempDrugsByType(TempDtoUtil.DRUGS_BY_TYPE, dto);
        }
    }

    //Add Temp Drugs By 9 Type
    private void addTempDrugsByType(Map<String, List<MoeDrugDto>> map, MoeDrugDto dto) {
        String aliasNameType = dto.getDisplayNameType();
        if (dto.getTerminologyName() == null) {
            TempDtoUtil.addDtoToMap(map, DrugType.NONMTT.toString(), dto);
        } else if (MoeCommonHelper.isAliasNameTypeVtm(aliasNameType)) {     //"V"
            if (MoeDrugDto.isMultiIngr(dto.getVtm())) {
                TempDtoUtil.addDtoToMap(map, DrugType.MULTIINGR.toString(), dto);
            } else if (MoeCommonHelper.isFlagTrue(dto.getGenericIndicator())) {
                TempDtoUtil.addDtoToMap(map, DrugType.GENERIC.toString(), dto);
            } else {
                if (dto.getVtm() != null) {
                    TempDtoUtil.addDtoToMap(map, DrugType.VTM.toString(), dto);
                }
                if (dto.getTradeName() != null) {
                    TempDtoUtil.addDtoToMap(map, DrugType.TRADENAME.toString(), dto);
                }
            }
        } else if (MoeCommonHelper.isAliasNameTypeBan(aliasNameType)) {     //"B"
            TempDtoUtil.addDtoToMap(map, DrugType.BAN.toString(), dto);
        } else if (MoeCommonHelper.isAliasNameTypeAbb(aliasNameType)) {     //"A"
            TempDtoUtil.addDtoToMap(map, DrugType.ABB.toString(), dto);
        } else if (MoeCommonHelper.isAliasNameTypeOther(aliasNameType)) {   //"O"
            TempDtoUtil.addDtoToMap(map, DrugType.OTHER.toString(), dto);
        } else if (MoeCommonHelper.isAliasNameTypeTradeNameAlias(aliasNameType)) {  //"N"
            TempDtoUtil.addDtoToMap(map, DrugType.TRADENAMEALIAS.toString(), dto);
        }
    }

    private void addTempDosageDrugs(List<MoeDrugDto> dtos) {
        for (MoeDrugDto dto : dtos) {
            if (dto.getTerminologyName() == null) {
                dto.setDisplayNameType(ServerConstant.ALIAS_NAME_TYPE_LOCAL_DRUG);
            }
            Formatter.setDisplayAttributes(dto);
            addTempDrugsByType(TempDtoUtil.DOSAGE_DRUGS_BY_TYPE, dto);
        }
    }

    private void sortTempDrugContent() {
        for (DrugType type : DrugType.values()) {
            DrugSearchEngine.DrugSearchType comparatorType = DrugSearchEngine.DrugSearchType.ASSORTED;
            if (type.equals(DrugType.TRADENAME)) {
                comparatorType = DrugSearchEngine.DrugSearchType.TRADENAME;
            } else if (type.equals(DrugType.GENERIC)) {
                comparatorType = DrugSearchEngine.DrugSearchType.GENERIC;
            } else if (type.equals(DrugType.NONMTT)) {
                comparatorType = DrugSearchEngine.DrugSearchType.NONMTT;
            }

            List<MoeDrugDto> drugs = TempDtoUtil.DRUGS_BY_TYPE.get(type.toString());
            if (drugs != null) {
                Collections.sort(drugs, DrugSearchEngine.COMPARATOR_BY_TYPE.get(comparatorType));
            }

            drugs = TempDtoUtil.DOSAGE_DRUGS_BY_TYPE.get(type.toString());
            if (drugs != null) {
                Collections.sort(drugs, DrugSearchEngine.COMPARATOR_BY_TYPE.get(comparatorType));
            }
        }
    }
    // Ricci 20190722 end --


    public void setSystemSetting() {
        SystemSettingUtil.systemSettingMap = new HashMap<>();
        SystemSettingUtil.displaySettingMap = new HashMap<>();

        List<MoeSystemSettingPo> list = baseRepository.findAll(MoeSystemSettingPo.class);
        for (MoeSystemSettingPo s : list) {
            MoeSystemSettingDto dto = DtoMapUtil.convertMoeSystemSettingDto(s);
            SystemSettingUtil.systemSettingMap.put(s.getParamName(), dto);
            if ("1".equals(s.getParamLevel())) {
                SystemSettingUtil.displaySettingMap.put(s.getParamName(), dto);
            }
        }
    }

    public void setHospitalSetting() {
        SystemSettingUtil.hospitalSettingMaps = new HashMap<>();
        SystemSettingUtil.hospitalDisplaySettingMaps = new HashMap<>();

        List<MoeHospitalSettingPo> list = baseRepository.findAll(MoeHospitalSettingPo.class);
        for (MoeHospitalSettingPo s : list) {
            MoeHospitalSettingDto dto = DtoMapUtil.convertMoeHospitalSettingDto(s);

            String key = s.getHospcode() + " " + s.getUserSpecialty();
            HashMap<String, MoeHospitalSettingDto> smap = SystemSettingUtil.hospitalSettingMaps.get(key);
            if (smap == null) {
                smap = new HashMap<>();
            }
            smap.put(s.getParamName(), dto);
            SystemSettingUtil.hospitalSettingMaps.put(key, smap);
            if ("1".equals(s.getParamLevel())) {
                HashMap<String, MoeHospitalSettingDto> dmap = SystemSettingUtil.hospitalDisplaySettingMaps.get(key);
                if (dmap == null) {
                    dmap = new HashMap<>();
                }
                dmap.put(s.getParamName(), dto);
                SystemSettingUtil.hospitalDisplaySettingMaps.put(key, dmap);
            }
        }
    }

    // Ricci 20190823 moe_drug_server start --
    public void setCommonDosageType() {
        if (!DtoUtil.COMMON_DOSAGE_TYPE_LIST.isEmpty()) {
            return;
        }
        List<MoeCommonDosageTypePo> list = moeCommonDosageTypeRepository.findAllByDisplay(ServerConstant.DB_FLAG_TRUE);
        for (MoeCommonDosageTypePo po : list) {
            DtoUtil.COMMON_DOSAGE_TYPE_LIST.add(hk.health.moe.util2.DtoMapUtil.convertMoeCommonDosageTypeDto(po));
            DtoUtil.COMMON_DOSAGE_TYPE_ID_MAP.put(po.getCommonDosageType(), po);
        }
    }

    public void setTempCommonDosageType() {
        if (!TempDtoUtil.COMMON_DOSAGE_TYPE_LIST.isEmpty()) {
            return;
        }
        List<MoeCommonDosageTypePo> list = moeCommonDosageTypeRepository.findAllByDisplay(ServerConstant.DB_FLAG_TRUE);
        for (MoeCommonDosageTypePo po : list) {
            TempDtoUtil.COMMON_DOSAGE_TYPE_LIST.add(hk.health.moe.util2.DtoMapUtil.convertMoeCommonDosageTypeDto(po));
            TempDtoUtil.COMMON_DOSAGE_TYPE_ID_MAP.put(po.getCommonDosageType(), po);
        }
    }
    // Ricci 20190823 moe_drug_server end --


    //Chris 20200110  Set Parent Drug of Generic Name Level  -Start
    public void setGenericNameContent() {
        if (!DtoUtil.GENERIC_NAME_DRUGS.isEmpty()) {
            return;
        }

        List<MoeDrugGenericNameLocalPo> moeDrugGenericNameLocalPoList = moeDrugGenericNameLocalRepository.findAll();

        for (MoeDrugGenericNameLocalPo moeDrugGenericNameLocalPo : moeDrugGenericNameLocalPoList) {

//            MoeDrugGenericNameLocalDto dto = DtoMapUtil.convertMoeDrugGenericNameLocalDto(moeDrugGenericNameLocalPo);
            MoeDrugDto dto = DtoMapUtil.convertMoeDrugDto(moeDrugGenericNameLocalPo);

            DtoUtil.GENERIC_NAME_DRUGS.add(dto);
        }

    }

    public void setTempGenericNameContent() {
        if (!TempDtoUtil.GENERIC_NAME_DRUGS.isEmpty()) {
            return;
        }

        List<MoeDrugGenericNameLocalPo> moeDrugGenericNameLocalPoList = moeDrugGenericNameLocalRepository.findAll();

        for (MoeDrugGenericNameLocalPo moeDrugGenericNameLocalPo : moeDrugGenericNameLocalPoList) {

//            MoeDrugGenericNameLocalDto dto = DtoMapUtil.convertMoeDrugGenericNameLocalDto(moeDrugGenericNameLocalPo);
            MoeDrugDto dto = DtoMapUtil.convertMoeDrugDto(moeDrugGenericNameLocalPo);

            TempDtoUtil.GENERIC_NAME_DRUGS.add(dto);
        }

    }
    //Chris 20200110  Set Parent Drug of Generic Name Level  -End

}
