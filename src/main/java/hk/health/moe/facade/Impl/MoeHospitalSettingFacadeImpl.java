package hk.health.moe.facade.Impl;

import hk.health.moe.facade.MoeHospitalSettingFacade;
import hk.health.moe.pojo.dto.MoeHospitalSettingDto;
import hk.health.moe.pojo.po.MoeHospitalSettingPo;
import hk.health.moe.repository.MoeHospitalSettingRepository;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MoeHospitalSettingFacadeImpl implements MoeHospitalSettingFacade {

    @Autowired
    private MoeHospitalSettingRepository moeHospitalSettingRepository;

    @Autowired
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public List<MoeHospitalSettingDto> getAllSpecialtyAvailability() throws Exception {
        List<MoeHospitalSettingPo> records = moeHospitalSettingRepository.getAllSpecialtyAvailability();
        List<MoeHospitalSettingDto> recordsDTO = new ArrayList<MoeHospitalSettingDto>();
        if (records != null) {
            for (MoeHospitalSettingPo record : records) {
                recordsDTO.add(dozerBeanMapper.map(record, MoeHospitalSettingDto.class));
            }
        }

        return recordsDTO;
    }

    @Override
    public List<MoeHospitalSettingDto> getAllLocationByUserGroupMap() throws Exception {
        List<MoeHospitalSettingPo> records = moeHospitalSettingRepository.getAllLocationByUserGroupMap();
        List<MoeHospitalSettingDto> recordsDTO = new ArrayList<MoeHospitalSettingDto>();
        if (records != null) {
            for (MoeHospitalSettingPo record : records) {
                recordsDTO.add(dozerBeanMapper.map(record, MoeHospitalSettingDto.class));
            }
        }

        return recordsDTO;
    }

    @Override
    public List<MoeHospitalSettingDto> getAllLocationByUserGroupMap(String hospCode) throws Exception {
        List<MoeHospitalSettingPo> records = moeHospitalSettingRepository.getAllLocationByUserGroupMap(hospCode);
        List<MoeHospitalSettingDto> recordsDTO = new ArrayList<MoeHospitalSettingDto>();
        if (records != null) {
            for (MoeHospitalSettingPo record : records) {
                recordsDTO.add(dozerBeanMapper.map(record, MoeHospitalSettingDto.class));
            }
        }

        return recordsDTO;
    }
}
