package hk.health.moe.facade.Impl;

import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.facade.MoeDrugToSaamFacade;
import hk.health.moe.pojo.dto.MoeDrugLocalDto;
import hk.health.moe.pojo.dto.inner.InnerSaveDrugDto;
import hk.health.moe.pojo.po.MoeDrugToSaamLocalLogPo;
import hk.health.moe.pojo.po.MoeDrugToSaamLocalPo;
import hk.health.moe.repository.MoeDrugToSaamLocalLogRepository;
import hk.health.moe.repository.MoeDrugToSaamLocalRepository;
import hk.health.moe.util.Formatter;
import hk.health.moe.util.LocaleMessageSourceUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MoeDrugToSaamFacadeImpl implements MoeDrugToSaamFacade {

    @Autowired
    private MoeDrugToSaamLocalRepository moeDrugToSaamLocalRepository;

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    @Autowired
    private MoeDrugToSaamLocalLogRepository moeDrugToSaamLocalLogRepository;

    @Override
    public void saveDrugDetail(InnerSaveDrugDto dto) throws Exception {

        // MoeDrugLocal
        MoeDrugLocalDto moeDrugLocalDto = dto.getMoeDrugLocal();

        if (dto.isAddToSAAM()) {
            if (moeDrugToSaamLocalRepository.existsById(dto.getLocalDrugId())) {
                MoeDrugToSaamLocalPo local = new MoeDrugToSaamLocalPo();
                local.setLocalDrugId(dto.getLocalDrugId());
                local.setCreateUserId(moeDrugLocalDto.getCreateUserId());
                local.setCreateUser(moeDrugLocalDto.getCreateUser());
                local.setCreateHosp(moeDrugLocalDto.getCreateHosp());
                local.setCreateRank(moeDrugLocalDto.getCreateRank());
                local.setCreateRankDesc(moeDrugLocalDto.getCreateRankDesc());
                local.setCreateDtm(moeDrugLocalDto.getCreateDtm());
                local.setUpdateUserId(moeDrugLocalDto.getUpdateUserId());
                local.setUpdateUser(moeDrugLocalDto.getUpdateUser());
                local.setUpdateHosp(moeDrugLocalDto.getUpdateHosp());
                local.setUpdateRank(moeDrugLocalDto.getCreateRank());
                local.setUpdateRankDesc(moeDrugLocalDto.getUpdateRankDesc());
                local.setUpdateDtm(moeDrugLocalDto.getUpdateDtm());
                insertMoeDrugToSaamLocal(local);
            }
        } else {
            deleteDrugDetail(moeDrugLocalDto.getLocalDrugId());
        }
    }

    private void insertMoeDrugToSaamLocal(MoeDrugToSaamLocalPo record) throws Exception {

        if (record == null || StringUtils.isEmpty(record.getLocalDrugId())) {
            //eric 20191227 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.error"));
            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage());
            //eric 20191227 end--
        }

        moeDrugToSaamLocalRepository.save(Formatter.emptyToNull(record));
        moeDrugToSaamLocalLogRepository.save(new MoeDrugToSaamLocalLogPo(ServerConstant.RECORD_INSERT, record));

    }

    @Override
    public void deleteDrugDetail(String localDrugId) {
        Optional<MoeDrugToSaamLocalPo> optionalMoeDrugToSaamLocalPo = moeDrugToSaamLocalRepository.findById(localDrugId);
        if (optionalMoeDrugToSaamLocalPo.isPresent()) {
            MoeDrugToSaamLocalPo record = optionalMoeDrugToSaamLocalPo.get();
            moeDrugToSaamLocalLogRepository.save(new MoeDrugToSaamLocalLogPo(ServerConstant.RECORD_DELETE, record));
            moeDrugToSaamLocalRepository.delete(record);
        }
    }

}
