package hk.health.moe.facade.Impl;
/**
 * @author Simon
 * @version 1.0
 * @date 2019/8/22
 * for moe_drug_server
 */

import hk.health.moe.common.ServerConstant;
import hk.health.moe.facade.MoeDrugFacade;
import hk.health.moe.pojo.dto.DrugDetailDto;
import hk.health.moe.pojo.dto.MoeDrugAliasNameLocalDto;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.MoeDrugLocalDto;
import hk.health.moe.pojo.dto.MoeDrugOrdPropertyLocalDto;
import hk.health.moe.pojo.dto.MoeDrugStrengthDto;
import hk.health.moe.pojo.dto.MoeDrugStrengthLocalDto;
import hk.health.moe.pojo.po.MoeDrugAliasNameLocalPo;
import hk.health.moe.pojo.po.MoeDrugAliasNamePo;
import hk.health.moe.pojo.po.MoeDrugLocalPo;
import hk.health.moe.pojo.po.MoeDrugOrdPropertyLocalPo;
import hk.health.moe.pojo.po.MoeDrugPo;
import hk.health.moe.pojo.po.MoeDrugStrengthLocalPo;
import hk.health.moe.pojo.po.MoeDrugStrengthPo;
import hk.health.moe.pojo.po.MoeDrugStrengthPoPK;
import hk.health.moe.repository.MoeDrugAliasNameLocalRepository;
import hk.health.moe.repository.MoeDrugAliasNameRepository;
import hk.health.moe.repository.MoeDrugLocalRepository;
import hk.health.moe.repository.MoeDrugOrdPropertyLocalRepository;
import hk.health.moe.repository.MoeDrugRepository;
import hk.health.moe.repository.MoeDrugStrengthLocalRepository;
import hk.health.moe.repository.MoeDrugStrengthRepository;
import hk.health.moe.util2.DtoMapUtil;
import hk.health.moe.util2.Formatter;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class MoeDrugFacadeImpl implements MoeDrugFacade {

    @Autowired
    private MoeDrugLocalRepository moeDrugLocalRepository;
    @Autowired
    private MoeDrugRepository moeDrugRepository;

    @Autowired
    private MoeDrugStrengthRepository moeDrugStrengthRepository;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private MoeDrugAliasNameLocalRepository moeDrugAliasNameLocalRepository;

    @Autowired
    private MoeDrugAliasNameRepository moeDrugAliasNameRepository;

    @Autowired
    private MoeDrugStrengthLocalRepository moeDrugStrengthLocalRepository;

    @Autowired
    private MoeDrugOrdPropertyLocalRepository moeDrugOrdPropertyLocalRepository;


    //Simon 20190822 for moe_drug_server start--
    @Override
    public boolean isLocalDrugKeyValid(boolean isNewARecord,
                                       String localDrugId, String hkRegNo) {
        if (isNewARecord && StringUtils.isNotEmpty(localDrugId)) {
            boolean isLocalDrugIdExist = moeDrugLocalRepository.isLocalDrugIdExist(localDrugId).intValue() > 0;
            // Check if input localDrugId is exist in DB
            if (!StringUtils.isEmpty(localDrugId) && isLocalDrugIdExist) {
                return true;
            }
        }

        if (!isNewARecord && StringUtils.isNotEmpty(localDrugId) && StringUtils.isNotEmpty(hkRegNo)) {
            boolean isValid = moeDrugLocalRepository.isLocalDrugIdHkRegNoCombinationExist(localDrugId, hkRegNo).intValue() > 0;

            // Check if input localDrugId and hkRegNo are exist in DB
            if (!StringUtils.isEmpty(localDrugId) && !StringUtils.isEmpty(hkRegNo)) {
                return isValid;
            }
        }
        return false;
    }

    /**
     * PURPOSE		:	check if these parameters combination valid
     *
     * @param isNewARecord :	indicate new a record or search record
     * @param localDrugId
     * @param hkRegNo
     * @param strengthRank :	the value of rank in moe_drug_strength table
     *                     only used when isNewRecord is true
     * @return :	true if these combination are valid
     */
    @Override
    public boolean isDrugParaCombinationValid(boolean isNewARecord,
                                              String localDrugId, String hkRegNo, Long strengthRank) {

        if (isNewARecord && StringUtils.isNotEmpty(localDrugId)
                && StringUtils.isNotEmpty(hkRegNo)) {

            // check if localDrugId exist in moe_drug_local, if exist, return false
            boolean isLocalDrugIdExist = moeDrugLocalRepository.isLocalDrugIdExist(localDrugId) > 0;
            if (isLocalDrugIdExist) {
                return false;
            }

            boolean isHkRegNoExist = StringUtils.isNotEmpty(hkRegNo) ? moeDrugRepository.isHkRegNoExist(hkRegNo) > 0 : false;

            if (isHkRegNoExist && strengthRank != null) {
                // check if hkRegNo strength have particular strengthRank, if not exist, return false
                MoeDrugStrengthPoPK poPK = new MoeDrugStrengthPoPK();
                poPK.setHkRegNo(hkRegNo);
                poPK.setRank(strengthRank);
                Optional<MoeDrugStrengthPo> byId = moeDrugStrengthRepository.findById(poPK);
                MoeDrugStrengthPo moeDrugStrength = null;
                if (byId.isPresent())
                    moeDrugStrength = byId.get();
                if (moeDrugStrength == null) {
                    return false;
                }

                // if strength value is inside local strength, return false
                String strength = moeDrugStrength.getStrength();
                String strengthLevelExtraInfo = moeDrugStrength.getStrengthLevelExtraInfo();
                boolean isStrengthLocalExist = moeDrugLocalRepository.isLocalStrengthExist(hkRegNo, strength, strengthLevelExtraInfo) > 0;
                if (isStrengthLocalExist) {
                    return false;
                }
            } else if (isHkRegNoExist && strengthRank == null) {
                // check if any hkRegNo related strength not in strength local, if not, return false
                boolean isRecordNotInLocal = moeDrugStrengthRepository.getNumRecordNotInLocal(hkRegNo) > 0;
                if (!isRecordNotInLocal) {
                    return false;
                }
            }

            return true;

        } else if (isNewARecord && StringUtils.isNotEmpty(localDrugId)
                && StringUtils.isEmpty(hkRegNo)) {
            // check if localDrugId exist in moe_drug_local, if exist, return false
            boolean isLocalDrugIdExist = moeDrugLocalRepository.isLocalDrugIdExist(localDrugId) > 0;

            if (isLocalDrugIdExist) {
                return false;
            }

            return true;

        } else if (!isNewARecord && StringUtils.isNotEmpty(localDrugId)
                && StringUtils.isNotEmpty(hkRegNo)) {
            // check if localDrugId & hkRegNo exist in local drug table, if not, return false
            boolean isLocalDrugIdHkRegNoRecordExist = moeDrugLocalRepository.isLocalDrugIdHkRegNoCombinationExist(localDrugId, hkRegNo) > 0;
            if (!isLocalDrugIdHkRegNoRecordExist) {
                return false;
            }

            return true;

        } else if (!isNewARecord && StringUtils.isNotEmpty(localDrugId)
                && StringUtils.isEmpty(hkRegNo)) {
            // check if localDrugId exist in local drug table, if not, return false
            boolean isLocalDrugIdRecordExist = moeDrugLocalRepository.isLocalDrugIdExist(localDrugId) > 0;
            if (!isLocalDrugIdRecordExist) {
                return false;
            }

            return true;

        } else if (!isNewARecord && StringUtils.isEmpty(localDrugId)
                && StringUtils.isNotEmpty(hkRegNo)) {
            // check if there are records with same hkRegNo exist in drug local, if not, return false
            boolean isHkRegNoRecordExist = moeDrugLocalRepository.getNumRecordsByHkRegNo(hkRegNo) > 0;
            if (!isHkRegNoRecordExist) {
                return false;
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * PURPOSE		:	prepare drug local record and return
     *
     * @param localDrugId
     * @param hkRegNo
     * @param strengthRank
     * @return DrugDetailDTO
     */
    @Override
    public DrugDetailDto newDrugLocalRecord(String localDrugId, String hkRegNo, Long strengthRank) {

        // check if return strength list for user to choose
        boolean isHkRegNoExist = StringUtils.isNotEmpty(hkRegNo) ? moeDrugRepository.isHkRegNoExist(hkRegNo) > 0 : false;
        boolean isChooseStrength = StringUtils.isNotEmpty(hkRegNo) && strengthRank == null && isHkRegNoExist;

        if (isChooseStrength) {
            int numRecordNotInLocal = moeDrugStrengthRepository.getNumRecordNotInLocal(hkRegNo);
            if (numRecordNotInLocal <= 1) {
                isChooseStrength = false;
            }
        }

        if (isChooseStrength) {
            // prepare strength list to choose
            List<MoeDrugStrengthPo> moeDrugStrengthList = moeDrugStrengthRepository.findRecordNotInLocal(hkRegNo);
            List<MoeDrugStrengthDto> moeDrugStrengthDTOList = new ArrayList<MoeDrugStrengthDto>();

            for (MoeDrugStrengthPo record : moeDrugStrengthList) {
                moeDrugStrengthDTOList.add(mapper.map(record, MoeDrugStrengthDto.class));
            }

            DrugDetailDto drugDetailDto = new DrugDetailDto();
            drugDetailDto.setLocalDrugId(localDrugId);
            drugDetailDto.setHkRegNo(hkRegNo);
            drugDetailDto.setMoeDrugStrengthList(moeDrugStrengthDTOList);

            return drugDetailDto;
        } else {
            // prepare drug detail dto return
            DrugDetailDto drugDetailDto = new DrugDetailDto();
            drugDetailDto.setLocalDrugId(localDrugId);
            drugDetailDto.setHkRegNo(hkRegNo);

            if (isHkRegNoExist) {
                drugDetailDto.setLocalDrugOnly(false);

                // Set MoeDrugLocalDTO
                MoeDrugLocalPo moeDrugLocal = null;
                if (strengthRank != null) {
                    MoeDrugPo moeDrug = moeDrugRepository.findByHkRegNoAndRank(hkRegNo, strengthRank);
                    moeDrugLocal = new MoeDrugLocalPo(localDrugId, moeDrug);
                } else {
                    List<MoeDrugPo> moeDrugPos = moeDrugRepository.findByHkRegNo(hkRegNo);
                    moeDrugLocal = new MoeDrugLocalPo(localDrugId, moeDrugPos.get(0));
                }
                MoeDrugLocalDto moeDrugLocalDto = mapper.map(moeDrugLocal, MoeDrugLocalDto.class);
                drugDetailDto.setMoeDrugLocal(moeDrugLocalDto);

                drugDetailDto.getMoeDrugLocal().setTerminologyName(ServerConstant.TERMINOLOGY_NAME);

                // Set List<MoeDrugAliasNameLocalDTO>
                List<MoeDrugAliasNameLocalPo> moeDrugAliasNameLocalList = moeDrugAliasNameLocalRepository.findByHkRegNo(hkRegNo);
                boolean isAliasNameLocalExist = moeDrugAliasNameLocalList != null && moeDrugAliasNameLocalList.size() > 0;
                if (!isAliasNameLocalExist) {
                    List<MoeDrugAliasNamePo> moeDrugAliasNameList = moeDrugAliasNameRepository.findByHkRegNo(hkRegNo);
                    moeDrugAliasNameLocalList = new ArrayList<MoeDrugAliasNameLocalPo>();
                    boolean isMoeDrugAliasNameListValid = moeDrugAliasNameList != null && moeDrugAliasNameList.size() > 0;
                    if (isMoeDrugAliasNameListValid) {
                        for (MoeDrugAliasNamePo record : moeDrugAliasNameList) {
                            moeDrugAliasNameLocalList.add(new MoeDrugAliasNameLocalPo(record));
                        }
                    }
                }

                List<MoeDrugAliasNameLocalDto> moeDrugAliasNameLocalDTOList = new ArrayList<MoeDrugAliasNameLocalDto>();
                if (moeDrugAliasNameLocalList.size() > 0) {
                    for (MoeDrugAliasNameLocalPo record : moeDrugAliasNameLocalList) {
                        moeDrugAliasNameLocalDTOList.add(mapper.map(record, MoeDrugAliasNameLocalDto.class));
                    }
                }
                drugDetailDto.setMoeDrugAliasNameLocalList(moeDrugAliasNameLocalDTOList);

                // Set MoeDrugStrengthLocalDTO
                MoeDrugStrengthPo moeDrugStrength = null;
                if (strengthRank != null) {
                    MoeDrugStrengthPoPK poPK = new MoeDrugStrengthPoPK();
                    poPK.setHkRegNo(hkRegNo);
                    poPK.setRank(strengthRank);
                    Optional<MoeDrugStrengthPo> byId = moeDrugStrengthRepository.findById(poPK);
                    if (byId.isPresent())
                        moeDrugStrength = byId.get();
                } else {
                    List<MoeDrugStrengthPo> moeDrugStrengthList = moeDrugStrengthRepository.findRecordNotInLocal(hkRegNo);
                    boolean isMoeDrugStrengthListValid = moeDrugStrengthList != null && moeDrugStrengthList.size() > 0;
                    if (isMoeDrugStrengthListValid) {
                        moeDrugStrength = moeDrugStrengthList.get(0);
                    }
                }
                MoeDrugStrengthLocalPo moeDrugStrengthLocal = null;
                if (moeDrugStrength != null) {
                    moeDrugStrengthLocal = new MoeDrugStrengthLocalPo(localDrugId, moeDrugStrength);
                } else {
                    moeDrugStrengthLocal = new MoeDrugStrengthLocalPo();
                    moeDrugStrengthLocal.setLocalDrugId(localDrugId);
                }
                MoeDrugStrengthLocalDto moeDrugStrengthLocalDto = mapper.map(moeDrugStrengthLocal, MoeDrugStrengthLocalDto.class);
                drugDetailDto.setMoeDrugStrengthLocal(moeDrugStrengthLocalDto);

            } else {    // only have localDrugId provided
                drugDetailDto.setLocalDrugOnly(true);

                // Set MoeDrugLocalDTO
                MoeDrugLocalDto moeDrugLocalDto = new MoeDrugLocalDto();
                moeDrugLocalDto.setLocalDrugId(localDrugId);
                moeDrugLocalDto.setHkRegNo(hkRegNo);
                drugDetailDto.setMoeDrugLocal(moeDrugLocalDto);

                drugDetailDto.getMoeDrugLocal().setTerminologyName(null);

                // Set List<MoeDrugAliasNameLocalDTO
                drugDetailDto.setMoeDrugAliasNameLocalList(new ArrayList<MoeDrugAliasNameLocalDto>());

                // Set MoeDrugStrengthLocalDTO
                MoeDrugStrengthLocalDto moeDrugStrengthLocalDto = new MoeDrugStrengthLocalDto();
                moeDrugStrengthLocalDto.setLocalDrugId(localDrugId);
                drugDetailDto.setMoeDrugStrengthLocal(moeDrugStrengthLocalDto);

            }

            // Common in both case
            // Set MoeDrugOrdPropertyLocalDTO
            MoeDrugOrdPropertyLocalDto moeDrugOrdPropertyLocalDto = new MoeDrugOrdPropertyLocalDto(localDrugId, "N", "N", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, false, false);
            drugDetailDto.setMoeDrugOrdPropertyLocal(moeDrugOrdPropertyLocalDto);

            return drugDetailDto;
        }
    }

    @Override
    public List<MoeDrugDto> findDrugByHkRegNo(String hkRegNo, Long rank) {

        List<MoeDrugPo> moeDrug = null;
        moeDrug = findMoeDrugByHkRegNo(hkRegNo);

        if (moeDrug == null) {
            return null;
        }

        List<MoeDrugDto> dtos = new ArrayList<MoeDrugDto>();

        for (MoeDrugPo drug : moeDrug) {
/*			for(MoeDrugStrength strength : drug.getMoeDrugStrengths()) {
				dtos.addAll(DtoMapUtil.convertMoeDrugDTOWithAllStrengths(drug, strength));
			}*/
            // Ricci 20190902 start --
            //dtos.addAll(DtoMapUtil.convertMoeDrugDTOWithAllStrengths(drug, drug.getMoeDrugStrengths()));
            dtos.addAll(DtoMapUtil.convertMoeDrugDTOWithAllStrengths(drug, drug.getMoeDrugStrength()));
            // Ricci 20190902 end --
        }
        return dtos;
    }

    @Override
    public List<MoeDrugPo> findMoeDrugByHkRegNo(String hkRegNo) {
        List<MoeDrugPo> dtoList = moeDrugRepository.findByHkRegNo(hkRegNo);
        if (dtoList == null) {
            return null;
        }

        List<MoeDrugPo> returnList = new ArrayList<>();
        for (MoeDrugPo d : dtoList) {
            MoeDrugStrengthPo strength = moeDrugStrengthRepository.findByHkRegNoAndRank(hkRegNo, d.getRank());
            boolean ampExist = moeDrugStrengthLocalRepository.isAmpExist(strength.getAmp()) > 0;
            if (!ampExist) {
                // Ricci 20190902 start --
                d.setMoeDrugStrength(strength);
                //d.getMoeDrugStrengths().add(strength);
                // Ricci 20190902 end --
                returnList.add(d);
            }
        }
        return returnList;
    }
    //Simon 20190822 for moe_drug_server end--

    //Simon 20190823 for moe_drug_server start--

    /**
     * PURPOSE		:	get drug local record
     *
     * @param localDrugId
     * @param hkRegNo
     * @return DrugDetailDTO
     */
    @Override
    public DrugDetailDto getDrugLocalRecord(String localDrugId, String hkRegNo) {

        // consider prepare drug info and return
        // or prepare strength list and return
        boolean isChooseStrength = StringUtils.isEmpty(localDrugId);
        if (isChooseStrength) {
            int numOfHkRegNoRecords = moeDrugLocalRepository.getNumRecordsByHkRegNo(hkRegNo);
            if (numOfHkRegNoRecords == 0) {
                return null;
            } else if (numOfHkRegNoRecords == 1) {
                isChooseStrength = false;
            }
        }

        if (isChooseStrength) {
            // prepare strength list for use to choose
            List<MoeDrugStrengthLocalPo> moeDrugStrengthLocalList = moeDrugStrengthLocalRepository.findByHkRegNo(hkRegNo);
            boolean isMoeDrugStrengthLocalListValid = moeDrugStrengthLocalList != null && moeDrugStrengthLocalList.size() > 1;
            if (!isMoeDrugStrengthLocalListValid) {
                // the moeDrugStrengthLocalList size should be > 1
                // otherwise, data problem occurred
                return null;
            }

            List<MoeDrugStrengthLocalDto> moeDrugStrengthLocalDTOList = new ArrayList<MoeDrugStrengthLocalDto>();
            for (MoeDrugStrengthLocalPo record : moeDrugStrengthLocalList) {
                moeDrugStrengthLocalDTOList.add(mapper.map(record, MoeDrugStrengthLocalDto.class));
            }

            DrugDetailDto drugDetailDto = new DrugDetailDto();
            drugDetailDto.setHkRegNo(hkRegNo);
            drugDetailDto.setMoeDrugStrengthLocalList(moeDrugStrengthLocalDTOList);

            return drugDetailDto;

        } else {
            // prepare drug information
            MoeDrugLocalPo moeDrugLocal = null;
            Optional<MoeDrugLocalPo> byId = null;
            if (StringUtils.isNotEmpty(localDrugId)) {
                byId = moeDrugLocalRepository.findById(localDrugId);
                if (byId.isPresent())
                    moeDrugLocal = byId.get();
            } else if (StringUtils.isEmpty(localDrugId) && StringUtils.isNotEmpty(hkRegNo)) {
                moeDrugLocal = moeDrugLocalRepository.findByHkRegNo(hkRegNo).get(0);
            } else {
                return null;
            }

            if (moeDrugLocal == null) {
                return null;
            }

            String localDrugIdValue = moeDrugLocal.getLocalDrugId();
            String hkRegNoValue = moeDrugLocal.getHkRegNo();

            boolean isHkRegNoExist = StringUtils.isNotEmpty(hkRegNoValue) ? "HKCTT".equalsIgnoreCase(moeDrugLocal.getTerminologyName()) : false;

            List<MoeDrugAliasNameLocalPo> moeDrugAliasNameLocalList = moeDrugAliasNameLocalRepository.findByHkRegNo(moeDrugLocal.getHkRegNo());
            boolean isMoeDrugAliasNameLocalListValid = moeDrugAliasNameLocalList != null && moeDrugAliasNameLocalList.size() > 0;
            List<MoeDrugAliasNameLocalDto> moeDrugAliasNameLocalDTOList = new ArrayList<MoeDrugAliasNameLocalDto>();
            if (isMoeDrugAliasNameLocalListValid) {
                for (MoeDrugAliasNameLocalPo record : moeDrugAliasNameLocalList) {
                    moeDrugAliasNameLocalDTOList.add(mapper.map(record, MoeDrugAliasNameLocalDto.class));
                }
            }

            MoeDrugOrdPropertyLocalPo moeDrugOrdPropertyLocal = null;
            Optional<MoeDrugOrdPropertyLocalPo> byId1 = moeDrugOrdPropertyLocalRepository.findById(localDrugIdValue);
            if (byId1.isPresent())
                moeDrugOrdPropertyLocal = byId1.get();

            if (moeDrugOrdPropertyLocal == null) {
                moeDrugOrdPropertyLocal = new MoeDrugOrdPropertyLocalPo();
                moeDrugOrdPropertyLocal.setLocalDrugId(localDrugIdValue);
            }
            MoeDrugStrengthLocalPo moeDrugStrengthLocal = null;
            Optional<MoeDrugStrengthLocalPo> byId2 = moeDrugStrengthLocalRepository.findById(localDrugIdValue);
            if (byId2.isPresent())
                moeDrugStrengthLocal = byId2.get();
            if (moeDrugStrengthLocal == null) {
                moeDrugStrengthLocal = new MoeDrugStrengthLocalPo();
                moeDrugStrengthLocal.setLocalDrugId(localDrugIdValue);
            }

            DrugDetailDto drugDetailDto = new DrugDetailDto();
            drugDetailDto.setLocalDrugId(localDrugIdValue);
            drugDetailDto.setHkRegNo(moeDrugLocal.getHkRegNo());
            if (isHkRegNoExist) {
                drugDetailDto.setLocalDrugOnly(false);
            } else {
                drugDetailDto.setLocalDrugOnly(true);
            }
            drugDetailDto.setMoeDrugLocal(mapper.map(moeDrugLocal, MoeDrugLocalDto.class));
            drugDetailDto.setMoeDrugAliasNameLocalList(moeDrugAliasNameLocalDTOList);
            drugDetailDto.setMoeDrugOrdPropertyLocal(mapper.map(moeDrugOrdPropertyLocal, MoeDrugOrdPropertyLocalDto.class));
            drugDetailDto.setMoeDrugStrengthLocal(mapper.map(moeDrugStrengthLocal, MoeDrugStrengthLocalDto.class));

            return drugDetailDto;
        }
    }

    @Override
    public List<MoeDrugDto> findById(String id) {
        Optional<MoeDrugLocalPo> byId = moeDrugLocalRepository.findById(id);
        MoeDrugLocalPo moeDrugLocal = null;
        if (byId.isPresent())
            moeDrugLocal = byId.get();
        if (moeDrugLocal == null) {
            return null;
        }
        if (moeDrugLocal.getMoeDrugStrengths() != null && moeDrugLocal.getMoeDrugStrengths().size() > 0) {
            Iterator<MoeDrugStrengthLocalPo> iterator = moeDrugLocal.getMoeDrugStrengths().iterator();
            return DtoMapUtil.convertMoeDrugDTOWithAllStrengths(moeDrugLocal, iterator.next());
        } else {
            return null;
        }
    }

    @Override
    public List<MoeDrugDto> findByHkRegNo(String hkRegNo) {

        List<MoeDrugLocalPo> moeDrugLocal = moeDrugLocalRepository.findByHkRegNo(hkRegNo);
        if (moeDrugLocal == null) {
            return null;
        }

        List<MoeDrugDto> dtos = new ArrayList<MoeDrugDto>();

        for (MoeDrugLocalPo drug : moeDrugLocal) {
            for (MoeDrugStrengthLocalPo strength : drug.getMoeDrugStrengths()) {
                dtos.addAll(DtoMapUtil.convertMoeDrugDTOWithAllStrengths(drug, strength));
            }
        }
        return dtos;
    }
    //Simon 20190823 for moe_drug_server end--


    //Simon 20190828 start--
    @Override
    public List<MoeDrugDto> listRelatedDrug(String localDrugId, String vtm, String tradeName, String routeEng, String formEng, String doseFormExtraInfo, String hospCode) {
        StringUtils.trimToEmpty(localDrugId);
        String vtmValue = StringUtils.isEmpty(vtm) ? "#" : vtm;
        String tradeNameValue = StringUtils.isEmpty(tradeName) ? "#" : tradeName;
        String routeEngValue = StringUtils.isEmpty(routeEng) ? "#" : routeEng;
        String formEngValue = StringUtils.isEmpty(formEng) ? "#" : formEng;
        String doseFormExtraInfoValue = StringUtils.isEmpty(doseFormExtraInfo) ? "#" : doseFormExtraInfo;

        List<MoeDrugLocalPo> moeDrugLocal = new ArrayList<>();
        if (!StringUtils.isEmpty(hospCode)) {
            moeDrugLocal = moeDrugLocalRepository.listRelatedDrugWithHospitalCode(localDrugId, vtmValue, tradeNameValue, routeEngValue, formEngValue, doseFormExtraInfoValue, hospCode);
        } else {
            moeDrugLocal = moeDrugLocalRepository.listRelatedDrugWithoutHospitalCode(localDrugId, vtmValue, tradeNameValue, routeEngValue, formEngValue, doseFormExtraInfoValue);
        }
        if (moeDrugLocal == null) {
            return null;
        }
        List<MoeDrugDto> dtos = new ArrayList<MoeDrugDto>();
        for (MoeDrugLocalPo drug : moeDrugLocal) {
            for (MoeDrugStrengthLocalPo strength : drug.getMoeDrugStrengths()) {
                dtos.addAll(DtoMapUtil.convertMoeDrugDTOWithAllStrengths(drug, strength));
            }

        }
        return dtos;
    }
    //Simon 20190828 end--


    //Chris 20190829 moe_drug_server  --Start
    @Override
    public List<MoeDrugDto> findByAmp(String amp) {

        //Chris 20190829 From moe_drug_server  moeDrugLocalDao  --Start
        String ampNew = amp;
        if (amp.contains("%")) {
            ampNew = amp.replace("%", "\\%");
        } else if (amp.contains("_")) {
            ampNew = amp.replace("_", "\\_");
        }
        ampNew = "%" + ampNew.toLowerCase() + "%";

        List<MoeDrugLocalPo> moeDrugLocal = moeDrugLocalRepository.findByAmp(ampNew);

        moeDrugLocal = !moeDrugLocal.isEmpty() ? moeDrugLocal : null;
        //Chris 20190829 From moe_drug_server  moeDrugLocalDao  --End

        if (moeDrugLocal == null) {
            return null;
        }

        List<MoeDrugDto> ampStartWithList = new ArrayList<MoeDrugDto>();
        List<MoeDrugDto> containsList = new ArrayList<MoeDrugDto>();
        List<MoeDrugDto> matchTradeNameAliasList = new ArrayList<MoeDrugDto>();
        List<MoeDrugDto> containsTradeNameAliasList = new ArrayList<MoeDrugDto>();
        List<MoeDrugDto> dtos = new ArrayList<MoeDrugDto>();

        for (MoeDrugLocalPo drug : moeDrugLocal) {
            for (MoeDrugStrengthLocalPo strength : drug.getMoeDrugStrengths()) {
                //Chris 20190829  DrugMaintenanceDtoMapUtil  --Start
                //(DrugMaintenanceMoeDrugDto record : DrugMaintenanceDtoMapUtil.convertMoeDrugDTOWithAllStrengths(drug, strength)) {
                //for (MoeDrugDto record : DtoMapUtil.convertMoeDrugDTOWithAllStrengths(drug, strength)) {
                for (MoeDrugDto record : DtoMapUtil.convertMoeDrugDTOWithAllStrengths(drug, strength)) {
                    //Chris 20190829  DrugMaintenanceDtoMapUtil  --End
                    if (drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() != null && drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias().toLowerCase().startsWith(amp.toLowerCase())) {
                        //Chris 20190829  DrugMaintenanceFormatter  --Start
                        //DrugMaintenanceFormatter.setTradeNameAliasString(record, drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias());
                        Formatter.setTradeNameAliasString(record, drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias());
                        //Chris 20190829  DrugMaintenanceFormatter  --End
                        matchTradeNameAliasList.add(record);
                    } else if (drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() != null && drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias().toLowerCase().contains(amp.toLowerCase())) {
                        //Chris 20190829  DrugMaintenanceFormatter  --Start
                        //DrugMaintenanceFormatter.setTradeNameAliasString(record, drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias());
                        Formatter.setTradeNameAliasString(record, drug.getMoeDrugOrdPropertyLocal().getTradeNameAlias());
                        //Chris 20190829  DrugMaintenanceFormatter  --End
                        containsTradeNameAliasList.add(record);
                    } else if (record.getDisplayString().startsWith(amp)) {
                        ampStartWithList.add(record);
                    } else {
                        containsList.add(record);
                    }
                }
            }
        }

        dtos.addAll(matchTradeNameAliasList);
        dtos.addAll(containsTradeNameAliasList);
        dtos.addAll(ampStartWithList);
        dtos.addAll(containsList);

        return dtos;
    }
    //Chris 20190829 moe_drug_server  --End

}
