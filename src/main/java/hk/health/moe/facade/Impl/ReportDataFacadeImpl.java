package hk.health.moe.facade.Impl;

import hk.health.moe.common.MoeCommonHelper;
import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.exception.RecordException;
import hk.health.moe.facade.ReportDataFacade;
import hk.health.moe.pojo.bo.ChunkBo;
import hk.health.moe.pojo.bo.CoreObjectBo;
import hk.health.moe.pojo.bo.PrescriptionDrugDosageListItemBo;
import hk.health.moe.pojo.bo.PrescriptionListItemBo;
import hk.health.moe.pojo.bo.ReportDataValueBo;
import hk.health.moe.pojo.bo.ReportListItemBo;
import hk.health.moe.pojo.dto.MoeActionStatusDto;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.pojo.po.MoeEhrMedAllergenPo;
import hk.health.moe.pojo.po.MoeEhrMedProfilePo;
import hk.health.moe.pojo.po.MoeEhrOrderPo;
import hk.health.moe.pojo.po.MoeEhrRxImageLogPo;
import hk.health.moe.pojo.po.MoeHospImagePo;
import hk.health.moe.pojo.po.MoeMedMultDosePo;
import hk.health.moe.pojo.po.MoeMedProfilePo;
import hk.health.moe.pojo.po.MoeOrderPo;
import hk.health.moe.pojo.po.MoeSupplFreqDescPo;
import hk.health.moe.repository.MoeEhrMedAllergenRepository;
import hk.health.moe.repository.MoeEhrOrderRepository;
import hk.health.moe.repository.MoeEhrRxImageLogRepository;
import hk.health.moe.repository.MoeHospImageRepository;
import hk.health.moe.repository.MoeMedMultDoseRepository;
import hk.health.moe.repository.MoeMedProfileRepository;
import hk.health.moe.repository.MoeSupplFreqDescRepository;
import hk.health.moe.restservice.bean.saam.MoePatientAdrDto;
import hk.health.moe.restservice.bean.saam.MoePatientAlertDto;
import hk.health.moe.restservice.bean.saam.MoePatientAllergyDto;
import hk.health.moe.restservice.bean.saam.MoePatientSummaryDto;
import hk.health.moe.util.DtoUtil;
import hk.health.moe.util.Formatter;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.StringUtil;
import hk.health.moe.util.SystemSettingUtil;
import hk.health.moe.util.WsFormatter;
import org.apache.commons.lang3.StringUtils;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.util.HtmlUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
public class ReportDataFacadeImpl implements ReportDataFacade {

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    @Autowired
    private MoeEhrOrderRepository moeEhrOrderRepository;

    @Autowired
    private MoeMedProfileRepository moeMedProfileRepository;

    @Autowired
    private MoeEhrMedAllergenRepository moeEhrMedAllergenRepository;

    @Autowired
    private MoeHospImageRepository moeHospImageRepository;

    @Autowired
    private MoeEhrRxImageLogRepository moeEhrRxImageLogRepository;

    @Autowired
    private MoeSupplFreqDescRepository moeSupplFreqDescRepository;

    @Autowired
    private MoeMedMultDoseRepository moeMedMultDoseRepository;

    private static final String YES = "Y";
    private static final String NO = "N";
    private final String hyphen = "\u2013 ";

    @Override
    public ReportDataValueBo reportForPrescription(MoeOrderPo moeOrderPo, UserDto userDto, CoreObjectBo coreObject,
                                                   Date curDateTime, MoePatientSummaryDto patientSummaryDto, boolean failToCallSaam) throws Exception {

        String docNum = "";
        String docNumLabel = "";
        if (StringUtils.isBlank(userDto.getHkid())) {
            if (StringUtils.isBlank(userDto.getDocNum())) {
                docNum = "";
                docNumLabel = "";
            } else {
                docNum = userDto.getDocNum() + (StringUtils.isEmpty(userDto.getDocTypeCd()) ? "" : " (" + userDto.getDocTypeCd() + ")");
                docNumLabel = "Doc no.";
            }
        } else {
            docNum = userDto.getHkid();
            docNumLabel = "HKIC";
        }

        String hospitalCode = coreObject.getHospitalCode();
        Long orderNo = moeOrderPo.getOrdNo();

        // moeMedProfile List is sorted by action status rank value first
        // and then sorted by org_item_no
        List<MoeActionStatusDto> moeActionStatusDTOList = DtoUtil.ACTIONS_STATUS_LIST;
        final HashMap<String, MoeActionStatusDto> moeActionStatusMapper = new HashMap<>();
        for (MoeActionStatusDto moeActionStatusDto : moeActionStatusDTOList) {
            moeActionStatusMapper.put(moeActionStatusDto.getActionStatusType().toUpperCase(), moeActionStatusDto);
        }
        List<MoeMedProfilePo> sortedMoeMedProfileList = moeMedProfileRepository.findByHospCodeOrdno(hospitalCode, orderNo);
        if (sortedMoeMedProfileList == null || sortedMoeMedProfileList.isEmpty()) {
            //eric 20191225 start--
//            throw new RecordException(localeMessageSourceUtil.getMessage("records.notExisted"));
            throw new RecordException(ResponseCode.SpecialMessage.RECORDS_NOT_EXISTED.getResponseCode(),ResponseCode.SpecialMessage.RECORDS_NOT_EXISTED.getResponseMessage());
            //eric 20191225 end--
        }
        // Ricci 20190730 start --
        sortMoeMedProfileList(moeActionStatusMapper, sortedMoeMedProfileList);
        // Ricci 20190730 end --

        // Get Overriding reason
        List<MoeEhrMedAllergenPo> allergens = moeEhrMedAllergenRepository.findAllByOrdNo(orderNo);
        List<Long> allergenItemList = new ArrayList<>(allergens.size());
        for (MoeEhrMedAllergenPo a : allergens) {
            if (a.getMatchType().equalsIgnoreCase("A")) {
                // Ricci 20190730 start --
                allergenItemList.add(a.getCmsItemNo());
                // Ricci 20190730 end --
            }
        }

        // Prepare Report Data Values start --
        ReportDataValueBo report = new ReportDataValueBo();

        // Hospital Image
        Optional<MoeHospImagePo> optionalMoeHospImagePo = moeHospImageRepository.findById("hosp_image");

        if (optionalMoeHospImagePo.isPresent()) {
            MoeHospImagePo moeHospImagePo = optionalMoeHospImagePo.get();
            InputStream inputStream = new ByteArrayInputStream(moeHospImagePo.getImage());
            BufferedImage bufferedImage = null;
            try {
                bufferedImage = ImageIO.read(inputStream);
            } catch (IOException e) {
                inputStream.close();
            } finally {
                inputStream.close();
            }
            Image image = null;
            if (bufferedImage != null) {
                image = Toolkit.getDefaultToolkit().createImage(bufferedImage.getSource());
            }

            report.setHospitalIcon(image);
        }

        if (moeOrderPo.getPrescType() != null) {
            report.setPrescType(String.valueOf(moeOrderPo.getPrescType()));
        }

        // Ward && PatientStatus && RefNo && OrdNo
        String ward = "";
        String patientStatus = "";
        if (moeOrderPo.getPrescType() != null) {

            if (MoeCommonHelper.isPrescTypeHomeLeave(moeOrderPo.getPrescType())
                    || MoeCommonHelper.isPrescTypeDischarge(moeOrderPo.getPrescType())) {
                ward = "Ward: " + HtmlUtils.htmlEscape(StringUtil.stripToEmpty(userDto.getWard()));
            } else if (MoeCommonHelper.isPrescTypeOutpatient(moeOrderPo.getPrescType())) {
                if (!userDto.getHospitalCd().equalsIgnoreCase(userDto.getSpec())) {
                    ward = "Specialty: " + HtmlUtils.htmlEscape(StringUtil.stripToEmpty(userDto.getSpec()));
                }
            }

            if (StringUtil.stripToEmpty(userDto.getPatientStatus()).length() > 0) {
                patientStatus = HtmlUtils.htmlEscape(StringUtils.stripToEmpty(userDto.getPatientStatusLabel())) + ": " + HtmlUtils.htmlEscape(userDto.getPatientStatus());
            }
        }
        report.setWard(ward);
        report.setPatientStatus(patientStatus);

        String refNo = moeOrderPo.getRefNo();
        if (StringUtils.isEmpty(refNo)) {
            refNo = Formatter.getRefNo(moeOrderPo.getOrdNo(), moeOrderPo.getVersion());
        }
        report.setRefNo(refNo);
        report.setOrdNo(Formatter.getOrdNo(moeOrderPo.getOrdNo()));

        Optional<MoeEhrOrderPo> optionalMoeEhrOrderPo = moeEhrOrderRepository.findById(orderNo);
        if (!optionalMoeEhrOrderPo.isPresent()) {
            //eric 20191225 start--
//            throw new RecordException(localeMessageSourceUtil.getMessage("records.notExisted"));
            throw new RecordException(ResponseCode.SpecialMessage.RECORDS_NOT_EXISTED.getResponseCode(),ResponseCode.SpecialMessage.RECORDS_NOT_EXISTED.getResponseMessage());
            //eric 20191225 end--
        }

        // VerticalBarcodeLabel && HorizontalBarcodeLabel && OrdNoLabel
        String rightBarcodeValue = "";
        String bottomBarcodeValue = "";
        String caseNo = moeOrderPo.getCaseNo();
        String prescriptionBarcodeRight = SystemSettingUtil.getParamValue("prescription_barcode_right");
        String prescriptionBarcodeBottom = SystemSettingUtil.getParamValue("prescription_barcode_bottom");

        if (prescriptionBarcodeRight.equals("mrnPatientEncounterNo")) {
            rightBarcodeValue = caseNo;
            report.setVerticalBarcodeLabel(userDto.getMrnDisplayLabelE() + ": " + rightBarcodeValue);
        } else if (prescriptionBarcodeRight.equals("mrnPatientIdentity")) {
            rightBarcodeValue = userDto.getMrnPatientIdentity();
            report.setVerticalBarcodeLabel(userDto.getMrnDisplayLabelP() + ": " + userDto.getMrnPatientIdentity());
        } else {
            rightBarcodeValue = report.getOrdNo();
            report.setVerticalBarcodeLabel("Prescription No.: " + rightBarcodeValue);
        }

        if (prescriptionBarcodeBottom.equals("mrnPatientEncounterNo")) {
            bottomBarcodeValue = caseNo;
            report.setHorizontalBarcodeLabel(userDto.getMrnDisplayLabelE() + ": " + bottomBarcodeValue);
            report.setOrdNoLabel("Prescription No.: " + report.getOrdNo());
        } else if (prescriptionBarcodeBottom.equals("mrnPatientIdentity")) {
            bottomBarcodeValue = userDto.getMrnPatientIdentity();
            report.setHorizontalBarcodeLabel(userDto.getMrnDisplayLabelP() + ": " + userDto.getMrnPatientIdentity());
            report.setOrdNoLabel("Prescription No.: " + report.getOrdNo());
        } else {
            bottomBarcodeValue = report.getOrdNo();
            report.setHorizontalBarcodeLabel("Prescription No.: " + bottomBarcodeValue);
        }

        report.setPurchasePrescriptionEncounterNo("<u><b>" + userDto.getMrnDisplayLabelE() + ":</b></u> " + caseNo);
        report.setPurchaseCaseNo(caseNo);
        report.setPurchasePrescriptionOrderNo("Prescription No.: " + report.getOrdNo());

        // Horizontal128Icon
        Image tempHorizontalBarcodeImage = null;
        BufferedImage tempHorizontalBufferedimage = null;
        try {
            tempHorizontalBufferedimage = genBarcode128(bottomBarcodeValue);
            tempHorizontalBarcodeImage = Toolkit.getDefaultToolkit().createImage(tempHorizontalBufferedimage.getSource());
        } catch (Exception e) {
            //eric 20191225 start--
//            throw new MoeServiceException(e.getMessage(), e);
            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage()+e.getMessage());
            //eric 20191225 end--
        }
        report.setHorizontal128Icon(tempHorizontalBarcodeImage);

        // VerticalBarcode128Icon
        Image tempImage = null;
        BufferedImage tempBufferedimage = null;
        try {
            tempBufferedimage = genBarcode128(rightBarcodeValue);
            int ww = tempBufferedimage.getWidth();
            int hh = tempBufferedimage.getHeight();
            tempBufferedimage = createResizedCopy(tempBufferedimage, ww, hh, -90, 0, 0);
            tempImage = Toolkit.getDefaultToolkit().createImage(tempBufferedimage.getSource());
        } catch (Exception e) {
            //eric 20191225 start--
//            throw new MoeServiceException(e.getMessage(), e);
            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage()+e.getMessage());
            //eric 20191225 end--
        }
        report.setVerticalBarcode128Icon(tempImage);

        // OrdNoBarcode128Icon
        Image tempOrdNoBarcodeImage = null;
        BufferedImage tempOrdNoBufferedImage = null;
        try {
            tempOrdNoBufferedImage = genBarcode128(report.getOrdNo());
            tempOrdNoBarcodeImage = Toolkit.getDefaultToolkit().createImage(tempOrdNoBufferedImage.getSource());
        } catch (Exception e) {
            //eric 20191225 start--
//            throw new MoeServiceException(e.getMessage(), e);
            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage()+e.getMessage());
            //eric 20191225 end--
        }
        report.setOrdNoBarcode128Icon(tempOrdNoBarcodeImage);

        // OrderDate
        String orderDate = new SimpleDateFormat("dd-MMM-yyyy", Locale.US).format(moeOrderPo.getOrdDate());
        report.setOrderDate(orderDate);

        // CommunityFlag
        String communityFlag = NO;
        String dangerousDrug = NO;
        for (MoeMedProfilePo moeMedProfile : sortedMoeMedProfileList) {
            if ("P".equalsIgnoreCase(moeMedProfile.getActionStatus())) {
                communityFlag = YES;
                if (YES.equals(dangerousDrug)) {
                    break;
                }
            }
            if ("Y".equalsIgnoreCase(moeMedProfile.getDangerdrug())) {
                dangerousDrug = YES;
                if (YES.equals(communityFlag)) {
                    break;
                }
            }
        }
        report.setCommunityFlag(communityFlag);

        // HospitalChineseName && HospitalEnglishName && HospitalChineseAddress && HospitalPhoneNumber && HospitalFaxNumber
        // && HospitalWebAddress && HospitalEmailAddress && PurchaseInCommunityRxBottomStatement && EnableDentalDisclaimer
        // && PrescriptionCutsomLine1 && PrescriptionCutsomLine2
        String key = userDto.getHospitalCd() + " " + userDto.getSpec();
        report.setHospitalChineseName(SystemSettingUtil.getHospParamValue(key, "hospital_name_chi"));
        report.setHospitalEnglishName(SystemSettingUtil.getHospParamValue(key, "hospital_name_eng"));
        report.setHospitalChineseAddress(SystemSettingUtil.getHospParamValue(key, "hospital_address_chi"));
        report.setHospitalEnglishAddress(SystemSettingUtil.getHospParamValue(key, "hospital_address_eng"));
        report.setHospitalPhoneNumber(SystemSettingUtil.getHospParamValue(key, "hospital_tel_no"));
        report.setHospitalFaxNumber(SystemSettingUtil.getHospParamValue(key, "hospital_fax_no"));
        report.setHospitalWebAddress(SystemSettingUtil.getHospParamValue(key, "hospital_web_url"));
        report.setHospitalEmailAddress(SystemSettingUtil.getHospParamValue(key, "hospital_email"));
        report.setPurchaseInCommunityRxBottomStatement(SystemSettingUtil.getHospParamValue(key, "purchase_in_community_rx_bottom_statement"));
        report.setEnableDentalDisclaimer(SystemSettingUtil.getHospParamValue(key, "enable_dental_disclaimer"));
        report.setPrescriptionCutsomLine1(SystemSettingUtil.getHospParamValue(key, "prescription_custom_line_1"));
        report.setPrescriptionCutsomLine2(SystemSettingUtil.getHospParamValue(key, "prescription_custom_line_2"));

        // PatientEnglishName
        String name = "";
        if (StringUtils.isNotBlank(userDto.getEngSurName()) && StringUtils.isNotBlank(userDto.getEngGivenName())) {
            name = userDto.getEngSurName() + ", " + userDto.getEngGivenName();
        } else if (StringUtils.isNotBlank(userDto.getEngSurName())) {
            name = userDto.getEngSurName();
        } else if (StringUtils.isNotBlank(userDto.getEngGivenName())) {
            name = userDto.getEngGivenName();
        }
        report.setPatientEnglishName(name);

        // PatientChineseName
        //report.setPatientChineseName("(" + userDto.getChiName() + ")");
        report.setPatientChineseName(userDto.getChiName());


        // AddressContainsChineseChr && PatientAddress
        if (userDto.getResidentialAddress() != null && StringUtil.containsChineseCharacter(userDto.getResidentialAddress())) {
            report.setAddressContainsChineseChr(YES);
        }
        boolean showAddress = true;
        if (YES.equals(SystemSettingUtil.getParamValue("remove_patient_address_non_dd_rx"))
                && !YES.equals(dangerousDrug)) {
            showAddress = false;
        }

        if (showAddress) {
            report.setPatientAddress(HtmlUtils.htmlEscape(userDto.getResidentialAddress()));
        }

        // DateOfBirth
        String dateOfBirth = userDto.getDob();
        if (StringUtils.isEmpty(dateOfBirth)) {
            dateOfBirth = "";
        }
        report.setDateOfBirth(HtmlUtils.htmlEscape(StringUtils.stripToEmpty(dateOfBirth)));

        // Sex
        report.setSex(HtmlUtils.htmlEscape(StringUtils.stripToEmpty(userDto.getGenderCd())));

        // DocTypeLabel
        report.setDocTypeLabel(HtmlUtils.htmlEscape(StringUtils.stripToEmpty(docNumLabel)));


        // Add code to print vertical bar code controlled by print_with_hkic flag
        String printWithHkic = SystemSettingUtil.getParamValue("print_with_hkic");
        String printWithHkicBarcode = SystemSettingUtil.getParamValue("print_with_hkic_barcode");
        boolean isPrintWithHkic = printWithHkic != null && printWithHkic.equalsIgnoreCase(YES);
        boolean isPrintWithHkicBarcode = printWithHkicBarcode != null && printWithHkicBarcode.equalsIgnoreCase(YES);
        String docNumForBarcode = "";
        if (isPrintWithHkic && isPrintWithHkicBarcode) {

            String hkicWithoutBracket = docNum;
            if ("HKIC".equalsIgnoreCase(docNumLabel)) {
                hkicWithoutBracket = StringUtils.remove(hkicWithoutBracket, "(");
                hkicWithoutBracket = StringUtils.remove(hkicWithoutBracket, ")");
                docNumForBarcode = hkicWithoutBracket;
            } else {
                docNumForBarcode = userDto.getDocNum();
            }
            if (StringUtils.isNotBlank(hkicWithoutBracket)) {
                report.setHkic(docNum);
                report.setDocNumWithoutType(docNumForBarcode);
                Image temphkicBarcodeImage = null;
                BufferedImage temphkicBufferedimage = null;
                try {
                    temphkicBufferedimage = this.genBarcode128(docNumForBarcode);
                    temphkicBarcodeImage = Toolkit.getDefaultToolkit().createImage(temphkicBufferedimage.getSource());
                } catch (Exception e) {
                    //eric 20191225 start--
//            throw new MoeServiceException(e.getMessage(), e);
                    throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage()+e.getMessage());
                    //eric 20191225 end--
                }
                report.setHkicBarcode128Icon(temphkicBarcodeImage);

            } else {
                report.setHkic("");
                report.setDocNumWithoutType(docNumForBarcode);
            }

        } else {
            report.setHkic(docNum);
            report.setDocNumWithoutType(docNumForBarcode);
        }

        // Age
        report.setAge(HtmlUtils.htmlEscape(StringUtils.stripToEmpty(userDto.getAge())));

        // PatientPhoneNo
        String patientPhoneNo = "";
        if (StringUtils.isBlank(userDto.getMobilePhone())) {
            if (StringUtils.isBlank(userDto.getHomePhone())) {
                if (StringUtils.isBlank(userDto.getOfficePhone())) {
                    if (StringUtils.isBlank(userDto.getOtherPhone())) {
                        patientPhoneNo = "";
                    } else {
                        patientPhoneNo = userDto.getOtherPhone();
                    }
                } else {
                    patientPhoneNo = userDto.getOfficePhone();
                }
            } else {
                patientPhoneNo = userDto.getHomePhone();
            }
        } else {
            patientPhoneNo = userDto.getMobilePhone();
        }
        report.setPatientPhoneNo(HtmlUtils.htmlEscape(StringUtils.stripToEmpty(patientPhoneNo)));

        // Weight -- not need
        /*if (YES.equalsIgnoreCase(SystemSettingUtil.getParamValue("print_with_patient_weight"))) {
            // TODO
            *//*if (userDto.get().isEmpty()){
                report.setWeight("N/A");
            }
            else{
                report.setWeight(userDto.getWeight());
            }*//*
            report.setWeight("N/A");
        }*/

        // Prepare Alert Allergy Information
        StringBuilder drugAllergyItem = new StringBuilder("No record");
        StringBuilder adverseDrugReactionItem = new StringBuilder("No record");
        StringBuilder alertItem = new StringBuilder("No record");

        if (patientSummaryDto != null && SystemSettingUtil.isSettingEnabled("enable_saam")) {

            List<MoePatientAllergyDto> allergyDtoArray = patientSummaryDto.getPatientAllergies();
            List<MoePatientAdrDto> adrDtoArray = patientSummaryDto.getPatientAdrs();
            List<MoePatientAlertDto> alertDtoArray = patientSummaryDto.getPatientAlerts();

            List<MoePatientAlertDto> alertDtos = new ArrayList<>();
            boolean hasClassifiedAlert = false;
            if (alertDtoArray != null) {
                for (MoePatientAlertDto alertDto : alertDtoArray) {
                    if (NO.equalsIgnoreCase(alertDto.getClassifiedIndicator())) {
                        alertDtos.add(alertDto);
                    } else if (YES.equalsIgnoreCase(alertDto.getClassifiedIndicator())) {
                        hasClassifiedAlert = true;
                    }
                }
            }
            alertDtoArray = alertDtos;

            if (allergyDtoArray != null && !allergyDtoArray.isEmpty()) {
                drugAllergyItem = new StringBuilder("");
                for (int i = 0; i < allergyDtoArray.size(); i++) {
                    String allergyDisplayName = getAllergyDisplayName(allergyDtoArray.get(i));
                    if ("No Known Drug Allergy".equals(allergyDisplayName)) {
                        allergyDisplayName = allergyDisplayName + " (Updated on: " + new SimpleDateFormat("dd-MMM-yyyy HH:mm", Locale.US).format(allergyDtoArray.get(i).getUpdateDtm()) + ")";
                    }
                    if (allergyDtoArray.size() == 1) {
                        drugAllergyItem.append(allergyDisplayName);
                    } else {
                        drugAllergyItem.append("(" + (i + 1) + ") " + allergyDisplayName + " ");
                    }
                }
            }

            if (adrDtoArray != null && !adrDtoArray.isEmpty()) {
                adverseDrugReactionItem = new StringBuilder("");
                for (int i = 0; i < adrDtoArray.size(); i++) {
                    if (adrDtoArray.size() == 1) {
                        adverseDrugReactionItem.append(getAdrDisplayName(adrDtoArray.get(i)));
                    } else {
                        adverseDrugReactionItem.append("(" + (i + 1) + ") " + getAdrDisplayName(adrDtoArray.get(i)) + " ");
                    }
                }
            }

            // Paperclip
            if (hasClassifiedAlert) {
                BufferedImage bufferedImage = null;
                alertItem = new StringBuilder("");
                try {
                    bufferedImage = ImageIO.read(ResourceUtils.getFile("classpath:reports/image/paperclip.bmp"));
                } catch (IOException e) {
                    //LOG.error(e.getMessage(), e);
                }
                Image image = null;
                if (bufferedImage != null) {
                    image = Toolkit.getDefaultToolkit().createImage(bufferedImage.getSource());
                }
                report.setPaperClipIcon(image);
            }

            if (alertDtoArray != null && !alertDtoArray.isEmpty()) {
                alertItem = new StringBuilder("");
                for (int i = 0; i < alertDtoArray.size(); i++) {
                    if (alertDtoArray.size() == 1) {
                        alertItem.append(alertDtoArray.get(i).getAlertDesc());
                    } else {
                        alertItem.append("(" + (i + 1) + ") " + alertDtoArray.get(i).getAlertDesc() + " ");
                    }
                }
            }

        }
        report.setFailToCallSaam(failToCallSaam);
        report.setDrugAllergyItem(drugAllergyItem.toString());
        report.setAdverseDrugReactionItem(adverseDrugReactionItem.toString());
        report.setAlertItem(alertItem.toString());

        // Moe Hospital Item
        List<ReportListItemBo> moeHospitalItemList = new ArrayList<>();
        report.setMoeHospitalItemList(moeHospitalItemList);

        List<PrescriptionListItemBo> prescriptionItemList = new ArrayList<>();
        boolean hasDischargeDangerDrug = false;
        boolean hasDischargeNonDangerDrug = false;
        boolean hasDischargeFreeTextDrug = false;
        boolean enableFreeTextIndicator = SystemSettingUtil.isSettingEnabled("enable_free_text_indicator");

        String actionStatusValue = null;
        for (int i = 0; i < sortedMoeMedProfileList.size(); i++) {
            MoeMedProfilePo moeMedProfile = sortedMoeMedProfileList.get(i);
            ReportListItemBo reportListItem = null;
            PrescriptionListItemBo prescriptionListItem = null;

            if (actionStatusValue == null || !actionStatusValue.equalsIgnoreCase(moeMedProfile.getActionStatus())) {
                actionStatusValue = moeMedProfile.getActionStatus();

                reportListItem = new ReportListItemBo();
                reportListItem.setLevel(1);
                reportListItem.setLabelName(moeActionStatusMapper.get(actionStatusValue).getActionStatus());
                moeHospitalItemList.add(reportListItem);
            }
            MoeEhrMedProfilePo moeEhrMedProfile = moeMedProfile.getMoeEhrMedProfile();

            if (moeEhrMedProfile == null) {
                //eric 20191225 start--
//                throw new MoeServiceException(localeMessageSourceUtil.getMessage("records.notExisted"));
                throw new MoeServiceException(ResponseCode.SpecialMessage.RECORDS_NOT_EXISTED.getResponseCode(),ResponseCode.SpecialMessage.RECORDS_NOT_EXISTED.getResponseMessage());
                //eric 20191225 end--
            }

            String qtyUnit = Formatter.getModu(moeMedProfile.getMoQtyUnit());
            boolean isHaveQty = moeMedProfile.getMoQty() != null;
            boolean isDangerDrug = YES.equalsIgnoreCase(moeMedProfile.getDangerdrug());
            if (isDangerDrug) {
                hasDischargeDangerDrug = true;
            } else {
                hasDischargeNonDangerDrug = true;
            }
            boolean isFreeTextDrug = MoeCommonHelper.isAliasNameTypeFreeText(moeMedProfile.getNameType());

            //--- start drug name, drug name detail ---
            StringBuilder drugName = new StringBuilder();
            StringBuilder drugNameDetail = new StringBuilder();

            List<ChunkBo> drugNameList = new ArrayList<>();
            List<ChunkBo> drugNameDetailList = new ArrayList<>();

            String vtm = moeEhrMedProfile.getVtm();
            if (vtm == null) {
                vtm = "";
            }
            int numIngredient = StringUtils.countMatches(vtm, "+");
            String tradeName = moeEhrMedProfile.getTradeName();

            if (enableFreeTextIndicator && isFreeTextDrug) {
                hasDischargeFreeTextDrug = true;
                drugName.append("^ ");
            }

            if (moeEhrMedProfile.getAliasName() != null
                    && moeEhrMedProfile.getAliasNameType() != null
                    && !MoeCommonHelper.isAliasNameTypeTradeNameAlias(moeEhrMedProfile.getAliasNameType())
                    && !MoeCommonHelper.isAliasNameTypeVtm(moeEhrMedProfile.getAliasNameType())) {
                if (YES.equalsIgnoreCase(moeEhrMedProfile.getGenericIndicator())) { // trade name doesn't exist
                    String manufacturer = WsFormatter.findGenericDrugTradeName(tradeName);
                    if (!StringUtils.isEmpty(manufacturer)) {
                        String vtmNew = moeEhrMedProfile.getTradeName().replace(manufacturer, "");
                        drugNameDetail.append(HtmlUtils.htmlEscape(vtmNew) + "<font face='Times New Roman'><i>" + HtmlUtils.htmlEscape(manufacturer) + "</i></font>");

                        ChunkBo chunk1 = new ChunkBo(false, false, false, vtmNew);
                        ChunkBo chunk2 = new ChunkBo(false, false, true, manufacturer);
                        drugNameDetailList.add(chunk1);
                        drugNameDetailList.add(chunk2);
                    } else {
                        drugNameDetail.append(moeEhrMedProfile.getTradeName());

                        ChunkBo chunk = new ChunkBo(false, false, false, moeEhrMedProfile.getTradeName());
                        drugNameDetailList.add(chunk);
                    }
                } else {
                    drugName.append(HtmlUtils.htmlEscape(tradeName));

                    ChunkBo chunk = new ChunkBo(true, false, false, tradeName);
                    drugNameList.add(chunk);
                }
                drugNameDetail.append(" (" + HtmlUtils.htmlEscape(moeEhrMedProfile.getAliasName()) + ")");

                ChunkBo chunk = new ChunkBo(false, false, false, " (" + moeEhrMedProfile.getAliasName() + ")");
                drugNameDetailList.add(chunk);

                String strength = moeMedProfile.getStrength();

                if (strength != null && !strength.isEmpty()) {
                    // Ricci 20190918 start --DHCIMSB-869
                    strength = splitStrength(strength, moeEhrMedProfile.getStrengthCompulsory());
                    // Ricci 20190918 end --DHCIMSB-869
                    drugNameDetail.append(" " + HtmlUtils.htmlEscape(strength));

                    ChunkBo chunk1 = new ChunkBo(false, false, false, " " + strength);
                    drugNameDetailList.add(chunk1);
                }
            } else {
                String genericIndicator = moeEhrMedProfile.getGenericIndicator();
                boolean isHideVtm = genericIndicator != null && YES.equalsIgnoreCase(genericIndicator);
                String screenDisplay = moeEhrMedProfile.getScreenDisplay();
                if (numIngredient == 0) {            // only have 1 vtm item
                    if (MoeCommonHelper.isAliasNameTypeLocalDrug(moeMedProfile.getNameType())) {
                        drugNameDetail.append(HtmlUtils.htmlEscape(screenDisplay));

                        ChunkBo chunk = new ChunkBo(false, false, false, screenDisplay);
                        drugNameDetailList.add(chunk);
                    } else {
                        if (MoeCommonHelper.isAliasNameTypeTradeNameAlias(moeEhrMedProfile.getAliasNameType())) {
                            drugName.append("[" + HtmlUtils.htmlEscape(moeEhrMedProfile.getAliasName()) + "] ");

                            ChunkBo chunk = new ChunkBo(true, false, false, "[" + moeEhrMedProfile.getAliasName() + "] ");
                            drugNameList.add(chunk);
                        }
                        if (StringUtils.isNotEmpty(tradeName)
                                && !isHideVtm) {
                            drugName.append(HtmlUtils.htmlEscape(tradeName));

                            ChunkBo chunk = new ChunkBo(true, false, false, tradeName);
                            drugNameList.add(chunk);

                            drugNameDetail.append(" (" + HtmlUtils.htmlEscape(vtm) + ")");

                            ChunkBo chunk1 = new ChunkBo(false, false, false, " (" + vtm + ")");
                            drugNameDetailList.add(chunk1);
                        } else if (StringUtils.isNotEmpty(tradeName)
                                && isHideVtm) {
                            String manufacturer = WsFormatter.findGenericDrugTradeName(tradeName);
                            if (!StringUtils.isEmpty(manufacturer)) {
                                vtm = tradeName.replace(manufacturer, "<font face='Times New Roman'><i>" + manufacturer + "</i></font>");
                                drugNameDetail.append(vtm);

                                if (tradeName.indexOf(manufacturer) != -1) {
                                    ChunkBo chunk1 = new ChunkBo(false, false, false, tradeName.substring(0, tradeName.indexOf(manufacturer)));
                                    ChunkBo chunk2 = new ChunkBo(false, false, true, manufacturer);
                                    ChunkBo chunk3 = new ChunkBo(false, false, false, tradeName.substring(tradeName.indexOf(manufacturer) + manufacturer.length(), tradeName.length()));
                                    drugNameDetailList.add(chunk1);
                                    drugNameDetailList.add(chunk2);
                                    drugNameDetailList.add(chunk3);
                                } else {
                                    ChunkBo chunk = new ChunkBo(false, false, false, tradeName);
                                    drugNameDetailList.add(chunk);
                                }
                            } else {
                                drugNameDetail.append(tradeName);

                                ChunkBo chunk = new ChunkBo(false, false, false, tradeName);
                                drugNameDetailList.add(chunk);
                            }
                        } else if (StringUtils.isNotEmpty(screenDisplay)) {
                            drugName.append(HtmlUtils.htmlEscape(screenDisplay));

                            ChunkBo chunk = new ChunkBo(true, false, false, drugName.toString());
                            drugNameList.add(chunk);
                        }

                        String strength = moeMedProfile.getStrength();

                        if (strength != null && !strength.isEmpty()) {
                            // Ricci 20190918 start --DHCIMSB-869
                            strength = splitStrength(strength, moeEhrMedProfile.getStrengthCompulsory());
                            // Ricci 20190918 end --DHCIMSB-869
                            drugNameDetail.append(" " + HtmlUtils.htmlEscape(strength));

                            ChunkBo chunk = new ChunkBo(false, false, false, " " + strength);
                            drugNameDetailList.add(chunk);
                        }

                        //			} else if (numIngredient == 1
                        //					|| numIngredient == 2) {	// have 2 or 3 vtm items
                        //				String amp = moeEhrMedProfile.getScreenDisplay();
                        //				if (amp != null && !amp.isEmpty()) {
                        //					drugNameDetail.append(HtmlUtils.htmlEscape(amp));
                        //				}
                        //			} else if (numIngredient >= 3) {	// 4 or more vtm items
                    }

                } else {
                    // try to remove amp last (....) value
                    String amp = moeEhrMedProfile.getScreenDisplay();
                    if (ServerConstant.ALIAS_NAME_TYPE_LOCAL_DRUG.equals(moeMedProfile.getNameType())) {
                        String chunkStr1 = amp;
                        drugNameDetail.append(amp);
                        ChunkBo chunk1 = new ChunkBo(false, false, false, chunkStr1);
                        drugNameDetailList.add(chunk1);
                    } else if (amp.indexOf("+") != -1 && !isHideVtm && amp != null && !amp.isEmpty()) {
                        boolean matchBasket = true;
                        int firstOpen = 0;
                        int firstPlus = amp.indexOf('+');
                        String temp = amp.substring(0, firstPlus);
                        int occurrence = 0;
                        while (temp.indexOf("(") == -1) {
                            int index = StringUtil.nthOccurrence(amp, "+", ++occurrence);
                            if (index != -1) {
                                temp = amp.substring(0, index - 1);
                            } else {
                                matchBasket = false;
                                break;
                            }
                        }


                        for (int ii = temp.length() - 1; ii >= 0; ii--) {
                            if (temp.substring(ii, ii + 1).equals(")")) {
                                firstOpen++;
                            } else if (temp.substring(ii, ii + 1).equals("(") && firstOpen != 0) {
                                firstOpen--;
                            } else if (temp.substring(ii, ii + 1).equals("(") && firstOpen == 0) {
                                firstOpen = ii;
                                break;
                            }
                        }

                        int firstClose = 0;
                        int lastPlus = amp.lastIndexOf('+');
                        temp = amp;
                        for (int ij = lastPlus; ij < amp.length(); ij++) {
                            if (temp.substring(ij, ij + 1).equals("(")) {
                                firstClose++;
                            }
                            if (temp.substring(ij, ij + 1).equals(")") && firstClose != 0) {
                                firstClose--;
                            } else if (temp.substring(ij, ij + 1).equals(")") && firstClose == 0) {
                                firstClose = ij;
                                break;
                            }
                        }
                        if (firstOpen != 0 && firstClose != 0) {
                            if (matchBasket && numIngredient > 2) {
                                tradeName = amp.substring(0, firstOpen);
                                String removeValue = amp.substring(firstOpen, firstClose + 1);
                                amp = amp.replace(removeValue, "*");
                                amp = amp.replace(tradeName, "");

                                String chunkStr1 = tradeName;
                                String chunkStr2 = amp.trim();

                                amp = "<b>" + HtmlUtils.htmlEscape(tradeName) + "</b>" + HtmlUtils.htmlEscape(amp.trim());
                                amp = StringUtils.replace(amp, "  ", " "); // replace double space by single space
                                if (!amp.isEmpty()) {
                                    drugNameDetail.append(amp);

                                    ChunkBo chunk1 = new ChunkBo(true, false, false, StringUtils.replace(chunkStr1, "  ", " "));
                                    ChunkBo chunk2 = new ChunkBo(false, false, false, StringUtils.replace(chunkStr2, "  ", " "));
                                    drugNameDetailList.add(chunk1);
                                    drugNameDetailList.add(chunk2);
                                }
                                report.setDrugIngredientMoreThanThreeMsg(ServerConstant.INGTEDIENT_MORE_THAN_THREE_MSG);
                                if (MoeCommonHelper.isActionTypePurchaseInCommunity(moeEhrMedProfile.getMoeMedProfile().getActionStatus())) {
                                    report.setDrugIngredientPurchaseMoreThanThreeMsg(ServerConstant.INGTEDIENT_MORE_THAN_THREE_MSG);
                                }
                            } else {
                                tradeName = amp.substring(0, firstOpen);
                                amp = HtmlUtils.htmlEscape(amp.replace(tradeName, ""));

                                String chunkStr1 = tradeName;
                                String chunkStr2 = amp;

                                amp = "<b>" + HtmlUtils.htmlEscape(tradeName) + "</b>" + amp;
                                drugNameDetail.append(amp);

                                ChunkBo chunk1 = new ChunkBo(true, false, false, chunkStr1);
                                ChunkBo chunk2 = new ChunkBo(false, false, false, chunkStr2);
                                drugNameDetailList.add(chunk1);
                                drugNameDetailList.add(chunk2);
                            }
                        } else {
                            amp = moeEhrMedProfile.getScreenDisplay();
                            String chunkStr1 = amp;
                            drugNameDetail.append(amp);
                            ChunkBo chunk1 = new ChunkBo(false, false, false, chunkStr1);
                            drugNameDetailList.add(chunk1);
                        }
                    } else {
                        // Generic indicator = 'Y'
                        if (MoeCommonHelper.isAliasNameTypeTradeNameAlias(moeEhrMedProfile.getAliasNameType())) {
                            amp = screenDisplay.replace("[" + moeEhrMedProfile.getAliasName() + "]", "");
                            drugName.append("[" + moeEhrMedProfile.getAliasName() + "]");

                            ChunkBo chunk = new ChunkBo(true, false, false, "[" + moeEhrMedProfile.getAliasName() + "]");
                            drugNameList.add(chunk);
                        }

                        String manufacturer = WsFormatter.findGenericDrugTradeName(HtmlUtils.htmlEscape(amp));
                        if (!StringUtils.isEmpty(manufacturer)) {
                            vtm = amp.replace(manufacturer, "<font face='Times New Roman'><i>" + manufacturer + "</i></font>");
                            drugNameDetail.append(vtm);

                            if (amp.indexOf(manufacturer) != -1) {
                                ChunkBo chunk1 = new ChunkBo(false, false, false, amp.substring(0, amp.indexOf(manufacturer)));
                                ChunkBo chunk2 = new ChunkBo(false, false, true, manufacturer);
                                ChunkBo chunk3 = new ChunkBo(false, false, false, amp.substring(amp.indexOf(manufacturer) + manufacturer.length(), amp.length()));
                                drugNameDetailList.add(chunk1);
                                drugNameDetailList.add(chunk2);
                                drugNameDetailList.add(chunk3);
                            } else {
                                ChunkBo chunk = new ChunkBo(false, false, false, amp);
                                drugNameDetailList.add(chunk);
                            }
                        } else {
                            drugNameDetail.append(amp);

                            ChunkBo chunk = new ChunkBo(false, false, false, amp);
                            drugNameDetailList.add(chunk);
                        }
                    }
                }
            }

            if (numIngredient == 0 &&
                    !ServerConstant.ALIAS_NAME_TYPE_LOCAL_DRUG.equals(moeMedProfile.getNameType())) {
                String routeDesc = moeMedProfile.getMoeEhrMedProfile().getDrugRouteEng();
                routeDesc = routeDesc == null ? "" : HtmlUtils.htmlEscape(routeDesc);
                String formDesc = moeMedProfile.getFormDesc();
                formDesc = formDesc == null ? "" : HtmlUtils.htmlEscape(formDesc);

                // consider if append route by consider formRouteMapping
                String formRouteMapKey = formDesc + " " + routeDesc;
                String formRouteMapValue = DtoUtil.FORM_ROUTE_MAP.get(formRouteMapKey);
                if (formRouteMapValue == null || YES.equalsIgnoreCase(formRouteMapValue)) {
                    drugNameDetail.append(" " + routeDesc);

                    ChunkBo chunk = new ChunkBo(false, false, false, " " + routeDesc);
                    drugNameDetailList.add(chunk);
                }

                // consider if append doseFormExtraInfo
                String doseFormExtraInfo = moeEhrMedProfile.getDoseFormExtraInfo();
                if (StringUtils.isNotEmpty(doseFormExtraInfo)) {
                    drugNameDetail.append(HtmlUtils.htmlEscape(" " + formDesc + " (" + doseFormExtraInfo + ")"));

                    ChunkBo chunk = new ChunkBo(false, false, false, " " + formDesc + " (" + doseFormExtraInfo + ")");
                    drugNameDetailList.add(chunk);
                } else {
                    drugNameDetail.append(" " + formDesc);

                    ChunkBo chunk = new ChunkBo(false, false, false, " " + formDesc);
                    drugNameDetailList.add(chunk);
                }

                // Strength Level Extra Info
                if (moeEhrMedProfile.getStrengthLevelExtraInfo() != null) {
                    if (!StringUtil.isAscii(moeEhrMedProfile.getStrengthLevelExtraInfo())) {
                        drugNameDetail.append(" (<font face='Times New Roman'>" + HtmlUtils.htmlEscape(moeEhrMedProfile.getStrengthLevelExtraInfo()) + "</font>)");
                    } else {
                        drugNameDetail.append(" (" + HtmlUtils.htmlEscape(moeEhrMedProfile.getStrengthLevelExtraInfo()) + ")");
                    }

                    ChunkBo chunk = new ChunkBo(false, false, false, " (" + moeEhrMedProfile.getStrengthLevelExtraInfo() + ")");
                    drugNameDetailList.add(chunk);
                }
            }

            reportListItem = new ReportListItemBo();
            reportListItem.setLevel(2);

            reportListItem.setDrugName(drugName.toString());
            reportListItem.setDrugNameDetail(drugNameDetail.toString());

            moeHospitalItemList.add(reportListItem);

            prescriptionListItem = new PrescriptionListItemBo();
            prescriptionListItem.setActionStatus(moeMedProfile.getActionStatus());
            prescriptionListItem.setDrugNameList(drugNameList);
            prescriptionListItem.setDrugNameDetailList(drugNameDetailList);
            prescriptionListItem.setIsDangerDrug(isDangerDrug);
            prescriptionListItem.setIsFreeText(isFreeTextDrug);

            prescriptionItemList.add(prescriptionListItem);

            if (moeEhrMedProfile.getOrderLineType() != null
                    && (MoeCommonHelper.isOrderLineTypeNormal(moeEhrMedProfile.getOrderLineType()))
                    || MoeCommonHelper.isOrderLineTypeAdvanced(moeEhrMedProfile.getOrderLineType())) {
                //--- Type N ---
                typeN(moeOrderPo, moeMedProfile, moeEhrMedProfile, moeHospitalItemList, prescriptionListItem);
            } else if (MoeCommonHelper.isRegimentTypeMonthly(moeEhrMedProfile.getOrderLineType())) {
                //--- Type M ---
                List<MoeMedMultDosePo> moeMedMultDoseList =
                        moeMedMultDoseRepository.findByHospcodeAndOrdNoAndCmsItemNo(hospitalCode, orderNo, moeMedProfile.getCmsItemNo());
                int listSize = moeMedMultDoseList.size();
                for (int j = 0; j < listSize; j++) {
                    boolean isLastLine = j == (listSize - 1);
                    MoeMedMultDosePo moeMedMultDose = moeMedMultDoseList.get(j);

                    typeM(moeOrderPo, moeMedMultDose, moeMedProfile, moeEhrMedProfile, isLastLine, moeHospitalItemList, prescriptionListItem);
                }

            } else if (moeEhrMedProfile.getOrderLineType() != null
                    && MoeCommonHelper.isOrderLineTypeStepUpDown(moeEhrMedProfile.getOrderLineType())) {
                //--- Type S ---
                List<MoeMedMultDosePo> moeMedMultDoseList =
                        moeMedMultDoseRepository.findByHospcodeAndOrdNoAndCmsItemNo(hospitalCode, orderNo, moeMedProfile.getCmsItemNo());

                int listSize = moeMedMultDoseList.size();
                for (int j = 0; j < listSize; j++) {
                    MoeMedMultDosePo moeMedMultDose = moeMedMultDoseList.get(j);

                    boolean isFirstLine = j == 0;
                    boolean isLastLine = j == (listSize - 1);

                    typeS(moeOrderPo, moeMedMultDose, moeMedProfile, isFirstLine, isLastLine, moeHospitalItemList, prescriptionListItem);
                }

            } else if (moeEhrMedProfile.getOrderLineType() != null
                    && MoeCommonHelper.isOrderLineTypeRecurrent(moeEhrMedProfile.getOrderLineType())) {
                //--- Type R ---
                List<MoeMedMultDosePo> moeMedMultDoseList =
                        moeMedMultDoseRepository.findByHospcodeAndOrdNoAndCmsItemNo(hospitalCode, orderNo, moeMedProfile.getCmsItemNo());

                int listSize = moeMedMultDoseList == null ? 0 : moeMedMultDoseList.size();
                for (int j = 0; j < listSize; j++) {
                    boolean isLastLine = j == (listSize - 1);
                    MoeMedMultDosePo moeMedMultDose = moeMedMultDoseList.get(j);

                    if ("on odd / even days".equalsIgnoreCase(moeMedMultDose.getSupFreqCode().trim())) {
                        //--- Same as M ---
                        typeM(moeOrderPo, moeMedMultDose, moeMedProfile, moeEhrMedProfile, isLastLine, moeHospitalItemList, prescriptionListItem);
                    } else {
                        //--- Same as N ---
                        typeN(moeOrderPo, moeMedProfile, moeEhrMedProfile, moeHospitalItemList, prescriptionListItem);
                    }
                }

                if (listSize == 0) {
                    //--- Same as N ---
                    typeN(moeOrderPo, moeMedProfile, moeEhrMedProfile, moeHospitalItemList, prescriptionListItem);
                }
            } else {
                continue;
            }

            // align format for total quantity
            int listSize = moeHospitalItemList.size();
            reportListItem = moeHospitalItemList.get(listSize - 1);
            prescriptionListItem = prescriptionItemList.get(prescriptionItemList.size() - 1);

            if (isHaveQty && isDangerDrug) {
                boolean isMultiRecord = false;
                String type = moeEhrMedProfile.getOrderLineType();
                List<MoeMedMultDosePo> moeMedMultDoseList =
                        moeMedMultDoseRepository.findByHospcodeAndOrdNoAndCmsItemNo(hospitalCode, orderNo, moeMedProfile.getCmsItemNo());

                int moeMedMultDoseListSize = 0;
                if (moeMedMultDoseList != null && !moeMedMultDoseList.isEmpty()) {
                    moeMedMultDoseListSize = moeMedMultDoseList.size();
                }

                if (MoeCommonHelper.isOrderLineTypeMultipleLine(type)
                        || MoeCommonHelper.isOrderLineTypeStepUpDown(type)
                        || MoeCommonHelper.isOrderLineTypeRecurrent(type)
                        && moeMedMultDoseListSize > 0) {
                    isMultiRecord = true;
                }

                DecimalFormat formatter = new DecimalFormat("###,###.####");

                if (!isMultiRecord) {
                    StringBuilder drugValueExt = new StringBuilder();
                    Long moQty = moeMedProfile.getMoQty();
                    String moQtyUnit = Formatter.getModu(moeMedProfile.getMoQtyUnit());
                    boolean dataValid = moQty != null && moQtyUnit != null;
                    if (dataValid) {

                        drugValueExt.append(" <style isBold='true'> (prescribe ");
                        drugValueExt.append(formatter.format(moQty) + " " + moQtyUnit);
                        drugValueExt.append(")</style>");

                        String drugValueDetail = reportListItem.getDrugValueDetail() + " " + hyphen + drugValueExt.toString();
                        reportListItem.setDrugValueDetail(drugValueDetail);

                        if (prescriptionListItem.getLastPrescriptionDrugDosageListItem() != null) {
                            List<ChunkBo> drugExtInfoList = new ArrayList<ChunkBo>();
                            ChunkBo chunk = new ChunkBo(true, false, false, " " + hyphen + "  (prescribe " + formatter.format(moQty) + " " + moQtyUnit + ")");
                            drugExtInfoList.add(chunk);
                            prescriptionListItem.getLastPrescriptionDrugDosageListItem().setDrugExtInfoList(drugExtInfoList);
                        }
                    }
                }


            } else if (isHaveQty && !isDangerDrug) {
                Long qty = moeMedProfile.getMoQty();
                if (qty != null) {
                    String drugValueExt = " <style isBold='true'>" + qty.toString() + " " + qtyUnit + "</style>";

                    String drugValueDetail = reportListItem.getDrugValueDetail() + " " + hyphen + drugValueExt;
                    reportListItem.setDrugValueDetail(drugValueDetail);

                    if (prescriptionListItem.getLastPrescriptionDrugDosageListItem() != null) {
                        List<ChunkBo> drugExtInfoList = new ArrayList<ChunkBo>();
                        ChunkBo chunk = new ChunkBo(true, false, false, " " + hyphen + " " + qty.toString() + " " + qtyUnit);
                        drugExtInfoList.add(chunk);
                        prescriptionListItem.getLastPrescriptionDrugDosageListItem().setDrugExtInfoList(drugExtInfoList);
                    }
                }
            }

            // handle special instruction
            String specInstruct = moeMedProfile.getSpecInstruct();
            boolean isWithSpecInstruct = specInstruct != null && !specInstruct.isEmpty();
            if (isWithSpecInstruct) {
                reportListItem = new ReportListItemBo();
                reportListItem.setLevel(3);
                reportListItem.setDrugValue("");
                if (StringUtil.containsChineseCharacter(specInstruct)) {
                    reportListItem.setDrugValueDetail("<font face='Times New Roman'>(" + HtmlUtils.htmlEscape(specInstruct) + ")</font>");
                } else {
                    reportListItem.setDrugValueDetail("(" + HtmlUtils.htmlEscape(specInstruct) + ")");
                }

                moeHospitalItemList.add(reportListItem);

                List<ChunkBo> specInstructList = new ArrayList<ChunkBo>();
                ChunkBo chunk = new ChunkBo(false, false, false, "(" + specInstruct + ")");
                specInstructList.add(chunk);
                prescriptionListItem.setSpecInstructList(specInstructList);
            }

            // Overriding reason
            if (allergenItemList.contains(moeMedProfile.getCmsItemNo())) {
                reportListItem = new ReportListItemBo();
                reportListItem.setLevel(3);
                reportListItem.setDrugValue("");
                reportListItem.setDrugValueDetail(ServerConstant.OVERRIDDEN_MSG);
                moeHospitalItemList.add(reportListItem);

                prescriptionListItem.setWithOverridingReason(true);
            }

            // handle Remark
            String remark = moeMedProfile.getRemarkText();
            boolean isWithRemark = remark != null && !remark.isEmpty();
            if (isWithRemark) {
                //remark = "Remark for updating: Confirmed by " + moeMedProfile.getRemarkCreateBy();
                remark = "Note: " + moeMedProfile.getRemarkText();
                reportListItem = new ReportListItemBo();
                reportListItem.setLevel(3);
                reportListItem.setDrugValue("");
                // Ricci 20190814 start --
                reportListItem.setDrugValueDetail("<style isItalic='true'>" + remark + "</style>");
                // Ricci 20190814 end --
                moeHospitalItemList.add(reportListItem);

                List<ChunkBo> remarkList = new ArrayList<ChunkBo>();
                ChunkBo chunk = new ChunkBo(false, false, true, remark);
                remarkList.add(chunk);
                prescriptionListItem.setRemarkList(remarkList);
            }

            // handle Discontinuation
            String discFlag = moeMedProfile.getMoeEhrMedProfile().getMedDiscontFlag();
            boolean isDiscontinue = MoeCommonHelper.isFlagTrue(discFlag);
            if (isDiscontinue) {
                SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy");
                String discontinuation = "Discontinued, " + moeMedProfile.getMoeEhrMedProfile().getMedDiscontUser() + ", " + sdf.format(moeMedProfile.getMoeEhrMedProfile().getMedDiscontDtm());
                //String discontinuation = "Discontinued, " + sdf.format(moeMedProfile.getMoeEhrMedProfile().getMedDiscontDtm());
                reportListItem = new ReportListItemBo();
                reportListItem.setLevel(3);
                reportListItem.setDrugValue("");
                reportListItem.setDrugValueDetail(discontinuation);
                moeHospitalItemList.add(reportListItem);

                List<ChunkBo> discontinuationList = new ArrayList<ChunkBo>();
                ChunkBo chunk = new ChunkBo(false, false, false, discontinuation);
                discontinuationList.add(chunk);
                prescriptionListItem.setDiscontinuationList(discontinuationList);
                markStrikethru(prescriptionListItem);
            }
        }
        report.setMoeHospitalItemList(moeHospitalItemList);
        report.setPrescriptionItemList(prescriptionItemList);
        report.setHasDischargeDangerDrug(hasDischargeDangerDrug);
        report.setHasDischargeNonDangerDrug(hasDischargeNonDangerDrug);
        report.setHasDischargeFreeTextDrug(hasDischargeFreeTextDrug);

        //--- Moe Community Item ---
        MoeEhrOrderPo moeEhrOrderPo = null;
        Optional<MoeEhrOrderPo> byId = moeEhrOrderRepository.findById(orderNo);
        if (byId.isPresent())
            moeEhrOrderPo = byId.get();
        List<ReportListItemBo> moeCommunityItemList = new ArrayList<ReportListItemBo>();
        report.setMoeCommunityItemList(moeCommunityItemList);
        for (MoeActionStatusDto moeActionStatusDto : moeActionStatusDTOList) {

            if ("P".equalsIgnoreCase(String.valueOf(moeActionStatusDto.getActionStatusType()))) {
                int startIndex = -1;

                for (int i = 0; i < moeHospitalItemList.size(); i++) {
                    ReportListItemBo reportListItem = moeHospitalItemList.get(i);
                    if (moeActionStatusDto.getActionStatus().trim().equalsIgnoreCase(
                            String.valueOf(reportListItem.getLabelName()).trim())) {

                        startIndex = i;
                        break;
                    }
                }

                if (startIndex != -1) {
                    moeCommunityItemList.add(new ReportListItemBo(moeHospitalItemList.get(startIndex)));
                    for (int i = startIndex + 1; i < moeHospitalItemList.size(); i++) {
                        ReportListItemBo reportListItem = moeHospitalItemList.get(i);
                        if (reportListItem.getLevel() != 1) {
                            moeCommunityItemList.add(new ReportListItemBo(moeHospitalItemList.get(i)));
                        } else { // level == 1
                            break;
                        }
                    }
                    startIndex = -1;
                }
            }
        }

        boolean hasPurchaseDangerDrug = false;
        boolean hasPurchaseNonDangerDrug = false;
        List<PrescriptionListItemBo> purchaseInCommunityItemList = new ArrayList<PrescriptionListItemBo>();
        for (int i = 0; i < prescriptionItemList.size(); i++) {
            PrescriptionListItemBo prescriptionListItem = prescriptionItemList.get(i);
            if ("P".equalsIgnoreCase(String.valueOf(prescriptionListItem.getActionStatus()))) {
                Boolean isDangerDrug = prescriptionListItem.getIsDangerDrug();
                if (isDangerDrug) {
                    hasPurchaseDangerDrug = true;
                } else {
                    hasPurchaseNonDangerDrug = true;
                }

                purchaseInCommunityItemList.add(new PrescriptionListItemBo(prescriptionListItem));

                if (enableFreeTextIndicator && prescriptionListItem.getIsFreeText()) {
                    report.setHasPurchaseFreeTextDrug(true);
                }
            }
        }
        report.setPurchaseInCommunityItemList(purchaseInCommunityItemList);
        report.setHasPurchaseDangerDrug(hasPurchaseDangerDrug);
        report.setHasPurchaseNonDangerDrug(hasPurchaseNonDangerDrug);

        if (moeEhrOrderPo != null) {
            if (StringUtils.isNotBlank(moeEhrOrderPo.getUpdateUser())) {
                report.setDoctorName(moeEhrOrderPo.getUpdateUser());
            } else {
                report.setDoctorName(moeEhrOrderPo.getUpdateUserId());
            }
            if (StringUtils.isNotBlank(moeEhrOrderPo.getRemarkUser())) {
                report.setRemarkBy(moeEhrOrderPo.getRemarkUser());
            } else {
                report.setRemarkBy(moeEhrOrderPo.getRemarkUserId());
            }
        }

        Date firstPrintDate = null;
        MoeEhrRxImageLogPo moeEhrRxImageLogPo = moeEhrRxImageLogRepository.findFirstByOrdNoAndRefNoOrderByCreateDtm(orderNo, report.getRefNo());
        if (moeEhrRxImageLogPo != null) {
            firstPrintDate = moeEhrRxImageLogPo.getCreateDtm();
        } else {
            firstPrintDate = curDateTime;
        }
        report.setFirstPrintDate(new SimpleDateFormat("dd-MMM-yyyy HH:mm", Locale.US).format(firstPrintDate));

        report.setLastPrintDate(new SimpleDateFormat("dd-MMM-yyyy HH:mm", Locale.US).format(curDateTime));

        report.setReprintFlag(coreObject.isReprint() ? YES : NO);

        StringBuffer reportPath = new StringBuffer();
        reportPath.append(ServerConstant.REPORT_PATH);
        reportPath.append(ServerConstant.REPORT_PATH_FOR_PRESCRIPTION);

        report.setTemplatePath(reportPath.toString());

        return report;
    }

    private void sortMoeMedProfileList(HashMap<String, MoeActionStatusDto> moeActionStatusMapper, List<MoeMedProfilePo> sortedMoeMedProfileList) {
        Comparator<MoeMedProfilePo> orgItemNoASC = (arg0, arg1) -> {
            MoeActionStatusDto dtoArg0 = moeActionStatusMapper.get(arg0.getActionStatus());
            MoeActionStatusDto dtoArg1 = moeActionStatusMapper.get(arg1.getActionStatus());
            Long rank0 = dtoArg0 == null ? 0L : dtoArg0.getRank();
            Long rank1 = dtoArg1 == null ? 0L : dtoArg1.getRank();
            Long value0 = arg0.getOrgItemNo();
            Long value1 = arg1.getOrgItemNo();

            if (rank0.equals(rank1)) {
                return value0.compareTo(value1);
            } else {
                return rank0.compareTo(rank1);
            }
        };

        Collections.sort(sortedMoeMedProfileList, orgItemNoASC);
    }

    // Add barcode 128 support
    private BufferedImage genBarcode128(String barCodeValue) throws Exception {
        BufferedImage barcodeImg = null;
        Code128Bean code128Bean = new Code128Bean();
        final int dpi = 150;

        code128Bean.setFontSize(0);
        code128Bean.setHeight(10);
        code128Bean.setModuleWidth(UnitConv.in2mm(5 * 1.0f / dpi)); //  5 pixels width per bar
        //set some spaces for the sides of the barcode
        //code128Bean.doQuietZone(true);
        //code128Bean.setQuietZone(2);
        //code128Bean.doQuietZone(false);
        try {
            BitmapCanvasProvider provider = new BitmapCanvasProvider(dpi, BufferedImage.TYPE_INT_RGB, true, 0);
            code128Bean.generateBarcode(provider, barCodeValue);
            provider.finish();
            barcodeImg = provider.getBufferedImage();
        } catch (IOException e) {
            throw new MoeServiceException(e.getMessage(), e);
        }
        return barcodeImg;
    }

    // Resize & rotate temp barcode 128 image
    private BufferedImage createResizedCopy(BufferedImage image, int scaledWidth, int scaledHeight, int currentAngle, int x, int y) {
        BufferedImage scaledBI = null;
        if (currentAngle == 90 || currentAngle == -270 || currentAngle == -90 || currentAngle == 270) {
            scaledBI = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = scaledBI.createGraphics();
            g.drawImage(image, x, y, scaledWidth, scaledHeight, null);
            g.dispose();
            scaledBI = changeOrientation(scaledBI, currentAngle);
        } else {
            scaledBI = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = scaledBI.createGraphics();
            g.rotate(Math.toRadians(currentAngle), Double.valueOf(Integer.valueOf(scaledHeight).toString()) / 2,
                    Double.valueOf(Integer.valueOf(scaledWidth).toString()) / 2);
            g.drawImage(image, x, y, scaledWidth, scaledHeight, null);
        }

        return scaledBI;
    }

    // make temporary barcode 128 image vertically
    private BufferedImage changeOrientation(BufferedImage image, int currentAngle) {
        int j = image.getWidth();
        int i = image.getHeight();

        BufferedImage rotatedBI = new BufferedImage(i, j, BufferedImage.TYPE_INT_RGB);
        int p = 0;
        if (currentAngle == 90 || currentAngle == -270) {
            for (int x1 = 0; x1 < j; x1++) {
                for (int y1 = 0; y1 < i; y1++) {
                    p = image.getRGB(x1, y1);
                    rotatedBI.setRGB(i - 1 - y1, x1, p);
                }
            }
        } else {
            for (int x1 = 0; x1 < j; x1++) {
                for (int y1 = 0; y1 < i; y1++) {
                    p = image.getRGB(x1, y1);
                    rotatedBI.setRGB(y1, j - 1 - x1, p);
                }
            }
        }

        return rotatedBI;
    }

    private void typeN(MoeOrderPo moeOrder,
                       MoeMedProfilePo moeMedProfile,
                       MoeEhrMedProfilePo moeEhrMedProfile,
                       List<ReportListItemBo> moeHospitalItemList,
                       PrescriptionListItemBo prescriptionListItem) {

        StringBuilder drugValue = new StringBuilder("  ");
        StringBuilder drugValueDetail = new StringBuilder();

        List<ChunkBo> drugDosageList = new ArrayList<ChunkBo>();
        List<ChunkBo> drugFreqList = new ArrayList<ChunkBo>();

        if (moeMedProfile.getDosage() != null
                && moeMedProfile.getModu() != null) {

            String dosageValue = "";
            DecimalFormat formatter = new DecimalFormat("###,###.####");
            dosageValue = formatter.format(moeMedProfile.getDosage().doubleValue());

            String modu = HtmlUtils.htmlEscape(Formatter.getModu(moeMedProfile.getModu()));
            drugValue.append(dosageValue + " " + modu);

            ChunkBo chunk = new ChunkBo(true, false, false, dosageValue + " " + modu);
            drugDosageList.add(chunk);
        }

        // handle freqText and specialInterval
        Long supplFreqId = moeEhrMedProfile.getSupplFreqId();
        String freqText = moeMedProfile.getFreqText();
        freqText = StringUtils.isEmpty(freqText) ? "" : " " + HtmlUtils.htmlEscape(freqText);

        if (supplFreqId != null && StringUtils.isNotEmpty(freqText)) {
            List<MoeSupplFreqDescPo> moeSupplFreqDescList = moeSupplFreqDescRepository.findRecordBySupplFreqId(supplFreqId);
            if (moeSupplFreqDescList != null) {

                String freqCode = moeMedProfile.getFreqCode();
                Long supFreq1 = moeMedProfile.getSupFreq1();

                String displayWithFreq = null;
                String specialInterval = "";
                int accessLevel = 4;    // 4 access level,
                // 1: isFreqCodeSame && isSupFreq1Same
                // 2: isFreqCodeSame && recordSupFreq1 == null
                // 3: StringUtils.isEmpty(recordFreqCode) && isSupFreq1Same
                // 4: StringUtils.isEmpty(recordFreqCode) && recordSupFreq1 == null

                for (MoeSupplFreqDescPo moeSupplFreqDesc : moeSupplFreqDescList) {
                    String recordFreqCode = moeSupplFreqDesc.getFreqCode();
                    Long recordSupFreq1 = moeSupplFreqDesc.getSupplFreq1();

                    boolean isFreqCodeSame = (freqCode == null && recordFreqCode == null)
                            || (freqCode != null && recordFreqCode != null && freqCode.equals(recordFreqCode));
                    boolean isSupFreq1Same = (supFreq1 == null && recordSupFreq1 == null)
                            || (supFreq1 != null && recordSupFreq1 != null && supFreq1.equals(recordSupFreq1));

                    if (isFreqCodeSame && isSupFreq1Same) {
                        displayWithFreq = moeSupplFreqDesc.getDisplayWithFreq();
                        specialInterval = moeSupplFreqDesc.getSupplFreqDescEng();
                        if (StringUtils.isEmpty(specialInterval)) {
                            specialInterval = "";
                        }
                        break;
                    } else if (isFreqCodeSame && recordSupFreq1 == null) {
                        accessLevel = 2;
                        displayWithFreq = moeSupplFreqDesc.getDisplayWithFreq();
                        specialInterval = moeSupplFreqDesc.getSupplFreqDescEng();
                        if (StringUtils.isEmpty(specialInterval)) {
                            specialInterval = "";
                        }
                    } else if (StringUtils.isEmpty(recordFreqCode) && isSupFreq1Same) {
                        if (accessLevel < 3) {
                            continue;
                        }
                        accessLevel = 3;
                        displayWithFreq = moeSupplFreqDesc.getDisplayWithFreq();
                        specialInterval = moeSupplFreqDesc.getSupplFreqDescEng();
                        if (StringUtils.isEmpty(specialInterval)) {
                            specialInterval = "";
                        }
                    } else if (StringUtils.isEmpty(recordFreqCode)
                            && recordSupFreq1 == null) {
                        if (accessLevel < 4) {
                            continue;
                        }
                        accessLevel = 4;
                        displayWithFreq = moeSupplFreqDesc.getDisplayWithFreq();
                        specialInterval = moeSupplFreqDesc.getSupplFreqDescEng();
                        if (StringUtils.isEmpty(specialInterval)) {
                            specialInterval = "";
                        }
                    }
                }

                boolean case1 = displayWithFreq != null
                        && MoeCommonHelper.isFlagTrue(displayWithFreq)
                        && StringUtils.isNotEmpty(freqText)
                        && StringUtils.isNotEmpty(specialInterval);
                boolean isSpecialIntervalValid = StringUtils.isNotEmpty(specialInterval);

                if (case1) {
                    if (addHyphen(drugValueDetail) || addHyphen(drugValue)) {
                        drugValueDetail.append(" " + hyphen);

                        ChunkBo chunk = new ChunkBo(false, false, false, " " + hyphen);
                        drugFreqList.add(chunk);
                    }
                    drugValueDetail.append(freqText + " (");

                    ChunkBo chunk = new ChunkBo(false, false, false, freqText + " (");
                    drugFreqList.add(chunk);
                } else if (isSpecialIntervalValid && (addHyphen(drugValueDetail) || addHyphen(drugValue))) {
                    drugValueDetail.append(" " + hyphen + " ");

                    ChunkBo chunk = new ChunkBo(false, false, false, " " + hyphen + " ");
                    drugFreqList.add(chunk);
                }

                // add the content of special interval value
                if (isSpecialIntervalValid) {
                    String regimen = moeMedProfile.getRegimen();
                    String dayOfWeek = moeMedProfile.getDayOfWeek();
                    boolean isDayOfWeekValid = StringUtils.isNotEmpty(dayOfWeek) && dayOfWeek.length() == 7 && dayOfWeek.contains("1");

                    if (MoeCommonHelper.isRegimentTypeWeekly(regimen) && isDayOfWeekValid) {
                        drugValueDetail.append("on every");

                        ChunkBo chunk1 = new ChunkBo(false, false, false, "on every");
                        drugFreqList.add(chunk1);

                        StringBuilder daySB = new StringBuilder();
                        for (int i = 0; i < 7; i++) {
                            if ('1' == dayOfWeek.charAt(i)) {
                                // Ricci 20190809 start --
                                //List<String> strings = moeSupplFreqDescRepository.findDescEngBySupplFreqIdSelectionValue(supplFreqId, i + 1);
                                List<String> strings = moeSupplFreqDescRepository.findDescEngBySupplFreqIdSelectionValue(supplFreqId, Long.valueOf(Integer.valueOf(i + 1).toString()));
                                // Ricci 20190809 end --
                                String day = (strings != null && !strings.isEmpty()) ? strings.get(0) : null;
                                if (StringUtils.isNotEmpty(day)) {
                                    daySB.append(" " + day + ",");
                                }
                            }
                        }
                        drugValueDetail.append(daySB.substring(0, daySB.length() - 1));

                        ChunkBo chunk2 = new ChunkBo(false, false, false, daySB.substring(0, daySB.length() - 1));
                        drugFreqList.add(chunk2);
                    } else {

                        String supFreq1Value = moeMedProfile.getSupFreq1() == null ? "" : moeMedProfile.getSupFreq1().toString();
                        String supFreq2Value = moeMedProfile.getSupFreq2() == null ? "" : moeMedProfile.getSupFreq2().toString();
                        String cycleMultiplierValue = moeEhrMedProfile.getCycleMultiplier() == null ? "" : moeEhrMedProfile.getCycleMultiplier().toString();

                        boolean isCType = MoeCommonHelper.isRegimentTypeCycle(regimen);
                        int numReplace = StringUtils.countMatches(specialInterval, "__");


                        if (numReplace == 0) {
                            //--- do nothing ---
                        } else if (numReplace == 1 && isCType) {
                            specialInterval = specialInterval.replaceFirst("__", " " + cycleMultiplierValue + " ");
                        } else if (numReplace == 1 && !isCType) {
                            specialInterval = specialInterval.replaceFirst("__", " " + supFreq1Value + " ");
                        } else if (numReplace == 2 && isCType) {
                            specialInterval = specialInterval.replaceFirst("__", " " + supFreq1Value + " ");
                            specialInterval = specialInterval.replaceFirst("__", " " + cycleMultiplierValue + " ");
                        } else if (numReplace == 2 && !isCType) {
                            specialInterval = specialInterval.replaceFirst("__", " " + supFreq1Value + " ");
                            specialInterval = specialInterval.replaceFirst("__", " " + supFreq2Value + " ");
                        } else if (numReplace == 3) {
                            specialInterval = specialInterval.replaceFirst("__", " " + supFreq1Value + " ");
                            specialInterval = specialInterval.replaceFirst("__", " " + supFreq2Value + " ");
                            specialInterval = specialInterval.replaceFirst("__", " " + cycleMultiplierValue + " ");
                        }

                        // replace double spaces to single space and trim the string
                        specialInterval = StringUtils.replace(specialInterval, "  ", " ");
                        specialInterval = StringUtils.trim(specialInterval);
                        drugValueDetail.append(HtmlUtils.htmlEscape(specialInterval));

                        ChunkBo chunk = new ChunkBo(false, false, false, HtmlUtils.htmlEscape(specialInterval));
                        drugFreqList.add(chunk);
                    }
                }

                // add back a close bracket
                if (case1) {
                    drugValueDetail.append(")");

                    ChunkBo chunk = new ChunkBo(false, false, false, ")");
                    drugFreqList.add(chunk);
                }
            }
        } else if (StringUtils.isNotEmpty(freqText)) {
            if (addHyphen(drugValueDetail) || addHyphen(drugValue)) {
                drugValueDetail.append(" " + hyphen);

                ChunkBo chunk = new ChunkBo(false, false, false, " " + hyphen);
                drugFreqList.add(chunk);
            }
            drugValueDetail.append(freqText);

            ChunkBo chunk = new ChunkBo(false, false, false, freqText);
            drugFreqList.add(chunk);
        }

        String prn = moeMedProfile.getPrn();
        if (YES.equalsIgnoreCase(prn)) {
            if (addHyphen(drugValueDetail) || addHyphen(drugValue)) {
                drugValueDetail.append(" " + hyphen + " ");

                ChunkBo chunk = new ChunkBo(false, false, false, " " + hyphen + " ");
                drugFreqList.add(chunk);
            }
            drugValueDetail.append("as required");

            ChunkBo chunk = new ChunkBo(false, false, false, "as required");
            drugFreqList.add(chunk);
        }

        String routeDesc = moeMedProfile.getRouteDesc();
        routeDesc = routeDesc == null ? "" : HtmlUtils.htmlEscape(routeDesc);
        String siteDesc = moeMedProfile.getSiteDesc();
        siteDesc = siteDesc == null ? "" : HtmlUtils.htmlEscape(siteDesc);
        String siteDescNonHtml = moeMedProfile.getSiteDesc() == null ? "" : moeMedProfile.getSiteDesc();
        String routeSiteMapKey = routeDesc + " " + siteDesc;
        String routeSiteMapValue = DtoUtil.ROUTE_SITE_MAP.get(routeSiteMapKey);

        if (!routeDesc.isEmpty()) {
            if (siteDesc.length() > 0 && (routeSiteMapValue == null || YES.equalsIgnoreCase(routeSiteMapValue))) {
                if (addHyphen(drugValueDetail) || addHyphen(drugValue)) {
                    drugValueDetail.append(" " + hyphen + " ");

                    ChunkBo chunk = new ChunkBo(false, false, false, " " + hyphen + " ");
                    drugFreqList.add(chunk);
                }
                drugValueDetail.append(routeDesc + " (" + siteDesc + ")");

                ChunkBo chunk = new ChunkBo(false, false, false, routeDesc + " (" + siteDescNonHtml + ")");
                drugFreqList.add(chunk);
            } else {
                if (addHyphen(drugValueDetail) || addHyphen(drugValue)) {
                    drugValueDetail.append(" " + hyphen + " ");

                    ChunkBo chunk = new ChunkBo(false, false, false, " " + hyphen + " ");
                    drugFreqList.add(chunk);
                }
                drugValueDetail.append(routeDesc);

                ChunkBo chunk = new ChunkBo(false, false, false, routeDesc);
                drugFreqList.add(chunk);
            }
        }

        // append duration and unit
        Long duration = moeMedProfile.getDuration();
        String durationUnit = moeMedProfile.getDurationUnit();
        String durationUnitValue = null;
        if (duration != null && durationUnit != null) {
            String record = DtoUtil.DURATION_UNIT_DESC_MAP.get(durationUnit);
            if (record != null) {
                durationUnitValue = record;
            }
            if (StringUtils.isNotBlank(durationUnitValue)) {
                if (duration.longValue() == 1L) {
                    durationUnitValue = StringUtils.remove(durationUnitValue, "(s)");
                } else {
                    durationUnitValue = StringUtils.replace(durationUnitValue, "(s)", "s");
                }
            }
        }

        if (duration != null && durationUnitValue != null) {
            if (addHyphen(drugValueDetail) || addHyphen(drugValue)) {
                drugValueDetail.append(" " + hyphen + " ");

                ChunkBo chunk = new ChunkBo(false, false, false, " " + hyphen + " ");
                drugFreqList.add(chunk);
            }
            drugValueDetail.append("for " + duration.toString() + " " + durationUnitValue);

            ChunkBo chunk = new ChunkBo(false, false, false, "for " + duration.toString() + " " + durationUnitValue);
            drugFreqList.add(chunk);
        }

        // check if dates are same
        Date startDate = moeMedProfile.getStartDate();
        Date createDate = moeOrder.getOrdDate();
        boolean isDateSame = false;
        if (startDate != null && createDate != null) {
            Calendar startDateCal = Calendar.getInstance();
            startDateCal.setTime(startDate);
            Calendar createDateCal = Calendar.getInstance();
            createDateCal.setTime(createDate);

            isDateSame = startDateCal.get(Calendar.YEAR) == createDateCal.get(Calendar.YEAR)
                    && startDateCal.get(Calendar.MONTH) == createDateCal.get(Calendar.MONTH)
                    && startDateCal.get(Calendar.DATE) == createDateCal.get(Calendar.DATE);
        }

        if (!isDateSame && startDate != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
            drugValueDetail.append(" (Start from: " + sdf.format(startDate) + ")");

            ChunkBo chunk = new ChunkBo(false, false, false, " (Start from: " + sdf.format(startDate) + ")");
            drugFreqList.add(chunk);
        }

        ReportListItemBo reportListItem = new ReportListItemBo();
        reportListItem.setLevel(3);
        reportListItem.setDrugValue(drugValue.toString());
/*		if (drugValue.toString().trim().length()==0
					&& drugValueDetail.toString().contains(hyphen)) {
			drugValueDetail = new StringBuilder(drugValueDetail.toString().substring(3));
		}*/
        reportListItem.setDrugValueDetail(drugValueDetail.toString());

        moeHospitalItemList.add(reportListItem);

        PrescriptionDrugDosageListItemBo drugDosageListItem = new PrescriptionDrugDosageListItemBo();
        drugDosageListItem.setDrugDosageList(drugDosageList);
        drugDosageListItem.setDrugFreqList(drugFreqList);
        prescriptionListItem.getPrescriptionDrugDosageList().add(drugDosageListItem);
    }

    private void typeM(MoeOrderPo moeOrder,
                       MoeMedMultDosePo moeMedMultDose,
                       MoeMedProfilePo moeMedProfile,
                       MoeEhrMedProfilePo moeEhrMedProfile,
                       boolean isLastLine,
                       List<ReportListItemBo> moeHospitalItemList,
                       PrescriptionListItemBo prescriptionListItem) {

        StringBuilder drugValue = new StringBuilder("  ");
        StringBuilder drugValueDetail = new StringBuilder();

        List<ChunkBo> drugDosageList = new ArrayList<ChunkBo>();
        List<ChunkBo> drugFreqList = new ArrayList<ChunkBo>();

        if (moeMedMultDose.getDosage() != null
                && moeMedProfile.getModu() != null) {

            String dosageValue = "";
            DecimalFormat formatter = new DecimalFormat("###,###.####");
            dosageValue = formatter.format(moeMedMultDose.getDosage().doubleValue());

            String modu = HtmlUtils.htmlEscape(Formatter.getModu(moeMedProfile.getModu()));
            drugValue.append(dosageValue + " " + modu);

            ChunkBo chunk = new ChunkBo(true, false, false, dosageValue + " " + modu);
            drugDosageList.add(chunk);
        }

        String supplFreqText = getSupplFreqText(moeMedMultDose, drugValue.toString().trim().length() > 0);
        drugValueDetail.append(supplFreqText);

        ChunkBo chunk = new ChunkBo(false, false, false, supplFreqText);
        drugFreqList.add(chunk);

        String regimen = moeMedProfile.getRegimen();
        if (MoeCommonHelper.isRegimentTypeCycle(regimen)) {
            Long cycleMultiplier = moeEhrMedProfile.getCycleMultiplier();
            boolean isValid = cycleMultiplier != null;
            if (isValid) {
                drugValueDetail.append(". Each cycle is " + cycleMultiplier.toString() + " days");

                ChunkBo chunk1 = new ChunkBo(false, false, false, ". Each cycle is " + cycleMultiplier.toString() + " days");
                drugFreqList.add(chunk1);
            }
        }

        String prn = moeMedProfile.getPrn();
        if (YES.equalsIgnoreCase(prn)) {
            if (addHyphen(drugValueDetail) || addHyphen(drugValue)) {
                drugValueDetail.append(" " + hyphen + " ");

                ChunkBo chunk1 = new ChunkBo(false, false, false, " " + hyphen + " ");
                drugFreqList.add(chunk1);
            }
            drugValueDetail.append("as required");

            ChunkBo chunk1 = new ChunkBo(false, false, false, "as required");
            drugFreqList.add(chunk1);
        }

        if (YES.equalsIgnoreCase(moeMedProfile.getDangerdrug())) {
            DecimalFormat formatter = new DecimalFormat("###,###.####");
            String moQtyUnit = Formatter.getModu(moeMedMultDose.getMoQtyUnit());
            drugValueDetail.append(" " + hyphen + " " + " <style isBold='true'>(prescribe ");
            drugValueDetail.append(formatter.format(moeMedMultDose.getMoQty()) + " " + moQtyUnit);
            drugValueDetail.append(")</style>");

            ChunkBo chunk1 = new ChunkBo(false, false, false, " " + hyphen + " ");
            ChunkBo chunk2 = new ChunkBo(true, false, false, " (prescribe " + formatter.format(moeMedMultDose.getMoQty()) + " " + moQtyUnit + ")");
            drugFreqList.add(chunk1);
            drugFreqList.add(chunk2);
        }

        if (!isLastLine) {
            if (addHyphen(drugValueDetail) || addHyphen(drugValue)) {
                drugValueDetail.append(" " + hyphen + " ");

                ChunkBo chunk1 = new ChunkBo(false, false, false, " " + hyphen + " ");
                drugFreqList.add(chunk1);
            }
            drugValueDetail.append("<style isBold='true'> and</style>");

            ChunkBo chunk1 = new ChunkBo(true, false, false, " and");
            drugFreqList.add(chunk1);

            ReportListItemBo reportListItem = new ReportListItemBo();
            reportListItem.setLevel(3);

            reportListItem.setDrugValue(drugValue.toString());

            reportListItem.setDrugValueDetail(drugValueDetail.toString());

            moeHospitalItemList.add(reportListItem);

            PrescriptionDrugDosageListItemBo drugDosageListItem = new PrescriptionDrugDosageListItemBo();
            drugDosageListItem.setDrugDosageList(drugDosageList);
            drugDosageListItem.setDrugFreqList(drugFreqList);
            prescriptionListItem.getPrescriptionDrugDosageList().add(drugDosageListItem);

            return;
        }

        // following only run when last line
        String routeDesc = moeMedProfile.getRouteDesc();
        routeDesc = routeDesc == null ? "" : HtmlUtils.htmlEscape(routeDesc);
        String siteDesc = moeMedProfile.getSiteDesc();
        siteDesc = siteDesc == null ? "" : HtmlUtils.htmlEscape(siteDesc);
        String siteDescNonHtml = moeMedProfile.getSiteDesc() == null ? "" : moeMedProfile.getSiteDesc();
        String routeSiteMapKey = routeDesc + " " + siteDesc;
        //java.util.Map.Entry<String, String> routeSiteMapKey = new java.util.AbstractMap.SimpleEntry<String, String>(routeDesc, siteDesc);
        String routeSiteMapValue = DtoUtil.ROUTE_SITE_MAP.get(routeSiteMapKey);

        if (!routeDesc.isEmpty()) {
            if (siteDesc.length() > 0 && (routeSiteMapValue == null || YES.equalsIgnoreCase(routeSiteMapValue))) {
                if (addHyphen(drugValueDetail) || addHyphen(drugValue)) {
                    drugValueDetail.append(" " + hyphen + " ");

                    ChunkBo chunk1 = new ChunkBo(false, false, false, " " + hyphen + " ");
                    drugFreqList.add(chunk1);
                }
                drugValueDetail.append(routeDesc + " (" + siteDesc + ")");

                ChunkBo chunk1 = new ChunkBo(false, false, false, routeDesc + " (" + siteDescNonHtml + ")");
                drugFreqList.add(chunk1);
            } else {
                if (addHyphen(drugValueDetail) || addHyphen(drugValue)) {
                    drugValueDetail.append(" " + hyphen + " ");

                    ChunkBo chunk1 = new ChunkBo(false, false, false, " " + hyphen + " ");
                    drugFreqList.add(chunk1);
                }
                drugValueDetail.append(routeDesc);

                ChunkBo chunk1 = new ChunkBo(false, false, false, routeDesc);
                drugFreqList.add(chunk1);
            }
        }

        // append duration and unit
        Long duration = moeMedProfile.getDuration();
        String durationUnit = moeMedProfile.getDurationUnit();
        String durationUnitValue = null;
        if (duration != null && durationUnit != null) {
            String record = DtoUtil.DURATION_UNIT_DESC_MAP.get(durationUnit);
            if (record != null) {
                durationUnitValue = record;
            }
            if (StringUtils.isNotBlank(durationUnitValue)) {
                if (duration.longValue() == 1L) {
                    durationUnitValue = StringUtils.remove(durationUnitValue, "(s)");
                } else {
                    durationUnitValue = StringUtils.replace(durationUnitValue, "(s)", "s");
                }
            }
        }

        if (duration != null && durationUnitValue != null) {
            if (addHyphen(drugValueDetail)) {
                drugValueDetail.append(" " + hyphen + " ");

                ChunkBo chunk1 = new ChunkBo(false, false, false, " " + hyphen + " ");
                drugFreqList.add(chunk1);
            }
            drugValueDetail.append("for " + duration.toString() + " " + durationUnitValue);

            ChunkBo chunk1 = new ChunkBo(false, false, false, "for " + duration.toString() + " " + durationUnitValue);
            drugFreqList.add(chunk1);
        }

        // check if dates are same
        Date startDate = moeMedProfile.getStartDate();
        Date createDate = moeOrder.getOrdDate();
        boolean isDateSame = false;
        if (startDate != null && createDate != null) {
            Calendar startDateCal = Calendar.getInstance();
            startDateCal.setTime(startDate);
            Calendar createDateCal = Calendar.getInstance();
            createDateCal.setTime(createDate);

            isDateSame = startDateCal.get(Calendar.YEAR) == createDateCal.get(Calendar.YEAR)
                    && startDateCal.get(Calendar.MONTH) == createDateCal.get(Calendar.MONTH)
                    && startDateCal.get(Calendar.DATE) == createDateCal.get(Calendar.DATE);
        }

        if (!isDateSame && startDate != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
            drugValueDetail.append(" (Start from: " + sdf.format(startDate) + ")");

            ChunkBo chunk1 = new ChunkBo(false, false, false, " (Start from: " + sdf.format(startDate) + ")");
            drugFreqList.add(chunk1);
        }

        ReportListItemBo reportListItem = new ReportListItemBo();
        reportListItem.setLevel(3);

        reportListItem.setDrugValue(drugValue.toString());

        reportListItem.setDrugValueDetail(drugValueDetail.toString());

        moeHospitalItemList.add(reportListItem);

        PrescriptionDrugDosageListItemBo drugDosageListItem = new PrescriptionDrugDosageListItemBo();
        drugDosageListItem.setDrugDosageList(drugDosageList);
        drugDosageListItem.setDrugFreqList(drugFreqList);
        prescriptionListItem.getPrescriptionDrugDosageList().add(drugDosageListItem);
    }

    private void typeS(MoeOrderPo moeOrder,
                       MoeMedMultDosePo moeMedMultDose,
                       MoeMedProfilePo moeMedProfile,
                       boolean isFirstLine,
                       boolean isLastLine,
                       List<ReportListItemBo> moeHospitalItemList,
                       PrescriptionListItemBo prescriptionListItem) {

        StringBuilder drugValue = new StringBuilder("  ");
        StringBuilder drugValueDetail = new StringBuilder();

        List<ChunkBo> drugDosageList = new ArrayList<ChunkBo>();
        List<ChunkBo> drugFreqList = new ArrayList<ChunkBo>();

        if (moeMedMultDose.getDosage() != null
                && moeMedProfile.getModu() != null) {

            String dosageValue = "";
            DecimalFormat formatter = new DecimalFormat("###,###.####");
            dosageValue = formatter.format(moeMedMultDose.getDosage().doubleValue());

            String modu = HtmlUtils.htmlEscape(Formatter.getModu(moeMedProfile.getModu()));
            drugValue.append(dosageValue + " " + modu);

            ChunkBo chunk = new ChunkBo(true, false, false, dosageValue + " " + modu);
            drugDosageList.add(chunk);
        }

        // append freq
        String freq = moeMedMultDose.getFreqText();
        if (StringUtils.isNotEmpty(freq)) {
            if (addHyphen(drugValue)) {
                drugValueDetail.append(" " + hyphen + " ");

                ChunkBo chunk = new ChunkBo(false, false, false, " " + hyphen + " ");
                drugFreqList.add(chunk);
            }

            drugValueDetail.append(HtmlUtils.htmlEscape(freq));

            ChunkBo chunk = new ChunkBo(false, false, false, HtmlUtils.htmlEscape(freq));
            drugFreqList.add(chunk);
        }

        String prn = moeMedMultDose.getPrn();
        if (MoeCommonHelper.isFlagTrue(prn)) {
            if (addHyphen(drugValueDetail) || addHyphen(drugValue)) {
                drugValueDetail.append(" " + hyphen + " ");

                ChunkBo chunk = new ChunkBo(false, false, false, " " + hyphen + " ");
                drugFreqList.add(chunk);
            }
            drugValueDetail.append("as required");

            ChunkBo chunk = new ChunkBo(false, false, false, "as required");
            drugFreqList.add(chunk);
        }

        if (YES.equalsIgnoreCase(moeMedProfile.getDangerdrug())) {
            DecimalFormat formatter = new DecimalFormat("###,###.####");
            String moQtyUnit = Formatter.getModu(moeMedMultDose.getMoQtyUnit());
            if (addHyphen(drugValueDetail)) {
                drugValueDetail.append(" " + hyphen + " ");

                ChunkBo chunk = new ChunkBo(false, false, false, " " + hyphen + " ");
                drugFreqList.add(chunk);
            }
            drugValueDetail.append(" <style isBold='true'>(prescribe ");
            drugValueDetail.append(formatter.format(moeMedMultDose.getMoQty()) + " " + moQtyUnit);
            drugValueDetail.append(")</style>");

            ChunkBo chunk = new ChunkBo(true, false, false, " (prescribe " + formatter.format(moeMedMultDose.getMoQty()) + " " + moQtyUnit + ")");
            drugFreqList.add(chunk);
        }

        // append site, route
        String routeDesc = moeMedProfile.getRouteDesc();
        routeDesc = routeDesc == null ? "" : HtmlUtils.htmlEscape(routeDesc);
        String siteDesc = moeMedProfile.getSiteDesc();
        siteDesc = siteDesc == null ? "" : HtmlUtils.htmlEscape(siteDesc);
        String siteDescNonHtml = moeMedProfile.getSiteDesc() == null ? "" : moeMedProfile.getSiteDesc();
        String routeSiteMapKey = routeDesc + " " + siteDesc;
        //java.util.Map.Entry<String, String> routeSiteMapKey = new java.util.AbstractMap.SimpleEntry<String, String>(routeDesc, siteDesc);
        String routeSiteMapValue = DtoUtil.ROUTE_SITE_MAP.get(routeSiteMapKey);

        if (!routeDesc.isEmpty()) {
            if (siteDesc.length() > 0 && (routeSiteMapValue == null || YES.equalsIgnoreCase(routeSiteMapValue))) {
                if (addHyphen(drugValueDetail) || addHyphen(drugValue)) {
                    drugValueDetail.append(" " + hyphen + " ");

                    ChunkBo chunk = new ChunkBo(false, false, false, " " + hyphen + " ");
                    drugFreqList.add(chunk);
                }
                drugValueDetail.append(routeDesc + " (" + siteDesc + ")");

                ChunkBo chunk = new ChunkBo(false, false, false, routeDesc + " (" + siteDescNonHtml + ")");
                drugFreqList.add(chunk);
            } else {
                if (addHyphen(drugValueDetail) || addHyphen(drugValue)) {
                    drugValueDetail.append(" " + hyphen + " ");

                    ChunkBo chunk = new ChunkBo(false, false, false, " " + hyphen + " ");
                    drugFreqList.add(chunk);
                }
                drugValueDetail.append(routeDesc);

                ChunkBo chunk = new ChunkBo(false, false, false, routeDesc);
                drugFreqList.add(chunk);
            }
        }

        // append duration and unit
        Long duration = moeMedMultDose.getDuration();
        String durationUnit = moeMedMultDose.getDurationUnit();
        String durationUnitValue = null;
        if (duration != null && durationUnit != null) {
            String record = DtoUtil.DURATION_UNIT_DESC_MAP.get(durationUnit);
            if (record != null) {
                durationUnitValue = record;
            }
            if (StringUtils.isNotBlank(durationUnitValue)) {
                if (duration.longValue() == 1L) {
                    durationUnitValue = StringUtils.remove(durationUnitValue, "(s)");
                } else {
                    durationUnitValue = StringUtils.replace(durationUnitValue, "(s)", "s");
                }
            }
        }

        if (duration != null && durationUnitValue != null) {
            if (addHyphen(drugValueDetail) || addHyphen(drugValue)) {
                drugValueDetail.append(" " + hyphen + " ");

                ChunkBo chunk = new ChunkBo(false, false, false, " " + hyphen + " ");
                drugFreqList.add(chunk);
            }
            drugValueDetail.append("for " + duration.toString() + " " + durationUnitValue);

            ChunkBo chunk = new ChunkBo(false, false, false, "for " + duration.toString() + " " + durationUnitValue);
            drugFreqList.add(chunk);
        }

        // consider append (start from: <Date>)
        if (isFirstLine) {
            // check if dates are same
            Date startDate = moeMedProfile.getStartDate();
            Date createDate = moeOrder.getOrdDate();
            boolean isDateSame = false;
            if (startDate != null && createDate != null) {
                Calendar startDateCal = Calendar.getInstance();
                startDateCal.setTime(startDate);
                Calendar createDateCal = Calendar.getInstance();
                createDateCal.setTime(createDate);

                isDateSame = startDateCal.get(Calendar.YEAR) == createDateCal.get(Calendar.YEAR)
                        && startDateCal.get(Calendar.MONTH) == createDateCal.get(Calendar.MONTH)
                        && startDateCal.get(Calendar.DATE) == createDateCal.get(Calendar.DATE);
            }

            if (!isDateSame && startDate != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
                drugValueDetail.append(" (Start from: " + sdf.format(startDate) + ")");

                ChunkBo chunk = new ChunkBo(false, false, false, " (Start from: " + sdf.format(startDate) + ")");
                drugFreqList.add(chunk);
            }
        }

        if (!isLastLine) {
            drugValueDetail.append(", <style isBold='true'>then</style>");

            ChunkBo chunk1 = new ChunkBo(false, false, false, ", ");
            ChunkBo chunk2 = new ChunkBo(true, false, false, "then");
            drugFreqList.add(chunk1);
            drugFreqList.add(chunk2);
        }

        ReportListItemBo reportListItem = new ReportListItemBo();
        reportListItem.setLevel(3);
        reportListItem.setDrugValue(drugValue.toString());

        reportListItem.setDrugValueDetail(drugValueDetail.toString());

        moeHospitalItemList.add(reportListItem);

        PrescriptionDrugDosageListItemBo drugDosageListItem = new PrescriptionDrugDosageListItemBo();
        drugDosageListItem.setDrugDosageList(drugDosageList);
        drugDosageListItem.setDrugFreqList(drugFreqList);
        prescriptionListItem.getPrescriptionDrugDosageList().add(drugDosageListItem);
    }

    private boolean addHyphen(StringBuilder s) {
        if (s.toString().trim().length() > 0) {
            return true;
        }
        return false;
    }

    private String getSupplFreqText(MoeMedMultDosePo moeMedMultDose, boolean addHyphen) {

        String supplFreqText = "";

        List<MoeSupplFreqDescPo> tempMoeSupplFreqDescNoFreq = new ArrayList<MoeSupplFreqDescPo>();
        List<MoeSupplFreqDescPo> tempMoeSupplFreqDesc = new ArrayList<MoeSupplFreqDescPo>();

        if (moeMedMultDose.getSupFreqCode() != null
                && "on odd / even days".equalsIgnoreCase(moeMedMultDose.getSupFreqCode().trim())) {
            List<MoeSupplFreqDescPo> moeSupplFreqDescList = moeSupplFreqDescRepository.findRecordBySupplFreqId(moeMedMultDose.getMoeEhrMedMultDose().getSupplFreqId());
            if (moeSupplFreqDescList != null) {
                for (MoeSupplFreqDescPo desc : moeSupplFreqDescList) {
                    if (moeMedMultDose.getFreqCode() != null) {
                        if (desc.getFreqCode() != null && desc.getFreqCode().equals(moeMedMultDose.getFreqCode()) && NO.equalsIgnoreCase(desc.getDisplayWithFreq())) {
                            tempMoeSupplFreqDescNoFreq.add(desc);
                        } else if (YES.equalsIgnoreCase(desc.getDisplayWithFreq())) {
                            tempMoeSupplFreqDesc.add(desc);
                        }
                    }
                }
                if (!tempMoeSupplFreqDescNoFreq.isEmpty() || !tempMoeSupplFreqDesc.isEmpty()) {
                    int index = Integer.valueOf(Long.toString(moeMedMultDose.getStepNo())) - 1;
                    if (!tempMoeSupplFreqDescNoFreq.isEmpty()) {
                        supplFreqText = tempMoeSupplFreqDescNoFreq.get(index).getSupplFreqDescEng();
                    } else {
                        supplFreqText = HtmlUtils.htmlEscape(moeMedMultDose.getFreqText()) + " (" + tempMoeSupplFreqDesc.get(index).getSupplFreqDescEng() + ")";
                    }
                }

            }
        } else if (moeMedMultDose.getFreqText() != null) {
            supplFreqText += HtmlUtils.htmlEscape(moeMedMultDose.getFreqText());
        }

        if (addHyphen && supplFreqText.length() > 0) {
            return " " + hyphen + " " + supplFreqText;
        }
        return supplFreqText;
    }

    private void markStrikethru(PrescriptionListItemBo prescriptionListItem) {
        for (ChunkBo cell : prescriptionListItem.getDrugNameList()) {
            cell.setIsStrikethru(true);
        }
        for (ChunkBo cell : prescriptionListItem.getDrugNameDetailList()) {
            cell.setIsStrikethru(true);
        }
        for (ChunkBo cell : prescriptionListItem.getSpecInstructList()) {
            cell.setIsStrikethru(true);
        }
        for (ChunkBo cell : prescriptionListItem.getRemarkList()) {
            cell.setIsStrikethru(true);
        }
        for (PrescriptionDrugDosageListItemBo prescriptionDrugDosageListItem : prescriptionListItem.getPrescriptionDrugDosageList()) {
            for (ChunkBo cell : prescriptionDrugDosageListItem.getDrugDosageList()) {
                cell.setIsStrikethru(true);
            }
            for (ChunkBo cell : prescriptionDrugDosageListItem.getDrugExtInfoList()) {
                cell.setIsStrikethru(true);
            }
            for (ChunkBo cell : prescriptionDrugDosageListItem.getDrugFreqList()) {
                cell.setIsStrikethru(true);
            }
        }
    }

    public String getAllergyDisplayName(MoePatientAllergyDto allergy) {
        // free text
        if (allergy.getAllergen() == null) {
            return allergy.getDisplayName();
        }
        if (StringUtils.countMatches(allergy.getAllergen().getVtm(), "+") > 2) {
            String displayName = allergy.getDisplayName().replace("(" + allergy.getAllergen().getVtm() + ")", "");
            if (displayName.length() > 0) {
                return displayName;
            } else {
                return allergy.getDisplayName();
            }
        } else {
            return allergy.getDisplayName();
        }
    }

    public String getAdrDisplayName(MoePatientAdrDto adr) {
        // free text
        if (adr.getAllergen() == null) {
            return adr.getDrugDesc();
        }
        if (StringUtils.countMatches(adr.getAllergen().getVtm(), "+") > 2) {
            String displayName = adr.getDrugDesc().replace("(" + adr.getAllergen().getVtm() + ")", "");
            if (displayName.length() > 0) {
                return displayName;
            } else {
                return adr.getDrugDesc();
            }
        } else {
            return adr.getDrugDesc();
        }
    }

    // Ricci 20190918 start --DHCIMSB-869
    private String splitStrength(String strength, String strengthCompulsory) {
        if (ServerConstant.STRENGTH_COMPULSORY_YES.equalsIgnoreCase(strengthCompulsory)) {
            String[] strengths = strength.split("\\+");
            if (strengths.length != 0) {
                return strengths[0];
            } else {
                return "";
            }
        } else {
            return strength;
        }
    }
    // Ricci 20190918 end --DHCIMSB-869

}
