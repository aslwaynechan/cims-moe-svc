package hk.health.moe.facade;

import hk.health.moe.pojo.dto.inner.InnerSaveDrugDto;

public interface MoeDrugGenericNameLocalFacade {

    boolean checkIsMoeDrugGenericNameLocalExist(InnerSaveDrugDto dto) throws Exception;

    void addGenericNameLocalRecord(InnerSaveDrugDto dto) throws Exception;

    void deleteGenericNameLocalRecord(InnerSaveDrugDto dto) throws Exception;

}
