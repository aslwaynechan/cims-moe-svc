package hk.health.moe.facade;

import hk.health.moe.pojo.dto.inner.InnerSaveDrugDto;

public interface MoeDrugTherapeuticLocalFacade {

    void saveDrugByTherapeuticLocal(InnerSaveDrugDto dto) throws Exception;

    // Simon 20191014 start--
    boolean deleteDrugByTherapeuticLocal(String localDrugId) throws Exception;
    //Simon 20191014 end--
}
