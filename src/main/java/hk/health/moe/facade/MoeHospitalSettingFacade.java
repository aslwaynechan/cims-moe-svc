package hk.health.moe.facade;

import hk.health.moe.pojo.dto.MoeHospitalSettingDto;

import java.util.List;

public interface MoeHospitalSettingFacade {

    List<MoeHospitalSettingDto> getAllSpecialtyAvailability() throws Exception;

    List<MoeHospitalSettingDto> getAllLocationByUserGroupMap() throws Exception;

    List<MoeHospitalSettingDto> getAllLocationByUserGroupMap(String hospCode) throws Exception;
}
