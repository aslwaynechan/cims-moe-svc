package hk.health.moe.facade;

import hk.health.moe.pojo.dto.DrugDetailDto;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.po.MoeDrugPo;

import java.util.List;

public interface MoeDrugFacade {
     //Simon 20190822 for moe_drug_server start--
     boolean isLocalDrugKeyValid(boolean isNewARecord,
                                       String localDrugId, String hkRegNo);
     boolean isDrugParaCombinationValid(boolean isNewARecord,
                                              String localDrugId, String hkRegNo, Long strengthRank);
     DrugDetailDto newDrugLocalRecord(String localDrugId, String hkRegNo, Long strengthRank);
     List<MoeDrugDto> findDrugByHkRegNo(String hkRegNo, Long rank);
     List<MoeDrugPo> findMoeDrugByHkRegNo(String hkRegNo);
     //Simon 20190822 for moe_drug_server end--

     //Simon 20190823 for moe_drug_server start--
     /**
      * PURPOSE		:	prepare drug local record and return
      * @param localDrugId
      * @param hkRegNo
      * @return DrugDetailDTO
      */
     DrugDetailDto getDrugLocalRecord(String localDrugId, String hkRegNo);
     List<MoeDrugDto> findById(String id);
     List<MoeDrugDto> findByHkRegNo(String hkRegNo);
     //Simon 20190823 for moe_drug_server end--

     //Simon 20190828 start--
     List<MoeDrugDto> listRelatedDrug(String localDrugId, String vtm, String tradeName, String routeEng, String formEng, String doseFormExtraInfo, String hospCode) throws Exception;
     //Simon 20190828 end--

     //Chris 20190829  --Start
     List<MoeDrugDto> findByAmp(String amp) throws Exception;
     //Chris 20190829  --End
}
