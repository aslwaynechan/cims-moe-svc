package hk.health.moe.facade;

import hk.health.moe.pojo.dto.MoeDrugBySpecialtyDto;
import hk.health.moe.pojo.dto.inner.InnerSaveDrugBySpecialtyDto;

import java.util.List;

public interface MoeDrugBySpecialtyFacade {

    void saveDrugBySpecialty(List<InnerSaveDrugBySpecialtyDto> drugBySpecialtyItems) throws Exception;

    List<MoeDrugBySpecialtyDto> getDrugBySpecialtyByLocalDrugId(String localDrugId, String hospitalCode) throws Exception;

    // Simon 20191014 start--
    boolean deleteDrugBySpecialty(String localDrugId) throws Exception;
    //Simon 20191014 end--
}
