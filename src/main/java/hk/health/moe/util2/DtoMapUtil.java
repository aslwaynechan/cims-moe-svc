/*********************************************************************
* Copyright (c) eHR HK Limited 2013. 
* All Rights Reserved.
* 
*********************************************************************
* PROJECT NAME    :  eHR Adaptation
*
* MODULE NAME     :  MOE Drug Maintenance
*
* PROGRAM NAME    :  DtoMapUtil.java
*
* PURPOSE         :  Defines a set of methods that perform common, often re-used functions
*
**********************************************************************
* VERSION         :   1.01.001
* REF NO          :  
* DATE CREATED    :   7 June 2013
* 
*********************************************************************
*
* CHANGES         :
* REF NO           DATE        DETAIL
* ------           ----        ------
*
*********************************************************************
*/
package hk.health.moe.util2;


import hk.health.moe.common.ServerConstant;
import hk.health.moe.pojo.dto.MoeBaseUnitDto;
import hk.health.moe.pojo.dto.MoeCommonDosageDto;
import hk.health.moe.pojo.dto.MoeCommonDosageTypeDto;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.MoeDrugLocalDto;
import hk.health.moe.pojo.dto.MoeDrugOrdPropertyLocalDto;
import hk.health.moe.pojo.dto.MoeDrugStrengthDto;
import hk.health.moe.pojo.dto.MoeFormDto;
import hk.health.moe.pojo.dto.MoeRouteDto;
import hk.health.moe.pojo.po.MoeCommonDosagePo;
import hk.health.moe.pojo.po.MoeCommonDosageTypePo;
import hk.health.moe.pojo.po.MoeDrugLocalPo;
import hk.health.moe.pojo.po.MoeDrugOrdPropertyLocalPo;
import hk.health.moe.pojo.po.MoeDrugPo;
import hk.health.moe.pojo.po.MoeDrugStrengthLocalPo;
import hk.health.moe.pojo.po.MoeDrugStrengthPo;
import hk.health.moe.pojo.po.SaDrugLocalPo;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DtoMapUtil {

	public static MoeDrugDto convertMoeDrugDTO(MoeDrugPo moeDrug, String aliasNameType, boolean isDidYouMean) {
		MoeDrugDto dto = new MoeDrugDto();
		dto.setDidYouMean(isDidYouMean);
		dto.setHkRegNo(moeDrug.getHkRegNo());
		dto.setGenericIndicator(moeDrug.getGenericIndicator());
		dto.setParent(true);
		dto.setDisplayNameType(aliasNameType);
		dto.setTradeName(moeDrug.getTradeName());
		dto.setVtm(moeDrug.getVtm());
		dto.setManufacturer(moeDrug.getManufacturer());
		dto.setStrengthCompulsory(moeDrug.getStrengthCompulsory());
		if(!isDidYouMean) {
			// Form
			if(moeDrug.getMoeForm() != null) {
				MoeFormDto form = new MoeFormDto();
				form.setFormId(moeDrug.getMoeForm().getFormId());
				form.setFormEng(moeDrug.getFormEng());
				dto.setForm(form);
			}
			if(moeDrug.getDoseFormExtraInfo() != null) {
				dto.setDoseFormExtraInfoId(moeDrug.getDoseFormExtraInfoId());
			}
			dto.setDoseFormExtraInfo(moeDrug.getDoseFormExtraInfo());
			// Route
			if(moeDrug.getMoeRoute() != null) {
				MoeRouteDto route = new MoeRouteDto();
				route.setRouteId(moeDrug.getMoeRoute().getRouteId());
				route.setRouteEng(moeDrug.getRouteEng());
				dto.setRoute(route);
			}
			dto.setFormEng(moeDrug.getFormEng());
			dto.setRouteEng(moeDrug.getRouteEng());
			dto.setBaseUnit(moeDrug.getBaseUnit());
			dto.setPrescribeUnit(moeDrug.getPrescribeUnit());
			dto.setDispenseUnit(moeDrug.getDispenseUnit());
			dto.setLegalClassId(moeDrug.getLegalClassId());
			dto.setLegalClass(moeDrug.getLegalClass());
			if(moeDrug.getMoeBaseUnitByBaseUnitId() != null) {
				MoeBaseUnitDto base = new MoeBaseUnitDto();
				base.setBaseUnitId(moeDrug.getMoeBaseUnitByBaseUnitId().getBaseUnitId());
				base.setBaseUnit(moeDrug.getBaseUnit());
				dto.setBaseUnitId(base);
			}
			if(moeDrug.getMoeBaseUnitByPrescribeUnitId() != null) {
				MoeBaseUnitDto base = new MoeBaseUnitDto();
				base.setBaseUnitId(moeDrug.getMoeBaseUnitByPrescribeUnitId().getBaseUnitId());
				base.setBaseUnit(moeDrug.getPrescribeUnit());
				dto.setPrescribeUnitId(base);
			}
			if(moeDrug.getMoeBaseUnitByDispenseUnitId() != null) {
				MoeBaseUnitDto base = new MoeBaseUnitDto();
				base.setBaseUnitId(moeDrug.getMoeBaseUnitByDispenseUnitId().getBaseUnitId());
				base.setBaseUnit(moeDrug.getDispenseUnit());
				dto.setDispenseUnitId(base);
			}
			// ID
			dto.setVtmRouteFormId(moeDrug.getVtmRouteFormId());
		}
		return dto;
	}

	public static List<MoeDrugDto> convertMoeDrugDTOWithAllStrengths(MoeDrugPo moeDrug, MoeDrugStrengthPo strength) {

		List<MoeDrugDto> dtos = new ArrayList<MoeDrugDto>();
		MoeDrugDto drugDto = convertMoeDrugDTO(moeDrug, ServerConstant.ALIAS_NAME_TYPE_VTM, false);
		dtos.add(drugDto);

//		List<MoeDrugDTO> dtos = convertMoeDrugDTO(moeDrug, ServerConstant.ALIAS_NAME_TYPE_VTM, false); // convertMoeDrugDTO(moeDrug, false);
		for(MoeDrugDto dto : dtos) {
			// Strength
			dto.addStrength(convertMoeDrugStrengthDTO(strength));
			Formatter.setDrugDisplayString(dto);
		}

		return dtos;
	}

	public static MoeDrugStrengthDto convertMoeDrugStrengthDTO(MoeDrugStrengthPo str) {
		MoeDrugStrengthDto dto = new MoeDrugStrengthDto();
		dto.setStrength(str.getStrength());
		dto.setStrengthLevelExtraInfo(str.getStrengthLevelExtraInfo());
		dto.setAmp(str.getAmp());
		dto.setAmpId(str.getAmpId());
		dto.setVmp(str.getVmp());
		dto.setVmpId(str.getVmpId());
		return dto;
	}

	public static List<MoeDrugDto> convertMoeDrugDTOWithAllStrengths(MoeDrugLocalPo moeDrug, MoeDrugStrengthLocalPo strength) {
		List<MoeDrugDto> dtos = convertMoeDrugDTO(moeDrug, false);
		for(MoeDrugDto dto : dtos) {
			// Strength
			MoeDrugStrengthDto strDto = new MoeDrugStrengthDto();
			strDto.setStrength(strength.getStrength());
			strDto.setStrengthLevelExtraInfo(strength.getStrengthLevelExtraInfo());
			strDto.setAmp(strength.getAmp());
			strDto.setAmpId(strength.getAmpId());
			strDto.setVmp(strength.getVmp());
			strDto.setVmpId(strength.getVmpId());
			dto.addStrength(strDto);
			Formatter.setDrugDisplayString(dto);

			// Simon 20191030 add common dosage start--
            List<MoeCommonDosageDto> commonDosageList = new ArrayList<MoeCommonDosageDto>();;
            MoeCommonDosageDto commonDosageDto = null;

            for (MoeCommonDosagePo commonDosagePo : moeDrug.getMoeCommonDosages()){
                commonDosageDto = new MoeCommonDosageDto();
                commonDosageDto.setVersion(commonDosagePo.getVersion());
                commonDosageDto.setCommonDosageId(commonDosagePo.getCommonDosageId());
                commonDosageList.add(commonDosageDto);
            }
            dto.setCommonDosages(commonDosageList);
			//Simon 20191030 add common dosage end--

		}

		return dtos;
	}

	public static List<MoeDrugDto> convertMoeDrugDTO(MoeDrugLocalPo moeDrug, boolean isDidYouMean) {
		List<MoeDrugDto> dtos = new ArrayList<MoeDrugDto>();

		//VTM
		MoeDrugDto dto = convertMoeDrugDTO(moeDrug, ServerConstant.ALIAS_NAME_TYPE_VTM, isDidYouMean);
		if(moeDrug.getTerminologyName() == null) {
			dto.setDisplayNameType(ServerConstant.ALIAS_NAME_TYPE_LOCAL_DRUG);
		}
		dtos.add(dto);

		return dtos;
	}

	public static MoeDrugDto convertMoeDrugDTO(MoeDrugLocalPo moeDrug, String aliasNameType, boolean isDidYouMean) {
		MoeDrugDto dto = new MoeDrugDto();
		dto.setDidYouMean(isDidYouMean);
		dto.setHkRegNo(moeDrug.getHkRegNo());
		dto.setLocalDrugId(moeDrug.getLocalDrugId());
		dto.setGenericIndicator(moeDrug.getGenericIndicator());
		dto.setParent(true);
		dto.setDisplayNameType(aliasNameType);
		dto.setTradeName(moeDrug.getTradeName());
		dto.setVtm(moeDrug.getVtm());
		dto.setManufacturer(moeDrug.getManufacturer());
		dto.setStrengthCompulsory(moeDrug.getStrengthCompulsory());
		if(!isDidYouMean) {
			// Form
			if(moeDrug.getFormId() != null) {
				MoeFormDto form = new MoeFormDto();
				form.setFormId(moeDrug.getFormId());
				form.setFormEng(moeDrug.getFormEng());
				dto.setForm(form);
			}
			dto.setDoseFormExtraInfoId(moeDrug.getDoseFormExtraInfoId());
			dto.setDoseFormExtraInfo(moeDrug.getDoseFormExtraInfo());
			// Route
			if(moeDrug.getRouteId() != null) {
				MoeRouteDto route = new MoeRouteDto();
				route.setRouteId(moeDrug.getRouteId());
				route.setRouteEng(moeDrug.getRouteEng());
				dto.setRoute(route);
			}
			dto.setFormEng(moeDrug.getFormEng());
			dto.setRouteEng(moeDrug.getRouteEng());
			dto.setBaseUnit(moeDrug.getBaseUnit());
			dto.setPrescribeUnit(moeDrug.getPrescribeUnit());
			dto.setDispenseUnit(moeDrug.getDispenseUnit());
			dto.setLegalClassId(moeDrug.getLegalClassId());
			dto.setLegalClass(moeDrug.getLegalClass());
			if(moeDrug.getBaseUnitId() != null) {
				MoeBaseUnitDto unit = new MoeBaseUnitDto();
				unit.setBaseUnit(moeDrug.getBaseUnit());
				unit.setBaseUnitId(moeDrug.getBaseUnitId());
				dto.setBaseUnitId(unit);
			}
			if(moeDrug.getPrescribeUnitId() != null) {
				MoeBaseUnitDto unit = new MoeBaseUnitDto();
				unit.setBaseUnit(moeDrug.getPrescribeUnit());
				unit.setBaseUnitId(moeDrug.getPrescribeUnitId());
				dto.setPrescribeUnitId(unit);
			}
			if(moeDrug.getDispenseUnitId() != null) {
				MoeBaseUnitDto unit = new MoeBaseUnitDto();
				unit.setBaseUnit(moeDrug.getDispenseUnit());
				unit.setBaseUnitId(moeDrug.getDispenseUnitId());
				dto.setDispenseUnitId(unit);
			}
		}

		if (moeDrug.getMoeDrugOrdPropertyLocal()!=null) {
			dto.setMoeDrugOrdPropertyLocal(convertMoeDrugOrdPropertyLocalDTO(moeDrug.getMoeDrugOrdPropertyLocal()));
		}

		// Simon 20191028 start--
		dto.setVersion(moeDrug.getVersion());
		//Simon 20191028 end--
		return dto;
	}

	public static MoeDrugOrdPropertyLocalDto convertMoeDrugOrdPropertyLocalDTO(MoeDrugOrdPropertyLocalPo local) {
		MoeDrugOrdPropertyLocalDto dto = new MoeDrugOrdPropertyLocalDto();
		dto.setCreateDtm(local.getCreateDtm());
		dto.setCreateHosp(local.getCreateHosp());
		dto.setCreateRank(local.getCreateRank());
		dto.setCreateRankDesc(local.getCreateRankDesc());
		dto.setCreateUser(local.getCreateUser());
		dto.setCreateUserId(local.getCreateUserId());
		dto.setLocalDrugId(local.getLocalDrugId());
		dto.setMaximumDuration(local.getMaximumDuration());
		dto.setMinimumDosage(local.getMinimumDosage());
		dto.setOutOfStock(local.getOutOfStock());
		dto.setPrice(local.getPrice());
		dto.setSuspend(local.getSuspend());
		dto.setUpdateDtm(local.getUpdateDtm());
		dto.setUpdateHosp(local.getUpdateHosp());
		dto.setUpdateRank(local.getUpdateRank());
		dto.setUpdateRankDesc(local.getUpdateHosp());
		dto.setUpdateUser(local.getUpdateUser());
		dto.setUpdateUserId(local.getUpdateUserId());
		// Simon 20191028 start--
		dto.setVersion(local.getVersion());
		//Simon 20191028 end--
		return dto;
	}

	//Simon 20190902 add for not exist in moe_drug_server start--
	public static List<MoeDrugDto> convertMoeDrugDTOWithAllStrengths(MoeDrugPo moeDrug, Set<MoeDrugStrengthPo> strength) {

		List<MoeDrugDto> dtos = new ArrayList<MoeDrugDto>();
		MoeDrugDto drugDto = convertMoeDrugDTO(moeDrug, ServerConstant.ALIAS_NAME_TYPE_VTM, false);
		dtos.add(drugDto);

//		List<MoeDrugDTO> dtos = convertMoeDrugDTO(moeDrug, ServerConstant.ALIAS_NAME_TYPE_VTM, false); // convertMoeDrugDTO(moeDrug, false);
		for(MoeDrugDto dto : dtos) {
			// Strength
			for (MoeDrugStrengthPo po :strength)
				dto.addStrength(convertMoeDrugStrengthDTO(po));
			hk.health.moe.util.Formatter.setDrugDisplayString(dto);
		}

		return dtos;
	}
	//Simon 20190902 end--
	public static SaDrugLocalPo convertSaDrugLocal(MoeDrugLocalDto dto){
		SaDrugLocalPo saDrugLocal = new SaDrugLocalPo();
		saDrugLocal.setAllergenId(dto.getLocalDrugId());
		saDrugLocal.setHkRegNo(dto.getHkRegNo());
		saDrugLocal.setVtm(dto.getVtm());
		saDrugLocal.setTradeName(dto.getTradeName());
		saDrugLocal.setFormerBan(null);
		saDrugLocal.setAbbreviation(null);
		saDrugLocal.setOtherName(null);
		saDrugLocal.setCreateDtm(dto.getCreateDtm());
		saDrugLocal.setUpdateDtm(dto.getUpdateDtm());
		saDrugLocal.setGenericProductInd(dto.getGenericIndicator());
		return saDrugLocal;
	}
	// Ricci 20190822 add for not exist moe_drug_server start --
	public static MoeCommonDosageTypeDto convertMoeCommonDosageTypeDto(MoeCommonDosageTypePo po){
		MoeCommonDosageTypeDto dto = new MoeCommonDosageTypeDto();
		dto.setCommonDosageType(po.getCommonDosageType());
		dto.setCommonDosageTypeDesc(po.getCommonDosageTypeDesc());
		dto.setRank(po.getRank());

		return dto;
	}
	// Ricci 20190822 add for not exist moe_drug_server end --
}
