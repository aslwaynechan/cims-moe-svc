/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  Formatter.java
 *
 * PURPOSE         :  Defines a set of methods that perform common, often re-used functions
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.util2;


import hk.health.moe.common.ServerConstant;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.MoeDrugStrengthDto;
import hk.health.moe.pojo.po.MoeDrugLocalPo;
import hk.health.moe.pojo.po.MoeDrugOrdPropertyLocalPo;
import hk.health.moe.pojo.po.MoeDrugStrengthLocalPo;
import hk.health.moe.pojo.po.MoeDrugToSaamLocalPo;
import hk.health.moe.util.DtoUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.util.HtmlUtils;

import java.util.HashMap;
import java.util.List;

public class Formatter {

    public static String trim(String s) {
        if (s == null) return "";
        return s.trim();
    }

    public static void setDrugDisplayString(MoeDrugDto dto) {
        String vtmDisplay = "";

        if (!dto.isFreeText()) {
            vtmDisplay = getVtmString(dto.getVtm(), dto.getIngredientList(), dto.getStrengths(), (dto.getStrengthCompulsory() != null && ServerConstant.DB_FLAG_TRUE.equalsIgnoreCase(dto.getStrengthCompulsory())), dto.getScreenDisplay());
        }
        StringBuffer sb = new StringBuffer();
        if (dto.isFreeText()) {
            //sb.append(" " + SafeHtmlUtils.htmlEscape(dto.getReplacementString()) + " (Free Text)");
            sb.append(" " + HtmlUtils.htmlEscape(dto.getReplacementString()) + " (Free Text)");
        } else if (dto.isParent()) {
            String nameType = dto.getDisplayNameType();
            if (dto.isSwapDisplayFormat() && !dto.isDidYouMean()) {
                if (trim(dto.getTradeName()).length() > 0) {
                    sb.append(dto.getTradeName() + " (" + vtmDisplay + ")");
                } else {
                    sb.append(vtmDisplay + " (" + dto.getManufacturer() + ")");

                    if (dto.getDisplayNameType().equalsIgnoreCase(ServerConstant.ALIAS_NAME_TYPE_ABB) ||
                            dto.getDisplayNameType().equalsIgnoreCase(ServerConstant.ALIAS_NAME_TYPE_OTHER)) {
                        sb.append(" (" + dto.getAliasNames().get(0).getAliasName() + ")");
                    }
                }
            } else if (Formatter.isMultipleIngredient(dto.getVtm()) || dto.getDisplayNameType().equalsIgnoreCase(ServerConstant.ALIAS_NAME_TYPE_LOCAL_DRUG)) {
                sb.append(dto.getStrengths().get(0).getAmp());
            } else if (ServerConstant.ALIAS_NAME_TYPE_VTM.equalsIgnoreCase(nameType) &&
                    dto.getGenericIndicator().equalsIgnoreCase(ServerConstant.DRUG_GENERIC_IND_YES)) { // trade name doesn't exist
                sb.append(dto.getTradeName());
            } else {
                if (trim(dto.getTradeName()).length() > 0) {
                    sb.append(dto.getTradeName() + " ");
                } else {
                    sb.append(vtmDisplay + " (" + dto.getManufacturer() + ") ");
                }
                if (ServerConstant.ALIAS_NAME_TYPE_VTM.equalsIgnoreCase(nameType)) {
                    sb.append("(" + vtmDisplay + ")");
                } else {
                    sb.append(" (" + dto.getAliasNames().get(0).getAliasName() + ")");
                }
            }

            if (!Formatter.isMultipleIngredient(dto.getVtm()) && !ServerConstant.ALIAS_NAME_TYPE_LOCAL_DRUG.equalsIgnoreCase(dto.getDisplayNameType())) {
                // Strength
                if (dto.getStrengths() != null &&
                        dto.getStrengths().get(0).getStrength() != null &&
                        dto.getStrengths().size() == 1) {
                    sb.append(" " + dto.getStrengths().get(0).getStrength());
                }

                //route
                if (dto.getForm() != null && dto.getRoute() != null) {
                    if (showRoute(dto.getForm().getFormEng(), dto.getRoute().getRouteEng())) {
                        sb.append(" " + dto.getRoute().getRouteEng());
                    }
                }

                // Form
                if (dto.getForm() != null) {
                    sb.append(" " + dto.getForm().getFormEng());
                }

                if (dto.getDoseFormExtraInfo() != null) {
                    sb.append(" (" + dto.getDoseFormExtraInfo() + ")");
                }

                if (dto.getStrengths() != null && dto.getStrengths().size() == 1
                        && dto.getStrengths().get(0).getStrengthLevelExtraInfo() != null) {
                    sb.append(" (" + dto.getStrengths().get(0).getStrengthLevelExtraInfo() + ") ");
                }
            }

        }
        dto.setDisplayString(sb.toString());
    }

    private static String getVtmString(String vtm, List<String> ingredients, List<MoeDrugStrengthDto> strengthList, Boolean isStrengthCompulsoryInput, String screenDisplay) {

        boolean isStrengthCompulsory = isStrengthCompulsoryInput == null ? true : isStrengthCompulsoryInput;
        if (vtm == null || isMultipleIngredient(vtm) || !isStrengthCompulsory) return vtm;
        if (isMultipleIngredient(vtm) && strengthList.size() > 1) {
            return screenDisplay;
        }
        if (ingredients == null || ingredients.isEmpty() ||
                strengthList == null || strengthList.isEmpty()) return vtm;
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < ingredients.size(); i++) {
            sb.append(i == 0 ? "(" : "");
            sb.append(ingredients.get(i));
            if (i < strengthList.size()) sb.append(" " + strengthList.get(i).getStrength());
            if (i < ingredients.size() - 1) sb.append(" + ");
            sb.append(i == ingredients.size() - 1 ? ")" : "");
        }

        return sb.toString();
    }

    public static boolean isMultipleIngredient(String vtm) {
        return MoeDrugDto.isMultiIngr(vtm);
    }

    public static boolean showRoute(String form, String route) {
        HashMap<String, String> map = DtoUtil.FORM_ROUTE_MAP;
        return map.get(form + " " + route) == null || "y".equalsIgnoreCase(map.get(form + " " + route));
    }


    public static void setTradeNameAliasString(MoeDrugDto dto, String tradeNameAlias) {
        dto.setDisplayString("[" + tradeNameAlias + "] " + dto.getDisplayString());
    }

    public static long nullToZero(Long integer) {
        if (integer == null) return 0;
        return integer.longValue();
    }

    public static MoeDrugLocalPo emptyToNull(MoeDrugLocalPo local) {
        local.setCreateHosp(StringUtils.trimToNull(local.getCreateHosp()));
        local.setUpdateHosp(StringUtils.trimToNull(local.getUpdateHosp()));
        return local;
    }

    public static MoeDrugToSaamLocalPo emptyToNull(MoeDrugToSaamLocalPo local) {
        local.setCreateHosp(StringUtils.trimToNull(local.getCreateHosp()));
        local.setUpdateHosp(StringUtils.trimToNull(local.getUpdateHosp()));
        return local;
    }

    public static MoeDrugStrengthLocalPo emptyToNull(MoeDrugStrengthLocalPo local) {
        local.setCreateHosp(StringUtils.trimToNull(local.getCreateHosp()));
        local.setUpdateHosp(StringUtils.trimToNull(local.getUpdateHosp()));
        return local;
    }

    public static MoeDrugOrdPropertyLocalPo emptyToNull(MoeDrugOrdPropertyLocalPo local) {
        local.setCreateHosp(StringUtils.trimToNull(local.getCreateHosp()));
        local.setUpdateHosp(StringUtils.trimToNull(local.getUpdateHosp()));
        return local;
    }
}
