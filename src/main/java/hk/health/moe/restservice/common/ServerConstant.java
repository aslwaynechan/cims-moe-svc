package hk.health.moe.restservice.common;

import io.jsonwebtoken.SignatureAlgorithm;

public class ServerConstant {

	public static final SignatureAlgorithm SIGNATURE_ALORITHM = SignatureAlgorithm.HS256;
	
	public static final String DB_FLAG_ACTIVE = "A";
	public static final String DB_FLAG_YES = "Y";
	
	public static final String REASON_TYPE_G = "G";
	public static final String REASON_TYPE_A = "A";
	public static final String REASON_TYPE_D = "D";
	
	public static final String LANG_ZH_HK = "zh_HK";
	public static final String LANG_EN = "en";
	
	//public static final String MY_LANG = LANG_ZH_HK;
	//public static final String MY_LANG = LANG_EN;
	
	public static final String ALERT_CODE_HLAB1502_POSITIVE = "A0017";
	public static final String ALERT_CODE_HLAB1502_NEGATIVE = "A0043";
	
	public static final String LOG_SETTING_ENALBED		="Y";
	public static final String LOG_SETTING_DISABLED		="N";
	public static final String LOG_PARAM = "enable_audit_log";
	public static final String LOG_PARAM_ENCRYPT = "enable_audit_log_encrypt";
	public static final String LOG_SETTING_ENALBED_DEFAULT = "Y";
	public static final String ALLERGY_MANIFESTATION_OTHERS = "Others";
	public static final String OTHER_ALLERGIC_REACTION = "Other allergic reaction";
	public static final String MANIFESTATION_UNCERTAIN = "Manifestation uncertain";
	public static final String REACTION_UNCERTAIN = "Reaction uncertain";
	public static final String ADR_REACTION_OTHERS = "Others";
	public static final String ENABLE_OLD_CLOSING_ROUTE = "close_saam_old_route";
	
	public static final char TRX_TYPE_INSERT = 'I';
	public static final char TRX_TYPE_UPDATE = 'U';
	public static final char TRX_TYPE_DELETE = 'D';
	
	public static final float SPELL_CHECKER_DRUG_ACCURACY = (float) 0.7;
	public static final float SPELL_CHECKER_ALERT_ACCURACY = (float) 0.7;
	public static final float SPELL_CHECKER_ALLERGY_ACCURACY = (float) 0.7;
	
	public static final String DISPLAY_NAME_TYPE_VTM = "V";
	public static final String DISPLAY_NAME_TYPE_TRADENAME = "T";
	public static final String DISPLAY_NAME_TYPE_OTHER = "O";
	public static final String DISPLAY_NAME_TYPE_BAN = "B";
	public static final String DISPLAY_NAME_TYPE_ABB = "A";
	public static final String DISPLAY_NAME_TYPE_ALLERGENGROUP = "G";
	
	public static final String ALLERGEN_TYPE_DRUG_ALLERGY = "D";
	public static final String ALLERGEN_TYPE_DATA_CONVERSION = "C";
	public static final String ALLERGEN_TYPE_ALLERGEN_GROUP = "G";
	public static final String ALLERGEN_TYPE_FREE_TEXT = "F";
	public static final String ALLERGEN_TYPE_FREE_TEXT_DRUG = "O";
	public static final String ALLERGEN_TYPE_FREE_TEXT_NON_DRUG = "X";
	public static final String ALLERGEN_TYPE_NO_KNOWN_DRUG_ALLERGY = "N";
	public static final String ALLERGEN_TYPE_LOCAL_DRUG = "L";


	
	public static final String IMPORT_FREE_TEXT = "C";
	public static final String IMPORT_STRUCTURED_DATA = "S";
	
	public static final String DRUG_LABEL ="(Drug)";
	public static final String NON_DRUG_LABEL ="(Non-Drug)";
	
	public static final String CM_DRUG_LABEL ="(西藥)";
	public static final String CM_NON_DRUG_LABEL ="(其他)";
	
	public static final String NO_KNOWN_DRUG_ALLERGY = "No Known Drug Allergy";
	public static final String NKDA = "NKDA";
	public static final String CM_NO_KNOWN_DRUG_ALLERGY = "沒有已知過敏紀錄";
	public static final String NO_KNOWN_DRUG_ALLERGY_REASON = "Drug allergy is added";
	public static final String CM_NO_KNOWN_DRUG_ALLERGY_REASON = "已新增過敏紀錄";
	public static final String SEPARATOR = ";";
	public static final String FREE_TEXT_IND = "#";
	public static final String CM_ADR_NATURE_CMM = "CMM";
	public static final String CM_ADR_NATURE_CM_P = "CM-P";
	public static final String CM_ADR_NATURE_CMM_DESC = "原藥材";
	public static final String CM_ADR_NATURE_CM_P_DESC = "炮製品";
	
	public static final String SOURCE_SYSTEM_EHR = "eHR";
	
	public static final String DELETE_REASON_ALLERGY = "G";
	public static final String DELETE_REASON_ADR = "D";
	
	public static final int NUM_OF_DID_YOU_MEAN = 9;
	
	public static final String HIBERNATE_DIALET_DB_ORACLE = "Oracle";
	public static final String HIBERNATE_DIALET_DB_MYSQL = "MySQL";

	public static final String ALERT_CLASSIFIED_TAG = "[Classified]";
	
	public static final String TRIAL_EXPIRY_DATE = "trial_expiry_date";
	public static final String PRODUCTION = "PRODUCTION";
	//public final static byte[] AES_KEY =  new byte[] {'H', 'o', 'S', 'P', 'i', 'T', 'a', 'L', 'A', 'u', 'T', 'h', 'O', 'r', 'I', 't'};
	
	//public static final String JASYPT_ALOGORITHM = "PBEWithMD5AndDES";
	public static final String JASYPT_ALOGORITHM = "uDAwfCK4OVl1NCqmov-bYpk-7zkZ1-t7ykX3r_sBycw"; 
	//public static final String JASYPT_PASSWORD = "HaITsCe3";
	public static final String JASYPT_KEY = "tO1ear62JFUnA8R2ws8BOw";
	//public static final String JASYPT_SALT = "s@1tV@!ue";
	public static final String JASYPT_SALT = "D85cEWnP9u8WV-uv9ho8Dg";
		
	public static enum APP {
		CMS("CMS"),
		CMIS("CMIS");
		
		private String code;  
		private APP(String code) {  
			this.code = code;  
		}  
		public String getCode() {  
			return code;  
		}
		public static boolean isValid(String code) {
			return (CMS.getCode().equals(code)
					|| CMIS.getCode().equals(code)
					);
		}
	}
}

