package hk.health.moe.restservice.bean.saam;

import org.dozer.Mapping;

import java.io.Serializable;
import java.util.Date;

public class MoePatientAdrDto implements Serializable {
	
	private static final long serialVersionUID = -3213820147398222356L;
	private String adrSeqNo;
	private String drugDesc;
	private Boolean isFreeText;
	private String adrType;
	@Mapping("allergenDto")
	private MoeAllergenDto allergen;
	private String updateBy;
	private Date updateDtm;
	private String updateUser;
	private int version;

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDtm() {
		return updateDtm;
	}

	public void setUpdateDtm(Date updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public MoeAllergenDto getAllergen() {
		return allergen;
	}

	public void setAllergen(MoeAllergenDto allergen) {
		this.allergen = allergen;
	}

	public String getAdrSeqNo() {
		return adrSeqNo;
	}

	public void setAdrSeqNo(String adrSeqNo) {
		this.adrSeqNo = adrSeqNo;
	}

	public String getDrugDesc() {
		return drugDesc;
	}

	public void setDrugDesc(String drugDesc) {
		this.drugDesc = drugDesc;
	}

	public Boolean getFreeText() {
		return isFreeText;
	}

	public void setFreeText(Boolean freeText) {
		isFreeText = freeText;
	}

	public String getAdrType() {
		return adrType;
	}

	public void setAdrType(String adrType) {
		this.adrType = adrType;
	}
}
