package hk.health.moe.restservice.bean.saam;

import org.dozer.Mapping;

import java.io.Serializable;
import java.util.Date;

public class MoePatientAlertDto implements Serializable {

    private static final long serialVersionUID = 5553697343538770822L;

    private String alertSeqNo;
    private String alertCode;
    private String alertDesc;
    private String alertType;
    @Mapping(value = "updateUserId")
    private String updateBy;
    private Date updateDtm;
    private String updateUser;
    private String classifiedIndicator;
    private int version;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDtm() {
        return updateDtm;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getClassifiedIndicator() {
        return classifiedIndicator;
    }

    public void setClassifiedIndicator(String classifiedIndicator) {
        this.classifiedIndicator = classifiedIndicator;
    }

    public String getAlertSeqNo() {
        return alertSeqNo;
    }

    public void setAlertSeqNo(String alertSeqNo) {
        this.alertSeqNo = alertSeqNo;
    }

    public String getAlertCode() {
        return alertCode;
    }

    public void setAlertCode(String alertCode) {
        this.alertCode = alertCode;
    }

    public String getAlertDesc() {
        return alertDesc;
    }

    public void setAlertDesc(String alertDesc) {
        this.alertDesc = alertDesc;
    }

    public String getAlertType() {
        return alertType;
    }

    public void setAlertType(String alertType) {
        this.alertType = alertType;
    }
}
