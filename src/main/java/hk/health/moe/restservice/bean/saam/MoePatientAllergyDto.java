package hk.health.moe.restservice.bean.saam;

import org.dozer.Mapping;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class MoePatientAllergyDto implements Serializable {

    private static final long serialVersionUID = -3213820147398222356L;

    private String allergySeqNo;
    private String displayName;
    private String allergenType;
    @Mapping(value = "allergenDto")
    private MoeAllergenDto allergen;
    private String certainty;
    private String updateBy;
    private Date updateDtm;
    private String updateUser;
    @Mapping(value = "allergyReactionDtoList")
    private List<MoeAllergyReactionDto> allergyReactions;
    private int version;
    private String additionInfo;

    public String getAdditionInfo() {
        return additionInfo;
    }

    public void setAdditionInfo(String additionInfo) {
        this.additionInfo = additionInfo;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getAllergySeqNo() {
        return allergySeqNo;
    }

    public void setAllergySeqNo(String allergySeqNo) {
        this.allergySeqNo = allergySeqNo;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getAllergenType() {
        return allergenType;
    }

    public void setAllergenType(String allergenType) {
        this.allergenType = allergenType;
    }

    public MoeAllergenDto getAllergen() {
        return allergen;
    }

    public void setAllergen(MoeAllergenDto allergen) {
        this.allergen = allergen;
    }

    public String getCertainty() {
        return certainty;
    }

    public void setCertainty(String certainty) {
        this.certainty = certainty;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDtm() {
        return updateDtm;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public List<MoeAllergyReactionDto> getAllergyReactions() {
        return allergyReactions;
    }

    public void setAllergyReactions(List<MoeAllergyReactionDto> allergyReactions) {
        this.allergyReactions = allergyReactions;
    }
}
