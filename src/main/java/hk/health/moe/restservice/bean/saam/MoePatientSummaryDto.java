package hk.health.moe.restservice.bean.saam;

import org.dozer.Mapping;

import java.io.Serializable;
import java.util.List;

public class MoePatientSummaryDto implements Serializable {

    private static final long serialVersionUID = 5553697343538770822L;

    @Mapping(value = "patientAllergyList")
    private List<MoePatientAllergyDto> patientAllergies;
    @Mapping(value = "patientAdrList")
    private List<MoePatientAdrDto> patientAdrs;
    @Mapping(value = "patientAlertList")
    private List<MoePatientAlertDto> patientAlerts;

    public List<MoePatientAllergyDto> getPatientAllergies() {
        return patientAllergies;
    }

    public void setPatientAllergies(List<MoePatientAllergyDto> patientAllergies) {
        this.patientAllergies = patientAllergies;
    }

    public List<MoePatientAdrDto> getPatientAdrs() {
        return patientAdrs;
    }

    public void setPatientAdrs(List<MoePatientAdrDto> patientAdrs) {
        this.patientAdrs = patientAdrs;
    }

    public List<MoePatientAlertDto> getPatientAlerts() {
        return patientAlerts;
    }

    public void setPatientAlerts(List<MoePatientAlertDto> patientAlerts) {
        this.patientAlerts = patientAlerts;
    }
}
