package hk.health.moe.restservice.bean.saam;

import java.io.Serializable;

public class MoeAllergyReactionDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String reactionSeqNo;
	private String displayName;

	public String getReactionSeqNo() {
		return reactionSeqNo;
	}

	public void setReactionSeqNo(String reactionSeqNo) {
		this.reactionSeqNo = reactionSeqNo;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
