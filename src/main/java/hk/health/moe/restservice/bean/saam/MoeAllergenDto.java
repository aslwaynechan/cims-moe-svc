package hk.health.moe.restservice.bean.saam;

import java.io.Serializable;

public class MoeAllergenDto implements Serializable {

    private static final long serialVersionUID = -7316689933759413917L;
    private String allergenId;
    private String vtm;
    private String tradeName;
    private String ban;
    private String abbreviation;
    private String otherName;
    private String displayNameType;
    private String displayName;
    private Boolean multiIngredient;
    private String additionInfo;

    public String getAdditionInfo() {
        return additionInfo;
    }

    public void setAdditionInfo(String additionInfo) {
        this.additionInfo = additionInfo;
    }

    public String getAllergenId() {
        return allergenId;
    }

    public void setAllergenId(String allergenId) {
        this.allergenId = allergenId;
    }

    public String getVtm() {
        return vtm;
    }

    public void setVtm(String vtm) {
        this.vtm = vtm;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getBan() {
        return ban;
    }

    public void setBan(String ban) {
        this.ban = ban;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getOtherName() {
        return otherName;
    }

    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }

    public String getDisplayNameType() {
        return displayNameType;
    }

    public void setDisplayNameType(String displayNameType) {
        this.displayNameType = displayNameType;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Boolean getMultiIngredient() {
        return multiIngredient;
    }

    public void setMultiIngredient(Boolean multiIngredient) {
        this.multiIngredient = multiIngredient;
    }

}
