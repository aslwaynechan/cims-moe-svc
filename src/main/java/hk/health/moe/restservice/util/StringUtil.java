/*********************************************************************
* Copyright (c) eHR HK Limited 2013. 
* All Rights Reserved.
* 
*********************************************************************
* PROJECT NAME    :  eHR Adaptation
*
* MODULE NAME     :  Structured Alert Adaptation Module (SAAM)
*
* PROGRAM NAME    :  ehr-apps-sa-model.war
*
* PURPOSE         :  An utility class defines a set of methods that perform common, often re-used functions
*
**********************************************************************
* VERSION         :   1.01.001
* REF NO          :  
* DATE CREATED    :   24 June 2013
* 
*********************************************************************
*
* CHANGES         :
* REF NO           DATE        DETAIL
* ------           ----        ------
*
*********************************************************************
*/
package hk.health.moe.restservice.util;

import hk.health.moe.restservice.common.ServerConstant;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class StringUtil {
	
	public static final String DRUG_OPEN_BRACKET = " (";
	public static final String DRUG_CLOSE_BRACKET = ")";

	public static final String LINE_SEPARATOR = "\\\\n";
	
	public static String convertStringsToLikeString(String input) {
		
		StringBuilder result = new StringBuilder();
		result.append('%');
		if (input == null) return null;
		String[] words =  StringUtils.split(input);
		for (String s : words) {			
			result.append(s);
			result.append('%');
			
		}
		return result.toString();
	}
	
	public static boolean containsAllPattern(String input, String target){
		String[] words = StringUtils.split(input);
		
		for(int i=0; i<words.length; i++) {
			String regEx = buildRegularExpressionForLikePattern(words[i]);
			if (!Pattern.matches(regEx, target)) {
				return false;
			}
		}
		return true;
	}
	
	public static String buildRegularExpressionForLikePattern(String input){
		StringBuilder sb = new StringBuilder();
		String[] words = StringUtils.split(input);
		
		sb.append(".*");
	    for (int i=0; i<words.length; i++){
	      sb.append(escapeRegularExpressionLiteral(words[i]));
	      sb.append(".*");
	    }
	    //sb.append("\\z");
	    return sb.toString();
	}
	
	public static String escapeRegularExpressionLiteral(String s){
	    // According to the documentation in the Pattern class:
	    //
	    // The backslash character ('\') serves to introduce escaped constructs,
	    // as defined in the table above, as well as to quote characters that
	    // otherwise would be interpreted as unescaped constructs. Thus the
	    // expression \\ matches a single backslash and \{ matches a left brace.
	    //
	    // It is an error to use a backslash prior to any alphabetic character
	    // that does not denote an escaped construct; these are reserved for future
	    // extensions to the regular-expression language. A backslash may be used
	    // prior to a non-alphabetic character regardless of whether that character
	    // is part of an unescaped construct.
	    //
	    // As a result, escape everything except [0-9a-zA-Z]

	    int length = s.length();
	    int newLength = length;
	    // first check for characters that might
	    // be dangerous and calculate a length
	    // of the string that has escapes.
	    for (int i=0; i<length; i++){
	      char c = s.charAt(i);
	      if (!((c>='0' && c<='9') || (c>='A' && c<='Z') || (c>='a' && c<='z'))){
	        newLength += 1;
	      }
	    }
	    if (length == newLength){
	      // nothing to escape in the string
	      return s;
	    }
	    StringBuilder sb = new StringBuilder(newLength);
	    for (int i=0; i<length; i++){
	      char c = s.charAt(i);
	      if (!((c>='0' && c<='9') || (c>='A' && c<='Z') || (c>='a' && c<='z'))){
	        sb.append('\\');
	      }
	      sb.append(c);
	    }
	    return sb.toString();
	  }
	
	public static String capitalize(String str) {
		return WordUtils.capitalize(StringUtils.lowerCase(str));
	}
	
	public static String upperInBetween(String str, String open, String close) {
		int openIndex = StringUtils.indexOf(str, open);
		int closeIndex = StringUtils.lastIndexOf(str, close);
		if (openIndex > -1 && closeIndex > -1 && closeIndex > openIndex) {
			return StringUtils.substring(str, 0, openIndex)
				+ StringUtils.upperCase(StringUtils.mid(str, openIndex, closeIndex-openIndex))
				+ StringUtils.substring(str, closeIndex);
		}
		return str;
	}
	
	public static String strip(String str) {
		return StringUtils.strip(str);
	}
	
	public static String formatDrug(String genericName, String tradeName) {
		if (genericName == null || tradeName == null) return genericName;
		return StringUtils.join(new String[]{genericName, DRUG_OPEN_BRACKET, tradeName, DRUG_CLOSE_BRACKET});
	}
	
	public static String getTradeNameByDisplayName(String displayName) {
		int openIndex = StringUtils.indexOf(displayName, DRUG_OPEN_BRACKET);
		int closeIndex = StringUtils.lastIndexOf(displayName, DRUG_CLOSE_BRACKET);
		if (openIndex > -1 && closeIndex > -1 && closeIndex > openIndex) {
			return StringUtils.substring(displayName, 0, openIndex);
		}
		return null;
	}
	
	public static String getGenericNameByDisplayName(String displayName) {
		int openIndex = StringUtils.indexOf(displayName, DRUG_OPEN_BRACKET);
		int closeIndex = StringUtils.lastIndexOf(displayName, DRUG_CLOSE_BRACKET);
		if (openIndex > -1 && closeIndex > -1 && closeIndex > openIndex) {
			return StringUtils.mid(displayName, openIndex+DRUG_OPEN_BRACKET.length(), closeIndex-(openIndex+DRUG_OPEN_BRACKET.length()));
		}
		return null;
	}
	
	public static String formatStringWithComma(List<String> stringList){

		StringBuilder record = new StringBuilder();
		Collections.sort(stringList, String.CASE_INSENSITIVE_ORDER);			
		for (int j=0; j<stringList.size(); j++) {
			
			record.append(stringList.get(j));
			if (j != stringList.size() - 1)
				record.append(", ");
			
		} 
		return record.toString();
		
	}
	
	public static String trim(String input){
		return input==null?"":input.trim();
	}
	
	public static String emptyToNull(String input){
		if (input==null || "".equals(input.trim())){
			 return null;
		}
		else{
			return input;		
		}
	}	
	
	public static String nullToEmpty(String input){
		if (input==null || "".equals(input.trim())){
			 return "";
		}
		else{
			return input;		
		}
	}
	
	public static boolean isEmptyString(String str)
	{
		return (str == null) || (str.isEmpty());  
	}
	
	//DOWNLOAD
	public static String[] convertArrayWithComma(String commaString) {
		String[] result = null;
		
		if(!StringUtils.isEmpty(commaString)) {
			result = commaString.split(", ");
		}
		
		return result;
	}
	
	public static String[] convertArrayWithSemiColon(String semiColonString) {
		String[] result = null;
		
		if(!StringUtils.isEmpty(semiColonString)) {
			result = semiColonString.split(ServerConstant.SEPARATOR);
		}
		
		return result;
	}
	
	public static String charToString(Character input) {
		if(input == null) {
			return null;
		} else {
			return input.toString();
		}
	}
	
	public static String trimString(String input) {
		if(input == null) {
			return null;
		} else {
			return input.trim();
		}		
	}
	
	@SuppressWarnings("rawtypes")
	public static String convertMapToString(HashMap<String,String> data) {
		StringBuilder buf = new StringBuilder();
		
		if(data != null) {
			Iterator it = data.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry) it.next();
				buf.append(pairs.getKey()).append('=').append(pairs.getValue());
				if(it.hasNext()) {
					buf.append('&');
				}
			}			
		}
		
		return buf.toString();
	}	

}
