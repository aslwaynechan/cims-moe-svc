package hk.health.moe.restservice.util;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.util.MimeType;
import org.springframework.util.MimeTypeUtils;

import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * @Author Simon
 * @Date 2019/10/25
 */
public class HttpEntityUtil {

    public static HttpHeaders genJsonHeader() {
        // 请求头
        HttpHeaders headers = new HttpHeaders();
        MimeType mimeType = MimeTypeUtils.APPLICATION_JSON;
        MediaType mediaType = new MediaType(mimeType.getType(), mimeType.getSubtype(), StandardCharsets.UTF_8);
        // 请求体
        headers.setContentType(mediaType);
        return headers;
    }

    public static HttpHeaders genMultiParamsHeader(Map<String,Object> map) {
        // 请求头
        HttpHeaders headers = new HttpHeaders();
        MimeType mimeType = MimeTypeUtils.APPLICATION_JSON;
        MediaType mediaType = new MediaType(mimeType.getType(), mimeType.getSubtype(), StandardCharsets.UTF_8);
        // 请求体
        headers.setContentType(mediaType);
        for (Map.Entry<String,Object> entry : map.entrySet()) {
            headers.add(entry.getKey(), (String) entry.getValue());
        }
        return headers;
    }

    public static HttpEntity genHttpEntity(Object body, @Nullable HttpHeaders headers) throws Exception {
        // default header is json
        if (headers == null) {
            headers = genJsonHeader();
        }
        return new HttpEntity(body, headers);
    }

}
