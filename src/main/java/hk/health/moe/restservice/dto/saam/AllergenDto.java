/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Structured Alert Adaptation Module (SAAM)
 *
 * PROGRAM NAME    :  ehr-apps-sa-model.war
 *
 * PURPOSE         :  A data transfer object (DTO) used to transfer data in the application
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   24 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.restservice.dto.saam;

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Objects;

/**
 * AllergenDTO is a data transfer object which contains information of allergen.
 *
 * @author HAITS CE3
 */
public class AllergenDto implements Serializable {

    private static final long serialVersionUID = -7316689933759413917L;
    private String allergenId = null;
    private String vtm = null;
    private String tradeName = null;
    private String ban = null;
    private String allergenType = null;
    private String abbreviation = null;
    private String otherName = null;
    private String displayNameType = null;
    private String displayName = null;
    private String shortDisplayName = null;
    private String searchDisplayName = null;
    private String displayNameTypeDesc = null;
    private String genericProductInd = null;

    /**
     * Gets the vtm of this record.
     *
     * @return the vtm
     */
    public String getVtm() {
        return vtm;
    }

    /**
     * Sets the vtm of this record.
     *
     * @param vtm the vtm
     */
    public void setVtm(String vtm) {
        this.vtm = vtm;
    }

    /**
     * Gets the trade name of this record.
     *
     * @return the trade name
     */
    public String getTradeName() {
        return tradeName;
    }

    /**
     * Sets the trade name of this record.
     *
     * @param tradeName the trade name
     */
    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    /**
     * Gets the ban of this record.
     *
     * @return the ban
     */
    public String getBan() {
        return ban;
    }

    /**
     * Sets the ban of this record.
     *
     * @param ban the ban
     */
    public void setBan(String ban) {
        this.ban = ban;
    }

    /**
     * Gets the abbreviation of this record.
     *
     * @return the abbreviation
     */
    public String getAbbreviation() {
        return abbreviation;
    }

    /**
     * Sets the abbreviation of this record.
     *
     * @param abbreviation the abbreviation
     */
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    /**
     * Gets the other name of this record.
     *
     * @return the other name
     */
    public String getOtherName() {
        return otherName;
    }

    /**
     * Sets the other name of this record.
     *
     * @param otherName the other name
     */
    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }

    /**
     * Gets the allergen id of this record.
     *
     * @return the allergen id
     */
    public String getAllergenId() {
        return allergenId;
    }

    /**
     * Sets the allergen id of this record.
     *
     * @param allergenId the allergen id
     */
    public void setAllergenId(String allergenId) {
        this.allergenId = allergenId;
    }

    /**
     * Gets the display name type of this record.
     *
     * @return the display name type
     * <ul>
     *        <li>A - Abberviation</li>
     *        <li>B - Former Ban</li>
     *        <li>G - Allergen Group</li>
     *        <li>O - Other name</li>
     *        <li>T - Tradename</li>
     *        <li>V - VTM</li>
     * </ul>
     */
    public String getDisplayNameType() {
        return this.displayNameType;
    }

    /**
     * Sets the display name type of this record.
     *
     * @param displayNameType the display name type
     *                        <ul>
     *                               <li>A - Abberviation</li>
     *                               <li>B - Former Ban</li>
     *                               <li>G - Allergen Group</li>
     *                               <li>O - Other name</li>
     *                               <li>T - Tradename</li>
     *                               <li>V - VTM</li>
     *                        </ul>
     */
    public void setDisplayNameType(String displayNameType) {
        this.displayNameType = displayNameType;
    }

    /**
     * Gets the display name of this record.
     *
     * @return the display name
     */
    public String getDisplayName() {
        return this.displayName;
    }

    /**
     * Sets the display name of this record.
     *
     * @param displayName the display name
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Gets the short display name of this record.
     *
     * @return the short display name
     */
    public String getShortDisplayName() {
        return this.shortDisplayName;
    }

    /**
     * Sets the short display name of this record.
     *
     * @param shortDisplayName the short display name
     */
    public void setShortDisplayName(String shortDisplayName) {
        this.shortDisplayName = shortDisplayName;
    }

    /**
     * Checks if is multi ingredient.
     *
     * @return true, if is multi ingredient
     */
    public boolean isMultiIngredient() {
        if (this.getVtm() != null) {
            return (this.getVtm().indexOf('+') > 0);
        }
        return false;
    }

    public void setMultiIngredient(Boolean isMulti) {
        // dummy method to allow input multiIngredient property in requestDto for RESTFul API

    }

    /**
     * Gets the search display name of this record.
     *
     * @return the search display name
     */
    public String getSearchDisplayName() {
        return searchDisplayName;
    }

    /**
     * Sets the search display name of this record.
     *
     * @param searchDisplayName the search display name
     */
    public void setSearchDisplayName(String searchDisplayName) {
        this.searchDisplayName = searchDisplayName;
    }

    public String getDisplayNameTypeDesc() {
        return displayNameTypeDesc;
    }

    public void setDisplayNameTypeDesc(String displayNameTypeDesc) {
        this.displayNameTypeDesc = displayNameTypeDesc;
    }

    public String getAllergenType() {
        return allergenType;
    }

    public void setAllergenType(String allergenType) {
        this.allergenType = allergenType;
    }

    public String getGenericProductInd() {
        return genericProductInd;
    }

    public void setGenericProductInd(String genericProductInd) {
        this.genericProductInd = genericProductInd;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o instanceof AllergenDto) {
            AllergenDto dto = (AllergenDto) o;
            if (StringUtils.equals(this.getVtm(), dto.getVtm()) &&
                    StringUtils.equals(this.getTradeName(), dto.getTradeName()) &&
                    StringUtils.equals(this.getBan(), dto.getBan()) &&
                    StringUtils.equals(this.getAbbreviation(), dto.getAbbreviation()) &&
                    StringUtils.equals(this.getOtherName(), dto.getOtherName())
            ) {
                return true;
            }
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(allergenId, vtm, tradeName, ban, allergenType, abbreviation, otherName, displayNameType,
                displayName, shortDisplayName, searchDisplayName, displayNameTypeDesc, genericProductInd);
    }

    @Override
    public String toString() {
        return "AllergenDto [allergenId=" + allergenId + ", vtm=" + vtm + ", tradeName=" + tradeName + ", ban=" + ban
                + ", allergenType=" + allergenType + ", abbreviation=" + abbreviation + ", otherName=" + otherName
                + ", displayNameType=" + displayNameType + ", displayName=" + displayName + ", shortDisplayName="
                + shortDisplayName + ", searchDisplayName=" + searchDisplayName + ", displayNameTypeDesc="
                + displayNameTypeDesc + ", genericProductInd=" + genericProductInd + "]";
    }


}
