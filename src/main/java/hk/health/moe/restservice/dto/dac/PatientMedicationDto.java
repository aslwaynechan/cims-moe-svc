package hk.health.moe.restservice.dto.dac;

import java.io.Serializable;

public class PatientMedicationDto implements Serializable {

    private static final long serialVersionUID = -7782299919095507035L;
    private String orderLineRowNum;
    private String tradeNameVtm;
    private String tradeNameVtmTermId;
    private String vtm;
    private String vtmTermId;
    private String formDesc;
    private String doseFormExtraInfo;
    private String vtmDoseFormTermId;
    private String routeDesc;
    private String vtmDoseFormRouteTermId;
    private String strength;
    private String vtmDoseFormRouteStrengthTermId;
    private String strengthCompulsory;

    public String getOrderLineRowNum() {
        return orderLineRowNum;
    }

    public void setOrderLineRowNum(String orderLineRowNum) {
        this.orderLineRowNum = orderLineRowNum;
    }

    public String getTradeNameVtm() {
        return tradeNameVtm;
    }

    public void setTradeNameVtm(String tradeNameVtm) {
        this.tradeNameVtm = tradeNameVtm;
    }

    public String getTradeNameVtmTermId() {
        return tradeNameVtmTermId;
    }

    public void setTradeNameVtmTermId(String tradeNameVtmTermId) {
        this.tradeNameVtmTermId = tradeNameVtmTermId;
    }

    public String getVtm() {
        return vtm;
    }

    public void setVtm(String vtm) {
        this.vtm = vtm;
    }

    public String getVtmTermId() {
        return vtmTermId;
    }

    public void setVtmTermId(String vtmTermId) {
        this.vtmTermId = vtmTermId;
    }

    public String getFormDesc() {
        return formDesc;
    }

    public void setFormDesc(String formDesc) {
        this.formDesc = formDesc;
    }

    public String getDoseFormExtraInfo() {
        return doseFormExtraInfo;
    }

    public void setDoseFormExtraInfo(String doseFormExtraInfo) {
        this.doseFormExtraInfo = doseFormExtraInfo;
    }

    public String getVtmDoseFormTermId() {
        return vtmDoseFormTermId;
    }

    public void setVtmDoseFormTermId(String vtmDoseFormTermId) {
        this.vtmDoseFormTermId = vtmDoseFormTermId;
    }

    public String getRouteDesc() {
        return routeDesc;
    }

    public void setRouteDesc(String routeDesc) {
        this.routeDesc = routeDesc;
    }

    public String getVtmDoseFormRouteTermId() {
        return vtmDoseFormRouteTermId;
    }

    public void setVtmDoseFormRouteTermId(String vtmDoseFormRouteTermId) {
        this.vtmDoseFormRouteTermId = vtmDoseFormRouteTermId;
    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public String getVtmDoseFormRouteStrengthTermId() {
        return vtmDoseFormRouteStrengthTermId;
    }

    public void setVtmDoseFormRouteStrengthTermId(String vtmDoseFormRouteStrengthTermId) {
        this.vtmDoseFormRouteStrengthTermId = vtmDoseFormRouteStrengthTermId;
    }

    public String getStrengthCompulsory() {
        return strengthCompulsory;
    }

    public void setStrengthCompulsory(String strengthCompulsory) {
        this.strengthCompulsory = strengthCompulsory;
    }
}
