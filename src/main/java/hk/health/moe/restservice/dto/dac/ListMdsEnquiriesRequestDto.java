package hk.health.moe.restservice.dto.dac;

/**
 * @Author Simon
 * @Date 2019/10/12
 */
public class ListMdsEnquiriesRequestDto {
    private String hospCode;
    private String userId;

    public String getHospCode() {
        return hospCode;
    }

    public void setHospCode(String hospCode) {
        this.hospCode = hospCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
