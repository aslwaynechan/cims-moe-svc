package hk.health.moe.restservice.dto.dac;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MedicationDecisionRequestDto implements Serializable {

    private static final long serialVersionUID = -5095159652480981372L;
    private List<PatientAllergyDto> patientAllergies;
    private List<PatientMedicationDto> patientMedications;
    private String workstationIp;
    private String userId;

    public List<PatientAllergyDto> getPatientAllergies() {
        if (null == patientAllergies) {
            patientAllergies = new ArrayList<>();
        }
        return patientAllergies;
    }

    public void setPatientAllergies(List<PatientAllergyDto> patientAllergies) {
        this.patientAllergies = patientAllergies;
    }

    public List<PatientMedicationDto> getPatientMedications() {
        if (null == patientMedications) {
            patientMedications = new ArrayList<>();
        }
        return patientMedications;
    }

    public void setPatientMedications(List<PatientMedicationDto> patientMedications) {
        this.patientMedications = patientMedications;
    }

    public String getWorkstationIp() {
        return workstationIp;
    }

    public void setWorkstationIp(String workstationIp) {
        this.workstationIp = workstationIp;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
