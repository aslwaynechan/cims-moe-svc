/*********************************************************************
* Copyright (c) eHR HK Limited 2013. 
* All Rights Reserved.
* 
*********************************************************************
* PROJECT NAME    :  eHR Adaptation
*
* MODULE NAME     :  Structured Alert Adaptation Module (SAAM)
*
* PROGRAM NAME    :  ehr-apps-sa-model.war
*
* PURPOSE         :  A data transfer object (DTO) used to transfer data in the application
*
**********************************************************************
* VERSION         :   1.01.001
* REF NO          :  
* DATE CREATED    :   24 June 2013
* 
*********************************************************************
*
* CHANGES         :
* REF NO           DATE        DETAIL
* ------           ----        ------
*
*********************************************************************
*/
package hk.health.moe.restservice.dto.saam;

import hk.health.moe.restservice.util.StringUtil;
import org.dozer.Mapping;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * AllergyDTO is a data transfer object which contains information of Patient's Allergy.
 *
 * @author HAITS CE3
 */
public class PatientAllergyRequestDto /*extends RequestDto*/ implements Serializable {
	
	private static final long serialVersionUID = -3213820147398222356L;
	private String allergySeqNo;	
	//@Mapping("patient.patientKey") private String patientKey;
	private String displayName;
	//private String searchDisplayName;
	@Mapping("allergenType.allergenType") private String allergenType;
	private AllergenDto allergenDto;
	@Mapping("allergyCode") private String allergenCode;
	private String certainty;
	private String additionInfo;
	private String hospitalCode;
	private String sourceSystem;
//	private Date createDate;
//	private String userId;
//	private String user;
//	private String userHosp;
//	private String userRank;
//	private String userRankDesc;
	private String deleteReason;
//	private String trxType;
	private int version;
	private String updateUserId;
	private Date updateDtm;
	private String updateUser;
	private String updateRank;
	private String updateRankDesc;
	private String updateHosp;
	private List<AllergyReactionDto> allergyReactionDtoList = new ArrayList<AllergyReactionDto>();
//	private String reactions = "";
//	private boolean isMigrateData;
	
//	private String importAllergyName;
//	private String importManifestDesc;
//	private String importAdditionInfo;
//	private AllergyTermDto allergyTermDto;
//	private String allergyType;
	
//	private String origDisplayName;
	
	/**
	 * Gets the user login id who update this record.
	 *
	 * @return the user login id
	 */
	public String getUpdateUserId() {
		return updateUserId;
	}
	

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	
	/**
	 * Gets the update date when update this record.
	 *
	 * @return the update date
	 */
	public Date getUpdateDtm() {
		return updateDtm;
	}
	

	public void setUpdateDtm(Date updateDtm) {
		this.updateDtm = updateDtm;
	}
	
	/**
	 * Gets the delete reason of this record.
	 *
	 * @return the delete reason  
	 */
	public String getDeleteReason() {
		return deleteReason;
	}
	
	/**
	 * Sets the delete reason of this record.
	 *
	 * @param deleteReason the delete reason
	 */
	public void setDeleteReason(String deleteReason) {
		this.deleteReason = StringUtil.trimString(deleteReason);
	}		
	
	/**
	 * Instantiates a new allergy dto.
	 */
	public PatientAllergyRequestDto(){
	}	
	
	public List<AllergyReactionDto> getAllergyReactionDtoList() {
		return allergyReactionDtoList;
	}

	public void setAllergyReactionDtoList(List<AllergyReactionDto> allergyReactionDtoList) {
		this.allergyReactionDtoList = allergyReactionDtoList;
	}

	public void addAllergyReactionDto(AllergyReactionDto allergyReactionDto) {
		allergyReactionDtoList.add(allergyReactionDto);
	}
	
	public void removeAllergyReactionDto(AllergyReactionDto allergyReactionDto) {
		allergyReactionDtoList.remove(allergyReactionDto);
	}
	
	/**
	 * Sort manifestBeanArr of this record in alphabetical order.
	 */
	@SuppressWarnings("unchecked")
	public void sortAllergyReactionDtoList() {
	//	Collections.sort(drugReactList, String.CASE_INSENSITIVE_ORDER); 
		Collections.sort(allergyReactionDtoList);
	}
	
	/**
	 * Gets the allergy sequence number of this record.
	 *
	 * @return the allergy sequence number
	 */
	public String getAllergySeqNo() {
		return allergySeqNo;
	}
	
	/**
	 * Sets the allergy sequence number of this record.
	 *
	 * @param allergySeqNo the unique key of identifying this record
	 */
	public void setAllergySeqNo(String allergySeqNo) {
		this.allergySeqNo = allergySeqNo;
	}
	
	/**
	 * Gets the allergen type of this record.
	 *
	 * @return the allergen type
     * <ul>
	 *        <li>D - Drug Allergy</li>
	 *        <li>G - Allergen Group</li>
	 *        <li>O - Free Text Allergy (Drug)</li>
	 *        <li>N - No Known Drug Allergy</li>
	 *        <li>X - Free Text Allergy (Non-Drug)</li>
	 *        <li>L - Local Drug</li>
	 * </ul>  
	 */
	public String getAllergenType() {
		return allergenType;
	}
	
	/**
	 * Sets the allergen type of this record.
	 *
	 * @param allergenType the allergen type
	 * <ul>
	 *        <li>D - Drug Allergy</li>
	 *        <li>G - Allergen Group</li>
	 *        <li>O - Free Text Allergy (Drug)</li>
	 *        <li>N - No Known Drug Allergy</li>
	 *        <li>X - Free Text Allergy (Non-Drug)</li>
	 * </ul>
	 */
	public void setAllergenType(String allergenType) {
		this.allergenType = allergenType;
	}
	
	/**
	 * Gets the allergen code of this record.
	 *
	 * @return the allergen code
	 */
	public String getAllergenCode() {
		return allergenCode;
	}
	
	/**
	 * Sets the allergen code of this record.
	 *
	 * @param allergenCode the allergen code
	 */
	public void setAllergenCode(String allergenCode) {
		this.allergenCode = allergenCode;
	}
	
	/**
	 * Gets the certainty of this record.
	 *
	 * @return the certainty
	 * <ul>
	 * 	      <li>S - suspected</li>
	 *        <li>C - Certain</li>
	 * </ul>
	 */
	public String getCertainty() {
		return certainty;
	}
	
	/**
	 * Sets the certainty of this record.
	 *
	 * @param certainty the certainty.
	 * <ul>
	 * 	      <li>S - suspected</li>
	 *        <li>C - Certain</li>
	 * </ul>
	 */
	public void setCertainty(String certainty) {
		this.certainty = certainty;
	}
	
	/**
	 * Gets the additional information of this record.
	 *
	 * @return the additional information
	 */
	public String getAdditionInfo() {
		return additionInfo;
	}
	
	/**
	 * Sets the additional information of this record.
	 *
	 * @param additionInfo the additional information
	 */
	public void setAdditionInfo(String additionInfo) {
		this.additionInfo = StringUtil.trimString(additionInfo);
	}

	/**
	 * Gets the hospital code of this record.
	 *
	 * @return the hospital code
	 */
	public String getHospitalCode() {
		return hospitalCode;
	}
	
	/**
	 * Sets the hospital code of this record.
	 *
	 * @param hospitalCode the hospital code
	 */
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	
	/**
	 * Gets the source of external system which access Alert Allergy Web Application.
	 *
	 * @return the name of source system
	 */
	public String getSourceSystem() {
		return sourceSystem;
	}
	
	/**
	 * Sets the source of external system which access Alert Allergy Web Application.
	 *
	 * @param sourceSystem the name of source system
	 */
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	
	/**
	 * Sets the version number of current Alert Allergy Web.
	 *
	 * @param version the version number
	 */
	public void setVersion(int version) {
		this.version = version;
	}

	/**
	 * Gets the version number of current Alert Allergy Web.
	 *
	 * @return the version number
	 */
	public int getVersion() {
		return version;
	}
	
	/**
	 * Gets the display name of this record.
	 *
	 * @return the display name
	 */
	public String getDisplayName() {
		return displayName;
	}
	
	/**
	 * Sets the display name of this record.
	 *
	 * @param displayName the display name
	 */
	public void setDisplayName(String displayName) {
		this.displayName = StringUtil.trimString(displayName);
	}
	
	/**
	 * Gets the user of login who update this record.
	 *
	 * @return the user of login
	 */
	public String getUpdateUser() {
		return updateUser;
	}
	
	/**
	 * Sets the user of login who update this record.
	 *
	 * @param updateUser the user of login
	 */
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
	/**
	 * Gets the rank of login who update this record.
	 *
	 * @return the rank of login
	 */
	public String getUpdateRank() {
		return updateRank;
	}
	
	/**
	 * Sets the rank of login who update this record.
	 *
	 * @param updateRank the rank of login
	 */
	public void setUpdateRank(String updateRank) {
		this.updateRank = updateRank;
	}
	
	/**
	 * Gets the rank description of login who update this record.
	 *
	 * @return the rank description of login
	 */
	public String getUpdateRankDesc() {
		return updateRankDesc;
	}
	
	/**
	 * Sets the rank description of login who update this record.
	 *
	 * @param updateRankDesc the rank description of login
	 */
	public void setUpdateRankDesc(String updateRankDesc) {
		this.updateRankDesc = updateRankDesc;
	}
	
	/**
	 * Gets the hospital name of login who update this record.
	 *
	 * @return the hospital name of login
	 */
	public String getUpdateHosp() {
		return updateHosp;
	}
	
	/**
	 * Sets the hospital name of login who update this record.
	 *
	 * @param updateHosp the hospital name of login
	 */
	public void setUpdateHosp(String updateHosp) {
		this.updateHosp = updateHosp;
	}
	
	public AllergenDto getAllergenDto() {
		return allergenDto;
	}

	public void setAllergenDto(AllergenDto allergenDto) {
		this.allergenDto = allergenDto;
	}
	
}
