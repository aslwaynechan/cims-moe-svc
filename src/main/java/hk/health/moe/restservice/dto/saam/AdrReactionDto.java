package hk.health.moe.restservice.dto.saam;

import java.io.Serializable;

public class AdrReactionDto implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String reactionSeqNo;
    private String drugReactionDesc;
    private String localDrugReactionDesc;
    private String quickList;
    private Integer displayOrder;
    private String status;

    public String getReactionSeqNo() {
        return reactionSeqNo;
    }

    public void setReactionSeqNo(String reactionSeqNo) {
        this.reactionSeqNo = reactionSeqNo;
    }

    public String getDrugReactionDesc() {
        return drugReactionDesc;
    }

    public void setDrugReactionDesc(String drugReactionDesc) {
        this.drugReactionDesc = drugReactionDesc;
    }

    public String getLocalDrugReactionDesc() {
        return localDrugReactionDesc;
    }

    public void setLocalDrugReactionDesc(String localDrugReactionDesc) {
        this.localDrugReactionDesc = localDrugReactionDesc;
    }

    public String getQuickList() {
        return quickList;
    }

    public void setQuickList(String quickList) {
        this.quickList = quickList;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
