/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Drug Allergy Checking (DAC)
 *
 * PROGRAM NAME    :  ehr-apps-drugchecking.war
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :  1.01.001
 * REF NO          :
 * DATE CREATED    :  7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.restservice.dto.dac;

import java.io.Serializable;
import java.util.List;

public class MedicationDecisionResponseDto implements Serializable {

    private static final long serialVersionUID = -162679785429655968L;
    private List<MedicationDecisionResultDto> medicationDecisions;
    private String returnCode;
    private String returnMessage;

    public List<MedicationDecisionResultDto> getMedicationDecisions() {
        return medicationDecisions;
    }

    public void setMedicationDecisions(List<MedicationDecisionResultDto> medicationDecisions) {
        this.medicationDecisions = medicationDecisions;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }
}
