package hk.health.moe.restservice.dto.dac;

/**
 * @Author Simon
 * @Date 2019/9/26
 */
public class DacAppDto {
    private String appId;
    private String appName;
    private String password;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
