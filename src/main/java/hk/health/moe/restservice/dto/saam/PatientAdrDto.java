/*********************************************************************
* Copyright (c) eHR HK Limited 2013. 
* All Rights Reserved.
* 
*********************************************************************
* PROJECT NAME    :  eHR Adaptation
*
* MODULE NAME     :  Structured Alert Adaptation Module (SAAM)
*
* PROGRAM NAME    :  ehr-apps-sa-model.war
*
* PURPOSE         :  A data transfer object (DTO) used to transfer data in the application
*
**********************************************************************
* VERSION         :   1.01.001
* REF NO          :  
* DATE CREATED    :   24 June 2013
* 
*********************************************************************
*
* CHANGES         :
* REF NO           DATE        DETAIL
* ------           ----        ------
*
*********************************************************************
*/
package hk.health.moe.restservice.dto.saam;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * AllergyDTO is a data transfer object which contains information of Patient's Allergy.
 *
 * @author HAITS CE3
 */
public class PatientAdrDto implements Serializable {
	
	private static final long serialVersionUID = -3213820147398222356L;
	private String adrSeqNo;	
	private String drugCode;
	private String drugDesc;
	private String shortDrugDesc;
	private String searchDisplayName;
	private AllergenDto allergenDto;
	private String severity;
	private String additionInfo;
	private String hospitalCode;
	private String sourceSystem;
	private String deleteReason;
	private int version;
	private String updateBy;
	private Date updateDtm;
	private String updateUser;
	private String updateRank;
	private String updateRankDesc;
	private String updateHosp;
	private List<AdrReactionDto> adrReactionDtoList = new ArrayList<AdrReactionDto>();
	private String reactions = "";
	private boolean isMigrateData;
	private boolean isFreeText;
	
	private String importAdrName;
	private String importReactionDesc;
	private String importAdditionInfo;
	private String adrType;
	private AdrTermDto adrTermDto;

	
	/**
	 * Gets the user login id who update this record.
	 *
	 * @return the user login id
	 */
	public String getUpdateBy() {
		return updateBy;
	}
	
	/**
	 * Sets the user login id who update this record.
	 *
	 * @param updateBy the user login id
	 */
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	
	/**
	 * Gets the update date when update this record.
	 *
	 * @return the update date
	 */
	public Date getUpdateDtm() {
		return updateDtm;
	}
	
	/**
	 * Sets the update date when update this record.
	 *
	 * @param updateDtm the update date
	 */
	public void setUpdateDtm(Date updateDtm) {
		this.updateDtm = updateDtm;
	}
	
	/**
	 * Gets the delete reason of this record.
	 *
	 * @return the delete reason
	 */
	public String getDeleteReason() {
		return deleteReason;
	}
	
	/**
	 * Sets the delete reason of this record.
	 *
	 * @param deleteReason the delete reason
	 */
	public void setDeleteReason(String deleteReason) {
		this.deleteReason = deleteReason;
	}		
	
	/**
	 * Instantiates a new allergy dto.
	 */
	public PatientAdrDto(){
	}	
	
	/**
	 * Gets the additional information of this record.
	 *
	 * @return the additional information
	 */
	public String getAdditionInfo() {
		return additionInfo;
	}
	
	/**
	 * Sets the additional information of this record.
	 *
	 * @param additionInfo the additional information
	 */
	public void setAdditionInfo(String additionInfo) {
		this.additionInfo = additionInfo;
	}
	
	/**
	 * Gets the hospital code of this record.
	 *
	 * @return the hospital code
	 */
	public String getHospitalCode() {
		return hospitalCode;
	}
	
	/**
	 * Sets the hospital code of this record.
	 *
	 * @param hospitalCode the hospital code
	 */
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	
	/**
	 * Gets the source of external system which access Alert Allergy Web Application.
	 *
	 * @return the name of source system
	 */
	public String getSourceSystem() {
		return sourceSystem;
	}
	
	/**
	 * Sets the source of external system which access Alert Allergy Web Application.
	 *
	 * @param sourceSystem the name of source system
	 */
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	
	/**
	 * Sets the version number of current Alert Allergy Web.
	 *
	 * @param version the version number
	 */
	public void setVersion(int version) {
		this.version = version;
	}

	/**
	 * Gets the version number of current Alert Allergy Web.
	 *
	 * @return the version number
	 */
	public int getVersion() {
		return version;
	}
	
	
	/**
	 * Gets the user of login who update this record.
	 *
	 * @return the user of login
	 */
	public String getUpdateUser() {
		return updateUser;
	}
	
	/**
	 * Sets the user of login who update this record.
	 *
	 * @param updateUser the user of login
	 */
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
	/**
	 * Gets the rank of login who update this record.
	 *
	 * @return the rank of login
	 */
	public String getUpdateRank() {
		return updateRank;
	}
	
	/**
	 * Sets the rank of login who update this record.
	 *
	 * @param updateRank the rank of login
	 */
	public void setUpdateRank(String updateRank) {
		this.updateRank = updateRank;
	}
	
	/**
	 * Gets the rank description of login who update this record.
	 *
	 * @return the rank description of login
	 */
	public String getUpdateRankDesc() {
		return updateRankDesc;
	}
	
	/**
	 * Sets the rank description of login who update this record.
	 *
	 * @param updateRankDesc the rank description of login
	 */
	public void setUpdateRankDesc(String updateRankDesc) {
		this.updateRankDesc = updateRankDesc;
	}
	
	/**
	 * Gets the hospital name of login who update this record.
	 *
	 * @return the hospital name of login
	 */
	public String getUpdateHosp() {
		return updateHosp;
	}
	
	/**
	 * Sets the hospital name of login who update this record.
	 *
	 * @param updateHosp the hospital name of login
	 */
	public void setUpdateHosp(String updateHosp) {
		this.updateHosp = updateHosp;
	}
	
	/**
	 * Gets the boolean of whether the record is a migrate data.
	 *
	 * @return the flag whether the record is a migrate data
	 */
	public boolean isMigrateData() {
		return isMigrateData;
	}

	/**
	 * Sets whether the record is a migrate data.
	 *
	 * @param isMigrateData of whether the record is a migrate data.
	 */
	public void setMigrateData(boolean isMigrateData) {
		this.isMigrateData = isMigrateData;
	}

	public String getReactions() {
		return reactions;
	}

	public void setReactions(String reactions) {
		this.reactions = reactions;
	}

	public AllergenDto getAllergenDto() {
		return allergenDto;
	}

	public void setAllergenDto(AllergenDto allergenDto) {
		this.allergenDto = allergenDto;
	}

	public String getAdrSeqNo() {
		return adrSeqNo;
	}

	public void setAdrSeqNo(String adrSeqNo) {
		this.adrSeqNo = adrSeqNo;
	}

	public String getDrugCode() {
		return drugCode;
	}

	public void setDrugCode(String drugCode) {
		this.drugCode = drugCode;
	}

	public String getDrugDesc() {
		return drugDesc;
	}

	public void setDrugDesc(String drugDesc) {
		this.drugDesc = drugDesc;
	}
	
	public String getShortDrugDesc() {
		return shortDrugDesc;
	}

	public void setShortDrugDesc(String shortDrugDesc) {
		this.shortDrugDesc = shortDrugDesc;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public List<AdrReactionDto> getAdrReactionDtoList() {
		return adrReactionDtoList;
	}

	public void setAdrReactionDtoList(List<AdrReactionDto> adrReactionDtoList) {
		this.adrReactionDtoList = adrReactionDtoList;
	}

	public String getSearchDisplayName() {
		return searchDisplayName;
	}

	public void setSearchDisplayName(String searchDisplayName) {
		this.searchDisplayName = searchDisplayName;
	}

	public boolean isFreeText() {
		return isFreeText;
	}

	public void setFreeText(boolean isFreeText) {
		this.isFreeText = isFreeText;
	}

	public String getImportAdrName() {
		return importAdrName;
	}

	public void setImportAdrName(String importAdrName) {
		this.importAdrName = importAdrName;
	}

	public String getImportReactionDesc() {
		return importReactionDesc;
	}

	public void setImportReactionDesc(String importReactionDesc) {
		this.importReactionDesc = importReactionDesc;
	}

	public String getImportAdditionInfo() {
		return importAdditionInfo;
	}

	public void setImportAdditionInfo(String importAdditionInfo) {
		this.importAdditionInfo = importAdditionInfo;
	}

	public String getAdrType() {
		return adrType;
	}

	public void setAdrType(String adrType) {
		this.adrType = adrType;
	}

	public AdrTermDto getAdrTermDto() {
		return adrTermDto;
	}

	public void setAdrTermDto(AdrTermDto adrTermDto) {
		this.adrTermDto = adrTermDto;
	}

}
