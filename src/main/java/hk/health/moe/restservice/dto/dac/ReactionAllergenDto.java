package hk.health.moe.restservice.dto.dac;

import java.io.Serializable;

public class ReactionAllergenDto implements Serializable {

    private static final long serialVersionUID = -5824608211200273463L;
    private String allergenVTM;
    private String allergySeqNo;
    private String allergenVTMTermID;
    private String reactionLevel;

    public String getAllergenVTM() {
        return allergenVTM;
    }

    public void setAllergenVTM(String allergenVTM) {
        this.allergenVTM = allergenVTM;
    }

    public String getAllergySeqNo() {
        return allergySeqNo;
    }

    public void setAllergySeqNo(String allergySeqNo) {
        this.allergySeqNo = allergySeqNo;
    }

    public String getAllergenVTMTermID() {
        return allergenVTMTermID;
    }

    public void setAllergenVTMTermID(String allergenVTMTermID) {
        this.allergenVTMTermID = allergenVTMTermID;
    }

    public String getReactionLevel() {
        return reactionLevel;
    }

    public void setReactionLevel(String reactionLevel) {
        this.reactionLevel = reactionLevel;
    }
}
