package hk.health.moe.restservice.dto.dac;

import java.util.List;

/**
 * @Author Simon
 * @Date 2019/10/14
 */
public class MDSEnquiryResponseDto {
    private List<AllergenGroupDto> allergenGroups;

    public List<AllergenGroupDto> getAllergenGroups() {
        return allergenGroups;
    }

    public void setAllergenGroups(List<AllergenGroupDto> allergenGroups) {
        this.allergenGroups = allergenGroups;
    }
}
