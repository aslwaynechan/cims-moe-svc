package hk.health.moe.restservice.dto.saam;

import java.io.Serializable;
import java.util.Date;

public class AddPatientAlertResponse implements Serializable {

	private static final long serialVersionUID = 1676554413067363119L;
	private ResponseDto responseDto;
	private Date validFrom;
	private Date validTo;

	public ResponseDto getResponseDto() {
		return responseDto;
	}

	public void setResponseDto(ResponseDto responseDto) {
		this.responseDto = responseDto;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
}
