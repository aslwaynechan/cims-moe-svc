package hk.health.moe.restservice.dto.saam;

import java.io.Serializable;
import java.util.Date;

/**
 * AlertDTO is a data transfer object which contains information of Patient's Alert.
 *
 * @author HAITS CE3
 */
public class PatientAlertDto implements Serializable {

    private static final long serialVersionUID = 5553697343538770822L;

    protected String alertSeqNo;
    protected String alertCode;
    protected String alertDesc;
    protected String additionInfo;
    protected String classifiedIndicator;
    protected Date validFrom;
    protected Date validTo;
    protected String hospitalCode;
    protected String sourceSystem;
    protected Date createDtm;
    protected String createUserId;
    protected String createUser;
    protected String createHosp;
    protected String createRank;
    protected String createRankDesc;
    protected String deleteReason;
    protected String trxType;
    protected Date trxDtm;
    protected String allowModifyVf;
    protected String allowModifyVt;
    protected int version;
    protected String updateUserId;
    protected Date updateDtm;
    protected String updateUser;
    protected String updateHosp;
    protected String updateRank;
    protected String updateRankDesc;

    protected String importAlertDesc;
    protected String alertType;


    /**
     * Gets the user login id who update this record.
     *
     * @return the user login id
     */
    public String getUpdateUserId() {
        return updateUserId;
    }

    /**
     * Sets the user login id who update this record.
     *
     * @param updateUserId the user login id
     */
    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    /**
     * Gets the update date when update this record.
     *
     * @return the update date
     */
    public Date getUpdateDtm() {
        return updateDtm;
    }

    /**
     * Sets the update date when update this record.
     *
     * @param date the update date
     */
    public void setUpdateDtm(Date date) {
        this.updateDtm = date;
    }

    /**
     * Gets the delete reason of this record.
     *
     * @return the delete reason
     */
    public String getDeleteReason() {
        return deleteReason;
    }

    /**
     * Sets the delete reason of this record.
     *
     * @param deleteReason the delete reason
     */
    public void setDeleteReason(String deleteReason) {
        this.deleteReason = deleteReason;
    }

    /**
     * Gets the alert sequence number of this record.
     *
     * @return the alert sequence number
     */
    public String getAlertSeqNo() {
        return alertSeqNo;
    }

    /**
     * Sets the alert sequence number of this record.
     *
     * @param alertSeqNo the unique key of identifying this record
     */
    public void setAlertSeqNo(String alertSeqNo) {
        this.alertSeqNo = alertSeqNo;
    }

    /**
     * Gets the id of login user who create this record.
     *
     * @return the id
     */
    public String getCreateUserId() {
        return createUserId;
    }

    /**
     * Sets the id of login user who create this record.
     *
     * @param createUserId the id
     */
    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    /**
     * Gets the hospital code of this record.
     *
     * @return the hospital code
     */
    public String getHospitalCode() {
        return hospitalCode;
    }

    /**
     * Gets the source of external system which access Alert Allergy Web Application.
     *
     * @return the name of source system
     */
    public String getSourceSystem() {
        return this.sourceSystem;
    }

    /**
     * Sets the source of external system which access Alert Allergy Web Application.
     *
     * @param sourceSystem the name of source system
     */
    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    /**
     * Sets the hospital code of this record.
     *
     * @param hospitalCode the hospital code
     */
    public void setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
    }

    /**
     * Gets the additional information of this record.
     *
     * @return the additional information
     */
    public String getAdditionInfo() {
        return additionInfo;
    }

    /**
     * Sets the additional information of this record.
     *
     * @param additionInfo the additional information
     */
    public void setAdditionInfo(String additionInfo) {
        this.additionInfo = additionInfo;
    }

    /**
     * Gets the classified indicator of this record.
     *
     * @return the classified indicator
     * <ul>
     *        <li>Y - Yes</li>
     *        <li>N - No</li>
     * </ul>
     */
    public String getClassifiedIndicator() {
        return classifiedIndicator;
    }

    /**
     * Sets the classified indicator of this record.
     *
     * @param classifiedIndicator the classified indicator
     *                            <ul>
     *                                   <li>Y - Yes</li>
     *                                   <li>N - No</li>
     *                            </ul>
     */
    public void setClassifiedIndicator(String classifiedIndicator) {
        this.classifiedIndicator = classifiedIndicator;
    }

    /**
     * Gets the valid from of this record.
     *
     * @return the valid from
     */
    public Date getValidFrom() {
        return validFrom;
    }

    /**
     * Sets the valid from of this record.
     *
     * @param validFrom the valid from
     */
    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    /**
     * Gets the valid to of this record.
     *
     * @return the valid to
     */
    public Date getValidTo() {
        return validTo;
    }

    /**
     * Sets the valid to of this record.
     *
     * @param validTo the valid to
     */
    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    /**
     * Gets the alert code of this record.
     *
     * @return the alert code
     */
    public String getAlertCode() {
        return alertCode;
    }

    /**
     * Sets the alert code of this record.
     *
     * @param alertCode the alert code
     */
    public void setAlertCode(String alertCode) {
        this.alertCode = alertCode;
    }

    /**
     * Gets the alert description of this record.
     *
     * @return the alert description
     */
    public String getAlertDesc() {
        return alertDesc;
    }

    /**
     * Sets the alert description of this record.
     *
     * @param alertDesc the alert description
     */
    public void setAlertDesc(String alertDesc) {
        this.alertDesc = alertDesc;
    }

    /**
     * Gets the allow modify valid date from of this record.
     *
     * @return the allow modify valid date from
     * <ul>
     *        <li>Y - Yes</li>
     *        <li>N - No</li>
     * </ul>
     */
    public String getAllowModifyVf() {
        return allowModifyVf;
    }

    /**
     * Sets the allow modify valid date from of this record.
     *
     * @param allowModifyVf the allow modify valid date from
     *                      <ul>
     *                             <li>Y - Yes</li>
     *                             <li>N - No</li>
     *                      </ul>
     */
    public void setAllowModifyVf(String allowModifyVf) {
        this.allowModifyVf = allowModifyVf;
    }

    /**
     * Gets the allow modify Valid Date To of this record.
     *
     * @return the allow modify Valid Date To
     * <ul>
     *        <li>Y - Yes</li>
     *        <li>N - No</li>
     * </ul>
     */
    public String getAllowModifyVt() {
        return allowModifyVt;
    }

    /**
     * Sets the allow modify Valid Date To of this record.
     *
     * @param allowModifyVt the allow modify Valid Date To
     *                      <ul>
     *                             <li>Y - Yes</li>
     *                             <li>N - No</li>
     *                      </ul>
     */
    public void setAllowModifyVt(String allowModifyVt) {
        this.allowModifyVt = allowModifyVt;
    }

    /**
     * Sets the version number of current Alert Allergy Web.
     *
     * @param version the version number
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * Gets the version number of current Alert Allergy Web.
     *
     * @return the version number
     */
    public int getVersion() {
        return version;
    }

    /**
     * Returns the transaction type of this record.
     *
     * @return the transaction type. D if record is deleted otherwise return NULL for records have been added.
     * <ul>
     *        <li>I - insert</li>
     *        <li>U - update</li>
     *        <li>D - delete</li>
     * </ul>
     */
    public String getTrxType() {
        return trxType;
    }

    /**
     * Sets the transaction type of this record.
     *
     * @param trxType the transaction type
     *                <ul>
     *                       <li>I - insert</li>
     *                       <li>U - update</li>
     *                       <li>D - delete</li>
     *                </ul>
     */
    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    public Date getTrxDtm() {
        return trxDtm;
    }

    public void setTrxDtm(Date trxDtm) {
        this.trxDtm = trxDtm;
    }

    /**
     * Gets the name of login user who create this record.
     *
     * @return the name
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Sets the name of login user who create this record.
     *
     * @param createUser the name
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * Gets the hospital name of login user who update this record.
     *
     * @return the hospital name
     */
    public String getCreateHosp() {
        return createHosp;
    }

    /**
     * Sets the hospital name of login user who update this record.
     *
     * @param createHosp the hospital name
     */
    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    /**
     * Gets the rank of login user who update this record.
     *
     * @return the rank
     */
    public String getCreateRank() {
        return createRank;
    }

    /**
     * Sets the rank of login user who update this record.
     *
     * @param createRank the rank
     */
    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    /**
     * Gets the rank description of login user who update this record.
     *
     * @return the rank description
     */
    public String getCreateRankDesc() {
        return createRankDesc;
    }

    /**
     * Sets the rank description of login user who update this record.
     *
     * @param createRankDesc the rank description
     */
    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    /**
     * Gets the user of login who update this record.
     *
     * @return the user of login
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * Sets the user of login who update this record.
     *
     * @param updateUser the user of login
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * Gets the hospital name of login who update this record.
     *
     * @return the hospital name of login
     */
    public String getUpdateHosp() {
        return updateHosp;
    }

    /**
     * Sets the hospital name of login who update this record.
     *
     * @param updateHosp the hospital name of login
     */
    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    /**
     * Gets the rank of login who update this record.
     *
     * @return the rank of login
     */
    public String getUpdateRank() {
        return updateRank;
    }

    /**
     * Sets the rank of login who update this record.
     *
     * @param updateUserRank the rank of login
     */
    public void setUpdateRank(String updateUserRank) {
        this.updateRank = updateUserRank;
    }

    /**
     * Gets the rank description of login who update this record.
     *
     * @return the rank description of login
     */
    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    /**
     * Sets the rank description of login who update this record.
     *
     * @param updateRankDesc the rank description of login
     */
    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    public String getImportAlertDesc() {
        return importAlertDesc;
    }

    public void setImportAlertDesc(String importAlertDesc) {
        this.importAlertDesc = importAlertDesc;
    }

    public String getAlertType() {
        return alertType;
    }

    public void setAlertType(String alertType) {
        this.alertType = alertType;
    }

    public Date getCreateDtm() {
        return createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }


}
