package hk.health.moe.restservice.dto.dac;

public class ActiveIngredientDto {
	
	private String activeIngredient;
	private Long activeIngredientTermId;
	private Boolean isSuppressed;
	private String customizationContent;
	
	public String getActiveIngredient() {
		return activeIngredient;
	}
	public void setActiveIngredient(String activeIngredient) {
		this.activeIngredient = activeIngredient;
	}
	public Long getActiveIngredientTermId() {
		return activeIngredientTermId;
	}
	public void setActiveIngredientTermId(Long activeIngredientTermId) {
		this.activeIngredientTermId = activeIngredientTermId;
	}
	public Boolean getIsSuppressed() {
		return isSuppressed;
	}
	public void setIsSuppressed(Boolean isSuppressed) {
		this.isSuppressed = isSuppressed;
	}
	
	public String getCustomizationContent() {
		return customizationContent;
	}
	public void setCustomizationContent(String customizationContent) {
		this.customizationContent = customizationContent;
	}
	@Override
	public String toString() {
		return "ActiveIngredientDto [activeIngredient=" + activeIngredient
				+ ", activeIngredientTermId=" + activeIngredientTermId
				+ ", isSuppressed=" + isSuppressed
				+ ", customizationContent=" + customizationContent+ "]";
	}

}
