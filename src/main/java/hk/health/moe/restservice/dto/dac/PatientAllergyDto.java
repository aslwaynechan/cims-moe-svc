package hk.health.moe.restservice.dto.dac;

import java.io.Serializable;

public class PatientAllergyDto implements Serializable {

    private static final long serialVersionUID = -4983224884178370877L;
    private String allergen;
    private String allergenTermID;
    private String allergenType;
    private String allergySeqNo;

    public String getAllergen() {
        return allergen;
    }

    public void setAllergen(String allergen) {
        this.allergen = allergen;
    }

    public String getAllergenTermID() {
        return allergenTermID;
    }

    public void setAllergenTermID(String allergenTermID) {
        this.allergenTermID = allergenTermID;
    }

    public String getAllergenType() {
        return allergenType;
    }

    public void setAllergenType(String allergenType) {
        this.allergenType = allergenType;
    }

    public String getAllergySeqNo() {
        return allergySeqNo;
    }

    public void setAllergySeqNo(String allergySeqNo) {
        this.allergySeqNo = allergySeqNo;
    }
}
