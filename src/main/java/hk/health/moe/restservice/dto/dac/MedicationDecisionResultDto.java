package hk.health.moe.restservice.dto.dac;

import java.util.ArrayList;
import java.util.List;

public class MedicationDecisionResultDto {

    private String orderLineRowNum;
    private List<ReactionAllergenDto> reactionAllergens;

    public String getOrderLineRowNum() {
        return orderLineRowNum;
    }

    public void setOrderLineRowNum(String orderLineRowNum) {
        this.orderLineRowNum = orderLineRowNum;
    }

    public List<ReactionAllergenDto> getReactionAllergens() {
        if (null == reactionAllergens) {
            reactionAllergens = new ArrayList<>();
        }
        return reactionAllergens;
    }

    public void setReactionAllergens(List<ReactionAllergenDto> reactionAllergens) {
        this.reactionAllergens = reactionAllergens;
    }
}
