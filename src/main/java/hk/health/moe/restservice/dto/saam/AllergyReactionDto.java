package hk.health.moe.restservice.dto.saam;

import java.io.Serializable;

public class AllergyReactionDto implements Serializable, Comparable<AllergyReactionDto> {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String reactionSeqNo;
    private String ehrValue;
    private String displayName;
    private String quickList;
    private Integer displayOrder;


    public String getReactionSeqNo() {
        return reactionSeqNo;
    }

    public void setReactionSeqNo(String reactionSeqNo) {
        this.reactionSeqNo = reactionSeqNo;
    }

    public String getEhrValue() {
        return ehrValue;
    }

    public void setEhrValue(String ehrValue) {
        this.ehrValue = ehrValue;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getQuickList() {
        return quickList;
    }

    public void setQuickList(String quickList) {
        this.quickList = quickList;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    @Override
    public int compareTo(AllergyReactionDto o) {
        AllergyReactionDto tmp = o;
        if(this.displayOrder < tmp.displayOrder)
        {
            /* instance lt received */
            return -1;
        }
        else if(this.displayOrder  > tmp.displayOrder)
        {
            /* instance gt received */
            return 1;
        }
        /* instance == received */
        return 0;
    }
}
