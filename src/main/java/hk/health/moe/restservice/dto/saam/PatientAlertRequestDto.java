package hk.health.moe.restservice.dto.saam;

import java.io.Serializable;
import java.util.Date;

public class PatientAlertRequestDto implements Serializable {

    private static final long serialVersionUID = 1L;
    private String alertSeqNo;
    private String alertCode;
    private String alertDesc;
    private String additionInfo;
    private String classifiedIndicator;
    private Date validFrom;
    private Date validTo;
    private String hospitalCode;
    private String sourceSystem;
    private Date createDtm;
    private String deleteReason;
    private String trxType;
    private String allowModifyVf;
    private String allowModifyVt;
    private int version;
    private String updateBy;
    private Date updateDtm;
    private String updateUser;
    private String updateUserId;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;

    public String getAlertSeqNo() {
        return alertSeqNo;
    }

    public void setAlertSeqNo(String alertSeqNo) {
        this.alertSeqNo = alertSeqNo;
    }

    public String getAlertCode() {
        return alertCode;
    }

    public void setAlertCode(String alertCode) {
        this.alertCode = alertCode;
    }

    public String getAlertDesc() {
        return alertDesc;
    }

    public void setAlertDesc(String alertDesc) {
        this.alertDesc = alertDesc;
    }

    public String getAdditionInfo() {
        return additionInfo;
    }

    public void setAdditionInfo(String additionInfo) {
        this.additionInfo = additionInfo;
    }

    public String getClassifiedIndicator() {
        return classifiedIndicator;
    }

    public void setClassifiedIndicator(String classifiedIndicator) {
        this.classifiedIndicator = classifiedIndicator;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public String getHospitalCode() {
        return hospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public Date getCreateDtm() {
        return createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    public String getDeleteReason() {
        return deleteReason;
    }

    public void setDeleteReason(String deleteReason) {
        this.deleteReason = deleteReason;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    public String getAllowModifyVf() {
        return allowModifyVf;
    }

    public void setAllowModifyVf(String allowModifyVf) {
        this.allowModifyVf = allowModifyVf;
    }

    public String getAllowModifyVt() {
        return allowModifyVt;
    }

    public void setAllowModifyVt(String allowModifyVt) {
        this.allowModifyVt = allowModifyVt;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDtm() {
        return updateDtm;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getUpdateHosp() {
        return updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    public String getUpdateRank() {
        return updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

}
