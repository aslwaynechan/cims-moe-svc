/*********************************************************************
* Copyright (c) eHR HK Limited 2013. 
* All Rights Reserved.
* 
*********************************************************************
* PROJECT NAME    :  eHR Adaptation
*
* MODULE NAME     :  Drug Allergy Checking (DAC)
*
* PROGRAM NAME    :  ehr-apps-drugchecking.war
*
* PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
*
**********************************************************************
* VERSION         :  1.01.001
* REF NO          :  
* DATE CREATED    :  30 Oct 2015
* 
*********************************************************************
*
* CHANGES         :
* REF NO           DATE        DETAIL
* ------           ----        ------
*
*********************************************************************
*/
package hk.health.moe.restservice.dto.dac;

import java.util.List;

public class AllergenGroupMaintenanceResponseDto {

	private List<AllergenGroupDto> allergenGroups;

	public List<AllergenGroupDto> getAllergenGroups() {
		return allergenGroups;
	}

	public void setAllergenGroups(List<AllergenGroupDto> allergenGroups) {
		this.allergenGroups = allergenGroups;
	}
}
