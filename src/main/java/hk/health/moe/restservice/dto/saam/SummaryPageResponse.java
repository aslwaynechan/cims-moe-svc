package hk.health.moe.restservice.dto.saam;

import java.io.Serializable;
import java.util.List;

/**
 * AlertDTO is a data transfer object which contains information of Patient's Alert.
 *
 * @author HAITS CE3
 */
public class SummaryPageResponse implements Serializable {

    private static final long serialVersionUID = 5553697343538770822L;

    private List<PatientAllergyDto> patientAllergyList;
    private List<PatientAdrDto> patientAdrList;
    private List<PatientAlertDto> patientAlertList;

    public List<PatientAllergyDto> getPatientAllergyList() {
        return patientAllergyList;
    }

    public void setPatientAllergyList(List<PatientAllergyDto> patientAllergyList) {
        this.patientAllergyList = patientAllergyList;
    }

    public List<PatientAdrDto> getPatientAdrList() {
        return patientAdrList;
    }

    public void setPatientAdrList(List<PatientAdrDto> patientAdrList) {
        this.patientAdrList = patientAdrList;
    }

    public List<PatientAlertDto> getPatientAlertList() {
        return patientAlertList;
    }

    public void setPatientAlertList(List<PatientAlertDto> patientAlertList) {
        this.patientAlertList = patientAlertList;
    }

}
