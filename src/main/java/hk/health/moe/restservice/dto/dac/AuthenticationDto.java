package hk.health.moe.restservice.dto.dac;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author Simon
 * @Date 2019/9/24
 */
public class AuthenticationDto {

    @NotBlank
    private String appName;
    @NotNull
    private DacAppDto dacApp;
    private String token;
    private String hospitalCode;
    private String version;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public DacAppDto getDacApp() {
        return dacApp;
    }

    public void setDacApp(DacAppDto dacApp) {
        this.dacApp = dacApp;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getHospitalCode() {
        return hospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
