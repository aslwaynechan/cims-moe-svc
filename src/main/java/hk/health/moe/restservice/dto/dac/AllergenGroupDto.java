package hk.health.moe.restservice.dto.dac;


import java.util.ArrayList;
import java.util.List;

public class AllergenGroupDto {
	
	private String allergenGroupDescription;
	private String allergenGroupRemark;
	private String allergenGroup;
	private Long rank;
	private List<ActiveIngredientDto> activeIngredientList = new ArrayList<ActiveIngredientDto>();
	
	public String getAllergenGroupDescription() {
		return allergenGroupDescription;
	}
	public void setAllergenGroupDescription(String allergenGroupDescription) {
		this.allergenGroupDescription = allergenGroupDescription;
	}
	public String getAllergenGroupRemark() {
		return allergenGroupRemark;
	}
	public void setAllergenGroupRemark(String allergenGroupRemark) {
		this.allergenGroupRemark = allergenGroupRemark;
	}
	public String getAllergenGroup() {
		return allergenGroup;
	}
	public void setAllergenGroup(String allergenGroup) {
		this.allergenGroup = allergenGroup;
	}
	public Long getRank() {
		return rank;
	}
	public void setRank(Long rank) {
		this.rank = rank;
	}
	public List<ActiveIngredientDto> getActiveIngredientList() {
		return activeIngredientList;
	}
	public void setActiveIngredientList(
			List<ActiveIngredientDto> activeIngredientList) {
		this.activeIngredientList = activeIngredientList;
	}
	
	@Override
	public String toString() {
		return "AllergenGroupDto [activeIngredientList=" + activeIngredientList
				+ ", allergenGroup=" + allergenGroup
				+ ", allergenGroupDescription=" + allergenGroupDescription
				+ ", allergenGroupRemark=" + allergenGroupRemark + ", rank="
				+ rank + "]";
	}

	
}
