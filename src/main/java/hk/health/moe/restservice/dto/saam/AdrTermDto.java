/*********************************************************************
* Copyright (c) eHR HK Limited 2013. 
* All Rights Reserved.
* 
*********************************************************************
* PROJECT NAME    :  eHR Adaptation
*
* MODULE NAME     :  Structured Alert Adaptation Module (SAAM)
*
* PROGRAM NAME    :  ehr-apps-sa-model.war
*
* PURPOSE         :  A data transfer object (DTO) used to transfer data in the application
*
**********************************************************************
* VERSION         :   1.01.001
* REF NO          :  
* DATE CREATED    :   18 Dec 2014
* 
*********************************************************************
*
* CHANGES         :
* REF NO           DATE        DETAIL
* ------           ----        ------
*
*********************************************************************
*/
package hk.health.moe.restservice.dto.saam;

import java.io.Serializable;

/**
 * AdrLogDTO is a data transfer object which log information of Patient's Adverse Drug Reaction.
 *
 * @author HAITS CE3
 */
public class AdrTermDto implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 5979397735164989072L;
	protected String adrSeqNo;
    protected int termId;
    protected String termDesc;

    
	/**
	 * @return the adrSeqNo
	 */
	public String getAdrSeqNo() {
		return adrSeqNo;
	}
	/**
	 * @param adrSeqNo the adrSeqNo to set
	 */
	public void setAdrSeqNo(String adrSeqNo) {
		this.adrSeqNo = adrSeqNo;
	}
	/**
	 * @return the termId
	 */
	public int getTermId() {
		return termId;
	}
	/**
	 * @param termId the termId to set
	 */
	public void setTermId(int termId) {
		this.termId = termId;
	}
	/**
	 * @return the termDesc
	 */
	public String getTermDesc() {
		return termDesc;
	}
	/**
	 * @param termDesc the termDesc to set
	 */
	public void setTermDesc(String termDesc) {
		this.termDesc = termDesc;
	}	            
}
