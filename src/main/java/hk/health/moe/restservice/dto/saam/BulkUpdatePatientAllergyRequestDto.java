package hk.health.moe.restservice.dto.saam;

import java.io.Serializable;

public class BulkUpdatePatientAllergyRequestDto /*extends RequestDto*/ implements Serializable {

    private static final long serialVersionUID = 1L;

    PatientAllergyRequestDto[] list;

    public PatientAllergyRequestDto[] getList() {
        return list;
    }
    public void setList(PatientAllergyRequestDto[] list) {
        this.list = list;
    }

}
