package hk.health.moe.restservice.dto.saam;

import java.io.Serializable;

public class ResponseDto implements Serializable {

	private static final long serialVersionUID = 1466503284000469308L;
	private String responseCode;
	private String responseDesc;
	private String responseAction;
	private String responseCause;
	private String[] buttonLabels;

	public ResponseDto(){super();}
	
	public String getResponseDesc() {
		return responseDesc;
	}

	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}

	public ResponseDto(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseAction() {
		return responseAction;
	}

	public void setResponseAction(String responseAction) {
		this.responseAction = responseAction;
	}

	public String getResponseCause() {
		return responseCause;
	}

	public void setResponseCause(String responseCause) {
		this.responseCause = responseCause;
	}

	public String[] getButtonLabels() {
		return (buttonLabels != null) ? buttonLabels.clone() : null;
	}

	public void setButtonLabels(String[] buttonLabels) {
		this.buttonLabels = (buttonLabels != null) ? buttonLabels.clone() : null;
	}
	
	
	
}
