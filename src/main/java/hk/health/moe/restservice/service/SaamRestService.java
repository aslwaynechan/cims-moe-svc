package hk.health.moe.restservice.service;

import hk.health.moe.restservice.dto.saam.AddPatientAlertResponse;
import hk.health.moe.restservice.dto.saam.PatientAlertRequestDto;
import hk.health.moe.restservice.dto.saam.PatientAllergyDto;
import hk.health.moe.restservice.dto.saam.PatientAllergyRequestDto;
import hk.health.moe.restservice.dto.saam.ResponseDto;
import hk.health.moe.restservice.dto.saam.SummaryPageResponse;

import java.util.List;

public interface SaamRestService {

    SummaryPageResponse getPatientSummaryByPatientRefKey(String refKey) throws Exception;

    AddPatientAlertResponse addPatientAlert(String refKey, PatientAlertRequestDto dto) throws Exception;

    ResponseDto deletePatientAlert(String alertSeqNo, String deleteReason, Long version) throws Exception;

    ResponseDto addPatientAllergy() throws Exception;

    //ResponseDto bulkUpdatePatientAllergy(List<PatientAllergyRequestDto> PatientAllergyDtoList) throws Exception;
    ResponseDto bulkUpdatePatientAllergy() throws Exception;

}


