package hk.health.moe.restservice.service.impl;

import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.exception.SaamServiceException;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.restservice.dto.saam.AddPatientAlertResponse;
import hk.health.moe.restservice.dto.saam.BulkUpdatePatientAllergyRequestDto;
import hk.health.moe.restservice.dto.saam.DeletePatientAlertRequestDto;
import hk.health.moe.restservice.dto.saam.PatientAlertRequestDto;
import hk.health.moe.restservice.dto.saam.PatientAllergyDto;
import hk.health.moe.restservice.dto.saam.PatientAllergyRequestDto;
import hk.health.moe.restservice.dto.saam.ResponseDto;
import hk.health.moe.restservice.dto.saam.SummaryPageResponse;
import hk.health.moe.restservice.service.SaamRestService;
import hk.health.moe.restservice.util.HttpEntityUtil;
import hk.health.moe.service.impl.BaseServiceImpl;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.SystemSettingUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeType;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("saamRestService")
public class SaamRestServiceImpl extends BaseServiceImpl implements SaamRestService {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${restTemplate.saam.patient.summary.url.get}")
    private String getPatientSummaryUrl;

    @Value("${restTemplate.saam.patient.alert.url.post}")
    private String addPatientAlertUrl;

    @Value("${restTemplate.saam.patient.alert.url.delete}")
    private String deletePatientAlertUrl;

    @Value("${restTemplate.saam.patient.allergy.url.post}")
    private String addPatientAllergyUrl;

    @Value("${restTemplate.saam.patient.allergy.url.put}")
    private String bulkUpdatePatientAllergyUrl;

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    @Override
    public SummaryPageResponse getPatientSummaryByPatientRefKey(String refKey) throws Exception {

        String[] msg;
        Map<String, Object> params = new HashMap<>();
        params.put("refKey", refKey);
        ResponseEntity<SummaryPageResponse> responseEntity = null;
        try {

            responseEntity = restTemplate.getForEntity(getPatientSummaryUrl, SummaryPageResponse.class, params);
            if (responseEntity != null && HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
            //eric 20191224 start--
//            throw new SaamServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.summary.get.noCause"));
            throw new SaamServiceException(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_SUMMARY_GET_NOCAUSE.getResponseCode(),
                    ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_SUMMARY_GET_NOCAUSE.getResponseMessage());
            //eric 20191224 end--
        } catch (ResourceAccessException e) {
            if (e.getCause() != null) {
                msg = new String[]{e.getCause().getMessage()};
            } else {
                msg = new String[]{e.getMessage()};
            }
            //eric 20191224 start--
//            throw new SaamServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.summary.get.withCause", msg));
            String message =String.join(",",msg);
            throw new SaamServiceException(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_SUMMARY_GET_WITHCAUSE.getResponseCode(),message);
            //eric 20191224 end--
        } catch (HttpClientErrorException e) {
            msg = new String[]{String.valueOf(e.getRawStatusCode())};
            //eric 20191224 start--
//            throw new SaamServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.summary.get.withCause", msg));
            String message =String.join(",",msg);
            throw new SaamServiceException(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_SUMMARY_GET_WITHCAUSE.getResponseCode(),
                    message);
            //eric 20191224 end--
        }
    }

    @Override
    public AddPatientAlertResponse addPatientAlert(String refKey, PatientAlertRequestDto patientAlertRequestDto) throws Exception {

        String[] msg;
        Map<String, Object> params = new HashMap<>();
        params.put("refKey", refKey);
        ResponseEntity<AddPatientAlertResponse> responseEntity;
        try {
            responseEntity = restTemplate.postForEntity(addPatientAlertUrl, patientAlertRequestDto, AddPatientAlertResponse.class, params);
            ResponseDto responseDto = responseEntity.getBody().getResponseDto();
            if (responseEntity != null && HttpStatus.OK.equals(responseEntity.getStatusCode()) && responseDto != null) {
                if (ServerConstant.ALERT_SUCCESS_ERRCODE.equalsIgnoreCase(responseDto.getResponseCode())) {
                    return responseEntity.getBody();
                } else {
                    //eric 20191223 start--
//                    throw new SaamServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.alert.post.withCause", handleErrResponse(responseDto)));
                    throw new SaamServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.alert.post.withCause", handleErrResponse(responseDto)));
                    //eric 20191223 end--
                }
            }
            //eric 20191223 start--
//            throw new SaamServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.alert.post.noCause"));
            throw new SaamServiceException(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALERT_POST_NOCAUSE.getResponseCode(),
                    ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALERT_POST_NOCAUSE.getResponseMessage());
            //eric 20191223 end--
        } catch (ResourceAccessException e) {
            if (e.getCause() != null) {
                msg = new String[]{e.getCause().getMessage()};
            } else {
                msg = new String[]{e.getMessage()};
            }

            throw new SaamServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.alert.post.withCause", msg));
        } catch (HttpClientErrorException e) {
            msg = new String[]{String.valueOf(e.getRawStatusCode())};
            throw new SaamServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.alert.post.withCause", msg));
        }
    }

    @Override
    public ResponseDto deletePatientAlert(String alertSeqNo, String deleteReason, Long version) throws Exception {
        DeletePatientAlertRequestDto alertDto = new DeletePatientAlertRequestDto();
        alertDto.setAlertSeqNo(alertSeqNo);
        alertDto.setDeleteReason(deleteReason);
        UserDto dto = getUserDto();
        String refKey = dto.getMrnPatientIdentity();
        alertDto.setUpdateUserId(dto.getLoginId());
        alertDto.setUpdateUser(dto.getLoginName());
        alertDto.setUpdateRank(dto.getUserRankCd());
        alertDto.setUpdateRankDesc(dto.getUserRankDesc());
        alertDto.setUpdateHosp(dto.getHospitalCd());
        alertDto.setVersion(version);
        String sourceSystem = SystemSettingUtil.getParamValue(ServerConstant.PARAM_NAME_SAAM_SOURCE_SYSTEM);
        if (sourceSystem == null) {
            sourceSystem = ServerConstant.APP_ID;
        }
        alertDto.setSourceSystem(sourceSystem);
        Map<String, Object> params = new HashMap<>();
        params.put("refKey", refKey);
        ResponseEntity<ResponseDto> responseEntity;
        responseEntity = restTemplate.exchange(deletePatientAlertUrl,
                HttpMethod.PUT, HttpEntityUtil.genHttpEntity(alertDto, null),
                ResponseDto.class, params);
        ResponseDto responseDto = null;
        if (responseEntity != null && responseEntity.getBody() != null) {
            responseDto = responseEntity.getBody();
        } else {
            //eric 20191225 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.alert.delete.noCause"));
            throw new MoeServiceException(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALERT_DELETE_NOCAUSE.getResponseCode(),
                    ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALERT_DELETE_NOCAUSE.getResponseMessage());
            //eric 20191225 end--
        }
        if (ServerConstant.ALERT_SUCCESS_ERRCODE.equalsIgnoreCase(responseDto.getResponseCode())) {
            return responseDto;
        } else if (ServerConstant.PATIENT_NOT_FOUND_ERRCODE.equalsIgnoreCase(responseDto.getResponseCode())) {
            //eric 20191225 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.notExist"));
            throw new MoeServiceException(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_NOEXIST.getResponseCode(),
                    ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_NOEXIST.getResponseMessage());
            //eric 20191225 end--
        } else if (ServerConstant.RECORD_NOT_FOUND.equalsIgnoreCase(responseDto.getResponseCode())) {
            //eric 20191225 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("records.concurrentError"));
            throw new MoeServiceException(ResponseCode.SpecialMessage.RECORDS_CONCURRENT_ERRORS.getResponseCode(),
                    ResponseCode.SpecialMessage.RECORDS_CONCURRENT_ERRORS.getResponseMessage());
            //eric 20191225 end--
        } else {
            //eric 20191225 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.alert.delete.noCause"));
            throw new MoeServiceException(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALERT_DELETE_NOCAUSE.getResponseCode(),
                    ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALERT_DELETE_NOCAUSE.getResponseMessage());
            //eric 20191225 end--
        }

    }

    @Override
    public ResponseDto addPatientAllergy() throws Exception {
        UserDto userDto = getUserDto();
        String refKey = userDto.getMrnPatientIdentity();
        String missingPara = "";
        if (StringUtils.isBlank(refKey)) {
            missingPara = "refKey";
        }
        if (StringUtils.isBlank(userDto.getHospitalCd())) {
            missingPara = ((missingPara.equalsIgnoreCase("")) ? "hospitalCode" : missingPara + ", hospitalCode");
        }
        if (StringUtils.isBlank(userDto.getLoginId())) {
            missingPara = ((missingPara.equalsIgnoreCase("")) ? "userId" : missingPara + ", userId");
        }

        if (!StringUtils.isBlank(missingPara)) {
            //eric 20191223 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.token.illegal"));
            throw new MoeServiceException(ResponseCode.CommonMessage.TOKEN_EXPIRE_ERROR.getResponseCode(),ResponseCode.CommonMessage.TOKEN_EXPIRE_ERROR.getResponseMessage());
            //eric 20191223 end--
        }

        String sourceSystem = userDto.getSourceSystem();
        if (StringUtils.isBlank(sourceSystem)) {
            sourceSystem = ServerConstant.APP_ID;
        }

        PatientAllergyRequestDto requestDto = new PatientAllergyRequestDto();
        requestDto.setHospitalCode(userDto.getHospitalCd());
        requestDto.setSourceSystem(sourceSystem);
//        requestDto.setUserId(userId);
//        requestDto.setUser(user);
//        requestDto.setUserHosp(hospitalCode);
//        requestDto.setUserRank(userRank);
//        requestDto.setUserRankDesc(userRankDesc);
//        requestDto.setUpdateBy(userId);
        requestDto.setUpdateUser(userDto.getLoginName());
        requestDto.setUpdateUserId(userDto.getLoginId());
        requestDto.setUpdateRank(userDto.getUserRankCd());
        requestDto.setUpdateRankDesc(userDto.getUserRankDesc());
        requestDto.setUpdateHosp(userDto.getHospitalCd());
        requestDto.setVersion(0);
        //only for NKDA
        requestDto.setAllergenType(hk.health.moe.restservice.common.ServerConstant.ALLERGEN_TYPE_NO_KNOWN_DRUG_ALLERGY);
        requestDto.setDisplayName(hk.health.moe.restservice.common.ServerConstant.NO_KNOWN_DRUG_ALLERGY);
        ResponseEntity<ResponseDto> responseEntity;
        Map<String, Object> params = new HashMap<>();
        params.put("refKey", refKey);
        responseEntity = restTemplate.postForEntity(addPatientAllergyUrl, requestDto, ResponseDto.class, params);
        ResponseDto responseDto = null;
        if (responseEntity == null || responseEntity.getBody() == null) {
            //eric 20191223 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.allergy.add"));
            throw new MoeServiceException(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_ALLERGY_ADD.getResponseCode(),
                    ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_ALLERGY_ADD.getResponseMessage());
            //eric 20191223 end--
        }
        responseDto = responseEntity.getBody();
        if (ServerConstant.ALERT_SUCCESS_ERRCODE.equalsIgnoreCase(responseDto.getResponseCode())) {
            return responseDto;
        } else if (ServerConstant.PATIENT_NOT_FOUND_ERRCODE.equalsIgnoreCase(responseDto.getResponseCode())) {
            //eric 20191223 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.notExist"));
            throw new MoeServiceException(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_NOEXIST.getResponseCode(),
                    ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_NOEXIST.getResponseMessage());
            //eric 20191223 end--
        } else if (ServerConstant.PATIENT_ALLERGY_ADD_DUPLICATE_ERRCODE.equalsIgnoreCase(responseDto.getResponseCode())) {
            String[] msg = new String[]{requestDto.getDisplayName()};
            //eric 20191223 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.allergy.duplicate", msg));
            throw new MoeServiceException(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_ALLERGY_DUPLICATE.getResponseCode(),
                    localeMessageSourceUtil.getMessage(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_ALLERGY_DUPLICATE.getResponseMessage(), msg));
            //eric 20191224 end--
        } else {
            //eric 20191223 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.allergy.add"));
            throw new MoeServiceException(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_ALLERGY_ADD.getResponseCode(),
                    ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_ALLERGY_ADD.getResponseMessage());
            //eric 20191223 end--
        }


    }

    private String[] handleErrResponse(ResponseDto responseDto) {

        String[] msg = new String[4];
        msg[0] = responseDto.getResponseDesc();
        if (StringUtils.isNotBlank(responseDto.getResponseCause())) {
            msg[1] = responseDto.getResponseAction();
        } else {
            msg[1] = "";
        }
        if (StringUtils.isNotBlank(responseDto.getResponseAction())) {
            msg[2] = responseDto.getResponseAction();
        } else {
            msg[2] = "";
        }
        if (StringUtils.isNotBlank(responseDto.getResponseCause())) {
            msg[3] = responseDto.getResponseCause();
        } else {
            msg[3] = "";
        }

        return msg;
    }

    /*@Override
    public ResponseDto bulkUpdatePatientAllergy(List<PatientAllergyRequestDto> PatientAllergyRequestDtoList) throws Exception {

        UserDto userDto = getUserDto();

        if (PatientAllergyRequestDtoList != null && PatientAllergyRequestDtoList.size() > 0) {

            PatientAllergyRequestDto[] patientAllergyRequestDtoArray = new PatientAllergyRequestDto[PatientAllergyRequestDtoList.size()];

            int index = 0;
            for (PatientAllergyRequestDto patientAllergyRequestDto : PatientAllergyRequestDtoList) {

                patientAllergyRequestDto.setUpdateUser(userDto.getLoginName());         //updateUser
                patientAllergyRequestDto.setUpdateUserId(userDto.getLoginId());         //updateUserId
                patientAllergyRequestDto.setUpdateRank(userDto.getUserRankCd());        //updateRank
                patientAllergyRequestDto.setUpdateRankDesc(userDto.getUserRankDesc());  //updateRankDesc
                patientAllergyRequestDto.setUpdateHosp(userDto.getHospitalCd());        //updateHosp

                String sourceSystem = SystemSettingUtil.getParamValue(ServerConstant.PARAM_NAME_SAAM_SOURCE_SYSTEM);
                patientAllergyRequestDto.setSourceSystem(sourceSystem != null ? sourceSystem : ServerConstant.APP_ID);  //sourceSystem

                patientAllergyRequestDtoArray[index] = patientAllergyRequestDto;
                index++;

            }

            //Get RefKey
            String refKey = userDto.getMrnPatientIdentity();
            Map<String, Object> params = new HashMap<>();
            params.put("refKey", refKey);
            String[] msg;

            ResponseEntity<ResponseDto> responseEntity;
            try {
                BulkUpdatePatientAllergyRequestDto RequestDto = new BulkUpdatePatientAllergyRequestDto();
                RequestDto.setList(patientAllergyRequestDtoArray);

                //Set HttpEntity
                HttpEntity httpEntity = HttpEntityUtil.genHttpEntity(RequestDto, null);

                responseEntity = restTemplate.exchange(bulkUpdatePatientAllergyUrl, HttpMethod.PUT, httpEntity, ResponseDto.class, params);

                ResponseDto responseDto = responseEntity.getBody();
                if (responseEntity != null && HttpStatus.OK.equals(responseEntity.getStatusCode()) && responseDto != null) {
                    if (ServerConstant.ALERT_SUCCESS_ERRCODE.equalsIgnoreCase(responseDto.getResponseCode())) {
                        return responseEntity.getBody();
                    } else {
                        throw new SaamServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.allergy.put.withCause", handleErrResponse(responseDto)));
                    }
                }
                throw new SaamServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.allergy.put.noCause"));
            } catch (ResourceAccessException e) {
                if (e.getCause() != null) {
                    msg = new String[]{e.getCause().getMessage()};
                } else {
                    msg = new String[]{e.getMessage()};
                }
                throw new SaamServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.allergy.put.withCause", msg));
            } catch (HttpClientErrorException e) {
                msg = new String[]{String.valueOf(e.getRawStatusCode())};
                throw new SaamServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.allergy.put.withCause", msg));
            }
        }
        //No Parameter
        throw new SaamServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.allergy.put.noCause"));
    }*/


    @Override
    public ResponseDto bulkUpdatePatientAllergy() throws Exception {

        //Get UserDto from Token
        UserDto userDto = getUserDto();

        //Get RefKey
        String refKey = userDto.getMrnPatientIdentity();

        //Get Patient Summary From SAAM
        SummaryPageResponse summaryPageResponse = this.getPatientSummaryByPatientRefKey(refKey);

        if (summaryPageResponse != null && summaryPageResponse.getPatientAllergyList() != null && summaryPageResponse.getPatientAllergyList().size() > 0) {

            //Patient Allergy Dto List
            List<PatientAllergyDto> patientAllergyDtoList = summaryPageResponse.getPatientAllergyList();

            PatientAllergyRequestDto[] patientAllergyRequestDtoArray = new PatientAllergyRequestDto[patientAllergyDtoList.size()];

//            convertPatientAllergyRequestDto(patientAllergyDtoList);

            int index = 0;
            for (PatientAllergyDto patientAllergyDto : patientAllergyDtoList) {

                PatientAllergyRequestDto patientAllergyRequestDto = new PatientAllergyRequestDto();

                //Convert [PatientAllergyDto] to [PatientAllergyRequestDto]
                patientAllergyRequestDto.setAllergySeqNo(patientAllergyDto.getAllergySeqNo());      //allergySeqNo
                patientAllergyRequestDto.setDisplayName(patientAllergyDto.getDisplayName());        //displayName
                patientAllergyRequestDto.setAllergenType(patientAllergyDto.getAllergenType());      //allergenType
                patientAllergyRequestDto.setAllergenDto(patientAllergyDto.getAllergenDto());        //allergenDto
                patientAllergyRequestDto.setAllergenCode(patientAllergyDto.getAllergenCode());      //allergenCode
                patientAllergyRequestDto.setCertainty(patientAllergyDto.getCertainty());            //certainty
                patientAllergyRequestDto.setAdditionInfo(patientAllergyDto.getAdditionInfo());      //additionInfo
                patientAllergyRequestDto.setHospitalCode(patientAllergyDto.getHospitalCode());      //hospitalCode
                patientAllergyRequestDto.setDeleteReason(patientAllergyDto.getDeleteReason());      //deleteReason
                patientAllergyRequestDto.setVersion(patientAllergyDto.getVersion());                //version
                patientAllergyRequestDto.setAllergyReactionDtoList(patientAllergyDto.getAllergyReactionDtoList());  //allergyReactionDtoList

                //Set Last Update's Info
                patientAllergyRequestDto.setUpdateUser(userDto.getLoginName());         //updateUser
                patientAllergyRequestDto.setUpdateUserId(userDto.getLoginId());         //updateUserId
                patientAllergyRequestDto.setUpdateRank(userDto.getUserRankCd());        //updateRank
                patientAllergyRequestDto.setUpdateRankDesc(userDto.getUserRankDesc());  //updateRankDesc
                patientAllergyRequestDto.setUpdateHosp(userDto.getHospitalCd());        //updateHosp

                String sourceSystem = SystemSettingUtil.getParamValue(ServerConstant.PARAM_NAME_SAAM_SOURCE_SYSTEM);
                patientAllergyRequestDto.setSourceSystem(sourceSystem != null ? sourceSystem : ServerConstant.APP_ID);     //sourceSystem

                patientAllergyRequestDtoArray[index] = patientAllergyRequestDto;
                index++;
            }

            //[Update Patient Allergy in Bulk]'s Request Dto
            BulkUpdatePatientAllergyRequestDto RequestDto = new BulkUpdatePatientAllergyRequestDto();
            RequestDto.setList(patientAllergyRequestDtoArray);

            Map<String, Object> params = new HashMap<>();
            params.put("refKey", refKey);
            //Error Msg
            String[] msg;

            ResponseEntity<ResponseDto> responseEntity;
            try {
                //Set HttpEntity
                HttpEntity httpEntity = HttpEntityUtil.genHttpEntity(RequestDto, null);

                responseEntity = restTemplate.exchange(bulkUpdatePatientAllergyUrl, HttpMethod.PUT, httpEntity, ResponseDto.class, params);

                ResponseDto responseDto = responseEntity.getBody();
                if (responseEntity != null && HttpStatus.OK.equals(responseEntity.getStatusCode()) && responseDto != null) {
                    if (ServerConstant.ALERT_SUCCESS_ERRCODE.equalsIgnoreCase(responseDto.getResponseCode())) {
                        return responseEntity.getBody();
                    } else {
                        //eric 20191224 start--
//                        throw new SaamServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.allergy.put.withCause", handleErrResponse(responseDto)));
                        throw new SaamServiceException(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALLERGY_PUT_WITHCAUSE.getResponseCode()
                                ,localeMessageSourceUtil.getMessage(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALLERGY_PUT_WITHCAUSE.getResponseMessage(), handleErrResponse(responseDto)));
                        //eric 20191224 end--
                    }
                }
                //eric 20191224 start--
//                throw new SaamServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.allergy.put.noCause"));
                throw new SaamServiceException(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALLERGY_PUT_NOCAUSE.getResponseCode(),
                        ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALLERGY_PUT_NOCAUSE.getResponseMessage());
                //eric 20191224 end--
            } catch (ResourceAccessException e) {
                if (e.getCause() != null) {
                    msg = new String[]{e.getCause().getMessage()};
                } else {
                    msg = new String[]{e.getMessage()};
                }
                //eric 20191224 start--
//                throw new SaamServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.allergy.put.withCause", msg));
                throw new SaamServiceException(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALLERGY_PUT_WITHCAUSE.getResponseCode(),
                        localeMessageSourceUtil.getMessage(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALLERGY_PUT_WITHCAUSE.getResponseMessage(), msg));
                //eric 20191224 end--
            } catch (HttpClientErrorException e) {
                msg = new String[]{String.valueOf(e.getRawStatusCode())};
                //eric 20191224 start--
//                throw new SaamServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.allergy.put.withCause", msg));
                throw new SaamServiceException(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALLERGY_PUT_WITHCAUSE.getResponseCode(),
                        localeMessageSourceUtil.getMessage(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALLERGY_PUT_WITHCAUSE.getResponseMessage(), msg));
                //eric 20191224 end--
            }
        }
        //No Parameter
        //eric 20191224 start--
//        throw new SaamServiceException(localeMessageSourceUtil.getMessage("system.restService.saam.error.patient.allergy.put.noCause"));
        throw new SaamServiceException(ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALLERGY_PUT_NOCAUSE.getResponseCode(),
                ResponseCode.SpecialMessage.SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALLERGY_PUT_NOCAUSE.getResponseMessage());
        //eric 20191224 end--
    }


}
