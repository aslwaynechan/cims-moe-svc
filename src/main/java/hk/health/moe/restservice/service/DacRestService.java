package hk.health.moe.restservice.service;

import hk.health.moe.pojo.vo.CimsResponseVo;
import hk.health.moe.restservice.dto.dac.AuthenticationDto;
import hk.health.moe.restservice.dto.dac.ListMdsEnquiriesRequestDto;
import hk.health.moe.restservice.dto.dac.MDSEnquiryResponseDto;
import hk.health.moe.restservice.dto.dac.MedicationDecisionRequestDto;
import hk.health.moe.restservice.dto.dac.MedicationDecisionResponseDto;

public interface DacRestService {

    CimsResponseVo<MedicationDecisionResponseDto> medicationDecision(MedicationDecisionRequestDto dto/*, String dacToken*/) throws Exception;

    CimsResponseVo<AuthenticationDto> login(AuthenticationDto dto) throws Exception;

    /// Simon 20191012 start--
    CimsResponseVo<MDSEnquiryResponseDto> listMDSEnquiries(ListMdsEnquiriesRequestDto dto) throws  Exception;
    //Simon 20191012 end--
}
