package hk.health.moe.restservice.service.impl;

import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.DacServiceException;
import hk.health.moe.pojo.po.MoeAppPo;
import hk.health.moe.pojo.vo.CimsResponseVo;
import hk.health.moe.repository.MoeAppRepository;
import hk.health.moe.restservice.dto.dac.AuthenticationDto;
import hk.health.moe.restservice.dto.dac.ListMdsEnquiriesRequestDto;
import hk.health.moe.restservice.dto.dac.MDSEnquiryResponseDto;
import hk.health.moe.restservice.dto.dac.MedicationDecisionRequestDto;
import hk.health.moe.restservice.dto.dac.MedicationDecisionResponseDto;
import hk.health.moe.restservice.service.DacRestService;
import hk.health.moe.restservice.util.HttpEntityUtil;
import hk.health.moe.util.LocaleMessageSourceUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service("dacRestService")
public class DacRestServiceImpl implements DacRestService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    @Value("${restTemplate.dac.medicationDecision.url}")
    private String medicationDecisionUrl;

    @Value("${restTemplate.dac.login.url}")
    private String loginUrl;

    // Simon 20191012 start--
    @Value("${restTemplate.dac.listMDSEnquiries.url}")
    private String getMdsContentUrl;
    //Simon 20191012 end--

    @Autowired
    private MoeAppRepository moeAppRepository;





    @Override
    public CimsResponseVo<MedicationDecisionResponseDto> medicationDecision(MedicationDecisionRequestDto dto/*, String dacToken*/) throws Exception {
        String[] msg;
        CimsResponseVo<MedicationDecisionResponseDto> dacResponse;
        try {
            // Simon 20191029 start--
            //dacResponse = restTemplate.postForObject(medicationDecisionUrl, setHeaders(dto, ""), CimsResponseVo.class);

            dacResponse = restTemplate.postForObject(medicationDecisionUrl, HttpEntityUtil.genHttpEntity(dto,HttpEntityUtil.genMultiParamsHeader(getDacLoginMap())), CimsResponseVo.class);
            //Simon 20191029 end--
            //if (dacResponse.getRespCode() == ServerConstant.SUCCESS_RESULT_CODE) {
            if (dacResponse.getRespCode() == ResponseCode.CommonMessage.SUCCESS.getResponseCode()) {
                return dacResponse;
            } else {
                String errMsg = dacResponse.getErrMsg();
                Integer respCode = dacResponse.getRespCode();
                if (StringUtils.isBlank(errMsg)) {
                    //errMsg = localeMessageSourceUtil.getMessage("system.restService.dac.error.allergyCheck.noCause");
                    errMsg = localeMessageSourceUtil.getMessage("system.restService.dac.error.allergyCheck.noCause");
                    throw new DacServiceException(ResponseCode.AllergyChecking.ALLERGY_CHECKING_ERROR_WITHOUT_CAUSE.getResponseCode());
                } else {
                    msg = new String[]{errMsg};
                    //errMsg = localeMessageSourceUtil.getMessage("system.restService.dac.error.allergyCheck.withCause", msg);
                    errMsg = localeMessageSourceUtil.getMessage("system.restService.dac.error.allergyCheck.withCause", msg);
                    throw new DacServiceException(ResponseCode.AllergyChecking.ALLERGY_CHECKING_ERROR_WITH_CAUSE.getResponseCode(), errMsg);
                }

                //throw new DacServiceException(errMsg);
            }
        } catch (ResourceAccessException e) {
            if (e.getCause() != null) {
                msg = new String[]{e.getCause().getMessage()};
            } else {
                msg = new String[]{e.getMessage()};
            }

            //throw new DacServiceException(localeMessageSourceUtil.getMessage("system.restService.dac.error.allergyCheck.withCause", msg));
            throw new DacServiceException(ResponseCode.AllergyChecking.RESOURCE_ACCESS_DENIED_ERROR.getResponseCode(),
                    localeMessageSourceUtil.getMessage("system.restService.dac.error.allergyCheck.withCause", msg));
        } catch (HttpClientErrorException e) {
            msg = new String[]{String.valueOf(e.getRawStatusCode())};
            //throw new DacServiceException(localeMessageSourceUtil.getMessage("system.restService.dac.error.allergyCheck.withCause", msg));
            throw new DacServiceException(ResponseCode.ListMDSEnquiries.SYSTEM_ERROR.getResponseCode(),
                    localeMessageSourceUtil.getMessage("system.restService.dac.error.allergyCheck.withCause", msg));
        }
    }

    @Override
    public CimsResponseVo<AuthenticationDto> login(AuthenticationDto requestDto) throws Exception {
        String[] msg;
        CimsResponseVo<AuthenticationDto> dacResponse;

        try {
            dacResponse = restTemplate.postForObject(loginUrl, requestDto, CimsResponseVo.class);
            if (dacResponse.getRespCode() == ServerConstant.SUCCESS_RESULT_CODE) {
                return dacResponse;
            } else {
                String errMsg = dacResponse.getErrMsg();
                if (StringUtils.isBlank(errMsg)) {
                    errMsg = localeMessageSourceUtil.getMessage("system.restService.dac.error.allergyCheck.noCause");
                } else {
                    msg = new String[]{errMsg};
                    errMsg = localeMessageSourceUtil.getMessage("system.restService.dac.error.allergyCheck.withCause", msg);
                }

                throw new DacServiceException(errMsg);
            }
        } catch (ResourceAccessException e) {
            if (e.getCause() != null) {
                msg = new String[]{e.getCause().getMessage()};
            } else {
                msg = new String[]{e.getMessage()};
            }

            throw new DacServiceException(localeMessageSourceUtil.getMessage("system.restService.dac.error.allergyCheck.withCause", msg));
        } catch (HttpClientErrorException e) {
            msg = new String[]{String.valueOf(e.getRawStatusCode())};
            throw new DacServiceException(localeMessageSourceUtil.getMessage("system.restService.dac.error.allergyCheck.withCause", msg));
        }
    }

//    private HttpEntity<Object> setHeaders(Object data, String token) throws Exception {
//        HttpHeaders headers = new HttpHeaders();
//        headers.add("Authorization", token);
//        headers.add("Username",userName);
//        headers.add("Password",password);
//        HttpEntity<Object> request = new HttpEntity<>(data, headers);
//
//        return request;
//    }

    // Simon 20191012 start--
    @Override
    public CimsResponseVo<MDSEnquiryResponseDto> listMDSEnquiries(ListMdsEnquiriesRequestDto dto) throws  Exception{
        String[] msg;
        CimsResponseVo<MDSEnquiryResponseDto> dacResponse;
        try{
            //dacResponse = restTemplate.getForObject(getMdsContentUrl, setHeaders(null,""),CimsResponseVo.class);
            ResponseEntity<CimsResponseVo> exchange = restTemplate.exchange(getMdsContentUrl, HttpMethod.GET, HttpEntityUtil.genHttpEntity(null,HttpEntityUtil.genMultiParamsHeader(getDacLoginMap())), CimsResponseVo.class);
            dacResponse = exchange.getBody();
            //if (dacResponse.getRespCode() == ServerConstant.SUCCESS_RESULT_CODE) {    //0
            if (dacResponse.getRespCode() == ResponseCode.CommonMessage.SUCCESS.getResponseCode()) {
                return dacResponse;
            } else {
                String errMsg = dacResponse.getErrMsg();
                if (StringUtils.isBlank(errMsg)) {
                    errMsg = localeMessageSourceUtil.getMessage("system.restService.dac.error.getMds.noCause");
                    throw new DacServiceException(ResponseCode.ListMDSEnquiries.GET_MDS_ERROR_WITHOUT_CAUSE.getResponseCode(),errMsg);
                } else {
                    msg = new String[]{errMsg};
                    errMsg = localeMessageSourceUtil.getMessage("system.restService.dac.error.getMds.withCause", msg);
                    throw new DacServiceException(ResponseCode.ListMDSEnquiries.GET_MDS_ERROR_WITH_CAUSE.getResponseCode(), errMsg);
                }

                //throw new DacServiceException(errMsg);
            }
        }catch (ResourceAccessException e) {
            if (e.getCause() != null) {
                msg = new String[]{e.getCause().getMessage()};
            } else {
                msg = new String[]{e.getMessage()};
            }

            //throw new DacServiceException(localeMessageSourceUtil.getMessage("system.restService.dac.error.getMds.withCause", msg));
            throw new DacServiceException(ResponseCode.ListMDSEnquiries.RESOURCE_ACCESS_DENIED_ERROR.getResponseCode(),
                    localeMessageSourceUtil.getMessage("system.restService.dac.error.getMds.withCause", msg));
        } catch (HttpClientErrorException e) {
            msg = new String[]{String.valueOf(e.getRawStatusCode())};
            //throw new DacServiceException(localeMessageSourceUtil.getMessage("system.restService.dac.error.getMds.withCause", msg));
            throw new DacServiceException(ResponseCode.ListMDSEnquiries.SYSTEM_ERROR.getResponseCode(),
                    localeMessageSourceUtil.getMessage("system.restService.dac.error.getMds.withCause", msg));
        }
    }
    //Simon 20191012 end--

    private Map<String,Object> getDacLoginMap(){
        Map<String,Object> map = new LinkedHashMap<>();

        List<MoeAppPo> byAppName = moeAppRepository.findByAppName(ServerConstant.DAC_LOGIN_USER);
        if (byAppName != null && byAppName.size() > 0){
            map.put("Username",byAppName.get(0).getAppName());
            map.put("Password",byAppName.get(0).getPassword());
        }
        return map;
    }
}
