package hk.health.moe.controller;

/**************************************************************************
 * NAME        : CodeListController.java
 * VERSION     : 1.0.0
 * DATE        : 03-JUL-2019
 * DESCRIPTION : Api for the code list service
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       03-JUL-2019  Initial Version
 * Ricci Liao       04-JUL-2019  Support to list code-list in prescript page
 **************************************************************************/

import hk.health.moe.common.ResponseCode;
import hk.health.moe.pojo.dto.MoeHospitalSettingDto;
import hk.health.moe.pojo.dto.inner.InnerListCodeListDto;
import hk.health.moe.pojo.vo.CimsResponseVo;
import hk.health.moe.service.CodeListService;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@Api(tags = "CodeList")
@RestController
public class CodeListController {

    @Autowired
    private CodeListService codeListService;

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    // Ricci 20190704 start --
    @ApiOperation(value = "List Code Table", notes = "Show Code Table`s Info")
    @PostMapping("/listCodeList")
    public CimsResponseVo listCodeList(@Validated @RequestBody InnerListCodeListDto listCodeListDto, BindingResult bindingResult) throws Exception {

        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        } else {
            Map<String, Object> data = codeListService.listCodeList(listCodeListDto.getCodeType());
            vo = ResponseUtil.initSuccessResultVO(data);
        }

        return vo;
    }
    // Ricci 20190704 end --

    // Ricci 20190819 start --
    @ApiOperation(value = "Get Current Hospital Setting", notes = "By HospCode and UserSpecialty")
    @PostMapping("/getHospitalSetting")
    public CimsResponseVo getHospitalSetting(@Validated @RequestBody MoeHospitalSettingDto dto, BindingResult bindingResult) throws Exception {
        CimsResponseVo vo = null;

        if (bindingResult.hasErrors()) {
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        } else {
            Map<String, MoeHospitalSettingDto> data = codeListService.getHospitalSetting(dto.getHospcode(), dto.getUserSpecialty());
            vo = ResponseUtil.initSuccessResultVO(data);
        }

        return vo;
    }
    // Ricci 20190819 end --

}
