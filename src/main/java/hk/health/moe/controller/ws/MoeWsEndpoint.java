package hk.health.moe.controller.ws;

import hk.health.moe.common.ServerConstant;
import hk.health.moe.common.WebServiceConstant;
import hk.health.moe.exception.MoeWebServiceException;
import hk.health.moe.pojo.dto.webservice.DeleteOrderDetailRequest;
import hk.health.moe.pojo.dto.webservice.DeleteOrderDetailResponse;
import hk.health.moe.pojo.dto.webservice.ObjectFactory;
import hk.health.moe.pojo.dto.webservice.OrderDetailRequest;
import hk.health.moe.pojo.dto.webservice.OrderListRequest;
import hk.health.moe.pojo.dto.webservice.OrderListResponse;
import hk.health.moe.pojo.dto.webservice.PatientOrderLogByPeriodRequest;
import hk.health.moe.pojo.dto.webservice.PrescriptionDto;
import hk.health.moe.pojo.dto.webservice.PrescriptionRequest;
import hk.health.moe.pojo.dto.webservice.PrescriptionResponse;
import hk.health.moe.pojo.dto.webservice.UserDto;
import hk.health.moe.security.WsMethodSecurityMetadataSource;
import hk.health.moe.service.MoeDrugService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.bind.JAXBElement;

/**
 * @Author Simon
 * @Date 2019/11/6
 */
@Endpoint
public class MoeWsEndpoint {

    @Autowired
    private MoeDrugService moeDrugService;

    @Autowired
    private WsMethodSecurityMetadataSource wsMethodSecurityMetadataSource;

    private ObjectFactory objectFactory;

    public MoeWsEndpoint() {
        objectFactory = new ObjectFactory();
    }

    private final static Logger logger = LoggerFactory.getLogger(MoeWsEndpoint.class);


    @PayloadRoot(localPart = "getPatientPrescriptionByOrderNoRequest", namespace = "http://ehr.gov.hk/moe/ws/beans")
    @ResponsePayload
    public JAXBElement<PrescriptionResponse> getPatientPrescriptionByOrderNo(@RequestPayload JAXBElement<PrescriptionRequest> req) throws Exception {

        isAuthorized("getPatientPrescriptionByOrderNo", MoeDrugService.class);

        Long orderNo = req.getValue().getOrderNo();
        if (orderNo == null || orderNo <= 0) {
            throw new MoeWebServiceException(WebServiceConstant.WS_MISSING_ORDER_NUMBER_ERR_CODE, WebServiceConstant.WS_MISSING_ORDER_NUMBER_ERR_MSG);
        } else if (StringUtils.isBlank(req.getValue().getHospCode())) {
            throw new MoeWebServiceException(WebServiceConstant.WS_MISSING_HOSPCODE_ERR_CODE, WebServiceConstant.WS_MISSING_HOSPCODE_ERR_MSG);
        }
        try {
            PrescriptionResponse prescriptionResponse = new PrescriptionResponse();
            PrescriptionDto patientPrescriptionByOrderNo = moeDrugService.getPatientPrescriptionByOrderNo(req.getValue());
            prescriptionResponse.getPrescriptionDto().add(patientPrescriptionByOrderNo);
            return objectFactory.createGetPatientPrescriptionByOrderNoResponse(prescriptionResponse);
        } catch (Exception e) {
            logger.error("Exception", e);
            throw new MoeWebServiceException(ServerConstant.WS_SYSTEM_ERR_CODE, ServerConstant.WS_SYSTEM_ERR_MSG);
        }
    }

    @PayloadRoot(localPart = "getPatientOrderListRequest", namespace = "http://ehr.gov.hk/moe/ws/beans")
    @ResponsePayload
    public JAXBElement<OrderListResponse> getPatientOrderList(@RequestPayload JAXBElement<OrderListRequest> req) throws Exception {

        isAuthorized("getPatientOrderList", MoeDrugService.class);
        if (StringUtils.trimToEmpty(req.getValue().getHospCode()).equals("")) {
            throw new MoeWebServiceException(WebServiceConstant.WS_MISSING_HOSPCODE_ERR_CODE, WebServiceConstant.WS_MISSING_HOSPCODE_ERR_MSG);
        } else if (StringUtils.trimToEmpty(req.getValue().getPatientKey()).equals("")) {
            throw new MoeWebServiceException(WebServiceConstant.WS_MISSING_PATIENT_KEY_ERR_CODE, WebServiceConstant.WS_MISSING_PATIENT_KEY_ERR_MSG);
        } else if (StringUtils.trimToEmpty(req.getValue().getEpisodeType()).equals("")) {
            throw new MoeWebServiceException(WebServiceConstant.WS_MISSING_EPISODE_TYPE_ERR_CODE, WebServiceConstant.WS_MISSING_EPISODE_TYPE_ERR_MSG);
        } else if ((req.getValue().getStartSearchDate() == null && req.getValue().getEndSearchDate() != null) ||
                (req.getValue().getStartSearchDate() != null && req.getValue().getEndSearchDate() == null)) {
            throw new MoeWebServiceException(WebServiceConstant.WS_MISSING_DATE_ERR_CODE, WebServiceConstant.WS_MISSING_DATE_ERR_MSG);
        }
        /*LOGGER.info("[getPatientOrderList] hospCode=" + req.getValue().getHospCode() +
                ", patientKey=" + req.getValue().getPatientKey() + ", espisodeNo=" + req.getValue().getEpisodeNo() + ", espisodeType=" + req.getValue().getEpisodeType() +
                ", startSearchDate=" + req.getValue().getStartSearchDate() + ", endSearchDate=" + req.getValue().getEndSearchDate());
*/        try {
            return objectFactory.createGetPatientOrderListResponse(moeDrugService.getPatientOrderList(req.getValue()));
        } catch (Exception e) {
            throw new MoeWebServiceException(ServerConstant.WS_SYSTEM_ERR_CODE, ServerConstant.WS_SYSTEM_ERR_MSG);
        }

    }


    @PayloadRoot(localPart = "getPatientOrderDetailRequest", namespace = "http://ehr.gov.hk/moe/ws/beans")
    @ResponsePayload
    public JAXBElement<PrescriptionResponse> getPatientOrderDetail(@RequestPayload JAXBElement<OrderDetailRequest> req) throws Exception {

        isAuthorized("getPatientOrderDetail", MoeDrugService.class);
        if (StringUtils.trimToEmpty(req.getValue().getHospCode()).equals("")) {
            throw new MoeWebServiceException(WebServiceConstant.WS_MISSING_HOSPCODE_ERR_CODE, WebServiceConstant.WS_MISSING_HOSPCODE_ERR_MSG);
        } else if (StringUtils.trimToEmpty(req.getValue().getPatientKey()).equals("")) {
            throw new MoeWebServiceException(WebServiceConstant.WS_MISSING_PATIENT_KEY_ERR_CODE, WebServiceConstant.WS_MISSING_PATIENT_KEY_ERR_MSG);
        } else if (StringUtils.trimToEmpty(req.getValue().getEpisodeNo()).equals("")) {
            throw new MoeWebServiceException(WebServiceConstant.WS_MISSING_EPISODE_NO_ERR_CODE, WebServiceConstant.WS_MISSING_EPISODE_NO_ERR_MSG);
        } else if (StringUtils.trimToEmpty(req.getValue().getEpisodeType()).equals("")) {
            throw new MoeWebServiceException(WebServiceConstant.WS_MISSING_EPISODE_TYPE_ERR_CODE, WebServiceConstant.WS_MISSING_EPISODE_TYPE_ERR_MSG);
        }
//        LOGGER.info("[getPatientOrderDetail] hospCode=" + req.getValue().getHospCode() +
//                ", patientKey="+req.getValue().getPatientKey()+", espisodeNo="+req.getValue().getEpisodeNo() +", espisodeType=" + req.getValue().getEpisodeType());
        return objectFactory.createGetPatientOrderDetailResponse(moeDrugService.getPatientOrderDetail(req.getValue()));
    }

    @PayloadRoot(localPart = "getPatientOrderLogByPeriodRequest", namespace = "http://ehr.gov.hk/moe/ws/beans")
    @ResponsePayload
    public JAXBElement<PrescriptionResponse> getPatientOrderByPeriod(@RequestPayload JAXBElement<PatientOrderLogByPeriodRequest> req) throws Exception {

//        isAuthorized("getPatientOrderLogByPeriod", MoeDrugService.class);
        if (StringUtils.trimToEmpty(req.getValue().getPatientKey()).equals("")) {
            throw new MoeWebServiceException(WebServiceConstant.WS_MISSING_PATIENT_KEY_ERR_CODE, WebServiceConstant.WS_MISSING_PATIENT_KEY_ERR_MSG);
        }
//        LOGGER.info("[getPatientOrderLogByPeriod] patientKey="+req.getValue().getPatientKey());
        return objectFactory.createGetPatientOrderLogByPeriodResponse(moeDrugService.getPatientOrderLogByPeriod(req.getValue()));
    }


    @PayloadRoot(localPart = "deletePatientOrderDetailRequest", namespace = "http://ehr.gov.hk/moe/ws/beans")
    @ResponsePayload
    public JAXBElement<DeleteOrderDetailResponse> deletePatientOrderDetail(@RequestPayload JAXBElement<DeleteOrderDetailRequest> req) throws Exception {

        isAuthorized("deletePatientOrderDetail", MoeDrugService.class);
        if (StringUtils.trimToEmpty(req.getValue().getPatientKey()).equals("")) {
            throw new MoeWebServiceException(WebServiceConstant.WS_MISSING_PATIENT_KEY_ERR_CODE, WebServiceConstant.WS_MISSING_PATIENT_KEY_ERR_MSG);
        } else if (req.getValue().getUserDto() != null) {
            UserDto userDto = req.getValue().getUserDto();
            if (StringUtils.trimToEmpty(userDto.getLoginId()).equals("")) {
                throw new MoeWebServiceException(WebServiceConstant.WS_MISSING_LOGIN_ID_ERR_CODE, WebServiceConstant.WS_MISSING_LOGIN_ID_ERR_MSG);
            } else if (StringUtils.trimToEmpty(userDto.getHospCode()).equals("")) {
                throw new MoeWebServiceException(WebServiceConstant.WS_MISSING_HOSPCODE_ERR_CODE, WebServiceConstant.WS_MISSING_HOSPCODE_ERR_MSG);
            } else if (StringUtils.trimToEmpty(userDto.getUserRank()).equals("")) {
                throw new MoeWebServiceException(WebServiceConstant.WS_MISSING_USER_RANK_ERR_CODE, WebServiceConstant.WS_MISSING_USER_RANK_ERR_MSG);
            }
        }
        /*LOGGER.info("[getPatientOrderDetail] hospCode=" + req.getValue().getUserDto().getHospCode() +
                ", patientKey="+req.getValue().getPatientKey()+", espisodeNo="+req.getValue().getEpisodeNo() +", espisodeType=" + req.getValue().getEpisodeType());*/
        DeleteOrderDetailResponse response = new DeleteOrderDetailResponse();
        try {
            moeDrugService.deletePatientOrderDetail(req.getValue());
            response.setResponseCode("00");
        } catch (Exception e) {
            logger.error("Exception", e);
            response.setResponseCode("01");
            response.setResponseMessage("Exception found when call delete prescription webservice");
        }

        return objectFactory.createDeletePatientOrderDetailResponse(response);
    }

    private void isAuthorized(String method, Class<?> targetClass) throws MoeWebServiceException {

        if (!wsMethodSecurityMetadataSource.isAuthorized(method, targetClass)) {
            throw new MoeWebServiceException(WebServiceConstant.WS_ACCESS_DENIED_ERR_CODE, WebServiceConstant.WS_ACCESS_DENIED_ERR_MSG);
        }
    }
}
