package hk.health.moe.controller;

import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.exception.ParamException;
import hk.health.moe.pojo.dto.DrugSuggestSearchDto;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.MoeEhrOrderDto;
import hk.health.moe.pojo.dto.MoeMedicationDecisionDto;
import hk.health.moe.pojo.dto.MoeMedicationDecisionResultDto;
import hk.health.moe.pojo.dto.MoeOrderSearchDto;
import hk.health.moe.pojo.dto.inner.InnerEhrOrderDrugHistoryDto;
import hk.health.moe.pojo.dto.inner.InnerMaxDosOfDdDto;
import hk.health.moe.pojo.dto.inner.InnerMoeMedProfileDto;
import hk.health.moe.pojo.dto.inner.InnerSpecialIntervalDto;
import hk.health.moe.pojo.dto.webservice.DeleteOrderDetailRequest;
import hk.health.moe.pojo.dto.webservice.DeleteOrderDetailRequestDto;
import hk.health.moe.pojo.dto.webservice.DeleteOrderDetailResponse;
import hk.health.moe.pojo.dto.webservice.OrderDetailRequest;
import hk.health.moe.pojo.dto.webservice.OrderDetailRequestDto;
import hk.health.moe.pojo.dto.webservice.OrderListRequest;
import hk.health.moe.pojo.dto.webservice.OrderListRequestDto;
import hk.health.moe.pojo.dto.webservice.OrderListResponse;
import hk.health.moe.pojo.dto.webservice.PatientOrderLogByPeriodRequest;
import hk.health.moe.pojo.dto.webservice.PatientOrderLogByPeriodRequestDto;
import hk.health.moe.pojo.dto.webservice.PrescriptionDto;
import hk.health.moe.pojo.dto.webservice.PrescriptionRequest;
import hk.health.moe.pojo.dto.webservice.PrescriptionResponse;
import hk.health.moe.pojo.dto.webservice.UserDto;
import hk.health.moe.pojo.po.MoeEhrOrderPo;
import hk.health.moe.pojo.vo.CimsResponseVo;
import hk.health.moe.restservice.bean.saam.MoePatientSummaryDto;
import hk.health.moe.restservice.dto.dac.ListMdsEnquiriesRequestDto;
import hk.health.moe.restservice.dto.dac.MDSEnquiryResponseDto;
import hk.health.moe.restservice.service.DacRestService;
import hk.health.moe.service.MoeContentService;
import hk.health.moe.service.MoeDrugService;
import hk.health.moe.service.MoeEhrOrderService;
import hk.health.moe.util.DtoUtil;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.ResponseUtil;
import hk.health.moe.util.XmlGregorianCalendarConversionUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.core.config.plugins.validation.constraints.Required;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@Api(tags = "Moe Ehr Order")
@RestController
public class MoeEhrOrderController {

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    @Autowired
    private MoeEhrOrderService moeEhrOrderService;

    @Autowired
    private DacRestService dacRestService;

    @Autowired
    private MoeContentService moeContentService;

    @Autowired
    private MoeDrugService moeDrugService;

    @Autowired
    private DozerBeanMapper mapper;

    @ApiOperation(value = "Add a Moe Order", notes = "Add a New Moe Order")
    @PostMapping("/insertOrder")
    public CimsResponseVo addMoeOrder(@RequestBody @Validated MoeEhrOrderDto moeEhrOrderDto, BindingResult bindingResult) throws Exception {

        // Ricci 20190805 start -- change to token -- comment
        /*UserDto userDto = (UserDto) HttpContextUtil.getSessionAttribute(ServerConstant.SESS_ATTR_USER_INFO);
        if (userDto == null) {
            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.session.error"));
        }*/
        // Ricci 20190805 end --

        CimsResponseVo vo = null;

        if (bindingResult.hasErrors()) {
            //eric 20191223 start--
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191223 end--
        } else {
            // Ricci 20190805 start -- change to token -- comment
            //MoeEhrOrderPo order = moeEhrOrderService.addMoeOrder(moeEhrOrderDto, userDto);
            MoeEhrOrderPo order = moeEhrOrderService.addMoeOrder(moeEhrOrderDto);
            // Ricci 20190805 end --
            vo = ResponseUtil.initSuccessResultVO(order != null && order.getMoeOrder() != null ? order.getMoeOrder().getOrdNo() : null);
        }

        return vo;
    }


    @ApiOperation(value = "Update a Moe Order", notes = "Update an Existent Moe Order")
    @PostMapping("/updateOrder")
    public CimsResponseVo updateMoeOrder(@RequestBody @Validated MoeEhrOrderDto moeEhrOrderDto, BindingResult bindingResult) throws Exception {

        // Ricci 20190805 start -- change to token -- comment
        /*UserDto userDto = (UserDto) HttpContextUtil.getSessionAttribute(ServerConstant.SESS_ATTR_USER_INFO);
        if (userDto == null) {
            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.session.error"));
        }*/
        // Ricci 20190805 end --

        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
            //eric 20191223 start--
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191223 end--
        } else {
            // Ricci 20190805 start -- change to token -- comment
            //MoeEhrOrderPo order = moeEhrOrderService.updateMoeOrder(moeEhrOrderDto, userDto);
            MoeEhrOrderPo order = moeEhrOrderService.updateMoeOrder(moeEhrOrderDto);
            // Ricci 20190805 end --
            vo = ResponseUtil.initSuccessResultVO(order != null && order.getMoeOrder() != null ? order.getMoeOrder().getOrdNo() : null);
        }

        return vo;
    }


    @ApiOperation(value = "Get a Moe Order", notes = "Show an Existent Moe Order")
    @PostMapping("/getOrder")
    public CimsResponseVo getMoeOrder(@RequestBody @Validated MoeOrderSearchDto moeOrderSearchDto, BindingResult bindingResult) throws Exception {

        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        } else {
            // Ricci 20191106 start --
            /*try {
                MoeEhrOrderDto moeEhrOrderDto = moeEhrOrderService.getMoeOrder(moeOrderSearchDto.getHospcode(), moeOrderSearchDto.getPatientKey(), moeOrderSearchDto.getCaseNo(), moeOrderSearchDto.getOrdNo());

                vo = ResponseUtil.initSuccessResultVO(moeEhrOrderDto);
            } catch (SecurityException securityException) {
                securityException.printStackTrace();
                throw new SecurityException(localeMessageSourceUtil.getMessage("system.token.expired"));
            }*/
            MoeEhrOrderDto moeEhrOrderDto = null;
            // Get the cache first ---
            moeEhrOrderDto = moeContentService.getMoeOrder();
            // If cache is empty and has order no -- Go to Database
            if (moeEhrOrderDto == null
                    && moeOrderSearchDto.getOrdNo() != null
                    && moeOrderSearchDto.getOrdNo().longValue() != 0L) {
                moeEhrOrderDto = moeEhrOrderService.getMoeOrder(moeOrderSearchDto.getHospcode(), moeOrderSearchDto.getPatientKey(), moeOrderSearchDto.getCaseNo(), moeOrderSearchDto.getOrdNo());
                moeEhrOrderDto.setIsCache(ServerConstant.DB_FLAG_FALSE);
            }
            vo = ResponseUtil.initSuccessResultVO(moeEhrOrderDto);
            // Ricci 20191106 end --
        }
        return vo;
    }


    // Simon 20190730 --start add for drug history
    @ApiOperation(value = "List Drug History", notes = "List Drug History")
    @PostMapping("/listDrugHistory")
    public CimsResponseVo getMoeEhrOrderDrugHistory(@RequestBody @Validated InnerEhrOrderDrugHistoryDto innerEhrOrderDrugHistoryDto, BindingResult bindingResult) throws Exception {
        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        } else {
            List<MoeEhrOrderDto> moeEhrOrderDtos = moeEhrOrderService.getMoeOrder(innerEhrOrderDrugHistoryDto.getWithinMonths(), innerEhrOrderDrugHistoryDto.getPrescType());
            vo = ResponseUtil.initSuccessResultVO(moeEhrOrderDtos);
        }
        return vo;
    }
    // Simon 20190730 --end add for drug history


    @ApiOperation(value = "Delete a Moe Order", notes = "Delete Moe Order by updating the status")
    @PostMapping("/deleteOrder")
    public CimsResponseVo deleteMoeOrder(@RequestBody @Validated MoeEhrOrderDto moeEhrOrderDto, BindingResult bindingResult) throws Exception {

        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
            //eric 20191223 start--
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191223 end--
        } else {
            //MoeEhrOrderPo order = moeEhrOrderService.deleteMoeOrder(moeEhrOrderDto, userDto);
            MoeEhrOrderPo order = moeEhrOrderService.deleteMoeOrder(moeEhrOrderDto);
            vo = ResponseUtil.initSuccessResultVO(order != null && order.getMoeOrder() != null ? order.getMoeOrder().getOrdNo() : null);
        }

        return vo;
    }


    @ApiOperation(value = "Generate description text with special interval", notes = "Generate description text with special interval for display")
    @PostMapping("/generateSpecialIntervalText")
    public CimsResponseVo generateSpecialIntervalText(@RequestBody @Validated InnerSpecialIntervalDto specialIntervalDto, BindingResult bindingResult) throws Exception {

        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
            //eric 20191219 start--
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 end--
        } else {
            Map<String, Object> specialIntervalText = moeEhrOrderService.generateSpecialIntervalText(specialIntervalDto);
            vo = ResponseUtil.initSuccessResultVO(specialIntervalText);
        }

        return vo;
    }


    //Simon 20190731 --start add checking for dangerous drug
    @ApiOperation(value = "Calculate Max Dosage Of DangerDrug", notes = "Calculate Max Dosage Of DangerDrug")
    @PostMapping("/getMaxDosage")
    public CimsResponseVo calculateMaxDosageOfDangerDrug(@RequestBody @Validated InnerMoeMedProfileDto moeMedProfileDto, BindingResult bindingResult) throws Exception {
        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
            //eric 20191219 start--
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 end--
        } else {
            InnerMaxDosOfDdDto maxDosage = null;
            try {
                maxDosage = moeEhrOrderService.calculateMaxDosageOfDangerDrug(moeMedProfileDto);
                vo = ResponseUtil.initSuccessResultVO(maxDosage);
            } catch (NullPointerException nullException) {
                //eric 20191219 start--
//                throw new ParamException("param.wrongInputSubmission");
                throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                        ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
                //eric 20191219 end--
            }
        }
        return vo;
    }
    //Simon 20190731 --end add checking for dangerous drug

    // Simon20190813 --start
//    @ApiOperation(value = "Get Comment Used Drug Strengths", notes = "Get Comment Used Drug Strengths")
//    @PostMapping("/listStrengths")
//    public CimsResponseVo listStrengths(@RequestBody @Validated InnerGetStrengthsDto innerGetStrengthsDto, BindingResult bindingResult) throws Exception {
//        CimsResponseVo vo = null;
//        if (bindingResult.hasErrors()) {
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
//        } else {
//            vo = ResponseUtil.initSuccessResultVO(moeEhrOrderService.listStrengths(innerGetStrengthsDto));
//        }
//        return vo;
//    }

    @ApiOperation(value = "Convert MoeDrugDto Into MedProfileDTO", notes = "Convert MoeDrugDto Into MedProfileDTO")
    @PostMapping("/convertMedProfile")
    public CimsResponseVo convertMedProfileDTO(@RequestBody @Validated MoeDrugDto drugDTO, BindingResult bindingResult) throws Exception {
        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
            //eric 20191219 start--
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 end--
        } else {
            vo = ResponseUtil.initSuccessResultVO(moeEhrOrderService.convertMedProfileDTO(drugDTO));
        }
        return vo;
    }

    // Simon 20190813 --end

    // Ricci 20190916 start support Allergy Checking --
    @ApiOperation(value = "Do Allergy Drug Checking for current patient before confirm order", notes = "Call DAC /medicationDecision API")
    @PostMapping("/allergyChecking")
    public CimsResponseVo allergyChecking(@RequestBody MoeMedicationDecisionDto dto) throws Exception {
        CimsResponseVo vo = null;

        List<MoeMedicationDecisionResultDto> resultDtos = moeEhrOrderService.allergyChecking(dto);
        vo = ResponseUtil.initSuccessResultVO(resultDtos);

        return vo;
    }
    // Ricci 20190916 end support Allergy Checking --


    // Simon 20191012 start support get mds content--
    @ApiOperation(value = "Get Clinical Alert Rule")
    @GetMapping("/listClinicalAlertRules")
    public CimsResponseVo listClinicalAlertRules() throws Exception {
        return ResponseUtil.initSuccessResultVO(DtoUtil.CLINICAL_ALERT_MAP);
    }
    //Simon 20191012 end--

    // Ricci 20191014 start --
    @ApiOperation(value = "")
    @GetMapping("/patientSummary")
    public CimsResponseVo getPatientSummaryByPatientRefKey() throws Exception {

        MoePatientSummaryDto result = moeEhrOrderService.getPatientSummaryByPatientRefKey();
        return ResponseUtil.initSuccessResultVO(result);
    }
    // Ricci 20191014 end --

    // Simon 20191016 start--
    @ApiOperation(value = "")
    @GetMapping("/listMDSEnquiries")
    public CimsResponseVo<MDSEnquiryResponseDto> listMDSEnquiries(@ModelAttribute ListMdsEnquiriesRequestDto dto) throws Exception {
        return dacRestService.listMDSEnquiries(dto);
    }
    //Simon 20191016 end--

    // Ricci 20191016 start --
    @ApiOperation(value = "")
    @PostMapping("/patientAlert")
    public CimsResponseVo addPatientAlert() throws Exception {

        String msg = moeEhrOrderService.addPatientAlert();
        return ResponseUtil.initSuccessResultVO(msg);
    }
    // Ricci 20191016 end --

    // Ricci 20191112 start --
    @ApiOperation(value = "")
    @PostMapping("/cancelOrder")
    public CimsResponseVo cancelOrder() throws Exception {

        moeEhrOrderService.cancelOrder();
        return ResponseUtil.initSuccessResultVO();
    }
    // Ricci 20191112 end --

    // Simon 20191107 for webservice testing start--
    @ApiOperation(value = "")
    @GetMapping("/patientPrescription/{orderNo}/{hospCode}")
    public CimsResponseVo getPatientPrescriptionByOrderNo(@PathVariable("orderNo") @Required long orderNo,
                                                          @PathVariable("hospCode") @Required String hospCode) throws Exception {
        PrescriptionRequest prescriptionRequest = new PrescriptionRequest();
        prescriptionRequest.setHospCode(hospCode);
        prescriptionRequest.setOrderNo(orderNo);
        PrescriptionDto patientPrescriptionByOrderNo = moeDrugService.getPatientPrescriptionByOrderNo(prescriptionRequest);
        return ResponseUtil.initSuccessResultVO(patientPrescriptionByOrderNo);
    }

    @ApiOperation(value = "")
    @PostMapping("/listPatientOrderList")
    public CimsResponseVo getPatientOrderList(@RequestBody OrderListRequestDto orderListRequestDto) throws Exception {
        OrderListRequest orderListRequest = mapper.map(orderListRequestDto, OrderListRequest.class);
        if ((orderListRequestDto.getStartDate() == null && orderListRequestDto.getEndDate() != null)||
                (orderListRequestDto.getStartDate() != null && orderListRequestDto.getEndDate() == null)){
            throw new MoeServiceException(localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
        }
        if (orderListRequestDto.getStartDate() != null) {
            orderListRequest.setStartSearchDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(orderListRequestDto.getStartDate()));
        }
        if (orderListRequestDto.getEndDate() != null) {
            orderListRequest.setEndSearchDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(orderListRequestDto.getEndDate()));
        }
        OrderListResponse patientOrderList = moeDrugService.getPatientOrderList(orderListRequest);
        return ResponseUtil.initSuccessResultVO(patientOrderList);
    }

    @ApiOperation(value = "")
    @PostMapping("/listPatientOrderDetail")
    public CimsResponseVo listPatientOrderDetail(@RequestBody OrderDetailRequestDto orderDetailRequestDto) throws Exception {
        OrderDetailRequest orderDetailRequest = mapper.map(orderDetailRequestDto, OrderDetailRequest.class);
        PrescriptionResponse patientOrderDetail = moeDrugService.getPatientOrderDetail(orderDetailRequest);
        return ResponseUtil.initSuccessResultVO(patientOrderDetail);
    }

    @ApiOperation(value = "")
    @PostMapping("/listPatientOrderLogByPeriod")
    public CimsResponseVo getPatientOrderLogByPeriod(@RequestBody PatientOrderLogByPeriodRequestDto requestDto) throws Exception {
        PatientOrderLogByPeriodRequest request = mapper.map(requestDto, PatientOrderLogByPeriodRequest.class);
        if (requestDto.getSearchStartTime() != null) {
            request.setStartTime(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(requestDto.getSearchStartTime()));
        }
        if (requestDto.getSearchEndTime() != null) {
            request.setEndTime(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(requestDto.getSearchEndTime()));
        }

        PrescriptionResponse patientOrderLogByPeriod = moeDrugService.getPatientOrderLogByPeriod(request);
        return ResponseUtil.initSuccessResultVO(patientOrderLogByPeriod);
    }
    @ApiOperation(value = "")
    @DeleteMapping("/patientOrder")
    public CimsResponseVo deletePatientOrderDetail(@RequestBody @Validated DeleteOrderDetailRequestDto requestDto,BindingResult bindingResult) throws Exception {
        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        }else{
            DeleteOrderDetailRequest request = mapper.map(requestDto,DeleteOrderDetailRequest.class);
            UserDto userDto = mapper.map(requestDto.getUserDto(), UserDto.class);
            request.setUserDto(userDto);
            DeleteOrderDetailResponse deleteOrderDetailResponse = new DeleteOrderDetailResponse();
            try{
                moeDrugService.deletePatientOrderDetail(request);
                deleteOrderDetailResponse.setResponseCode("00");
            } catch (Exception e) {
                deleteOrderDetailResponse.setResponseMessage("01");
                deleteOrderDetailResponse.setResponseMessage("Exception found when call delete prescription webservice");
            }
            vo  = ResponseUtil.initSuccessResultVO(deleteOrderDetailResponse);
        }

         return vo;
    }

    //Simon 20191107 for webservice testing end--


    //Chris  For DrugSearch from Redis  -Start
    /*//Version 1
    //TEST
    @ApiOperation(value = "test insert drugs data into Redis with Json String format")
    @GetMapping("/testInsertJson")
    public CimsResponseVo insertDrugJson() throws Exception {
        moeContentService.cacheDrugSearchDataToJson();
        return ResponseUtil.initSuccessResultVO();
    }


    //TEST
    @ApiOperation(value = "test insert drugs data into Redis with Object format")
    @GetMapping("/testInsertObject")
    public CimsResponseVo insertDrugObject() throws Exception {
        moeContentService.cacheDrugSearchDataToObject();
        return ResponseUtil.initSuccessResultVO();
    }


    //TEST
    @ApiOperation(value = "test match drugs by keyword")
    @PostMapping("/testMatch")
    public CimsResponseVo matchDrugByKeyword(@Validated @RequestBody DrugSuggestSearchDto drugSuggestSearchDto, BindingResult bindingResult) {

        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
            //eric 20191220 start--
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult,ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191220 end--
        } else {
            String searchString = drugSuggestSearchDto.getSearchString();
            List<MoeDrugDto> drugSet = moeContentService.matchDrugByKeyword(searchString);
            vo = ResponseUtil.initSuccessResultVO(drugSet);
        }
        return vo;
    }*/

    //Version 2
    //Insert all Drug items into Redis(Being used)
    @ApiOperation(value = "test insert all drugs data into Redis with Json String format base on MOE1")
    @GetMapping("/testInsertAllDrugsInRedis")
    public CimsResponseVo insertAllDrugsInRedis() throws Exception {
        moeContentService.cacheAllDrugsInRedis();
        return ResponseUtil.initSuccessResultVO();
    }

    //Match all Drug items by keyword(Being used)
    @ApiOperation(value = "test get all drugs data from Redis with Object format base on MOE1")
    @GetMapping("/testMatchAllDrugs/{input}/{isSearchMultiIngrByBanVtm}")
    public CimsResponseVo getAllDrugs(@PathVariable("input") String input, @PathVariable("isSearchMultiIngrByBanVtm") String isSearchMultiIngrByBanVtm) throws Exception {
        Map<String, Map<String, List<MoeDrugDto>>> result = moeContentService.getAllDrugsFromRedis(input, "Y".equalsIgnoreCase(isSearchMultiIngrByBanVtm)? true : false);
        return ResponseUtil.initSuccessResultVO(result);
    }


    /*//Version 3
    //TEST
    @ApiOperation(value = "test insert Drug Map into Redis with Object format base on MOE1")
    @GetMapping("/testInsertDrugMap")
    public CimsResponseVo insertDrugMapInRedis() throws Exception {
        moeContentService.cacheDrugMapInRedis();
        return ResponseUtil.initSuccessResultVO();
    }

    //TEST
    @ApiOperation(value = "test get all drugs data from Redis with Object format base on MOE1")
    @GetMapping("/testGetDrugMap")
    public CimsResponseVo getDrugMap() throws Exception {
        Map<String, Map<String, List<MoeDrugDto>>> result = moeContentService.getDrugMap();
        return ResponseUtil.initSuccessResultVO(result);
    }*/
    //Chris  For DrugSearch from Redis  -End


}
