package hk.health.moe.controller;

import hk.health.moe.common.ResponseCode;
import hk.health.moe.pojo.dto.AuthenticationDto;
import hk.health.moe.pojo.dto.inner.InnerRefreshToken;
import hk.health.moe.pojo.vo.CimsResponseVo;
import hk.health.moe.service.AuthenticationService;
import hk.health.moe.util.JwtUtil;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Authentication")
@RestController
public class AuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    @Autowired
    private JwtUtil jwtUtil;

    @ApiOperation(value = "Authenticate user login", notes = "Add Authenticate user info into session")
    @PostMapping("/login")
    public CimsResponseVo login(@RequestBody @Validated AuthenticationDto dto, BindingResult bindingResult) throws Exception {

        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        } else {
            dto = authenticationService.login(dto);
            vo = ResponseUtil.initSuccessResultVO(dto);
        }

        return vo;
    }

    @ApiOperation(value = "Authenticate refresh token", notes = "Refresh Authenticate user info into session")
    @PostMapping("/refreshToken")
    public CimsResponseVo refreshToken(@RequestBody @Validated InnerRefreshToken dto, BindingResult bindingResult) throws Exception {

        CimsResponseVo vo;
        if (bindingResult.hasErrors()) {
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        } else {
            vo = ResponseUtil.initSuccessResultVO(jwtUtil.refreshToken(dto.getOldToken()));
        }

        return vo;
    }

    @ApiOperation(value = "Authenticate user logout", notes = "Remove Authenticate user info in session")
    @PostMapping("/logout")
    public CimsResponseVo logout() throws Exception {

        authenticationService.logout();
        return ResponseUtil.initSuccessResultVO();
    }

}
