package hk.health.moe.controller;

import hk.health.moe.common.ResponseCode;
import hk.health.moe.pojo.dto.MoeMyFavouriteHdrDto;
import hk.health.moe.pojo.dto.MyFavouriteSortDto;
import hk.health.moe.pojo.dto.inner.InnerDeleteFavouriteListDto;
import hk.health.moe.pojo.dto.inner.InnerListMyFavouriteDto;
import hk.health.moe.pojo.dto.inner.InnerSaveMyFavouriteDto;
import hk.health.moe.pojo.vo.CimsResponseVo;
import hk.health.moe.service.MoeMyFavouriteHdrService;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "My Favourite")
@RestController
public class MyFavouriteHdrController {
    @Autowired
    MoeMyFavouriteHdrService moeMyFavouriteHdrService;

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;


    @ApiOperation(value = "Show My Favourite", notes = "Show My Favourite")
    @PostMapping("/listMyFavourite")
    public CimsResponseVo listMyFavourite(@RequestBody InnerListMyFavouriteDto listMyFavouriteDto) throws Exception {
        CimsResponseVo vo = null;

//        if(bindingResult.hasErrors()){
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
//        }else{
        List<MoeMyFavouriteHdrDto> result = moeMyFavouriteHdrService.listMyFavourite(listMyFavouriteDto.getUserId(),listMyFavouriteDto.getSearchString(),listMyFavouriteDto.isDepartment());
        vo = ResponseUtil.initSuccessResultVO(result);
//        }
        return vo;
    }

    //Simon 20180801 --start comment
//    @ApiOperation(value = "Order My Favourite [Detail]", notes = "Order My Favourite [Detail]")
//    @PostMapping("/orderMyFavouriteDetail")
//    public CimsResponseVo orderMyFavouriteDetail(@Validated @RequestBody MyFavouriteSortDto myFavouriteSortDto, BindingResult bindingResult) throws Exception {
//        CimsResponseVo vo = null;
//        if (bindingResult.hasErrors()) {
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
//        } else {
//            List<MoeMyFavouriteHdrDto> result = moeMyFavouriteHdrService.orderMyFavourites(myFavouriteSortDto.getFavouriteHdrs(), myFavouriteSortDto.isDepartment());
//            vo = ResponseUtil.initSuccessResultVO(result);
//        }
//        return vo;
//    }
    //Simon 20180801 --end comment

    @ApiOperation(value = "Order My Favourite Detail", notes = "Order My Favourite Detail")
    @PostMapping("/orderMyFavouriteDetail")
    public CimsResponseVo orderMyFavouriteDetail(@Validated @RequestBody MyFavouriteSortDto myFavouriteSortDto, BindingResult bindingResult) throws Exception {
        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
            //eric 20191218 start--
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191218 end--
        } else {
            MoeMyFavouriteHdrDto result = moeMyFavouriteHdrService.sortMyFavouriteDetail(myFavouriteSortDto.getFavouriteHdrs().get(0), myFavouriteSortDto.isDepartment());
            vo = ResponseUtil.initSuccessResultVO(result);
        }
        return vo;
    }


    @ApiOperation(value = "Order My Favourites", notes = "Order My Favourites")
    @PostMapping("/orderMyFavourites")
    public CimsResponseVo orderMyFavourite(@Validated @RequestBody MyFavouriteSortDto myFavouriteSortDto, BindingResult bindingResult) throws Exception {
        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
            //eric 20191218 start--
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191218 end--
        } else {
            List<MoeMyFavouriteHdrDto> result = moeMyFavouriteHdrService.sortMyFavourites(myFavouriteSortDto.getFavouriteHdrs(), myFavouriteSortDto.isDepartment());
            vo = ResponseUtil.initSuccessResultVO(result);
        }
        return vo;
    }


    @ApiOperation(value = "Save My Favourite [Detail]", notes = "Save My Favourite [Detail]")
    @PostMapping("/saveMyFavourite")
    public CimsResponseVo saveMyFavourite(@Validated @RequestBody InnerSaveMyFavouriteDto saveMyFavouriteDto, BindingResult bindingResult) throws Exception {//42
        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
            //eric 20191218 start--
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191218 end--
        } else {
            MoeMyFavouriteHdrDto moeMyFavouriteHdrPo = moeMyFavouriteHdrService.saveMyFavourite(saveMyFavouriteDto.getMoeMyFavouriteHdr(), saveMyFavouriteDto.isDepartment());
            vo = ResponseUtil.initSuccessResultVO(moeMyFavouriteHdrPo);
        }
        return vo;
    }

    @ApiOperation(value = "Update My Favourite [Detail]", notes = "Update My Favourite [Detail]")
    @PostMapping("/updateMyFavourites")
    public CimsResponseVo updateMyFavourites(@Validated @RequestBody InnerSaveMyFavouriteDto saveMyFavouriteDto, BindingResult bindingResult) throws Exception {
        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
            //eric 20191218 start--
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191218 end--
        } else {
            MoeMyFavouriteHdrDto moeMyFavouriteHdrPo = moeMyFavouriteHdrService.updateMyFavourite(saveMyFavouriteDto.getMoeMyFavouriteHdr(), saveMyFavouriteDto.isDepartment());
            vo = ResponseUtil.initSuccessResultVO(moeMyFavouriteHdrPo);
        }
        return vo;
    }


    @ApiOperation(value = "Delete My Favourite", notes = "Delete My Favourite")
    @PostMapping("/deleteMyFavourites")
    public CimsResponseVo deleteMyFavourite(@Validated @RequestBody MoeMyFavouriteHdrDto myFavouriteDto, BindingResult bindingResult) throws Exception {
        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
            //eric 20191218 start--
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191218 end--
        } else {
            MoeMyFavouriteHdrDto moeMyFavouriteHdrPo = moeMyFavouriteHdrService.deleteMyFavourite(myFavouriteDto);
            vo = ResponseUtil.initSuccessResultVO(moeMyFavouriteHdrPo);
        }
        return vo;
    }


    @ApiOperation(value = "Delete My Favourite Detail", notes = "Delete My Favourite Detail")
    @PostMapping("/deleteMyFavouriteDetail")
    public CimsResponseVo deleteMyFavouriteDetail(@Validated @RequestBody MoeMyFavouriteHdrDto hdr, BindingResult bindingResult) throws Exception {
        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
            //eric 20191218 start--
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191218 end--
        } else {
            MoeMyFavouriteHdrDto moeMyFavouriteHdrPo = moeMyFavouriteHdrService.deleteMyFavouriteDetail(hdr);
            vo = ResponseUtil.initSuccessResultVO(moeMyFavouriteHdrPo);
        }
        return vo;
    }
    //Simon 20190906 start--
/*    @ApiOperation(value = "List My Favourite By Keyword", notes = "List My Favourite By Keyword")
    @PostMapping("/listMyFavouriteByKeyWord")
    public CimsResponseVo listMyFavouriteByKeyword(@RequestBody InnerListMyFavouriteDto listMyFavouriteDto) throws Exception {
        CimsResponseVo vo = null;

//        if(bindingResult.hasErrors()){
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
//        }else{
        List<MoeMyFavouriteHdrDto> result = moeMyFavouriteHdrService.listMyFavouriteByKeyword(listMyFavouriteDto.getUserId(),listMyFavouriteDto.getSearchString());
        vo = ResponseUtil.initSuccessResultVO(result);
//        }
        return vo;
    }*/
    //Simon 20190906 end--

    // Ricci 20191109 start --
    @ApiOperation(value = "Save My Favourite [Detail] For Departmental", notes = "Save My Favourite [Detail] For Departmental")
    @PostMapping("/saveDepartFavourite")
    public CimsResponseVo saveDepartFavourite(@Validated @RequestBody MoeMyFavouriteHdrDto moeMyFavouriteHdr, BindingResult bindingResult) throws Exception {//42
        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
            //eric 20191218 start--
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191218 end--
        } else {
            MoeMyFavouriteHdrDto moeMyFavouriteHdrPo = moeMyFavouriteHdrService.saveDepartFavourite(moeMyFavouriteHdr);
            vo = ResponseUtil.initSuccessResultVO(moeMyFavouriteHdrPo);
        }
        return vo;
    }
    // Ricci 20191109 end --

    // Ricci 20191109 start --
    @ApiOperation(value = "Cancel My Favourite [Detail] For Departmental", notes = "Cancel My Favourite [Detail] For Departmental")
    @DeleteMapping("/cancelDepartFavourite")
    public CimsResponseVo cancelDepartFavourite(@RequestParam(value = "cacheId") String cacheId,
                                                @RequestParam(value = "myFavouriteId", required = false) String myFavouriteId) throws Exception {
        CimsResponseVo vo = null;
        moeMyFavouriteHdrService.cancelDepartFavourite(cacheId, myFavouriteId);
        vo = ResponseUtil.initSuccessResultVO();

        return vo;
    }
    // Ricci 20191109 end --

    /// Simon 20191127 start--
    @ApiOperation(value = "Delete MyFavouriteList", notes = "Delete MyFavouriteList")
    @PostMapping("/deleteFavouriteList")
    public CimsResponseVo deleteFavouriteList(@RequestBody InnerDeleteFavouriteListDto moeMyFavouriteHdrList) throws Exception{
        List<MoeMyFavouriteHdrDto> moeMyFavouriteHdr = moeMyFavouriteHdrList.getMoeMyFavouriteHdrDtos();
        moeMyFavouriteHdrService.deleteMyFavouriteList(moeMyFavouriteHdr);
        return ResponseUtil.initSuccessResultVO();
    }
    //Simon 20191127 end--
}
