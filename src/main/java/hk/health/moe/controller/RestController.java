package hk.health.moe.controller;

import hk.health.moe.pojo.vo.CimsResponseVo;
import hk.health.moe.restservice.dto.saam.ResponseDto;
import hk.health.moe.restservice.service.SaamRestService;
import hk.health.moe.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.core.config.plugins.validation.constraints.Required;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

/**
 * @Author Simon
 * @Date 2019/10/25
 */
@Api(tags = "RestController")
@org.springframework.web.bind.annotation.RestController
public class RestController {

    @Autowired
    SaamRestService saamRestService;


    @ApiOperation(value = "Delete Patient Alert", notes = "Delete Patient Alert")
    @DeleteMapping("/patientAlert/{alertSeqNo}/{deleteReason}/{version}")
    public CimsResponseVo deletePatientAlert(@PathVariable @Required String alertSeqNo,
                                             @PathVariable @Required String deleteReason,
                                             @PathVariable @Required Long version) throws Exception {
        CimsResponseVo vo = null;
        ResponseDto responseDto = saamRestService.deletePatientAlert(alertSeqNo, deleteReason,version);
        vo = ResponseUtil.initSuccessResultVO(responseDto);
        return vo;
    }


    @ApiOperation(value = "Add No Known Drug Allergy", notes = "Add No Known Drug Allergy")
    @PostMapping("/patientAllergy")
    public CimsResponseVo addPatientAllergy() throws Exception {
        CimsResponseVo vo = null;
        ResponseDto responseDto = saamRestService.addPatientAllergy();
        vo = ResponseUtil.initSuccessResultVO(responseDto);
        return vo;
    }


    @ApiOperation(value = "Update Patient Allergy in Bulk", notes = "Re-Confirm Button")
    @PutMapping("/bulkUpdatePatientAllergy")
    public CimsResponseVo bulkUpdatePatientAllergy() throws Exception {
        CimsResponseVo vo = null;
        ResponseDto responseDto = saamRestService.bulkUpdatePatientAllergy();
        vo = ResponseUtil.initSuccessResultVO(responseDto);
        return vo;
    }


}
