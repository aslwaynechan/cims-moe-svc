package hk.health.moe.controller;

import hk.health.moe.common.MoeAsync;
import hk.health.moe.common.ResponseCode;
import hk.health.moe.pojo.dto.DrugDetailDto;
import hk.health.moe.pojo.dto.MoeCommonDosageDto;
import hk.health.moe.pojo.dto.MoeDrugBySpecialtyDto;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.MoeHospitalSettingDto;
import hk.health.moe.pojo.dto.inner.InnerDeleteDrugDto;
import hk.health.moe.pojo.dto.inner.InnerGetDrugByNameDto;
import hk.health.moe.pojo.dto.inner.InnerGetDrugBySpecialtyDto;
import hk.health.moe.pojo.dto.inner.InnerGetDrugDto;
import hk.health.moe.pojo.dto.inner.InnerListCommonDosageDto;
import hk.health.moe.pojo.dto.inner.InnerListRelatedDrugDto;
import hk.health.moe.pojo.dto.inner.InnerSaveDrugDto;
import hk.health.moe.pojo.vo.CimsResponseVo;
import hk.health.moe.service.CommonDosageService;
import hk.health.moe.service.MoeDrugBySpecialtyService;
import hk.health.moe.service.MoeDrugLocalService;
import hk.health.moe.service.MoeHospitalSettingService;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/8/22
 */
@Api(tags = "Drug Maintenance")
@RestController
public class DrugMaintenanceController {
    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    @Autowired
    private MoeDrugLocalService moeDrugLocalService;

    @Autowired
    private MoeHospitalSettingService moeHospitalSettingService;

    @Autowired
    private MoeDrugBySpecialtyService moeDrugBySpecialtyService;

    @Autowired
    private MoeAsync moeAsync;

    @Autowired
    private CommonDosageService commonDosageService;

    @ApiOperation(value = "Create New Local Drug Record ", notes = "Create New Local Drug Record")
    @PostMapping("/newLocalDrugRecord")
    public CimsResponseVo newLocalDrugRecord(@RequestBody InnerGetDrugDto innerGetDrugDto, BindingResult bindingResult) throws Exception {

        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
            //eric 20191218 start--
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191218 end--
        } else {
            DrugDetailDto drugDetailDto = moeDrugLocalService.newLocalDrugRecord(innerGetDrugDto.getLocalDrugId(), innerGetDrugDto.getHkRegNo(), innerGetDrugDto.getStrengthRank());
            vo = ResponseUtil.initSuccessResultVO(drugDetailDto);
        }
        return vo;
    }


    @ApiOperation(value = "Get Local Drug Record ", notes = "Get Local Drug Record")
    @PostMapping("/getLocalDrugRecord")
    public CimsResponseVo getLocalDrugRecord(@RequestBody InnerGetDrugDto innerGetDrugDto, BindingResult bindingResult) throws Exception {

        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        } else {
            DrugDetailDto drugDetailDto = moeDrugLocalService.getLocalDrugRecord(innerGetDrugDto.getLocalDrugId(), innerGetDrugDto.getHkRegNo());
            vo = ResponseUtil.initSuccessResultVO(drugDetailDto);
        }
        return vo;
    }

    // Ricci 20190823 start --
    @ApiOperation(value = "Save Drug Into Drug Local", notes = "Add a drug without HK Registration No")
    @PostMapping("/saveDrug")
    public CimsResponseVo saveDrug(@Validated @RequestBody InnerSaveDrugDto dto, BindingResult bindingResult) throws Exception {

        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        } else {
            moeDrugLocalService.saveDrug(dto);
            vo = ResponseUtil.initSuccessResultVO();
            moeAsync.refreshDrugListTask();
        }

        return vo;
    }
    // Ricci 20190823 end --

    //Simon 20190828 start--
    @ApiOperation(value = "List Related Drug", notes = "List Related Drug")
    @PostMapping("/listRelatedDrug")
    public CimsResponseVo listRelatedDrug(@Validated @RequestBody InnerListRelatedDrugDto dto, BindingResult bindingResult) throws Exception {

        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        } else {
            List<MoeDrugDto> moeDrugDtos = moeDrugLocalService.listRelatedDrug(dto.getLocalDrugId(), dto.getVtm(), dto.getTradeName(), dto.getRouteEng(),
                    dto.getFormEng(), dto.getDoseFormExtraInfo(), dto.getHospCode());
            vo = ResponseUtil.initSuccessResultVO(moeDrugDtos);
        }
        return vo;
    }
    //Simon 20190828 end--


    // Ricci 20190828 start --
    @ApiOperation(value = "Drug Main for Drug by Specialty", notes = "First -Call this api when you click tab of Drug by Specialty")
    @GetMapping("getAllSpecialtyAvailability")
    public CimsResponseVo getAllSpecialtyAvailability() throws Exception {

        List<MoeHospitalSettingDto> list = moeHospitalSettingService.getAllSpecialtyAvailability();
        return ResponseUtil.initSuccessResultVO(list);
    }

    @ApiOperation(value = "Drug Main for Drug by Specialty", notes = "Second -Call this api after getAllSpecialtyAvailability(API) returns")
    @GetMapping("getAllLocationByUserGroupMap")
    public CimsResponseVo getAllLocationByUserGroupMap() throws Exception {

        List<MoeHospitalSettingDto> list = moeHospitalSettingService.getAllLocationByUserGroupMap();
        return ResponseUtil.initSuccessResultVO(list);
    }

    @ApiOperation(value = "Drug Main for Drug by Specialty", notes = "Third -Call this api after getAllLocationByUserGroupMap(API) returns")
    @GetMapping("getDrugBySpecialty")
    public CimsResponseVo getDrugBySpecialty(@ModelAttribute InnerGetDrugBySpecialtyDto dto) throws Exception {

        List<MoeDrugBySpecialtyDto> list = moeDrugBySpecialtyService.getDrugBySpecialty(dto);
        return ResponseUtil.initSuccessResultVO(list);
    }
    // Ricci 20190828 end --

    // Chris 20190829  -Start
    @ApiOperation(value = "", notes = "")
    @PostMapping("/findDrugByName")
    public CimsResponseVo findDrugByName(@Validated @RequestBody InnerGetDrugByNameDto dto, BindingResult bindingResult) throws Exception {

        CimsResponseVo vo = null;

        if (bindingResult.hasErrors()) {
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        } else {
            List<MoeDrugDto> moeDrugDtos = moeDrugLocalService.findByAmp(dto.getAmp());
            vo = ResponseUtil.initSuccessResultVO(moeDrugDtos);
        }
        return vo;
    }
    // Chris 20190829  -End

    // Simon 20190912 for drug maintenance (predefine dosage) start--
    @ApiOperation(value = "", notes = "")
    @PostMapping("/listCommonDosage")
    public CimsResponseVo listCommonDosage(@Validated @RequestBody InnerListCommonDosageDto innerListCommonDosageDto, BindingResult bindingResult) throws Exception {
        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        } else {
            List<MoeCommonDosageDto> dosageDto = commonDosageService.getCommonDosage(innerListCommonDosageDto.isNewDrug(), innerListCommonDosageDto.getLocalDrugId(), innerListCommonDosageDto.getHkRegNo());
            vo = ResponseUtil.initSuccessResultVO(dosageDto);
        }
        return vo;
    }
    //Simon 20190912 for drug maintenance(predefine dosage) end--

    @ApiOperation(value = "", notes = "")
    @DeleteMapping("/drug")
    public CimsResponseVo deleteDrug(@Validated @ModelAttribute InnerDeleteDrugDto innerDeleteDrugDto, BindingResult bindingResult) throws Exception {
        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        } else {
            moeDrugLocalService.deleteDrugs(innerDeleteDrugDto);
            vo = ResponseUtil.initSuccessResultVO();
            moeAsync.refreshDrugListTask();
        }
        return vo;
    }
}
