package hk.health.moe.controller;

import hk.health.moe.common.ResponseCode;
import hk.health.moe.pojo.bo.ReportDetailBo;
import hk.health.moe.pojo.dto.inner.InnerPrintPrescriptionDto;
import hk.health.moe.pojo.dto.inner.InnerPrintPrescriptionLogDto;
import hk.health.moe.pojo.vo.CimsResponseVo;
import hk.health.moe.service.ReportService;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "Report")
@RestController
public class ReportController {

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    @Autowired
    private ReportService reportService;

    @ApiOperation(value = "Print a prescription", notes = "Print a prescription")
    @PostMapping("/printPrescription")
    public CimsResponseVo printPrescription(@Valid @RequestBody InnerPrintPrescriptionDto dto, BindingResult bindingResult) throws Exception {
        CimsResponseVo vo = null;

        if (bindingResult.hasErrors()) {
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        } else {
            ReportDetailBo reportDetailBo = reportService.reportForPrescription(dto);
            vo = ResponseUtil.initSuccessResultVO(reportDetailBo);
        }
        return vo;
    }

    // Ricci 20190813 start --
    @ApiOperation(value = "Log a prescription for print", notes = "Log a prescription for print")
    @PostMapping("/logPrintPrescription")
    public CimsResponseVo logPrintPrescription(@Valid @RequestBody InnerPrintPrescriptionLogDto dto, BindingResult bindingResult) throws Exception {
        CimsResponseVo vo = null;

        if (bindingResult.hasErrors()) {
            //eric 20191225 start--
//            vo = ResponseUtil.initErrorResultVO(localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191225 end--
        } else {
            InnerPrintPrescriptionLogDto innerVo = reportService.logPrintPrescription(dto);
            vo = ResponseUtil.initSuccessResultVO(innerVo);
        }

        return vo;
    }
    // Ricci 20190813 end --

}
