package hk.health.moe.controller;

import hk.health.moe.common.ResponseCode;
import hk.health.moe.pojo.dto.DrugSuggestSearchDto;
import hk.health.moe.pojo.dto.GenericDto;
import hk.health.moe.pojo.vo.CimsResponseVo;
import hk.health.moe.service.MoeMyFavouriteHdrService;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "Drug Suggestion")
@RestController
public class DrugSuggestController {

    @Autowired
    private MoeMyFavouriteHdrService moeMyFavouriteHdrService;

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    @ApiOperation(value = "List Recommended Drugs", notes = "Show Recommended Drugs")
    @PostMapping("/listDrugSuggest")
    public CimsResponseVo listDrugSuggest(@Validated @RequestBody DrugSuggestSearchDto drugSuggestSearchDto, BindingResult bindingResult) throws Exception {

        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
            //eric 20191219 start--
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 end--

        } else {
            List<GenericDto> result = moeMyFavouriteHdrService.listDrugSuggest(drugSuggestSearchDto);
            vo = ResponseUtil.initSuccessResultVO(result);
        }
        return vo;
    }

}
