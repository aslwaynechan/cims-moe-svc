package hk.health.moe.controller;

import hk.health.moe.common.ResponseCode;
import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.pojo.dto.MoeEhrOrderDto;
import hk.health.moe.pojo.dto.MoeMyFavouriteHdrDto;
import hk.health.moe.pojo.vo.CimsResponseVo;
import hk.health.moe.service.MoeContentService;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.locks.ReentrantLock;

@Api(tags = "MoeContent")
@RestController
public class MoeContentController {

    @Autowired
    private MoeContentService moeContentService;

    @Autowired
    private ReentrantLock refreshDrugListLock;

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    @ApiOperation(value = "Refresh the drug cache from DB", notes = "")
    @PostMapping("/refreshDrugList")
    public CimsResponseVo refreshDrugList() throws Exception {

        CimsResponseVo vo = null;
        try {
            refreshDrugListLock.lock();
            moeContentService.refreshDrugList();
            vo = ResponseUtil.initSuccessResultVO(localeMessageSourceUtil.getMessage("system.refresh.drugList.success"));

            return vo;
        } catch (Exception e) {
            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.refresh.drugList.error"));
        } finally {
            refreshDrugListLock.unlock();
        }
    }

    @ApiOperation(value = "Refresh the system setting cache from DB", notes = "")
    @PostMapping("/refreshSystemSetting")
    public CimsResponseVo refreshSystemSetting() throws Exception {

        CimsResponseVo vo = null;
        try {
            moeContentService.refreshSystemSetting();
            vo = ResponseUtil.initSuccessResultVO(localeMessageSourceUtil.getMessage("system.refresh.setting.success"));

            return vo;
        } catch (Exception e) {
            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.refresh.setting.error"));
        }
    }

    @ApiOperation(value = "Cache My Favourite [Detail]", notes = "Cache My Favourite [Detail]")
    @PostMapping("/cacheDepartFavourite")
    public CimsResponseVo cacheDepartFavourite(@Validated @RequestBody MoeMyFavouriteHdrDto cacheDto, BindingResult bindingResult) throws Exception {
        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
            //eric 20191219 start--
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 end--
        } else {
            String cacheId = moeContentService.cacheDepartFavourite(cacheDto); // TODO
            vo = ResponseUtil.initSuccessResultVO(cacheId);
        }

        return vo;
    }

    // Ricci 20191105 start --
    @ApiOperation(value = "Save Prescription into Redis")
    @PostMapping("/cacheOrder")
    public CimsResponseVo cacheOrder(@Validated @RequestBody MoeEhrOrderDto cacheDto, BindingResult bindingResult) throws Exception {

        CimsResponseVo vo = null;
        if (bindingResult.hasErrors()) {
//            vo = ResponseUtil.initErrorResultVO(bindingResult, localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            vo = ResponseUtil.initErrorResultVO(bindingResult, ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        } else {
            moeContentService.cacheMoeOrder(cacheDto);
            vo = ResponseUtil.initSuccessResultVO();
        }

        return vo;
    }
    // Ricci 20191105 end --

}
