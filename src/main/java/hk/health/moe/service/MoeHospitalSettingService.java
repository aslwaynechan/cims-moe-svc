package hk.health.moe.service;

import hk.health.moe.pojo.dto.MoeHospitalSettingDto;

import java.util.List;

public interface MoeHospitalSettingService {

    List<MoeHospitalSettingDto> getAllSpecialtyAvailability() throws Exception;

    List<MoeHospitalSettingDto> getAllLocationByUserGroupMap() throws Exception;

}
