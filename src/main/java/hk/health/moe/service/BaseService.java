package hk.health.moe.service;

/**************************************************************************
 * NAME        : BaseService.java
 * VERSION     : 1.0.0
 * DATE        : 03-JUL-2019
 * DESCRIPTION : Interface for basic service
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       03-JUL-2019  Initial Version
 **************************************************************************/

import hk.health.moe.pojo.dto.TokenInfoDto;
import hk.health.moe.pojo.dto.UserDto;

public interface BaseService {

    TokenInfoDto getTokenInfo() throws Exception;

    UserDto getUserDto() throws Exception;

}
