package hk.health.moe.service.impl;

import hk.health.moe.common.ServerConstant;
import hk.health.moe.facade.MoeCommonDosageFacade;
import hk.health.moe.facade.MoeDrugBySpecialtyFacade;
import hk.health.moe.facade.MoeDrugFacade;
import hk.health.moe.facade.MoeDrugGenericNameLocalFacade;
import hk.health.moe.facade.MoeDrugLocalFacade;
import hk.health.moe.facade.MoeDrugTherapeuticLocalFacade;
import hk.health.moe.facade.MoeDrugToSaamFacade;
import hk.health.moe.facade.SaDrugLocalFacade;
import hk.health.moe.pojo.dto.DrugDetailDto;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.MoeDrugLocalDto;
import hk.health.moe.pojo.dto.MoeDrugStrengthDto;
import hk.health.moe.pojo.dto.inner.InnerDeleteDrugDto;
import hk.health.moe.pojo.dto.inner.InnerRelatedDrugDosageLocalDrug;
import hk.health.moe.pojo.dto.inner.InnerSaveCommonDosageDto;
import hk.health.moe.pojo.dto.inner.InnerSaveDrugBySpecialtyDto;
import hk.health.moe.pojo.dto.inner.InnerSaveDrugDto;
import hk.health.moe.pojo.po.MoeDrugLocalPo;
import hk.health.moe.pojo.po.MoeDrugStrengthPo;
import hk.health.moe.repository.MoeDrugGenericNameLocalRepository;
import hk.health.moe.repository.MoeDrugStrengthRepository;
import hk.health.moe.repository.MoeDrugToSaamLocalRepository;
import hk.health.moe.service.MoeDrugLocalService;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.ValidateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Simon
 * @version 1.0
 * @Date 2019/8/22
 * for moe_drug_server
 */
@Service
public class MoeDrugLocalServiceImpl extends BaseServiceImpl implements MoeDrugLocalService {
    @Autowired
    private MoeDrugFacade moeDrugFacade;

    @Autowired
    private MoeDrugStrengthRepository moeDrugStrengthRepository;

    @Autowired
    private MoeDrugToSaamLocalRepository moeDrugToSaamLocalRepository;

    @Autowired
    private MoeDrugGenericNameLocalRepository moeDrugGenericNameLocalRepository;

    @Autowired
    private MoeDrugLocalFacade moeDrugLocalFacade;

    @Autowired
    private SaDrugLocalFacade saDrugLocalFacade;

    @Autowired
    private MoeDrugToSaamFacade moeDrugToSaamFacade;

    @Autowired
    private MoeCommonDosageFacade moeCommonDosageFacade;

    @Autowired
    private MoeDrugBySpecialtyFacade moeDrugBySpecialtyFacade;

    @Autowired
    private MoeDrugTherapeuticLocalFacade moeDrugTherapeuticLocalFacade;

    //Chris 20200109  -Start
    @Autowired
    private MoeDrugGenericNameLocalFacade moeDrugGenericNameLocalFacade;
    //Chris 20200109  -End

    @Autowired
    private ValidateUtil validateUtil;

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    @Override
    public DrugDetailDto newLocalDrugRecord(String localDrugId, String hkRegNo, Long strengthRank) throws Exception {
        //TODO  not sure if need check login status --Simon
        //UserDto userDto = getUserDto();
        // check if localDrugId, hkRegNo, strengthRank combinations are correct
        // Simon 20190925 start--
        localDrugId = localDrugId.toUpperCase();
        //Simon 20190925 end--
        boolean isValid = moeDrugFacade.isLocalDrugKeyValid(true, localDrugId, hkRegNo);
        if (isValid) {
            DrugDetailDto dto = new DrugDetailDto();
            //Simon 20190823 comment start--
            //dto.setDrugKeyAndHkRegValid(isValid);
            dto.setDrugKeyAndHkRegValid(true);
            //Simon 20190823 comment end--

            //Simon 20190823 start--
            dto.setRetreiveMethod("new");
            //Simon 20190823 end--
            return dto;
        }
        boolean isCombinationValid = moeDrugFacade.isDrugParaCombinationValid(true, localDrugId, hkRegNo, strengthRank);
        if (!isCombinationValid) {
            return null;
        }
        DrugDetailDto drugDetailDto = moeDrugFacade.newDrugLocalRecord(localDrugId, hkRegNo, strengthRank);
        // Check display name
        List<MoeDrugDto> moeDrugDto = moeDrugFacade.findDrugByHkRegNo(hkRegNo, strengthRank);
        if (moeDrugDto != null) {
            for (MoeDrugDto dto : moeDrugDto) {
                for (MoeDrugStrengthDto strength : dto.getStrengths()) {
                    MoeDrugStrengthPo s = moeDrugStrengthRepository.findByHkRegNoAndAmp(hkRegNo, strength.getAmp());
                    //MoeDrugStrengthIdDto id = new MoeDrugStrengthIdDto(hkRegNo, s.getId().getRank());
                    //strength.setId(id);
                    strength.setHkRegNo(hkRegNo);
                    strength.setRank((int) s.getRank());
                }
            }
            drugDetailDto.setMoeDrugList(moeDrugDto);
        }
        //Simon 20190823 start--
        drugDetailDto.setRetreiveMethod("new");
        //Simon 20190823 end--
        return drugDetailDto;
    }


    @Transactional(readOnly = true)
    @Override
    public DrugDetailDto getLocalDrugRecord(String localDrugId, String hkRegNo) throws Exception {
        // Simon 20190925 start--
        localDrugId = localDrugId.toUpperCase();
        //Simon 20190925 end--
        boolean isValid = moeDrugFacade.isLocalDrugKeyValid(false, localDrugId, hkRegNo);
        if (!isValid && StringUtils.isNotEmpty(localDrugId) && StringUtils.isNotEmpty(hkRegNo)) {
            DrugDetailDto dto = new DrugDetailDto();
            //Simon 20190823 comment start--
            //dto.setDrugKeyAndHkRegValid(isValid);
            dto.setDrugKeyAndHkRegValid(false);
            //Simon 20190823 comment end--

            //Simon 20190823 start--
            dto.setRetreiveMethod("search");
            //Simon 20190823 end--
            return dto;
        }
        // check if localDrugId, hkRegNo combinations are correct
        boolean isCombinationValid = moeDrugFacade.isDrugParaCombinationValid(false, localDrugId, hkRegNo, null);
        if (!isCombinationValid) {
            return null;
        }
        DrugDetailDto drugDetailDto = moeDrugFacade.getDrugLocalRecord(localDrugId, hkRegNo);
        if (StringUtils.isNotEmpty(localDrugId)) {
            List<MoeDrugDto> moeDrugDto = moeDrugFacade.findById(localDrugId);
            drugDetailDto.setAddToSAAM(moeDrugToSaamLocalRepository.isExist(localDrugId.toUpperCase()).intValue() > 0);
            if (moeDrugDto != null) {
                drugDetailDto.setMoeDrugList(moeDrugDto);
            }
        } else {
            List<MoeDrugDto> moeDrugDto = moeDrugFacade.findByHkRegNo(hkRegNo);
            if (moeDrugDto != null) {
                drugDetailDto.setMoeDrugList(moeDrugDto);
                if (drugDetailDto.getLocalDrugId() != null) {
                    drugDetailDto.setAddToSAAM(moeDrugToSaamLocalRepository.isExist(drugDetailDto.getLocalDrugId()) > 0);
                } else {
                    drugDetailDto.setAddToSAAM(false);
                }
            }
        }

        //Chris 20200108  Query and Return Generic Name Level Flag -Start
        //Chris 20200113  Return is it allow Generic Name Check Box enable  -Start
        Integer vtmRouteFormIdExist = null;
        Integer vmpIdExist = null;
        boolean genericNameLevelFlag = ServerConstant.DEFAULT_VALUE_FALSE;
        boolean allowGenericNameCheckBoxFlag = ServerConstant.DEFAULT_VALUE_FALSE;

        if (drugDetailDto != null && drugDetailDto.getMoeDrugLocal() != null) {

            MoeDrugLocalDto moeDrugLocal = drugDetailDto.getMoeDrugLocal();

            String strengthCompulsory = moeDrugLocal.getStrengthCompulsory();


            if (!"Y".equalsIgnoreCase(strengthCompulsory)) {
                //Strength Compulsory = "N", check "vtmRouteFormId"

                if (moeDrugLocal.getVtmRouteFormId() != null && moeDrugLocal.getVtmRouteFormId() != 0L) {
                    vtmRouteFormIdExist = moeDrugGenericNameLocalRepository.isVtmRouteFormIdExist(moeDrugLocal.getVtmRouteFormId());

                    genericNameLevelFlag = vtmRouteFormIdExist != null && vtmRouteFormIdExist > 0;

                    allowGenericNameCheckBoxFlag = true;
                }

            } else if ("Y".equalsIgnoreCase(strengthCompulsory)) {
                //Strength Compulsory = "Y", check "vmpId"

                if (drugDetailDto.getMoeDrugStrengthLocal() != null) {
                    if (drugDetailDto.getMoeDrugStrengthLocal().getVmpId() != null && drugDetailDto.getMoeDrugStrengthLocal().getVmpId() != 0) {
                        vmpIdExist = moeDrugGenericNameLocalRepository.isVmpIdExist(drugDetailDto.getMoeDrugStrengthLocal().getVmpId().longValue());

                        genericNameLevelFlag = vmpIdExist != null && vmpIdExist > 0;

                        allowGenericNameCheckBoxFlag = true;
                    } else if (drugDetailDto.getMoeDrugStrengthLocal().getAmpId() != null && drugDetailDto.getMoeDrugStrengthLocal().getAmpId() != 0 && StringUtils.isNotBlank(hkRegNo)) {
                        //Get vmpId from moe_drug_strength by HkRegNo and Amp
                        MoeDrugStrengthPo moeDrugStrength = moeDrugStrengthRepository.findByHkRegNoAndAmpId(hkRegNo, drugDetailDto.getMoeDrugStrengthLocal().getAmpId().longValue());
                        if (moeDrugStrength != null && moeDrugStrength.getVmpId() != null && moeDrugStrength.getVmpId() != 0L) {
                            vmpIdExist = moeDrugGenericNameLocalRepository.isVmpIdExist(moeDrugStrength.getVmpId());

                            genericNameLevelFlag = vmpIdExist != null && vmpIdExist > 0;

                            allowGenericNameCheckBoxFlag = true;
                        }

                    }
                }

            }
            //Others: Default Value = false
        }

         drugDetailDto.getMoeDrugOrdPropertyLocal().setGenericNameLevel(genericNameLevelFlag);
        drugDetailDto.getMoeDrugOrdPropertyLocal().setAllowGenericNameCheckBox(allowGenericNameCheckBoxFlag);
        //Chris 20200108  Query and Return Generic Name Level Flag -End
        //Chris 20200113  Return is it allow Generic Name Check Box enable  -End

        drugDetailDto.setDrugKeyAndHkRegValid(true);
        //Simon 20190823 start--
        drugDetailDto.setRetreiveMethod("search");
        //Simon 20190823 end--
        return drugDetailDto;
    }

    // Ricci 20190823 start --
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveDrug(InnerSaveDrugDto dto) throws Exception {
        // Ricci 20191022 start --
        //validateUtil.validate(dto);
        // Ricci 20191022 end --

        boolean isNewADrug = dto.isNewDrugRecord();
        List<InnerRelatedDrugDosageLocalDrug> relatedDrugDosageLocalDrugs = dto.getRelatedDrugDosageLocalDrugs();

        MoeDrugLocalPo dataPo = moeDrugLocalFacade.saveDrugDetail(dto, relatedDrugDosageLocalDrugs);
        saDrugLocalFacade.saveDrugDetail(dto);
        moeDrugToSaamFacade.saveDrugDetail(dto);

        if (dto.getPredefineDosageItems() != null && !dto.getPredefineDosageItems().isEmpty()) {
            List<InnerSaveCommonDosageDto> predefineDosageItems = dto.getPredefineDosageItems();
            moeCommonDosageFacade.saveCommonDosage(predefineDosageItems, relatedDrugDosageLocalDrugs, isNewADrug, dataPo);
        }

        if (dto.getMoeDrugBySpecialtyItems() != null && !dto.getMoeDrugBySpecialtyItems().isEmpty()) {
            List<InnerSaveDrugBySpecialtyDto> drugBySpecialtyItems = dto.getMoeDrugBySpecialtyItems();
            moeDrugBySpecialtyFacade.saveDrugBySpecialty(drugBySpecialtyItems);
        }

        moeDrugTherapeuticLocalFacade.saveDrugByTherapeuticLocal(dto);

        //Chris 20200109  Generic Name Level  -Start
        boolean isExist = moeDrugGenericNameLocalFacade.checkIsMoeDrugGenericNameLocalExist(dto);
        if (dto.getMoeDrugOrdPropertyLocal().isGenericNameLevel()) {
            //Check whether need to add a Generic Name Level Items
            if (!isExist) {
                moeDrugGenericNameLocalFacade.addGenericNameLocalRecord(dto);
            }
        } else {
            //Check whether need to delete a Generic Name Level Items
            if (isExist) {
                moeDrugGenericNameLocalFacade.deleteGenericNameLocalRecord(dto);
            }
        }
        //Chris 20200109  Generic Name Level  -End
    }
    // Ricci 20190823 end --

    //Simon 20190828 start--
    @Override
    public List<MoeDrugDto> listRelatedDrug(String localDrugId, String vtm, String tradeName, String routeEng, String formEng, String doseFormExtraInfo, String hospCode) throws Exception {
        return moeDrugFacade.listRelatedDrug(localDrugId, vtm, tradeName, routeEng, formEng, doseFormExtraInfo, hospCode);
    }
    //Simon 20190828 end--

    //Chris 20190829 From moe_drug_server --Start
    @Override
    public List<MoeDrugDto> findByAmp(String amp) throws Exception {
        return moeDrugFacade.findByAmp(amp);
    }
    //Chris 20190829 From moe_drug_server --End

    // Simon 20191014 start--
    @Transactional(rollbackFor = Exception.class)
    public void deleteDrugs(InnerDeleteDrugDto dto) throws Exception {
        moeDrugLocalFacade.deleteDrugs(dto.getLocalDrugId());
    }
    //Simon 20191014 end--
}
