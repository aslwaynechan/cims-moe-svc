package hk.health.moe.service.impl;

/**************************************************************************
 * NAME        : RestTemplateService.java
 * VERSION     : 1.0.0
 * DATE        : 03-JUL-2019
 * DESCRIPTION : Implementation of the RestTemplate
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       03-JUL-2019  Initial Version
 **************************************************************************/

import hk.health.moe.pojo.dto.SystemLogDto;
import hk.health.moe.pojo.vo.CimsResponseVo;
import hk.health.moe.service.RestTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestTemplateServiceImpl implements RestTemplateService {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${restTemplate.systemLog.url}")
    String systemLogUrl;

    @Override
    public void writeSystemLog(SystemLogDto systemLogDto) throws Exception {
        restTemplate.postForObject(systemLogUrl, systemLogDto, CimsResponseVo.class);
    }

    // Ricci 20190823 start --
    @Override
    public void refreshSaamDrugs() throws Exception {
        /*String refreshMoeIp = SystemSettingUtil.getParamValue(ServerConstant.PARAM_NAME_REFRESH_MOE_IP_ADDR);
        if (refreshMoeIp != null && ServerConstant.DB_FLAG_TRUE.equalsIgnoreCase(SystemSettingUtil.getParamValue(ServerConstant.PARAM_NAME_ENABLE_REFRESH_MOE))){
            String refreshUrl = ServerConstant.REFRESH_DRUG_URL;
            String moeUrlArray[] = refreshMoeIp.split("\\|");
            for (String url : moeUrlArray){
                restTemplate.postForEntity(url+refreshUrl, null, CimsResponseVo.class);
            }
        }*/
    }
    // Ricci 20190823 end --



}
