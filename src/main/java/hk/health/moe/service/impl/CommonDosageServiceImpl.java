package hk.health.moe.service.impl;


import hk.health.moe.facade.MoeCommonDosageFacade;
import hk.health.moe.pojo.dto.MoeCommonDosageDto;
import hk.health.moe.pojo.dto.MoeCommonDosageTypeDto;
import hk.health.moe.pojo.dto.MoeFreqDto;
import hk.health.moe.pojo.po.MoeCommonDosagePo;
import hk.health.moe.service.CommonDosageService;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Simon
 * @Date 2019/9/12
 * @Description for drug maintenance
 */
@Service
public class CommonDosageServiceImpl implements CommonDosageService {
    @Autowired
    MoeCommonDosageFacade moeCommonDosageFacade;

    @Autowired
    DozerBeanMapper mapper;
    @Override
    public List<MoeCommonDosageDto> getCommonDosage(boolean isNewADrug, String localDrugId, String hkRegNo) throws Exception {
        List<MoeCommonDosagePo> records = moeCommonDosageFacade.getCommonDosage(
                isNewADrug, localDrugId, hkRegNo);
        List<MoeCommonDosageDto> recordsDTO = new ArrayList<>();
        if (records != null) {
            for (MoeCommonDosagePo record : records) {
                //Simon 20190912 modify for get CommonDosageType start--
                //recordsDTO.add(mapper.map(record, MoeCommonDosageDto.class));
                MoeCommonDosageDto dto = mapper.map(record, MoeCommonDosageDto.class);
                dto.setCommonDosageType(mapper.map(record.getMoeCommonDosageType(), MoeCommonDosageTypeDto.class));
                recordsDTO.add(dto);
                //Simon 20190912 modify for get CommonDosageType end--
                // Simon 20191011 for get MoeFreqDto start--
                dto.setFreq(mapper.map(record.getMoeFreq(), MoeFreqDto.class));
                //Simon 20191011 end--

            }
        }
        return recordsDTO;
    }
}
