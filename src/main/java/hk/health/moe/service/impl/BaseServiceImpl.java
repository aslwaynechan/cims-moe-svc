package hk.health.moe.service.impl;

/**************************************************************************
 * NAME        : BaseServiceImpl.java
 * VERSION     : 1.0.0
 * DATE        : 03-JUL-2019
 * DESCRIPTION : Implementation of the basic service
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       03-JUL-2019  Initial Version
 **************************************************************************/

import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.pojo.dto.TokenInfoDto;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.service.BaseService;
import hk.health.moe.util.HttpContextUtil;
import hk.health.moe.util.JwtUtil;
import hk.health.moe.util.LocaleMessageSourceUtil;
import io.jsonwebtoken.ExpiredJwtException;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import hk.health.moe.exception.SecurityException;

import java.util.Map;

@Service("baseService")
public class BaseServiceImpl implements BaseService {

    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;
    @Autowired
    private RedisTemplate tokenRedisTemplate;

    @Override
    public TokenInfoDto getTokenInfo() throws Exception {

        UserDto userDto = null;
        String token = null;
        Map<String, Object> claims = null;
        Map<String, Object> mapUserDto = null;

        try {
            token = jwtUtil.getJwtFromRequest(HttpContextUtil.getHttpServletRequest());
            claims = jwtUtil.getClaimsFromJWT(token);

            mapUserDto = (Map) claims.get(ServerConstant.SESS_ATTR_USER_INFO);
            userDto = (UserDto) JSONObject.toBean(JSONObject.fromObject(mapUserDto), UserDto.class);
            if (null == userDto) {
                //eric 20191223 start--
//                throw new SecurityException(localeMessageSourceUtil.getMessage("system.token.illegal"));
                throw new SecurityException(ResponseCode.CommonMessage.TOKEN_EXPIRE_ERROR.getResponseCode(),
                        ResponseCode.CommonMessage.TOKEN_EXPIRE_ERROR.getResponseMessage());
                //eric 20191223 end--
            }
            /*if(ServerConstant.SYSTEM_LOGIN_FOR_MOE_SVC.equalsIgnoreCase(userDto.getSystemLogin())){
                String redisTokenKey = String.format(ServerConstant.MOE_CACHE_TOKEN_BLACKLIST_PREFIX,
                        userDto.getSystemLogin(),userDto.getSpec(), userDto.getHospitalCd(),
                        userDto.getLoginId(), "*");

                Set<String> blackListKeyList = tokenRedisTemplate.keys(redisTokenKey);
                List<Object> blackList = tokenRedisTemplate.opsForValue().multiGet(blackListKeyList);
                for(Object unPassToken : blackList){
                    if(unPassToken != null && unPassToken.toString().equalsIgnoreCase(token)){
                        throw new SecurityException(localeMessageSourceUtil.getMessage("system.token.illegal"));
                    }
                }
            }*/

        } catch (SecurityException e) {
            throw e;
        } catch (ExpiredJwtException e) {
            //eric 20191223 start--
//            throw new SecurityException(localeMessageSourceUtil.getMessage("system.token.expired"));
            throw new SecurityException(ResponseCode.CommonMessage.TOKEN_EXPIRE_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.TOKEN_EXPIRE_ERROR.getResponseMessage());
            //eric 20191223 end--
        } catch (Exception e) {
            //eric 20191223 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.error"), e);
            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage());
            //eric 20191223 end--
        }

        TokenInfoDto tokenInfo = new TokenInfoDto();
        tokenInfo.setUserDto(userDto);

        return tokenInfo;
    }

    public UserDto getUserDto() throws Exception {

        UserDto userDto = null;

        TokenInfoDto tokenInfo = getTokenInfo();
        if (tokenInfo != null) {
            userDto = tokenInfo.getUserDto();
            if (userDto != null) {
                return userDto;
            } else {
                //eric 20191223 start--
//                throw new SecurityException(localeMessageSourceUtil.getMessage("system.token.illegal"));
                throw new SecurityException(ResponseCode.CommonMessage.TOKEN_EXPIRE_ERROR.getResponseCode(),ResponseCode.CommonMessage.TOKEN_EXPIRE_ERROR.getResponseMessage());
                //eric 20191223 end--
            }
        } else {
            //eric 20191223 start--
//                throw new SecurityException(localeMessageSourceUtil.getMessage("system.token.illegal"));
            throw new SecurityException(ResponseCode.CommonMessage.TOKEN_EXPIRE_ERROR.getResponseCode(),ResponseCode.CommonMessage.TOKEN_EXPIRE_ERROR.getResponseMessage());
            //eric 20191223 end--
        }
    }

}
