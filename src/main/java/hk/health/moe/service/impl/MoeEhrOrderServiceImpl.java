package hk.health.moe.service.impl;

import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.ConcurrentException;
import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.exception.ParamException;
import hk.health.moe.exception.RecordException;
import hk.health.moe.facade.MoeOrderFacade;
import hk.health.moe.pojo.dto.InnerMoePatientAllergyDto;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.MoeEhrMedProfileDto;
import hk.health.moe.pojo.dto.MoeEhrOrderDto;
import hk.health.moe.pojo.dto.MoeMedProfileDto;
import hk.health.moe.pojo.dto.MoeMedicationDecisionDto;
import hk.health.moe.pojo.dto.MoeMedicationDecisionResultDto;
import hk.health.moe.pojo.dto.MoePatientMedicationDto;
import hk.health.moe.pojo.dto.PreparationDto;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.pojo.dto.inner.InnerMaxDosDto;
import hk.health.moe.pojo.dto.inner.InnerMaxDosOfDdDto;
import hk.health.moe.pojo.dto.inner.InnerMoeMedMultDoseDto;
import hk.health.moe.pojo.dto.inner.InnerMoeMedProfileDto;
import hk.health.moe.pojo.dto.inner.InnerSpecialIntervalDto;
import hk.health.moe.pojo.po.MoeEhrAcceptDrugCheckingPo;
import hk.health.moe.pojo.po.MoeEhrMedMultDosePo;
import hk.health.moe.pojo.po.MoeEhrMedProfilePo;
import hk.health.moe.pojo.po.MoeEhrOrderLogPo;
import hk.health.moe.pojo.po.MoeEhrOrderPo;
import hk.health.moe.pojo.po.MoeMedMultDosePo;
import hk.health.moe.pojo.po.MoeMedProfileLogPo;
import hk.health.moe.pojo.po.MoeMedProfilePo;
import hk.health.moe.pojo.po.MoeOrderPo;
import hk.health.moe.pojo.po.MoePatientCasePo;
import hk.health.moe.pojo.po.MoePatientPo;
import hk.health.moe.pojo.vo.CimsResponseVo;
import hk.health.moe.repository.MoeEhrAcceptDrugCheckingRepository;
import hk.health.moe.repository.MoeEhrMedMultDoseRepository;
import hk.health.moe.repository.MoeEhrMedProfileRepository;
import hk.health.moe.repository.MoeEhrOrderLogRepository;
import hk.health.moe.repository.MoeEhrOrderRepository;
import hk.health.moe.repository.MoeMedMultDoseRepository;
import hk.health.moe.repository.MoeMedProfileRepository;
import hk.health.moe.repository.MoeOrderLogRepository;
import hk.health.moe.repository.MoeOrderRepository;
import hk.health.moe.restservice.bean.saam.MoePatientSummaryDto;
import hk.health.moe.restservice.dto.dac.MedicationDecisionRequestDto;
import hk.health.moe.restservice.dto.dac.MedicationDecisionResponseDto;
import hk.health.moe.restservice.dto.dac.MedicationDecisionResultDto;
import hk.health.moe.restservice.dto.dac.ReactionAllergenDto;
import hk.health.moe.restservice.dto.saam.AddPatientAlertResponse;
import hk.health.moe.restservice.dto.saam.PatientAlertRequestDto;
import hk.health.moe.restservice.dto.saam.SummaryPageResponse;
import hk.health.moe.restservice.service.DacRestService;
import hk.health.moe.restservice.service.SaamRestService;
import hk.health.moe.service.MoeContentService;
import hk.health.moe.service.MoeEhrOrderService;
import hk.health.moe.util.DateUtil;
import hk.health.moe.util.DtoMapUtil;
import hk.health.moe.util.DtoModeChangeHelper;
import hk.health.moe.util.Formatter;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.OrderLogUtil;
import hk.health.moe.util.QuantityCalculator;
import hk.health.moe.util.StringUtil;
import hk.health.moe.util.SystemSettingUtil;
import hk.health.moe.util.WsDtoMapUtil;
import hk.health.moe.util.WsFormatter;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

@Service
public class MoeEhrOrderServiceImpl extends BaseServiceImpl implements MoeEhrOrderService {

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    @Autowired
    private MoeEhrOrderRepository moeEhrOrderRepository;

    @Autowired
    private MoeOrderRepository moeOrderRepository;

    @Autowired
    private MoeEhrOrderLogRepository moeEhrOrderLogRepository;

    @Autowired
    private MoeOrderLogRepository moeOrderLogRepository;

    @Autowired
    private MoeEhrMedMultDoseRepository moeEhrMedMultDoseRepository;

    @Autowired
    private MoeEhrMedProfileRepository moeEhrMedProfileRepository;

    @Autowired
    private MoeMedMultDoseRepository moeMedMultDoseRepository;

    @Autowired
    private MoeMedProfileRepository moeMedProfileRepository;

    @Autowired
    private MoeEhrAcceptDrugCheckingRepository moeEhrAcceptDrugCheckingRepository;

    @Autowired
    private DacRestService dacRestService;

    @Autowired
    private DozerBeanMapper dozerBeanMapper;

    @Autowired
    private MoeOrderFacade moeOrderFacade;

    @Autowired
    private SaamRestService saamRestService;

    @Autowired
    private MoeContentService moeContentService;

    @Override
    public MoeEhrOrderPo addMoeOrder(MoeEhrOrderDto moeEhrOrderDto) throws Exception {
        UserDto userDto = getUserDto();
        moeEhrOrderDto.getMoeOrder().setPrescType(userDto.getPrescTypeCd());

        //Chris 20190927  SetHospcode  -Start
        String hospitalCode = userDto.getHospitalCd();
        moeEhrOrderDto.setHospcode(hospitalCode);
        moeEhrOrderDto.getMoeOrder().setHospcode(hospitalCode);
        //Chris 20190927  SetHospcode  -End

        //Chris 20190816  Back-Date Mode  -Start
        if (userDto.getBackDate() != null && !"".equals(userDto.getBackDate())) {
            //Is Back-Date
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date backDate = simpleDateFormat.parse(userDto.getBackDate());
            //Chris 20190822  -Start
            moeEhrOrderDto.getMoeOrder().setOrdDate(backDate);

            moeEhrOrderDto.getMoeOrder().setBackDate(userDto.getBackDate());
            //Chris 20190822  -End

            //Chris 20180816  Set Remark Info   -Start
            if (null != moeEhrOrderDto.getMoeOrder().getRemarkText()) {
                boolean outOfLength = StringUtils.isBlank(moeEhrOrderDto.getMoeOrder().getRemarkText()) ? true : moeEhrOrderDto.getMoeOrder().getRemarkText().length() > ServerConstant.LENGTH_100;
                if (outOfLength) {
                    //eric 20191220 start--
//                    throw new ParamException(localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
                    throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                            ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
                    //eric 20191220 end--
                }

                moeEhrOrderDto.getMoeOrder().setRemarkText(moeEhrOrderDto.getMoeOrder().getRemarkText().trim());

                //Set Remark Info
                String name = userDto.getLoginName() != null ? userDto.getLoginName() : userDto.getLoginId();
                moeEhrOrderDto.getMoeOrder().setRemarkCreateBy(name);
                moeEhrOrderDto.getMoeOrder().setRemarkCreateDate(new Date());
            }
            //Chris 20180816  Set Remark Info   -End

        }
        //Chris 20190816  BackDate Mode  -End

        for (MoeMedProfileDto profile : moeEhrOrderDto.getMoeOrder().getMoeMedProfiles()) {
            DateUtil.calEndDate(profile, moeEhrOrderDto.getMoeOrder().getOrdDate());
        }

        //generate OrdNo
        Long ordNo = moeEhrOrderRepository.getNextVal();
        moeEhrOrderDto.setOrdNo(ordNo);
        moeEhrOrderDto.getMoeOrder().setOrdNo(ordNo);

        MoeEhrOrderPo order = DtoMapUtil.convertMoeEhrOrder(moeEhrOrderDto);
        setCreateInfo(order, userDto);

        //Chris 20190814  Set Order Print Type  --Start
        order.getMoeOrder().setPrintType(ServerConstant.ORDER_PRINT_TYPE_PRINT);
        //Chris 20190814  Set Order Print Type  --End

        order = moeOrderFacade.addMoeOrder(order, getUserDto());

        // Ricci 20191106 start -- remove redis cache
        String redisKey = String.format(ServerConstant.MOE_CACHE_ORDER_PREFIX,
                userDto.getMrnPatientIdentity(), userDto.getMrnPatientEncounterNum());
        moeContentService.deleteOrder(redisKey);
        // Ricci 20191106 end --
        moeContentService.tryReleasePrescription(0);

        return order;
    }

    @Override
    public MoeEhrOrderPo updateMoeOrder(MoeEhrOrderDto moeEhrOrderDto) throws Exception {
        UserDto userDto = getUserDto();
        moeEhrOrderDto.getMoeOrder().setPrescType(userDto.getPrescTypeCd());

        //Chris 20190927  SetHospcode  -Start
        String hospitalCode = userDto.getHospitalCd();
        moeEhrOrderDto.setHospcode(hospitalCode);
        moeEhrOrderDto.getMoeOrder().setHospcode(hospitalCode);
        //Chris 20190927  SetHospcode  -End

        //Chris 20180816  Set Remark Info   -Start
        if (null != moeEhrOrderDto.getMoeOrder().getRemarkText()) {
            boolean outOfLength = StringUtils.isBlank(moeEhrOrderDto.getMoeOrder().getRemarkText()) ? true : moeEhrOrderDto.getMoeOrder().getRemarkText().length() > 100;
            if (outOfLength) {
                //eric 20191220 start--
//                throw new ParamException(localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
                throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                        ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
                //eric 20191220 end--
            }

            moeEhrOrderDto.getMoeOrder().setRemarkText(moeEhrOrderDto.getMoeOrder().getRemarkText().trim());

            //Set Remark Info
            String name = userDto.getLoginName() != null ? userDto.getLoginName() : userDto.getLoginId();
            moeEhrOrderDto.getMoeOrder().setRemarkCreateBy(name);
            moeEhrOrderDto.getMoeOrder().setRemarkCreateDate(new Date());
        }
        //Chris 20180816  Set Remark Info   -End

        MoeEhrOrderPo oorder = null;
        List<MoeEhrOrderPo> oorders = moeEhrOrderRepository.findByPatientKeyAndOrderNo(userDto.getMoePatientKey(), moeEhrOrderDto.getOrdNo());

        if (null != oorders & oorders.size() != 0) {
            oorder = oorders.get(0);
        }

        if (oorder == null) {
            //eric 20191220 start--
//            throw new RecordException(localeMessageSourceUtil.getMessage("records.prescription.notExisted"));
            throw new RecordException(ResponseCode.SpecialMessage.RECORDS_PRESCRIPTION_NOTEXISTED.getResponseCode(),
                    ResponseCode.SpecialMessage.RECORDS_PRESCRIPTION_NOTEXISTED.getResponseMessage());
            //eric 20191220 end--
        }
        if (oorder.getMoeOrder().getVersion() != moeEhrOrderDto.getVersion()) {
            //eric 20191220 start--
//            throw new ConcurrentException(localeMessageSourceUtil.getMessage("moeOrder.updateByOthers"));
            throw new ConcurrentException(ResponseCode.SpecialMessage.MOEORDER_UPDATE_BY_OTHERS.getResponseCode(),
                    ResponseCode.SpecialMessage.MOEORDER_UPDATE_BY_OTHERS.getResponseMessage());
            //eric 20191220 end--
        }

        for (MoeMedProfileDto profile : moeEhrOrderDto.getMoeOrder().getMoeMedProfiles()) {
            DateUtil.calEndDate(profile, moeEhrOrderDto.getMoeOrder().getOrdDate());
        }

        MoeEhrOrderPo order = DtoMapUtil.convertMoeEhrOrder(moeEhrOrderDto);
        order.getMoeOrder().setRefNo(Formatter.getRefNo(order.getMoeOrder().getOrdNo(), order.getMoeOrder().getVersion() + 1));
        MoeEhrOrderLogPo orderLog = setUpdateInfo(oorder, order, userDto);

        removeDeletedItems(order);

        //Chris 20190814  Set Order Print Type  --Start
        order.getMoeOrder().setPrintType(ServerConstant.ORDER_PRINT_TYPE_PRINT);
        //Chris 20190814  Set Order Print Type  --End

        // Ricci 20191106 start -- remove redis cache
        order = moeOrderFacade.updateMoeOrder(order, orderLog, userDto);
        String redisKey = String.format(ServerConstant.MOE_CACHE_ORDER_PREFIX,
                userDto.getMrnPatientIdentity(), userDto.getMrnPatientEncounterNum());
        moeContentService.deleteOrder(redisKey);
        // Ricci 20191106 end --
        moeContentService.tryReleasePrescription(0);


        return order;
    }

    private void setCreateInfo(MoeEhrOrderPo newOrder, final UserDto userDto) {
        Date now = new Date();

        updateOrder(newOrder, userDto, now, true);

        int cmsItemNo = 1;
        for (MoeMedProfilePo newProfile : newOrder.getMoeOrder().getMoeMedProfiles()) {
            newProfile.setCmsItemNo(cmsItemNo++);
            updateProfile(newProfile, userDto, now, true);
        }
    }


    private void updateOrder(MoeEhrOrderPo order, final UserDto userDto, final Date now, boolean setCreateInfo) {
        updateOrder(null, order, userDto, now, setCreateInfo);
    }


    private void updateOrder(final MoeEhrOrderPo oorder, MoeEhrOrderPo order, final UserDto userDto, final Date now, boolean setCreateInfo) {
        if (setCreateInfo) {
            order.setHospcode(userDto.getHospitalCd());
            MoePatientPo patient = new MoePatientPo();
            //Chris 20190724    Start
//            patient.setMoePatientKey(userDto.getMrnPatientIdentity());
            patient.setMoePatientKey(userDto.getMoePatientKey());
            //Chris 20190724    End

            order.setMoePatient(patient);
            MoePatientCasePo patientCase = new MoePatientCasePo();
            //Chris 20190724    Start
//            patientCase.setMoeCaseNo(userDto.getMrnPatientEncounterNum());
            patientCase.setMoeCaseNo(userDto.getMoeCaseNo());
            //Chris 20190724    End

            order.setMoePatientCase(patientCase);
            order.setCreateUserId(userDto.getLoginId());
            order.setCreateUser(userDto.getLoginName());
            order.setCreateHosp(userDto.getHospitalCd());
            order.setCreateRank(userDto.getUserRankCd());
            order.setCreateRankDesc(userDto.getUserRankDesc());
        }
        order.setUpdateUserId(userDto.getLoginId());
        order.setUpdateUser(userDto.getLoginName());
        order.setUpdateHosp(userDto.getHospitalCd());
        order.setUpdateRank(userDto.getUserRankCd());
        order.setUpdateRankDesc(userDto.getUserRankDesc());

        MoeOrderPo moeOrder = order.getMoeOrder();
        moeOrder.setLastUpdDate(now);
        if (userDto.getActionCd() != null && userDto.getActionCd().equalsIgnoreCase(ServerConstant.MODE_EDIT_WITH_REMARK)) {
            order.setRemarkHosp(userDto.getHospitalCd());
            order.setRemarkRank(userDto.getUserRankCd());
            order.setRemarkRankDesc(userDto.getUserRankDesc());
            order.setRemarkUser(userDto.getLoginName());
            order.setRemarkUserId(userDto.getLoginId());

            if (oorder != null) {
                order.setUpdateUserId(oorder.getUpdateUserId());
                order.setUpdateUser(oorder.getUpdateUser());
                order.setUpdateHosp(oorder.getUpdateHosp());
                order.setUpdateRank(oorder.getUpdateRank());
                order.setUpdateRankDesc(oorder.getUpdateRankDesc());
                //moeOrder.setLastUpdDate(oorder.getMoeOrder().getLastUpdDate());
                moeOrder.setLastUpdBy(oorder.getMoeOrder().getLastUpdBy());
            } else {
                // Delete order
                //moeOrder.setLastUpdDate(now);
                moeOrder.setLastUpdBy(userDto.getLoginId());
            }
        } else {
            order.setUpdateUserId(userDto.getLoginId());
            order.setUpdateUser(userDto.getLoginName());
            order.setUpdateHosp(userDto.getHospitalCd());
            order.setUpdateRank(userDto.getUserRankCd());
            order.setUpdateRankDesc(userDto.getUserRankDesc());

            order.setRemarkHosp(null);
            order.setRemarkRank(null);
            order.setRemarkRankDesc(null);
            order.setRemarkUser(null);
            order.setRemarkUserId(null);

            //moeOrder.setLastUpdDate(now);
            moeOrder.setLastUpdBy(userDto.getLoginId());
        }
        if (setCreateInfo) {
            moeOrder.setHospcode(userDto.getHospitalCd());
            moeOrder.setPatHospcode(userDto.getHospitalCd());
            if (moeOrder.getBackDate() != null) {
                //Chris 20190822  -Start
                //SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                //Chris 2190822  -End
                try {
                    moeOrder.setOrdDate(sdf.parse(moeOrder.getBackDate()));
                } catch (ParseException e) {

                }
            } else {
                moeOrder.setOrdDate(now);
            }
            moeOrder.setOrdType(String.valueOf('P'));
            moeOrder.setOrdStatus(ServerConstant.ORDER_STATUS_CREATE);
            moeOrder.setMoCode(userDto.getLoginId());
            //chris 20190724 start
            moeOrder.setCaseNo(userDto.getMrnPatientEncounterNum());
//            moeOrder.setCaseNo(userDto.getCaseNo());  //MOE1 code
            //chris 20190724 end
            moeOrder.setIpasWard(userDto.getWard());
            moeOrder.setBedNo(userDto.getBedNum());
            moeOrder.setSpecialty(userDto.getSpec());
            if (userDto.getSourceSystem() != null) {
                moeOrder.setOrdSubtype(ServerConstant.SOURCE_SYSTEM_ORD_SUBTYPE_MAP.get(userDto.getSourceSystem()));
            }
            if (moeOrder.getBackDate() != null) {
                moeOrder.setOrdSubtype(String.valueOf('B'));
            }
        }
    }


    private void updateProfile(MoeMedProfilePo profile, final UserDto userDto, final Date now, boolean setCreateInfo) {
        if (setCreateInfo) {
            profile.setHospcode(userDto.getHospitalCd());
            //chris 20190724 start
            profile.setCaseNo(userDto.getMrnPatientEncounterNum());
            //profile.setCaseNo(userDto.getCaseNo());     //MOE1 code
            //chris 20190724 end
            profile.setPatHospcode(userDto.getHospitalCd());
        }
        profile.setUpdateBy(userDto.getLoginId());
        profile.setUpdateTime(new Time(now.getTime()));

        MoeEhrMedProfilePo ehrProfile = profile.getMoeEhrMedProfile();
        if (setCreateInfo) {
            // Ricci 20190722 start --
            ehrProfile.setHospcode(userDto.getHospitalCd());
            // Ricci 20190722 end --
            ehrProfile.setCreateUserId(userDto.getLoginId());
            ehrProfile.setCreateUser(userDto.getLoginName());
            ehrProfile.setCreateHosp(userDto.getHospitalCd());
            ehrProfile.setCreateRank(userDto.getUserRankCd());
            ehrProfile.setCreateRankDesc(userDto.getUserRankDesc());
            ehrProfile.setCreateDtm(new Time(now.getTime()));
        }
        ehrProfile.setUpdateUserId(userDto.getLoginId());
        ehrProfile.setUpdateUser(userDto.getLoginName());
        ehrProfile.setUpdateHosp(userDto.getHospitalCd());
        ehrProfile.setUpdateRank(userDto.getUserRankCd());
        ehrProfile.setUpdateRankDesc(userDto.getUserRankDesc());
        ehrProfile.setUpdateDtm(new Time(now.getTime()));

        for (MoeMedMultDosePo dose : profile.getMoeMedMultDoses()) {
            if (setCreateInfo) {
                // Ricci 20190722 start --
                MoeEhrMedMultDosePo ehrMedMultDosePo = dose.getMoeEhrMedMultDose();
                dose.setHospcode(userDto.getHospitalCd());
                ehrMedMultDosePo.setHospcode(userDto.getHospitalCd());
                // Ricci 20190722 end --
                dose.setPatHospcode(userDto.getHospitalCd());
            }
            dose.setUpdateBy(userDto.getLoginId());
            dose.setUpdateTime(new Time(now.getTime()));
        }
    }

    private void removeDeletedItems(MoeEhrOrderPo order) {
        ArrayList<MoeMedProfilePo> listToDelete = new ArrayList<MoeMedProfilePo>();

//        boolean backFlag = false;

        for (MoeMedProfilePo profile : order.getMoeOrder().getMoeMedProfiles()) {

            if (profile.getItemStatus().toUpperCase().equals("D")) {
                listToDelete.add(profile);
            }

        }

        if (listToDelete.size() > 0) {
            order.getMoeOrder().getMoeMedProfiles().removeAll(listToDelete);

            updateProfilesOrgItemNoSort(order);
        }

    }


    //Chris 20190730 Update Profiles's org_item_no asc after delete profiles action -Start
    private void updateProfilesOrgItemNoSort(MoeEhrOrderPo order) {
        //Update profiles's org_item_no(sort) After delete profile action

        //Start index
        long newOrgItemNo = 1L;

        for (MoeMedProfilePo profile : order.getMoeOrder().getMoeMedProfiles()) {
            //Update MoeMedProfiles's OrgItemNo asc
            profile.setOrgItemNo((long) (newOrgItemNo++));
        }

    }
    //Chris 20190730 Update Profiles's org_item_no asc after delete profiles action -End


    private MoeEhrOrderLogPo setUpdateInfo(final MoeEhrOrderPo order, MoeEhrOrderPo newOrder, final UserDto userDto) {
        long maxCmsItemNo = 0;
        for (MoeMedProfilePo profile : order.getMoeOrder().getMoeMedProfiles()) {
            if (profile.getCmsItemNo() > maxCmsItemNo) {
                maxCmsItemNo = profile.getCmsItemNo();
            }
        }

        Date now = new Date();

        updateOrder(order, newOrder, userDto, now, false);
        newOrder.getMoeOrder().setOrdDate(order.getMoeOrder().getOrdDate());

        // Log Order
        //MoeEhrOrderLogPo orderLog = OrderLogUtil.convertMoeEhrOrderLog(newOrder, userDto, 'D', false);
        MoeEhrOrderLogPo orderLog = OrderLogUtil.convertMoeEhrOrderLog(newOrder, userDto, ServerConstant.ORDER_TRANSACTION_TYPE_DELETE, false);

        for (MoeMedProfilePo newProfile : newOrder.getMoeOrder().getMoeMedProfiles()) {
            // new Profile
            if (newProfile.getUpdateBy() == null) {
                newProfile.setCmsItemNo(++maxCmsItemNo);
                updateProfile(newProfile, userDto, now, true);

                //Chris 20191022  SetCmsItemNo  -Start
                if (newProfile.getMoeEhrMedProfile() != null)
                    newProfile.getMoeEhrMedProfile().setCmsItemNo(newProfile.getCmsItemNo());
                //Chris 20191022  SetCmsItemNo  -End

                // insert log
                //addNewMedProfileToOrderLog(newProfile, orderLog, 'I');
                addNewMedProfileToOrderLog(newProfile, orderLog, ServerConstant.ORDER_TRANSACTION_TYPE_INSERT);
            }
        }

        for (MoeMedProfilePo profile : order.getMoeOrder().getMoeMedProfiles()) {

            for (MoeMedProfilePo newProfile : newOrder.getMoeOrder().getMoeMedProfiles()) {
                if (profile.getHospcode().equals(newProfile.getHospcode()) &&
                        profile.getOrdNo() == newProfile.getOrdNo() &&
                        profile.getCmsItemNo() == newProfile.getCmsItemNo()) {
                    //if (newProfile.getItemStatus() != String.valueOf('D')) {
                    if (!newProfile.getItemStatus().equals(ServerConstant.PROFILE_ITEM_STATUS_DELETE)) {
                        if (!newProfile.equals(profile)) {
                            copyProfile(newProfile, profile, false);
                            updateProfile(newProfile, userDto, now, false);
                            // update log
                            //addNewMedProfileToOrderLog(newProfile, orderLog, 'U');
                            addNewMedProfileToOrderLog(newProfile, orderLog, ServerConstant.ORDER_TRANSACTION_TYPE_UPDATE);
                        } else {
                            copyProfile(newProfile, profile, true);
                            // n/a log
                            //addNewMedProfileToOrderLog(newProfile, orderLog, 'N');
                            addNewMedProfileToOrderLog(newProfile, orderLog, ServerConstant.ORDER_TRANSACTION_TYPE_N_A);
                        }
                    } else {
                        copyProfile(newProfile, profile, false);
                        updateProfile(newProfile, userDto, now, false);
                        // delete log
                        //addNewMedProfileToOrderLog(newProfile, orderLog, 'D');
                        addNewMedProfileToOrderLog(newProfile, orderLog, ServerConstant.ORDER_TRANSACTION_TYPE_DELETE);
                    }
                    break;
                }
            }
        }

        return orderLog;

    }


    //private void addNewMedProfileToOrderLog(MoeMedProfilePo newProfile, MoeEhrOrderLogPo orderLog, char trxType) {
    private void addNewMedProfileToOrderLog(MoeMedProfilePo newProfile, MoeEhrOrderLogPo orderLog, String trxType) {
        if (newProfile == null || orderLog == null) return;
        MoeMedProfileLogPo profileLog = OrderLogUtil.convertMoeMedProfileLog(newProfile, orderLog.getMoeOrderLog());
        //Chris 20190724 Start
        profileLog.getMoeEhrMedProfileLog().setTrxType(trxType);
        //profileLog.getMoeEhrMedProfileLog().setTrxType(trxType + "");
        //Chris 20190724 End
        orderLog.getMoeOrderLog().getMoeMedProfileLogs().add(profileLog);
    }


    private void copyProfile(MoeMedProfilePo newProfile, final MoeMedProfilePo profile, boolean setUpdateInfo) {
        MoeEhrMedProfilePo newEhrProfile = newProfile.getMoeEhrMedProfile();
        MoeEhrMedProfilePo ehrProfile = profile.getMoeEhrMedProfile();

        newEhrProfile.setCreateUserId(ehrProfile.getCreateUserId());
        newEhrProfile.setCreateUser(ehrProfile.getCreateUser());
        newEhrProfile.setCreateHosp(ehrProfile.getCreateHosp());
        newEhrProfile.setCreateRank(ehrProfile.getCreateRank());
        newEhrProfile.setCreateRankDesc(ehrProfile.getCreateRankDesc());
        newEhrProfile.setCreateDtm(ehrProfile.getCreateDtm());
        if (setUpdateInfo) {
            newProfile.setUpdateBy(profile.getUpdateBy());
            newProfile.setUpdateTime(profile.getUpdateTime());

            newEhrProfile.setUpdateUserId(ehrProfile.getUpdateUserId());
            newEhrProfile.setUpdateUser(ehrProfile.getUpdateUser());
            newEhrProfile.setUpdateHosp(ehrProfile.getUpdateHosp());
            newEhrProfile.setUpdateRank(ehrProfile.getUpdateRank());
            newEhrProfile.setUpdateRankDesc(ehrProfile.getUpdateRankDesc());
            newEhrProfile.setUpdateDtm(ehrProfile.getUpdateDtm());

            for (MoeMedMultDosePo dose : newProfile.getMoeMedMultDoses()) {
                dose.setUpdateBy(profile.getUpdateBy());
                dose.setUpdateTime(profile.getUpdateTime());
            }
        }
    }


    @Override
    public MoeEhrOrderDto getMoeOrder(String hospcode, String patientKey, String caseNo, long ordNo) throws Exception {

        if (StringUtil.stripToEmpty(patientKey).length() == 0 || ordNo <= 0) {
            //return null;
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("param.Invalid"));
            throw new MoeServiceException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 end--
        }

        MoeEhrOrderDto ehrOrderDto = null;
        List<MoeEhrOrderPo> orders = moeEhrOrderRepository.findByPatientKeyAndOrderNo(patientKey, ordNo);
        if (orders.size() == 0) {
            //eric 20191219 start--
//            throw new RecordException(localeMessageSourceUtil.getMessage("records.notExisted"));
            throw new RecordException(ResponseCode.SpecialMessage.RECORDS_NOT_EXISTED.getResponseCode(),ResponseCode.SpecialMessage.RECORDS_NOT_EXISTED.getResponseMessage());
            //eric 20191219 end--
        }

        if (0 == orders.get(0).getMoeOrder().getMoeMedProfiles().size()) { //Cannot find order with existent profiles
            //Chris ConvertMoeEhrOrderDto without profiles / orderOnly:true 20190821  -Start
            ehrOrderDto = DtoMapUtil.convertMoeEhrOrderDto(orders.get(0), true, getUserDto());
            //Chris ConvertMoeEhrOrderDto without profiles / orderOnly:true 20190821  -End
        } else {
            MoeEhrOrderPo order = orders.get(0);
            if (order != null && order.getHospcode().equals(hospcode) &&
                    order.getMoePatientCase().getMoeCaseNo().equals(caseNo)) {
                // Simon 20190814 start-- add param userDto for get prep lsit
                ehrOrderDto = DtoMapUtil.convertMoeEhrOrderDto(order, false, getUserDto());
                // Simon 20190814 end-- add param userDto for get prep lsit
            }
        }

        return ehrOrderDto;
    }


    // Simon 20190730 --start add for drug history
    @Override
    public List<MoeEhrOrderDto> getMoeOrder(String withinMonths, String prescType) throws Exception {

        // Ricci 20190805 start -- change to token -- comment
        /*UserDto userDto = (UserDto) HttpContextUtil.getSessionAttribute(ServerConstant.SESS_ATTR_USER_INFO);
        if (userDto == null) {
            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.session.error"));
        }*/
        UserDto userDto = getUserDto();
        // Ricci 20190805 end --
        List<MoeEhrOrderDto> orders;
        if (!ServerConstant.DRUG_HISTORY_WITHIN_MONTH_ALL.equals(withinMonths)) {
            //for special period
            int withinMonth = Integer.parseInt(withinMonths);
            // Ricci 20190820 replace with constant start --
            if (ServerConstant.MODE_ENQUIRY.equalsIgnoreCase(userDto.getActionCd())) {
                // Ricci 20190820 replace with constant end --
                orders = moeOrderFacade.getMoeOrder(userDto, withinMonth, prescType, userDto.getHospitalCd());
            } else {
                orders = moeOrderFacade.getMoeOrder(userDto, withinMonth, prescType, null);
            }
        } else {
            //for default period [all records will be shown]
            // Ricci 20190820 replace with constant start --
            if (ServerConstant.MODE_ENQUIRY.equalsIgnoreCase(userDto.getActionCd())) {
                // Ricci 20190820 replace with constant end --
                orders = moeOrderFacade.getMoeOrder(userDto, prescType, userDto.getHospitalCd());
            } else {
                orders = moeOrderFacade.getMoeOrder(userDto, prescType, null);
            }
        }
        return orders;
    }
    // Simon 20190730 --end add for drug history


    //Chris 20190730 Delete Moe Order -Start
    @Override
    @Transactional(rollbackFor = Exception.class)
    //public MoeEhrOrderPo deleteMoeOrder(MoeEhrOrderDto moeEhrOrderDto, UserDto userDto) throws Exception {
    public MoeEhrOrderPo deleteMoeOrder(MoeEhrOrderDto moeEhrOrderDto) throws Exception {

        // Ricci 20190805 start -- change to token -- comment
        UserDto userDto = getUserDto();
        // Ricci 20190805 end --


        if (moeEhrOrderDto == null || moeEhrOrderDto.getOrdNo() == null || moeEhrOrderDto.getMoeOrder() == null) {
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("param.Invalid"));
            throw new MoeServiceException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 end--
        }


        String patientKey = userDto.getMoePatientKey();
        long ordNo = moeEhrOrderDto.getOrdNo();


        //Chris  20190815  Set Remark Info   -Start
//        if (null != moeEhrOrderDto.getMoeOrder().getRemarkText()) {

        if (null != moeEhrOrderDto.getMoeOrder().getRemarkText()) {
            //boolean outOfLength = StringUtils.isBlank(moeEhrOrderDto.getMoeOrder().getRemarkText()) ? true : moeEhrOrderDto.getMoeOrder().getRemarkText().length() > 100;
            boolean outOfLength = StringUtils.isBlank(moeEhrOrderDto.getMoeOrder().getRemarkText()) ? true : moeEhrOrderDto.getMoeOrder().getRemarkText().length() > ServerConstant.LENGTH_100;
            if (outOfLength) {
                //eric 20191220 start--
//                throw new ParamException(localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
                throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                        ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
                //eric 20191220 end--
            }

            moeEhrOrderDto.getMoeOrder().setRemarkText(moeEhrOrderDto.getMoeOrder().getRemarkText().trim());


            moeEhrOrderDto.getMoeOrder().setRemarkText(moeEhrOrderDto.getMoeOrder().getRemarkText().trim());

            String name = userDto.getLoginName() != null ? userDto.getLoginName() : userDto.getLoginId();
            moeEhrOrderDto.getMoeOrder().setRemarkCreateBy(name);
            moeEhrOrderDto.getMoeOrder().setRemarkCreateDate(new Date());
            //moeEhrOrderDto.getMoeOrder().setRemarkStatus("Y");
            moeEhrOrderDto.getMoeOrder().setRemarkStatus(ServerConstant.ORDER_REMARK_STATUS_Y);
        }
        //Chris  20190815  Set Remark Info   -End


        if (StringUtil.stripToEmpty(patientKey).length() == 0 || ordNo <= 0 || userDto == null) {
            //eric 20191219 start--
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("param.Invalid"));
            throw new MoeServiceException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 end--
        }

        List<MoeEhrOrderPo> orders = moeEhrOrderRepository.findByPatientKeyAndOrderNo(patientKey, ordNo);
        if (orders == null || 0 == orders.size()) {
            //Cannot find order
            //throw new MoeServiceException(localeMessageSourceUtil.getMessage("records.prescription.notExisted"));
            //eric 20191223 start--
//            throw new RecordException(localeMessageSourceUtil.getMessage("records.prescription.notExisted"));
            throw new RecordException(ResponseCode.SpecialMessage.RECORDS_PRESCRIPTION_NOTEXISTED.getResponseCode(),
                    ResponseCode.SpecialMessage.RECORDS_PRESCRIPTION_NOTEXISTED.getResponseMessage());
            //eric 20191223 end--
        } else {
            MoeEhrOrderPo order = orders.get(0);

            setDeleteInfo(moeEhrOrderDto, order, userDto);
            order.getMoeOrder().setRefNo(Formatter.getRefNo(order.getMoeOrder().getOrdNo(), order.getMoeOrder().getVersion() + 1));

            // Log Order
            //MoeEhrOrderLogPo orderLog = OrderLogUtil.convertMoeEhrOrderLog(order, userDto, 'D', true);
            MoeEhrOrderLogPo orderLog = OrderLogUtil.convertMoeEhrOrderLog(order, userDto, ServerConstant.ORDER_TRANSACTION_TYPE_DELETE, true);

            MoeEhrOrderPo tempOrder = DtoMapUtil.cloneMoeEhrOrder(order, userDto);
            tempOrder.getMoeOrder().getMoeMedProfiles().removeAll(tempOrder.getMoeOrder().getMoeMedProfiles());

            moeOrderRepository.save(order.getMoeOrder());
            moeEhrOrderRepository.saveAndFlush(order);

            moeOrderLogRepository.saveAndFlush(orderLog.getMoeOrderLog());
            moeEhrOrderLogRepository.saveAndFlush(orderLog);

            // Ricci 20191106 start -- remove redis cache
            String redisKey = String.format(ServerConstant.MOE_CACHE_ORDER_PREFIX,
                    userDto.getMrnPatientIdentity(), userDto.getMrnPatientEncounterNum());
            moeContentService.deleteOrder(redisKey);
            // Ricci 20191106 end --
            moeContentService.tryReleasePrescription(0);
            return order;

        }

    }


    private void setDeleteInfo(MoeEhrOrderDto moeEhrOrderDto, MoeEhrOrderPo order, final UserDto userDto) {
        setDeleteInfo(order, userDto);
        setDeleteRemarkInfo(moeEhrOrderDto, order);
        for (MoeMedProfilePo profile : order.getMoeOrder().getMoeMedProfiles()) {
            profile.setRemarkCreateBy(moeEhrOrderDto.getMoeOrder().getRemarkCreateBy());
            profile.setRemarkCreateDate(moeEhrOrderDto.getMoeOrder().getRemarkCreateDate());
            profile.setRemarkText(moeEhrOrderDto.getMoeOrder().getRemarkText());
        }
    }


    private void setDeleteInfo(MoeEhrOrderPo order, final UserDto userDto) {
        Date now = new Date();

        updateOrder(order, userDto, now, false);

        order.getMoeOrder().setOrdStatus(ServerConstant.ORDER_STATUS_DELETE);
        for (MoeMedProfilePo profile : order.getMoeOrder().getMoeMedProfiles()) {
            //profile.setItemStatus("D");
            profile.setItemStatus(ServerConstant.PROFILE_ITEM_STATUS_DELETE);
        }
    }


    private void setDeleteRemarkInfo(MoeEhrOrderDto moeEhrOrderDto, MoeEhrOrderPo order) {
        order.getMoeOrder().setRemarkCreateDate(moeEhrOrderDto.getMoeOrder().getRemarkCreateDate());
        order.getMoeOrder().setRemarkStatus(moeEhrOrderDto.getMoeOrder().getRemarkStatus());
        order.getMoeOrder().setRemarkText(moeEhrOrderDto.getMoeOrder().getRemarkText());
        order.getMoeOrder().setRemarkCreateBy(moeEhrOrderDto.getMoeOrder().getRemarkCreateBy());
    }
    //Chris 20190730 Delete Moe Order -End


    //Simon 20190731 --start for calculate dangerous drug max dosage
    @Override
    public InnerMaxDosOfDdDto calculateMaxDosageOfDangerDrug(InnerMoeMedProfileDto dto) throws Exception {
        // Ricci 20190805 start -- change to token -- comment
        /*UserDto userDto = (UserDto) HttpContextUtil.getSessionAttribute(ServerConstant.SESS_ATTR_USER_INFO);
        if (userDto == null) {
            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.session.error"));
        }*/
        UserDto userDto = getUserDto();
        // Ricci 20190805 end --
        Long defaultQty = 0L;
        //Simon 20190904 checking dupulicate multDoseNo start--
        Set<Long> multDoseNo = new LinkedHashSet<>();
        if (dto.getMoeMedMultDoses() != null) {
            for (InnerMoeMedMultDoseDto innerDto : dto.getMoeMedMultDoses())
                multDoseNo.add(innerDto.getMultDoseNo());
            if (multDoseNo.size() != dto.getMoeMedMultDoses().size())
                //eric 20191220 start--
//                throw new ParamException(localeMessageSourceUtil.getMessage("moeOrder.duplicate.multDoseNo"));
                throw new ParamException(ResponseCode.SpecialMessage.MOEORDER_DUPLICATE_MULTDOSENO.getResponseCode(),
                        ResponseCode.SpecialMessage.MOEORDER_DUPLICATE_MULTDOSENO.getResponseMessage());
            //eric 20191220 end--
        }

        //Simon 20190904 end--


        String enable_max_dd_qty_check = SystemSettingUtil.systemSettingMap.get("enable_max_dd_qty_check").getParamValue();
        List<InnerMaxDosDto> maxDosageList = new ArrayList<>();
        if (DtoModeChangeHelper.isNormalAndAdvance(dto)) {
            if ("Y".equalsIgnoreCase(enable_max_dd_qty_check)) {
                InnerMaxDosDto maxDosage = new InnerMaxDosDto();
                defaultQty = (QuantityCalculator.calculateQuantity(dto, 0)).longValue();
                maxDosage.setMultDoseNo(0L);
                maxDosage.setMaxDosage(defaultQty);
                maxDosageList.add(maxDosage);
            }
        } else {
            if ("Y".equalsIgnoreCase(enable_max_dd_qty_check)) {
                InnerMaxDosDto maxDosage = null;
                for (int i = 0; i < dto.getMoeMedMultDoses().size(); i++) {
                    try {
                        maxDosage = new InnerMaxDosDto();
                        Integer tempQty = QuantityCalculator.calculateQuantity(dto, i).intValue();
                        maxDosage.setMultDoseNo(dto.getMoeMedMultDoses().get(i).getMultDoseNo());
                        maxDosage.setMaxDosage(tempQty.longValue());
                        defaultQty += tempQty;
                        maxDosageList.add(maxDosage);
                        maxDosage = null;
                    } catch (Exception e) {
                        //eric 20191220 start--
//                        throw new ParamException(localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
                        throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                                ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
                        //eric 20191220 end--
                    }
                }
            }
        }
        Long key = null;
        InnerMaxDosOfDdDto innerMaxDosOfDdDto = new InnerMaxDosOfDdDto();
        innerMaxDosOfDdDto.setMaxDosage(maxDosageList);
        innerMaxDosOfDdDto.setTotalMaxDosage(defaultQty);

        return innerMaxDosOfDdDto;
    }
    //Simon 20190731 --end for calculate dangerous drug max dosage


    //Chris 20190806 Generate special interval text  -Start
    @Override
    public Map<String, Object> generateSpecialIntervalText(InnerSpecialIntervalDto specialIntervalDto) throws Exception {
        //public ArrayList<String> generateSpecialIntervalText(InnerSpecialIntervalDto specialIntervalDto) throws Exception {
        Long supFreq1 = specialIntervalDto.getSupFreq1();
        if (supFreq1 != null && supFreq1 == 0) supFreq1 = null;
        Long supFreq2 = specialIntervalDto.getSupFreq2();
        if (supFreq2 != null && supFreq2 == 0) supFreq2 = null;
        Long cycleMultiplier = specialIntervalDto.getCycleMultiplier();
        if (cycleMultiplier != null && cycleMultiplier == 0) cycleMultiplier = null;

        MoeMedProfileDto moeMedProfileDto = new MoeMedProfileDto();
        MoeEhrMedProfileDto moeEhrMedProfileDto = new MoeEhrMedProfileDto();

        moeMedProfileDto.setSupFreqCode(specialIntervalDto.getSupFreqCode());
        moeMedProfileDto.setRegimen(specialIntervalDto.getRegimen());
        moeMedProfileDto.setSupFreq1(supFreq1);
        moeMedProfileDto.setSupFreq2(supFreq2);
        moeMedProfileDto.setDayOfWeek(specialIntervalDto.getDayOfWeek());
        moeEhrMedProfileDto.setCycleMultiplier(cycleMultiplier);
        moeMedProfileDto.setMoeEhrMedProfile(moeEhrMedProfileDto);

        //Chris  Support to return flag of displayWithFreq  20190828  -Start
        Map<String, Object> resultMap = new HashMap<String, Object>();

        //Chris  Support to return "displayWithFreq" Result List and "specialIntervalText" Result List 20190828  -Start
        List<String> displayWithFreqResultList = new ArrayList<String>();
        List<String> specialIntervalResultList = new ArrayList<String>();


        //Chris 20190909
        //moeMedProfileDto.setFreqCode(specialIntervalDto.getFreqCode());
        if (specialIntervalDto.getFreqCode() != null && specialIntervalDto.getFreqCode().size() > 0) {
            for (int i = 0; i < specialIntervalDto.getFreqCode().size(); i++) {
                moeMedProfileDto.setFreqCode(specialIntervalDto.getFreqCode().get(i));
                // Ricci 20191112 start --
                Map tempMap = Formatter.toRecurText(moeMedProfileDto);
                //Handle specialInterval for title display
                if (tempMap != null) {
                    resultMap = tempMap;
                    if (resultMap.get("specialIntervalText") != null) {
                        try {
                            String currentLineSpecialIntervalText = ((List<String>) resultMap.get("specialIntervalText")).get(i);
                            specialIntervalResultList.add(currentLineSpecialIntervalText);
                        } catch (IndexOutOfBoundsException e) {
                            //eric 20191219 start--
//                            throw new ParamException(localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
                            throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
                            //eric 20191219 end--
                        }
                    }

                    //Handle displayWithFreq for title display
                    if (resultMap.get("displayWithFreq") != null) {
                        displayWithFreqResultList.add((String) resultMap.get("displayWithFreq"));
                    }
                }
                // Ricci 20191112 end --
            }

        } else {
            //FreqCode parameter is null
            //eric 20191219 start--
//            throw new ParamException(localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 end--
        }
        //Chris 20190909

        // Ricci 20191112 start --
        resultMap.put("displayWithFreq", displayWithFreqResultList);
        resultMap.put("specialIntervalText", specialIntervalResultList);

        // Ricci 20191112 end --
        //Chris  Support to return "displayWithFreq" Result List and "specialIntervalText" Result List 20190828  -End

        return resultMap;
        //Chris  Support to return flag of displayWithFreq  20190828  -End
    }
    //Chris 20190806 Generate special interval text  -End


    // Simon 20190813 --start copy from GWT
//    private List<PreparationDto> getStrengths(String vtm, String formEng, String routeEng, String tradeName, String localDrugId) throws Exception {
//        UserDto userDto = getUserDto();
//        String key = getStrengthKey(vtm, formEng, routeEng, tradeName);
//        List<PreparationDto> preparationDtos = DtoUtil.PREPARATION_MAP.get(key);
//        if (preparationDtos == null || preparationDtos.size() == 0)
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("records.notExisted"));
//        List<String> strings = DtoUtil.HKREG_BY_SPECIALTY.get(userDto.getHospitalCd() + " " + userDto.getSpec());
//        List<PreparationDto> resultDtos = new ArrayList<>();
//        for (PreparationDto dto : preparationDtos) {
//            if (strings == null || (strings != null || !strings.contains(localDrugId))) {
//                resultDtos.add(dto);
//            }
//        }
//        return resultDtos;
//    }
//    private String getStrengthKey(String vtm, String formEng, String routeEng, String tradeName) {
//        StringBuilder sb = new StringBuilder();
//        if (vtm != null || !vtm.equals("")) sb.append(vtm);
//        if (formEng != null || !formEng.equals("")) sb.append(" " + formEng);
//        if (routeEng != null || !routeEng.equals("")) sb.append(" " + routeEng);
//        if (tradeName != null || !tradeName.equals("")) sb.append(" " + tradeName);
//        return sb.toString();
//    }
//    private List<PreparationDto> setSelectedPreparation(List<PreparationDto> sourceList,String strength,String strengthExtraInfo){
//        for (PreparationDto preparationDto : sourceList) {
//            if (strengthExtraInfo != null) {
//                if (strength.equals(preparationDto.getStrength()) && strengthExtraInfo.equals(preparationDto.getStrengthLevelExtraInfo())) {
//                    preparationDto.setSelected(true);
//                }
//            } else {
//                if (strength.equals(preparationDto.getStrength()))
//                    preparationDto.setSelected(true);
//            }
//        }
//        return sourceList;
//    }

    @Override
    public MoeMedProfileDto convertMedProfileDTO(MoeDrugDto drugDTO) throws Exception {
        MoeMedProfileDto moeMedProfileDto = null;
        List<PreparationDto> preparationDtos = null;
        String strengthLevelExtraInfo = null;
        String strength = null;
        UserDto userDto = getUserDto();
        try {
            moeMedProfileDto = DtoMapUtil.convertMedProfileDTO(drugDTO, userDto);
            //preparationDtos = DtoMapUtil.getStrengths(drugDTO.getVtm(),
            //        drugDTO.getFormEng(),
            //        drugDTO.getRouteEng(),
            //        drugDTO.getTradeName(),
            //        userDto);
            //strengthLevelExtraInfo = drugDTO.getStrengths().get(0).getStrengthLevelExtraInfo();
            //strength = drugDTO.getStrengths().get(0).getStrength();

            //Chris 20191009 Return max duration from cache  -Start
            moeMedProfileDto.setMaxDuration(DtoMapUtil.getMaxDuration(moeMedProfileDto) != null ? DtoMapUtil.getMaxDuration(moeMedProfileDto) : null);
            //Chris 20191009 Return max duration from cache  -End

            //Chris 20191011 Return min dosage from cache  -Start
            moeMedProfileDto.setMinDosages(DtoMapUtil.getMinDosage(moeMedProfileDto) != null ? DtoMapUtil.getMinDosage(moeMedProfileDto) : null);
            //Chris 20191011 Return min dosage from cache  -End

            //Chris 20191014 Return min dosages message  -Start
            moeMedProfileDto.setMinDosagesMessage(DtoMapUtil.getMinDosageString(moeMedProfileDto) != null ? DtoMapUtil.getMinDosageString(moeMedProfileDto) : null);
            //Chris 20191014 Return min dosages message  -End

        } catch (NullPointerException nullException) {
            //eric 20191219 start--
//            throw new ParamException(localeMessageSourceUtil.getMessage("param.Invalid"));
            throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191219 end--
        }
        //moeMedProfileDto.setPrep(DtoMapUtil.setSelectedPreparation(preparationDtos,strength,strengthLevelExtraInfo));
        return moeMedProfileDto;
    }
    // Simon 20190813 --start from GWT

    // Ricci 20190814 start --
    @Override
    public List<Object[]> getMoeOrderNoForLogin(String hospitalCd, String moePatientKey, String moeCaseNo) throws Exception {

        return moeEhrOrderRepository.findMoeOrderNoForLogin(hospitalCd, moePatientKey, moeCaseNo);
    }
    // Ricci 20190814 end --

    @Override
    public List<Object[]> getMoeOrderNoForLogin(String hospitalCd, String moePatientKey, String moeCaseNo, long orderNo) throws Exception {

        return moeEhrOrderRepository.findMoeOrderNoForLogin(hospitalCd, moePatientKey, moeCaseNo, orderNo);
    }

    // Ricci 20190916 start --
    @Override
    public List<MoeMedicationDecisionResultDto> allergyChecking(MoeMedicationDecisionDto moeDto) throws Exception {

        // Simon 20191014 login is unneccessary when moe call dac start--
        // Ricci 20190926 start --
        //String dacToken = loginDac();
        // Ricci 20190926 end --
        //Simon 20191014 end--


        MedicationDecisionRequestDto requestDto = WsDtoMapUtil.convertMedicationRequest(moeDto);
        String acceptId = addAcceptDrugCheckingRecord(requestDto);
        CimsResponseVo<MedicationDecisionResponseDto> dacResponse = dacRestService.medicationDecision(requestDto);

        List<MoeMedicationDecisionResultDto> resultDtos = new ArrayList<>();
        MedicationDecisionResponseDto decision = dozerBeanMapper.map(dacResponse.getData(), MedicationDecisionResponseDto.class);
        // Ricci 20190918 start support front-end show message--
        Map<String, InnerMoePatientAllergyDto> moePatientAllergyDtoMap = new HashMap<>();
        Map<String, MoePatientMedicationDto> moePatientMedicationDtoMap = new HashMap<>();
        List<InnerMoePatientAllergyDto> moePatientAllergyDtos = moeDto.getPatientAllergies();
        List<MoePatientMedicationDto> moePatientMedicationDtos = moeDto.getPatientMedications();

        for (InnerMoePatientAllergyDto moePatientAllergyDto : moePatientAllergyDtos) {
            moePatientAllergyDtoMap.put(moePatientAllergyDto.getAllergySeqNo(), moePatientAllergyDto);
        }
        for (MoePatientMedicationDto moePatientMedicationDto : moePatientMedicationDtos) {
            moePatientMedicationDtoMap.put(moePatientMedicationDto.getOrderLineRowNum(), moePatientMedicationDto);
        }

        MoePatientMedicationDto moePatientMedicationDto = null;
        MoeMedicationDecisionResultDto moeMedicationDecisionResultDto = null;
        for (MedicationDecisionResultDto medicationDecisionResultDto : decision.getMedicationDecisions()) {
            String orderLineRowNum = medicationDecisionResultDto.getOrderLineRowNum();
            moePatientMedicationDto = moePatientMedicationDtoMap.get(orderLineRowNum);
            moeMedicationDecisionResultDto = new MoeMedicationDecisionResultDto();

            for (ReactionAllergenDto reactionAllergenDto : medicationDecisionResultDto.getReactionAllergens()) {

                InnerMoePatientAllergyDto tmpMoePatientAllergyDto = moePatientAllergyDtoMap.get(reactionAllergenDto.getAllergySeqNo());

                if (tmpMoePatientAllergyDto != null) {
                    InnerMoePatientAllergyDto moePatientAllergyDto = new InnerMoePatientAllergyDto();

                    moePatientAllergyDto.setAllergen(tmpMoePatientAllergyDto.getAllergen());
                    moePatientAllergyDto.setAllergenTermID(tmpMoePatientAllergyDto.getAllergenTermID());
                    moePatientAllergyDto.setAllergenType(tmpMoePatientAllergyDto.getAllergenType());
                    moePatientAllergyDto.setAllergySeqNo(tmpMoePatientAllergyDto.getAllergySeqNo());
                    moePatientAllergyDto.setAdditionalInfo(tmpMoePatientAllergyDto.getAdditionalInfo());
                    moePatientAllergyDto.setDisplayName(tmpMoePatientAllergyDto.getDisplayName());
                    moePatientAllergyDto.setAllergyReactions(tmpMoePatientAllergyDto.getAllergyReactions());
                    moePatientAllergyDto.setCertainty(tmpMoePatientAllergyDto.getCertainty().toUpperCase());
                    moePatientAllergyDto.setReactionLevel(reactionAllergenDto.getReactionLevel());
                    moeMedicationDecisionResultDto.getPatientAllergies().add(moePatientAllergyDto);
                }

            }
            moeMedicationDecisionResultDto.setAcceptId(acceptId);
            moeMedicationDecisionResultDto.setPatientMedication(moePatientMedicationDto);
            resultDtos.add(moeMedicationDecisionResultDto);
        }
        // Ricci 20190918 end support front-end show message--
        //List<MoeMedicationDecisionResultDto> resultDtos = WsDtoMapUtil.convertMoeDacList(decision.getMedicationDecisions(), moeDto, acceptId);

        return resultDtos;
    }

    @Override
    public MoePatientSummaryDto getPatientSummaryByPatientRefKey() throws Exception {

        UserDto userDto = getUserDto();
        SummaryPageResponse response = saamRestService.getPatientSummaryByPatientRefKey(userDto.getMrnPatientIdentity());

        return dozerBeanMapper.map(response, MoePatientSummaryDto.class);
    }

    @Override
    public String addPatientAlert() throws Exception {
        Date date = new Date();
        PatientAlertRequestDto patientAlertRequestDto = new PatientAlertRequestDto();
        String HLABNegative = SystemSettingUtil.getParamValue(ServerConstant.SYSTEM_SETTING_PARAM_NAME_HLAB1502_NEG);
        String HLABNegativeCode = SystemSettingUtil.getParamValue(ServerConstant.SYSTEM_SETTING_PARAM_NAME_HLAB1502_NEG_CODE);

        patientAlertRequestDto.setAlertCode(HLABNegativeCode);
        patientAlertRequestDto.setAlertDesc(HLABNegative);
        patientAlertRequestDto.setHospitalCode(getUserDto().getHospitalCd());
        patientAlertRequestDto.setSourceSystem(SystemSettingUtil.getParamValue(ServerConstant.PARAM_NAME_SAAM_SOURCE_SYSTEM));
        patientAlertRequestDto.setCreateDtm(date);
        patientAlertRequestDto.setUpdateBy(getUserDto().getLoginName());
        patientAlertRequestDto.setUpdateDtm(date);
        patientAlertRequestDto.setUpdateUser(getUserDto().getLoginName());
        patientAlertRequestDto.setUpdateUserId(getUserDto().getLoginId());
        patientAlertRequestDto.setUpdateHosp(getUserDto().getHospitalCd());
        patientAlertRequestDto.setUpdateRank(getUserDto().getUserRankCd());
        patientAlertRequestDto.setUpdateRankDesc(getUserDto().getUserRankDesc());

        AddPatientAlertResponse response = saamRestService.addPatientAlert(getUserDto().getMrnPatientIdentity(), patientAlertRequestDto);
        return response.getResponseDto().getResponseDesc();
    }

    @Override
    public void cancelOrder() throws Exception {
        UserDto user = getUserDto();

        String redisKey = String.format(ServerConstant.MOE_CACHE_ORDER_PREFIX,
                user.getMrnPatientIdentity(), user.getMrnPatientEncounterNum());
        moeContentService.deleteOrder(redisKey);
        moeContentService.tryReleasePrescription(0);
    }

    private String addAcceptDrugCheckingRecord(MedicationDecisionRequestDto dto) throws Exception {
        MoeEhrAcceptDrugCheckingPo checking = new MoeEhrAcceptDrugCheckingPo();
        String acceptId = StringUtil.uuid();
        // set id start --
        checking.setAcceptId(acceptId);
        checking.setButtonLabel("Drug Allergy Checking");
        checking.setRecordDtm(new Date());
        // set id end --
        checking.setDrugVtm(WsFormatter.toPrescriptionContent(dto.getPatientMedications()));
        checking.setAllergen(WsFormatter.totAllergyContent(dto.getPatientAllergies()));
        checking.setScreenMsg(WsFormatter.toAuditLogContent(dto.getPatientAllergies(), dto.getPatientMedications()));
        moeEhrAcceptDrugCheckingRepository.save(checking);
        return acceptId;
    }
    // Ricci 20190916 end --

    // Ricci 20190926 start --

    /**
     * @return DAC TOKEN
     * @apiNote try to check DAC Service and try to get the token
     */
/*    private String loginDac() throws Exception {

        AuthenticationDto requestDto = new AuthenticationDto();

        requestDto.setAppName(ServerConstant.SYSTEM_LOGIN_FOR_MOE_SVC);
        requestDto.setHospitalCode(getUserDto().getHospitalCd());
        requestDto.setVersion(ServerConstant.APP_VERSION);

        DacAppDto appDto = new DacAppDto();
        appDto.setAppName(ServerConstant.SYSTEM_LOGIN_FOR_MOE_SVC);
        appDto.setPassword(ServerConstant.DAC_PAWD);
        requestDto.setDacApp(appDto);

        requestDto = dozerBeanMapper.map(dacRestService.login(requestDto).getData(), AuthenticationDto.class);

        return requestDto.getToken();
    }
    // Ricci 20190926 end --*/

}
