package hk.health.moe.service.impl;

import hk.health.moe.common.MoeCommonHelper;
import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.exception.ParamException;
import hk.health.moe.exception.RecordException;
import hk.health.moe.facade.ReportDataFacade;
import hk.health.moe.pojo.bo.CoreObjectBo;
import hk.health.moe.pojo.bo.ReportDataValueBo;
import hk.health.moe.pojo.bo.ReportDetailBo;
import hk.health.moe.pojo.bo.ReportListItemBo;
import hk.health.moe.pojo.dto.MoeEhrRxImageLogDto;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.pojo.dto.inner.InnerPrintPrescriptionDto;
import hk.health.moe.pojo.dto.inner.InnerPrintPrescriptionLogDto;
import hk.health.moe.pojo.po.MoeEhrRxImageLogPo;
import hk.health.moe.pojo.po.MoeOrderPo;
import hk.health.moe.repository.MoeEhrRxImageLogRepository;
import hk.health.moe.repository.MoeOrderRepository;
import hk.health.moe.restservice.bean.saam.MoePatientSummaryDto;
import hk.health.moe.service.MoeEhrOrderService;
import hk.health.moe.service.ReportService;
import hk.health.moe.service.RestTemplateService;
import hk.health.moe.util.*;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.ExporterInputItem;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleExporterInputItem;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service("reportService")
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class ReportServiceImpl extends BaseServiceImpl implements ReportService {

    @Autowired
    private ReportDataFacade reportDataFacade;

    @Autowired
    private MoeOrderRepository moeOrderRepository;

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    @Autowired
    private RestTemplateService restTemplateService;

    @Autowired
    private MoeEhrRxImageLogRepository moeEhrRxImageLogRepository;

    @Autowired
    private MoeEhrOrderService moeEhrOrderService;

    @Override
    public ReportDetailBo reportForPrescription(InnerPrintPrescriptionDto dto) throws Exception {

        String saveRxImage = SystemSettingUtil.getParamValue("save_rx_image");
        boolean isSaveRxImage = saveRxImage != null && saveRxImage.equalsIgnoreCase("Y");

        boolean failToCallSaam = false;
        boolean isReprint = dto.getIsReprint().equalsIgnoreCase("Y");

        Long ordNo = dto.getOrdNo();
        Date curDateTime = new Date();
        MoePatientSummaryDto patientSummaryDto = null;
        UserDto userDto = getTokenInfo().getUserDto();
        String enableRxPreview = SystemSettingUtil.getParamValue(ServerConstant.PARAM_NAME_ENABLE_RX_PREVIEW);
        String directPrintFlag = "";
        if (enableRxPreview != null && enableRxPreview.equalsIgnoreCase("N")) {
            directPrintFlag = "Y";
        } else {
            directPrintFlag = "N";
        }

        CoreObjectBo coreObject = new CoreObjectBo();
        coreObject.setHkic(userDto.getHkid());
        coreObject.setOrderNo(ordNo);
        coreObject.setHospitalCode(userDto.getHospitalCd());
        coreObject.setDirectPrintFlag(directPrintFlag);
        coreObject.setReprint(isReprint);

        ReportDetailBo reportDetailBo = new ReportDetailBo();   // return object
        reportDetailBo.setDirectPrintFlag(directPrintFlag);

        Optional<MoeOrderPo> optionalMoeOrderPo = moeOrderRepository.findById(ordNo);
        if (!optionalMoeOrderPo.isPresent()) {
            //eric 20191225 start--
//            throw new RecordException(localeMessageSourceUtil.getMessage("records.notExisted"));
            throw new RecordException(ResponseCode.SpecialMessage.RECORDS_NOT_EXISTED.getResponseCode(),ResponseCode.SpecialMessage.RECORDS_NOT_EXISTED.getResponseMessage());
            //eric 20191225 end--
        }
        MoeOrderPo moeOrderPo = optionalMoeOrderPo.get();

        if (SystemSettingUtil.getParamValue("enable_saam") != null
                && SystemSettingUtil.getParamValue("enable_saam").equalsIgnoreCase("Y")) {
            try {
                patientSummaryDto = moeEhrOrderService.getPatientSummaryByPatientRefKey(/*userDto.getMrnPatientEncounterNum()*/);
            } catch (Exception e) {
                failToCallSaam = true;
            }
        }

        ReportDataValueBo reportDataValue = reportDataFacade.reportForPrescription(moeOrderPo, userDto, coreObject, curDateTime, patientSummaryDto, failToCallSaam);

        if ("N".equalsIgnoreCase(directPrintFlag)) {
            String refNo = moeOrderPo.getRefNo();
            if (StringUtils.isEmpty(refNo)) {
                refNo = Formatter.getRefNo(moeOrderPo.getOrdNo(), moeOrderPo.getVersion());
            }
            //writeAuditLog(auditDto, Integer.toString(orderNo), refNo, coreObject.isReprint());
        }
        breakLines(reportDataValue);

        // generate JasperPrint for Prescription start --
        JasperPrint dischargePrescriptionJasperPrint = generatePrescriptionReport(reportDataValue, ServerConstant.REPORT_NAME_FOR_DISCHARGE_PRESCRIPTION);
        JasperPrint purchasePrescriptionJasperPrint = null;
        if (reportDataValue.getCommunityFlag().equalsIgnoreCase("Y")) {
            purchasePrescriptionJasperPrint = generatePrescriptionReport(reportDataValue, ServerConstant.REPORT_NAME_FOR_PURCHASE_PRESCRIPTION);
        }
        // generate JasperPrint for Prescription end --

        // save DischargePrescription into DataBase
        if (dischargePrescriptionJasperPrint != null) {
            String dischargeXml = null;
            byte[] dischargePrescriptionBytes = jasperPrintToByteArray(dischargePrescriptionJasperPrint);
            String dischargeReportId = MoeCommonHelper.stringToHex(dischargePrescriptionBytes);

            if ("N".equalsIgnoreCase(directPrintFlag)) {
                reportDetailBo.setDischargeBase64(Base64.getEncoder().encodeToString(dischargePrescriptionBytes));
            }

            MoeEhrRxImageLogPo dischargeEhrRxImageLogPo = new MoeEhrRxImageLogPo();
            dischargeEhrRxImageLogPo.setHospcode(userDto.getHospitalCd());
            dischargeEhrRxImageLogPo.setOrdNo(ordNo);
            // add ref no to Image Log table
            dischargeEhrRxImageLogPo.setRefNo(reportDataValue.getRefNo());
            if (purchasePrescriptionJasperPrint != null) {
                dischargeEhrRxImageLogPo.setReportType("S");  // TODO maybe use Constant will be better
            } else {
                dischargeEhrRxImageLogPo.setReportType("Y");  // TODO maybe use Constant will be better
            }
            dischargeEhrRxImageLogPo.setDirectPrint(directPrintFlag);
            dischargeEhrRxImageLogPo.setCreateDtm(curDateTime);
            dischargeEhrRxImageLogPo.setLastPrintDtm(curDateTime);
            dischargeEhrRxImageLogPo.setRxImage(dischargePrescriptionBytes);
            if (isSaveRxImage) {
                dischargeXml = jasperPrintToXml(dischargePrescriptionJasperPrint);
                dischargeEhrRxImageLogPo.setRxXml(dischargeXml);
            }
            if ("Y".equalsIgnoreCase(directPrintFlag)) {
                dischargeEhrRxImageLogPo = moeEhrRxImageLogRepository.saveAndFlush(dischargeEhrRxImageLogPo);
                // Ricci 20190814 start --
                moeOrderPo.setPrintType(ServerConstant.ORDER_PRINT_TYPE_RE_PRINT);
                moeOrderRepository.saveAndFlush(moeOrderPo);
                // Ricci 20190814 end --
                //reportDetailBo.setDischargeReportId(dischargeReportId);
                reportDetailBo.setDischargeReportImageId(dischargeEhrRxImageLogPo.getReportId());
            } else {
                reportDetailBo.setDischargeReportLog(dischargeEhrRxImageLogPo);
            }
        }

        // save PurchasePrescription into DataBase
        if (purchasePrescriptionJasperPrint != null) {
            String purchaseXml = null;
            byte[] purchasePrescriptionBytes = jasperPrintToByteArray(purchasePrescriptionJasperPrint);
            String purchaseReportId = MoeCommonHelper.stringToHex(purchasePrescriptionBytes);

            if ("N".equalsIgnoreCase(directPrintFlag)) {
                reportDetailBo.setPurchaseBase64(Base64.getEncoder().encodeToString(purchasePrescriptionBytes));
            }

            MoeEhrRxImageLogPo purchaseEhrRxImageLogPo = new MoeEhrRxImageLogPo();
            purchaseEhrRxImageLogPo.setHospcode(userDto.getHospitalCd());
            purchaseEhrRxImageLogPo.setOrdNo(ordNo);
            // add ref no to Image Log table
            purchaseEhrRxImageLogPo.setRefNo(reportDataValue.getRefNo());
            purchaseEhrRxImageLogPo.setReportType("P");  // TODO maybe use Constant will be better
            purchaseEhrRxImageLogPo.setDirectPrint(directPrintFlag);
            purchaseEhrRxImageLogPo.setCreateDtm(curDateTime);
            purchaseEhrRxImageLogPo.setLastPrintDtm(curDateTime);
            purchaseEhrRxImageLogPo.setRxImage(purchasePrescriptionBytes);
            if (isSaveRxImage) {
                purchaseXml = jasperPrintToXml(purchasePrescriptionJasperPrint);
                purchaseEhrRxImageLogPo.setRxXml(purchaseXml);
            }
            if ("Y".equalsIgnoreCase(directPrintFlag)) {
                purchaseEhrRxImageLogPo = moeEhrRxImageLogRepository.saveAndFlush(purchaseEhrRxImageLogPo);

                //reportDetailBo.setDischargeReportId(dischargeReportId);
                reportDetailBo.setPurchaseReportImageId(purchaseEhrRxImageLogPo.getReportId());
            } else {
                reportDetailBo.setPurchaseReportLog(purchaseEhrRxImageLogPo);
            }
        }

        List<JasperPrint> jasperPrints = new ArrayList<>();
        jasperPrints.add(dischargePrescriptionJasperPrint);
        jasperPrints.add(purchasePrescriptionJasperPrint);
        byte[] reportBytes = jasperPrintToByteArray(jasperPrints);
        reportDetailBo.setReportBase64(Base64.getEncoder().encodeToString(reportBytes));

        return reportDetailBo;
    }

    // Ricci 20190813 start --
    @Override
    @Transactional(rollbackFor = Exception.class)
    public InnerPrintPrescriptionLogDto logPrintPrescription(InnerPrintPrescriptionLogDto dto) throws Exception {

        MoeEhrRxImageLogPo dischargeReportLog = null;
        MoeEhrRxImageLogPo purchaseReportLog = null;
        if (dto.getDischargeReportLog() != null && StringUtils.isBlank(dto.getDischargeReportLog().getReportId())) {
            dischargeReportLog = DtoMapUtil.convertMoeEhrRxImageLog(dto.getDischargeReportLog());
            if (dto.getPurchaseReportLog() != null) {
                if (StringUtils.isBlank(dto.getPurchaseReportLog().getReportId())) {
                    purchaseReportLog = DtoMapUtil.convertMoeEhrRxImageLog(dto.getPurchaseReportLog());
                } else {
                    //eric 20191225 start--
//                    throw new ParamException(localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
                    throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
                    //eric 20191225 end--
                }
            }
            InnerPrintPrescriptionLogDto vo = new InnerPrintPrescriptionLogDto();

            dischargeReportLog = moeEhrRxImageLogRepository.save(dischargeReportLog);
            MoeEhrRxImageLogDto dischargeReportLogVo = new MoeEhrRxImageLogDto();
            dischargeReportLogVo.setReportId(dischargeReportLog.getReportId());
            vo.setDischargeReportLog(dischargeReportLogVo);

            if (purchaseReportLog != null) {
                purchaseReportLog = moeEhrRxImageLogRepository.save(purchaseReportLog);
                MoeEhrRxImageLogDto purchaseReportLogVo = new MoeEhrRxImageLogDto();
                purchaseReportLogVo.setReportId(purchaseReportLog.getReportId());
                vo.setPurchaseReportLog(purchaseReportLogVo);
            }
            // Ricci 20190814 start --
            Optional<MoeOrderPo> optionalMoeOrder = moeOrderRepository.findById(dto.getDischargeReportLog().getOrdNo());
            if (optionalMoeOrder.isPresent()) {
                MoeOrderPo moeOrder = optionalMoeOrder.get();
                moeOrder.setPrintType(ServerConstant.ORDER_PRINT_TYPE_RE_PRINT);
                moeOrderRepository.saveAndFlush(moeOrder);
            } else {
                //eric 20191225 start--
//                throw new RecordException(localeMessageSourceUtil.getMessage("records.notExisted"));
                throw new RecordException(ResponseCode.SpecialMessage.RECORDS_NOT_EXISTED.getResponseCode(),ResponseCode.SpecialMessage.RECORDS_NOT_EXISTED.getResponseMessage());
                //eric 20191225 end--
            }
            // Ricci 20190814 end --

            return vo;
        } else {
            //eric 20191225 start--
//            throw new ParamException(localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
            throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191225 end--
        }
    }

    /*@Override
    public byte[] generateReportByte(Map<String, Object> data, String sourceLocation) throws Exception {
        File insideFile;
        try {
            insideFile = ResourceUtils.getFile(sourceLocation);
        } catch (FileNotFoundException e) {
            throw new MoeServiceException(e.getMessage(), e);
        }
        try (InputStream inputStream = new FileInputStream(insideFile)) {
            byte[] bytes = JasperRunManager.runReportToPdf(inputStream, data, new JREmptyDataSource());
            return bytes;
        } catch (JRException e) {
            throw new MoeServiceException(e.getMessage(), e);
        } catch (IOException e) {
            throw new MoeServiceException(e.getMessage(), e);
        }
    }*/
    // Ricci 20190813 end --

    private String jasperPrintToXml(JasperPrint jasperPrint) throws JRException {
        return JasperExportManager.exportReportToXml(jasperPrint);
    }

    private void breakLines(ReportDataValueBo record) {

        if (record == null) {
            return;
        }

        if (record.getMoeHospitalItemList() != null
                && record.getMoeHospitalItemList().size() > 0) {
            for (ReportListItemBo item : record.getMoeHospitalItemList()) {
                breakLine(item, 165);
            }

        }

        if (record.getMoeCommunityItemList() != null
                && record.getMoeCommunityItemList().size() > 0) {
            for (ReportListItemBo item : record.getMoeCommunityItemList()) {
                breakLine(item, 120);
            }
        }

    }

    private void breakLine(ReportListItemBo item, float maxLineWidth) {

        if (item.getLevel() != 2
                || item.getDrugName() == null
                || item.getDrugNameDetail() == null) {
            return;
        }

        float lineWidth = item.getDrugName().length() * 1.5f + item.getDrugNameDetail().length();
        if (lineWidth > maxLineWidth) {

            List<Integer> insertDNIndex = new ArrayList<Integer>();
            List<Integer> insertDNDetailIndex = new ArrayList<Integer>();
            float curLineWidth = 0;
            int lastSymbolIndex = 0;
            int lastSpaceIndex = 0;
            if (item.getDrugName().contains(" + ")) {
                curLineWidth += 1.5f;
                for (int k = 1; k < item.getDrugName().length() - 1; k++) {

                    curLineWidth += 1.5f;

                    if (item.getDrugName().charAt(k - 1) == ' '
                            && item.getDrugName().charAt(k) == '+'
                            && item.getDrugName().charAt(k + 1) == ' ') {
                        lastSymbolIndex = k + 1;
                    } else if (item.getDrugName().charAt(k) == ' '
                            && item.getDrugName().charAt(k + 1) != '+') {
                        lastSpaceIndex = k;
                    } else {
                        //--- do nothing ---
                    }

                    if (curLineWidth > maxLineWidth) {
                        if (lastSymbolIndex != 0) {
                            insertDNIndex.add(lastSymbolIndex);
                            curLineWidth = (k - lastSymbolIndex) * 1.5f;
                        } else if (lastSpaceIndex != 0) {
                            insertDNIndex.add(lastSpaceIndex);
                            curLineWidth = (k - lastSpaceIndex) * 1.5f;
                        } else {
                            //--- do nothing ---
                        }

                        lastSymbolIndex = 0;
                        lastSpaceIndex = 0;
                    }
                }
            }

            if (item.getDrugNameDetail().contains(" + ")) {
                for (int k = 1; k < item.getDrugNameDetail().length() - 1; k++) {
                    curLineWidth += 1f;

                    if (item.getDrugNameDetail().charAt(k - 1) == ' '
                            && item.getDrugNameDetail().charAt(k) == '+'
                            && item.getDrugNameDetail().charAt(k + 1) == ' ') {
                        lastSymbolIndex = k + 1;
                    } else if (item.getDrugNameDetail().charAt(k) == ' '
                            && item.getDrugNameDetail().charAt(k + 1) != '+') {
                        lastSpaceIndex = k;
                    } else {
                        //--- do nothing ---
                    }

                    if (curLineWidth > maxLineWidth) {
                        if (lastSymbolIndex != 0) {
                            insertDNDetailIndex.add(lastSymbolIndex);
                            curLineWidth = (float) k - lastSymbolIndex;
                        } else if (lastSpaceIndex != 0) {
                            insertDNDetailIndex.add(lastSpaceIndex);
                            curLineWidth = (float) k - lastSpaceIndex;
                        } else {
                            //--- do nothing ---
                        }

                        lastSymbolIndex = 0;
                        lastSpaceIndex = 0;
                    }
                }
            }

            for (int k = insertDNIndex.size() - 1; k >= 0; k--) {
                int index = insertDNIndex.get(k);
                item.setDrugName(item.getDrugName().substring(0, index + 1) +
                        "\n" + item.getDrugName().substring(index + 1));
            }

            for (int k = insertDNDetailIndex.size() - 1; k >= 0; k--) {
                int index = insertDNDetailIndex.get(k);
                item.setDrugNameDetail(item.getDrugNameDetail().substring(0, index + 1) +
                        "\n" + item.getDrugNameDetail().substring(index + 1));
            }

        }
    }

    private JasperPrint generatePrescriptionReport(ReportDataValueBo convertReportData, String reportName) throws Exception {

        Map<String, Object> parameters = new HashMap<String, Object>();
        ReportDataValueBo reportDataValue = convertReportData;
        StringBuffer reportPath = new StringBuffer();
        reportPath.append(ServerConstant.CLASS_PATH);
        reportPath.append(ServerConstant.REPORT_PATH);
        reportPath.append(ServerConstant.REPORT_PATH_FOR_PRESCRIPTION);
        reportPath.append(reportName);

        // Add vertcal barcode code128 image for caseNo to report
        parameters.put("verticalBarcodeImage", reportDataValue.getVerticalBarcode128Icon());
        parameters.put("print_with_hkic", SystemSettingUtil.getParamValue("print_with_hkic"));
        parameters.put("print_with_hkic_barcode", SystemSettingUtil.getParamValue("print_with_hkic_barcode"));
        // pass horizontal barcode code128 image for prescription no to report
        parameters.put("ordNoBarcodeImage", reportDataValue.getOrdNoBarcode128Icon());
        // pass horizontal barcode code128 image for prescription no to report
        parameters.put("horizontalBarcodeImage", reportDataValue.getHorizontal128Icon());
        // pass horizontal barcode code128 image for hkic to report
        parameters.put("hkicBarcodeImage", reportDataValue.getHkicBarcode128Icon());
        parameters.put("image", reportDataValue.getHospitalIcon());
        parameters.put("prescType", reportDataValue.getPrescType());
        parameters.put("moeHospitalItemList", reportDataValue.getMoeHospitalItemList());
        parameters.put("orderDate", reportDataValue.getOrderDate());
        parameters.put("patientEnglishName", reportDataValue.getPatientEnglishName());
        /*String patientChineseName = "";
        boolean useJasperReport = true;
        if (!StringUtils.isEmpty(reportDataValue.getPatientChineseName())) {
            if (useJasperReport) {
                patientChineseName = "(" + StringUtil.toHtmlTag(reportDataValue.getPatientChineseName()) + ")";
            } else {
                patientChineseName = "(" + reportDataValue.getPatientChineseName() + ")";
            }
        }*/
        parameters.put("patientChineseName", "(" + reportDataValue.getPatientChineseName() + ")");
        parameters.put("patientAddress", reportDataValue.getPatientAddress());
        parameters.put("dateOfBirth", reportDataValue.getDateOfBirth());
        parameters.put("sex", reportDataValue.getSex());
        parameters.put("docTypeLabel", reportDataValue.getDocTypeLabel());
        parameters.put("hkic", reportDataValue.getHkic());
        parameters.put("ward", reportDataValue.getWard());
        parameters.put("age", reportDataValue.getAge());
        parameters.put("weight", reportDataValue.getWeight());
        parameters.put("patientStatus", reportDataValue.getPatientStatus());
        parameters.put("drugAllergyItem", reportDataValue.getDrugAllergyItem());
        parameters.put("adverseDrugReactionItem", reportDataValue.getAdverseDrugReactionItem());
        parameters.put("alertItem", reportDataValue.getAlertItem());
        parameters.put("failToCallSaam", reportDataValue.isFailToCallSaam());
        parameters.put("doctorName", reportDataValue.getDoctorName());
        parameters.put("remarkBy", reportDataValue.getRemarkBy());
        parameters.put("firstPrintDate", reportDataValue.getFirstPrintDate());
        parameters.put("lastPrintDate", reportDataValue.getLastPrintDate());
        parameters.put("refNo", reportDataValue.getRefNo());
        parameters.put("SUBREPORT_DIR", "Discharge_Prescription/");
        parameters.put("hospChineseName", reportDataValue.getHospitalChineseName());
        parameters.put("hospEnglishName", reportDataValue.getHospitalEnglishName());
        parameters.put("hospChineseAddress", reportDataValue.getHospitalChineseAddress());
        parameters.put("hospEnglishAddress", reportDataValue.getHospitalEnglishAddress());
        parameters.put("hospPhoneNumber", reportDataValue.getHospitalPhoneNumber());
        parameters.put("hospFaxNumber", reportDataValue.getHospitalFaxNumber());
        parameters.put("hospWebAddress", reportDataValue.getHospitalWebAddress());
        parameters.put("hospEmailAddress", reportDataValue.getHospitalEmailAddress());
        parameters.put("formID", SystemSettingUtil.getParamValue("prescription_form_id"));
        parameters.put("verticalBarcodeLabel", reportDataValue.getVerticalBarcodeLabel());
        parameters.put("horizontalBarcodeLabel", reportDataValue.getHorizontalBarcodeLabel());
        parameters.put("ordNoLabel", reportDataValue.getOrdNoLabel());
        parameters.put("purchasePrescriptionEncounterNo", reportDataValue.getPurchasePrescriptionEncounterNo());
        parameters.put("purchasePrescriptionOrderNo", reportDataValue.getPurchasePrescriptionOrderNo());
        parameters.put("templatePath", reportDataValue.getTemplatePath());
        parameters.put("phone", reportDataValue.getPatientPhoneNo());
        parameters.put("drugIngredientMoreThanThreeMsg", reportDataValue.getDrugIngredientMoreThanThreeMsg());
        parameters.put("drugIngredientPurchaseMoreThanThreeMsg", reportDataValue.getDrugIngredientPurchaseMoreThanThreeMsg());
        parameters.put("addressContainsChineseChr", reportDataValue.getAddressContainsChineseChr());
        // add reprint flag
        parameters.put("reprintFlag", reportDataValue.getReprintFlag());

        //20190822 add new report parameter by Louis Chen
        String drugIngredientMoreThanThreeMsg = null;
        if (reportDataValue.getDrugIngredientMoreThanThreeMsg() != null) {
            drugIngredientMoreThanThreeMsg = reportDataValue.getDrugIngredientMoreThanThreeMsg();
        }
        if (SystemSettingUtil.isSettingEnabled("enable_free_text_indicator") && reportDataValue.isHasDischargeFreeTextDrug()) {
            if (drugIngredientMoreThanThreeMsg != null)
                drugIngredientMoreThanThreeMsg += " ^ Non-formulary";
            else
                drugIngredientMoreThanThreeMsg = "^ Non-formulary";
        }
        parameters.put("drugIngredientMoreThanThreeMsg", reportDataValue.getDrugIngredientMoreThanThreeMsg());
        parameters.put("prescriptionCutsomLine1", reportDataValue.getPrescriptionCutsomLine1());
        parameters.put("prescriptionCutsomLine2", reportDataValue.getPrescriptionCutsomLine2());
        //20190822 add new report parameter by Louis Chen

        JRMapCollectionDataSource moeItem;
        Collection<Map<String, ?>> moeItemList = new ArrayList<Map<String, ?>>();
        Map<String, Object> moeItemMap = new HashMap<String, Object>();

        if (reportDataValue.getMoeHospitalItemList() != null) {
            JRMapCollectionDataSource subItem;
            List<Map<String, ?>> subItemList = new ArrayList<Map<String, ?>>();
            Map<String, Object> subItemMap = new HashMap<String, Object>();

            JRMapCollectionDataSource drugItem;
            List<Map<String, ?>> drugItemList = new ArrayList<Map<String, ?>>();

            //--- indicators to create new Map for level 2 and level 3 ---
            boolean newLevel2 = true;
            boolean newLevel3 = true;
            for (int i = 0; i < reportDataValue.getMoeHospitalItemList().size(); i++) {

                int level = reportDataValue.getMoeHospitalItemList().get(i).getLevel();

                if (level == 1) {
                    newLevel2 = true;
                    newLevel3 = true;

                    subItem = null;
                    moeItemMap = new HashMap<String, Object>();
                    moeItemMap.put("hosp", reportDataValue.getMoeHospitalItemList().get(i).getLabelName());
                    moeItemMap.put("templatePath", reportDataValue.getTemplatePath());
                    subItemList = new ArrayList<Map<String, ?>>();
                } else if (level == 2) {
                    if (newLevel2) {
                        newLevel2 = false;
                        newLevel3 = true;

                        subItemMap = new HashMap<String, Object>();
                        subItemMap.put("templatePath", reportDataValue.getTemplatePath());
                        subItemMap.put("hosp", moeItemMap.get("hosp"));
                        subItemMap.put("drugname", reportDataValue.getMoeHospitalItemList().get(i).getDrugName());
                        subItemMap.put("drugnamedetail", reportDataValue.getMoeHospitalItemList().get(i).getDrugNameDetail());

                        drugItem = null;
                        drugItemList = new ArrayList<>();
                    }

                    if ((i + 1) == reportDataValue.getMoeHospitalItemList().size()
                            || reportDataValue.getMoeHospitalItemList().get(i + 1).getLevel() == 1) {

                        drugItem = new JRMapCollectionDataSource(drugItemList);
                        HashMap<String, Object> drugItemReport = new HashMap<String, Object>();
                        drugItemReport.put("drugItem", drugItem);

                        subItemMap.put("drugItemReport", drugItemReport);
                        subItemList.add(subItemMap);
                    }

                    if ((i + 1) == reportDataValue.getMoeHospitalItemList().size()
                            || reportDataValue.getMoeHospitalItemList().get(i + 1).getLevel() == 1) {

                        subItem = new JRMapCollectionDataSource(subItemList);
                        HashMap<String, Object> subItemReport = new HashMap<String, Object>();
                        subItemReport.put("title1", "SIMPLE_DATA TITLE 1");
                        subItemReport.put("subItem", subItem);

                        moeItemMap.put("subItemReport", subItemReport);
                        moeItemList.add(moeItemMap);
                    }

                } else if (level == 3) {
                    if (newLevel3) {
                        newLevel2 = true;
                        newLevel3 = false;

                    }

                    Map<String, Object> drugItemMap = null;
                    drugItemMap = new HashMap<String, Object>();
                    drugItemMap.put("templatePath", reportDataValue.getTemplatePath());
                    drugItemMap.put("drugvalue", reportDataValue.getMoeHospitalItemList().get(i).getDrugValue());
                    drugItemMap.put("drugvaluedetail", reportDataValue.getMoeHospitalItemList().get(i).getDrugValueDetail());
                    drugItemList.add(drugItemMap);

                    if ((i + 1) == reportDataValue.getMoeHospitalItemList().size()
                            || reportDataValue.getMoeHospitalItemList().get(i + 1).getLevel() != 3) {

                        drugItem = new JRMapCollectionDataSource(drugItemList);
                        HashMap<String, Object> drugItemReport = new HashMap<String, Object>();
                        drugItemReport.put("drugItem", drugItem);

                        subItemMap.put("drugItemReport", drugItemReport);
                        subItemList.add(subItemMap);
                    }

                    if ((i + 1) == reportDataValue.getMoeHospitalItemList().size()
                            || reportDataValue.getMoeHospitalItemList().get(i + 1).getLevel() == 1) {

                        subItem = new JRMapCollectionDataSource(subItemList);
                        HashMap<String, Object> subItemReport = new HashMap<String, Object>();
                        subItemReport.put("title1", "SIMPLE_DATA TITLE 1");
                        subItemReport.put("subItem", subItem);

                        moeItemMap.put("subItemReport", subItemReport);
                        moeItemList.add(moeItemMap);
                    }
                }
            }
        }

        moeItem = new JRMapCollectionDataSource(moeItemList);
        HashMap<String, Object> reportData = new HashMap<String, Object>();
        reportData.put("title", "SIMPLE_DATA TITLE");
        reportData.put("moeItem", moeItem);

        JRMapCollectionDataSource subHospItem;
        List<Map<String, ?>> subHospItemList = new ArrayList<Map<String, ?>>();
        if (reportDataValue.getMoeHospitalItemList() != null) {

            Map<String, Object> subItemMap = new HashMap<String, Object>();
            JRMapCollectionDataSource drugItem;
            List<Map<String, ?>> drugItemList = new ArrayList<Map<String, ?>>();

            boolean newLevel2 = true;
            boolean newLevel3 = true;

            for (int i = 0; i < reportDataValue.getMoeHospitalItemList().size(); i++) {

                int level = reportDataValue.getMoeHospitalItemList().get(i).getLevel();

                if (level == 1) {
                    newLevel2 = true;
                    newLevel3 = true;

                } else if (level == 2) {

                    if (newLevel2) {
                        newLevel2 = false;
                        newLevel3 = true;

                        subItemMap = new HashMap<String, Object>();
                        subItemMap.put("templatePath", reportDataValue.getTemplatePath());
                        subItemMap.put("hosp", reportDataValue.getMoeHospitalItemList().get(i).getLabelName());
                        subItemMap.put("drugname", reportDataValue.getMoeHospitalItemList().get(i).getDrugName());
                        subItemMap.put("drugnamedetail", reportDataValue.getMoeHospitalItemList().get(i).getDrugNameDetail());
                        drugItem = null;
                        drugItemList = new ArrayList<Map<String, ?>>();
                    }

                } else if (level == 3) {

                    if (newLevel3) {
                        newLevel2 = true;
                        newLevel3 = false;
                    }

                    Map<String, Object> drugItemMap = null;
                    drugItemMap = new HashMap<String, Object>();
                    drugItemMap.put("templatePath", reportDataValue.getTemplatePath());
                    drugItemMap.put("drugvalue", reportDataValue.getMoeHospitalItemList().get(i).getDrugValue());
                    drugItemMap.put("drugvaluedetail", reportDataValue.getMoeHospitalItemList().get(i).getDrugValueDetail());
                    drugItemList.add(drugItemMap);

                    if ((i + 1) == reportDataValue.getMoeHospitalItemList().size()
                            || reportDataValue.getMoeHospitalItemList().get(i + 1).getLevel() != 3) {

                        drugItem = new JRMapCollectionDataSource(drugItemList);
                        HashMap<String, Object> drugItemReport = new HashMap<String, Object>();
                        drugItemReport.put("drugItem", drugItem);

                        subItemMap.put("drugItemReport", drugItemReport);
                        subHospItemList.add(subItemMap);
                    }
                }
            }
        }

        subHospItem = new JRMapCollectionDataSource(subHospItemList);
        HashMap<String, Object> subHospItemReport = new HashMap<String, Object>();
        subHospItemReport.put("title1", "SIMPLE_DATA TITLE 1");
        subHospItemReport.put("subHospItem", subHospItem);

        parameters.put("moeHospitalData", reportData);


        //--- Community Item ---
        JRMapCollectionDataSource subCommItem;
        List<Map<String, ?>> subItemList = new ArrayList<Map<String, ?>>();
        if (reportDataValue.getMoeCommunityItemList().size() > 0) {

            Map<String, Object> subItemMap = new HashMap<String, Object>();
            JRMapCollectionDataSource drugItem;
            List<Map<String, ?>> drugItemList = new ArrayList<Map<String, ?>>();


            boolean newLevel2 = true;
            boolean newLevel3 = true;

            for (int j = 0; j < reportDataValue.getMoeCommunityItemList().size(); j++) {

                int level = reportDataValue.getMoeCommunityItemList().get(j).getLevel();

                if (level == 2) {

                    if (newLevel2) {
                        newLevel2 = false;
                        newLevel3 = true;

                        subItemMap = new HashMap<String, Object>();
                        drugItemList = new ArrayList<Map<String, ?>>();
                    }
                    subItemMap.put("templatePath", reportDataValue.getTemplatePath());
                    subItemMap.put("drugname", reportDataValue.getMoeCommunityItemList().get(j).getDrugName());
                    subItemMap.put("drugnamedetail", reportDataValue.getMoeCommunityItemList().get(j).getDrugNameDetail());

                } else if (level == 3) {

                    if (newLevel3) {
                        newLevel2 = true;
                        newLevel3 = false;


                    }

                    Map<String, Object> drugItemMap = null;
                    drugItemMap = new HashMap<String, Object>();
                    drugItemMap.put("templatePath", reportDataValue.getTemplatePath());
                    drugItemMap.put("drugvalue", reportDataValue.getMoeCommunityItemList().get(j).getDrugValue());
                    drugItemMap.put("drugvaluedetail", reportDataValue.getMoeCommunityItemList().get(j).getDrugValueDetail());
                    drugItemList.add(drugItemMap);

                    if ((j + 1) == reportDataValue.getMoeCommunityItemList().size()
                            || reportDataValue.getMoeCommunityItemList().get(j + 1).getLevel() != 3) {

                        drugItem = new JRMapCollectionDataSource(drugItemList);
                        HashMap<String, Object> drugItemReport = new HashMap<String, Object>();
                        drugItemReport.put("drugItem", drugItem);

                        subItemMap.put("drugItemReport", drugItemReport);
                        subItemList.add(subItemMap);

                    }
                }

            }
        }

        subCommItem = new JRMapCollectionDataSource(subItemList);
        HashMap<String, Object> subCommItemReport = new HashMap<String, Object>();
        subCommItemReport.put("title1", "SIMPLE_DATA TITLE 1");
        subCommItemReport.put("subCommItem", subCommItem);

        parameters.put("moeCommunityData", subCommItemReport);


        return generateReport(parameters, reportPath.toString());
    }

    private JasperPrint generateReport(Map<String, Object> data, String sourceLocation) throws Exception {

        File insideFile;
        JasperPrint jasperPrint = null;

        try {
            insideFile = ResourceUtils.getFile(sourceLocation);
        } catch (FileNotFoundException e) {
            //eric 20191225 start--
//            throw new MoeServiceException(e.getMessage(), e);
            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage()+e.getMessage());
            //eric 20191225 end--
        }
        try (InputStream inputStream = new FileInputStream(insideFile)) {
            jasperPrint = JasperFillManager.fillReport(inputStream, data, new JREmptyDataSource());
            return jasperPrint;
        } catch (JRException e) {
            //eric 20191225 start--
//            throw new MoeServiceException(e.getMessage(), e);
            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage()+e.getMessage());
            //eric 20191225 end--
        } catch (IOException e) {
            //eric 20191225 start--
//            throw new MoeServiceException(e.getMessage(), e);
            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage()+e.getMessage());
            //eric 20191225 end--
        }
    }

    private byte[] jasperPrintToByteArray(JasperPrint jasperPrints) throws Exception {

        JRPdfExporter exporter = new JRPdfExporter();
        ByteArrayOutputStream pdfops = new ByteArrayOutputStream();

        try {

            exporter.setExporterInput(new SimpleExporterInput(jasperPrints));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfops));

            exporter.exportReport();
            pdfops.close();

            return pdfops.toByteArray();
        } catch (JRException e) {
            //eric 20191225 start--
//            throw new MoeServiceException(e.getMessage(), e);
            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage()+e.getMessage());
            //eric 20191225 end--
        } catch (FileNotFoundException e) {
            //eric 20191225 start--
//            throw new MoeServiceException(e.getMessage(), e);
            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage()+e.getMessage());
            //eric 20191225 end--
        } catch (IOException e) {
            //eric 20191225 start--
//            throw new MoeServiceException(e.getMessage(), e);
            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage()+e.getMessage());
            //eric 20191225 end--
        } finally {
            pdfops.close();
        }
    }

    private byte[] jasperPrintToByteArray(List<JasperPrint> jasperPrints) throws Exception {

        ExporterInputItem exporterInputItem = null;
        JRPdfExporter exporter = new JRPdfExporter();
        ByteArrayOutputStream pdfops = new ByteArrayOutputStream();
        List<ExporterInputItem> exporterInputItems = new ArrayList<>();
        Iterator<JasperPrint> jasperPrintIterable = jasperPrints.iterator();

        try {

            while (jasperPrintIterable.hasNext()) {
                JasperPrint jasperPrint = jasperPrintIterable.next();
                if (jasperPrint != null) {
                    exporterInputItem = new SimpleExporterInputItem(jasperPrint);
                    exporterInputItems.add(exporterInputItem);
                }
            }
            exporter.setExporterInput(new SimpleExporterInput(exporterInputItems));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfops));

            exporter.exportReport();
            pdfops.close();

            return pdfops.toByteArray();
        } catch (JRException e) {
            //eric 20191225 start--
//            throw new MoeServiceException(e.getMessage(), e);
            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage()+e.getMessage());
            //eric 20191225 end--
        } catch (FileNotFoundException e) {
            //eric 20191225 start--
//            throw new MoeServiceException(e.getMessage(), e);
            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage()+e.getMessage());
            //eric 20191225 end--
        } catch (IOException e) {
            //eric 20191225 start--
//            throw new MoeServiceException(e.getMessage(), e);
            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage()+e.getMessage());
            //eric 20191225 end--
        } finally {
            pdfops.close();
        }
    }

}
