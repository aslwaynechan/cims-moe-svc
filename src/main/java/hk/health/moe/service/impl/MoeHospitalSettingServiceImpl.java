package hk.health.moe.service.impl;

import hk.health.moe.facade.MoeHospitalSettingFacade;
import hk.health.moe.pojo.dto.MoeHospitalSettingDto;
import hk.health.moe.service.MoeHospitalSettingService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MoeHospitalSettingServiceImpl extends BaseServiceImpl implements MoeHospitalSettingService {

    @Autowired
    private MoeHospitalSettingFacade moeHospitalSettingFacade;

    @Transactional(readOnly = true)
    @Override
    public List<MoeHospitalSettingDto> getAllSpecialtyAvailability() throws Exception {

        return moeHospitalSettingFacade.getAllSpecialtyAvailability();
    }

    @Transactional(readOnly = true)
    @Override
    public List<MoeHospitalSettingDto> getAllLocationByUserGroupMap() throws Exception {
        String hospCode = getUserDto().getHospitalCd();
        if (StringUtils.isBlank(hospCode)) {
            return moeHospitalSettingFacade.getAllLocationByUserGroupMap();
        } else {
            return moeHospitalSettingFacade.getAllLocationByUserGroupMap(hospCode);
        }
    }
}
