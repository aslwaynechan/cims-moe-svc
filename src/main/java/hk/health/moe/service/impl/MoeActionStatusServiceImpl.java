package hk.health.moe.service.impl;

import hk.health.moe.pojo.po.MoeActionStatusPo;
import hk.health.moe.repository.MoeActionStatusRepository;
import hk.health.moe.service.MoeActionStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MoeActionStatusServiceImpl implements MoeActionStatusService {

    @Autowired
    private MoeActionStatusRepository moeActionStatusRepository;

    @Override
    public MoeActionStatusPo getActionStatus(String statusType) throws Exception {

        return moeActionStatusRepository.findByActionStatusType(statusType);
    }

}
