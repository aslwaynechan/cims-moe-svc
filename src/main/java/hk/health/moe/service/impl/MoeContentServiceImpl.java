package hk.health.moe.service.impl;

import hk.health.moe.common.ResponseCode;
import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import hk.health.moe.common.DrugType;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.ConcurrentException;
import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.facade.MoeContentFacade;
import hk.health.moe.pojo.dto.MoeCommonDosageDto;
import hk.health.moe.pojo.dto.MoeDrugAliasNameDto;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.MoeDrugStrengthDto;
import hk.health.moe.pojo.dto.MoeEhrOrderDto;
import hk.health.moe.pojo.dto.MoeMyFavouriteHdrDto;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.pojo.po.MoeMyFavouriteHdrPo;
import hk.health.moe.repository.MoeMyFavouriteHdrRepository;
import hk.health.moe.service.MoeContentService;
import hk.health.moe.util.DrugSearchEngine;
import hk.health.moe.util.DtoMapUtil;
import hk.health.moe.util.DtoUtil;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.SystemSettingUtil;
import hk.health.moe.util.TempDtoUtil;
import io.lettuce.core.RedisCommandExecutionException;
import net.sf.json.JSONArray;
import net.sf.json.JSONNull;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.processors.DefaultDefaultValueProcessor;
import net.sf.json.util.JSONUtils;
import org.apache.commons.lang3.StringUtils;
import org.jboss.jandex.Index;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.RedisSystemException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;


@Service("moeContentService")
public class MoeContentServiceImpl extends BaseServiceImpl implements MoeContentService {

    @Autowired
    private MoeContentFacade moeContentFacade;
    @Autowired
    private RedisTemplate orderRedisTemplate;
    @Autowired
    private RedisTemplate favRedisTemplate;
    @Autowired
    private RedisTemplate tokenRedisTemplate;

    //Chris For Drug Search from Redis  -Start
    //Redis DB: 15
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    //Redis DB: 15
    @Autowired
    private RedisTemplate drugSearchTemplate;

    //Redis DB: 14  (In use : About insert and query drug items)
    @Autowired
    private RedisTemplate allDrugsRedisTemplate;

    //Redis DB: 14  (In use : About match drug key from Redis by keyword)
    @Autowired
    private StringRedisTemplate allDrugsStringRedisTemplate;

    //Redis DB: 13
    @Autowired
    private RedisTemplate drugMapRedisTemplate;
    //Chris For Drug Search from Redis  -End

    @Autowired
    private MoeMyFavouriteHdrRepository moeMyFavouriteHdrRepository;
    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;
    @Value("${redis.order.cache.expire}")
    private Long orderCacheExpire;
    @Value("${redis.order.lock.expire}")
    private Long orderLockExpire;
    @Value("${redis.order.lock.tryCount}")
    private Integer orderLockTryCount;
    @Value("${redis.fav.cache.expire}")
    private Long favCacheExpire;
    @Value("${redis.token.cache.expire}")
    private Long tokenCacheExpire;

    //Chris 20191125  -Start
//    @Autowired
//    private StringRedisTemplate stringRedisTemplate;
//    private RedisTemplate drugSearchTemplate;
    public static MoeContentServiceImpl moeContentServiceImpl;
    //TODO
    public void setUserDto(MoeContentServiceImpl moeContentServiceImpl) {
        this.moeContentServiceImpl = moeContentServiceImpl;
    }
    //
    @PostConstruct
    private void init() {
        moeContentServiceImpl = this;
        moeContentServiceImpl.stringRedisTemplate = this.stringRedisTemplate;
        moeContentServiceImpl.drugSearchTemplate = this.drugSearchTemplate;

        moeContentServiceImpl.allDrugsRedisTemplate = this.allDrugsRedisTemplate;
        moeContentServiceImpl.allDrugsStringRedisTemplate = this.allDrugsStringRedisTemplate;

        moeContentServiceImpl.drugMapRedisTemplate = this.drugMapRedisTemplate;
    }
    //Chris 20191125  -End

    @Override
    @PostConstruct
    public void setContent() throws Exception {

        moeContentFacade.setContent();

        //Chris 20191217  For Initialize Drug Items in Redis  -Start
        cacheAllDrugsInRedis();
        //Chris 20191217  For Initialize Drug Items in Redis  -End

    }

    @Override
    public void refreshDrugList() throws Exception {

        TempDtoUtil.clearAll();
        moeContentFacade.setTempContent();
        TempDtoUtil.copyContentFromTemp();

        //Chris 20200106  Control whether or not to refresh Redis drug list  -Start
        List<String> flagList = new ArrayList<>();
        flagList.add(SystemSettingUtil.getParamValue(ServerConstant.PARAM_NAME_ENABLE_MOE_REDIS).toUpperCase());        //enable_moe_redis
        flagList.add(SystemSettingUtil.getParamValue(ServerConstant.PARAM_NAME_ENABLE_DEPT_FAV_REDIS).toUpperCase());   //enable_dept_fav_redis

        if (flagList.contains("Y")) {
            //Chris 20191217  For update Drug Items in Redis  -Start
            cacheAllDrugsInRedis();
            //Chris 20191217  For update Drug Items in Redis  -End
        }
        //Chris 20200106  Control whether or not to refresh Redis drug list  -End

    }

    @Override
    public void refreshSystemSetting() throws Exception {

        moeContentFacade.refreshSystemSetting();
    }

    @Override
    public void cacheMoeOrder(MoeEhrOrderDto cacheDto) throws Exception {
        UserDto user = getUserDto();
        Duration timeout = Duration.ofMillis(orderCacheExpire);

        if (tryLockPrescription(user, 0)) {
            String redisKey = String.format(ServerConstant.MOE_CACHE_ORDER_PREFIX,
                    user.getMrnPatientIdentity(), user.getMrnPatientEncounterNum());
            cacheDto.setCacheBy(user.getLoginName());
            cacheDto.setCacheDtm(new Date());
            orderRedisTemplate.opsForValue().set(redisKey, cacheDto, timeout);
        } else {
            //eric 20191219 start--
//            throw new ConcurrentException(localeMessageSourceUtil.getMessage("records.concurrentError"));
            throw new ConcurrentException(ResponseCode.SpecialMessage.RECORDS_CONCURRENT_ERRORS.getResponseCode(),
                    ResponseCode.SpecialMessage.RECORDS_CONCURRENT_ERRORS.getResponseMessage());
            //eric 20191219 end--
        }
    }

    @Override
    public MoeEhrOrderDto getMoeOrder() throws Exception {

        MoeEhrOrderDto cacheDto = null;
        UserDto user = getUserDto();
        String redisKey = String.format(ServerConstant.MOE_CACHE_ORDER_PREFIX,
                user.getMrnPatientIdentity(), user.getMrnPatientEncounterNum());
        Object cacheObj = orderRedisTemplate.opsForValue().get(redisKey);
        if (cacheObj != null && cacheObj instanceof MoeEhrOrderDto) {
            cacheDto = (MoeEhrOrderDto) cacheObj;
            cacheDto.setIsCache(ServerConstant.DB_FLAG_TRUE);
        }

        return cacheDto;
    }

    @Override
    public boolean deleteOrder(String key) throws Exception {

        return orderRedisTemplate.delete(key);
    }

    @Override
    public boolean deleteFav(String key) throws Exception {

        return favRedisTemplate.delete(key);
    }

    @Override
    public String cacheDepartFavourite(MoeMyFavouriteHdrDto cacheDto) throws Exception {

        String cacheId;
        String redisKey;
        MoeMyFavouriteHdrPo cachePo;
        UserDto user = getUserDto();
        Duration timeout = Duration.ofMillis(favCacheExpire);

        if (StringUtils.isNotBlank(cacheDto.getMyFavouriteId())) {
            if (!moeMyFavouriteHdrRepository.existsById(cacheDto.getMyFavouriteId())) {
                //eric 20191219 start--
//                    throw new ConcurrentException();
                throw new ConcurrentException(ResponseCode.SpecialMessage.RECORDS_CONCURRENT_ERRORS.getResponseCode(),
                        ResponseCode.SpecialMessage.RECORDS_CONCURRENT_ERRORS.getResponseMessage());
                //eric 20191219 end--
            }
            if (StringUtils.isNotBlank(cacheDto.getCacheId())) {
                cacheId = UUID.randomUUID().toString();
                redisKey = checkExistAndPresent(cacheDto, user, ServerConstant.MOE_CACHE_DEPT_FAV_MAINTENANCE_PREFIX_WITH_FAV_ID, cacheId);
                cachePo = DtoMapUtil.convertMoeMyFavouriteHdrForCache(cacheDto, false);
            } else {
                String existedKey = String.format(ServerConstant.MOE_CACHE_DEPT_FAV_MAINTENANCE_PREFIX_WITH_FAV_ID,
                        user.getSpec(), "*", cacheDto.getMyFavouriteId());
                if (favRedisTemplate.keys(existedKey).size() == 0) {
                    cacheId = UUID.randomUUID().toString();
                    String newKey = String.format(ServerConstant.MOE_CACHE_DEPT_FAV_MAINTENANCE_PREFIX_WITH_FAV_ID,
                            user.getSpec(), cacheId, cacheDto.getMyFavouriteId());
                    redisKey = newKey;
                    cacheDto.setIsCache(ServerConstant.DB_FLAG_TRUE);
                    cacheDto.setCacheId(cacheId);
                    cachePo = DtoMapUtil.convertMoeMyFavouriteHdrForCache(cacheDto, false);
                } else {
                    //eric 20191219 start--
//                    throw new ConcurrentException();
                    throw new ConcurrentException(ResponseCode.SpecialMessage.RECORDS_CONCURRENT_ERRORS.getResponseCode(),
                            ResponseCode.SpecialMessage.RECORDS_CONCURRENT_ERRORS.getResponseMessage());
                    //eric 20191219 end--
                }
            }
        } else {
            if (StringUtils.isNotBlank(cacheDto.getCacheId())) {
                cacheId = UUID.randomUUID().toString();
                redisKey = checkExistAndPresent(cacheDto, user, ServerConstant.MOE_CACHE_DEPT_FAV_MAINTENANCE_PREFIX, cacheId);
                cachePo = DtoMapUtil.convertMoeMyFavouriteHdrForCache(cacheDto, false);
            } else {
                cacheId = UUID.randomUUID().toString();
                redisKey = String.format(ServerConstant.MOE_CACHE_DEPT_FAV_MAINTENANCE_PREFIX,
                        user.getSpec(), cacheId);
                cacheDto.setIsCache(ServerConstant.DB_FLAG_TRUE);
                cacheDto.setCacheId(cacheId);
                cachePo = DtoMapUtil.convertMoeMyFavouriteHdrForCache(cacheDto, false);
            }
        }
        favRedisTemplate.opsForValue().set(redisKey, cachePo, timeout);

        return cacheId;
    }

    @Override
    public ArrayList<MoeMyFavouriteHdrPo> listMyFavourite() throws Exception {

        UserDto user = getUserDto();
        ArrayList<MoeMyFavouriteHdrPo> cacheDtoList = null;
        String redisKey = String.format(ServerConstant.MOE_CACHE_DEPT_FAV_MAINTENANCE_PREFIX, user.getSpec(), "*");
        Set ketSet = favRedisTemplate.keys(redisKey);
        if (ketSet != null && ketSet.size() != 0) {
            cacheDtoList = (ArrayList<MoeMyFavouriteHdrPo>) favRedisTemplate.opsForValue().multiGet(ketSet);
        }

        return cacheDtoList;
    }

    @Override
    public boolean tryReleasePrescription(int tryCount) throws Exception {
        String redisLockKey = String.format(ServerConstant.MOE_CACHE_LOCK_ORDER_PREFIX,
                getUserDto().getMrnPatientIdentity(), getUserDto().getMrnPatientEncounterNum());
        Object object = orderRedisTemplate.opsForValue().get(redisLockKey);
        if (object != null && object.toString().equalsIgnoreCase(getUserDto().getLoginId())) {
            if (deleteOrder(redisLockKey)) {
                return true;
            } else {
                if (tryCount < orderLockTryCount) {
                    return tryReleasePrescription(++tryCount);
                } else {
                    return false;
                }
            }
        } else {
            return true;
        }
    }

    @Override
    public boolean checkExistPrescriptionLock(UserDto user) throws Exception {
        String redisLockKey = String.format(ServerConstant.MOE_CACHE_LOCK_ORDER_PREFIX,
                user.getMrnPatientIdentity(), user.getMrnPatientEncounterNum());
        Object object = orderRedisTemplate.opsForValue().get(redisLockKey);
        if (object != null) {
            String loginId = object.toString();
            if (StringUtils.isNotBlank(loginId)
                    && orderRedisTemplate.getExpire(redisLockKey, TimeUnit.SECONDS) > 0) {
                if (loginId.equalsIgnoreCase(user.getLoginId())) {
                    return false;
                } else {
                    return true;
                }
            } else {
                deleteOrder(redisLockKey);
                return true;
            }
        } else {
            return false;
        }
    }

    @Override
    public void addTokenIntoBlackList(String token) throws Exception {
        Duration timeout = Duration.ofMillis(tokenCacheExpire);

        String redisTokenKey = String.format(ServerConstant.MOE_CACHE_TOKEN_BLACKLIST_PREFIX,
                getUserDto().getSystemLogin(), getUserDto().getSpec(), getUserDto().getHospitalCd(),
                getUserDto().getLoginId(), UUID.randomUUID().toString());

        tokenRedisTemplate.opsForValue().set(redisTokenKey, token, timeout);
    }

    private String checkExistAndPresent(MoeMyFavouriteHdrDto cacheDto, UserDto user, String prefix, String newCacheId) throws Exception {

        String oldKey;
        String newKey;
        String oldCacheId = cacheDto.getCacheId();

        switch (prefix) {
            case ServerConstant.MOE_CACHE_DEPT_FAV_MAINTENANCE_PREFIX:
                oldKey = String.format(prefix, user.getSpec(), oldCacheId);
                newKey = String.format(prefix, user.getSpec(), newCacheId);
                break;
            case ServerConstant.MOE_CACHE_DEPT_FAV_MAINTENANCE_PREFIX_WITH_FAV_ID:
                oldKey = String.format(prefix, user.getSpec(), oldCacheId, cacheDto.getMyFavouriteId());
                newKey = String.format(prefix, user.getSpec(), newCacheId, cacheDto.getMyFavouriteId());
                break;
            default:
                throw new MoeServiceException();
        }
        try {
            if (favRedisTemplate.renameIfAbsent(oldKey, newKey)) {
                cacheDto.setCacheId(newCacheId);
                return newKey;
            } else {
                throw new ConcurrentException();
            }
        } catch (RedisSystemException e) {
            if (e.getCause() instanceof RedisCommandExecutionException) {
                throw new ConcurrentException();
            } else {
                throw e;
            }
        }
    }

    private boolean tryLockPrescription(UserDto user, int tryCount) throws Exception {
        if (tryCount < orderLockTryCount) {
            Duration timeout = Duration.ofMillis(orderLockExpire);

            String redisLockKey = String.format(ServerConstant.MOE_CACHE_LOCK_ORDER_PREFIX,
                    user.getMrnPatientIdentity(), user.getMrnPatientEncounterNum());
            if (orderRedisTemplate.opsForValue().setIfAbsent(redisLockKey, user.getLoginId(), timeout)) {
                return true;
            } else {
                Object object = orderRedisTemplate.opsForValue().get(redisLockKey);
                if (object != null) {
                    String loginId = object.toString();
                    if (StringUtils.isNotBlank(loginId)
                            && orderRedisTemplate.getExpire(redisLockKey, TimeUnit.SECONDS) > 0) {
                        if(loginId.equalsIgnoreCase(user.getLoginId())){
                            orderRedisTemplate.expire(redisLockKey, orderLockExpire, TimeUnit.MILLISECONDS);
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        deleteOrder(redisLockKey);
                        return tryLockPrescription(user, ++tryCount);
                    }
                } else {
                    return tryLockPrescription(user, ++tryCount);
                }
            }
        } else {
            return false;
        }
    }


    //Chris For Drug Search from Redis  -Start

/*    @Override
    public List<MoeDrugDto> matchDrugByKeyword(String keyword) {

        //Version1.1 --Start
        List<String> stringList = new ArrayList<>();
        List<MoeDrugDto> objectList = new ArrayList<>();

        String input = "*" + keyword.replaceAll("\\[", "\\\\[").toUpperCase() + "*";  //Processing escape symbol "["

        String pattern = "";

        Set<String> keysSet = stringRedisTemplate.keys(pattern);
        if (keysSet != null && keysSet.size() != 0) {
            stringList = stringRedisTemplate.opsForValue().multiGet(keysSet);
            objectList = (ArrayList<MoeDrugDto>)drugSearchTemplate.opsForValue().multiGet(keysSet);
        }

        System.out.println("objectList = " + objectList.toString());

        return objectList;

        //Version1.1 --End

    }*/


    @Override
    public List<MoeDrugDto> matchDrugByKeyword(String keyword) {

        //Version2.0 --Start
        List<MoeDrugDto> objectList = new ArrayList<>();
        List<MoeDrugDto> drugList = new ArrayList<>();

        Set<String> keysResult = new HashSet<>();

        //Count Duplicate Keys
        Map<String, Integer> countMap = new HashMap<>();

//        String input = "*" + keyword.replaceAll("\\[", "\\\\[").toUpperCase() + "*";  //Processing escape symbol "["

        String[] word = StringUtils.split(keyword);

        for (int i = 0; i < word.length; i++) {
            String input = word[i].replaceAll("\\[", "\\\\[").toUpperCase();

            //KEY_WORD_TEMPLATE_BASIC = "DRUG:DRUG_CATEGORY:*:DRUG_TYPE:*:MATCH_FIELD_REPLACEMENT_NAME:*%S*:CACHE_ID:*"
            String pattern = String.format(ServerConstant.KEY_WORD_TEMPLATE_BASIC, input);
//            String pattern = "*" + input +"*";

            //Redis Keys Result
            keysResult = this.keys(pattern);

            if (keysResult == null || keysResult.size() == 0) {
                return null;
            } else {

                keysResult.forEach(key ->{
                    if (countMap.get(key) == null) {
                        countMap.put(key, 1);
                    }else if (countMap.get(key) != null) {
                        countMap.put(key, countMap.get(key) + 1);
                    }
                });

            }

        }

        /*Iterator<String> iterator = countMap.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            if (countMap.get(key) != word.length) {
                iterator.remove();
            }
        }*/

        //Filter keys that do not match exactly
        Iterator<Map.Entry<String, Integer>> entrySetIterator = countMap.entrySet().iterator();
        while (entrySetIterator.hasNext()) {
            Map.Entry<String, Integer> entry = entrySetIterator.next();
            if (entry.getValue() != word.length) {
                entrySetIterator.remove();
            }
        }

        if (countMap != null && countMap.size() > 0) {

            Set<String> keySet = new HashSet<>();

            countMap.entrySet().iterator().forEachRemaining(entry->{
                keySet.add( entry.getKey() );
            });

//            stringList = stringRedisTemplate.opsForValue().multiGet(keysResult);
            //stringList = stringRedisTemplate.opsForValue().multiGet(keySet);
//            objectList = (ArrayList<MoeDrugDto>)drugSearchTemplate.opsForValue().multiGet(keySet);

            List<String> durgStrList = (ArrayList<String>)drugSearchTemplate.opsForValue().multiGet(keySet);

            for (String drugStr : durgStrList) {
                MoeDrugDto drug = JSON.parseObject(drugStr, MoeDrugDto.class);
                drugList.add(drug);
            }

        }

//        return objectList;
        return drugList;
        //Version2.0 --End

    }


    @Override
    public List<MoeDrugDto> matchDrugByKeywordForMultiIngr(String keyword) {

        List<MoeDrugDto> objectList = new ArrayList<>();
        Set<String> keysResult = new HashSet<>();

        //Count Duplicate Keys
        Map<String, Integer> countMap = new HashMap<>();

//        String input = "*" + keyword.replaceAll("\\[", "\\\\[").toUpperCase() + "*";  //Processing escape symbol "["

        String[] word = StringUtils.split(keyword);

        String input = word[0].replaceAll("\\[", "\\\\[").toUpperCase();

        //KEY_WORD_TEMPLATE_MULTI_INGR = "DRUG:DRUG_CATEGORY:*:DRUG_TYPE:BAN:MATCH_FIELD_ALIAS_NAME:%S*:CACHE_ID:*"S
        String pattern = String.format(ServerConstant.KEY_WORD_TEMPLATE_MULTI_INGR, input);
//            String pattern = "*" + input +"*";

        //Redis Keys Result
        keysResult = this.keys(pattern);

        if (keysResult == null || keysResult.size() == 0) {
            return null;
        } else {


            objectList = (ArrayList<MoeDrugDto>)drugSearchTemplate.opsForValue().multiGet(keysResult);
//                keysResult.forEach(key ->{
//                    if (countMap.get(key) == null) {
//                        countMap.put(key, 1);
//                    }else if (countMap.get(key) != null) {
//                        countMap.put(key, countMap.get(key) + 1);
//                    }
//                });

        }
//
//
//
//        /*Iterator<String> iterator = countMap.keySet().iterator();
//        while (iterator.hasNext()) {
//            String key = iterator.next();
//            if (countMap.get(key) != word.length) {
//                iterator.remove();
//            }
//        }*/
//
//        //Filter keys that do not match exactly
//        Iterator<Map.Entry<String, Integer>> entrySetIterator = countMap.entrySet().iterator();
//        while (entrySetIterator.hasNext()) {
//            Map.Entry<String, Integer> entry = entrySetIterator.next();
//            if (entry.getValue() != word.length) {
//                entrySetIterator.remove();
//            }
//        }
//
//
//        if (countMap != null && countMap.size() > 0) {
//
//            Set<String> keySet = new HashSet<>();
//
//            countMap.entrySet().iterator().forEachRemaining(entry->{
//                keySet.add( entry.getKey() );
//            });
//
////            stringList = stringRedisTemplate.opsForValue().multiGet(keysResult);
//            //stringList = stringRedisTemplate.opsForValue().multiGet(keySet);
//            objectList = (ArrayList<MoeDrugDto>)drugSearchTemplate.opsForValue().multiGet(keySet);
//        }

        return objectList;
        //Version2.0 --End

    }


    //    public List<String> keys(String pattern) {
    public Set<String> keys(String pattern) {
//        List<String> keys = new ArrayList<>();
        Set<String> keys = new HashSet<>();
        this.scan(pattern, item -> {
            String key = new String(item);
            keys.add(key);
        });
        return keys;
    }


    public void scan (String pattern , Consumer<byte[]> consumer) {
        //Detail Key
        //this.stringRedisTemplate.execute((RedisConnection connection) -> {
        //Single Key
        this.allDrugsStringRedisTemplate.execute((RedisConnection connection) -> {
            System.out.println(connection.toString());
            try (Cursor<byte[]> cursor = connection.scan(ScanOptions.scanOptions().count(Long.MAX_VALUE).match(pattern).build())) {
                cursor.forEachRemaining(consumer);
                return null;
            } catch (Exception e) {
                //TODO
                System.out.println("Drug scan by keyword failed.");
                //e.printStackTrace();
                throw new RuntimeException(e);
            } /*finally { //TEST
                if(connection!=null) {
                    connection.close();
                }
            }*/
        });

    }



    // Save Drug Suggestion Data into Redis with Json String format
    @Override
    public void cacheDrugSearchDataToJson() {

        //Total of all Drugs
        int count = 0;

        //4 Drug Cat
        for (DrugCategory drugCategory : DrugCategory.values()) {
            String drugCategoryStr = drugCategory.toString();

            //9 Drug Type
            for (DrugType drugType : DrugType.values()) {
                String drugTypeStr = drugType.toString();

                //IsDosage
                boolean isDosage = (DrugCategory.DOSAGE_DRUGS_STARTS_WITH.equals(drugCategory) || DrugCategory.DOSAGE_DRUGS_CONTAINS_WITH.equals(drugCategory));

                //Target Drugs Map
                List<MoeDrugDto> targetDrugs = isDosage ? DtoUtil.DOSAGE_DRUGS_BY_TYPE.get(drugTypeStr) : DtoUtil.DRUGS_BY_TYPE.get(drugTypeStr);

                //Add up every targetDrugs List
                count += targetDrugs.size();

                //Key Template : "DRUG:DRUG_CATEGORY:%S:DRUG_TYPE:%S:DRUG_NAME:%S:CACHE_ID:%S"
                String keyTemplate = ServerConstant.DRUG_SEARCH_CACHE_PREFIX_BASIC;

                ObjectMapper objectMapper = new ObjectMapper();

//                Set<String> drugSet = new HashSet<>();
//                Set<MoeDrugDto> drugDtoSet = new HashSet<>();

                String objectString = "";
                for (MoeDrugDto drug : targetDrugs) {
                    try {
                        //Set key & objectString into Redis
                        //DRUG_SEARCH_CACHE_PREFIX = "DRUG:DRUG_CATEGORY:%S:DRUG_TYPE:%S:DRUG_NAME:%S:CACHE_ID:%S";
                        String cacheId = UUID.randomUUID().toString();
                        String redisKey = String.format(keyTemplate, drugCategoryStr, drugTypeStr, drug.toString(), cacheId);

                        //Save RedisKey & DrugCategory & DrugType into Drug Obj Data for Redis
//                        drug.setRedisKey(redisKey);
//                        drug.setDrugCategory(drugCategoryStr);
//                        drug.setDrugType(drugTypeStr);

                        //Obj convert to String
                        objectString = objectMapper.writeValueAsString(drug);

                        drugSearchTemplate.opsForValue().set(redisKey, objectString);

                        //Log duplicate redisKey
//                        boolean duplicatekeySetResult = drugSet.add(redisKey);
//                        boolean duplicateDrugDto = drugDtoSet.add(drug);
//                        if (!duplicatekeySetResult) {
//                            System.out.println("Key set Result: " + drug.getDisplayString() + "======== Duplicate Redis Key : " + redisKey);
//                        }
//                        if (!duplicateDrugDto) {
//                            System.out.println("Drug set Result: ======== Duplicate Drug: " + drug.getDisplayString());
//                        }

                    } catch (Exception e) {
                        System.out.println("Exception = " + e);
                    }
                }
                //Duplicate Result Set
//                System.out.println("Drug Category = " + drugCategoryStr + ", Drug Type = " + drugTypeStr + ", Duplicate Redis Key Set's size = " + drugSet.size());
//                System.out.println("Drug Category = " + drugCategoryStr + ", Drug Type = " + drugTypeStr + ", Duplicate DrugDto Set's size = " + drugDtoSet.size());
            }

        }
        //Print Count
        System.out.println("count = " + count);

    }

//    private enum DrugType {
//        TRADENAMEALIAS, TRADENAME, GENERIC, VTM, BAN, OTHER, ABB, NONMTT, MULTIINGR;
//    }

    private enum DrugCategory {
        DRUGS_STARTS_WITH, DRUGS_CONTAINS_WITH, DOSAGE_DRUGS_STARTS_WITH, DOSAGE_DRUGS_CONTAINS_WITH;
    }

    // Save Drug Suggestion Data into Redis with Object format
    @Override
    public void cacheDrugSearchDataToObject() {

        //Total of all Drugs
        int count = 0;

        //Insert Basic Target
        //4 Drug Cat
        for (DrugCategory drugCategory : DrugCategory.values()) {
            String drugCategoryStr = drugCategory.toString();

            //9 Drug Type
            for (DrugType drugType : DrugType.values()) {
                String drugTypeStr = drugType.toString();

                boolean isDosage = (DrugCategory.DOSAGE_DRUGS_STARTS_WITH.equals(drugCategory) || DrugCategory.DOSAGE_DRUGS_CONTAINS_WITH.equals(drugCategory));

                //Target Drugs Map
                List<MoeDrugDto> targetDrugs = isDosage ? DtoUtil.DOSAGE_DRUGS_BY_TYPE.get(drugTypeStr) : DtoUtil.DRUGS_BY_TYPE.get(drugTypeStr);
                //List<MoeDrugDto> drugMoeDrugDtos = DtoUtil.DRUGS_BY_TYPE.get(drugType);
                //List<MoeDrugDto> dosageMoeDrugDtos = DtoUtil.DOSAGE_DRUGS_BY_TYPE.get(drugType);

                //Add up every targetDrugs List
                count += targetDrugs.size();

                //Target Key Template : "DRUG:DRUG_CATEGORY:%S:DRUG_TYPE:%S:DRUG_NAME:%S:CACHE_ID:%S"
                String targetKeyTemplate = ServerConstant.DRUG_SEARCH_CACHE_PREFIX_BASIC;

                //ObjectMapper objectMapper = new ObjectMapper();

//                Set<String> drugSet = new HashSet<>();
//                Set<MoeDrugDto> drugDtoSet = new HashSet<>();

                for (MoeDrugDto drug : targetDrugs) {
                    try {
                        //Set key & objectString into Redis
                        //DRUG_SEARCH_CACHE_PREFIX = "DRUG:DRUG_CATEGORY:%S:DRUG_TYPE:%S:DRUG_NAME:%S:CACHE_ID:%S";
                        String cacheId = UUID.randomUUID().toString();
                        String redisKey = String.format(targetKeyTemplate, drugCategoryStr, drugTypeStr, drug.toString(), cacheId);

                        //Save RedisKey & DrugCategory & DrugType into Drug Obj Data for Redis
//                        drug.setRedisKey(redisKey);
//                        drug.setDrugCategory(drugCategoryStr);
//                        drug.setDrugType(drugTypeStr);


                        //Obj convert to String
                        ObjectMapper objectMapper = new ObjectMapper();
                        String objectString = objectMapper.writeValueAsString(drug);

                        drugSearchTemplate.opsForValue().set(redisKey, objectString);   //MoeDrugDto
//                        drugSearchTemplate.opsForValue().set(redisKey, drug);

                        //Log duplicate redisKey
//                        boolean duplicatekeySetResult = drugSet.add(redisKey);
//                        boolean duplicateDrugDto = drugDtoSet.add(drug);
//                        if (!duplicatekeySetResult) {
//                            System.out.println("Key set Result: " + drug.getDisplayString() + "======== Duplicate Redis Key : " + redisKey);
//                        }
//                        if (!duplicateDrugDto) {
//                            System.out.println("Drug set Result: ======== Duplicate Drug: " + drug.getDisplayString());
//                        }

                    } catch (Exception e) {
                        System.out.println("Exception = " + e);
                    }
                }   //Traversal targetDrugs  End

            }   //9 Type  End

        }   //4 Cat  End

        //Print Basic Target Drug's Count
        System.out.println("Basic Target Drug's Count = " + count);


        count = 0;
/*

        //Multi Ingr
        List<MoeDrugDto> dosageDrugTarget = DtoUtil.DOSAGE_DRUGS_BY_TYPE.get(DrugType.BAN.toString());
        List<MoeDrugDto> drugTarget = DtoUtil.DRUGS_BY_TYPE.get(DrugType.BAN.toString());

        //Target Key Template : "DRUG:DRUG_CATEGORY:%S:DRUG_TYPE:BAN:MATCH_FIELD_ALIAS_NAME:%S:CACHE_ID:%S"
        String targetKeyTemplate = ServerConstant.DRUG_SEARCH_CACHE_PREFIX_MULTI_INGR;

        //dosageDrugTarget
        for (MoeDrugDto drug : dosageDrugTarget) {
            try {
                //Set key & objectString into Redis
                String cacheId = UUID.randomUUID().toString();

                String aliasName = "";
                if (drug.getAliasNames() != null && drug.getAliasNames().size() > 0 && drug.getAliasNames().get(0) != null && drug.getAliasNames().get(0).getAliasName() != null) {
                        aliasName = drug.getAliasNames().get(0).getAliasName().toLowerCase();
                } else {
                    //Drug doesn't have the Alias Name
                    continue;
                }
                String redisKey = String.format(targetKeyTemplate, DrugCategory.DOSAGE_DRUGS_CONTAINS_WITH.toString(), aliasName, cacheId);

                //Save RedisKey & DrugCategory & DrugType into Drug Obj Data for Redis
                drug.setRedisKey(redisKey);
                drug.setDrugCategory(DrugCategory.DOSAGE_DRUGS_CONTAINS_WITH.toString());
                drug.setDrugType(DrugType.BAN.toString());

                drugSearchTemplate.opsForValue().set(redisKey, drug);

                count++;

            } catch (Exception e) {
                System.out.println("Exception = " + e);
            }
        }   //Traversal dosageDrugTarget  End


        //drugTarget
        for (MoeDrugDto drug : drugTarget) {
            try {
                //Set key & objectString into Redis
                String cacheId = UUID.randomUUID().toString();

                String aliasName = "";
                if (drug.getAliasNames() != null && drug.getAliasNames().size() > 0 && drug.getAliasNames().get(0) != null && drug.getAliasNames().get(0).getAliasName() != null) {
                    aliasName = drug.getAliasNames().get(0).getAliasName().toLowerCase();
                } else {
                    //Drug doesn't have the Alias Name
                    continue;
                }
                String redisKey = String.format(targetKeyTemplate, DrugCategory.DRUGS_CONTAINS_WITH.toString(), aliasName, cacheId);

                //Save RedisKey & DrugCategory & DrugType into Drug Obj Data for Redis
                drug.setRedisKey(redisKey);
                drug.setDrugCategory(DrugCategory.DRUGS_CONTAINS_WITH.toString());
                drug.setDrugType(DrugType.BAN.toString());

                drugSearchTemplate.opsForValue().set(redisKey, drug);

                count++;

            } catch (Exception e) {
                System.out.println("Exception = " + e);
            }
        }   //Traversal drugTarget  End

        System.out.println("Multi Ingr Target Drug's Count = " + count);
*/
    }


    @Override
    public void cacheAllDrugsInRedis() throws Exception {

        System.out.println("======== Start to reload all drugs data in Redis ========");
        //Delete old data in Redis
        //KEY_WORD_PREFIX = "DRUG:*"
//        Set<String> keySet = this.keys(ServerConstant.KEY_WORD_PREFIX);   /*Can not delete completely. May be cause by serialization. (Between different redisTemplate)*/
        Set keySet = allDrugsRedisTemplate.keys(ServerConstant.KEY_WORD_PREFIX);
//        Set keySet = allDrugsStringRedisTemplate.keys(ServerConstant.KEY_WORD_PREFIX);
        Long deleteTotalNum = 0L;
        if (keySet != null && keySet.size() > 0) {
            //Delete all drugs by key set
            deleteTotalNum = allDrugsRedisTemplate.delete(keySet);
//            Long deleteTotalNum = allDrugsStringRedisTemplate.delete(keySet);
        }
        System.out.println("======== Total number of drug deletions: " + deleteTotalNum + " ========");

//        Map<String, MoeDrugDto> drugs = new HashMap<>();
        Map<String, String> drugs = new HashMap<>();

        //"DRUG:DRUGS_BY_TYPE:DRUG_TYPE:%S:CACHE_ID:%S"
        //"DRUG:DRUGS_BY_TYPE:DRUG_TYPE:%S:REPLACEMENT_NAME:%S:CACHE_ID:%S"
        String drugsRedisKeyTemplate = ServerConstant.DRUGS_BY_TYPE_SIMPLE_REDIS_KEY;
        //"DRUG:DOSAGE_DRUGS_BY_TYPE:DRUG_TYPE:%S:CACHE_ID:%S"
        //"DRUG:DOSAGE_DRUGS_BY_TYPE:DRUG_TYPE:%S:REPLACEMENT_NAME:%S:CACHE_ID:%S"
        String dosageDrugsRedisKeyTemplate = ServerConstant.DOSAGE_DRUGS_BY_TYPE_SIMPLE_REDIS_KEY;

        //"DRUG:DRUGS_BY_TYPE:DRUG_TYPE:BAN:ALIAS_NAME:%S:CACHE_ID:%S"
        String drugsOfBanRedisKeyTemplate = ServerConstant.DRUGS_BY_TYPE_OF_BAN_REDIS_KEY;
        //"DRUG:DOSAGE_DRUGS_BY_TYPE:DRUG_TYPE:BAN:ALIAS_NAME:%S:CACHE_ID:%S"
        String dosageDrugsOfBanRedisKeyTemplate = ServerConstant.DOSAGE_DRUGS_BY_TYPE_OF_BAN_REDIS_KEY;

        //String isDosage = "DOSAGE_DRUGS_BY_TYPE";
        //String notDosage = "DRUGS_BY_TYPE";

        //"DRUG:GENERIC_NAME_LEVEL:LOCAL_DRUG_ID:%S"
        String genericNameDrugsRedisKeyTemplate = ServerConstant.GENERIC_NAME_LEVEL_DRUGS_REDIS_KEY;


        //JsonConfig    For handle jsonObject default value  -Start
        JsonConfig jsonConfig = new JsonConfig();
        DefaultDefaultValueProcessor defaultDefaultValueProcessor = new DefaultDefaultValueProcessor(){
            public Object getDefaultValue(Class type) {
                if (JSONUtils.isArray(type)) {
                    return null;        //return new JSONArray();
                } else if (JSONUtils.isNumber(type)) {
                    return null;        //return JSONUtils.isDouble(type) ? new Double(0.0D) : new Integer(0);
                } else if (JSONUtils.isBoolean(type)) {
                    return null;        //return Boolean.FALSE;
                } else {
                    return JSONUtils.isString(type) ? /*null*/ JSONNull.getInstance()/*""*/ : JSONNull.getInstance();
                }
            }
        };

        jsonConfig.registerDefaultValueProcessor(Long.class, defaultDefaultValueProcessor);
        jsonConfig.registerDefaultValueProcessor(Integer.class, defaultDefaultValueProcessor);
        jsonConfig.registerDefaultValueProcessor(String.class, defaultDefaultValueProcessor);
        jsonConfig.registerDefaultValueProcessor(List.class, defaultDefaultValueProcessor);
        jsonConfig.registerDefaultValueProcessor(BigDecimal.class, defaultDefaultValueProcessor);
        //JsonConfig    For handle jsonObject default value  -End

        try {
            //9 Drug Type
            for (DrugType drugType : DrugType.values()) {
                String drugTypeStr = drugType.toString();
                System.out.println("Load with Drug Type : " + drugTypeStr);

                List<MoeDrugDto> drugsTargetDrugs = DtoUtil.DRUGS_BY_TYPE.get(drugTypeStr);
                List<MoeDrugDto> dosageDrugsTargetDrugs = DtoUtil.DOSAGE_DRUGS_BY_TYPE.get(drugTypeStr);

                //Index in Type
                int index = 0;
                //Package the drug data of DRUGS_BY_TYPE
                if (drugsTargetDrugs != null && drugsTargetDrugs.size() > 0) {
                    Iterator<MoeDrugDto> iterator = drugsTargetDrugs.iterator();
                    while (iterator.hasNext()) {
                        MoeDrugDto drug = iterator.next();
                        if (drug == null) {
                            System.out.println("This drug is null, DrugType = " + drugTypeStr);
                        } else {

                            /*Exist Object with the same memory address , Direct operation will modify the previous Objects. */
//                            MoeDrugDto insertDrug = new MoeDrugDto();
//                            BeanUtils.copyProperties(drug, insertDrug);

                            String uuid = UUID.randomUUID().toString();
                            //"DRUG:DRUGS_BY_TYPE:DRUG_TYPE:%S:CACHE_ID:%S"
                            //"DRUG:DRUGS_BY_TYPE:DRUG_TYPE:%S:REPLACEMENT_NAME:%S:CACHE_ID:%S"
                            //"DRUG:DOSAGE_DRUGS_BY_TYPE:DRUG_TYPE:%S:REPLACEMENT_NAME:%S:CACHE_ID:%S"
//                            String redisKey = String.format(drugsRedisKeyTemplate, drugTypeStr, uuid);
                            String redisKey = String.format(drugsRedisKeyTemplate, drugTypeStr, drug.toString(), uuid);

                            //For keep sort
                            drug.setIndexInType(index);
                            index++;
//                            drug.setRedisKey(redisKey);
//                            drug.setDrugType(drugTypeStr);
//                            drug.setDrugCategory(isDosage);

//                            drugs.put(redisKey, drug);
                            //drugs.put(redisKey, JSONObject.fromObject(drug).toString());
                            drugs.put(redisKey, JSONObject.fromObject(drug, jsonConfig).toString());

//                            insertDrug.setRedisKey(redisKey);
//                            insertDrug.setDrugType(drugTypeStr);
//                            insertDrug.setDrugCategory(notDosage);

//                            drugs.put(redisKey, insertDrug);
                        }
                    }
                }

                //Index in Type
                index = 0;
                //Package the drug data of DOSAGE_DRUGS_BY_TYPE
                if (dosageDrugsTargetDrugs != null && dosageDrugsTargetDrugs.size() > 0) {
                    Iterator<MoeDrugDto> iterator = dosageDrugsTargetDrugs.iterator();
                    while (iterator.hasNext()) {
                        MoeDrugDto drug = iterator.next();
                        if (drug == null) {
                            System.out.println("This drug is null, DrugType = " + drugTypeStr);
                        } else {

//                            MoeDrugDto insertDrug = new MoeDrugDto();
//                            BeanUtils.copyProperties(drug, insertDrug);

                            String uuid = UUID.randomUUID().toString();
                            //"DRUG:DOSAGE_DRUGS_BY_TYPE:DRUG_TYPE:%S:CACHE_ID:%S"
//                            String redisKey = String.format(dosageDrugsRedisKeyTemplate, drugTypeStr, uuid);
                            String redisKey = String.format(dosageDrugsRedisKeyTemplate, drugTypeStr, drug.toString(), uuid);

                            //For keep sort
                            drug.setIndexInType(index);
                            index++;
//                            drug.setRedisKey(redisKey);
//                            drug.setDrugType(drugTypeStr);
//                            drug.setDrugCategory(isDosage);

//                            drugs.put(redisKey, drug);    /*Exist Object with the same memory address , Direct operation will modify the previous Objects. */
                            //drugs.put(redisKey, JSONObject.fromObject(drug).toString());
                            drugs.put(redisKey, JSONObject.fromObject(drug, jsonConfig).toString());

//                            insertDrug.setRedisKey(redisKey);
//                            insertDrug.setDrugType(drugTypeStr);
//                            insertDrug.setDrugCategory(isDosage);

//                            drugs.put(redisKey, insertDrug);
                        }
                    }
                }


                //Insert BAN type's ALIAS_NAME
                if(drugType.equals(DrugType.BAN)) {
                    //Index in Type
                    index = 0;
                    //DRUGS_BY_TYPE
                    if (drugsTargetDrugs != null && drugsTargetDrugs.size() > 0) {
                        Iterator<MoeDrugDto> iterator = drugsTargetDrugs.iterator();
                        while (iterator.hasNext()) {
                            MoeDrugDto drug = iterator.next();
                            if (drug == null) {
                                System.out.println("This drug is null in BAN Type for Search Multiple Ingredient, Drugs Target = DRUGS_BY_TYPE");
                            } else {
                                String uuid = UUID.randomUUID().toString();
                                String redisKey = String.format(drugsOfBanRedisKeyTemplate, drug.getAliasNames().get(0).getAliasName(), uuid);
                                drug.setIndexInType(index);
                                index++;
                                drugs.put(redisKey, JSONObject.fromObject(drug, jsonConfig).toString());
                            }
                        }
                    }

                    //Index in Type
                    index = 0;
                    //DOSAGE_DRUGS_BY_TYPE
                    if (dosageDrugsTargetDrugs != null && dosageDrugsTargetDrugs.size() > 0) {
                        Iterator<MoeDrugDto> iterator = dosageDrugsTargetDrugs.iterator();
                        while (iterator.hasNext()) {
                            MoeDrugDto drug = iterator.next();
                            if (drug == null) {
                                System.out.println("This drug is null in BAN Type for Search Multiple Ingredient, Drugs Target = DOSAGE_DRUGS_BY_TYPE");
                            } else {
                                String uuid = UUID.randomUUID().toString();
                                String redisKey = String.format(dosageDrugsOfBanRedisKeyTemplate, drug.getAliasNames().get(0).getAliasName(), uuid);
                                drug.setIndexInType(index);
                                index++;
                                drugs.put(redisKey, JSONObject.fromObject(drug, jsonConfig).toString());
                            }
                        }
                    }

                }

            }   //ForEach 9 DrugType End


            //Chris 20200110  Cache Generic Name Level Drugs  -Start
            List<MoeDrugDto> genericNameTargetDrugs = DtoUtil.GENERIC_NAME_DRUGS;

            int index = 0;
            //GENERIC_NAME_LEVEL
            if (genericNameTargetDrugs != null && genericNameTargetDrugs.size() > 0) {
                System.out.println("Load with Generic Name Level Drugs.");
                Iterator<MoeDrugDto> iterator = genericNameTargetDrugs.iterator();
                while (iterator.hasNext()) {
                    MoeDrugDto drug = iterator.next();
                    if (drug == null) {
                        System.out.println("This drug is null in Generic Name Level , Drugs Target = DtoUtil.GENERIC_NAME_DRUGS");
                    } else {
                        String redisKey = String.format(genericNameDrugsRedisKeyTemplate, drug.getLocalDrugId());
                        drug.setIndexInType(index);
                        index++;
                        drugs.put(redisKey, JSONObject.fromObject(drug, jsonConfig).toString());
                    }
                }
            }
            //Chris 20200110  Cache Generic Name Level Drugs  -End


            //Insert into Redis
            //test StringRedisTemplate save and get
//            allDrugsStringRedisTemplate.opsForValue().multiSet(drugs);
            allDrugsRedisTemplate.opsForValue().multiSet(drugs);
            System.out.println("======== Total number of drug creations: " + drugs.size() + " ========");

        } catch (Exception e) {
            System.out.println("Failed to cache all drug items into Redis.");
//            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }


    @Override
    public Map<String, Map<String, List<MoeDrugDto>>> getAllDrugsFromRedis(String input, Boolean isSearchMultiIngrByBanVtm) throws Exception {

        String matchKeyTemplate = "DRUG:*:DRUG_TYPE:*:REPLACEMENT_NAME:*:CACHE_ID:*";
//        String matchKeyTemplate = "DRUG:*:DRUG_TYPE:*:CACHE_ID:*";
        String drugListType = "DRUGS_BY_TYPE";
        String dosageDrugListType = "DOSAGE_DRUGS_BY_TYPE";

//        Set<String> keySet = new HashSet<>();
        Set<String> keySet = new LinkedHashSet<>();
        //For All Type fetch drug by Replacement Name
        Map<String, List<MoeDrugDto>> drugsMap = new HashMap<>();
        Map<String, List<MoeDrugDto>> dosageDrugsMap = new HashMap<>();
        //For BAN Type fetch drug by Alias Name
        Map<String, List<MoeDrugDto>> banDrugsMap = new HashMap<>();
        Map<String, List<MoeDrugDto>> banDosageDrugsMap = new HashMap<>();

        Map<String, Map<String, List<MoeDrugDto>>> total = new HashMap<>();


        //Get all drug Dto
//        String pattern = matchKeyTemplate;

        //String pattern = "DRUG:*:DRUG_TYPE:*:REPLACEMENT_NAME:*" + input.replaceAll("\\[", "\\\\[").toUpperCase() + "*:CACHE_ID:*";

        String[] words = StringUtils.split(input.replaceAll("\\[", "\\\\[").toUpperCase());
        String processKeyword = "";
        String pattern = "";

        if (!isSearchMultiIngrByBanVtm) {
            //For Search in all type (Match Replacement Name)
            for (int i = 0; i < words.length; i++) {
                processKeyword += (words[i] + "*");
            }
            pattern = "DRUG:*:DRUG_TYPE:*:REPLACEMENT_NAME:*" + processKeyword/* + ":CACHE_ID:*"*/;
        } else {
            //For Search Multiple Ingredient by BAN type VTM (Match Alias Name)
            processKeyword = words[0] + "*";
            pattern = "DRUG:*:DRUG_TYPE:BAN:ALIAS_NAME:*" + processKeyword/* + ":CACHE_ID:*"*/;
        }

        keySet = this.keys(pattern);
        if (keySet == null || keySet.size() == 0) {
            return null;
        }

//        List<MoeDrugDto> resultDrugList = new ArrayList<>();
//        keySet.forEach(key->{
////            System.out.println("Key = " + key);
//            Object obj = allDrugsRedisTemplate.opsForValue().get(key);
//            if (obj != null &&  obj instanceof MoeDrugDto) {
//                MoeDrugDto drug = (MoeDrugDto) obj;
//                resultDrugList.add(drug);
//            }
//
//        });
//        System.out.println("resultDrugList.size() = " + resultDrugList.size());


        //Multiple get Obj from Redis by key set
        Object obj = allDrugsRedisTemplate.opsForValue().multiGet(keySet);

        //Test mult get by StringRedisTemplate
//        Object obj = allDrugsStringRedisTemplate.opsForValue().multiGet(keySet);    //java.lang.ClassCastException: class net.sf.json.JSONNull cannot be cast to class net.sf.json.JSONObject //Cause by serializer.

        //convert to Json String
        JSONArray jsonArray = JSONArray.fromObject(obj);

        //MoeDrugStrengthDto
        //List<MoeDrugAliasNameDto> aliasNames
        //List<MoeCommonDosageDto> commonDosages
        //MoeFormDto form
        //MoeRouteDto route
        //MoeBaseUnitDto prescribeUnitId
        //List<MoeDrugStrengthDto> strengths
        Map<String, Class> classMap = new HashMap<>();
        classMap.put("aliasNames", MoeDrugAliasNameDto.class);
        classMap.put("commonDosages", MoeCommonDosageDto.class);
        classMap.put("strengths", MoeDrugStrengthDto.class);
//        classMap.put("moeFormDto", MoeFormDto.class);
//        classMap.put("moeRouteDto", MoeRouteDto.class);
//        classMap.put("moeBaseUnitDto", MoeBaseUnitDto.class);

        //convert KeySet to List
//        List<String> resultKeyList = new ArrayList<>(keySet);
        //For delete invalid key in KeySet
        List<Integer> deleteKeyList = new ArrayList<Integer>();

        List<MoeDrugDto> resultDrugList = new ArrayList<>();

        for (int i=0; i < jsonArray.size(); i++) {
            try {
                if (jsonArray.get(i) instanceof net.sf.json.JSONNull) {
                    deleteKeyList.add(i);
                }

                MoeDrugDto drug = (MoeDrugDto)JSONObject.toBean((JSONObject)jsonArray.get(i), MoeDrugDto.class, classMap);
//                  drug.setRedisKey(i + "");
                resultDrugList.add(drug);
            } catch (ClassCastException cce) {
                System.out.println("The No." + (i+1) +" drug json is failed to fetch from Redis!");
            } catch (Exception e) {
                System.out.println("The No." + (i+1) +" drug json is failed to cast to MoeDrugDto!");
            }
        }


//        JSONObject.toBean(jsonObject, MoeDrugDto.class, classMap);
//        List<MoeDrugDto> resultDrugList2 = (List<MoeDrugDto>)JSONArray.toArray(jsonArray, List.class, classMap);      //Exception


        //JsonStr
//        List<MoeDrugDto> resultDrugList2 = (List<MoeDrugDto>)JSONArray.toCollection(jsonArray, MoeDrugDto.class);


        // ====================== Only match "Adal" from DtoUtil's Map Data. Result is true! ======================
        //Test "Adal" -Start
        /*String testInput1 = "Adal";
        String testInput2 = "cap";
        Map<String, List<MoeDrugDto>> target1 = DtoUtil.DRUGS_BY_TYPE;
        Map<String, List<MoeDrugDto>> target2 = DtoUtil.DOSAGE_DRUGS_BY_TYPE;
        List<MoeDrugDto> tradename1 = target1.get("TRADENAME");
        List<MoeDrugDto> vtm1 = target1.get("VTM");
        List<MoeDrugDto> tradename2 = target2.get("TRADENAME");
        List<MoeDrugDto> vtm2 = target2.get("VTM");
        Map<String, List<MoeDrugDto>> drugMap = new HashMap<>();
        Map<String, List<MoeDrugDto>> dosageMap = new HashMap<>();
        List<MoeDrugDto> drugResult1 = new ArrayList<>();
        List<MoeDrugDto> drugResult2 = new ArrayList<>();
        List<MoeDrugDto> dosageResult1 = new ArrayList<>();
        List<MoeDrugDto> dosageResult2 = new ArrayList<>();

        tradename1.forEach(d->{
            if (d.toString().contains(testInput1) && d.toString().contains(testInput2)) {
                drugResult1.add(d);
            }
        });
        drugMap.put("TRADENAME", drugResult1);
        vtm1.forEach(d->{
            if (d.toString().contains(testInput1) && d.toString().contains(testInput2)) {
                drugResult2.add(d);
            }
        });
        drugMap.put("VTM", drugResult2);

        tradename2.forEach(d->{
            if (d.toString().contains(testInput1) && d.toString().contains(testInput2)) {
                dosageResult1.add(d);
            }
        });
        dosageMap.put("TRADENAME", dosageResult1);
        vtm2.forEach(d->{
            if (d.toString().contains(testInput1) && d.toString().contains(testInput2)) {
                dosageResult2.add(d);
            }
        });
        dosageMap.put("VTM", dosageResult2);*/
        //Test "Adal" -End
        // ====================== Only match "Adal" from DtoUtil's Map Data. Result is true! ======================




//        List<MoeDrugDto> resultDrugList3 = (ArrayList<MoeDrugDto>)allDrugsRedisTemplate.opsForValue().multiGet(keySet);


        //Delete invalid key
        Iterator<String> it = keySet.iterator();
        int keySetIndex = 0;
        while (it.hasNext()) {
            it.next();
            if (deleteKeyList.contains(keySetIndex)) {
                it.remove();
            }
            keySetIndex++;
        }

        //Validate
        if(keySet.size() != resultDrugList.size()) {
            throw new MoeServiceException("The number of key and value is inconsistent.");
        }

        Map<String, MoeDrugDto> keyValueMap = new HashMap<>();
        int index = 0;
        for (String key : keySet) {
            keyValueMap.put(key, resultDrugList.get(index));
            index++;
        }

        if (resultDrugList != null & resultDrugList.size() > 0) {

            int count = 0;
            int nullCount = 0;
            //9 Drug Type
            for (DrugType drugType : DrugType.values()) {
                String drugTypeStr = drugType.toString();

                List<MoeDrugDto> drugList = new ArrayList<>();
                List<MoeDrugDto> dosageDrugList = new ArrayList<>();
                //For store BAN type drugs
                List<MoeDrugDto> banDrugList = new ArrayList<>();
                List<MoeDrugDto> banDosageDrugList = new ArrayList<>();

                //Version 2
                Iterator<Map.Entry<String, MoeDrugDto>> iterator = keyValueMap.entrySet().iterator();
                    while (iterator.hasNext()) {
                    Map.Entry<String, MoeDrugDto> entry = iterator.next();
                    String redisKey = entry.getKey();
                    MoeDrugDto redisValue = entry.getValue();
                    if (redisValue == null) {
                        System.out.println("Drug is null");
                    }
                    if (redisKey.contains("DRUG_TYPE:" + drugTypeStr + ":REPLACEMENT_NAME:")) {
                        if (redisKey.contains("DRUG:" + drugListType + ":DRUG_TYPE:")) {
                            drugList.add(redisValue);
                        } else if (redisKey.contains("DRUG:" + dosageDrugListType + ":DRUG_TYPE:")) {
                            dosageDrugList.add(redisValue);
                        }
                    } else if (redisKey.contains("DRUG_TYPE:BAN:ALIAS_NAME:")) {
                        if (redisKey.contains("DRUG:" + drugListType + ":DRUG_TYPE:")) {
                            banDrugList.add(redisValue);
                        } else if (redisKey.contains("DRUG:" + dosageDrugListType + ":DRUG_TYPE:")) {
                            banDosageDrugList.add(redisValue);
                        }

                    }
                } //Foreach End


                //Version 1
                //resultDrugList.forEach(drug->{
                /*for (MoeDrugDto drug : resultDrugList) {
                    count++;
                    if (drug == null) {
                        System.out.println(count);
                        nullCount++;
                    } else {
                        String redisKey = drug.getRedisKey();
                        // DRUG:*:DRUG_TYPE:*:REPLACEMENT_NAME:*:CACHE_ID:*
                        if (redisKey.contains("DRUG_TYPE:" + drugTypeStr + ":CACHE_ID:")) {
                            if (redisKey.contains("DRUG:" + drugListType + ":DRUG_TYPE:")) {
                                drugList.add(drug);

                            } else if (redisKey.contains("DRUG:" + dosageDrugListType + ":DRUG_TYPE:")) {
                                dosageDrugList.add(drug);

                            }
                        }
                    }

                }*/
                //});

                if (drugList.size() > 0) {
                    //Sort after match result
                    Collections.sort(drugList, new Comparator<MoeDrugDto>() {
                        @Override
                        public int compare(MoeDrugDto o1, MoeDrugDto o2) {
                            return o1.getIndexInType() - o2.getIndexInType();
                        }
                    });
                    drugsMap.put(drugTypeStr, drugList);
                }
                if (dosageDrugList.size() > 0) {
                    //Sort after match result
                    Collections.sort(dosageDrugList, new Comparator<MoeDrugDto>() {
                        @Override
                        public int compare(MoeDrugDto o1, MoeDrugDto o2) {
                            return o1.getIndexInType() - o2.getIndexInType();
                        }
                    });
                    dosageDrugsMap.put(drugTypeStr, dosageDrugList);
                }

                //Search Multiple Ingredient By BAN Vtm
                if (banDrugList.size() > 0) {
                    Collections.sort(banDrugList, new Comparator<MoeDrugDto>() {
                        @Override
                        public int compare(MoeDrugDto o1, MoeDrugDto o2) {
                            return o1.getIndexInType() - o2.getIndexInType();
                        }
                    });
                    banDrugsMap.put("BAN", banDrugList);
                }
                if (banDosageDrugList.size() > 0) {
                    Collections.sort(banDrugList, new Comparator<MoeDrugDto>() {
                        @Override
                        public int compare(MoeDrugDto o1, MoeDrugDto o2) {
                            return o1.getIndexInType() - o2.getIndexInType();
                        }
                    });
                    banDosageDrugsMap.put("BAN", banDosageDrugList);
                }

            }

            //Test 2  -Start
            /*drugsMap.put("VTM", drugsMap.get("TRADENAME"));
            dosageDrugsMap.put("VTM", dosageDrugsMap.get("TRADENAME"));*/
            //Test 2  -End

            if (isSearchMultiIngrByBanVtm) {
                //Sort Redis Data of BAN Type
                sortDrugContent(banDrugsMap, banDosageDrugsMap);

                //DRUG_BY_TYPE
                total.put(drugListType, banDrugsMap);
                //DOSAGE_DRUG_BY_TYPE
                total.put(dosageDrugListType, banDosageDrugsMap);
            } else {
                //Sort Redis Data
                sortDrugContent(drugsMap, dosageDrugsMap);

                //DRUG_BY_TYPE
                total.put(drugListType, drugsMap);
                //DOSAGE_DRUG_BY_TYPE
                total.put(dosageDrugListType, dosageDrugsMap);      System.out.println("==================== Match Data from Redis! ====================");
            }

            System.out.println("null Count = " + nullCount);


            // ====================== Only match "Adal" from DtoUtil's Map Data. Result is true!  (Debug it's not necessarily true)======================
            //Test "Adal" -Start
//            total.put(drugListType, drugMap);
//            total.put(dosageDrugListType, dosageMap);     System.out.println("==================== Match Data from DtoUtil! ====================");
            //Test "Adal" -End
            // ====================== Only match "Adal" from DtoUtil's Map Data. Result is true! ======================

            return total;
        }
        return null;
    }


    //For sort by Type
    private void sortDrugContent(Map<String, List<MoeDrugDto>> drugsByType, Map<String, List<MoeDrugDto>> dosageDrugsByType) {
        for (DrugType type : DrugType.values()) {
            DrugSearchEngine.DrugSearchType comparatorType = DrugSearchEngine.DrugSearchType.ASSORTED;
            if (type.equals(DrugType.TRADENAME)) {
                comparatorType = DrugSearchEngine.DrugSearchType.TRADENAME;
            } else if (type.equals(DrugType.GENERIC)) {
                comparatorType = DrugSearchEngine.DrugSearchType.GENERIC;
            } else if (type.equals(DrugType.NONMTT)) {
                comparatorType = DrugSearchEngine.DrugSearchType.NONMTT;
            }

            //List<MoeDrugDto> drugs = DtoUtil.DRUGS_BY_TYPE.get(type.toString());
            List<MoeDrugDto> drugs = drugsByType.get(type.toString());
            if (drugs != null) {
                Collections.sort(drugs, DrugSearchEngine.COMPARATOR_BY_TYPE.get(comparatorType));
            }

            //drugs = DtoUtil.DOSAGE_DRUGS_BY_TYPE.get(type.toString());
            drugs = dosageDrugsByType.get(type.toString());
            if (drugs != null) {
                Collections.sort(drugs, DrugSearchEngine.COMPARATOR_BY_TYPE.get(comparatorType));
            }
        }
    }


//    static class indexComparator implements Comparator {
//        public int compare(Object object1, Object object2) {
//            MoeDrugDto d1 = (MoeDrugDto) object1;
//            MoeDrugDto d2 = (MoeDrugDto) object2;
//            return  d1.compareTo(d2);
//        }
//    }


    public Map<String, Map<String, List<MoeDrugDto>>> getAllMultiIngrDrugsFromRedis() throws Exception {

        Set<String> keySet = new LinkedHashSet<>();
        String pattern = "DRUG:*:DRUG_TYPE:MULTIINGR:REPLACEMENT_NAME:*:CACHE_ID:*";

        String drugListType = ServerConstant.DRUG_LIST_TYPE;
        String dosageDrugListType = ServerConstant.DOSAGE_DRUG_LIST_TYPE;

        keySet = this.keys(pattern);
        if (keySet == null || keySet.size() == 0) {
            return null;
        }

        //Test multi get by StringRedisTemplate
//        Object obj = allDrugsStringRedisTemplate.opsForValue().multiGet(keySet);

        Object obj = allDrugsRedisTemplate.opsForValue().multiGet(keySet);

        //convert to Json String
        JSONArray jsonArray = JSONArray.fromObject(obj);

        Map<String, Class> classMap = new HashMap<>();
        classMap.put("aliasNames", MoeDrugAliasNameDto.class);
        classMap.put("commonDosages", MoeCommonDosageDto.class);
        classMap.put("strengths", MoeDrugStrengthDto.class);

        //For delete invalid key in KeySet
        List<Integer> deleteKeyList = new ArrayList<Integer>();

        List<MoeDrugDto> resultDrugList = new ArrayList<>();

        for (int i=0; i < jsonArray.size(); i++) {
            try {
                if (jsonArray.get(i) instanceof net.sf.json.JSONNull) {
                    deleteKeyList.add(i);
                }

                MoeDrugDto drug = (MoeDrugDto)JSONObject.toBean((JSONObject)jsonArray.get(i), MoeDrugDto.class, classMap);

                resultDrugList.add(drug);
            } catch (ClassCastException cce) {
                System.out.println("The No." + (i+1) +" drug json is failed to fetch from Redis!");
            } catch (Exception e) {
                System.out.println("The No." + (i+1) +" drug json is failed to cast to MoeDrugDto!");
            }
        }

        //Delete invalid key
        Iterator<String> it = keySet.iterator();
        int keySetIndex = 0;
        while (it.hasNext()) {
            it.next();
            if (deleteKeyList.contains(keySetIndex)) {
                it.remove();
            }
            keySetIndex++;
        }

        //Validate
        if(keySet.size() != resultDrugList.size()) {
            throw new MoeServiceException("The number of key and value is inconsistent.");
        }

        Map<String, MoeDrugDto> keyValueMap = new HashMap<>();
        int index = 0;
        for (String key : keySet) {
            keyValueMap.put(key, resultDrugList.get(index));
            index++;
        }

        if (resultDrugList != null & resultDrugList.size() > 0) {

            List<MoeDrugDto> drugList = new ArrayList<>();
            List<MoeDrugDto> dosageDrugList = new ArrayList<>();
            Map<String, List<MoeDrugDto>> drugsMap = new HashMap<>();
            Map<String, List<MoeDrugDto>> dosageDrugsMap = new HashMap<>();
            Map<String, Map<String, List<MoeDrugDto>>> total = new HashMap<>();

            //Version 2
            Iterator<Map.Entry<String, MoeDrugDto>> iterator = keyValueMap.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, MoeDrugDto> entry = iterator.next();
                String redisKey = entry.getKey();
                MoeDrugDto redisValue = entry.getValue();
                if (redisValue == null) {
                    System.out.println("Drug is null");
                }

                if (redisKey.contains("DRUG:" + drugListType + ":DRUG_TYPE:MULTIINGR:REPLACEMENT_NAME:")) {
                    drugList.add(redisValue);
                } else if (redisKey.contains("DRUG:" + dosageDrugListType + ":DRUG_TYPE:MULTIINGR:REPLACEMENT_NAME:")) {
                    dosageDrugList.add(redisValue);
                }

            } //Foreach End


            if (drugList.size() > 0) {
                //Sort after match result
                Collections.sort(drugList, new Comparator<MoeDrugDto>() {
                    @Override
                    public int compare(MoeDrugDto o1, MoeDrugDto o2) {
                        return o1.getIndexInType() - o2.getIndexInType();
                    }
                });
                drugsMap.put("MULTIINGR", drugList);
            }
            if (dosageDrugList.size() > 0) {
                //Sort after match result
                Collections.sort(dosageDrugList, new Comparator<MoeDrugDto>() {
                    @Override
                    public int compare(MoeDrugDto o1, MoeDrugDto o2) {
                        return o1.getIndexInType() - o2.getIndexInType();
                    }
                });
                dosageDrugsMap.put("MULTIINGR", dosageDrugList);
            }

            //Sort Redis Data
            sortDrugContent(drugsMap, dosageDrugsMap);

            //DRUG_BY_TYPE
            total.put(drugListType, drugsMap);
            //DOSAGE_DRUG_BY_TYPE
            total.put(dosageDrugListType, dosageDrugsMap);      System.out.println("==================== Match All MultiIngr Type Data from Redis! ====================");

            return total;
        }

        return null;
    }


    //Chris 20200110  For get Generic Name Level Items by localDrugId  -Start
    public List<MoeDrugDto> getGenericNameLevelItemsById(Set<String> localDrugIdSet, boolean isRedis) throws Exception {
        List<MoeDrugDto> genericNameTargetDrugs = null;
        Set<String> keySet = new LinkedHashSet<>();

        List<MoeDrugDto> resultList = new ArrayList<>();

        if (localDrugIdSet == null || localDrugIdSet.size() == 0) {
            return null;
        }

        if (isRedis) {
            //Redis Key
            for(String localDrugId : localDrugIdSet) {
                //"DRUG:GENERIC_NAME_LEVEL:LOCAL_DRUG_ID:%S"
                String redisKey = String.format(ServerConstant.GENERIC_NAME_LEVEL_DRUGS_REDIS_KEY, localDrugId);
                keySet.add(redisKey);
            }

            //Get data from Reids DB
            Object obj = allDrugsRedisTemplate.opsForValue().multiGet(keySet);

            //convert to Json String
            JSONArray jsonArray = JSONArray.fromObject(obj);

            Map<String, Class> classMap = new HashMap<>();
            classMap.put("aliasNames", MoeDrugAliasNameDto.class);
            classMap.put("commonDosages", MoeCommonDosageDto.class);
            classMap.put("strengths", MoeDrugStrengthDto.class);


            List<Integer> deleteKeyList = new ArrayList<Integer>();

            List<MoeDrugDto> resultDrugList = new ArrayList<>();

            for (int i=0; i < jsonArray.size(); i++) {
                try {
                    if (jsonArray.get(i) instanceof net.sf.json.JSONNull) {
                        deleteKeyList.add(i);
                    }

                    MoeDrugDto drug = (MoeDrugDto)JSONObject.toBean((JSONObject)jsonArray.get(i), MoeDrugDto.class, classMap);
//                  drug.setRedisKey(i + "");
                    resultDrugList.add(drug);
                } catch (ClassCastException cce) {
                    System.out.println("The No." + (i+1) +" drug json is failed to fetch from Redis!");
                } catch (Exception e) {
                    System.out.println("The No." + (i+1) +" drug json is failed to cast to MoeDrugDto!");
                }
            }

            //Delete invalid key
            Iterator<String> it = keySet.iterator();
            int keySetIndex = 0;
            while (it.hasNext()) {
                it.next();
                if (deleteKeyList.contains(keySetIndex)) {
                    it.remove();
                }
                keySetIndex++;
            }
            System.out.println("The Generic Name Level Items what can find : " + keySet.toString());

            //Validate
            if(keySet.size() != resultDrugList.size()) {
                throw new MoeServiceException("The number of key and value is inconsistent.");
            }

            resultList.addAll(resultDrugList);

        } else {
            //Get data from Application Cache
            genericNameTargetDrugs = DtoUtil.GENERIC_NAME_DRUGS;
            for (MoeDrugDto dto: genericNameTargetDrugs) {
                if (localDrugIdSet.contains(dto.getLocalDrugId())) {
                    resultList.add(dto);
                }
            }
        }

        if (resultList.size() > 0) {
            //Sort after match result
            Collections.sort(resultList, new Comparator<MoeDrugDto>() {
                @Override
                public int compare(MoeDrugDto o1, MoeDrugDto o2) {
                    return o1.getIndexInType() - o2.getIndexInType();
                }
            });
        }

        return resultList;
    }
    //Chris 20200110  For get Generic Name Level Items by localDrugId  -End



    @Override
    public void cacheDrugMapInRedis() throws Exception {
        Map<String, List<MoeDrugDto>> drugsByTypeMap = DtoUtil.DRUGS_BY_TYPE;
        Map<String, List<MoeDrugDto>> dosageDrugsByTypeMap = DtoUtil.DOSAGE_DRUGS_BY_TYPE;

        String notDosage = "DRUGS_BY_TYPE";
        String isDosage = "DOSAGE_DRUGS_BY_TYPE";

        String redisKeyTemplate = "DRUG:%S";

        //
//        JSONObject drugMapJsonObj = JSONObject.fromObject(drugsByTypeMap);
//        Map<String, List> o1 = (Map<String, List>)JSONObject.toBean(drugMapJsonObj,Map.class);


//        for (Map.Entry<String, List> entry : o1.entrySet())
//
////        while (entrySet.hasNext()) {
////            Map.Entry entry = iterator.next();
////            if (entry.getValue() instanceof  List) {
//////                entry.get("").put((List<MoeDrugDto>) entry.getValue());
////
////            }
////        }

//        JSONObject dosageDrugMapJsonObj = JSONObject.fromObject(dosageDrugsByTypeMap);
//        Map<String, List<MoeDrugDto>> o2 = (Map<String, List<MoeDrugDto>>)JSONObject.toBean(dosageDrugMapJsonObj, Map.class);



        ObjectMapper objectMapper = new ObjectMapper();

//        String objectString = "";
        //Obj convert to String
        String drugsByTypeMapObjStr = objectMapper.writeValueAsString(drugsByTypeMap);
        String dosageDrugsByTypeMapObjStr = objectMapper.writeValueAsString(dosageDrugsByTypeMap);

        //ALIBABA-fastJSON
//        String jsonStr = JSON.toJSONString(drugsByTypeMap);
//
//        Map map = JSON.parseObject(jsonStr, Map.class);


        //net.sf.json
        JSONObject drugMapJsonObject = JSONObject.fromObject(drugsByTypeMap);
        JSONObject dosageDrugMapJsonObject = JSONObject.fromObject(dosageDrugsByTypeMap);


        /*Map<String, List<MoeDrugDto>> drugResultmap = new HashMap<>();
        Map<String, List<MoeDrugDto>> dosageDrugResultmap = new HashMap<>();

        Iterator<Map.Entry<String, net.sf.json.JSONArray>> drugIterator = drugMapJsonObject.entrySet().iterator();
        while (drugIterator.hasNext()) {
            Map.Entry<String, net.sf.json.JSONArray> entry = drugIterator.next();
            if (entry.getValue() instanceof net.sf.json.JSONArray) {
                net.sf.json.JSONArray jsonArray = net.sf.json.JSONArray.fromObject(entry.getValue());
                List<MoeDrugDto> drugList = (List<MoeDrugDto>)net.sf.json.JSONArray.toCollection(jsonArray, MoeDrugDto.class);
                drugResultmap.put(entry.getKey(), drugList);
            }
        }

        Iterator<Map.Entry<String, net.sf.json.JSONArray>> dosageDrugIterator = dosageDrugMapJsonObject.entrySet().iterator();
        while (dosageDrugIterator.hasNext()) {
            Map.Entry<String, net.sf.json.JSONArray> entry = dosageDrugIterator.next();
            if (entry.getValue() instanceof net.sf.json.JSONArray) {
                net.sf.json.JSONArray jsonArray = net.sf.json.JSONArray.fromObject(entry.getValue());
                List<MoeDrugDto> drugList = (List<MoeDrugDto>)net.sf.json.JSONArray.toCollection(jsonArray, MoeDrugDto.class);
                dosageDrugResultmap.put(entry.getKey(), drugList);
            }
        }*/

        String drugRedisKey = String.format(redisKeyTemplate, notDosage);
        String dosageDrugRedisKey = String.format(redisKeyTemplate, isDosage);

//        drugMapRedisTemplate.opsForValue().set(drugRedisKey, drugsByTypeMap);
//        drugMapRedisTemplate.opsForValue().set(dosageDrugRedisKey, dosageDrugsByTypeMap);
        //Test JsonString
//        drugMapRedisTemplate.opsForValue().set(drugRedisKey, drugsByTypeMapObjStr);
//        drugMapRedisTemplate.opsForValue().set(dosageDrugRedisKey, dosageDrugsByTypeMapObjStr);
        //Test Serialize Byte Array
//        drugMapRedisTemplate.opsForValue().set(drugRedisKey, serialize(drugsByTypeMap));
//        drugMapRedisTemplate.opsForValue().set(dosageDrugRedisKey, serialize(dosageDrugsByTypeMap));
        //Test save into Redis by Json Object
        drugMapRedisTemplate.opsForValue().set(drugRedisKey, drugMapJsonObject.toString());
        drugMapRedisTemplate.opsForValue().set(dosageDrugRedisKey, dosageDrugMapJsonObject.toString());

    }


    @Override
    public Map<String ,Map<String, List<MoeDrugDto>>> getDrugMap() throws Exception {
        Map<String ,Map<String, List<MoeDrugDto>>> result = new HashMap<>();

        String notDosage = "DRUGS_BY_TYPE";
        String isDosage = "DOSAGE_DRUGS_BY_TYPE";
        String notDosageKey = "DRUG:DRUGS_BY_TYPE";
        String isDosageKey = "DRUG:DOSAGE_DRUGS_BY_TYPE";

        //Get Object from Redis     -version 1
//        Map<String, List<MoeDrugDto>> drugsByTypeMap = (Map<String, List<MoeDrugDto>>)drugMapRedisTemplate.opsForValue().get("DRUG:" + notDosage);
//        Map<String, List<MoeDrugDto>> dosageDrugsByTypeMap = (Map<String, List<MoeDrugDto>>)drugMapRedisTemplate.opsForValue().get("DRUG:" + isDosage);
        //Get Json String from Redis    -version 2
//        String drugsByTypeMapJsonStr = (String)drugMapRedisTemplate.opsForValue().get("DRUG:" + notDosage);
//        String dosageDrugsByTypeMapJsonStr = (String)drugMapRedisTemplate.opsForValue().get("DRUG:" + isDosage);
        //Get Byte Array from Redis     -version 3
//        Object drugBytes = drugMapRedisTemplate.opsForValue().get("DRUG:" + notDosage);
//        Map<String, List<MoeDrugDto>> drugsByTypeMap = (Map<String, List<MoeDrugDto>>)unserialize(drugBytes);
//        byte[] dosageDrugBytes = drugMapRedisTemplate.opsForValue().get("DRUG:" + isDosage).toString().getBytes();
//        Map<String, List<MoeDrugDto>> dosageDrugsByTypeMap = (Map<String, List<MoeDrugDto>>)unserialize(dosageDrugBytes);
        //Get JsonObject from Redis     -version4
        String drugMapJsonStr = (String)drugMapRedisTemplate.opsForValue().get(notDosageKey);
        JSONObject drugMapJsonObject = JSONObject.fromObject(drugMapJsonStr);
        String dosageDrugMapJsonStr = (String)drugMapRedisTemplate.opsForValue().get(isDosageKey);
        JSONObject dosageDrugMapJsonObject = JSONObject.fromObject(dosageDrugMapJsonStr);

        Map<String, List<MoeDrugDto>> drugResultmap = new HashMap<>();
        Map<String, List<MoeDrugDto>> dosageDrugResultmap = new HashMap<>();

        Iterator<Map.Entry<String, net.sf.json.JSONArray>> drugIterator = drugMapJsonObject.entrySet().iterator();
        while (drugIterator.hasNext()) {
            Map.Entry<String, net.sf.json.JSONArray> entry = drugIterator.next();
            if (entry.getValue() instanceof net.sf.json.JSONArray) {
                net.sf.json.JSONArray jsonArray = net.sf.json.JSONArray.fromObject(entry.getValue());
                List<MoeDrugDto> drugList = (List<MoeDrugDto>)net.sf.json.JSONArray.toCollection(jsonArray, MoeDrugDto.class);
                drugResultmap.put(entry.getKey(), drugList);
            }
        }

        Iterator<Map.Entry<String, net.sf.json.JSONArray>> dosageDrugIterator = dosageDrugMapJsonObject.entrySet().iterator();
        while (dosageDrugIterator.hasNext()) {
            Map.Entry<String, net.sf.json.JSONArray> entry = dosageDrugIterator.next();
            if (entry.getValue() instanceof net.sf.json.JSONArray) {
                net.sf.json.JSONArray jsonArray = net.sf.json.JSONArray.fromObject(entry.getValue());
                List<MoeDrugDto> drugList = (List<MoeDrugDto>)net.sf.json.JSONArray.toCollection(jsonArray, MoeDrugDto.class);
                dosageDrugResultmap.put(entry.getKey(), drugList);
            }
        }

        result.put(notDosageKey, drugResultmap);
        result.put(isDosageKey, dosageDrugResultmap);

//        Map<String, List<MoeDrugDto>> drugsByTypeMap = (Map<String, List<MoeDrugDto>>)drugMapRedisTemplate.opsForValue().get("DRUG:" + notDosage);
//        Map<String, List<MoeDrugDto>> dosageDrugsByTypeMap = (Map<String, List<MoeDrugDto>>)drugMapRedisTemplate.opsForValue().get("DRUG:" + isDosage);


        //Get Json String from Redis    -version 2
//        ObjectMapper objectMapper = new ObjectMapper();
//        Map<String, List<MoeDrugDto>> drugsByTypeMap = (Map<String, List<MoeDrugDto>>)objectMapper.readValue(drugsByTypeMapJsonStr, Object.class);
//        Map<String, List<MoeDrugDto>> dosageDrugsByTypeMap = (Map<String, List<MoeDrugDto>>)objectMapper.readValue(dosageDrugsByTypeMapJsonStr, Object.class);


//        drugsByTypeMap.put("VTM",drugsByTypeMap.get("TRADENAME"));
//        dosageDrugsByTypeMap.put("VTM",drugsByTypeMap.get("TRADENAME"));

//        result.put(notDosage, drugsByTypeMap);
//        result.put(isDosage, dosageDrugsByTypeMap);

        return result;
    }
    //Chris For Drug Search from Redis  -End

}
