package hk.health.moe.service.impl;

import hk.health.moe.pojo.po.MoePatientCasePo;
import hk.health.moe.pojo.po.MoePatientPo;
import hk.health.moe.repository.MoePatientCaseRepository;
import hk.health.moe.repository.MoePatientRepository;
import hk.health.moe.service.MoePatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MoePatientServiceImpl implements MoePatientService {

    @Autowired
    private MoePatientRepository patientRepository;

    @Autowired
    private MoePatientCaseRepository patientCaseRepository;

    @Override
    public MoePatientPo getPatientByRefKey(String patientRefKey) {

        MoePatientPo patient = null;
        List<MoePatientPo> patients = patientRepository.getPatientByRefKey(patientRefKey);
        if (patients != null && patients.size() != 0) {
            patient = patients.get(0);
        }
        if (null == patient) {
            patient = new MoePatientPo();
            patient.setPatientRefKey(patientRefKey);

            patient = patientRepository.save(patient);
        }

        return patient;
    }


    @Override
    public MoePatientCasePo getPatientCaseByRefKey(String caseRefNo) {

        MoePatientCasePo patientCase = null;
        List<MoePatientCasePo> patientCases = patientCaseRepository.getPatientCaseByRefKey(caseRefNo);
        if (patientCases != null && patientCases.size() != 0) {
            patientCase = patientCases.get(0);
        }
        if (null == patientCase) {
            patientCase = new MoePatientCasePo();
            patientCase.setCaseRefNo(caseRefNo);

            patientCase = patientCaseRepository.save(patientCase);
        }

        return patientCase;
    }


}
