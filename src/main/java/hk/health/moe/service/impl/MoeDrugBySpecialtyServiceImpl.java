package hk.health.moe.service.impl;

import hk.health.moe.facade.MoeDrugBySpecialtyFacade;
import hk.health.moe.facade.MoeDrugFacade;
import hk.health.moe.pojo.dto.MoeDrugBySpecialtyDto;
import hk.health.moe.pojo.dto.inner.InnerGetDrugBySpecialtyDto;
import hk.health.moe.pojo.po.MoeDrugLocalPo;
import hk.health.moe.pojo.po.MoeDrugPo;
import hk.health.moe.repository.MoeDrugLocalRepository;
import hk.health.moe.service.MoeDrugBySpecialtyService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MoeDrugBySpecialtyServiceImpl extends BaseServiceImpl implements MoeDrugBySpecialtyService {

    @Autowired
    private MoeDrugLocalRepository moeDrugLocalRepository;

    @Autowired
    private MoeDrugFacade moeDrugFacade;

    @Autowired
    private MoeDrugBySpecialtyFacade moeDrugBySpecialtyFacade;


    @Transactional(readOnly = true)
    @Override
    public List<MoeDrugBySpecialtyDto> getDrugBySpecialty(InnerGetDrugBySpecialtyDto dto) throws Exception {

        String localDrugId = dto.getLocalDrugId();
        String hkRegNo = dto.getHkRegNo();
        String hospitalCode = getUserDto().getHospitalCd();

        if (StringUtils.isBlank(localDrugId)) {
            return null;
        }

        if (!StringUtils.isBlank(hkRegNo)) {
            if (!(moeDrugLocalRepository.isLocalDrugIdHkRegNoCombinationExist(localDrugId, hkRegNo) > 0)) {
                return null;
            }
            List<MoeDrugPo> moeDrugList = moeDrugFacade.findMoeDrugByHkRegNo(hkRegNo);
            List<MoeDrugLocalPo> moeDrugLocalList = moeDrugLocalRepository.findByHkRegNo(hkRegNo);
            boolean isMoeDrugListValid = moeDrugList != null && !moeDrugList.isEmpty();
            boolean isMoeDrugLocalListValid = moeDrugLocalList != null && !moeDrugLocalList.isEmpty();
            if (!isMoeDrugLocalListValid && !isMoeDrugListValid) {
                return null;
            }
        }

        List<MoeDrugBySpecialtyDto> result = moeDrugBySpecialtyFacade.getDrugBySpecialtyByLocalDrugId(localDrugId, hospitalCode);

        return result;
    }

}
