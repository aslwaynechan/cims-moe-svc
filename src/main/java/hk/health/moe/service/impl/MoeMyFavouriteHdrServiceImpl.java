package hk.health.moe.service.impl;

import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.facade.MoeFavouriteFacade;
import hk.health.moe.pojo.dto.DrugSuggestSearchDto;
import hk.health.moe.pojo.dto.GenericDto;
import hk.health.moe.pojo.dto.MoeBaseUnitDto;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.MoeDrugStrengthDto;
import hk.health.moe.pojo.dto.MoeMedProfileDto;
import hk.health.moe.pojo.dto.MoeMyFavouriteHdrDto;
import hk.health.moe.pojo.dto.MoeSiteDto;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.pojo.po.MoeDrugStrengthPo;
import hk.health.moe.pojo.po.MoeMyFavouriteHdrPo;
import hk.health.moe.repository.MoeDrugStrengthRepository;
import hk.health.moe.repository.MoeMyFavouriteHdrRepository;
import hk.health.moe.service.MoeContentService;
import hk.health.moe.service.MoeMyFavouriteHdrService;
import hk.health.moe.util.DrugSearchEngine;
import hk.health.moe.util.DtoMapUtil;
import hk.health.moe.util.DtoUtil;
import hk.health.moe.util.Formatter;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.StringUtil;
import hk.health.moe.util.SystemSettingUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class MoeMyFavouriteHdrServiceImpl extends BaseServiceImpl implements MoeMyFavouriteHdrService {

    @Autowired
    private MoeFavouriteFacade moeFavouriteFacade;
    @Autowired
    private Comparator<MoeMyFavouriteHdrDto> favDtoComparator;
    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    @Autowired
    private MoeContentService moeContentService;

    @Autowired
    private MoeMyFavouriteHdrRepository moeMyFavouriteHdrRepository;

    @Autowired
    private MoeDrugStrengthRepository moeDrugStrengthRepository;

    @Override
    public List<GenericDto> listDrugSuggest(DrugSuggestSearchDto drugSuggestSearchDto) throws Exception {

        List<GenericDto> suggestions = null;
        // Ricci 20190805 start -- change to token -- comment
        /*UserDto userDto = (UserDto) HttpContextUtil.getSessionAttribute(ServerConstant.SESS_ATTR_USER_INFO);
        if (userDto == null) {
            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.session.error"));
        }*/
        UserDto userDto = getUserDto();
        // Ricci 20190805 end --

        String prefix = drugSuggestSearchDto.getSearchString();
        String loginId = drugSuggestSearchDto.getLoginId();
        Integer limit = drugSuggestSearchDto.getLimit();
        if (limit == null || limit == 0)
            limit = 50;
        String showFav = String.valueOf(drugSuggestSearchDto.isShowFav());
        String showDosage = String.valueOf(drugSuggestSearchDto.isShowDosage());
        String dosageType = drugSuggestSearchDto.getDosageType();
        //Chris Add get from Redis flag 20191127  -Start
        String isRedis = String.valueOf(drugSuggestSearchDto.isRedis());
        //Chris Add get from Redis flag 20191127  -End

        List<String> params = new ArrayList<>();
        params.add(showDosage);
        params.add(showFav);
        params.add(dosageType);
        //Chris Add get from Redis flag 20191127  -Start
        params.add(isRedis);
        //Chris Add get from Redis flag 20191127  -End

        suggestions = matches(prefix, params, loginId, limit, userDto);
        if (suggestions.size() == 0 && !ServerConstant.APP_MY_FAV.equals(userDto.getSystemLogin())) {
            suggestions = suggestSimilar(prefix, params);
        }
        // No free text of My Favorite Maintenance
        if (!ServerConstant.APP_MY_FAV.equals(userDto.getSystemLogin())) {
            MoeDrugDto freeTextDTO = new MoeDrugDto();
            freeTextDTO.setDidYouMean(true);
            freeTextDTO.setFreeText(true);
            freeTextDTO.setReplacementString(prefix);
            suggestions.add(freeTextDTO);
        }

        /*//Filter and Package Generic Name Level Items
        Set<Long> vmpIdSet = new HashSet<>();
        Set<Long> vmpIdGenericNameLevelSet = new HashSet<>();

        Set<Long> vtmRouteFormIdSet = new HashSet<>();
        Set<Long> vtmRouteFormIdGenericNameLevelSet = new HashSet<>();

        List<GenericDto> resultList = new ArrayList<>();

        Iterator<GenericDto> iterator = suggestions.iterator();
        while (iterator.hasNext()) {
            MoeDrugDto drugDto = (MoeDrugDto)iterator.next();
            //Parent
            if (drugDto.isParent()) {
                boolean isDuplicate = false;

                if ("Y".equalsIgnoreCase(drugDto.getStrengthCompulsory())) {
                    //Strength Compulsory = "Y"
                    if (drugDto.getStrengths() != null && drugDto.getStrengths().size() > 0) {
                        String vmp;
                        Long vmpId;

                        if (drugDto.getStrengths().get(0).getVmpId() != null && drugDto.getStrengths().get(0).getVmpId() != 0L) {
                            vmp = drugDto.getStrengths().get(0).getVmp();
                            vmpId = drugDto.getStrengths().get(0).getVmpId();
                        } else {
                            //Search VMP_ID from moeDrugStrength
                            List<MoeDrugStrengthPo> moeDrugStrengthPos = moeDrugStrengthRepository.findByHkRegNo(drugDto.getHkRegNo());
                            if (moeDrugStrengthPos == null ||
                                    moeDrugStrengthPos.size() == 0 ||
                                    moeDrugStrengthPos.get(0).getVmpId() == null ||
                                    moeDrugStrengthPos.get(0).getVmpId() == 0L) {
                                continue;
                            }
                            vmp = moeDrugStrengthPos.get(0).getVmp();
                            vmpId = moeDrugStrengthPos.get(0).getVmpId();
                        }

                        //Long vmpId = drugDto.getStrengths().get(0).getVmpId();
                        if (drugDto.getStrengths().get(0).getVmpId() == null) {
                            drugDto.getStrengths().get(0).setVmpId(vmpId);
                        }



                        isDuplicate = !vmpIdSet.add(vmpId);
                        if (isDuplicate && vmpIdGenericNameLevelSet.add(vmpId)) {
                            System.out.println("Add Generic Name Level with Strength Compulsory = Y");
                            MoeDrugDto genericNameLevelItem = new MoeDrugDto();
                            genericNameLevelItem.setDisplayNameType(drugDto.getDisplayNameType());
                            genericNameLevelItem.setShortName(vmp);  //VMP
                            genericNameLevelItem.setDisplayString(vmp);
                            genericNameLevelItem.setParent(true);
                            genericNameLevelItem.setSwapDisplayFormat(drugDto.isSwapDisplayFormat());
                            genericNameLevelItem.setHkRegNo(null);
                            genericNameLevelItem.setLocalDrugId("");    //Can't be null
                            genericNameLevelItem.setTradeName(null);
                            genericNameLevelItem.setTradeNameVtmId(null);
                            genericNameLevelItem.setVtm(drugDto.getVtm());
                            genericNameLevelItem.setVtmId(drugDto.getVtmId());
                            genericNameLevelItem.setBaseUnit(drugDto.getBaseUnit());
                            genericNameLevelItem.setPrescribeUnit(drugDto.getPrescribeUnit());
                            genericNameLevelItem.setDispenseUnit(drugDto.getDispenseUnit());
                            genericNameLevelItem.setLegalClassId(drugDto.getLegalClassId());
                            genericNameLevelItem.setLegalClass(drugDto.getLegalClass());
                            genericNameLevelItem.setManufacturer(null);
                            genericNameLevelItem.setGenericIndicator(drugDto.getGenericIndicator());
                            genericNameLevelItem.setStrengthCompulsory("Y");
                            genericNameLevelItem.setAllergyCheckFlag(drugDto.getAllergyCheckFlag());
                            //genericNameLevelItem.setAliasNames();
                            genericNameLevelItem.setForm(drugDto.getForm());
                            genericNameLevelItem.setFormEng(drugDto.getFormEng());
                            genericNameLevelItem.setDoseFormExtraInfoId(drugDto.getDoseFormExtraInfoId());
                            genericNameLevelItem.setDoseFormExtraInfo(drugDto.getDoseFormExtraInfo());
                            genericNameLevelItem.setRoute(drugDto.getRoute());
                            genericNameLevelItem.setRouteEng(drugDto.getRouteEng());
                            genericNameLevelItem.setScreenDisplay(drugDto.getVtm());    //Vtm

                            genericNameLevelItem.setPrescribeUnitId(drugDto.getPrescribeUnitId());
                            genericNameLevelItem.setBaseUnitId(drugDto.getBaseUnitId());
                            genericNameLevelItem.setDispenseUnitId(drugDto.getDispenseUnitId());

                            genericNameLevelItem.addStrength(new MoeDrugStrengthDto());
                            genericNameLevelItem.getStrengths().get(0).setStrength(drugDto.getStrengths().get(0).getStrength());
                            genericNameLevelItem.getStrengths().get(0).setStrengthLevelExtraInfo(drugDto.getStrengths().get(0).getStrengthLevelExtraInfo());
                            genericNameLevelItem.getStrengths().get(0).setAmp(null);
                            genericNameLevelItem.getStrengths().get(0).setAmpId(null);
                            genericNameLevelItem.getStrengths().get(0).setVmp(vmp);
                            genericNameLevelItem.getStrengths().get(0).setVmpId(vmpId);
                            genericNameLevelItem.getStrengths().get(0).setHkRegNo(null);
                            genericNameLevelItem.getStrengths().get(0).setRank(drugDto.getStrengths().get(0).getRank());

                            //genericNameLevelItem.setIngredientList();
                            genericNameLevelItem.setVtmRouteFormId(drugDto.getVtmRouteFormId());
                            genericNameLevelItem.setTradeVtmRouteFormId(null);
                            genericNameLevelItem.setTradeNameAlias(null);
                            genericNameLevelItem.setTerminologyName(drugDto.getTerminologyName());
                            genericNameLevelItem.setDrugCategoryId(drugDto.getDrugCategoryId());
                            genericNameLevelItem.setDangerDrug(drugDto.getDangerDrug());

                            //genericNameLevelItem.setMoeDrugOrdPropertyLocal();
                            genericNameLevelItem.setDidYouMean(drugDto.isDidYouMean());
                            genericNameLevelItem.setFreeText(false);

                            resultList.add(genericNameLevelItem);
                        }

                    }
                } else if ("N".equalsIgnoreCase(drugDto.getStrengthCompulsory())) {
                    //Strength Compulsory = "N"
                    Long vtmRouteFormId = drugDto.getVtmRouteFormId();
                    if (vtmRouteFormId != null && vtmRouteFormId != 0) {

                        isDuplicate = !vtmRouteFormIdSet.add(vtmRouteFormId);
                        if (isDuplicate && vtmRouteFormIdGenericNameLevelSet.add(vtmRouteFormId)) {
                            System.out.println("Add Generic Name Level with Strength Compulsory = N");
                            MoeDrugDto genericNameLevelItem = new MoeDrugDto();
                            genericNameLevelItem.setDisplayNameType(drugDto.getDisplayNameType());
                            genericNameLevelItem.setShortName(drugDto.getVtm() + " " + drugDto.getFormEng());
                            genericNameLevelItem.setDisplayString(drugDto.getVtm() + " " + drugDto.getFormEng());
                            genericNameLevelItem.setParent(true);
                            genericNameLevelItem.setSwapDisplayFormat(drugDto.isSwapDisplayFormat());
                            genericNameLevelItem.setHkRegNo(null);
                            genericNameLevelItem.setLocalDrugId("");    //Can't be null
                            genericNameLevelItem.setTradeName(null);
                            genericNameLevelItem.setTradeNameVtmId(null);
                            genericNameLevelItem.setVtm(drugDto.getVtm());
                            genericNameLevelItem.setVtmId(drugDto.getVtmId());
                            genericNameLevelItem.setBaseUnit(drugDto.getBaseUnit());
                            genericNameLevelItem.setPrescribeUnit(drugDto.getPrescribeUnit());
                            genericNameLevelItem.setDispenseUnit(drugDto.getDispenseUnit());
                            genericNameLevelItem.setLegalClassId(drugDto.getLegalClassId());
                            genericNameLevelItem.setLegalClass(drugDto.getLegalClass());
                            genericNameLevelItem.setManufacturer(null);
                            genericNameLevelItem.setGenericIndicator(drugDto.getGenericIndicator());
                            genericNameLevelItem.setStrengthCompulsory("N");
                            genericNameLevelItem.setAllergyCheckFlag(drugDto.getAllergyCheckFlag());
                            //genericNameLevelItem.setAliasNames();
                            genericNameLevelItem.setForm(drugDto.getForm());
                            genericNameLevelItem.setFormEng(drugDto.getFormEng());
                            genericNameLevelItem.setDoseFormExtraInfoId(drugDto.getDoseFormExtraInfoId());
                            genericNameLevelItem.setDoseFormExtraInfo(drugDto.getDoseFormExtraInfo());
                            genericNameLevelItem.setRoute(drugDto.getRoute());
                            genericNameLevelItem.setRouteEng(drugDto.getRouteEng());
                            genericNameLevelItem.setScreenDisplay(drugDto.getVtm());

                            genericNameLevelItem.setPrescribeUnitId(drugDto.getPrescribeUnitId());
                            genericNameLevelItem.setBaseUnitId(drugDto.getBaseUnitId());
                            genericNameLevelItem.setDispenseUnitId(drugDto.getDispenseUnitId());

                            genericNameLevelItem.addStrength(new MoeDrugStrengthDto());

                            //genericNameLevelItem.setIngredientList();
                            genericNameLevelItem.setVtmRouteFormId(drugDto.getVtmRouteFormId());
                            genericNameLevelItem.setTradeVtmRouteFormId(null);
                            genericNameLevelItem.setTradeNameAlias(null);
                            genericNameLevelItem.setTerminologyName(drugDto.getTerminologyName());
                            genericNameLevelItem.setDrugCategoryId(drugDto.getDrugCategoryId());
                            genericNameLevelItem.setDangerDrug(drugDto.getDangerDrug());

                            //genericNameLevelItem.setMoeDrugOrdPropertyLocal();
                            genericNameLevelItem.setDidYouMean(drugDto.isDidYouMean());
                            genericNameLevelItem.setFreeText(false);

                            resultList.add(genericNameLevelItem);
                        }

                    }
                }
            }

        }

        suggestions.addAll(0, resultList);*/

        return suggestions;

    }


    private List<GenericDto> matches(String prefix, List<String> params, String loginId, int limit, final UserDto userDto) throws Exception {
        String prefixToMatch = prefix.toLowerCase();

        boolean showDosage = false; //Default value: false
        boolean showFav = false;
        String dosageType = "C";    //Default value: "C"
        //Chris Add get from Redis flag 20191127  -Start
        boolean isRedis = false;    //Default value: false
        //Chris Add get from Redis flag 20191127  -End

        if (params != null) {
            if (params.size() > 0) {
                showDosage = Boolean.parseBoolean(params.get(0));
            }

            if (params.size() > 1) {
                showFav = Boolean.parseBoolean(params.get(1));
            }

            if (params.size() > 2 && params.get(2).length() > 0) {
                dosageType = params.get(2);
            }

            //Chris Add get from Redis flag 20191127  -Start
            if (params.size() > 3) {
                isRedis = Boolean.parseBoolean(params.get(3));
            }
            //Chris Add get from Redis flag 20191127  -End
        }

        List<GenericDto> returnList = new ArrayList<GenericDto>(0);
        // Simon 20191021 no need to show my favourite start--
        //if (showFav)
        //    returnList.addAll(listSimilarFavourite(userDto, prefixToMatch, loginId));
        //Simon 20191021 no need to show my favourite end--

        // predefined drugs
        DrugSearchEngine engine = new DrugSearchEngine(prefixToMatch, showDosage, dosageType, limit, userDto, isRedis);
        List<MoeDrugDto> drugs = engine.process();
        returnList.addAll(drugs);

        // Ricci 20190719 start --
        // set Site
        setDrugSite(returnList);
        // Ricci 20190719 end --
        return returnList;
    }

    private void matchFavourite(String prefixToMatch, List<MoeMyFavouriteHdrDto> favlist, List<MoeMyFavouriteHdrDto> favs, List<MoeMyFavouriteHdrDto> favsContains) {
        for (MoeMyFavouriteHdrDto fav : favlist) {
            String target = "";
            if (fav.getMyFavouriteName() != null) {
                target = fav.getMyFavouriteName();
            } else if (fav.getMoeMedProfiles() != null && !fav.getMoeMedProfiles().isEmpty()) {
                target = Formatter.toMedProfileDisplayString(fav.getMoeMedProfiles().get(0));
            }

            if (!addFavourite(prefixToMatch, target, fav, favs, favsContains)
                    && fav.getMyFavouriteName() != null) { // look up each profile in drug set
                for (MoeMedProfileDto profile : fav.getMoeMedProfiles()) {
                    target = Formatter.toMedProfileDisplayString(profile);

                    if (addFavourite(prefixToMatch, target, fav, favs, favsContains)) {
                        break;
                    }
                }
            }
        }
    }

    private boolean addFavourite(String prefixToMatch, String target, MoeMyFavouriteHdrDto fav, List<MoeMyFavouriteHdrDto> favs, List<MoeMyFavouriteHdrDto> favsContains) {
        boolean added = false;

        if (StringUtil.startsWithAndContainsAllPattern(prefixToMatch, target.toLowerCase())) {
            favs.add(fav);
            added = true;
        } else if (StringUtil.containsAllPattern(prefixToMatch, target.toLowerCase())) {
            favsContains.add(fav);
            added = true;
        }

        return added;
    }

    public List<GenericDto> suggestSimilar(String name, List<String> params) {
        List<MoeDrugDto> drugs = new ArrayList<MoeDrugDto>();

/*		try {
			if("Y".equals(systemSetting.getParamValue(ServerConstant.PARAM_NAME_ENABLE_DID_YOU_MEAN))) {
				Set<MoeDrugDto> dtos = new HashSet<MoeDrugDto>();

				List<String> suggestions = tradeNameSc.suggestSimilar(name, 50);
				DtoUtil.addDtoToCollection(DtoUtil.DRUGS_BY_TRADE_NAME, suggestions, dtos);

				suggestions = vtmSc.suggestSimilar(name, 50);
				DtoUtil.addDtoToCollection(DtoUtil.DRUGS_BY_VTM, suggestions, dtos);

				suggestions = banSc.suggestSimilar(name, 50);
				DtoUtil.addDtoToCollection(DtoUtil.DRUGS_BY_BAN, suggestions, dtos);

				suggestions = otherSc.suggestSimilar(name, 50);
				DtoUtil.addDtoToCollection(DtoUtil.DRUGS_BY_OTHERS, suggestions, dtos);

				suggestions = abbSc.suggestSimilar(name, 50);
				DtoUtil.addDtoToCollection(DtoUtil.DRUGS_BY_ABB, suggestions, dtos);

				drugs = new ArrayList<MoeDrugDto>(dtos);
				Collections.sort(drugs, drugDTOComparator);
			}
		} catch (IOException e) {
			LOGGER.error(e);
		}*/

        return new ArrayList<GenericDto>(drugs);
    }

    private void setCurrentExist(List<MoeMyFavouriteHdrDto> favs, String specKey) {
        for (MoeMyFavouriteHdrDto fav : favs) {
            if (fav.getMyFavouriteName() != null) {
                for (MoeMedProfileDto profile : fav.getMoeMedProfiles()) {
                    boolean isCurrentExist = DtoUtil.isCurrentExist(profile, specKey);
                    profile.getMoeEhrMedProfile().setCurrentExist(isCurrentExist);
                    if (!isCurrentExist) {
                        profile.getMoeEhrMedProfile().setNotSuspend(DtoUtil.notCurrentSuspend(profile, specKey));
                    }
                }
            } else if (fav.getMoeMedProfiles() != null && !fav.getMoeMedProfiles().isEmpty()) {
                MoeMedProfileDto profile = fav.getMoeMedProfiles().get(0);
                boolean isCurrentExist = DtoUtil.isCurrentExist(profile, specKey);
                profile.getMoeEhrMedProfile().setCurrentExist(isCurrentExist);
                if (!isCurrentExist) {
                    profile.getMoeEhrMedProfile().setNotSuspend(DtoUtil.notCurrentSuspend(profile, specKey));
                }
            }
        }
    }

    private void setCurrentMoQtyUnit(List<MoeMyFavouriteHdrDto> favs) {
        for (MoeMyFavouriteHdrDto fav : favs) {
            if (fav.getMyFavouriteName() != null) {
                for (MoeMedProfileDto profile : fav.getMoeMedProfiles()) {
                    MoeBaseUnitDto baseUnitDto = DtoUtil.getCurrentBaseUnit(profile);
                    if (baseUnitDto != null) {
                        profile.setCurrentMoQtyUnit(baseUnitDto.getBaseUnit());
                        profile.getMoeEhrMedProfile().setCurrentMoQtyUnitId(baseUnitDto.getBaseUnitId());
                    }
                }
            } else if (fav.getMoeMedProfiles() != null && !fav.getMoeMedProfiles().isEmpty()) {
                MoeMedProfileDto profile = fav.getMoeMedProfiles().get(0);
                MoeBaseUnitDto baseUnitDto = DtoUtil.getCurrentBaseUnit(profile);
                if (baseUnitDto != null) {
                    profile.setCurrentMoQtyUnit(baseUnitDto.getBaseUnit());
                    profile.getMoeEhrMedProfile().setCurrentMoQtyUnitId(baseUnitDto.getBaseUnitId());
                }
            }
        }
    }

    private void setCurrentDrugCategoryId(List<MoeMyFavouriteHdrDto> favs) {
        for (MoeMyFavouriteHdrDto fav : favs) {
            if (fav.getMyFavouriteName() != null) {
                for (MoeMedProfileDto profile : fav.getMoeMedProfiles()) {
                    String currentDrugCategoryId = DtoUtil.getCurrentDrugCategoryId(profile);

                    profile.setFormulStatus(currentDrugCategoryId);
                    profile.setCurrentFormulStatus(currentDrugCategoryId);
                }
            } else if (fav.getMoeMedProfiles() != null && !fav.getMoeMedProfiles().isEmpty()) {
                MoeMedProfileDto profile = fav.getMoeMedProfiles().get(0);
                String currentDrugCategoryId = DtoUtil.getCurrentDrugCategoryId(profile);

                profile.setFormulStatus(currentDrugCategoryId);
                profile.setCurrentFormulStatus(currentDrugCategoryId);
            }
        }
    }

    private void setDrugSite(List<GenericDto> genericDtos) {
        if (null != genericDtos && genericDtos.size() != 0) {
            Iterator iterable = genericDtos.iterator();
            while (iterable.hasNext()) {
                Object o = iterable.next();
                if (o instanceof MoeDrugDto) {
                    MoeDrugDto drugDto = (MoeDrugDto) o;
                    if (!StringUtils.isBlank(drugDto.getRouteEng()) && drugDto.getRoute() != null) {
                        List<MoeSiteDto> moeSiteDtos = DtoUtil.ROUTE_MAP.get(drugDto.getRouteEng());
                        drugDto.getRoute().setSites(moeSiteDtos);
                    }
                }
            }

        }
    }

    @Override
    public List<MoeMyFavouriteHdrDto> listMyFavourite(String userId, String searchString, boolean isDepartmental) throws Exception {
        // Ricci 20190805 start -- change to token -- comment
        /*UserDto userDto = (UserDto) HttpContextUtil.getSessionAttribute(ServerConstant.SESS_ATTR_USER_INFO);
        if (userDto == null) {
            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.session.error"));
        }*/
        UserDto userDto = getUserDto();
        // Ricci 20190805 end --
        List<MoeMyFavouriteHdrDto> favs = new ArrayList<MoeMyFavouriteHdrDto>(0);
        if (userId == null || "".equals(userId))
            userId = userDto.getLoginId();
        // Simon 20190909 for integrate  list all my favourite and by keyword start--
        if (searchString == null || "".equalsIgnoreCase(searchString)) {
            if (isDepartmental) {
                favs = moeFavouriteFacade.getDepartmentalFavourite(userDto);
            } else {
                favs = moeFavouriteFacade.getMyFavourite(userId, userDto);
            }
        } else {
            favs = this.listMyFavouriteByKeyword(userId, searchString, isDepartmental);
        }
        return favs;
        //Simon 20190909 end--
    }

    // Simon 20190812 start--comment
//    @Override
//    @Transactional
//    public List<MoeMyFavouriteHdrDto> orderMyFavourites(List<MoeMyFavouriteHdrDto> favs, boolean isDepartment) throws Exception {
//        // Ricci 20190805 start -- change to token -- comment
//        /*UserDto userDto = (UserDto) HttpContextUtil.getSessionAttribute(ServerConstant.SESS_ATTR_USER_INFO);
//        if (userDto == null) {
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.session.error"));
//        }*/
//        UserDto userDto = getUserDto();
//        // Ricci 20190805 end --
//        return moeFavouriteFacade.orderMyFavouritesDetail(favs, userDto, isDepartment);
//    }
    // Simon 20190812 end--comment


    @Override
    @Transactional
    public List<MoeMyFavouriteHdrDto> sortMyFavourites(List<MoeMyFavouriteHdrDto> favs, boolean isDepartment) throws Exception {
        // Ricci 20190805 start -- change to token -- comment
        /*UserDto userDto = (UserDto) HttpContextUtil.getSessionAttribute(ServerConstant.SESS_ATTR_USER_INFO);
        if (userDto == null) {
            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.session.error"));
        }*/
        UserDto userDto = getUserDto();
        // Ricci 20190805 end --
        return moeFavouriteFacade.sortMyFavourites(favs, userDto, isDepartment);
    }


    @Override
    public MoeMyFavouriteHdrDto deleteMyFavourite(MoeMyFavouriteHdrDto hdr) throws Exception {
        // Ricci 20190805 start -- change to token -- comment
        /*UserDto userDto = (UserDto) HttpContextUtil.getSessionAttribute(ServerConstant.SESS_ATTR_USER_INFO);
        if (userDto == null) {
//            return new ResponseBean(ServerConstant.APP_MY_FAVP_SERVICE_UNAUTHORIZED);
            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.session.error"));
        }*/
        UserDto userDto = getUserDto();
        // Ricci 20190805 end --
        String createUserId = userDto.getLoginId();
        // Ricci 20191111 start --
        MoeMyFavouriteHdrDto result = moeFavouriteFacade.deleteMyFavourite(createUserId, hdr);

        String redisKey;
        if (StringUtils.isNotBlank(hdr.getMyFavouriteId())) {
            redisKey = String.format(ServerConstant.MOE_CACHE_DEPT_FAV_MAINTENANCE_PREFIX_WITH_FAV_ID, userDto.getSpec(), hdr.getCacheId(), hdr.getMyFavouriteId());
        } else {
            redisKey = String.format(ServerConstant.MOE_CACHE_DEPT_FAV_MAINTENANCE_PREFIX, userDto.getSpec(), hdr.getCacheId());
        }
        moeContentService.deleteFav(redisKey);

        return result;
        // Ricci 20191111 end --
    }

    @Override
    public MoeMyFavouriteHdrDto deleteMyFavouriteDetail(MoeMyFavouriteHdrDto hdr) throws Exception {
        // Ricci 20190805 start -- change to token -- comment
        /*UserDto userDto = (UserDto) HttpContextUtil.getSessionAttribute(ServerConstant.SESS_ATTR_USER_INFO);
        if (userDto == null) {
            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.session.error"));
        }*/
        UserDto userDto = getUserDto();
        // Ricci 20190805 end --
        String createUserId = userDto.getLoginId();
        return moeFavouriteFacade.deleteMyFavouriteDetail(createUserId, hdr);
    }
    // Simon 20190726 start--comment
//    @Override
//    public List<MoeMyFavouriteHdrDto> deleteMyFavourite(List<MoeMyFavouriteHdrDto> hdr) throws Exception {
//        UserDto userDto = (UserDto) HttpContextUtil.getSessionAttribute(ServerConstant.SESS_ATTR_USER_INFO);
//        if (userDto == null) {
////            return new ResponseBean(ServerConstant.APP_SERVICE_UNAUTHORIZED);
//            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.session.error"));
//        }
//        String createUserId = userDto.getLoginId();
////        try {
//        return moeFavouriteFacade.deleteMyFavourite(createUserId, hdr);
////        } catch (Exception e) {
////            return new ResponseBean(ServerConstant.DB_FAIL_TO_DELETE_RECORD_ERRCODE);
////            return null;
////        }
//    }
    // Simon 20190726 end--comment


    @Override
    public MoeMyFavouriteHdrDto saveMyFavourite(MoeMyFavouriteHdrDto fav, boolean isDepartment) throws Exception {
        // Ricci 20190805 start -- change to token -- comment
        /*UserDto userDto = (UserDto) HttpContextUtil.getSessionAttribute(ServerConstant.SESS_ATTR_USER_INFO);
        if (userDto == null) {
            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.session.error"));
        }*/
        UserDto userDto = getUserDto();
        // Ricci 20190805 end --
        MoeMyFavouriteHdrDto result = moeFavouriteFacade.saveMyFavourite(fav, userDto, isDepartment);

        return result;
    }

    @Override
    public MoeMyFavouriteHdrDto updateMyFavourite(MoeMyFavouriteHdrDto fav, boolean isDepartment) throws Exception {
        // Ricci 20190805 start -- change to token -- comment
        /*UserDto userDto = (UserDto) HttpContextUtil.getSessionAttribute(ServerConstant.SESS_ATTR_USER_INFO);
        if (userDto == null) {
            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.session.error"));
        } */

        UserDto userDto = getUserDto();
        // Ricci 20190805 end --
        MoeMyFavouriteHdrDto result = moeFavouriteFacade.updateMyFavourite(fav, userDto, isDepartment);
        return result;
    }

    @Override
    public List<MoeMyFavouriteHdrDto> updateMyFavourite(List<MoeMyFavouriteHdrDto> fav, boolean isDepartment) throws Exception {
        // Ricci 20190805 start -- change to token -- comment
        /*UserDto userDto = (UserDto) HttpContextUtil.getSessionAttribute(ServerConstant.SESS_ATTR_USER_INFO);
        if (userDto == null) {
            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.session.error"));
        }*/
        UserDto userDto = getUserDto();
        // Ricci 20190805 end --
        List<MoeMyFavouriteHdrDto> resultList = new ArrayList<>();
        for (MoeMyFavouriteHdrDto dto : fav) {
            resultList.add(moeFavouriteFacade.updateMyFavourite(dto, userDto, isDepartment));
        }
        return resultList;
    }

    @Override
    public List<MoeMyFavouriteHdrDto> saveMyFavourite(List<MoeMyFavouriteHdrDto> fav, boolean isDepartment) throws Exception {
        List<MoeMyFavouriteHdrDto> hdrs = new ArrayList<>();
        for (MoeMyFavouriteHdrDto dto : fav) {
            MoeMyFavouriteHdrDto moeMyFavouriteHdrDto = saveMyFavourite(dto, isDepartment);
            hdrs.add(moeMyFavouriteHdrDto);
        }
        return hdrs;
    }

    //Simon 20190809 --start add by simon
    @Override
    public MoeMyFavouriteHdrDto sortMyFavouriteDetail(MoeMyFavouriteHdrDto favs, boolean isDepartment) throws Exception {
        UserDto userDto = getUserDto();
        return moeFavouriteFacade.orderMyFavouriteDetail(favs, userDto, isDepartment);

    }

    //Simon 20190809 --end add by simon
    // Simon 20190906 start--
    @Override
    public List<MoeMyFavouriteHdrDto> listMyFavouriteByKeyword(String userId, String keyword, boolean isDepartmental) throws Exception {
        String prefixToMatch = keyword.toLowerCase();
        UserDto userDto = getUserDto();
        return listSimilarFavourite(userDto, prefixToMatch, userId, isDepartmental);
    }

    @Override
    public MoeMyFavouriteHdrDto saveDepartFavourite(MoeMyFavouriteHdrDto saveDto) throws Exception {
        UserDto userDto = getUserDto();
        MoeMyFavouriteHdrPo result = moeFavouriteFacade.saveDepartFavourite(saveDto, userDto);
        String enableHkmttIndicator = SystemSettingUtil.getParamValue("enable_my_favourite_hkmtt_update_indicator");
        Integer updateIndicator = null;
        if (SystemSettingUtil.getParamValue("my_favourite_hkmtt_update_indicator") != null) {
            updateIndicator = Integer.parseInt(SystemSettingUtil.getParamValue("my_favourite_hkmtt_update_indicator"));
        }

        String redisKey;
        if (StringUtils.isNotBlank(saveDto.getMyFavouriteId())) {
            redisKey = String.format(ServerConstant.MOE_CACHE_DEPT_FAV_MAINTENANCE_PREFIX_WITH_FAV_ID, userDto.getSpec(), saveDto.getCacheId(), saveDto.getMyFavouriteId());
        } else {
            redisKey = String.format(ServerConstant.MOE_CACHE_DEPT_FAV_MAINTENANCE_PREFIX, userDto.getSpec(), saveDto.getCacheId());
        }
        moeContentService.deleteFav(redisKey);

        return DtoMapUtil.convertMoeMyFavouriteHdrDto(result, enableHkmttIndicator, updateIndicator, userDto);
    }

    @Override
    public void cancelDepartFavourite(String cacheId, String myFavouriteId) throws Exception {
        String redisKey;
        if (StringUtils.isNotBlank(myFavouriteId)) {
            redisKey = String.format(ServerConstant.MOE_CACHE_DEPT_FAV_MAINTENANCE_PREFIX_WITH_FAV_ID,
                    getUserDto().getSpec(), cacheId, myFavouriteId);
        } else {
            redisKey = String.format(ServerConstant.MOE_CACHE_DEPT_FAV_MAINTENANCE_PREFIX,
                    getUserDto().getSpec(), cacheId);
        }

        moeContentService.deleteFav(redisKey);
    }

    //Simon 20190906 end--

    @Transactional
    @Override
    public void deleteMyFavouriteList(List<MoeMyFavouriteHdrDto> list) throws Exception {
//        UserDto userDto = getUserDto();
//        String createUserId = userDto.getLoginId();
        List<MoeMyFavouriteHdrPo> deletedList = new ArrayList<>();
        List<String> deleteFavouriteIds = new ArrayList<>();
        for (MoeMyFavouriteHdrDto dto : list) {
            Optional<MoeMyFavouriteHdrPo> byId = moeMyFavouriteHdrRepository.findById(dto.getMyFavouriteId());
            MoeMyFavouriteHdrPo po = null;
            if (byId.isPresent()) {
                po = byId.get();
            }
            if (po == null) {
                //eric 20191219 start--
//                throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.deletedByOthers"));
                throw new MoeServiceException(ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseCode(),
                        ResponseCode.SpecialMessage.MY_FAVOURITE_DELETED_BY_OTHERS.getResponseMessage());
                //eric 20191219 end--
            }
            if (dto.getVersion().longValue() != po.getVersion().longValue()) {
                //eric 20191219 start--
//                throw new MoeServiceException(localeMessageSourceUtil.getMessage("myfavourite.UpdatedByOthers"));
                throw new MoeServiceException(ResponseCode.SpecialMessage.MY_FAVOURITE_UPDATED_BY_OTHERS.getResponseCode(),
                        ResponseCode.SpecialMessage.MY_FAVOURITE_UPDATED_BY_OTHERS.getResponseMessage());

                //eric 20191219 end--
            }
            deletedList.add(po);
            deleteFavouriteIds.add(dto.getMyFavouriteId());
        }
        for (MoeMyFavouriteHdrPo po1 : deletedList){
            moeMyFavouriteHdrRepository.delete(po1);
            List<MoeMyFavouriteHdrPo> backFavourites = moeMyFavouriteHdrRepository.getByMyFrontFavouriteId(po1.getMyFavouriteId());
            if (backFavourites != null && backFavourites.size() > 0 && backFavourites.get(0) != null) {
                MoeMyFavouriteHdrPo backFavourite = backFavourites.get(0);
                backFavourite.setFrontMyFavouriteId(po1.getFrontMyFavouriteId());
                moeMyFavouriteHdrRepository.save(backFavourite);
            }
        }


    }


    // Simon 20190906 for duplicate code start--
    private List<MoeMyFavouriteHdrDto> listSimilarFavourite(UserDto userDto, String prefixToMatch, String userId, boolean isDepartmental) throws Exception {
        List<MoeMyFavouriteHdrDto> returnList = new ArrayList<MoeMyFavouriteHdrDto>(0);
        // User favourite
        List<MoeMyFavouriteHdrDto> favs = new ArrayList<MoeMyFavouriteHdrDto>(0);
        List<MoeMyFavouriteHdrDto> favsContains = new ArrayList<MoeMyFavouriteHdrDto>(0);
        String specKey = userDto.getHospitalCd() + " " + userDto.getSpec();
        String login = userId;
        if (login == null) {
            login = userDto.getLoginId();
        }
        if (!isDepartmental) {
            if (StringUtil.stripToEmpty(login).length() > 0) {
                List<MoeMyFavouriteHdrDto> favlist = null;
                favlist = moeFavouriteFacade.getMyFavourite(login, userDto);
                matchFavourite(prefixToMatch, favlist, favs, favsContains);
                setCurrentExist(favs, specKey);
                setCurrentExist(favsContains, specKey);
                setCurrentMoQtyUnit(favs);
                setCurrentMoQtyUnit(favsContains);
                setCurrentDrugCategoryId(favs);
                setCurrentDrugCategoryId(favsContains);
            }
            Collections.sort(favs, favDtoComparator);
            Collections.sort(favsContains, favDtoComparator);
            returnList.addAll(favs);
            returnList.addAll(favsContains);
        } else {
            // departmental favourite
            favs = new ArrayList<MoeMyFavouriteHdrDto>(0);
            favsContains = new ArrayList<MoeMyFavouriteHdrDto>(0);
            if (userDto != null && ServerConstant.SETTING_ENABLED.equals(
                    SystemSettingUtil.getHospParamValue(specKey, ServerConstant.PARAM_NAME_ENABLE_DEPARTMENTAL_FAV))) {
                List<MoeMyFavouriteHdrDto> favlist = moeFavouriteFacade.getDepartmentalFavourite(userDto);
                matchFavourite(prefixToMatch, favlist, favs, favsContains);
                setCurrentExist(favs, specKey);
                setCurrentExist(favsContains, specKey);
                setCurrentMoQtyUnit(favs);
                setCurrentMoQtyUnit(favsContains);
                setCurrentDrugCategoryId(favs);
                setCurrentDrugCategoryId(favsContains);
            }
            Collections.sort(favs, favDtoComparator);
            Collections.sort(favsContains, favDtoComparator);
            returnList.addAll(favs);
            returnList.addAll(favsContains);

        }
        return returnList;
    }
    //Simon 20190906 end--

}
