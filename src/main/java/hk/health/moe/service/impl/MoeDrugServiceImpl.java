package hk.health.moe.service.impl;

import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.facade.MoeOrderFacade;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.pojo.dto.webservice.DeleteOrderDetailRequest;
import hk.health.moe.pojo.dto.webservice.OrderDetailRequest;
import hk.health.moe.pojo.dto.webservice.OrderListRequest;
import hk.health.moe.pojo.dto.webservice.OrderListResponse;
import hk.health.moe.pojo.dto.webservice.PatientOrderLogByPeriodRequest;
import hk.health.moe.pojo.dto.webservice.PrescriptionDto;
import hk.health.moe.pojo.dto.webservice.PrescriptionRequest;
import hk.health.moe.pojo.dto.webservice.PrescriptionResponse;
import hk.health.moe.service.MoeDrugService;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.WsDtoMapUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author Simon
 * @Date 2019/11/6
 */
@Service("drugService")
public class MoeDrugServiceImpl implements MoeDrugService {

    @Autowired
    private MoeOrderFacade moeOrderFacade;

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;


    @Override
    public PrescriptionDto getPatientPrescriptionByOrderNo(PrescriptionRequest prescriptionRequest) throws Exception {

        return moeOrderFacade.getMoeOrder(prescriptionRequest.getOrderNo(), prescriptionRequest.getHospCode());
    }

    @Override
    public OrderListResponse getPatientOrderList(OrderListRequest orderListRequest) throws Exception {

        return moeOrderFacade.getMoeOrder(orderListRequest.getHospCode(), orderListRequest.getPatientKey(), orderListRequest.getEpisodeNo(), orderListRequest.getEpisodeType(),
                orderListRequest.getStartSearchDate(), orderListRequest.getEndSearchDate());
    }

    @Override
    public PrescriptionResponse getPatientOrderDetail(OrderDetailRequest orderDetailRequest) throws Exception {

        return moeOrderFacade.getMoeOrder(orderDetailRequest.getHospCode(), orderDetailRequest.getPatientKey(),
                orderDetailRequest.getEpisodeNo(), orderDetailRequest.getEpisodeType(), orderDetailRequest.getLoginId());

    }


    @Override
    public PrescriptionResponse getPatientOrderLogByPeriod(PatientOrderLogByPeriodRequest patientOrderLogByPeriodRequest) throws Exception {

        PrescriptionResponse patientOrderLog = null;
        patientOrderLog = moeOrderFacade.getPatientOrderLog(patientOrderLogByPeriodRequest.getPatientKey(),
                patientOrderLogByPeriodRequest.getStartTime(), patientOrderLogByPeriodRequest.getEndTime(), patientOrderLogByPeriodRequest.getUploadMode());
        return patientOrderLog;

    }

    @Override
    public void deletePatientOrderDetail(DeleteOrderDetailRequest deleteOrderDetailRequest) throws Exception{

        PrescriptionResponse prescription = moeOrderFacade.getMoeOrder(deleteOrderDetailRequest.getUserDto().getHospCode(), deleteOrderDetailRequest.getPatientKey(),
                deleteOrderDetailRequest.getEpisodeNo(), deleteOrderDetailRequest.getEpisodeType(), null);
        UserDto userDto = WsDtoMapUtil.convertUserDto(deleteOrderDetailRequest.getUserDto());
        if (prescription == null || prescription.getPrescriptionDto() == null || prescription.getPrescriptionDto().size()==0){
            throw new MoeServiceException(localeMessageSourceUtil.getMessage("records.notExisted"));
        }
        for (PrescriptionDto dto : prescription.getPrescriptionDto()) {
            moeOrderFacade.deleteMoeOrderByPatientRefKey(deleteOrderDetailRequest.getPatientKey(), dto.getOrderNo(), userDto);
				/*				if (!ServerConstant.SUCCESS_ERRCODE.equalsIgnoreCase(responseBean.getResponseErrorCode())){
					response.setResponseCode("01");
					response.setResponseMessage("Cannot find order["+Dto.getOrderNo()+"] for patient["+Dto.getPatientKey()+"]");
					return response;
				}	*/
        }
    }

}
