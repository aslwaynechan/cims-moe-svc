package hk.health.moe.service.impl;

import hk.health.moe.common.MoeCommonHelper;
import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.exception.ParamException;
import hk.health.moe.pojo.dto.AuthenticationDto;
import hk.health.moe.pojo.dto.FieldErrorDto;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.pojo.po.MoePatientCasePo;
import hk.health.moe.pojo.po.MoePatientPo;
import hk.health.moe.service.AuthenticationService;
import hk.health.moe.service.MoeContentService;
import hk.health.moe.service.MoeEhrOrderService;
import hk.health.moe.service.MoePatientService;
import hk.health.moe.util.Formatter;
import hk.health.moe.util.HttpContextUtil;
import hk.health.moe.util.JwtUtil;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.SystemSettingUtil;
import hk.health.moe.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

@Service
public class AuthenticationServiceImpl extends BaseServiceImpl implements AuthenticationService {

    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private MoeEhrOrderService moeEhrOrderService;
    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;
    @Autowired
    private MoePatientService moePatientService;
    @Autowired
    private ValidateUtil validateUtil;
    @Autowired
    private MoeContentService moeContentService;

    @Override
    public AuthenticationDto login(AuthenticationDto dto) throws Exception {

        UserDto userDto = dto.getUser();
        // Ricci 20190925 start -- support Drug Main Login
        String systemLogin = userDto.getSystemLogin().toUpperCase();

        switch (systemLogin) {
            case ServerConstant.SYSTEM_LOGIN_FOR_MOE_SVC:
                userDto = loginByMoe(dto);
                break;

            case ServerConstant.SYSTEM_LOGIN_FOR_DRUG_MAINTENANCE_SVC:
                userDto = loginByDms(dto);
                break;

            default:
                List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();
                //eric 20191223 start--
//                FieldErrorDto fieldErrorDto = new FieldErrorDto("systemLogin", localeMessageSourceUtil.getMessage("param.Invalid"));
                FieldErrorDto fieldErrorDto = new FieldErrorDto("systemLogin",ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                        ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
                //eric 20191223 end--
                fieldErrorDtos.add(fieldErrorDto);
                //eric 20191223 start--
//                throw new ParamException(localeMessageSourceUtil.getMessage("Submission failed, please check your input!"), true, fieldErrorDtos);
                throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage(), true, fieldErrorDtos);
                //eric 20191223 end--
        }

        // Ricci 20190925 end --

        String token = jwtUtil.generateToken(userDto);
        dto.setToken(ServerConstant.JWT_ISSUER + " " + token);
        //HttpContextUtil.setSessionAttribute(ServerConstant.SESS_ATTR_USER_INFO, dto.getUser());

        return dto;
    }

    @Override
    public void logout() throws Exception {
        String systemLogin = getUserDto().getSystemLogin().toUpperCase();

        switch (systemLogin) {
            case ServerConstant.SYSTEM_LOGIN_FOR_MOE_SVC:
                String token = jwtUtil.getJwtFromRequest(HttpContextUtil.getHttpServletRequest());
                moeContentService.addTokenIntoBlackList(token);
                moeContentService.tryReleasePrescription(0);
                break;
            default:
                break;
        }
    }

    // Ricci 20190925 start --
    private UserDto loginByMoe(AuthenticationDto dto) throws Exception {

        UserDto userDto = dto.getUser();

        List<FieldErrorDto> fieldErrorDtos = validateUtil.validateLoginByMoe(dto);
        if (fieldErrorDtos.size() > 0) {
            //eric 20191223 start--
//            throw new ParamException(localeMessageSourceUtil.getMessage("param.wrongInputSubmission"), true, fieldErrorDtos);
            throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage(), true, fieldErrorDtos);
            //eric 20191223 end--
        }

        MoePatientPo moePatientPo = moePatientService.getPatientByRefKey(userDto.getMrnPatientIdentity());
        String toUserDto_MoePatientKey = moePatientPo.getMoePatientKey();
        userDto.setMoePatientKey(toUserDto_MoePatientKey);

        MoePatientCasePo moePatientCasePo = moePatientService.getPatientCaseByRefKey(userDto.getMrnPatientEncounterNum());
        String toUserDto_MoeCaseNo = moePatientCasePo.getMoeCaseNo();
        userDto.setMoeCaseNo(toUserDto_MoeCaseNo);

        Long ordNo = userDto.getOrderNum();
        LocalDate lastUpdateDate = null;
        if (ordNo != null && ordNo != 0) {
            List<Object[]> orderInfo = moeEhrOrderService.getMoeOrderNoForLogin(userDto.getHospitalCd(), toUserDto_MoePatientKey, toUserDto_MoeCaseNo, ordNo);
            if (orderInfo.size() > 0) {
                long timestamp = Timestamp.valueOf(orderInfo.get(0)[1].toString()).getTime();
                lastUpdateDate = Instant.ofEpochMilli(timestamp).atZone(ZoneOffset.ofHours(8)).toLocalDate();
            } else {
                //eric 20191223 start--
//                throw new ParamException(localeMessageSourceUtil.getMessage("records.prescription.notExisted"));
                throw new ParamException(ResponseCode.SpecialMessage.RECORDS_PRESCRIPTION_NOTEXISTED.getResponseCode(),
                        ResponseCode.SpecialMessage.RECORDS_PRESCRIPTION_NOTEXISTED.getResponseMessage());
                //eric 20191223 end--
            }
        } else {
            List<Object[]> orderInfo = moeEhrOrderService.getMoeOrderNoForLogin(userDto.getHospitalCd(), toUserDto_MoePatientKey, toUserDto_MoeCaseNo);
            if (orderInfo.size() > 0) {
                userDto.setOrderNum(Long.valueOf(orderInfo.get(0)[0].toString()));
                long timestamp = Timestamp.valueOf(orderInfo.get(0)[1].toString()).getTime();
                lastUpdateDate = Instant.ofEpochMilli(timestamp).atZone(ZoneOffset.ofHours(8)).toLocalDate();
            }
        }

        // Ricci 20190820 support action code start --
        String actionCd = "";
        if (!dto.getApptHospitalCd().equalsIgnoreCase(userDto.getHospitalCd())
                || !dto.getApptSpec().equalsIgnoreCase(userDto.getSpec())) {
            actionCd = ServerConstant.MODE_ENQUIRY;
        } else {
            if (moeContentService.checkExistPrescriptionLock(userDto)) {
                actionCd = ServerConstant.MODE_LOCK;
            } else {
                // Ricci 20191122 start -- support cache
                LocalDate apptDate = dto.getApptDate();
                LocalDate nowDate = LocalDate.now();
                long apptDiffer = nowDate.toEpochDay() - apptDate.toEpochDay();
                if (apptDiffer == 0) {    // today
                    actionCd = ServerConstant.MODE_CREATE_NEW;
                } else {    // not today
                    // clean cache start --
                    String redisKey = String.format(ServerConstant.MOE_CACHE_ORDER_PREFIX,
                            userDto.getMrnPatientIdentity(), userDto.getMrnPatientEncounterNum());
                    moeContentService.deleteOrder(redisKey);
                    // clean cache end --
                    if (userDto.getOrderNum() != null
                            && userDto.getOrderNum() != 0
                            && lastUpdateDate != null) {  // has order no
                        long updateDiffer = nowDate.toEpochDay() - lastUpdateDate.toEpochDay();
                        StringBuffer sb = new StringBuffer(userDto.getHospitalCd()).append(" ").append(userDto.getSpec());
                        String strEditableDuration = SystemSettingUtil.getHospParamValue(sb.toString(), ServerConstant.PRESCRIPTION_EDITABLE_DURATION);
                        if (!MoeCommonHelper.isNumber(strEditableDuration)) {
                            //eric 20191223 start--
//                            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.error"));
                            throw new MoeServiceException(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),
                                    ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage());
                            //eric 20191223 end--
                        }
                        long editableDuration = Long.valueOf(strEditableDuration);
                        if (updateDiffer <= editableDuration) {
                            actionCd = ServerConstant.MODE_EDIT_WITH_REMARK;
                        } else {
                            actionCd = ServerConstant.MODE_ENQUIRY;
                        }
                    } else {
                        actionCd = ServerConstant.MODE_BACKDATE;
                        if (dto.getApptDate() != null) {
                            userDto.setBackDate(dto.getApptDate().toString());
                        } else {
                            //eric 20191219 start--
//                            throw new ParamException(localeMessageSourceUtil.getMessage("param.wrongInputSubmission"));
                            throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),
                                    ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
                            //eric 20191219 end--
                        }
                    }
                }
            }
            // Ricci 20191122 end --
        }

        userDto.setActionCd(actionCd);
        // Ricci 20190820 support action code end --
        Formatter.formatUserDTO(userDto);

        return userDto;
    }

    /*This function is login for Drug Maintenance*/
    private UserDto loginByDms(AuthenticationDto dto) {
        // DO NOTHING
        return dto.getUser();
    }
    // Ricci 20190925 end --
}
