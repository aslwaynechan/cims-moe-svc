package hk.health.moe.service.impl;

/**************************************************************************
 * NAME        : CodeListServiceImpl.java
 * VERSION     : 1.0.0
 * DATE        : 03-JUL-2019
 * DESCRIPTION : Implementation of the code list service
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       03-JUL-2019  Initial Version
 * Ricci Liao       04-JUL-2019  Support to list code-list in prescript page
 **************************************************************************/

import hk.health.moe.common.ServerConstant;
import hk.health.moe.pojo.dto.MoeDoseFormExtraInfoDto;
import hk.health.moe.pojo.dto.MoeDrugAliasNameTypeDto;
import hk.health.moe.pojo.dto.MoeHospitalSettingDto;
import hk.health.moe.pojo.dto.MoeLegalClassDto;
import hk.health.moe.pojo.dto.MoeRouteDto;
import hk.health.moe.pojo.dto.MoeSiteDto;
import hk.health.moe.pojo.dto.MoeSupplFreqDto;
import hk.health.moe.pojo.po.MoeDoseFormExtraInfoPo;
import hk.health.moe.pojo.po.MoeLegalClassPo;
import hk.health.moe.pojo.po.MoeSystemSettingPo;
import hk.health.moe.repository.BaseRepository;
import hk.health.moe.repository.MoeDrugAliasNameTypeRepository;
import hk.health.moe.repository.MoeLegalClassRepository;
import hk.health.moe.service.CodeListService;
import hk.health.moe.util.DtoUtil;
import hk.health.moe.util.SystemSettingUtil;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class CodeListServiceImpl implements CodeListService {

    // Simon 20190917 start--
    @Autowired
    MoeLegalClassRepository moeLegalClassRepository;
    @Autowired
    DozerBeanMapper mapper;
    //Simon 20190917 end--

    @Autowired
    private MoeDrugAliasNameTypeRepository moeDrugAliasNameTypeRepository;

    @Autowired
    private BaseRepository baseRepository;

    @Override
    public Map<String, Object> listCodeList(Set<String> codeList) {
        Map<String, Object> codeMap = null;
        if (null != codeList && codeList.size() != 0) {
            String code = null;
            Iterator<String> codeIterator = codeList.iterator();
            codeMap = new LinkedHashMap<>();

            while (codeIterator.hasNext()) {
                code = codeIterator.next().toLowerCase();
                switch (code) {
                    case ServerConstant.CODE_LIST_TYPE_ACTION_STATUS:
                        codeMap.put(ServerConstant.CODE_LIST_TYPE_ACTION_STATUS, DtoUtil.ACTIONS_STATUS_LIST);
                        break;
                    case ServerConstant.CODE_LIST_TYPE_BASE_UNIT:
                        codeMap.put(ServerConstant.CODE_LIST_TYPE_BASE_UNIT, DtoUtil.BASE_UNIT_LIST);
                        break;
                    case ServerConstant.CODE_LIST_TYPE_DURATION_UNIT:
                        codeMap.put(ServerConstant.CODE_LIST_TYPE_DURATION_UNIT, DtoUtil.DURATION_UNIT_LIST);
                        break;
                    case ServerConstant.CODE_LIST_TYPE_FREQ_CODE:
                        codeMap.put(ServerConstant.CODE_LIST_TYPE_FREQ_CODE, DtoUtil.FREQS);
                        break;
                    case ServerConstant.CODE_LIST_TYPE_ROUTE:
                        // Ricci 20190722 start --
                        List<MoeRouteDto> result = new ArrayList<>();
                        List<MoeRouteDto> moeRouteDtos = DtoUtil.ROUTE_LIST;
                        for (MoeRouteDto routeDto : moeRouteDtos) {
                            if (!StringUtils.isBlank(routeDto.getRouteEng())) {
                                List<MoeSiteDto> siteDtos = DtoUtil.ROUTE_MAP.get(routeDto.getRouteEng());
                                if (siteDtos != null && siteDtos.size() != 0) {
                                    routeDto.setSites(siteDtos);
                                }
                                result.add(routeDto);
                            }
                        }
                        codeMap.put(ServerConstant.CODE_LIST_TYPE_ROUTE, result);
                        // Ricci 20190722 end --
                        break;
                    case ServerConstant.CODE_LIST_TYPE_SITE:
                        codeMap.put(ServerConstant.CODE_LIST_TYPE_SITE, DtoUtil.SITE_LIST);
                        break;
                    // Chris 20190805  For Special Interval  -Start
                    case ServerConstant.CODE_LIST_TYPE_REGIMEN_TYPE:
                        List<Map> supplFreqDtosList = new ArrayList<>();
                        HashMap<String, List<MoeSupplFreqDto>> regimenTypeMap = DtoUtil.REGIMEN_TYPE_MAP;
                        supplFreqDtosList.add(regimenTypeMap);
                        codeMap.put(ServerConstant.CODE_LIST_TYPE_REGIMEN_TYPE, supplFreqDtosList);
                        break;
                    // Chris 20190805  For Special Interval  -End
                    // Simon 20190917 start--
                    case ServerConstant.CODE_LIST_TYPE_LEGAL_CLASS:
                        List<MoeLegalClassPo> legalClassList = moeLegalClassRepository.findAll();
                        List<MoeLegalClassDto> legalClassDtos = new ArrayList<>();
                        for (MoeLegalClassPo po : legalClassList) {
                            legalClassDtos.add(mapper.map(po, MoeLegalClassDto.class));
                        }
                        codeMap.put(ServerConstant.CODE_LIST_TYPE_LEGAL_CLASS, legalClassDtos);
                        break;
                    case ServerConstant.CODE_LIST_TYPE_FORM:
                        codeMap.put(ServerConstant.CODE_LIST_TYPE_FORM, DtoUtil.FORM_LIST);
                        break;
                    case ServerConstant.CODE_LIST_TYPE_DOSE_FORM_EXTRA_INFO:
                        List<MoeDoseFormExtraInfoDto> dtos = new ArrayList<>();
                        for (MoeDoseFormExtraInfoPo po : DtoUtil.DOSE_FORM_EXTRA_INFO_LIST)
                            dtos.add(mapper.map(po, MoeDoseFormExtraInfoDto.class));
                        codeMap.put(ServerConstant.CODE_LIST_TYPE_DOSE_FORM_EXTRA_INFO, dtos);
                        break;
                    //Simon 20190917 end--
                    // Ricci 20191012 start --
                    case ServerConstant.CODE_LIST_TYPE_COMMON_DOSAGE_TYPE:
                        codeMap.put(ServerConstant.CODE_LIST_TYPE_COMMON_DOSAGE_TYPE, DtoUtil.COMMON_DOSAGE_TYPE_LIST);
                        break;
                    // Ricci 20191012 end --
                    // Ricci 20191014 start --
                    case ServerConstant.CODE_LIST_TYPE_ALIAS_NAME_TYPE:
                        List<MoeDrugAliasNameTypeDto> aliasNameTypeList = moeDrugAliasNameTypeRepository.findAllAliasNameType();
                        codeMap.put(ServerConstant.CODE_LIST_TYPE_ALIAS_NAME_TYPE, aliasNameTypeList);
                        break;
                    // Ricci 20191014 end --
                    // Chris 20191014 Start--
                    case ServerConstant.CODE_LIST_TYPE_DURATION_UNIT_MAP:
                        List<Map> durationUnitMapList = new ArrayList<Map>();
                        HashMap<String, Long> durationUnitMap = DtoUtil.DURATION_UNIT_MAP;
                        durationUnitMapList.add(durationUnitMap);
                        codeMap.put(ServerConstant.CODE_LIST_TYPE_DURATION_UNIT_MAP, durationUnitMapList);
                        break;
                    // Chris 20191014 End--
                    // Simon 20191016 for system setting start--
                    case ServerConstant.CODE_LIST_TYPE_SYSTEM_SETTING:
                        List<MoeSystemSettingPo> list = baseRepository.findAll(MoeSystemSettingPo.class);
                        codeMap.put(ServerConstant.CODE_LIST_TYPE_SYSTEM_SETTING,list);
                        break;
                    //Simon 20191016 for system setting end--
                    // Ricci 20191216 start --
                    case ServerConstant.CODE_LIST_TYPE_FORM_ROUTE_MAP:
                        codeMap.put(ServerConstant.CODE_LIST_TYPE_FORM_ROUTE_MAP, DtoUtil.FORM_ROUTE_MAP);
                        break;
                    // Ricci 20191216 end --
                    default:
                        break;
                }
            }

        } else {
            //TODO
        }

        return codeMap;
    }

    // Ricci 20190819 start --
    @Override
    public Map<String, MoeHospitalSettingDto> getHospitalSetting(String hospcode, String userSpecialty) {
        StringBuffer sb = new StringBuffer();
        sb.append(hospcode).append(" ").append(userSpecialty);

        return SystemSettingUtil.hospitalSettingMaps.get(sb.toString());
    }
    // Ricci 20190819 end --

}
