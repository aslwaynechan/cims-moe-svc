package hk.health.moe.service;


import hk.health.moe.pojo.dto.MoeCommonDosageDto;

import java.util.List;

/**
 * for drug maintenance
 */
public interface CommonDosageService {

    List<MoeCommonDosageDto> getCommonDosage(boolean isNewADrug, String localDrugId, String hkRegNo) throws Exception;

}
