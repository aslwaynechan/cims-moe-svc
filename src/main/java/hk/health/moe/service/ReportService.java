package hk.health.moe.service;

import hk.health.moe.pojo.bo.ReportDetailBo;
import hk.health.moe.pojo.dto.inner.InnerPrintPrescriptionDto;
import hk.health.moe.pojo.dto.inner.InnerPrintPrescriptionLogDto;

public interface ReportService {

    ReportDetailBo reportForPrescription(InnerPrintPrescriptionDto dto) throws Exception;

    // Ricci 20190813 start --
    /*byte[] generateReportByte(Map<String, Object> data, String sourceLocation) throws Exception;*/

    InnerPrintPrescriptionLogDto logPrintPrescription(InnerPrintPrescriptionLogDto dto) throws Exception;
    // Ricci 20190813 end --

}
