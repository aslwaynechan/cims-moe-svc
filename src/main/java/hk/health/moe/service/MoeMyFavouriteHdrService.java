package hk.health.moe.service;

import hk.health.moe.pojo.dto.DrugSuggestSearchDto;
import hk.health.moe.pojo.dto.GenericDto;
import hk.health.moe.pojo.dto.MoeMyFavouriteHdrDto;

import java.util.List;

public interface MoeMyFavouriteHdrService {

    List<GenericDto> listDrugSuggest(DrugSuggestSearchDto drugSuggestSearchDto) throws Exception;

    // Simon 20190909 add a param for search by keyword start--
    List<MoeMyFavouriteHdrDto> listMyFavourite(String userId, String searchString, boolean isDepartmental) throws Exception;
    //Simon 20190909 end--
    // Simon 20190812 start--comment
//    List<MoeMyFavouriteHdrDto> orderMyFavourites(List<MoeMyFavouriteHdrDto> favs, boolean isDepartment) throws Exception;
//    MoeMyFavouriteHdrDto orderMyFavouriteDetail(List<MoeMyFavouriteHdrDto> fav, boolean isDepartment) throws Exception;
    // Simon 20190812 end--comment

    List<MoeMyFavouriteHdrDto> sortMyFavourites(List<MoeMyFavouriteHdrDto> favs, boolean isDepartment) throws Exception;

    // Simon 20190726 start--comment
    // List<MoeMyFavouriteHdrDto> deleteMyFavourite(List<MoeMyFavouriteHdrDto> hdr) throws Exception;
    // Simon 20190726 end--comment

    MoeMyFavouriteHdrDto deleteMyFavourite(MoeMyFavouriteHdrDto hdr) throws Exception;

    MoeMyFavouriteHdrDto deleteMyFavouriteDetail(MoeMyFavouriteHdrDto hdr) throws Exception;


    MoeMyFavouriteHdrDto saveMyFavourite(MoeMyFavouriteHdrDto fav, boolean isDepartment) throws Exception;

    List<MoeMyFavouriteHdrDto> saveMyFavourite(List<MoeMyFavouriteHdrDto> hdr, boolean isDepartment) throws Exception;

    MoeMyFavouriteHdrDto updateMyFavourite(MoeMyFavouriteHdrDto fav, boolean isDepartment) throws Exception;

    List<MoeMyFavouriteHdrDto> updateMyFavourite(List<MoeMyFavouriteHdrDto> fav, boolean isDepartment) throws Exception;

    //Simon 20190809 --start add by simon
    MoeMyFavouriteHdrDto sortMyFavouriteDetail(MoeMyFavouriteHdrDto fav, boolean isDepartment) throws Exception;

    // Simon 20190726 end--for sort myfavourite only


    /// Simon 20190906 start--
    List<MoeMyFavouriteHdrDto> listMyFavouriteByKeyword(String userId, String keyword, boolean isDepartmental) throws Exception;
    //Simon 20190906 end--

    // Ricci 20191108 start --
    MoeMyFavouriteHdrDto saveDepartFavourite(MoeMyFavouriteHdrDto saveDto) throws Exception;
    // Ricci 20191108 end --

    // Ricci 20191113 start --
    void cancelDepartFavourite(String cacheId, String myFavouriteId) throws Exception;
    // Ricci 20191113 end --

    // Simon 20191128 start--
    void deleteMyFavouriteList(List<MoeMyFavouriteHdrDto> list) throws Exception;
    //Simon 20191128 end--
}
