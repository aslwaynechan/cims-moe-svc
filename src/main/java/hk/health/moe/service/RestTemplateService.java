package hk.health.moe.service;

/**************************************************************************
 * NAME        : RestTemplateService.java
 * VERSION     : 1.0.0
 * DATE        : 03-JUL-2019
 * DESCRIPTION : Interface for RestTemplate, to call other module
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       03-JUL-2019  Initial Version
 **************************************************************************/

import hk.health.moe.pojo.dto.SystemLogDto;

public interface RestTemplateService {

    void writeSystemLog(SystemLogDto systemLogDto) throws Exception;

    void refreshSaamDrugs() throws Exception;
}
