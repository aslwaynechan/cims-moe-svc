package hk.health.moe.service;

import hk.health.moe.pojo.po.MoeActionStatusPo;

public interface MoeActionStatusService {

    MoeActionStatusPo getActionStatus(String statusType) throws Exception;

}
