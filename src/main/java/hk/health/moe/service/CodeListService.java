package hk.health.moe.service;

/**************************************************************************
 * NAME        : CodeListService.java
 * VERSION     : 1.0.0
 * DATE        : 04-JUL-2019
 * DESCRIPTION : Interface for code list table
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       04-JUL-2019  Initial Version
 * Ricci Liao       04-JUL-2019  Support to list code-list in prescript page
 **************************************************************************/

import hk.health.moe.pojo.dto.MoeHospitalSettingDto;

import java.util.Map;
import java.util.Set;

public interface CodeListService {

    Map<String, Object> listCodeList(Set<String> codeList);

    // Ricci 20190819 start --
    Map<String, MoeHospitalSettingDto> getHospitalSetting(String hospcode, String userSpecialty);
    // Ricci 20190819 end --

}
