package hk.health.moe.service;

import hk.health.moe.pojo.dto.MoeDrugBySpecialtyDto;
import hk.health.moe.pojo.dto.inner.InnerGetDrugBySpecialtyDto;

import java.util.List;

public interface MoeDrugBySpecialtyService {

    List<MoeDrugBySpecialtyDto> getDrugBySpecialty(InnerGetDrugBySpecialtyDto dto) throws Exception;

}
