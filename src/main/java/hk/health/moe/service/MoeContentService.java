package hk.health.moe.service;

import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.MoeEhrOrderDto;
import hk.health.moe.pojo.dto.MoeMyFavouriteHdrDto;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.pojo.po.MoeMyFavouriteHdrPo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface MoeContentService {

    void setContent() throws Exception;

    void refreshDrugList() throws Exception;

    void refreshSystemSetting() throws Exception;

    void cacheMoeOrder(MoeEhrOrderDto cacheDto) throws Exception;

    MoeEhrOrderDto getMoeOrder() throws Exception;

    boolean deleteOrder(String key) throws Exception;

    boolean deleteFav(String key) throws Exception;

    String cacheDepartFavourite(MoeMyFavouriteHdrDto cacheDto) throws Exception;

    ArrayList<MoeMyFavouriteHdrPo> listMyFavourite() throws Exception;

    boolean tryReleasePrescription(int tryCount) throws Exception;

    boolean checkExistPrescriptionLock(UserDto user) throws Exception;

    void addTokenIntoBlackList(String token) throws Exception;


    //Chris  For DrugSearch from Redis  -Start
    //Version 1
    void cacheDrugSearchDataToJson() throws Exception;
    List<MoeDrugDto> matchDrugByKeyword(String keyword);
    List<MoeDrugDto> matchDrugByKeywordForMultiIngr(String keyword);

    void cacheDrugSearchDataToObject() throws Exception;

    //Version 2
    void cacheAllDrugsInRedis() throws Exception;
    Map<String, Map<String, List<MoeDrugDto>>> getAllDrugsFromRedis(String input, Boolean isSearchMultiIngr) throws Exception;

    //Version 3
    void cacheDrugMapInRedis() throws Exception;
    Map<String, Map<String, List<MoeDrugDto>>> getDrugMap() throws Exception;
    //Chris  For DrugSearch from Redis  -End

}
