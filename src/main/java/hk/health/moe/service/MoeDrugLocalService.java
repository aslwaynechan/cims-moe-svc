package hk.health.moe.service;

import hk.health.moe.pojo.dto.DrugDetailDto;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.inner.InnerDeleteDrugDto;
import hk.health.moe.pojo.dto.inner.InnerSaveDrugDto;

import java.util.List;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/8/22
 */
public interface MoeDrugLocalService {

    DrugDetailDto newLocalDrugRecord(String localDrugId, String hkRegNo, Long strengthRank) throws Exception;

    DrugDetailDto getLocalDrugRecord(String localDrugId, String hkRegNo) throws Exception;

    void saveDrug(InnerSaveDrugDto dto) throws Exception;

    //Simon 20190828 start--
    List<MoeDrugDto> listRelatedDrug(String localDrugId, String vtm, String tradeName,
                                     String routeEng, String formEng, String doseFormExtraInfo, String hospCode) throws Exception;
    //Simon 20190828 end--

    //Chris 20190829 From moe_drug_server --Start
    List<MoeDrugDto> findByAmp(String amp) throws Exception;
    //Chris 20190829 From moe_drug_server --End

    // Simon 20191014 start--
    void deleteDrugs(InnerDeleteDrugDto dto) throws Exception;
    //Simon 20191014 end--

}
