package hk.health.moe.service;

import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.MoeEhrOrderDto;
import hk.health.moe.pojo.dto.MoeMedProfileDto;
import hk.health.moe.pojo.dto.MoeMedicationDecisionDto;
import hk.health.moe.pojo.dto.MoeMedicationDecisionResultDto;
import hk.health.moe.pojo.dto.inner.InnerMaxDosOfDdDto;
import hk.health.moe.pojo.dto.inner.InnerMoeMedProfileDto;
import hk.health.moe.pojo.dto.inner.InnerSpecialIntervalDto;
import hk.health.moe.pojo.po.MoeEhrOrderPo;
import hk.health.moe.restservice.bean.saam.MoePatientSummaryDto;

import java.util.List;
import java.util.Map;

public interface MoeEhrOrderService {

    // Ricci 20190805 start -- change to token -- comment
    //MoeEhrOrderPo addMoeOrder(MoeEhrOrderDto orderDto, UserDto userDto) throws Exception;
    MoeEhrOrderPo addMoeOrder(MoeEhrOrderDto orderDto) throws Exception;
    // Ricci 20190805 end --

    MoeEhrOrderDto getMoeOrder(String hospcode, String patientKey, String caseNo, long ordNo) throws Exception;

    // Ricci 20190805 start -- change to token -- comment
    //MoeEhrOrderPo updateMoeOrder(MoeEhrOrderDto orderDto, UserDto userDto) throws Exception;
    MoeEhrOrderPo updateMoeOrder(MoeEhrOrderDto orderDto) throws Exception;
    // Ricci 20190805 end --

    // Ricci 20190805 start -- change to token -- comment
    //MoeEhrOrderPo deleteMoeOrder(MoeEhrOrderDto moeEhrOrderDto, UserDto userDto) throws Exception;
    MoeEhrOrderPo deleteMoeOrder(MoeEhrOrderDto moeEhrOrderDto) throws Exception;
    // Ricci 20190805 end --

    // Simon 20190730 --start add for drug history
    List<MoeEhrOrderDto> getMoeOrder(String withinMonths, String prescType) throws Exception;
    // Simon 20190730 --end add for drug history

    // Simon 20190731 --start add for calculate Max Dosage Of Danger Drug
    InnerMaxDosOfDdDto calculateMaxDosageOfDangerDrug(InnerMoeMedProfileDto dto) throws Exception;
    // Simon 20190731 --end add for calculate Max Dosage Of Danger Drug

    // Chris 20190806 --Start
    // Chris 20190827 Change return type --Start
    //ArrayList<String> generateSpecialIntervalText(InnerSpecialIntervalDto specialIntervalDto) throws Exception;
    Map<String, Object> generateSpecialIntervalText(InnerSpecialIntervalDto specialIntervalDto) throws Exception;
    // Chris 20190827 Change return type --End
    // Chris 20190806 --End


    // Simon 20190813 --start
    MoeMedProfileDto convertMedProfileDTO (MoeDrugDto drugDTO) throws Exception;
    // Simon 20190813 --start

    // Ricci 20190814 start --
    List<Object[]> getMoeOrderNoForLogin(String hospitalCd, String moePatientKey, String moeCaseNo) throws Exception;
    // Ricci 20190814 end --

    List<Object[]> getMoeOrderNoForLogin(String hospitalCd, String moePatientKey, String moeCaseNo, long orderNo) throws Exception;

    // Ricci 20190916 start --
    List<MoeMedicationDecisionResultDto> allergyChecking(MoeMedicationDecisionDto dto) throws Exception;
    // Ricci 20190916 end --

    // Ricci 20191015 start --
    MoePatientSummaryDto getPatientSummaryByPatientRefKey() throws Exception;
    // Ricci 20191015 end --

    // Ricci 20191016 start --
    String addPatientAlert() throws Exception;
    // Ricci 20191016 end --

    // Ricci 20191112 start --
    void cancelOrder() throws Exception;
    // Ricci 20191112 end --

}
