package hk.health.moe.service;

import hk.health.moe.pojo.dto.AuthenticationDto;

public interface AuthenticationService {

    AuthenticationDto login(AuthenticationDto dto) throws Exception;

    void logout() throws Exception;

}
