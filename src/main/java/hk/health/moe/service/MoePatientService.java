package hk.health.moe.service;

import hk.health.moe.pojo.po.MoePatientCasePo;
import hk.health.moe.pojo.po.MoePatientPo;

public interface MoePatientService {

    MoePatientPo getPatientByRefKey(String patientRefKey);

    MoePatientCasePo getPatientCaseByRefKey(String caseRefNo);

}
