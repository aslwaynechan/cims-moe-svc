package hk.health.moe.service;

import hk.health.moe.pojo.dto.webservice.DeleteOrderDetailRequest;
import hk.health.moe.pojo.dto.webservice.OrderDetailRequest;
import hk.health.moe.pojo.dto.webservice.OrderListRequest;
import hk.health.moe.pojo.dto.webservice.OrderListResponse;
import hk.health.moe.pojo.dto.webservice.PatientOrderLogByPeriodRequest;
import hk.health.moe.pojo.dto.webservice.PrescriptionDto;
import hk.health.moe.pojo.dto.webservice.PrescriptionRequest;
import hk.health.moe.pojo.dto.webservice.PrescriptionResponse;

/**
 * @Author Simon
 * @Date 2019/11/6
 */
public interface MoeDrugService {

    /**
     * PURPOSE         : Get patient prescription records by providing the unique Hospital Code and Prescription Order Number
     *
     * @param prescriptionRequest the moe request to be send through web service
     * @return PrescriptionResponse contains list of prescription records
     */
    PrescriptionDto getPatientPrescriptionByOrderNo(PrescriptionRequest prescriptionRequest) throws Exception;

    OrderListResponse getPatientOrderList(OrderListRequest orderListRequest) throws Exception;

    PrescriptionResponse getPatientOrderDetail(OrderDetailRequest orderDetailRequest) throws Exception;

    PrescriptionResponse getPatientOrderLogByPeriod(PatientOrderLogByPeriodRequest patientOrderLogByPeriodRequest) throws Exception;

    void deletePatientOrderDetail(DeleteOrderDetailRequest deleteOrderDetailRequest) throws Exception;

}
