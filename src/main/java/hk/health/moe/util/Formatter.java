/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  Formatter.java
 *
 * PURPOSE         :  Defines a set of methods that perform common, often re-used functions
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.util;


import hk.health.moe.common.MoeCommonHelper;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.common.WebServiceConstant;
import hk.health.moe.pojo.dto.DurationUnit;
import hk.health.moe.pojo.dto.MoeCommonDosageDto;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.MoeDrugStrengthDto;
import hk.health.moe.pojo.dto.MoeEhrMedProfileDto;
import hk.health.moe.pojo.dto.MoeMedMultDoseDto;
import hk.health.moe.pojo.dto.MoeMedProfileDto;
import hk.health.moe.pojo.dto.MoeSupplFreqDescDto;
import hk.health.moe.pojo.dto.MoeSupplFreqDto;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.pojo.po.MoeCommonDosagePo;
import hk.health.moe.pojo.po.MoeDrugLocalPo;
import hk.health.moe.pojo.po.MoeDrugOrdPropertyLocalPo;
import hk.health.moe.pojo.po.MoeDrugStrengthLocalPo;
import hk.health.moe.pojo.po.MoeDrugTherapeuticLocalPo;
import hk.health.moe.pojo.po.MoeDrugToSaamLocalPo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.util.HtmlUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Formatter {

    public static final List<DurationUnit> DURATION_UNIT_LIST = new ArrayList<DurationUnit>();

    static {
        DURATION_UNIT_LIST.add(new DurationUnit("d", "day(s)"));
        DURATION_UNIT_LIST.add(new DurationUnit("w", "week(s)"));
        DURATION_UNIT_LIST.add(new DurationUnit("m", "month(s)"));
        DURATION_UNIT_LIST.add(new DurationUnit("c", "cycle(s)"));
        DURATION_UNIT_LIST.add(new DurationUnit("s", "dose(s)"));
    }

    private static final String ORDER_NO_PREFIX = "00000000";

    public static String toDurationUnitDisplay(String code) {
        for (DurationUnit d : DURATION_UNIT_LIST) {
            if (d.getCode().equals(code)) return d.getName();
        }
        return "";
    }

    public static String toDurationUnitDisplay(String code, Long duration) {
        String unit = toDurationUnitDisplay(code);
        if (unit.length() > 0) {
            if (duration != null && duration < 2) {
                return unit.replace("(s)", "");
            } else {
                return unit.replace("(s)", "") + "s";
            }
        }
        return "";
    }

    public static void setDisplayAttributes(MoeDrugDto dto) {
        String nameType = dto.getDisplayNameType();
        StringBuilder searchBuf = new StringBuilder();
        StringBuilder displayBuf = new StringBuilder();
        StringBuilder commonBuf = new StringBuilder();

        if (MoeCommonHelper.isAliasNameTypeTradeNameAlias(nameType) &&
                StringUtil.stripToEmpty(dto.getTradeNameAlias()).length() > 0) {
            searchBuf.append("[" + dto.getTradeNameAlias() + "] ");
        }

        // Amp
        if (MoeCommonHelper.isAliasNameTypeLocalDrug(dto.getDisplayNameType()) ||   //"L"
                (MoeDrugDto.isMultiIngr(dto.getVtm()) &&
                        dto.getStrengths() != null && dto.getStrengths().size() > 0)) {
            String amp = dto.getStrengths().get(0).getAmp();
            searchBuf.append(amp);
            displayBuf.append(amp);
            commonBuf = new StringBuilder();
            dto.setScreenDisplay(displayBuf.toString());
            dto.setShortName(searchBuf.toString());
        } else {
            String vtm = dto.getVtm();
            // Strength for Multiple Ingredients
			/*if(dto.getStrengthCompulsory() == ServerConstant.DB_FLAG_TRUE && 
					dto.getStrengths() != null && dto.getStrengths().size() > 1) {
				vtm = appendIngredientWithStrength(vtm, dto.getStrengths());
			}*/

            if ((MoeCommonHelper.isAliasNameTypeVtm(nameType) || MoeCommonHelper.isAliasNameTypeTradeNameAlias(nameType)) &&
                    MoeCommonHelper.isDrugGenericIndYes(dto.getGenericIndicator())) { // trade name doesn't exist
                //searchBuf.append(vtm + " (" + dto.getManufacturer() + ")");
                searchBuf.append(dto.getTradeName());
            } else {
                if (StringUtil.stripToEmpty(dto.getTradeName()).length() > 0) {
                    searchBuf.append(dto.getTradeName());
                } else {
                    searchBuf.append(vtm + " (" + dto.getManufacturer() + ")");
                }
                if (MoeCommonHelper.isAliasNameTypeVtm(nameType) || MoeCommonHelper.isAliasNameTypeTradeNameAlias(nameType)) {
                    searchBuf.append(" (" + vtm + ")");
                } else {
                    searchBuf.append(" (" + dto.getAliasNames().get(0).getAliasName() + ")");
                }
            }

            if (dto.isSwapDisplayFormat()) {
                if (StringUtil.stripToEmpty(dto.getTradeName()).length() > 0) {
                    displayBuf.append(dto.getTradeName() + " (" + vtm + ")");
                } else {
                    displayBuf.append(vtm + " (" + dto.getManufacturer() + ")");

                    if (MoeCommonHelper.isAliasNameTypeAbb(dto.getDisplayNameType()) ||
                            MoeCommonHelper.isAliasNameTypeOther(dto.getDisplayNameType())) {
                        displayBuf.append(" (" + dto.getAliasNames().get(0).getAliasName() + ")");
                    }
                }
            } else {
                displayBuf.append(searchBuf.toString());
            }

            dto.setScreenDisplay(displayBuf.toString());

            // Route
            if (dto.getForm() != null && dto.getRoute() != null) {
                String key = dto.getForm().getFormEng() + " " + dto.getRoute().getRouteEng();
                if (DtoUtil.FORM_ROUTE_MAP.get(key) == null || MoeCommonHelper.isFormRouteDisplay(DtoUtil.FORM_ROUTE_MAP.get(key))) {
                    commonBuf.append(" " + dto.getRoute().getRouteEng());
                }
            }

            // Form
            if (dto.getForm() != null) {
                commonBuf.append(" " + dto.getForm().getFormEng());

                if (dto.getDoseFormExtraInfo() != null) {
                    commonBuf.append(" " + dto.getDoseFormExtraInfo());
                }
            }

            // Strength
            if (MoeCommonHelper.isFlagTrue(dto.getStrengthCompulsory()) &&
                    dto.getStrengths() != null && dto.getStrengths().size() > 0) {
                commonBuf.append(" " + dto.getStrengths().get(0).getStrength());

                if (dto.getStrengths().get(0).getStrengthLevelExtraInfo() != null) {
                    commonBuf.append(" " + dto.getStrengths().get(0).getStrengthLevelExtraInfo());
                }
            }

            dto.setShortName(displayBuf.toString() + commonBuf.toString());
        }

        // Common Dosage
        if (dto.hasCommonDosage()) {
            for (MoeCommonDosageDto dosage : dto.getCommonDosages()) {
                // Dosage
                if (StringUtil.stripToEmpty(dosage.getDosageEng()).length() > 0) {
                    commonBuf.append(" " + dosage.getDosageEng());
                }
                // Freq
                if (dosage.getFreq() != null) {
                    String freq = dosage.getFreq().getFreqCode();
                    if (dosage.getFreq1() != null) {
                        freq = freq.replaceFirst(
                                ServerConstant.DRUG_FREQ_PARAM_REGEX, dosage.getFreq1().toString());
                    }
                    commonBuf.append(" " + freq);
                }
                // Suppl Freq
                if (StringUtil.stripToEmpty(dosage.getSupplFreqEng()).length() > 0) {
                    String freq = dosage.getSupplFreqEng();
                    if (dosage.getSupplFreq1() != null) {
                        freq = freq.replaceFirst(
                                ServerConstant.DRUG_FREQ_PARAM_REGEX, dosage.getSupplFreq1().toString());
                    }
                    if (dosage.getSupplFreq2() != null) {
                        freq = freq.replaceFirst(
                                ServerConstant.DRUG_FREQ_PARAM_REGEX, dosage.getSupplFreq2().toString());
                    }
                    commonBuf.append(" " + freq);
                }
                // prn
                if (MoeCommonHelper.isDrugDosagePrnYes(dosage.getPrn())) {
                    commonBuf.append(" " + ServerConstant.DRUG_DOSAGE_PRN_STRING);
                }
                // Route
                if (dosage.getRouteEng() != null) {
                    commonBuf.append(" " + dosage.getRouteEng());
                }
            }
        }
        searchBuf.append(commonBuf.toString());
        displayBuf.append(commonBuf.toString());
        dto.setReplacementString(searchBuf.toString());
        dto.setDisplayString(displayBuf.toString());
    }

    public static String toMedProfileDisplayString(MoeMedProfileDto dto) {
        StringBuilder sb = new StringBuilder();
        MoeEhrMedProfileDto moeEhrMedProfileDto = dto.getMoeEhrMedProfile();
        String orderLineType = moeEhrMedProfileDto.getOrderLineType();
//		String durationUnit = String.valueOf(dto.getDurationUnit());
        String durationUnit = toDurationUnitDisplay(dto.getDurationUnit(), dto.getDuration());

        sb.append(dto.getMoeEhrMedProfile().getScreenDisplay());

        // Route
        if (dto.getFormDesc() != null && dto.getRouteDesc() != null) {
            String key = dto.getFormDesc() + " " + dto.getRouteDesc();
            if (DtoUtil.FORM_ROUTE_MAP.get(key) == null || MoeCommonHelper.isFormRouteDisplay(DtoUtil.FORM_ROUTE_MAP.get(key))) {
                sb.append(" " + dto.getRouteDesc());
            }
        }

        // Form
        if (dto.getFormDesc() != null) {
            sb.append(" " + dto.getFormDesc());

            if (dto.getMoeEhrMedProfile().getDoseFormExtraInfo() != null) {
                sb.append(" " + dto.getMoeEhrMedProfile().getDoseFormExtraInfo());
            }
        }

        // Strength
        if (dto.getStrength() != null) {
            sb.append(" " + dto.getStrength());

            if (dto.getMoeEhrMedProfile().getStrengthLevelExtraInfo() != null) {
                sb.append(" " + dto.getMoeEhrMedProfile().getStrengthLevelExtraInfo());
            }
        }

        // Dosage
        if (dto.getDosage() != null && StringUtil.stripToEmpty(dto.getModu()).length() > 0) {
            sb.append(" " + dto.getDosage());
            sb.append(" " + StringUtil.stripToEmpty(dto.getModu()));
        } else if (dto.getMoeMedMultDoses() != null && dto.getMoeMedMultDoses().size() > 0 && dto.getMoeMedMultDoses().get(0).getDosage() != null) {
            MoeMedMultDoseDto dosage = dto.getMoeMedMultDoses().get(0);
            sb.append(" " + dosage.getDosage());
            sb.append(" " + StringUtil.stripToEmpty(dto.getModu()));
        }

        // freq
        if (StringUtil.stripToEmpty(dto.getSupFreqText()).length() > 0) {
            sb.append(" " + dto.getSupFreqText());
        }
        if (StringUtil.stripToEmpty(dto.getFreqText()).length() > 0) {
            sb.append(" " + dto.getFreqText());
        }

        // PRN if step-up down
        if (orderLineType == null || (orderLineType != null && MoeCommonHelper.isOrderLineTypeStepUpDown(orderLineType))
                && MoeCommonHelper.isDrugDosagePrnYes(dto.getPrn())) {
            sb.append(" " + ServerConstant.DRUG_DOSAGE_PRN_STRING);
        }

        // Route
        if (orderLineType != null && MoeCommonHelper.isOrderLineTypeStepUpDown(orderLineType)
                && StringUtil.stripToEmpty(dto.getRouteDesc()).length() > 0) {
            if (dto.getSiteDesc() != null) {
                sb.append(" " + dto.getSiteDesc());
            } else {
                sb.append(" " + dto.getRouteDesc());
            }
        }

        if (orderLineType != null && MoeCommonHelper.isOrderLineTypeStepUpDown(orderLineType)
                && dto.getDuration() != null && dto.getDuration() > 0 && StringUtil.stripToEmpty(durationUnit).length() > 0) {
            sb.append(" " + dto.getDuration());
            sb.append(" " + StringUtil.stripToEmpty(durationUnit));
        }


        // Loop Common Dosage
        for (MoeMedMultDoseDto dosage : dto.getMoeMedMultDoses()) {

            if (dosage.getDosage() == null &&
                    StringUtil.stripToEmpty(dosage.getFreqText()).length() == 0) {
                continue;
            }

            StringBuilder buffer = new StringBuilder();

            // Dosage
            if (dosage.getDosage() != null && StringUtil.stripToEmpty(dto.getModu()).length() > 0) {
                buffer.append(" " + dosage.getDosage());
                buffer.append(" " + StringUtil.stripToEmpty(dto.getModu()));
            }

            // freq
            //buffer.append(Formatter.getMultiDoseFreqText(dto, dosage));

//			if(trim(dosage.getFreqText()).length() > 0) {
//				buffer.append(" " + dosage.getFreqText());
//	 		}

            // Suppl Freq
//			if(trim(dosage.getSupFreqText()).length() > 0) {
//				buffer.append(dosage.getSupFreqText());
//			}

            if (orderLineType != null && MoeCommonHelper.isOrderLineTypeStepUpDown(orderLineType)
                    && (StringUtil.stripToEmpty(dto.getRouteDesc()).length() > 0 || StringUtil.stripToEmpty(dto.getSiteDesc()).length() > 0)) {
                if (StringUtil.stripToEmpty(dto.getSiteDesc()).length() > 0) {
                    buffer.append(" " + StringUtil.stripToEmpty(dto.getSiteDesc()));
                } else {
                    buffer.append(" " + StringUtil.stripToEmpty(dto.getRouteDesc()));
                }
            }
            String dosageDurationUnit = toDurationUnitDisplay(dosage.getDurationUnit(), dosage.getDuration());
            ;
            if (dosage.getDuration() != null && dosage.getDuration() > 0 && StringUtil.stripToEmpty(dosageDurationUnit).length() > 0) {
                buffer.append(" " + dosage.getDuration());
                buffer.append(" " + StringUtil.stripToEmpty(dosageDurationUnit));
            }

            sb.append(buffer.toString());
        }

        // PRN if not step-up down
        if (orderLineType == null || (orderLineType != null && !MoeCommonHelper.isOrderLineTypeStepUpDown(orderLineType)) &&
                MoeCommonHelper.isDrugDosagePrnYes(dto.getPrn())) {
            sb.append(" " + ServerConstant.DRUG_DOSAGE_PRN_STRING);
        }

        // Route if not step-up down
        if (orderLineType == null || (orderLineType != null && !MoeCommonHelper.isOrderLineTypeStepUpDown(orderLineType))
                && StringUtil.stripToEmpty(dto.getRouteDesc()).length() > 0) {
            if (StringUtil.stripToEmpty(dto.getSiteDesc()).length() > 0) {
                sb.append(" " + StringUtil.stripToEmpty(dto.getSiteDesc()));
            } else {
                sb.append(" " + StringUtil.stripToEmpty(dto.getRouteDesc()));
            }
        }

        // Duration if not step-up down
        if (orderLineType == null || (orderLineType != null && !MoeCommonHelper.isOrderLineTypeStepUpDown(orderLineType))
                && dto.getDuration() != null && dto.getDuration() > 0 && StringUtil.stripToEmpty(durationUnit).length() > 0) {
            sb.append(" " + dto.getDuration());
            sb.append(" " + StringUtil.stripToEmpty(durationUnit));
            // start date
            if (!MoeCommonHelper.isOrderLineTypeNormal(orderLineType) && dto.getStartDate() != null) {
                sb.append(DateUtil.formatDate(dto.getStartDate()));
            }
        }

        // Qty
        if (dto.getMoQty() != null && dto.getMoQty() > 0) {
            sb.append(" " + dto.getMoQty() + " " + StringUtil.strip(dto.getMoQtyUnit()));
        }

        // special instruction
        if (StringUtil.stripToEmpty(dto.getSpecInstruct()).length() > 0) {
            sb.append(" " + StringUtil.stripToEmpty(dto.getSpecInstruct()));
        }

        return sb.toString();
    }

    private static String appendIngredientWithStrength(String vtm, List<MoeDrugStrengthDto> strs) {
        String[] ins = vtm.split("\\+");
        StringBuilder vtmBuf = new StringBuilder();
        for (int i = 0; i < strs.size(); i++) {
            if (i > 0) {
                vtmBuf.append(" +");
            }
            vtmBuf.append(ins[i]);
            vtmBuf.append(" ");
            vtmBuf.append(strs.get(i).getStrength());
        }
        return vtmBuf.toString();
    }

    public static String getOrdNo(long ordNo) {
        String ordNoPrefix = StringUtil.stripToEmpty(SystemSettingUtil.getParamValue(ServerConstant.PARAM_NAME_ORDER_NUMBER_PREFIX));

        String ordNoStr = String.valueOf(ordNo);
        if (ordNoStr.length() < ORDER_NO_PREFIX.length()) {
            ordNoStr = ORDER_NO_PREFIX.substring(0, ORDER_NO_PREFIX.length() - ordNoStr.length()) + ordNoStr;
        }

        return ordNoPrefix + ordNoStr;
    }

    // RefNo XXXX-YY
    public static String getRefNo(long ordNo, long version) {
        String ordNoStr = getOrdNo(ordNo);
        String refNo = ordNoStr.substring(ordNoStr.length() - 4);
        if (version > 0) {
            refNo += "-" + version;
        }
        return refNo;
    }

    public static void formatUserDTO(UserDto dto) {
        if (dto == null) return;

        if ("".equals(dto.getWard())) {
            dto.setWard(null);
        }
        if ("".equals(dto.getBedNum())) {
            dto.setBedNum(null);
        }
    }

    public static String getDrugKey(MoeDrugDto dto) {
        if (dto == null) return "";

        String drugKey = dto.getHkRegNo();
        if (MoeCommonHelper.isFlagTrue(dto.getStrengthCompulsory())) {
            drugKey += " " + dto.getStrengths().get(0).getStrength() + " " + dto.getStrengths().get(0).getStrengthLevelExtraInfo();
        }
        return drugKey;
    }

    public static String getModu(String input) {
        String modu = input;
        if (modu == null) return "";

        if ("5mL spoonful".equalsIgnoreCase(modu)) {
            modu = "x " + modu;
        }
        return modu;
    }

    public static int countOccurrences(String haystack, String needle) {
        int count = 0;
        for (int i = 0; i < haystack.length(); i++) {
            if (haystack.charAt(i) == needle.charAt(0)) {
                count++;
            }
        }
        return count;
    }

    //chris 20190802 For MoeMedProfile.SupFreqText (copy from MOE1:EhrMoeModule GWT code)  -Start
    //Chris 20190827  Change return type for support to return displayWithFreq  -Start
    public static Map<String, Object> toRecurText(MoeMedProfileDto moeMedProfileDto) {
        //public static ArrayList<String> toRecurText(MoeMedProfileDto moeMedProfileDto) {
        //Chris 20190827  Change return type for support to return displayWithFreq  -Start

        final MoeStringBuffer sb = new MoeStringBuffer();

        final String suppFreqCode = moeMedProfileDto.getSupFreqCode();
        final String regiment = moeMedProfileDto.getRegimen();
        //Chris 20190906 For fix the other case: freq code is not "daily"  -Start
        final String freq = ServerConstant.SPECIAL_INTERVAL_FREQ_CODE_DAILY.equalsIgnoreCase(moeMedProfileDto.getFreqCode()) ? ServerConstant.SPECIAL_INTERVAL_FREQ_CODE_DAILY : null;
        //Chris 20190906 For fix the other case: freq code is not "daily"  -End
        final Long suppFreq1 = moeMedProfileDto.getSupFreq1();
        final Long suppFreq2 = moeMedProfileDto.getSupFreq2();

        //HashMap<Character, List<MoeSupplFreqDto>> map = EhrMoeRegistry.get().getMetaDataDTO().getRegimenTypeMap();
        HashMap<String, List<MoeSupplFreqDto>> map = DtoUtil.REGIMEN_TYPE_MAP;
        if (map.get(regiment) == null) return null;

        List<MoeSupplFreqDto> suppFreqDTOList = map.get(regiment);
        MoeSupplFreqDto moeSupplFreqDto = null;
        for (MoeSupplFreqDto dto : suppFreqDTOList) {
            if (dto != null && suppFreqCode.equals(dto.getSupplFreqEng())) {
                moeSupplFreqDto = dto;
                break;
            }
        }
        if (moeSupplFreqDto == null || moeSupplFreqDto.getMoeSupplFreqDescs() == null) {
            return null;
        }

        List<MoeSupplFreqDescDto> moeSupplFreqDescDTOList = moeSupplFreqDto.getMoeSupplFreqDescs();
        ArrayList<MoeSupplFreqDescDto> targetMoeSupplFreqDescDTOList = new ArrayList<MoeSupplFreqDescDto>();
        ArrayList<MoeSupplFreqDescDto> defaultSameFreqMoeSupplFreqDescDTOList = new ArrayList<MoeSupplFreqDescDto>();
        ArrayList<MoeSupplFreqDescDto> defaultMoeSupplFreqDescDTOList = new ArrayList<MoeSupplFreqDescDto>();

        if (moeSupplFreqDto.getMoeSupplFreqSelections().size() == 1) {
            for (MoeSupplFreqDescDto dto : moeSupplFreqDescDTOList) {
                if (trim(freq).equals(trim(dto.getFreqCode()))) {
                    if (nullToZero(suppFreq1).equals(dto.getSupplFreq1())) {
                        targetMoeSupplFreqDescDTOList.add(dto);
                    } else if (dto.getSupplFreq1() == null) {
                        defaultSameFreqMoeSupplFreqDescDTOList.add(dto);
                    }
                } else if (dto.getFreqCode() == null && dto.getSupplFreq1() == null) {
                    defaultMoeSupplFreqDescDTOList.add(dto);
                }
            }
        }

        // monday case...
        if (moeSupplFreqDto.getMoeSupplFreqSelections().size() > 1
                && trim(moeMedProfileDto.getDayOfWeek()).length() == 7) {
            targetMoeSupplFreqDescDTOList.add(convertMoeSupplFreqDescDTOByDayWeekText(moeMedProfileDto, moeSupplFreqDto));
        }

        // default case
        if (!targetMoeSupplFreqDescDTOList.isEmpty()) {
            return Formatter.toRecurText(
                    sb.toString(),
                    moeSupplFreqDto,
                    targetMoeSupplFreqDescDTOList,
                    suppFreq1 == null ? null : suppFreq1.intValue(),
                    suppFreq2 == null ? null : suppFreq2.intValue(),
                    moeMedProfileDto.getMoeEhrMedProfile().getCycleMultiplier() != null ? moeMedProfileDto.getMoeEhrMedProfile().getCycleMultiplier().intValue() : null
            );
        }
        if (!defaultSameFreqMoeSupplFreqDescDTOList.isEmpty()) {
            targetMoeSupplFreqDescDTOList = defaultSameFreqMoeSupplFreqDescDTOList;
        } else if (!defaultMoeSupplFreqDescDTOList.isEmpty()) {
            targetMoeSupplFreqDescDTOList = defaultMoeSupplFreqDescDTOList;
        }

        return Formatter.toRecurText(
                sb.toString(),
                moeSupplFreqDto,
                targetMoeSupplFreqDescDTOList,
                suppFreq1 == null ? null : suppFreq1.intValue(),
                suppFreq2 == null ? null : suppFreq2.intValue(),
                moeMedProfileDto.getMoeEhrMedProfile().getCycleMultiplier() != null ? moeMedProfileDto.getMoeEhrMedProfile().getCycleMultiplier().intValue() : null
        );
    }

    public static String trim(String s) {
        if (s == null) return "";
        return s.trim();
    }

    //chris 20190802 Changed parameter Integer to Long  -Start
    public static Long nullToZero(Long i) {
        if (i == null) return 0L;
        return i;
    }
    //chris 20190802 Changed parameter Integer to Long  -End

    public static class MoeStringBuffer {

        String str = "";

        public MoeStringBuffer() {
        }

        public MoeStringBuffer(String str) {
            this.str = str;
        }

        public void append(String appendString) {
            str += appendString;
        }

        public void append(int appendInt) {
            str += appendInt;
        }

        public String toString() {
            return str;
        }

        public int length() {
            return str.length();
        }
    }

    private static MoeSupplFreqDescDto convertMoeSupplFreqDescDTOByDayWeekText(MoeMedProfileDto moeMedProfilesDTO, MoeSupplFreqDto moeSupplFreqDto) {
        final MoeSupplFreqDescDto result = new MoeSupplFreqDescDto();
        final ArrayList<MoeSupplFreqDescDto> dayWeekList = new ArrayList<MoeSupplFreqDescDto>();
        final String dayOfWeek = trim(moeMedProfilesDTO.getDayOfWeek());
        Map<String, MoeSupplFreqDescDto> descMap = new HashMap<String, MoeSupplFreqDescDto>();
        if (dayOfWeek.length() != 7) {
            return result;
        }

        for (MoeSupplFreqDescDto dto : moeSupplFreqDto.getMoeSupplFreqDescs()) {
            final String key = trim(dto.getFreqCode()) + "|" + nullToZero(dto.getSupplFreq1()) + "|" + nullToZero(dto.getSupplFreqSelectionValue());
            descMap.put(key, dto);
        }

        for (int i = 0; i < dayOfWeek.length(); i++) {
            if (dayOfWeek.charAt(i) == '1') {
                String targetKey = trim(moeMedProfilesDTO.getFreqCode()) + "|" + nullToZero(moeMedProfilesDTO.getSupFreq1()) + "|" + nullToZero(i + 1L);
                if (!descMap.containsKey(targetKey)) {
                    targetKey = trim(null) + "|" + nullToZero(null) + "|" + nullToZero(i + 1L);
                }

                if (descMap.get(targetKey) != null) {
                    dayWeekList.add(descMap.get(targetKey));
                }
            }
        }

        // create a new SupplFreqDescDTO for the selected day of week value
        if (!dayWeekList.isEmpty()) {
            result.setFreqCode(dayWeekList.get(0).getFreqCode());
            result.setSupplFreq1(dayWeekList.get(0).getSupplFreq1());
            result.setDisplayWithFreq(dayWeekList.get(0).getDisplayWithFreq());
            String desc = "";
            for (MoeSupplFreqDescDto dto : dayWeekList) {
                desc += ", " + dto.getSupplFreqDescEng();
            }
            result.setSupplFreqDescEng(desc.replaceFirst(", ", ""));
        }

        return result;
    }

    //Chris 20190827  Change return type for support to return displayWithFreq  -Start
    private static Map<String, Object> toRecurText(String freqText, MoeSupplFreqDto moeSupplFreqDto, ArrayList<MoeSupplFreqDescDto> moeSupplFreqDescDTOList, Integer suppFreq1, Integer suppFreq2, Integer cycleMultiplier) {
        //private static ArrayList<String> toRecurText(String freqText, MoeSupplFreqDto moeSupplFreqDto, ArrayList<MoeSupplFreqDescDto> moeSupplFreqDescDTOList, Integer suppFreq1, Integer suppFreq2, Integer cycleMultiplier) {
        //Chris 20190827  Change return type for support to return displayWithFreq  -End

        ArrayList<String> specialIntervalTextResultList = new ArrayList<String>();

        //Support to return Flag of "diaplay with freq"
        Map<String, Object> resultMap = new HashMap<>();

        if (moeSupplFreqDescDTOList == null || moeSupplFreqDto == null) return resultMap;

        for (MoeSupplFreqDescDto targetMoeSupplFreqDescDTO : moeSupplFreqDescDTOList) {

            MoeStringBuffer sb = null;

            //replace __ to value
            String suppFreqText = targetMoeSupplFreqDescDTO.getSupplFreqDescEng();
            //if (targetMoeSupplFreqDescDTO.getSupplFreqDescEng().indexOf(ModuleConstant.BLANKS_IND) > 0) {
            //Chris 20190905 Correct code  -Start
            if (targetMoeSupplFreqDescDTO.getSupplFreqDescEng().indexOf(WebServiceConstant.BLANKS_IND) != -1) {
                //Chris 20190905 Correct code  -End
                suppFreqText = targetMoeSupplFreqDescDTO.getSupplFreqDescEng();
                //suppFreqText = (suppFreq1 == null) ? suppFreqText : suppFreqText.replaceFirst(ModuleConstant.BLANKS_IND, " " + Integer.toString(suppFreq1)+ " ");
                //suppFreqText = (suppFreq2 == null) ? suppFreqText : suppFreqText.replaceFirst(ModuleConstant.BLANKS_IND, " " + Integer.toString(suppFreq2)+ " ");
                //suppFreqText = (cycleMultiplier == null) ? suppFreqText : suppFreqText.replaceFirst(ModuleConstant.BLANKS_IND, " " + Integer.toString(cycleMultiplier)+ " ");
                suppFreqText = (suppFreq1 == null) ? suppFreqText : suppFreqText.replaceFirst(WebServiceConstant.BLANKS_IND, " " + Integer.toString(suppFreq1) + " ");
                suppFreqText = (suppFreq2 == null) ? suppFreqText : suppFreqText.replaceFirst(WebServiceConstant.BLANKS_IND, " " + Integer.toString(suppFreq2) + " ");
                suppFreqText = (cycleMultiplier == null) ? suppFreqText : suppFreqText.replaceFirst(WebServiceConstant.BLANKS_IND, "" + Integer.toString(cycleMultiplier) + "");
            }

            sb = new MoeStringBuffer();
            sb.append(trim(moeSupplFreqDto.getPrefixSupplFreqEng()).length() > 0 ? trim(moeSupplFreqDto.getPrefixSupplFreqEng()) + " " : "");
            sb.append(trim(suppFreqText));

            specialIntervalTextResultList.add(sb.toString());

            //Display With Freq
            resultMap.put("displayWithFreq", targetMoeSupplFreqDescDTO.getDisplayWithFreq());

            //Chris 20190905 Return Type about Selections hasUnderLine  -Start
            if (moeSupplFreqDto.getMoeSupplFreqSelections() != null &&
                    moeSupplFreqDto.getMoeSupplFreqSelections().size() > 0 &&
                    moeSupplFreqDto.getMoeSupplFreqSelections().get(0).getSupplFreqDisplay() != null) {
                String supplFreqSelectionsDisplay = moeSupplFreqDto.getMoeSupplFreqSelections().get(0).getSupplFreqDisplay();
                Boolean hasUnderlineResult = supplFreqSelectionsDisplay.contains(WebServiceConstant.BLANKS_IND);

                String hasUnderline = hasUnderlineResult ? "Y" : "N";
                resultMap.put("hasUnderline", hasUnderline);

                //Chris 20190905 Return Type about Input Box Special Interval Text  -Start
                List<String> inputBoxResult = new ArrayList<String>();
                if (moeSupplFreqDto.getSupplFreqEng().equals(ServerConstant.SPECIAL_INTERVAL_ODD_EVEN_DAYS)) {
                    //Case of "Odd / Even Days"
                    List<String> oddEvenDaysList = new ArrayList<String>();
                    oddEvenDaysList.add(ServerConstant.INPUT_BOX_SPECIAL_INTERVAL_ON_ODD_DAYS);
                    oddEvenDaysList.add(ServerConstant.INPUT_BOX_SPECIAL_INTERVAL_ON_EVEN_DAYS);
                    inputBoxResult = oddEvenDaysList;
                } else if (moeSupplFreqDto.getSupplFreqEng().equals(ServerConstant.SPECIAL_INTERVAL_DAY_OF_WEEK)) {
                    //Case of "Day of Week"
                    inputBoxResult = specialIntervalTextResultList;
                } else {
                    //Case of others
                    String inputBoxSpecialIntervalText = supplFreqSelectionsDisplay;
                    if (inputBoxSpecialIntervalText.indexOf(WebServiceConstant.BLANKS_IND) != -1) {
                        inputBoxSpecialIntervalText = (suppFreq1 == null) ? inputBoxSpecialIntervalText : inputBoxSpecialIntervalText.replaceFirst(WebServiceConstant.BLANKS_IND, " " + suppFreq1 + " ");
                        inputBoxSpecialIntervalText = (suppFreq2 == null) ? inputBoxSpecialIntervalText : inputBoxSpecialIntervalText.replaceFirst(WebServiceConstant.BLANKS_IND, " " + suppFreq2 + " ");
                    }
                    inputBoxResult.add(inputBoxSpecialIntervalText.trim());
                }
                resultMap.put("inputBoxSpecialIntervalText", inputBoxResult);
                //Chris 20190905 Return Type about Input Box Special Interval Text  -End

            }
            //Chris 20190905 Return Type about Selections hasUnderLine  -End

        }

        resultMap.put("specialIntervalText", specialIntervalTextResultList);

        return resultMap;
    }
    //chris 20190802 For MoeMedProfile.SupFreqText (copy from MOE1:EhrMoeModule GWT code)  -End


    // Simon 20190813 start--
    public static boolean isMultipleIngredient(String vtm) {
        return MoeDrugDto.isMultiIngr(vtm);
    }
    // Simon 20190813 end--

    // Simon 20190822 for moe_drug_server start--
    public static void setDrugDisplayString(MoeDrugDto dto) {
        String vtmDisplay = "";

        if (!dto.isFreeText()) {
            vtmDisplay = getVtmString(dto.getVtm(), dto.getIngredientList(), dto.getStrengths(), (dto.getStrengthCompulsory() != null && ServerConstant.DB_FLAG_TRUE.equals(dto.getStrengthCompulsory())), dto.getScreenDisplay());
        }
        StringBuffer sb = new StringBuffer();
        if (dto.isFreeText()) {
            //TODO not sure if HtmlUtils can replace SafeHtmlUtils --Simon
            //sb.append(" " + SafeHtmlUtils.htmlEscape(dto.getReplacementString()) + " (Free Text)");
            sb.append(" " + HtmlUtils.htmlEscape(dto.getReplacementString()) + " (Free Text)");
        } else if (dto.isParent()) {
            String nameType = dto.getDisplayNameType();
            if (dto.isSwapDisplayFormat() && !dto.isDidYouMean()) {
                if (trim(dto.getTradeName()).length() > 0) {
                    sb.append(dto.getTradeName() + " (" + vtmDisplay + ")");
                } else {
                    sb.append(vtmDisplay + " (" + dto.getManufacturer() + ")");

                    if (dto.getDisplayNameType().equals(ServerConstant.ALIAS_NAME_TYPE_ABB) ||
                            dto.getDisplayNameType().equalsIgnoreCase(ServerConstant.ALIAS_NAME_TYPE_OTHER)) {
                        sb.append(" (" + dto.getAliasNames().get(0).getAliasName() + ")");
                    }
                }
            } else if (Formatter.isMultipleIngredient(dto.getVtm()) || dto.getDisplayNameType().equals(ServerConstant.ALIAS_NAME_TYPE_LOCAL_DRUG)) {
                sb.append(dto.getStrengths().get(0).getAmp());
            } else if (ServerConstant.ALIAS_NAME_TYPE_VTM.equals(nameType) &&
                    dto.getGenericIndicator().equals(ServerConstant.DRUG_GENERIC_IND_YES)) { // trade name doesn't exist
                sb.append(dto.getTradeName());
            } else {
                if (trim(dto.getTradeName()).length() > 0) {
                    sb.append(dto.getTradeName() + " ");
                } else {
                    sb.append(vtmDisplay + " (" + dto.getManufacturer() + ") ");
                }
                if (ServerConstant.ALIAS_NAME_TYPE_VTM.equals(nameType)) {
                    sb.append("(" + vtmDisplay + ")");
                } else {
                    sb.append(" (" + dto.getAliasNames().get(0).getAliasName() + ")");
                }
            }

            if (!Formatter.isMultipleIngredient(dto.getVtm()) && !dto.getDisplayNameType().equals(ServerConstant.ALIAS_NAME_TYPE_LOCAL_DRUG)) {
                // Strength
                if (dto.getStrengths() != null &&
                        dto.getStrengths().get(0).getStrength() != null &&
                        dto.getStrengths().size() == 1) {
                    sb.append(" " + dto.getStrengths().get(0).getStrength());
                }

                //route
                if (dto.getForm() != null && dto.getRoute() != null) {
                    if (showRoute(dto.getForm().getFormEng(), dto.getRoute().getRouteEng())) {
                        sb.append(" " + dto.getRoute().getRouteEng());
                    }
                }

                // Form
                if (dto.getForm() != null) {
                    sb.append(" " + dto.getForm().getFormEng());
                }

                if (dto.getDoseFormExtraInfo() != null) {
                    sb.append(" (" + dto.getDoseFormExtraInfo() + ")");
                }

                if (dto.getStrengths() != null && dto.getStrengths().size() == 1
                        && dto.getStrengths().get(0).getStrengthLevelExtraInfo() != null) {
                    sb.append(" (" + dto.getStrengths().get(0).getStrengthLevelExtraInfo() + ") ");
                }
            }

        }
        dto.setDisplayString(sb.toString());
    }

    private static String getVtmString(String vtm, List<String> ingredients, List<MoeDrugStrengthDto> strengthList, Boolean isStrengthCompulsoryInput, String screenDisplay) {

        boolean isStrengthCompulsory = isStrengthCompulsoryInput == null ? true : isStrengthCompulsoryInput;
        if (vtm == null || isMultipleIngredient(vtm) || !isStrengthCompulsory) return vtm;
        if (isMultipleIngredient(vtm) && strengthList.size() > 1) {
            return screenDisplay;
        }
        if (ingredients == null || ingredients.isEmpty() ||
                strengthList == null || strengthList.isEmpty()) return vtm;
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < ingredients.size(); i++) {
            sb.append(i == 0 ? "(" : "");
            sb.append(ingredients.get(i));
            if (i < strengthList.size()) sb.append(" " + strengthList.get(i).getStrength());
            if (i < ingredients.size() - 1) sb.append(" + ");
            sb.append(i == ingredients.size() - 1 ? ")" : "");
        }

        return sb.toString();
    }

    public static boolean showRoute(String form, String route) {
        HashMap<String, String> map = DtoUtil.FORM_ROUTE_MAP;
        return map.get(form + " " + route) == null || map.get(form + " " + route).equalsIgnoreCase("y");
    }
    // Simon 20190822 for moe_drug_server end--

    // Ricci 20190823 moe_drug_server start --
    public static MoeCommonDosagePo emptyToNull(MoeCommonDosagePo local) {
        local.setCreateHosp(StringUtils.trimToNull(local.getCreateHosp()));
        local.setUpdateHosp(StringUtils.trimToNull(local.getUpdateHosp()));
        local.setSupplFreqEng(StringUtils.trimToNull(local.getSupplFreqEng()));
        local.setDosageUnit(StringUtils.trimToNull(local.getDosageUnit()));

        if (local.getDosageUnit() != null && local.getDosageUnit().equals("[Blank]")) {
            local.setDosageUnit(null);
        }
        return local;
    }

    public static MoeDrugStrengthLocalPo emptyToNull(MoeDrugStrengthLocalPo local) {
        local.setCreateHosp(StringUtils.trimToNull(local.getCreateHosp()));
        local.setUpdateHosp(StringUtils.trimToNull(local.getUpdateHosp()));
        return local;
    }

    public static MoeDrugLocalPo emptyToNull(MoeDrugLocalPo local) {
        local.setCreateHosp(StringUtils.trimToNull(local.getCreateHosp()));
        local.setUpdateHosp(StringUtils.trimToNull(local.getUpdateHosp()));
        return local;
    }

    public static MoeDrugOrdPropertyLocalPo emptyToNull(MoeDrugOrdPropertyLocalPo local) {
        local.setCreateHosp(StringUtils.trimToNull(local.getCreateHosp()));
        local.setUpdateHosp(StringUtils.trimToNull(local.getUpdateHosp()));
        return local;
    }

    public static MoeDrugToSaamLocalPo emptyToNull(MoeDrugToSaamLocalPo local) {
        local.setCreateHosp(StringUtils.trimToNull(local.getCreateHosp()));
        local.setUpdateHosp(StringUtils.trimToNull(local.getUpdateHosp()));
        return local;
    }

    public static MoeDrugTherapeuticLocalPo emptyToNull(MoeDrugTherapeuticLocalPo local) {
        local.setCreateHosp(StringUtils.trimToNull(local.getCreateHosp()));
        local.setUpdateHosp(StringUtils.trimToNull(local.getUpdateHosp()));
        return local;
    }
    // Ricci 20190823 moe_drug_server end --


    // Chris 20190829 From moe_drug_server  -Start
    public static void setTradeNameAliasString(MoeDrugDto dto, String tradeNameAlias) {
        dto.setDisplayString("[" + tradeNameAlias + "] " + dto.getDisplayString());
    }
    // Chris 20190829 From moe_drug_server  -End


    // Chris 20191009  For splicing key to get max duration.  From MOE1-GWT:EhrMoeModule  -Start
    public static String toKeyString(boolean isStrengthCompulsory, String tradeName, String vtm, String route, String form, String formExtraInfo, String strength) {
        MoeStringBuffer returnString = new MoeStringBuffer();

        if (tradeName != null) returnString.append(" " + tradeName);
        if (vtm != null) returnString.append(" " + vtm);
        if (route != null) returnString.append(" " + route);
        if (form != null) returnString.append(" " + form);
        if (formExtraInfo != null) returnString.append(" " + formExtraInfo);
        if (isStrengthCompulsory && strength != null) {
            //Chris 20191104  Remove the last "+" if it exists  -Start
            strength = strength.lastIndexOf("+") != -1 && strength.lastIndexOf("+") == strength.length() - 1 ?
                    strength.substring(0, strength.length() - 1) : strength;
            //Chris 20191104  Remove the last "+" if it exists  -End
            returnString.append(" " + strength);
        }
        return returnString.toString().trim();
    }
    // Chris 20191009  For splicing key to get max duration.  From MOE1-GWT:EhrMoeModule  -End


}
