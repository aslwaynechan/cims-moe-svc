package hk.health.moe.util;


import hk.health.moe.pojo.dto.MoeActionStatusDto;
import hk.health.moe.pojo.dto.MoeBaseUnitDto;
import hk.health.moe.pojo.dto.MoeClinicalAlertRuleDto;
import hk.health.moe.pojo.dto.MoeCommonDosageTypeDto;
import hk.health.moe.pojo.dto.MoeDrugCategoryDto;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.MoeDrugGenericNameLocalDto;
import hk.health.moe.pojo.dto.MoeDrugOrderPropertyDto;
import hk.health.moe.pojo.dto.MoeDurationUnitMultiplierDto;
import hk.health.moe.pojo.dto.MoeFormDto;
import hk.health.moe.pojo.dto.MoeFreqDto;
import hk.health.moe.pojo.dto.MoeOverrideReasonDto;
import hk.health.moe.pojo.dto.MoeRegimenDto;
import hk.health.moe.pojo.dto.MoeRegimenUnitDto;
import hk.health.moe.pojo.dto.MoeRouteDto;
import hk.health.moe.pojo.dto.MoeSiteDto;
import hk.health.moe.pojo.dto.MoeSupplFreqDto;
import hk.health.moe.pojo.dto.PreparationDto;
import hk.health.moe.pojo.po.MoeBaseUnitTermIdMapPo;
import hk.health.moe.pojo.po.MoeCommonDosageTypePo;
import hk.health.moe.pojo.po.MoeConjunctionTermIdMapPo;
import hk.health.moe.pojo.po.MoeDoseFormExtraInfoPo;
import hk.health.moe.pojo.po.MoeDrugGenericNameLocalPo;
import hk.health.moe.pojo.po.MoeDrugLocalPo;
import hk.health.moe.pojo.po.MoeDurationTermIdMapPo;
import hk.health.moe.pojo.po.MoeFreqPo;
import hk.health.moe.pojo.po.MoeFreqTermIdMapPo;
import hk.health.moe.pojo.po.MoePrescribeUnitTermIdMapPo;
import hk.health.moe.pojo.po.MoePrnTermIdMapPo;
import hk.health.moe.pojo.po.MoeRegimenPo;
import hk.health.moe.pojo.po.MoeRoutePo;
import hk.health.moe.pojo.po.MoeRouteTermIdMapPo;
import hk.health.moe.pojo.po.MoeSiteTermIdMapPo;
import hk.health.moe.pojo.po.MoeSupplFreqTermIdMapPo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class TempDtoUtil {
    public static final Map<String, String> DRUGS_BY_HKREGNO = new HashMap<String, String>();
    public static final Map<String, MoeDrugDto> DRUGS_BY_DRUGNAME = new HashMap<String, MoeDrugDto>();
    public static final Map<String, List<MoeDrugDto>> DRUGS_BY_TYPE = new HashMap<String, List<MoeDrugDto>>();
    public static final Map<String, List<MoeDrugDto>> DOSAGE_DRUGS_BY_TYPE = new HashMap<String, List<MoeDrugDto>>();
    public static final Map<String, List<MoeDrugDto>> DRUGS_BY_VTM = new HashMap<String, List<MoeDrugDto>>();
    public static final Map<String, List<MoeDrugDto>> DRUGS_BY_TRADE_NAME = new HashMap<String, List<MoeDrugDto>>();
    public static final Map<String, List<MoeDrugDto>> DRUGS_BY_BAN = new HashMap<String, List<MoeDrugDto>>();
    public static final Map<String, List<MoeDrugDto>> DRUGS_BY_ABB = new HashMap<String, List<MoeDrugDto>>();
    public static final Map<String, List<MoeDrugDto>> DRUGS_BY_OTHERS = new HashMap<String, List<MoeDrugDto>>();
    public static final Map<String, List<String>> HKREG_BY_SPECIALTY = new HashMap<String, List<String>>();
    public static final List<MoeFreqDto> FREQS_LOCAL = new ArrayList<MoeFreqDto>();
    public static final List<MoeFreqDto> FREQS = new ArrayList<MoeFreqDto>();
    public static final Map<String, Integer> FREQ_ORDER = new HashMap<String, Integer>();
    public static final List<MoeActionStatusDto> ACTIONS_STATUS_LIST = new ArrayList<MoeActionStatusDto>();
    public static final HashMap<String, MoeActionStatusDto> ACTIONS_STATUS_MAP = new HashMap<String, MoeActionStatusDto>();
    public static final HashMap<String, List<MoeSupplFreqDto>> REGIMEN_TYPE_MAP = new HashMap<String, List<MoeSupplFreqDto>>();
    public static final HashMap<String, List<MoeRegimenUnitDto>> REGIMENT_UNIT_MAP = new HashMap<String, List<MoeRegimenUnitDto>>();
    public static final HashMap<String, List<MoeSiteDto>> ROUTE_MAP = new HashMap<String, List<MoeSiteDto>>();
    public static final HashMap<String, String> ROUTE_SITE_MAP = new HashMap<String, String>();
    public static final HashMap<String, String> FORM_ROUTE_MAP = new HashMap<String, String>();
    public static final List<String> DUP_BASIC_NAMES = new ArrayList<String>();
    public static final List<MoeRouteDto> ROUTE_LIST = new ArrayList<MoeRouteDto>();
    public static final List<MoeSiteDto> SITE_LIST = new ArrayList<MoeSiteDto>();
    public static final List<MoeBaseUnitDto> BASE_UNIT_LIST = new ArrayList<MoeBaseUnitDto>();
    public static final List<MoeBaseUnitDto> PRESCRIBE_UNIT_LIST = new ArrayList<MoeBaseUnitDto>();
    public static final List<MoeBaseUnitDto> DISPENSE_UNIT_LIST = new ArrayList<MoeBaseUnitDto>();
    public static final List<MoeRegimenDto> REGIMENT_LIST = new ArrayList<MoeRegimenDto>();
    public static final List<MoeFormDto> FORM_LIST = new ArrayList<MoeFormDto>();
    public static final List<MoeDoseFormExtraInfoPo> DOSE_FORM_EXTRA_INFO_LIST = new ArrayList<MoeDoseFormExtraInfoPo>();
    public static final HashMap<String, Long> DURATION_UNIT_MAP = new HashMap<String, Long>();
    public static final HashMap<String, String> DURATION_UNIT_DESC_MAP = new HashMap<String, String>();
    public static final HashMap<String, MoeClinicalAlertRuleDto> CLINICAL_ALERT_MAP = new HashMap<String, MoeClinicalAlertRuleDto>();
    public static final HashMap<String, List<Double>> MIN_DOSAGE_MAP = new HashMap<String, List<Double>>();
    public static final HashMap<String, Long> MAX_DURATION_MAP = new HashMap<String, Long>();
    public static final List<String> SPECIFY_QUANTITY_FLAG = new ArrayList<String>();
    public static final List<String> STRENGTH_COMP_SPECIFY_QUANTITY_FLAG = new ArrayList<String>();
    public static final HashMap<String, MoeDrugOrderPropertyDto> SINGLE_DRUG_ORDER_PROPERTY_MAP = new HashMap<String, MoeDrugOrderPropertyDto>();
    public static final HashMap<String, MoeDrugOrderPropertyDto> MULT_DRUG_ORDER_PROPERTY_MAP = new HashMap<String, MoeDrugOrderPropertyDto>();
    public static final HashMap<String, Long> LEGAL_CLASS_MAP = new HashMap<String, Long>();
    public static final HashMap<String, List<PreparationDto>> PREPARATION_MAP = new HashMap<String, List<PreparationDto>>();
    public static final HashMap<String, MoeDrugLocalPo> EXIST_DRUGS_MAP = new HashMap<String, MoeDrugLocalPo>();
    public static final HashMap<String, MoeDrugLocalPo> EXIST_DRUGS_MAP_WITH_STRENGTH = new HashMap<String, MoeDrugLocalPo>();
    public static final HashMap<String, MoeDrugLocalPo> EXIST_DRUGS_WITH_TRADE_ALIAS_MAP = new HashMap<String, MoeDrugLocalPo>();
    public static final HashSet<String> EXIST_DANGER_DRUGS = new HashSet<String>();
    public static final List<String> SUSPEND_DRUGS_MAP = new ArrayList<String>();
    public static final List<String> EXIST_DRUGS_WITH_ALIAS_LIST = new ArrayList<String>();
    public static final HashMap<String, List<String>> EXIST_SPECIALTY_DRUGS = new HashMap<String, List<String>>();
    public static final HashMap<String, List<String>> EXIST_NS_SPECIALTY_DRUGS = new HashMap<String, List<String>>();
    public static final ArrayList<String> DANGEROUS_DRUG_LEGAL_CLASS = new ArrayList<String>();
    public static final HashMap<String, MoeDrugCategoryDto> DRUG_CATEGORY_MAP = new HashMap<String, MoeDrugCategoryDto>();
    public static Long baseUnitDoseId;
    // Ricci 20190722 start --
    public static final List<MoeDurationUnitMultiplierDto> DURATION_UNIT_LIST = new ArrayList();
    // Ricci 20190722 end --
    public static final HashMap<String, MoeBaseUnitTermIdMapPo> BASE_UNIT_TERM_ID_MAP = new HashMap<String, MoeBaseUnitTermIdMapPo>();
    public static final HashMap<String, MoePrescribeUnitTermIdMapPo> PRESCRIBE_UNIT_TERM_ID_MAP = new HashMap<String, MoePrescribeUnitTermIdMapPo>();
    public static final HashMap<String, MoeConjunctionTermIdMapPo> CONJUNCTION_TERM_ID_MAP = new HashMap<String, MoeConjunctionTermIdMapPo>();
    public static final HashMap<String, MoeDurationTermIdMapPo> DURATION_TERM_ID_MAP = new HashMap<String, MoeDurationTermIdMapPo>();
    public static final HashMap<String, MoeFreqTermIdMapPo> FREQ_TERM_ID_MAP = new HashMap<String, MoeFreqTermIdMapPo>();
    public static final HashMap<String, MoePrnTermIdMapPo> PRN_TERM_ID_MAP = new HashMap<String, MoePrnTermIdMapPo>();
    public static final HashMap<String, MoeRouteTermIdMapPo> ROUTE_TERM_ID_MAP = new HashMap<String, MoeRouteTermIdMapPo>();
    public static final HashMap<String, MoeSiteTermIdMapPo> SITE_TERM_ID_MAP = new HashMap<String, MoeSiteTermIdMapPo>();
    public static final HashMap<String, MoeSupplFreqTermIdMapPo> SUPPL_FREQ_TERM_ID_MAP = new HashMap<String, MoeSupplFreqTermIdMapPo>();
    public static final List<MoeOverrideReasonDto> OVERRIDE_REASON_LIST = new ArrayList<MoeOverrideReasonDto>();

    // Ricci 20190823 moe_drug_server start --
    public static final List<MoeCommonDosageTypeDto> COMMON_DOSAGE_TYPE_LIST = new ArrayList();
    // Ricci 20190823 moe_drug_server end --
    public static final HashMap<String, MoeRegimenPo> REGIMEN_ID_MAP = new HashMap();
    public static final HashMap<Long, MoeFreqPo> FREQ_ID_MAP = new HashMap();
    public static final HashMap<String, MoeCommonDosageTypePo> COMMON_DOSAGE_TYPE_ID_MAP = new HashMap();
    public static final HashMap<Long, MoeRoutePo> ROUTE_ID_MAP = new HashMap();
    //Chris 20200110  -Start
//    public static final Map<String, List<MoeDrugDto>> GENERIC_NAME_DRUGS = new HashMap<String, List<MoeDrugDto>>();
//    public static final List<MoeDrugGenericNameLocalDto> GENERIC_NAME_DRUGS = new ArrayList<>();
    public static final List<MoeDrugDto> GENERIC_NAME_DRUGS = new ArrayList<>();
    //Chris 20200110  -End

    public static void clearAll() {
        DRUGS_BY_HKREGNO.clear();
        DRUGS_BY_DRUGNAME.clear();
        DRUGS_BY_TYPE.clear();
        DOSAGE_DRUGS_BY_TYPE.clear();
        DRUGS_BY_VTM.clear();
        DRUGS_BY_TRADE_NAME.clear();
        DRUGS_BY_BAN.clear();
        DRUGS_BY_ABB.clear();
        DRUGS_BY_OTHERS.clear();
        HKREG_BY_SPECIALTY.clear();
        FREQS_LOCAL.clear();
        FREQS.clear();
        FREQ_ORDER.clear();
        REGIMEN_TYPE_MAP.clear();
        ROUTE_MAP.clear();
        ROUTE_SITE_MAP.clear();
        FORM_ROUTE_MAP.clear();
        ROUTE_LIST.clear();
        SITE_LIST.clear();
        BASE_UNIT_LIST.clear();
        PRESCRIBE_UNIT_LIST.clear();
        FORM_LIST.clear();
        DOSE_FORM_EXTRA_INFO_LIST.clear();
        MIN_DOSAGE_MAP.clear();
        MAX_DURATION_MAP.clear();
        SINGLE_DRUG_ORDER_PROPERTY_MAP.clear();
        MULT_DRUG_ORDER_PROPERTY_MAP.clear();
        PREPARATION_MAP.clear();
        EXIST_DRUGS_MAP.clear();
        EXIST_DRUGS_WITH_ALIAS_LIST.clear();
        EXIST_DRUGS_MAP_WITH_STRENGTH.clear();
        EXIST_DANGER_DRUGS.clear();
        EXIST_DRUGS_WITH_TRADE_ALIAS_MAP.clear();
        SUSPEND_DRUGS_MAP.clear();
        EXIST_SPECIALTY_DRUGS.clear();
        EXIST_NS_SPECIALTY_DRUGS.clear();
        STRENGTH_COMP_SPECIFY_QUANTITY_FLAG.clear();
        SPECIFY_QUANTITY_FLAG.clear();
        DRUG_CATEGORY_MAP.clear();
        BASE_UNIT_TERM_ID_MAP.clear();
        PRESCRIBE_UNIT_TERM_ID_MAP.clear();
        CONJUNCTION_TERM_ID_MAP.clear();
        DURATION_TERM_ID_MAP.clear();
        FREQ_TERM_ID_MAP.clear();
        PRN_TERM_ID_MAP.clear();
        ROUTE_TERM_ID_MAP.clear();
        SITE_TERM_ID_MAP.clear();
        SUPPL_FREQ_TERM_ID_MAP.clear();
        OVERRIDE_REASON_LIST.clear();

        // Ricci 20190722 start --
        ACTIONS_STATUS_LIST.clear();
        ACTIONS_STATUS_MAP.clear();
        REGIMENT_UNIT_MAP.clear();
        DUP_BASIC_NAMES.clear();
        DISPENSE_UNIT_LIST.clear();
        REGIMENT_LIST.clear();
        DURATION_UNIT_MAP.clear();
        DURATION_UNIT_DESC_MAP.clear();
        CLINICAL_ALERT_MAP.clear();
        LEGAL_CLASS_MAP.clear();
        DANGEROUS_DRUG_LEGAL_CLASS.clear();
        DURATION_UNIT_LIST.clear(); // needed
        // Ricci 20190722 end --

        // Ricci 20190823 moe_drug_server start --
        COMMON_DOSAGE_TYPE_LIST.clear();
        // Ricci 20190823 moe_drug_server end --
        REGIMEN_ID_MAP.clear();
        FREQ_ID_MAP.clear();
        COMMON_DOSAGE_TYPE_ID_MAP.clear();
        ROUTE_ID_MAP.clear();

        //Chris 20200110  -Start
        GENERIC_NAME_DRUGS.clear();
        //Chris 20200110  -End

    }

    public static Long getBaseUnitDoseId() {
        return baseUnitDoseId;
    }

    public static void setBaseUnitDoseId(Long baseUnitDoseIdInput) {
        baseUnitDoseId = baseUnitDoseIdInput;
    }

    public static void addDtoToMap(Map<String, List<MoeDrugDto>> map, String key, MoeDrugDto dto) {
        List<MoeDrugDto> value = map.get(key);
        if (value == null) value = new ArrayList<MoeDrugDto>();
        value.add(dto);
        map.put(key, value);
    }

    public static void copyContentFromTemp() throws Exception {
        try {
            DtoUtil.clearAll();

            DtoUtil.DRUGS_BY_HKREGNO.putAll(TempDtoUtil.DRUGS_BY_HKREGNO);
            DtoUtil.DRUGS_BY_DRUGNAME.putAll(TempDtoUtil.DRUGS_BY_DRUGNAME);
            DtoUtil.DRUGS_BY_TYPE.putAll(TempDtoUtil.DRUGS_BY_TYPE);
            DtoUtil.DOSAGE_DRUGS_BY_TYPE.putAll(TempDtoUtil.DOSAGE_DRUGS_BY_TYPE);
            DtoUtil.DRUGS_BY_VTM.putAll(TempDtoUtil.DRUGS_BY_VTM);
            DtoUtil.DRUGS_BY_TRADE_NAME.putAll(TempDtoUtil.DRUGS_BY_TRADE_NAME);
            DtoUtil.DRUGS_BY_BAN.putAll(TempDtoUtil.DRUGS_BY_BAN);
            DtoUtil.DRUGS_BY_ABB.putAll(TempDtoUtil.DRUGS_BY_ABB);
            DtoUtil.DRUGS_BY_OTHERS.putAll(TempDtoUtil.DRUGS_BY_OTHERS);
            DtoUtil.HKREG_BY_SPECIALTY.putAll(TempDtoUtil.HKREG_BY_SPECIALTY);
            DtoUtil.FREQS_LOCAL.addAll(TempDtoUtil.FREQS_LOCAL);
            DtoUtil.FREQS.addAll(TempDtoUtil.FREQS);
            DtoUtil.FREQ_ORDER.putAll(TempDtoUtil.FREQ_ORDER);
            DtoUtil.ROUTE_MAP.putAll(TempDtoUtil.ROUTE_MAP);
            DtoUtil.ROUTE_SITE_MAP.putAll(TempDtoUtil.ROUTE_SITE_MAP);
            DtoUtil.FORM_ROUTE_MAP.putAll(TempDtoUtil.FORM_ROUTE_MAP);
            DtoUtil.ROUTE_LIST.addAll(TempDtoUtil.ROUTE_LIST);
            DtoUtil.SITE_LIST.addAll(TempDtoUtil.SITE_LIST);
            DtoUtil.BASE_UNIT_LIST.addAll(TempDtoUtil.BASE_UNIT_LIST);
            DtoUtil.PRESCRIBE_UNIT_LIST.addAll(TempDtoUtil.PRESCRIBE_UNIT_LIST);
            DtoUtil.FORM_LIST.addAll(TempDtoUtil.FORM_LIST);
            DtoUtil.DOSE_FORM_EXTRA_INFO_LIST.addAll(TempDtoUtil.DOSE_FORM_EXTRA_INFO_LIST);
            DtoUtil.MIN_DOSAGE_MAP.putAll(TempDtoUtil.MIN_DOSAGE_MAP);
            DtoUtil.MAX_DURATION_MAP.putAll(TempDtoUtil.MAX_DURATION_MAP);
            DtoUtil.SINGLE_DRUG_ORDER_PROPERTY_MAP.putAll(TempDtoUtil.SINGLE_DRUG_ORDER_PROPERTY_MAP);
            DtoUtil.MULT_DRUG_ORDER_PROPERTY_MAP.putAll(TempDtoUtil.MULT_DRUG_ORDER_PROPERTY_MAP);
            DtoUtil.PREPARATION_MAP.putAll(TempDtoUtil.PREPARATION_MAP);
            DtoUtil.EXIST_DRUGS_MAP.putAll(TempDtoUtil.EXIST_DRUGS_MAP);
            DtoUtil.EXIST_DRUGS_WITH_ALIAS_LIST.addAll(TempDtoUtil.EXIST_DRUGS_WITH_ALIAS_LIST);
            DtoUtil.EXIST_DRUGS_MAP_WITH_STRENGTH.putAll(TempDtoUtil.EXIST_DRUGS_MAP_WITH_STRENGTH);
            DtoUtil.EXIST_DANGER_DRUGS.addAll(TempDtoUtil.EXIST_DANGER_DRUGS);
            DtoUtil.EXIST_DRUGS_WITH_TRADE_ALIAS_MAP.putAll(TempDtoUtil.EXIST_DRUGS_WITH_TRADE_ALIAS_MAP);
            DtoUtil.SUSPEND_DRUGS_MAP.addAll(TempDtoUtil.SUSPEND_DRUGS_MAP);
            DtoUtil.EXIST_SPECIALTY_DRUGS.putAll(TempDtoUtil.EXIST_SPECIALTY_DRUGS);
            DtoUtil.EXIST_NS_SPECIALTY_DRUGS.putAll(TempDtoUtil.EXIST_NS_SPECIALTY_DRUGS);
            DtoUtil.STRENGTH_COMP_SPECIFY_QUANTITY_FLAG.addAll(TempDtoUtil.STRENGTH_COMP_SPECIFY_QUANTITY_FLAG);
            DtoUtil.SPECIFY_QUANTITY_FLAG.addAll(TempDtoUtil.SPECIFY_QUANTITY_FLAG);
            DtoUtil.DRUG_CATEGORY_MAP.putAll(TempDtoUtil.DRUG_CATEGORY_MAP);
            DtoUtil.BASE_UNIT_TERM_ID_MAP.putAll(TempDtoUtil.BASE_UNIT_TERM_ID_MAP);
            DtoUtil.PRESCRIBE_UNIT_TERM_ID_MAP.putAll(TempDtoUtil.PRESCRIBE_UNIT_TERM_ID_MAP);
            DtoUtil.CONJUNCTION_TERM_ID_MAP.putAll(TempDtoUtil.CONJUNCTION_TERM_ID_MAP);
            DtoUtil.DURATION_TERM_ID_MAP.putAll(TempDtoUtil.DURATION_TERM_ID_MAP);
            DtoUtil.FREQ_TERM_ID_MAP.putAll(TempDtoUtil.FREQ_TERM_ID_MAP);
            DtoUtil.PRN_TERM_ID_MAP.putAll(TempDtoUtil.PRN_TERM_ID_MAP);
            DtoUtil.ROUTE_TERM_ID_MAP.putAll(TempDtoUtil.ROUTE_TERM_ID_MAP);
            DtoUtil.SITE_TERM_ID_MAP.putAll(TempDtoUtil.SITE_TERM_ID_MAP);
            DtoUtil.SUPPL_FREQ_TERM_ID_MAP.putAll(TempDtoUtil.SUPPL_FREQ_TERM_ID_MAP);
            DtoUtil.OVERRIDE_REASON_LIST.addAll(TempDtoUtil.OVERRIDE_REASON_LIST);

            DtoUtil.setBaseUnitDoseId(TempDtoUtil.getBaseUnitDoseId());

            // Ricci 20190722 start --
            DtoUtil.REGIMEN_TYPE_MAP.putAll(TempDtoUtil.REGIMEN_TYPE_MAP);
            DtoUtil.ACTIONS_STATUS_LIST.addAll(TempDtoUtil.ACTIONS_STATUS_LIST);
            DtoUtil.ACTIONS_STATUS_MAP.putAll(TempDtoUtil.ACTIONS_STATUS_MAP);
            DtoUtil.REGIMENT_UNIT_MAP.putAll(TempDtoUtil.REGIMENT_UNIT_MAP);
            DtoUtil.DUP_BASIC_NAMES.addAll(TempDtoUtil.DUP_BASIC_NAMES);
            DtoUtil.DISPENSE_UNIT_LIST.addAll(TempDtoUtil.DISPENSE_UNIT_LIST);
            DtoUtil.REGIMENT_LIST.addAll(TempDtoUtil.REGIMENT_LIST);
            DtoUtil.DURATION_UNIT_MAP.putAll(TempDtoUtil.DURATION_UNIT_MAP);
            DtoUtil.DURATION_UNIT_DESC_MAP.putAll(TempDtoUtil.DURATION_UNIT_DESC_MAP);
            DtoUtil.CLINICAL_ALERT_MAP.putAll(TempDtoUtil.CLINICAL_ALERT_MAP);
            DtoUtil.LEGAL_CLASS_MAP.putAll(TempDtoUtil.LEGAL_CLASS_MAP);
            DtoUtil.DANGEROUS_DRUG_LEGAL_CLASS.addAll(TempDtoUtil.DANGEROUS_DRUG_LEGAL_CLASS);
            DtoUtil.DURATION_UNIT_LIST.addAll(TempDtoUtil.DURATION_UNIT_LIST);
            // Ricci 20190722 end --

            // Ricci 20190823 moe_drug_server start --
            DtoUtil.COMMON_DOSAGE_TYPE_LIST.addAll(TempDtoUtil.COMMON_DOSAGE_TYPE_LIST);
            // Ricci 20190823 moe_drug_server end --
            DtoUtil.REGIMEN_ID_MAP.putAll(TempDtoUtil.REGIMEN_ID_MAP);
            DtoUtil.FREQ_ID_MAP.putAll(TempDtoUtil.FREQ_ID_MAP);
            DtoUtil.COMMON_DOSAGE_TYPE_ID_MAP.putAll(TempDtoUtil.COMMON_DOSAGE_TYPE_ID_MAP);
            DtoUtil.ROUTE_ID_MAP.putAll(TempDtoUtil.ROUTE_ID_MAP);

            //Chris 20200110  -Start
            DtoUtil.GENERIC_NAME_DRUGS.addAll(TempDtoUtil.GENERIC_NAME_DRUGS);
            //Chris 20200110  -End

        } catch (Exception e) {
            throw e;
        }
    }
}
