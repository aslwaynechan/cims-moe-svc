package hk.health.moe.util;

import hk.health.moe.common.EhrOrderLineTypeEnum;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.common.WebServiceConstant;
import hk.health.moe.pojo.dto.DurationUnit;
import hk.health.moe.pojo.dto.MoeActionStatusDto;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.MoeEhrMedAllergenDto;
import hk.health.moe.pojo.dto.MoeEhrMedProfileDto;
import hk.health.moe.pojo.dto.MoeMedMultDoseDto;
import hk.health.moe.pojo.dto.MoeMedProfileDto;
import hk.health.moe.pojo.dto.MoeMedicationDecisionDto;
import hk.health.moe.pojo.dto.InnerMoePatientAllergyDto;
import hk.health.moe.pojo.dto.MoePatientMedicationDto;
import hk.health.moe.pojo.dto.MoeSupplFreqDescDto;
import hk.health.moe.pojo.dto.MoeSupplFreqDto;
import hk.health.moe.restservice.dto.dac.PatientAllergyDto;
import hk.health.moe.restservice.dto.dac.PatientMedicationDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.util.HtmlUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WsFormatter {

    protected static final List<DurationUnit> DURATION_UNIT_LIST = new ArrayList<DurationUnit>();

    static {
        DURATION_UNIT_LIST.add(new DurationUnit("d", "day(s)"));
        DURATION_UNIT_LIST.add(new DurationUnit("w", "week(s)"));
        DURATION_UNIT_LIST.add(new DurationUnit("m", "month(s)"));
        DURATION_UNIT_LIST.add(new DurationUnit("c", "cycle(s)"));
        DURATION_UNIT_LIST.add(new DurationUnit("s", "dose(s)"));
    }

    public static Double toDouble(BigDecimal bd) {
        try {
            return Double.valueOf((bd.stripTrailingZeros().toPlainString()));
        } catch (NumberFormatException e) {
            return new Double(bd.floatValue());
        }
    }

    public static final String findGenericDrugTradeName(String tradeName) {
        int openCount = 0;
        int start = 0;
        int close = 0;
        for (int i = 0; i < tradeName.length(); i++) {
            char c = tradeName.charAt(i);
            if (c == '('
                    && tradeName.substring(i, tradeName.length()).length() > 3
                    && tradeName.substring(i, i + 3).equals("(as")) {
                openCount++;
            }
            if (openCount == 0 && c == '(') {
                start = i;
            }
            if (openCount == 0 && c == ')') {
                close = i;
            } else if (c == ')') {
                openCount--;
            }
            if (start != 0 && close != 0 && openCount == 0) {
                break;
            }
        }
        if (start == 0 && close == 0) {
            return "";
        }
        return tradeName.substring(start, close + 1);
    }

    // Ricci 20190916 start --
    public static String toPrescriptionContent(List<PatientMedicationDto> medicationDtos) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < medicationDtos.size(); i++) {
            PatientMedicationDto dto = medicationDtos.get(i);
            sb.append("Drug VTM: " + dto.getVtm());
            sb.append("&&VTM Term ID: " + dto.getVtmTermId());

            if (i != medicationDtos.size() - 1) {
                sb.append("##");
            }
        }

        return sb.toString();
    }

    public static String totAllergyContent(List<PatientAllergyDto> allergy) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < allergy.size(); i++) {
            PatientAllergyDto dto = allergy.get(i);
            sb.append("Drug VTM: " + dto.getAllergen());
            sb.append("&&VTM Term ID: " + dto.getAllergenTermID());

            if (i != allergy.size() - 1) {
                sb.append(";;");
            }
        }
        return sb.toString();
    }

    public static String toAuditLogContent(List<PatientAllergyDto> allergy, List<PatientMedicationDto> medicationDtos) {
        StringBuffer sb = new StringBuffer();
        sb.append("Drug::");
        sb.append(toPrescriptionContent(medicationDtos));
        sb.append("||Allergy::");
        sb.append(totAllergyContent(allergy));
        return sb.toString();
    }

    public static InnerMoePatientAllergyDto getPatientAllergyDTO(MoeMedicationDecisionDto supportDto, String seqNo) {
        for (InnerMoePatientAllergyDto dto : supportDto.getPatientAllergies()) {
            if (dto.getAllergySeqNo().equals(seqNo)) {
                return dto;
            }
        }
        return null;
    }

    public static MoePatientMedicationDto getMedicationByOrderLineNo(List<MoePatientMedicationDto> moeMedicationDecisionDtos, String orderLineNum) {
        for (MoePatientMedicationDto p : moeMedicationDecisionDtos) {
            if (p.getOrderLineRowNum().equals(orderLineNum)) {
                return p;
            }
        }
        return null;
    }
    // Ricci 20190916 end --

    public static String formatDosage(BigDecimal s) {
        Double a;
        if (s == null) {
            a = new Double(0);
        } else {
            a = new Double(s.doubleValue());
        }

        DecimalFormat formatter = new DecimalFormat(WebServiceConstant.DOSAGE_FORMAT2);
        return formatter.format(a);
    }

    public static String[] toMedProfileDisplayString(MoeMedProfileDto dto, Date orderDate, boolean notEpr) {
        return toMedProfileDisplayString(dto, false, false, orderDate, notEpr);
    }

    public static String toDurationUnitDisplay(String code, Long duration) {
        String unit = toDurationUnitDisplay(code);
        if (unit.length() > 0) {
            if (duration != null && duration < 2) {
                return unit.replace("(s)", "");
            } else {
                return unit.replace("(s)", "") + "s";
            }
        }
        return "";
    }

    public static String toDurationUnitDisplay(String code) {
        for (DurationUnit d : DURATION_UNIT_LIST) {
            if (d.getCode().equals(code)) return d.getName();
        }
        return "";
    }


    public static boolean isMultipleIngredient(String vtm) {
        return MoeDrugDto.isMultiIngr(vtm);
    }

    public static String[] toMedProfileDisplayString(MoeMedProfileDto dto, boolean showFavIcon, boolean isSearchMyFavourite, Date orderDate, boolean addSpecInstToDoseInst) {

        StringBuffer sb = new StringBuffer();
        StringBuffer sbWithStyle = new StringBuffer();
        StringBuffer sbPrinting = new StringBuffer();
        boolean addDashFirst = false;
        final MoeEhrMedProfileDto moeEhrMedProfileDto = dto.getMoeEhrMedProfile();
        final String orderLineType = moeEhrMedProfileDto.getOrderLineType();

        final String durationUnit = toDurationUnitDisplay(dto.getDurationUnit(), dto.getDuration());
        final String conjunction = (orderLineType != null && orderLineType.equals(EhrOrderLineTypeEnum.STEP_UP_DOWN)) ?
                WebServiceConstant.MESSAGES_THEN : WebServiceConstant.MESSAGES_AND;
        final String conjunctionWithStyle = (orderLineType != null && orderLineType.equals(EhrOrderLineTypeEnum.STEP_UP_DOWN)) ?
                "<b>" + WebServiceConstant.MESSAGES_THEN + "</b><br>" : "<b>" + WebServiceConstant.MESSAGES_AND + "</b><br>";
        final String conjunctionPrinting = (orderLineType != null && orderLineType.equals(EhrOrderLineTypeEnum.STEP_UP_DOWN)) ?
                "<b>" + WebServiceConstant.MESSAGES_THEN + "</b><br>" : "<b>" + WebServiceConstant.MESSAGES_AND + "</b><br>";
        final String nameType = dto.getNameType();

        String vtm = moeEhrMedProfileDto.getVtm();
        if (vtm == null) {
            vtm = "";
        }
        int numIngredient = StringUtils.countMatches(vtm, "+");
        String tradeName = moeEhrMedProfileDto.getTradeName();

        if (!WsFormatter.isMultipleIngredient(dto.getMoeEhrMedProfile().getVtm()) && moeEhrMedProfileDto.getAliasNameType() != null && ServerConstant.ALIAS_NAME_TYPE_TRADE_NAME_ALIAS.equalsIgnoreCase(moeEhrMedProfileDto.getAliasNameType())) {
            sbWithStyle.append("<b>[" + HtmlUtils.htmlEscape(moeEhrMedProfileDto.getAliasName()) + "]</b> ");
            sbPrinting.append("<b>[" + HtmlUtils.htmlEscape(moeEhrMedProfileDto.getAliasName()) + "]</b> ");
            sb.append("[" + moeEhrMedProfileDto.getAliasName() + "] ");
        }

        if (moeEhrMedProfileDto.getAliasName() != null
                && moeEhrMedProfileDto.getAliasNameType() != null
                && moeEhrMedProfileDto.getAliasNameType().equalsIgnoreCase(ServerConstant.ALIAS_NAME_TYPE_TRADE_NAME_ALIAS)
                && moeEhrMedProfileDto.getAliasNameType().equalsIgnoreCase(ServerConstant.ALIAS_NAME_TYPE_VTM)) {
            if ("Y".equalsIgnoreCase(moeEhrMedProfileDto.getGenericIndicator())) { // trade name doesn't exist
                String manufacturer = findGenericDrugTradeName(tradeName);
                if (!StringUtils.isEmpty(manufacturer)) {
                    String vtmNew = tradeName.replace(manufacturer, "");
                    sbWithStyle.append(HtmlUtils.htmlEscape(vtmNew) + "<i>" + HtmlUtils.htmlEscape(manufacturer) + "</i>");
                    sbPrinting.append(HtmlUtils.htmlEscape(vtmNew) + "<i>" + HtmlUtils.htmlEscape(manufacturer) + "</i>");
                } else {
                    sbWithStyle.append(tradeName);
                    sbPrinting.append(tradeName);
                }
                sb.append(tradeName);
            } else {
                sbWithStyle.append("<b>" + HtmlUtils.htmlEscape(tradeName) + "</b>");
                sbPrinting.append("<b>" + HtmlUtils.htmlEscape(tradeName) + "</b>");
                sb.append(tradeName);
            }

            sbWithStyle.append(" (" + HtmlUtils.htmlEscape(moeEhrMedProfileDto.getAliasName()) + ")");
            sbPrinting.append(" (" + HtmlUtils.htmlEscape(moeEhrMedProfileDto.getAliasName()) + ")");
            sb.append(" (" + moeEhrMedProfileDto.getAliasName() + ")");

            String strength = dto.getStrength();
            if (strength != null && !strength.isEmpty()) {
                sbWithStyle.append(" " + HtmlUtils.htmlEscape(strength));
                sbPrinting.append(" " + HtmlUtils.htmlEscape(strength));
                sb.append(" " + strength);
            }
        } else {
            String genericIndicator = moeEhrMedProfileDto.getGenericIndicator();
            boolean isHideVtm = genericIndicator != null && "Y".equalsIgnoreCase(genericIndicator);
            String screenDisplay = moeEhrMedProfileDto.getScreenDisplay();
            if (screenDisplay != null && ServerConstant.ALIAS_NAME_TYPE_LOCAL_DRUG.equals(dto.getNameType())) {
                sbWithStyle.append(HtmlUtils.htmlEscape(screenDisplay));
                sbPrinting.append(HtmlUtils.htmlEscape(screenDisplay));
                sb.append(screenDisplay);
                addDashFirst = true;
            } else if (numIngredient == 0) {            // only have 1 vtm item
                if (StringUtils.isNotEmpty(tradeName)
                        && !isHideVtm) {
                    sbWithStyle.append("<b>" + HtmlUtils.htmlEscape(tradeName) + "</b>");
                    sbPrinting.append("<b>" + HtmlUtils.htmlEscape(tradeName) + "</b>");
                    sb.append(tradeName);
                    sbWithStyle.append(" (" + HtmlUtils.htmlEscape(vtm) + ")");
                    sbPrinting.append(" (" + HtmlUtils.htmlEscape(vtm) + ")");
                    sb.append(" (" + vtm + ")");
                } else if (StringUtils.isNotEmpty(tradeName)
                        && isHideVtm) {
                    String manufacturer = findGenericDrugTradeName(tradeName);
                    if (!StringUtils.isEmpty(manufacturer)) {
                        vtm = tradeName.replace(manufacturer, "");
                        sbWithStyle.append(HtmlUtils.htmlEscape(vtm) + "<i>" + HtmlUtils.htmlEscape(manufacturer) + "</i> ");
                        sbPrinting.append(HtmlUtils.htmlEscape(vtm) + "<i>" + HtmlUtils.htmlEscape(manufacturer) + "</i> ");
                        addDashFirst = true;
                    } else {
                        sbWithStyle.append(tradeName);
                        sbPrinting.append(tradeName);
                        addDashFirst = true;
                    }
                    sb.append(HtmlUtils.htmlEscape(tradeName));
                } else if (StringUtils.isNotEmpty(screenDisplay)) {
                    sbWithStyle.append("<b>" + HtmlUtils.htmlEscape(screenDisplay) + "</b>");
                    sbPrinting.append("<b>" + HtmlUtils.htmlEscape(screenDisplay) + "</b>");
                    sb.append(screenDisplay);
                    addDashFirst = true;
                }

                String strength = dto.getStrength();
                if (strength != null && !strength.isEmpty()) {
                    sbWithStyle.append(" " + HtmlUtils.htmlEscape(strength));
                    sbPrinting.append(" " + HtmlUtils.htmlEscape(strength));
                    sb.append(" " + strength);
                    addDashFirst = true;
                }
            } else {
                addDashFirst = true;
                // try to remove amp last (....) value
                String amp = moeEhrMedProfileDto.getScreenDisplay();

                boolean matchBasket = true;
                if (amp.indexOf("+") == -1) {
                    matchBasket = false;
                }

                if (matchBasket && !isHideVtm && amp != null && !amp.isEmpty()) {

                    int firstOpen = 0;
                    int firstPlus = amp.indexOf('+');
                    String temp = amp.substring(0, firstPlus);
                    int occurrence = 0;
                    while (temp.indexOf("(") == -1) {
                        int index = StringUtil.nthOccurrence(amp, "+", ++occurrence);
                        if (index != -1) {
                            temp = amp.substring(0, index - 1);
                        } else {
                            matchBasket = false;
                            break;
                        }
                    }

                    for (int ii = temp.length() - 1; ii >= 0; ii--) {
                        if (temp.substring(ii, ii + 1).equals(")")) {
                            firstOpen++;
                        } else if (temp.substring(ii, ii + 1).equals("(") && firstOpen != 0) {
                            firstOpen--;
                        } else if (temp.substring(ii, ii + 1).equals("(") && firstOpen == 0) {
                            firstOpen = ii;
                            break;
                        }
                    }

                    int firstClose = 0;
                    int lastPlus = amp.lastIndexOf('+');
                    temp = amp;
                    for (int ij = lastPlus; ij < amp.length(); ij++) {
                        if (temp.substring(ij, ij + 1).equals("(")) {
                            firstClose++;
                        }
                        if (temp.substring(ij, ij + 1).equals(")") && firstClose != 0) {
                            firstClose--;
                        } else if (temp.substring(ij, ij + 1).equals(")") && firstClose == 0) {
                            firstClose = ij;
                            break;
                        }
                    }

                    if (firstOpen != 0 && firstClose != 0) {
                        if (matchBasket && numIngredient > 2) {
                            tradeName = amp.substring(0, firstOpen);
                            String removeValue = amp.substring(firstOpen, firstClose + 1);
                            amp = StringUtils.remove(amp, removeValue);
                            String ampWithStyle = amp.replace(tradeName, "");
                            ampWithStyle = "<b>" + HtmlUtils.htmlEscape(tradeName) + "</b>" + HtmlUtils.htmlEscape(ampWithStyle);
                            amp = amp.trim();
                            ampWithStyle = ampWithStyle.trim();
                            amp = StringUtils.replace(amp, "  ", " "); // replace double space by single space
                            ampWithStyle = StringUtils.replace(ampWithStyle, "  ", " "); // replace double space by single space
                            if (!amp.isEmpty()) {
                                sbWithStyle.append(ampWithStyle);
                                sbPrinting.append(ampWithStyle);
                                sb.append(amp);
                            }
                        } else {
                            tradeName = amp.substring(0, firstOpen);
                            String ampWithStyle = amp.replace(tradeName, "");
                            ampWithStyle = "<b>" + HtmlUtils.htmlEscape(tradeName) + "</b>" + HtmlUtils.htmlEscape(ampWithStyle);
                            sbWithStyle.append(ampWithStyle);
                            sbPrinting.append(ampWithStyle);
                            sb.append(amp);
                        }
                    }
                } else {
                    // Generic indicator = 'Y'
                    if (dto.getMoeEhrMedProfile().getAliasNameType() != null && ServerConstant.ALIAS_NAME_TYPE_TRADE_NAME_ALIAS.equalsIgnoreCase(dto.getMoeEhrMedProfile().getAliasNameType())) {
                        amp = screenDisplay.replace("[" + dto.getMoeEhrMedProfile().getAliasName() + "]", "");
                        sbWithStyle.append("<b>[" + dto.getMoeEhrMedProfile().getAliasName() + "]</b>");
                        sbPrinting.append("<b>[" + dto.getMoeEhrMedProfile().getAliasName() + "]</b>");
                        sb.append("[" + dto.getMoeEhrMedProfile().getAliasName() + "]");
                    }

                    String manufacturer = findGenericDrugTradeName(amp);
                    if (!StringUtils.isEmpty(manufacturer)) {
                        vtm = amp.replace(HtmlUtils.htmlEscape(manufacturer), "<i> " + manufacturer + "</i>");
                        sbWithStyle.append(vtm);
                        sbPrinting.append(vtm);
                    } else {
                        sbWithStyle.append(amp);
                        sbPrinting.append(amp);
                    }

                    sb.append(amp);
                }
            }

        }

        if (!WsFormatter.isMultipleIngredient(dto.getMoeEhrMedProfile().getVtm())
                && dto.getNameType().compareTo(ServerConstant.ALIAS_NAME_TYPE_LOCAL_DRUG) != 0) {
            //route
            if (dto.getFormDesc() != null && dto.getMoeEhrMedProfile().getDrugRouteEng() != null) {
                String key = dto.getFormDesc() + " " + dto.getMoeEhrMedProfile().getDrugRouteEng();
                if (DtoUtil.FORM_ROUTE_MAP.get(key) == null || "Y".equalsIgnoreCase(DtoUtil.FORM_ROUTE_MAP.get(key))) {
                    sb.append(" " + dto.getMoeEhrMedProfile().getDrugRouteEng());
                    sbWithStyle.append(" " + HtmlUtils.htmlEscape(dto.getMoeEhrMedProfile().getDrugRouteEng()));
                    sbPrinting.append(" " + HtmlUtils.htmlEscape(dto.getMoeEhrMedProfile().getDrugRouteEng()));
                }
            }

            // Form
            if (dto.getFormDesc() != null) {
                sb.append(" " + trim(dto.getFormDesc()));
                sbPrinting.append(" " + HtmlUtils.htmlEscape(trim(dto.getFormDesc())));
                sbWithStyle.append(" " + HtmlUtils.htmlEscape(trim(dto.getFormDesc())));
            }

            // dose form extra info
            if (dto.getMoeEhrMedProfile().getDoseFormExtraInfo() != null
                    && dto.getNameType().compareTo(ServerConstant.ALIAS_NAME_TYPE_LOCAL_DRUG) != 0) {
                sb.append(" (" + dto.getMoeEhrMedProfile().getDoseFormExtraInfo() + ") ");
                sbWithStyle.append(" (" + HtmlUtils.htmlEscape(dto.getMoeEhrMedProfile().getDoseFormExtraInfo()) + ") ");
                sbPrinting.append(" (" + HtmlUtils.htmlEscape(dto.getMoeEhrMedProfile().getDoseFormExtraInfo()) + ") ");
            }

            // strength level extra info
            if (dto.getMoeEhrMedProfile().getStrengthLevelExtraInfo() != null
                    && dto.getNameType().compareTo(ServerConstant.ALIAS_NAME_TYPE_LOCAL_DRUG) != 0) {
                sb.append(" (" + dto.getMoeEhrMedProfile().getStrengthLevelExtraInfo() + ") ");
                sbWithStyle.append(" (" + HtmlUtils.htmlEscape(dto.getMoeEhrMedProfile().getStrengthLevelExtraInfo()) + ") ");
                sbPrinting.append(" (" + HtmlUtils.htmlEscape(dto.getMoeEhrMedProfile().getStrengthLevelExtraInfo()) + ") ");
            }
        }

        boolean wrapAlready = false;
        if (dto.getMoeMedMultDoses().size() > 0) {
            wrapAlready = true;
            sbWithStyle.append("<br>");
            sbWithStyle.append("&nbsp;&nbsp;&nbsp;&nbsp;");
            sbPrinting.append("<br>");
            sbPrinting.append("&nbsp;&nbsp;&nbsp;&nbsp;");
        }

        // Dosage
        if (isNormalAndAdvance(dto) && dto.getDosage() != null && !trim(dto.getModu()).isEmpty()) {
            sb.append(" - " + formatDosage(dto.getDosage()));
            String modu = StringUtil.stripToEmpty(dto.getModu());
            sb.append(" " + (ServerConstant.FIVE_ML_SPOONFUL.equals(modu) ? " x " + modu : modu));
            sbWithStyle.append(" - <b>" + formatDosage(dto.getDosage()));
            sbWithStyle.append(" " + (ServerConstant.FIVE_ML_SPOONFUL.equals(modu) ? " x " + modu : modu) + "</b>");
            sbPrinting.append(" - <b>" + formatDosage(dto.getDosage()));
            sbPrinting.append(" " + (ServerConstant.FIVE_ML_SPOONFUL.equals(modu) ? " x " + modu : modu) + "</b>");
        } else if (dto.getMoeMedMultDoses() != null && dto.getMoeMedMultDoses().size() > 0 && dto.getMoeMedMultDoses().get(0).getDosage() != null) {
            MoeMedMultDoseDto dosage = dto.getMoeMedMultDoses().get(0);
            sb.append(" - " + formatDosage(new BigDecimal(dosage.getDosage())));
            String modu = StringUtil.stripToEmpty(dto.getModu());
            sb.append(" " + (ServerConstant.FIVE_ML_SPOONFUL.equals(modu) ? " x " + modu : modu));
            sbWithStyle.append("<b>" + formatDosage(new BigDecimal(dosage.getDosage())));
            sbWithStyle.append(" " + (ServerConstant.FIVE_ML_SPOONFUL.equals(modu) ? " x " + modu : modu) + "</b>");
            sbPrinting.append("<b>" + formatDosage(new BigDecimal(dosage.getDosage())));
            sbPrinting.append(" " + (ServerConstant.FIVE_ML_SPOONFUL.equals(modu) ? " x " + modu : modu) + "</b>");
            addDashFirst = true;
            if (wrapAlready) {
                wrapAlready = false;
            }
        }

        // freq
        if (isNormalAndAdvance(dto)) {
            String freeText = getFreqText(dto, isSearchMyFavourite);
            if (freeText != null && freeText.trim().length() > 0) {
                String freqText = " - " + freeText;
                sb.append(freqText);
                sbWithStyle.append(freqText);
                sbPrinting.append(freqText);
                if (wrapAlready) {
                    wrapAlready = false;
                }
            }
        } else if (dto.getMoeMedMultDoses() != null && dto.getMoeMedMultDoses().size() > 0) {
            MoeMedMultDoseDto dosage = dto.getMoeMedMultDoses().get(0);
            String multiDoseFreqText = getMultiDoseFreqText(dto, dosage, 0, isSearchMyFavourite);
            if (multiDoseFreqText.trim().length() > 0) {
                if (addDashFirst && !wrapAlready) {
                    sbWithStyle.append(" - ");
                    sbPrinting.append(" - ");
                }
                if (wrapAlready) {
                    wrapAlready = false;
                }
                sb.append(" - " + multiDoseFreqText);
                sbWithStyle.append(multiDoseFreqText);
                sbPrinting.append(multiDoseFreqText);
                addDashFirst = true;
            }
        }

        // PRN
        if (orderLineType.equals(EhrOrderLineTypeEnum.MULTIPLE_LINE) || (orderLineType.equals(EhrOrderLineTypeEnum.RECURRENT)
                && dto.getMoeMedMultDoses().size() > 0)) {
            if (ServerConstant.DRUG_DOSAGE_PRN_YES.equals(dto.getPrn())) {
                if (addDashFirst && !wrapAlready) {
                    sbWithStyle.append(" - ");
                    sbPrinting.append(" - ");
                }
                if (wrapAlready) {
                    wrapAlready = false;
                }
                sb.append(" - " + ServerConstant.DRUG_DOSAGE_PRN_STRING);
                sbWithStyle.append(ServerConstant.DRUG_DOSAGE_PRN_STRING);
                sbPrinting.append(ServerConstant.DRUG_DOSAGE_PRN_STRING);
                addDashFirst = true;
            }
        } else if (orderLineType.equals(EhrOrderLineTypeEnum.STEP_UP_DOWN)) {
            if (ServerConstant.DRUG_DOSAGE_PRN_YES.equals(dto.getMoeMedMultDoses().get(0).getPrn())) {
                if (addDashFirst && !wrapAlready) {
                    sbWithStyle.append(" - ");
                    sbPrinting.append(" - ");
                }
                if (wrapAlready) {
                    wrapAlready = false;
                }
                sb.append(" - " + ServerConstant.DRUG_DOSAGE_PRN_STRING);
                sbWithStyle.append(ServerConstant.DRUG_DOSAGE_PRN_STRING);
                sbPrinting.append(ServerConstant.DRUG_DOSAGE_PRN_STRING);
                addDashFirst = true;
            }
        }

        if (!isNormalAndAdvance(dto) && isDangerDrug(dto)) {
            sb.append(getDangerDrugQuantityString(dto, 0));
            sbWithStyle.append(getDangerDrugQuantityString(dto, 0));
            sbPrinting.append(getDangerDrugQuantityString(dto, 0));
        }

        // Route
        if (orderLineType != null && orderLineType.equals(ServerConstant.STEP_UP_DOWN)
                && StringUtil.stripToEmpty(dto.getRouteDesc()).length() > 0) {
            if (addDashFirst && !wrapAlready) {
                sb.append(" - ");
                sbWithStyle.append(" - ");
                sbPrinting.append(" - ");
            }
            if (wrapAlready) {
                wrapAlready = false;
            } else {
                sb.append(" ");
            }
            if (dto.getSiteDesc() != null && showSite(dto.getRouteDesc(), dto.getSiteDesc())) {
                sb.append(dto.getRouteDesc() + " (" + dto.getSiteDesc() + ")");
                sbWithStyle.append(dto.getRouteDesc() + " (" + dto.getSiteDesc() + ")");
                sbPrinting.append(dto.getRouteDesc() + " (" + dto.getSiteDesc() + ")");
            } else {
                sb.append(dto.getRouteDesc());
                sbWithStyle.append(dto.getRouteDesc());
                sbPrinting.append(dto.getRouteDesc());
            }

        }

        if (orderLineType.equals(EhrOrderLineTypeEnum.STEP_UP_DOWN)
                && dto.getMoeMedMultDoses().size() > 0
                && dto.getMoeMedMultDoses().get(0).getDuration() != null
                && dto.getMoeMedMultDoses().get(0).getDuration() > 0) {
            MoeMedMultDoseDto dosage = dto.getMoeMedMultDoses().get(0);
            String dosageDurationUnit = toDurationUnitDisplay(dosage.getDurationUnit(), dosage.getDuration());
            if (dosageDurationUnit != null && !trim(dosageDurationUnit).isEmpty()) {

                sb.append(" - for " + formatDosage(new BigDecimal(dosage.getDuration())));
                sb.append(!trim(dosageDurationUnit).isEmpty() ? " " + trim(dosageDurationUnit) : "");
                sb.append(" " + trim(durationUnit));

                sbWithStyle.append(" - for " + formatDosage(new BigDecimal(dosage.getDuration())));
                sbWithStyle.append(!trim(dosageDurationUnit).isEmpty() ? " " + trim(dosageDurationUnit) : "");
                sbWithStyle.append(" " + trim(durationUnit));

                sbPrinting.append(" - for " + formatDosage(new BigDecimal(dosage.getDuration())));
                sbPrinting.append(!trim(dosageDurationUnit).isEmpty() ? " " + trim(dosageDurationUnit) : "");
                sbPrinting.append(" " + trim(durationUnit));
            }
        }

        if (orderLineType.equals(EhrOrderLineTypeEnum.STEP_UP_DOWN)
                && !ServerConstant.ALIAS_NAME_TYPE_FREE_TEXT.equalsIgnoreCase(nameType)
                && dto.getStartDate() != null && !DateUtil.formatDate(DateUtil.clearTime(orderDate)).equals(DateUtil.formatDate(DateUtil.clearTime(dto.getStartDate())))) {
            sb.append(" " + orderLineStartFrom(DateUtil.formatDate(dto.getStartDate(), ServerConstant.DEFAULT_DATE_FORMAT)));
            sbWithStyle.append(" " + orderLineStartFrom(DateUtil.formatDate(dto.getStartDate(), ServerConstant.DEFAULT_DATE_FORMAT)));
            sbPrinting.append(" " + orderLineStartFrom(DateUtil.formatDate(dto.getStartDate(), ServerConstant.DEFAULT_DATE_FORMAT)));

        }

        // Loop Common Dosage
        for (int i = 1; i < dto.getMoeMedMultDoses().size(); i++) {
            boolean addDash = false;
            MoeMedMultDoseDto dosage = dto.getMoeMedMultDoses().get(i);

            String dosageDurationUnit = toDurationUnitDisplay(dosage.getDurationUnit(), dosage.getDuration());

            StringBuffer buffer = new StringBuffer();
            StringBuffer bufferWithStyle = new StringBuffer();
            StringBuffer bufferForPrint = new StringBuffer();

            bufferWithStyle.append("&nbsp;&nbsp;&nbsp;&nbsp;");

            // Dosage
            if (dosage.getDosage() != null) {
                buffer.append(formatDosage(new BigDecimal(dosage.getDosage())));
                if (StringUtil.stripToEmpty(dto.getModu()).length() > 0) {
                    buffer.append(" " + StringUtil.stripToEmpty(dto.getModu()));
                    addDash = true;
                    wrapAlready = false;
                }

                // text with style version
                bufferWithStyle.append("<b>" + formatDosage(new BigDecimal(dosage.getDosage())));
                if (StringUtil.stripToEmpty(dto.getModu()).length() > 0) {
                    bufferWithStyle.append(" " + StringUtil.stripToEmpty(dto.getModu()) + "</b>");
                }
            }

            // freq
            String multipleDoseFreqText = WsFormatter.getMultiDoseFreqText(dto, dosage, i, isSearchMyFavourite);
            if (StringUtils.isNotEmpty(multipleDoseFreqText)) {
                if (buffer.length() != 0) {
                    multipleDoseFreqText = " - " + multipleDoseFreqText;
                }
                addDash = true;
                wrapAlready = false;
            }
            buffer.append(multipleDoseFreqText);
            bufferWithStyle.append(multipleDoseFreqText);

            if (ServerConstant.DRUG_DOSAGE_PRN_YES.equals(dosage.getPrn())
                    && orderLineType.equals(EhrOrderLineTypeEnum.STEP_UP_DOWN)) {
                if (buffer.toString().trim().length() > 0) {
                    buffer.append(" - ");
                    bufferWithStyle.append(" - ");
                }
                buffer.append(ServerConstant.DRUG_DOSAGE_PRN_STRING);
                bufferWithStyle.append(ServerConstant.DRUG_DOSAGE_PRN_STRING);
                addDash = true;
                wrapAlready = false;
            }

            bufferForPrint.append(bufferWithStyle);

            if (isDangerDrug(dto)) {
                buffer.append(getDangerDrugQuantityString(dto, i));
                bufferWithStyle.append(getDangerDrugQuantityString(dto, i));
                bufferForPrint.append(getDangerDrugQuantityString(dto, i));
                addDash = true;
                wrapAlready = false;
            }

            // route if step-up down
            if (orderLineType.equals(EhrOrderLineTypeEnum.STEP_UP_DOWN)
                    && (!trim(dto.getRouteDesc()).isEmpty() || !trim(dto.getSiteDesc()).isEmpty())) {
                if (addDash) {
                    buffer.append(" - ");
                    bufferWithStyle.append(" - ");
                    bufferForPrint.append(" - ");
                } else {
                    buffer.append(" ");
                }
                if (trim(dto.getSiteDesc()).length() > 0 && showSite(dto.getRouteDesc(), dto.getSiteDesc())) {
                    buffer.append(trim(dto.getRouteDesc()) + " (" + trim(dto.getSiteDesc()) + ")");
                    bufferWithStyle.append(trim(dto.getRouteDesc()) + " (" + trim(dto.getSiteDesc()) + ")");
                    bufferForPrint.append(trim(dto.getRouteDesc()) + " (" + trim(dto.getSiteDesc()) + ")");
                } else {
                    buffer.append(trim(dto.getRouteDesc()));
                    bufferWithStyle.append(trim(dto.getRouteDesc()));
                    bufferForPrint.append(trim(dto.getRouteDesc()));
                }
            }

            // duration
            if (orderLineType.equals(EhrOrderLineTypeEnum.STEP_UP_DOWN) && dosage.getDuration() != null && dosage.getDuration() > 0 && !trim(dosageDurationUnit).isEmpty()) {
                if (addDash || (orderLineType.equals(ServerConstant.STEP_UP_DOWN) && dosage.getDuration() != null && trim(dosageDurationUnit).length() > 0)) {
                    buffer.append(" - ");
                    bufferWithStyle.append(" - ");
                    bufferForPrint.append(" - ");
                } else {
                    buffer.append(" ");
                }
                buffer.append("for " + formatDosage(new BigDecimal(dosage.getDuration())));
                buffer.append(trim(dosageDurationUnit).length() > 0 ? " " + trim(dosageDurationUnit) : "");
                bufferWithStyle.append("for " + formatDosage(new BigDecimal(dosage.getDuration())));
                bufferWithStyle.append(trim(dosageDurationUnit).length() > 0 ? " " + trim(dosageDurationUnit) : "");
                bufferForPrint.append("for " + formatDosage(new BigDecimal(dosage.getDuration())));
                bufferForPrint.append(trim(dosageDurationUnit).length() > 0 ? " " + trim(dosageDurationUnit) : "");
            }

            // PRN
            if (ServerConstant.DRUG_DOSAGE_PRN_YES.equals(dto.getPrn())
                    && orderLineType.equals(EhrOrderLineTypeEnum.MULTIPLE_LINE)) {
                if (buffer.toString().trim().length() > 0) {
                    buffer.append(" - ");
                    bufferWithStyle.append(" - ");
                    bufferForPrint.append(" - ");
                }
                buffer.append(ServerConstant.DRUG_DOSAGE_PRN_STRING);
                bufferWithStyle.append(ServerConstant.DRUG_DOSAGE_PRN_STRING);
                bufferForPrint.append(ServerConstant.DRUG_DOSAGE_PRN_STRING);
                addDash = true;
                wrapAlready = false;
            }

            if (wrapAlready) {
                addDashFirst = false;
            }
            sb.append(orderLineType.equals(EhrOrderLineTypeEnum.STEP_UP_DOWN) ? ", " + conjunction : " - " + conjunction);
            sb.append(" ");
            sbWithStyle.append(orderLineType.equals(EhrOrderLineTypeEnum.STEP_UP_DOWN) ? ", " + conjunctionWithStyle : (addDashFirst ? " - " : "") + conjunctionWithStyle);
            sbPrinting.append(orderLineType.equals(EhrOrderLineTypeEnum.STEP_UP_DOWN) ? ", " + conjunctionPrinting : (addDashFirst ? " - " : "") + conjunctionPrinting);

            addDashFirst = addDash;
            sb.append(buffer.toString());
            sbWithStyle.append(bufferWithStyle.toString());
            sbPrinting.append(bufferForPrint.toString());

            addDash = false;
        }


        if (dto.getMoeMedMultDoses().size() == 0) {
            addDashFirst = true;
        }
        // PRN
        if (!orderLineType.equals(EhrOrderLineTypeEnum.MULTIPLE_LINE)
                && ServerConstant.DRUG_DOSAGE_PRN_YES.equals(dto.getPrn())) {
            if (addDashFirst) {
                sb.append(" - ");
                sbWithStyle.append(" - ");
                sbPrinting.append(" - ");
            }
            addDashFirst = true;
            sb.append(ServerConstant.DRUG_DOSAGE_PRN_STRING);
            sbWithStyle.append(ServerConstant.DRUG_DOSAGE_PRN_STRING);
            sbPrinting.append(ServerConstant.DRUG_DOSAGE_PRN_STRING);
        }

        // Route if not step-up down
        if (!orderLineType.equals(EhrOrderLineTypeEnum.STEP_UP_DOWN)
                && !trim(dto.getRouteDesc()).isEmpty()) {
            if (trim(dto.getSiteDesc()).length() > 0 && showSite(dto.getRouteDesc(), dto.getSiteDesc())) {
                if (addDashFirst) {
                    sbWithStyle.append(" - ");
                    sbPrinting.append(" - ");
                }
                addDashFirst = true;
                sb.append(" - " + dto.getRouteDesc() + " (" + dto.getSiteDesc() + ")");
                sbWithStyle.append(dto.getRouteDesc() + " (" + dto.getSiteDesc() + ")");
                sbPrinting.append(dto.getRouteDesc() + " (" + dto.getSiteDesc() + ")");
            } else {
                if (addDashFirst) {
                    sbWithStyle.append(" - ");
                    sbPrinting.append(" - ");
                }
                addDashFirst = true;
                sb.append(" - " + dto.getRouteDesc());
                sbWithStyle.append(dto.getRouteDesc());
                sbPrinting.append(dto.getRouteDesc());
            }
        }

        // Duration if not step-up down
        if ((!orderLineType.equals(EhrOrderLineTypeEnum.STEP_UP_DOWN))
                && dto.getDuration() != null && dto.getDuration() > 0 && !trim(durationUnit).isEmpty()) {
            if (addDashFirst) {
                sb.append(" - ");
                sbWithStyle.append(" - ");
                sbPrinting.append(" - ");
            }
            addDashFirst = true;
            sb.append("for " + formatDosage(new BigDecimal(dto.getDuration())));
            sb.append(" " + trim(durationUnit));
            sbWithStyle.append("for " + formatDosage(new BigDecimal(dto.getDuration())));
            sbWithStyle.append(" " + trim(durationUnit));
            sbPrinting.append("for " + formatDosage(new BigDecimal(dto.getDuration())));
            sbPrinting.append(" " + trim(durationUnit));
        }

        if ((!orderLineType.equals(EhrOrderLineTypeEnum.STEP_UP_DOWN))
                && dto.getStartDate() != null) {
            // Start date to show in order line
            if (dto.getStartDate() != null && !DateUtil.formatDate(DateUtil.clearTime(orderDate)).equals(DateUtil.formatDate(DateUtil.clearTime(dto.getStartDate())))) {
                sb.append(" " + orderLineStartFrom(DateUtil.formatDate(dto.getStartDate(), ServerConstant.DEFAULT_DATE_FORMAT)));
                sbWithStyle.append(" " + orderLineStartFrom(DateUtil.formatDate(dto.getStartDate(), ServerConstant.DEFAULT_DATE_FORMAT)));
                sbPrinting.append(" " + orderLineStartFrom(DateUtil.formatDate(dto.getStartDate(), ServerConstant.DEFAULT_DATE_FORMAT)));
            }
        }

        // Qty
        if (!isDangerDrug(dto)) {
            if (dto.getMoQty() != null && dto.getMoQty() > 0) {
                sb.append(" - " + formatDosage(new BigDecimal(dto.getMoQty())) + " " + trim(dto.getMoQtyUnit()));
                sbWithStyle.append(" - <b>" + formatDosage(new BigDecimal(dto.getMoQty())) + " " + trim(dto.getMoQtyUnit()) + "</b>");
                sbPrinting.append(" - <b>" + formatDosage(new BigDecimal(dto.getMoQty())) + " " + trim(dto.getMoQtyUnit()) + "</b>");
            }
        } else if (isNormalAndAdvance(dto)) {
            String dangerousDrugQty = getDangerDrugQuantityString(dto, 0);
            sb.append(" - " + dangerousDrugQty);
            sbWithStyle.append(" - <b>" + dangerousDrugQty + "</b>");
            sbPrinting.append(" - <b>" + dangerousDrugQty + "</b>");
        }

        // new line for special instruction
        if (addSpecInstToDoseInst && dto.getSpecInstruct() != null && !dto.getSpecInstruct().isEmpty()) {
            sb.append("(" + trim(dto.getSpecInstruct()) + ")");
            sbWithStyle.append("<br>(" + HtmlUtils.htmlEscape(trim(dto.getSpecInstruct())) + ")");
            sbPrinting.append("<br>(" + HtmlUtils.htmlEscape(trim(dto.getSpecInstruct())) + ")");
        }

        // new line for action status
        if (dto.getActionStatus() != null && !ServerConstant.ACTION_TYPE_DISPENSE_IN_PHARMACY.equalsIgnoreCase(dto.getActionStatus())) {
            MoeActionStatusDto actionStatus = getActionStatus(dto.getActionStatus());
            sb.append(" [" + trim(actionStatus.getActionStatus()) + "]");
            sbWithStyle.append("<br> [" + trim(actionStatus.getActionStatus()) + "]");
            sbPrinting.append("<br> [" + trim(actionStatus.getActionStatus()) + "]");
        }

        if (dto.getRemarkText() != null && !dto.getRemarkText().isEmpty()) {
            sbWithStyle.append("<br><i>Note: " + HtmlUtils.htmlEscape(trim(dto.getRemarkText())) + "</i>");
        }

        if (!allMatchTypeH(dto.getMoeEhrMedAllergens())) {
            sbPrinting.append("<br>" + ServerConstant.OVERRIDDEN_MSG);
            sbWithStyle.append("<br>" + ServerConstant.OVERRIDDEN_MSG);
        }

        return new String[]{sb.toString(), sbWithStyle.toString(), sbPrinting.toString()};

    }

    public static String trim(String s) {
        if (s == null) return "";
        return s.trim();
    }

    public static boolean isNormalAndAdvance(MoeMedProfileDto dto) {

        return isNormalAndAdvance(dto.getMoeEhrMedProfile().getOrderLineType(), dto.getSupFreqCode());
    }

    public static boolean isNormalAndAdvance(String orderLineType, String supFreqCode) {
        if (orderLineType.compareTo(EhrOrderLineTypeEnum.MULTIPLE_LINE) != 0 &&
                orderLineType.compareTo(EhrOrderLineTypeEnum.STEP_UP_DOWN) != 0 &&
                (supFreqCode == null || (supFreqCode != null && !supFreqCode.equals(WebServiceConstant.ON_EVEN_ODD)))) {
            return true;
        }
        return false;
    }


    public static String getFreqText(MoeMedProfileDto moeMedProfileDto, boolean isSearchMyFavourite) {

        final StringBuffer sb = new StringBuffer();
        final String suppFreqCode = moeMedProfileDto.getSupFreqCode();
        final String regiment = moeMedProfileDto.getRegimen();
        final String freq = moeMedProfileDto.getFreqCode();
        final Long suppFreq1 = moeMedProfileDto.getSupFreq1();
        final Long suppFreq2 = moeMedProfileDto.getSupFreq2();

        // no supp freq
        if (trim(moeMedProfileDto.getFreqText()).length() == 0) {
            return "";
        } else if (suppFreqCode == null || regiment == null) {
            if (isSearchMyFavourite) {
                String freqText = trim(moeMedProfileDto.getFreqCode());
                if (freqText.contains(WebServiceConstant.BLANKS_IND))
                    freqText = freqText.replaceAll(WebServiceConstant.BLANKS_IND, " " + moeMedProfileDto.getFreq1().toString() + " ");
                sb.append(freqText);
            } else {
                sb.append(trim(moeMedProfileDto.getFreqText()));
            }
            return sb.toString();
        } else {
            if (isSearchMyFavourite) {
                String freqText = trim(moeMedProfileDto.getFreqCode());
                if (freqText.contains(WebServiceConstant.BLANKS_IND))
                    freqText = freqText.replaceAll(WebServiceConstant.BLANKS_IND, " " + moeMedProfileDto.getFreq1().toString() + " ");
                sb.append(freqText);
            } else {
                sb.append(trim(moeMedProfileDto.getFreqText()));
            }
        }

        // have supp freq
        HashMap<String, List<MoeSupplFreqDto>> map = DtoUtil.REGIMEN_TYPE_MAP;
        if (map.get(regiment) == null) return sb.toString();

        List<MoeSupplFreqDto> suppFreqDTOList = map.get(regiment);
        MoeSupplFreqDto moeSupplFreqDto = null;
        for (MoeSupplFreqDto dto : suppFreqDTOList) {
            if (dto != null && suppFreqCode.equals(dto.getSupplFreqEng())) {
                moeSupplFreqDto = dto;
                break;
            }
        }
        if (moeSupplFreqDto == null || moeSupplFreqDto.getMoeSupplFreqDescs() == null) {
            return sb.toString();
        }

        List<MoeSupplFreqDescDto> moeSupplFreqDescDTOList = moeSupplFreqDto.getMoeSupplFreqDescs();
        List<MoeSupplFreqDescDto> targetMoeSupplFreqDescDTOList = new ArrayList<MoeSupplFreqDescDto>();
        List<MoeSupplFreqDescDto> defaultSameFreqMoeSupplFreqDescDTOList = new ArrayList<MoeSupplFreqDescDto>();
        List<MoeSupplFreqDescDto> defaultMoeSupplFreqDescDTOList = new ArrayList<MoeSupplFreqDescDto>();
        // Simon 20191203 for sonarqube start--
        //MoeSupplFreqDescDto targetMoeSupplFreqDescDTO = null;
        MoeSupplFreqDescDto targetMoeSupplFreqDescDTO = new MoeSupplFreqDescDto();
        //Simon 20191203 end--

        if (moeSupplFreqDto.getMoeSupplFreqSelections().size() == 1) {
            for (MoeSupplFreqDescDto dto : moeSupplFreqDescDTOList) {
                if (trim(freq).equals(trim(dto.getFreqCode()))) {
                    if (nullToZero(suppFreq1).equals(dto.getSupplFreq1())) {
                        targetMoeSupplFreqDescDTOList.add(dto);
                    } else if (dto.getSupplFreq1() == null) {
                        defaultSameFreqMoeSupplFreqDescDTOList.add(dto);
                    }
                } else if (dto.getFreqCode() == null && nullToZero(suppFreq1).equals(dto.getSupplFreq1())) {
                    targetMoeSupplFreqDescDTOList.add(dto);
                } else if (dto.getFreqCode() == null && dto.getSupplFreq1() == null) {
                    defaultMoeSupplFreqDescDTOList.add(dto);
                }
            }
        }

        // monday case...
        if (moeSupplFreqDto.getMoeSupplFreqSelections().size() > 1) {
            if (trim(moeMedProfileDto.getDayOfWeek()).length() == 7) {
                targetMoeSupplFreqDescDTO = convertMoeSupplFreqDescDTOByDayWeekText(moeMedProfileDto, moeSupplFreqDto);
            }
        }

        // default case
        if (targetMoeSupplFreqDescDTOList.size() > 0) {
            return WsFormatter.toSuppFreqText(sb.toString(), moeSupplFreqDto, targetMoeSupplFreqDescDTOList.get(0), suppFreq1, suppFreq2, moeMedProfileDto.getMoeEhrMedProfile().getCycleMultiplier());
        }
        if (defaultSameFreqMoeSupplFreqDescDTOList.size() > 0) {
            targetMoeSupplFreqDescDTO = defaultSameFreqMoeSupplFreqDescDTOList.get(0);
        } else if (defaultMoeSupplFreqDescDTOList.size() > 0) {
            targetMoeSupplFreqDescDTO = defaultMoeSupplFreqDescDTOList.get(0);
        }

        return WsFormatter.toSuppFreqText(sb.toString(), moeSupplFreqDto, targetMoeSupplFreqDescDTO, suppFreq1, suppFreq2, moeMedProfileDto.getMoeEhrMedProfile().getCycleMultiplier());
    }

    public static String orderLineStartFrom(String s) {
        return "(Start from: " + s + ")";
    }

    public static boolean isDangerDrug(MoeMedProfileDto dto) {
        return "Y".equalsIgnoreCase(dto.getDangerdrug());
    }

    public static String getDangerDrugQuantityString(MoeMedProfileDto dto, int rowIndex) {
        if (isNormalAndAdvance(dto)) {
            if (dto.getMoQty() != null) {
                return "(prescribe " + formatDosage(new BigDecimal(dto.getMoQty())) + " " + dto.getMoQtyUnit() + ")";
            }
        } else {
            MoeMedMultDoseDto dose = dto.getMoeMedMultDoses().get(rowIndex);
            if (dose.getMoQty() != null) {
                return " - (prescribe " + formatDosage(new BigDecimal(dose.getMoQty())) + " " + dto.getMoQtyUnit() + ")";
            }
        }
        return "";
    }

    public static MoeActionStatusDto getActionStatus(String actionStatus) {

        for (MoeActionStatusDto dto : DtoUtil.ACTIONS_STATUS_LIST) {
            if (dto.getActionStatusType().equalsIgnoreCase(actionStatus)) {
                dto.setActionStatus(dto.getActionStatus());
                return dto;
            }
        }
        return null;
    }

    public static Long nullToZero(Long i) {
        if (i == null) {
            return 0l;
        } else {
            return i;
        }
    }

    private static MoeSupplFreqDescDto convertMoeSupplFreqDescDTOByDayWeekText(MoeMedProfileDto moeMedProfilesDTO, MoeSupplFreqDto moeSupplFreqDto) {
        final MoeSupplFreqDescDto result = new MoeSupplFreqDescDto();
        final List<MoeSupplFreqDescDto> dayWeekList = new ArrayList<MoeSupplFreqDescDto>();
        final String dayOfWeek = trim(moeMedProfilesDTO.getDayOfWeek());
        Map<String, MoeSupplFreqDescDto> descMap = new HashMap<String, MoeSupplFreqDescDto>();
        if (dayOfWeek.length() != 7) {
            return result;
        }

        for (MoeSupplFreqDescDto dto : moeSupplFreqDto.getMoeSupplFreqDescs()) {
            final String key = trim(dto.getFreqCode()) + "|" + nullToZero(dto.getSupplFreq1()) + "|" + nullToZero(dto.getSupplFreqSelectionValue());
            descMap.put(key, dto);
        }

        for (int i = 0; i < dayOfWeek.length(); i++) {
            if (dayOfWeek.charAt(i) == '1') {
                String targetKey = trim(moeMedProfilesDTO.getFreqCode()) + "|" + nullToZero(moeMedProfilesDTO.getSupFreq1()) + "|" + nullToZero((long) (i + 1));
                if (!descMap.containsKey(targetKey)) {
                    targetKey = trim(null) + "|" + nullToZero(null) + "|" + nullToZero((long) i + 1);
                }

                if (descMap.get(targetKey) != null) {
                    dayWeekList.add(descMap.get(targetKey));
                }
            }
        }

        // create a new SupplFreqDescDTO for the selected day of week value
        if (dayWeekList.size() > 0) {
            result.setFreqCode(dayWeekList.get(0).getFreqCode());
            result.setSupplFreq1(dayWeekList.get(0).getSupplFreq1());
            result.setDisplayWithFreq(dayWeekList.get(0).getDisplayWithFreq());
            String desc = "";
            for (MoeSupplFreqDescDto dto : dayWeekList) {
                desc += ", " + dto.getSupplFreqDescEng();
            }
            result.setSupplFreqDescEng(desc.replaceFirst(", ", ""));
        }

        return result;
    }

    private static String toSuppFreqText(String freqText, MoeSupplFreqDto moeSupplFreqDto, MoeSupplFreqDescDto targetMoeSupplFreqDescDTO, Long suppFreq1, Long suppFreq2, Long cycleMultiplier) {

        StringBuffer sb = null;

        //replace __ to value
        String suppFreqText = targetMoeSupplFreqDescDTO.getSupplFreqDescEng();
        if (targetMoeSupplFreqDescDTO.getSupplFreqDescEng().indexOf(WebServiceConstant.BLANKS_IND) > 0) {
            suppFreqText = targetMoeSupplFreqDescDTO.getSupplFreqDescEng();
            suppFreqText = (suppFreq1 == null) ? suppFreqText : suppFreqText.replaceFirst(WebServiceConstant.BLANKS_IND, " " + Long.toString(suppFreq1) + " ");
            suppFreqText = (suppFreq2 == null) ? suppFreqText : suppFreqText.replaceFirst(WebServiceConstant.BLANKS_IND, " " + Long.toString(suppFreq2) + " ");
            suppFreqText = (cycleMultiplier == null) ? suppFreqText : suppFreqText.replaceFirst(WebServiceConstant.BLANKS_IND, " " + Long.toString(cycleMultiplier) + " ");
        }

        if (targetMoeSupplFreqDescDTO.getDisplayWithFreq().equalsIgnoreCase("Y")) {
            sb = new StringBuffer(freqText);
            sb.append(" (");
            sb.append(trim(moeSupplFreqDto.getPrefixSupplFreqEng()).length() > 0 ? trim(moeSupplFreqDto.getPrefixSupplFreqEng()) + " " : "");
            sb.append(trim(suppFreqText) + ")");
        } else {
            sb = new StringBuffer();
            sb.append(trim(moeSupplFreqDto.getPrefixSupplFreqEng()).length() > 0 ? trim(moeSupplFreqDto.getPrefixSupplFreqEng()) + " " : "");
            sb.append(trim(suppFreqText));
        }

        return sb.toString();
    }

    public static String getMultiDoseFreqText(final MoeMedProfileDto moeMedProfileDto, final MoeMedMultDoseDto moeMedMultDoseDto, int multiDosePos, boolean isSearchMyFavourite) {
        final StringBuffer sb = new StringBuffer();

        final String suppFreqCode = moeMedMultDoseDto.getSupFreqCode();
        final String regiment = moeMedProfileDto.getRegimen();
        final String freq = moeMedMultDoseDto.getFreqCode();
        final Long suppFreq1 = moeMedMultDoseDto.getSupFreq1();
        final Long suppFreq2 = moeMedMultDoseDto.getSupFreq2();

        // no supp freq
        if (trim(moeMedMultDoseDto.getFreqText()).length() == 0) {
            return "";
        } else if (suppFreqCode == null || regiment == null) {
            if (isSearchMyFavourite) {
                String freqText = trim(moeMedMultDoseDto.getFreqCode());
                if (freqText.contains(WebServiceConstant.BLANKS_IND))
                    freqText = freqText.replaceAll(WebServiceConstant.BLANKS_IND, " " + moeMedMultDoseDto.getFreq1().toString() + " ");
                sb.append(freqText);
            } else {
                sb.append(trim(moeMedMultDoseDto.getFreqText()));
            }
            return sb.toString();
        } else {
            if (isSearchMyFavourite) {
                String freqText = trim(moeMedMultDoseDto.getFreqCode());
                if (freqText.contains(WebServiceConstant.BLANKS_IND))
                    freqText = freqText.replaceAll(WebServiceConstant.BLANKS_IND, " " + moeMedMultDoseDto.getFreq1().toString() + " ");
                sb.append(freqText);
            } else {
                sb.append(trim(moeMedMultDoseDto.getFreqText()));
            }
        }

        // have supp freq
        HashMap<String, List<MoeSupplFreqDto>> map = DtoUtil.REGIMEN_TYPE_MAP;
        if (map.get(regiment) == null) return sb.toString();

        List<MoeSupplFreqDto> suppFreqDTOList = map.get(regiment);
        MoeSupplFreqDto moeSupplFreqDto = null;
        for (MoeSupplFreqDto dto : suppFreqDTOList) {
            if (dto != null && suppFreqCode.equals(dto.getSupplFreqEng())) {
                moeSupplFreqDto = dto;
                break;
            }
        }
        if (moeSupplFreqDto == null || moeSupplFreqDto.getMoeSupplFreqDescs() == null) {
            return sb.toString();
        }

        List<MoeSupplFreqDescDto> moeSupplFreqDescDTOList = moeSupplFreqDto.getMoeSupplFreqDescs();
        List<MoeSupplFreqDescDto> targetMoeSupplFreqDescDTOList = new ArrayList<MoeSupplFreqDescDto>();
        List<MoeSupplFreqDescDto> defaultSameFreqMoeSupplFreqDescDTOList = new ArrayList<MoeSupplFreqDescDto>();
        List<MoeSupplFreqDescDto> defaultMoeSupplFreqDescDTOList = new ArrayList<MoeSupplFreqDescDto>();
        // Simon 20191203 start--
        //MoeSupplFreqDescDto targetMoeSupplFreqDescDTO = null;
        MoeSupplFreqDescDto targetMoeSupplFreqDescDTO = new MoeSupplFreqDescDto();
        //Simon 20191203 end--

        if (moeSupplFreqDto.getMoeSupplFreqSelections().size() == 1) {
            for (MoeSupplFreqDescDto dto : moeSupplFreqDescDTOList) {
                if (trim(freq).equals(trim(dto.getFreqCode()))) {
                    if (nullToZero(suppFreq1).equals(dto.getSupplFreq1())) {
                        targetMoeSupplFreqDescDTOList.add(dto);
                    } else if (dto.getSupplFreq1() == null) {
                        defaultSameFreqMoeSupplFreqDescDTOList.add(dto);
                    }
                } else if (dto.getFreqCode() == null && dto.getSupplFreq1() == null) {
                    defaultMoeSupplFreqDescDTOList.add(dto);
                }
            }
        }

        // monday case...
        if (moeSupplFreqDto.getMoeSupplFreqSelections().size() > 1) {
            if (trim(moeMedProfileDto.getDayOfWeek()).length() == 7) {
                targetMoeSupplFreqDescDTO = convertMoeSupplFreqDescDTOByDayWeekText(moeMedProfileDto, moeSupplFreqDto);
            }
        }

        // default case
        if (targetMoeSupplFreqDescDTOList.size() > 1) {
            return WsFormatter.toSuppFreqText(sb.toString(), moeSupplFreqDto, targetMoeSupplFreqDescDTOList.get(1), suppFreq1, suppFreq2, moeMedProfileDto.getMoeEhrMedProfile().getCycleMultiplier());
        }
        if (defaultSameFreqMoeSupplFreqDescDTOList.size() > 1) {
            targetMoeSupplFreqDescDTO = defaultSameFreqMoeSupplFreqDescDTOList.get(multiDosePos);
        } else if (defaultMoeSupplFreqDescDTOList.size() > 1) {
            targetMoeSupplFreqDescDTO = defaultMoeSupplFreqDescDTOList.get(multiDosePos);
        }

        return WsFormatter.toSuppFreqText(sb.toString(), moeSupplFreqDto, targetMoeSupplFreqDescDTO, suppFreq1, suppFreq2, moeMedProfileDto.getMoeEhrMedProfile().getCycleMultiplier());

    }

    public static boolean showSite(String route, String site) {
        HashMap<String, String> map = DtoUtil.ROUTE_SITE_MAP;
        String showSite = map.get(route + " " + site);
        if (showSite != null) {
            return "y".equalsIgnoreCase(map.get(route + " " + site));
        } else {
            return true;
        }
    }

    public static boolean allMatchTypeH(List<MoeEhrMedAllergenDto> allergens) {
        if (allergens != null) {
            for (MoeEhrMedAllergenDto allergen : allergens) {
                if (!allergen.getMatchType().equals("H")) {
                    return false;
                }
            }
        }
        return true;
    }
}
