/*********************************************************************
* Copyright (c) eHR HK Limited 2013. 
* All Rights Reserved.
* 
*********************************************************************
* PROJECT NAME    :  eHR Adaptation
*
* MODULE NAME     :  Medication Order Entry (MOE)
*
* PROGRAM NAME    :  MoePrescriptionComparator.java
*
* PURPOSE         :  Comparator for DTOs
*
**********************************************************************
* VERSION         :   1.01.001
* REF NO          :  
* DATE CREATED    :   7 June 2013
* 
*********************************************************************
*
* CHANGES         :
* REF NO           DATE        DETAIL
* ------           ----        ------
*
*********************************************************************
*/
package hk.health.moe.util.comparator;

import hk.health.moe.pojo.dto.MoeActionStatusDto;
import hk.health.moe.pojo.po.MoeMedProfilePo;
import hk.health.moe.util.DtoUtil;

import java.util.Comparator;

public class MoePrescriptionComparator implements
		Comparator<MoeMedProfilePo> {

	@Override
	public int compare(MoeMedProfilePo arg0, MoeMedProfilePo arg1) {
		MoeActionStatusDto dtoArg0 = (MoeActionStatusDto) DtoUtil.ACTIONS_STATUS_MAP.get(arg0.getActionStatus());
		MoeActionStatusDto dtoArg1 = (MoeActionStatusDto)DtoUtil.ACTIONS_STATUS_MAP.get(arg1.getActionStatus());
		Long rank0 = dtoArg0==null?0:dtoArg0.getRank();
		Long rank1 = dtoArg1==null?0:dtoArg1.getRank();
		Long value0 = arg0.getOrgItemNo();
		Long value1 = arg1.getOrgItemNo();
		
		if (rank0.equals(rank1)) {
			return value0.compareTo(value1);
		} else {
			return rank0.compareTo(rank1);
		}
	}
	
}
