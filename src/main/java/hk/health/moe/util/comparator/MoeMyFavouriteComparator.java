/*********************************************************************
* Copyright (c) eHR HK Limited 2013. 
* All Rights Reserved.
* 
*********************************************************************
* PROJECT NAME    :  eHR Adaptation
*
* MODULE NAME     :  Medication Order Entry (MOE)
*
* PROGRAM NAME    :  MoeMyFavouriteComparator.java
*
* PURPOSE         :  Comparator for DTOs
*
**********************************************************************
* VERSION         :   1.01.001
* REF NO          :  
* DATE CREATED    :   7 June 2013
* 
*********************************************************************
*
* CHANGES         :
* REF NO           DATE        DETAIL
* ------           ----        ------
*
*********************************************************************
*/
package hk.health.moe.util.comparator;


import hk.health.moe.pojo.dto.MoeMedProfileDto;
import hk.health.moe.pojo.dto.MoeMyFavouriteHdrDto;
import hk.health.moe.util.DtoUtil;
import org.springframework.stereotype.Component;

import java.util.Comparator;

@Component
public class MoeMyFavouriteComparator implements Comparator<MoeMyFavouriteHdrDto> {

	@Override
	public int compare(MoeMyFavouriteHdrDto o1, MoeMyFavouriteHdrDto o2) {
		StringBuilder sb1 = new StringBuilder();
		StringBuilder sb2 = new StringBuilder();
		
		if(o1.getMyFavouriteName() != null) {
			sb1.append(o1.getMyFavouriteName());
		} else {
			sb1.append(getDrugName(o1.getMoeMedProfiles().get(0)));
		}
		
		if(o2.getMyFavouriteName() != null) {
			sb2.append(o2.getMyFavouriteName());
		} else {
			sb2.append(getDrugName(o2.getMoeMedProfiles().get(0)));
		}
		
		return sb1.toString().toLowerCase().compareTo(sb2.toString().toLowerCase());
	}
	
	private String getDrugName(MoeMedProfileDto dto) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(dto.getMoeEhrMedProfile().getScreenDisplay());
		
		// Route
		if(dto.getFormDesc() != null && dto.getRouteDesc() != null) {
			String key = dto.getFormDesc()+" "+dto.getRouteDesc();
			if(DtoUtil.FORM_ROUTE_MAP.get(key) == null || DtoUtil.FORM_ROUTE_MAP.get(key).toCharArray()[0] == 'Y') {
				sb.append(" " + dto.getRouteDesc());
			}
		}
		
		// Form
		if(dto.getFormDesc() != null) {
			sb.append(" " + dto.getFormDesc());
			
			if(dto.getMoeEhrMedProfile().getDoseFormExtraInfo() != null) {
				sb.append(" " + dto.getMoeEhrMedProfile().getDoseFormExtraInfo());
			}
		}
		
		return sb.toString();
	}
}
