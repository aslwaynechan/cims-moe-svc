package hk.health.moe.util.comparator;


import hk.health.moe.common.MoeCommonHelper;
import hk.health.moe.pojo.dto.MoeCommonDosageDto;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.util.DtoUtil;
import hk.health.moe.util.StringUtil;
import hk.health.moe.util.DrugSearchEngine;

import java.util.Comparator;

public class MoeDrugDtoComparator implements Comparator<MoeDrugDto> {

    private DrugSearchEngine.DrugSearchType type;

    public MoeDrugDtoComparator() {
    }

    public MoeDrugDtoComparator(DrugSearchEngine.DrugSearchType type) {
        this.type = type;
    }

    @Override
    public int compare(MoeDrugDto o1, MoeDrugDto o2) {
        boolean o1Multi = MoeDrugDto.isMultiIngr(o1.getVtm());
        boolean o2Multi = MoeDrugDto.isMultiIngr(o2.getVtm());
        if (o1Multi && !o2Multi) {
            return 1;
        } else if (!o1Multi && o2Multi) {
            return -1;
        }

        if (o1.hasCommonDosage() && !o2.hasCommonDosage())
            return 1;
        else if (!o1.hasCommonDosage() && o2.hasCommonDosage())
            return -1;
        else if (!o1.hasCommonDosage() && !o2.hasCommonDosage()) {
            if ((o1Multi && o2Multi) || DrugSearchEngine.DrugSearchType.NONMTT.equals(type)) {
                return compareMultiIngr(o1, o2);
            }
            return compareByType(o1, o2);
        } else {
            int value = 0;
            if ((o1Multi && o2Multi) || DrugSearchEngine.DrugSearchType.NONMTT.equals(type)) {
                value = compareMultiIngr(o1, o2);
            } else {
                value = compareByType(o1, o2);
            }
            if (value == 0) {
                MoeCommonDosageDto o1CommonDosage = o1.getCommonDosages().get(0);
                MoeCommonDosageDto o2CommonDosage = o2.getCommonDosages().get(0);
                // Ricci 20190823 moe_drug_server start --
                double o1Dosage = o1CommonDosage.getDosage() != null ? o1CommonDosage.getDosage().doubleValue() : 0;
                double o2Dosage = o2CommonDosage.getDosage() != null ? o2CommonDosage.getDosage().doubleValue() : 0;
                // Ricci 20190823 moe_drug_server end --
                if (o1Dosage == o2Dosage) {
                    String o1Freq = o1CommonDosage.getFreq().getFreqEng();
                    String o2Freq = o2CommonDosage.getFreq().getFreqEng();
                    if (StringUtil.stripToEmpty(o1Freq).equalsIgnoreCase(StringUtil.stripToEmpty(o2Freq))) {
                        return o1.getDisplayString().toLowerCase().compareTo(o2.getDisplayString().toLowerCase());
                    } else {
                        Integer thisvalue = DtoUtil.FREQ_ORDER.get(o1Freq);
                        Integer ovalue = DtoUtil.FREQ_ORDER.get(o2Freq);
                        if (thisvalue == null) thisvalue = 0;
                        if (ovalue == null) ovalue = 0;
                        return thisvalue.compareTo(ovalue);
                    }
                } else {
                    return compareValue(o1Dosage, o2Dosage);
                }
            }
            return value;
        }
    }

    private int compareValue(double thisvalue, double ovalue) {
        if (thisvalue < ovalue) return -1;
        else if (thisvalue > ovalue) return 1;
        else return 0;
    }

    // VTM -> route -> form -> strength -> TN
    private int compareByType(MoeDrugDto o1, MoeDrugDto o2) {
        String s1 = (MoeCommonHelper.isAliasNameTypeVtm(o1.getDisplayNameType()) || MoeCommonHelper.isAliasNameTypeTradeNameAlias(o1.getDisplayNameType()) ? o1.getVtm() : o1.getAliasNames().get(0).getAliasName());
        String s2 = (MoeCommonHelper.isAliasNameTypeVtm(o2.getDisplayNameType()) || MoeCommonHelper.isAliasNameTypeTradeNameAlias(o2.getDisplayNameType()) ? o2.getVtm() : o2.getAliasNames().get(0).getAliasName());

        int diff = s1.toLowerCase().compareTo(s2.toLowerCase());
        if (diff == 0) {
            diff = compareByDrugAttribute(o1, o2);
        }
        if (diff == 0) {
            diff = o1.getTradeName().toLowerCase().compareTo(o2.getTradeName().toLowerCase());
        }

        return diff;
    }

    // order by route, form, strength
    private int compareByDrugAttribute(MoeDrugDto o1, MoeDrugDto o2) {
        StringBuilder rfs1 = new StringBuilder();
        StringBuilder rfs2 = new StringBuilder();

        // route
        String key = o1.getFormEng() + " " + o1.getRouteEng();
        if (DtoUtil.FORM_ROUTE_MAP.get(key) == null || DtoUtil.FORM_ROUTE_MAP.get(key).equals("Y")) {

            rfs1.append(o1.getRouteEng() + " ");
        }
        key = o2.getFormEng() + " " + o2.getRouteEng();
        if (DtoUtil.FORM_ROUTE_MAP.get(key) == null || DtoUtil.FORM_ROUTE_MAP.get(key).equals("Y")) {
            rfs2.append(o2.getRouteEng() + " ");
        }

        // form
        rfs1.append(StringUtil.stripToEmpty(o1.getFormEng()) + " ");
        if (o1.getDoseFormExtraInfo() != null) {
            rfs1.append(o1.getDoseFormExtraInfo() + " ");
        }
        rfs2.append(StringUtil.stripToEmpty(o2.getFormEng()) + " ");
        if (o2.getDoseFormExtraInfo() != null) {
            rfs2.append(o2.getDoseFormExtraInfo() + " ");
        }

        // strength
        if (MoeCommonHelper.isFlagTrue(o1.getStrengthCompulsory()) &&
                o1.getStrengths() != null && o1.getStrengths().size() == 1) {
            rfs1.append(StringUtil.stripToEmpty(o1.getStrengths().get(0).getStrength()));

            if (o1.getStrengths().get(0).getStrengthLevelExtraInfo() != null) {
                rfs1.append(" " + o1.getStrengths().get(0).getStrengthLevelExtraInfo());
            }
        }
        if (MoeCommonHelper.isFlagTrue(o2.getStrengthCompulsory()) &&
                o2.getStrengths() != null && o2.getStrengths().size() == 1) {
            rfs2.append(StringUtil.stripToEmpty(o2.getStrengths().get(0).getStrength()));

            if (o2.getStrengths().get(0).getStrengthLevelExtraInfo() != null) {
                rfs2.append(" " + o2.getStrengths().get(0).getStrengthLevelExtraInfo());
            }
        }

        return rfs1.toString().toLowerCase().compareTo(rfs2.toString().toLowerCase());
    }

    private int compareMultiIngr(MoeDrugDto o1, MoeDrugDto o2) {
        String amp1 = o1.getStrengths().get(0).getAmp();
        String amp2 = o2.getStrengths().get(0).getAmp();
        return StringUtil.stripToEmpty(amp1).toLowerCase().compareTo(StringUtil.stripToEmpty(amp2).toLowerCase());
    }
}
