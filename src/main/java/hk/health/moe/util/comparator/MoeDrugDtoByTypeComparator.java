package hk.health.moe.util.comparator;


import hk.health.moe.common.MoeCommonHelper;
import hk.health.moe.pojo.dto.MoeDrugDto;

import java.util.Comparator;

public class MoeDrugDtoByTypeComparator implements Comparator<MoeDrugDto> {

    public MoeDrugDtoByTypeComparator() {
    }

    @Override
    public int compare(MoeDrugDto o1, MoeDrugDto o2) {

        if (MoeCommonHelper.isAliasNameTypeTradeNameAlias(o1.getDisplayNameType())
                && !MoeCommonHelper.isAliasNameTypeTradeNameAlias(o2.getDisplayNameType())) {
            return -1;
        } else if (!MoeCommonHelper.isAliasNameTypeTradeNameAlias(o1.getDisplayNameType())
                && MoeCommonHelper.isAliasNameTypeTradeNameAlias(o2.getDisplayNameType())) {
            return 1;
        } else {
            if (MoeCommonHelper.isAliasNameTypeTradeNameAlias(o1.getDisplayNameType())
                    && MoeCommonHelper.isAliasNameTypeTradeNameAlias(o2.getDisplayNameType())) {
                return o1.getTradeNameAlias().toLowerCase().compareTo(o2.getTradeNameAlias().toLowerCase());
            }
            return 0;
        }

    }
}
