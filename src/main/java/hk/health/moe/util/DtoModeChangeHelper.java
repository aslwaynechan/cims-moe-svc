package hk.health.moe.util;

import hk.health.moe.common.EhrOrderLineTypeEnum;
import hk.health.moe.common.ModuleConstant;
import hk.health.moe.pojo.dto.MoeMedProfileDto;
import hk.health.moe.pojo.dto.inner.InnerMoeMedProfileDto;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/7/31
 */
public class DtoModeChangeHelper {

    public static boolean isNormalAndAdvance(InnerMoeMedProfileDto dto) {
        String orderLineType = dto.getMoeEhrMedProfile().getOrderLineType();
        String supFreqCode = dto.getSupFreqCode();
        if (!orderLineType.equals(EhrOrderLineTypeEnum.MULTIPLE_LINE) &&
                !orderLineType.equals(EhrOrderLineTypeEnum.STEP_UP_DOWN) &&
                (supFreqCode == null || (supFreqCode != null && !supFreqCode.equals(ModuleConstant.ON_EVEN_ODD)))) {
            return true;
        }
        return false;
    }

    public static boolean isStepUpDown(InnerMoeMedProfileDto dto) {
        return dto.getMoeEhrMedProfile().getOrderLineType().equals(EhrOrderLineTypeEnum.STEP_UP_DOWN);
    }

    public static boolean isMultiLine(InnerMoeMedProfileDto dto) {
        return dto.getMoeEhrMedProfile().getOrderLineType().equals(EhrOrderLineTypeEnum.MULTIPLE_LINE);
    }

    public static boolean isRecurrent(InnerMoeMedProfileDto dto) {
        return dto.getMoeEhrMedProfile().getOrderLineType().equals(EhrOrderLineTypeEnum.RECURRENT);
    }

    //Chris 20191009 From GWT:EhrMoeModule DtoModeChangeHelper  -Start
    public static boolean isStepUpDown(MoeMedProfileDto dto){
        return dto.getMoeEhrMedProfile().getOrderLineType().equals(EhrOrderLineTypeEnum.STEP_UP_DOWN);
    }

    public static boolean isMultiLine(MoeMedProfileDto dto){
        return dto.getMoeEhrMedProfile().getOrderLineType().equals(EhrOrderLineTypeEnum.MULTIPLE_LINE);
    }

    public static boolean isRecurrent(MoeMedProfileDto dto){
        return dto.getMoeEhrMedProfile().getOrderLineType().equals(EhrOrderLineTypeEnum.RECURRENT);
    }
    //Chris 20191009 From GWT:EhrMoeModule DtoModeChangeHelper  -End

}
