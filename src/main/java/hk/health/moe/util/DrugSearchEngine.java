package hk.health.moe.util;

import hk.health.moe.common.DrugType;
import hk.health.moe.common.MoeCommonHelper;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.MoeDrugStrengthDto;
import hk.health.moe.pojo.dto.MoeUserSettingDto;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.service.impl.MoeContentServiceImpl;
import hk.health.moe.util.comparator.MoeDrugDtoByTypeComparator;
import hk.health.moe.util.comparator.MoeDrugDtoComparator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DrugSearchEngine {

    public final static Logger logger = LoggerFactory.getLogger(DrugSearchEngine.class);
    public static final HashMap<DrugSearchType, Comparator<MoeDrugDto>> COMPARATOR_BY_TYPE = new HashMap<DrugSearchType, Comparator<MoeDrugDto>>();

    private UserDto userDto;

    private Set<String> matchedVTM = new HashSet<String>();
    private Set<String> matchedParent = new HashSet<String>();
    private Set<String> matchedDosage = new HashSet<String>();
    private Set<String> matchedTypeDrug = new HashSet<String>();
    private int count = 0;

    private String keyword;
    private int limit;
    private boolean showDosage;
    private String dosageType;
    //Chris Add get from Redis flag 20191127  -Start
    //From MOE_SYSTEM_SETTING table : enable_moe_redis / enable_dept_fav_redis
    private boolean isRedis;
    //Chris Add get from Redis flag 20191127  -End
    private List<String> hkregs;

    //Chris Get Data of Drug Search From Redis  -Start
    private Map<String, Map<String, List<MoeDrugDto>>> drugsRedisResult = new HashMap<>();
    //Chris Get Data of Drug Search From Redis  -End
    //Chris Get Data of Drug Search From Redis By BAN type  -Start
    private Map<String, Map<String, List<MoeDrugDto>>> banDrugsRedisResult = new HashMap<>();
    //Chris Get Data of Drug Search From Redis By BAN type  -End

    private HashMap<String, HashMap<String, List<MoeDrugDto>>> matchedDrugs = new HashMap<String, HashMap<String, List<MoeDrugDto>>>();


    static {
        for (DrugSearchType type : DrugSearchType.values()) {
            COMPARATOR_BY_TYPE.put(type, new MoeDrugDtoComparator(type));
        }
    }

    private enum DrugCat {
        DRUGS_STARTS_WITH, DRUGS_CONTAINS_WITH, DOSAGE_DRUGS_STARTS_WITH, DOSAGE_DRUGS_CONTAINS_WITH;
    }

    public enum DrugSearchType {
        TRADENAME(DrugType.TRADENAME),
        GENERIC(DrugType.GENERIC),
        ASSORTED("ASSORTED"),
        NONMTT(DrugType.NONMTT),
        MULTIINGR(DrugType.MULTIINGR);

        public final String s;

        DrugSearchType(String s) {
            this.s = s;
        }

        DrugSearchType(DrugType t) {
            this.s = t.toString();
        }

        public String toString() {
            return s;
        }
    }


    public DrugSearchEngine(String keyword, boolean showDosage, String dosageType, int limit) {
        this.keyword = keyword;
        this.limit = limit;
        this.showDosage = showDosage;
        this.dosageType = dosageType;
    }

    public DrugSearchEngine(String keyword, boolean showDosage, String dosageType, Integer limit, UserDto userDto) {
        this(keyword, showDosage, dosageType, limit);
        this.userDto = userDto;
        this.hkregs = DtoUtil.HKREG_BY_SPECIALTY.get(userDto.getHospitalCd() + " " + userDto.getSpec());
    }

    //Chris Add get from Redis flag 20191127  -Start
    public DrugSearchEngine(String keyword, boolean showDosage, String dosageType, Integer limit, UserDto userDto, Boolean isRedis) {
        this(keyword, showDosage, dosageType, limit, userDto);
        this.isRedis = isRedis;
    }
    //Chris Add get from Redis flag 20191127  -End

    private void init() {
        for (DrugCat cat : DrugCat.values()) {
            matchedDrugs.put(cat.toString(), new HashMap<String, List<MoeDrugDto>>());
        }
    }

    public List<MoeDrugDto> process() {
        init();

        // get setting of enable search with common dosage
        boolean enableCommonDosage = false;
        MoeUserSettingDto setting = userDto.getSetting().get(ServerConstant.PARAM_NAME_ENABLE_COMMON_DOSAGE);
        if (setting == null || !"N".equalsIgnoreCase(setting.getParamValue()))
            enableCommonDosage = true;


        if (isRedis) {
            //Get target drugs data from Redis
            MoeContentServiceImpl moeContentService = new MoeContentServiceImpl();

            //drugSearchEngineRedis.moeContentService;
            try {
                drugsRedisResult = moeContentService.moeContentServiceImpl.getAllDrugsFromRedis(keyword, false);    /*Version 2*/
//                drugsRedisResult = moeContentService.moeContentServiceImpl.getDrugMap();    /*Version 3*/

                banDrugsRedisResult = moeContentService.moeContentServiceImpl.getAllDrugsFromRedis(keyword, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        for (DrugCat cat : DrugCat.values()) {      //4 Category
            boolean isDosage = (DrugCat.DOSAGE_DRUGS_STARTS_WITH.equals(cat) || DrugCat.DOSAGE_DRUGS_CONTAINS_WITH.equals(cat));
            boolean isContains = (DrugCat.DRUGS_CONTAINS_WITH.equals(cat) || DrugCat.DOSAGE_DRUGS_CONTAINS_WITH.equals(cat));

            if (isDosage && !showDosage) continue;  //isDosage == true  &&  showDosage == false     ==> remove item, needn't searchMultiIngrByBanVtm

            for (DrugType type : DrugType.values()) {       //9 Type

                if (count >= limit) return packMatched();

                List<MoeDrugDto> drugs = searchBy(type, keyword, isDosage, isContains, false, enableCommonDosage);

                if (drugs.size() > 0) {
                    if (DrugType.TRADENAME.equals(type) ||          //TRADENAME or GENERIC or MULTIINGR or NONMTT
                            DrugType.GENERIC.equals(type) ||
                            DrugType.MULTIINGR.equals(type) ||
                            DrugType.NONMTT.equals(type)) {
                        addMatchedDrugsByType(cat.toString(), type.toString(), drugs);
                    } else {    //ASSORTED
                        addMatchedDrugsByType(cat.toString(), DrugSearchType.ASSORTED.toString(), drugs);
                    }
                }

                if (DrugType.MULTIINGR.equals(type) && isContains) {    //Drug Type = MULTIINGR   &&   Drug Cat = DRUGS_CONTAINS_WITH / DOSAGE_DRUGS_CONTAINS_WITH
                    searchMultiIngrByBanVtm(isDosage, enableCommonDosage);
                }
            }
        }

        //Handle Generic Name in packMatched() function
        return packMatched();
    }

    private List<MoeDrugDto> searchBy(DrugType drugType, String keyword, boolean isDosage, boolean isContains, boolean banMulti, boolean enableCommonDosage) {
        /*List<MoeDrugDto> targetDrugs = isDosage ?
                DtoUtil.DOSAGE_DRUGS_BY_TYPE.get(drugType.toString()) :
                DtoUtil.DRUGS_BY_TYPE.get(drugType.toString());*/

        List<MoeDrugDto> targetDrugs = null;

        if (isRedis) {
            if (drugsRedisResult != null && drugsRedisResult.get("DOSAGE_DRUGS_BY_TYPE") != null && drugsRedisResult.get("DRUGS_BY_TYPE") != null ) {
                if (banMulti) { //Multi Ingr Type
                    try {
                        MoeContentServiceImpl moeContentService = new MoeContentServiceImpl();
//                        Map<String, Map<String, List<MoeDrugDto>>> multiIngrTypeDrugRedisResult = moeContentService.moeContentServiceImpl.getAllDrugsFromRedis("*", false);
                        Map<String, Map<String, List<MoeDrugDto>>> multiIngrTypeDrugRedisResult = moeContentService.moeContentServiceImpl.getAllMultiIngrDrugsFromRedis();
                        targetDrugs = isDosage ?
                                multiIngrTypeDrugRedisResult.get("DOSAGE_DRUGS_BY_TYPE").get(drugType.toString()) : //DrugType = MULTIINGR
                                multiIngrTypeDrugRedisResult.get("DRUGS_BY_TYPE").get(drugType.toString());
                    } catch (Exception e) {
                        //TODO
                        System.out.println("Failed to get multi Ingr Type Drug from Redis");
                        //e.printStackTrace();
                    }
                } else {//All Type
                    targetDrugs = isDosage ?
                            drugsRedisResult.get("DOSAGE_DRUGS_BY_TYPE").get(drugType.toString()) :
                            drugsRedisResult.get("DRUGS_BY_TYPE").get(drugType.toString());
                }
            }
        } else {
            targetDrugs = isDosage ?
                    DtoUtil.DOSAGE_DRUGS_BY_TYPE.get(drugType.toString()) :
                    DtoUtil.DRUGS_BY_TYPE.get(drugType.toString());
        }

        String cat = isContains ? DrugCat.DRUGS_CONTAINS_WITH.toString() : DrugCat.DRUGS_STARTS_WITH.toString();

        List<MoeDrugDto> matched = new ArrayList<MoeDrugDto>();
        if (targetDrugs == null || targetDrugs.size() == 0) {
            return matched;     //new ArrayList
        }


        //Traversal targetDrugs
        for (MoeDrugDto d : targetDrugs) {

            // validate
            if (isDosage) {
                if (!enableCommonDosage) {      // isDosage = true && enableCommonDosage = false    --> return matched = new ArrayList
                    continue;
                }
                //String type = "C";
                String type = ServerConstant.DOSAGE_TYPE_ALL;
                try {
                    type = d.getCommonDosages().get(0).getCommonDosageType().getCommonDosageType();
                } catch (Exception e) {
                    logger.warn(e.getMessage());
                }
                //if (!type.equals("C") && !type.equals(dosageType)) continue;
                if (!type.equals(ServerConstant.DOSAGE_TYPE_ALL) && !type.equals(dosageType)) continue;     //type!="C" && type!=dosageType   --> return matched = new ArrayList
            }// validate --End


            if (!StringUtil.containsAllPattern(keyword, d.toString().toLowerCase())) {      //Match from Redis
                continue;       // Any one pattern can't match target(replacementString)
            }

            // lookup target and drugSearchType
            String target = "";
            String drugSearchType = drugType.toString();
            if (DrugType.MULTIINGR.equals(drugType) || DrugType.NONMTT.equals(drugType)) {
                target = d.getStrengths().get(0).getAmp().toLowerCase();
            } else if (DrugType.TRADENAME.equals(drugType)) {
                target = d.getTradeName().toLowerCase();
            } else if (DrugType.GENERIC.equals(drugType)) {
                target = d.getScreenDisplay().toLowerCase();
            } else if (DrugType.TRADENAMEALIAS.equals(drugType)) {
                target = d.getTradeNameAlias().toLowerCase();
                drugSearchType = DrugSearchType.ASSORTED.toString();
            } else {
                if (DrugType.VTM.equals(drugType)) {
                    target = d.getVtm().toLowerCase();
                } else {
                    target = d.getAliasNames().get(0).getAliasName().toLowerCase();     //"BAN"  /  "OTHER"  /  "ABB"
                }
                for (MoeDrugStrengthDto strength : d.getStrengths()) {
                    target += " " + strength.getStrengthLevelExtraInfo();
                }
                if (d.getDoseFormExtraInfo() != null) {
                    target += " " + d.getDoseFormExtraInfo();
                }
                if (d.getStrengthCompulsory().equalsIgnoreCase(ServerConstant.STRENGTH_COMPULSORY_YES)) {   //"Y"
                    target += " " + d.getStrengths().get(0).getStrength();
                }
                drugSearchType = DrugSearchType.ASSORTED.toString();
            }

            if (matchKeyword(keyword, target, isContains, banMulti) && allowAdd(d, cat, drugSearchType)) {
                //Simon 20190731--start for identifying dangerous drug
                d.setDangerDrug(DtoUtil.DANGEROUS_DRUG_LEGAL_CLASS.contains(d.getLegalClass()) ? ServerConstant.DANGEROUS_DRUG_LEGAL_CLASS_Y : ServerConstant.DANGEROUS_DRUG_LEGAL_CLASS_N);
                //Simon 20190731--start for identifying dangerous drug);
                matched.add(d);
            }

            if (count + matched.size() >= limit) break;

        }       //Foreach targetDrugs  End

        count += matched.size();
        return matched;
    }

    private boolean matchKeyword(String keyword, String target, boolean isContains, boolean banMulti) {
        if (banMulti) {
            //return target.contains(keyword);
            return true;
        }

        if (!isContains) {
            return StringUtil.startsWithFirstPattern(keyword, target);
        }
        return StringUtil.containsFirstPattern(keyword, target);
    }

    private boolean matchTradeAlias(String keyword, String target) {
        if (target == null)
            return false;

        return StringUtil.containsFirstPattern(keyword, target.toLowerCase());
    }

    private boolean allowAdd(MoeDrugDto d, String cat, String type) {
        if (this.hkregs != null && hkregs.contains(d.getLocalDrugId())) {
            return false;
        }
        if (!isDuplicate(d)) {
            if (!d.isParent()) {
                return addParent(d.getShortName(), cat, type);  //isDuplicate = false && isParent = false
            }
            return true;    //isDuplicate = false && isParent = true        can be allow
        }
        return false;   //isDuplicate = true    can't be allow add
    }

    private boolean isDuplicate(MoeDrugDto d) {
        if (d.isParent()) {     //Parent
            // co-exist with VTM
            if (MoeCommonHelper.isAliasNameTypeVtm(d.getDisplayNameType())  //"V"
                    || MoeCommonHelper.isAliasNameTypeTradeNameAlias(d.getDisplayNameType())    //"N"
                    || MoeCommonHelper.isAliasNameTypeLocalDrug(d.getDisplayNameType())) {      //"L"
                return !matchedVTM.add(Formatter.getDrugKey(d))
                        || !matchedParent.add(d.getDisplayString());
            } else if (MoeCommonHelper.isAliasNameTypeAbb(d.getDisplayNameType())   //"A"
                    || MoeCommonHelper.isAliasNameTypeBan(d.getDisplayNameType())   //"B"
                    || MoeCommonHelper.isAliasNameTypeOther(d.getDisplayNameType())) {      //"O"
                return matchedVTM.contains(Formatter.getDrugKey(d))
                        || !matchedTypeDrug.add(d.getDisplayNameType() + d.getDisplayString());
            }
            return matchedVTM.contains(Formatter.getDrugKey(d))
                    || !matchedParent.add(d.getDisplayString());
        } else {        //Child
            return !matchedDosage.add(d.getReplacementString());
        }
    }

    // add parent if doesnt exist one
    private boolean addParent(String parentName, String cat, String type) {
        MoeDrugDto parentDto = DtoUtil.DRUGS_BY_DRUGNAME.get(parentName);
        List<MoeDrugDto> parentList = matchedDrugs.get(cat).get(type);
//        System.out.println("=!=!=!=!=!=!=!=!=!=!=!=!=!=!=!addParent :  Category = " + cat);
        if (parentList == null) {
            parentList = new ArrayList<MoeDrugDto>();
            matchedDrugs.get(cat).put(type, parentList);
        }
        if (parentList.contains(parentDto)) {
//            System.out.println("Contains allow add============================!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            return true;    //allow add
        } else if (!isDuplicate(parentDto)) {
//            System.out.println("Else if allow add============================!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            parentList.add(parentDto);
            count++;
            return true;    //allow add
        }
        return false;
    }

    private void addMatchedDrugsByType(String cat, String type, List<MoeDrugDto> drugs) {
        HashMap<String, List<MoeDrugDto>> drugsByType = matchedDrugs.get(cat);  //4 Cat
        List<MoeDrugDto> matched = drugsByType.get(type);
        if (matched != null) {
            matched.addAll(drugs);
        } else {
            drugsByType.put(type, drugs);
        }
    }

    private List<MoeDrugDto> packMatched() {
        List<MoeDrugDto> packed = new ArrayList<MoeDrugDto>();
        List<MoeDrugDto> packedMulti = new ArrayList<MoeDrugDto>();

        for (DrugCat cat : DrugCat.values()) {
            for (DrugSearchType type : DrugSearchType.values()) {
                List<MoeDrugDto> drugs = matchedDrugs.get(cat.toString()).get(type.toString());
                if (drugs != null) {
                    Collections.sort(drugs, COMPARATOR_BY_TYPE.get(type));
                    if (DrugSearchType.MULTIINGR.equals(type)) {
                        packedMulti.addAll(filterDrugs(drugs));
                    } else {
                        packed.addAll(filterDrugs(drugs));
                    }
                }
            }
        }
        packed.addAll(packedMulti);
        MoeDrugDtoByTypeComparator typeComparator = new MoeDrugDtoByTypeComparator();
        Collections.sort(packed, typeComparator);
/*		if (packed.size() > limit)

			packed = packed.subList(0, limit-1);*/

        //Chris 20200110 Generic Name  -Start
        String genericNameDosageItemlocalDrugId = null;
//        Set<String> childKeyList = new LinkedHashSet<>();
        Set<String> parentKeyList = new LinkedHashSet<>();
        List<MoeDrugDto> genericNameChildList = new ArrayList<>();

        for (MoeDrugDto dto : packed) {

            try {
                genericNameDosageItemlocalDrugId = dto.getStrengthCompulsory().equalsIgnoreCase("Y") ?
                        "Y" + dto.getStrengths().get(0).getVmpId() :
                        "N" + dto.getVtmRouteFormId();
            } catch (Exception e) {
                System.out.println("Failed to get Generic Name Dosage Item localDrugId.");
            }

            if (StringUtils.isNotBlank(genericNameDosageItemlocalDrugId)) {
                if (!dto.isParent()) {  //Collect Generic Name Level Child Dosage Items

                    //childKeyList.add(genericNameDosageItemlocalDrugId);

                    //Copy MoeDrugDto
                    MoeDrugDto newDosageDto = resetGenericNameDosageItemDto(genericNameDosageItemlocalDrugId, dto);

                    genericNameChildList.add(newDosageDto);

                } else {    //Collect Generic Name Level Parent Dosage Items

                    parentKeyList.add(genericNameDosageItemlocalDrugId);

                }
            }

        }

        List<MoeDrugDto> genericNameLevelParentItems = new ArrayList<>();

        //Get Generic Name Level Parent Items Data
        try {
            MoeContentServiceImpl moeContentService = new MoeContentServiceImpl();

            //genericNameLevelParentItems = moeContentService.moeContentServiceImpl.getGenericNameLevelItemsById(childKeyList, isRedis);
            genericNameLevelParentItems = moeContentService.moeContentServiceImpl.getGenericNameLevelItemsById(parentKeyList, isRedis);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //If have Generic Name Level Items, add Parent and child Items
        if(genericNameLevelParentItems != null && genericNameLevelParentItems.size() > 0) {
            packed.addAll(0, genericNameLevelParentItems);

            Set<String> genericNameKeySet = new LinkedHashSet<>();

            for (MoeDrugDto dto : genericNameLevelParentItems) {
                genericNameKeySet.add(dto.getLocalDrugId());
            }


            Iterator<MoeDrugDto> iterator = genericNameChildList.iterator();
            while (iterator.hasNext()) {
                MoeDrugDto childDosageDto = iterator.next();
                if (!genericNameKeySet.contains(childDosageDto.getLocalDrugId())) {
                    iterator.remove();
                }
            }


            if (genericNameChildList.size() > 0) {
                packed.addAll(genericNameChildList);
            }
        }
        //Chris 20200110 Generic Name  -End
        return packed;
    }

    //Chris 20200110  Restructuring
    private MoeDrugDto resetGenericNameDosageItemDto(String dosageChildLocalDrugId, MoeDrugDto srcDto) {
        MoeDrugDto newDto = new MoeDrugDto();

        //MoeDrugDto
        newDto.setLocalDrugId(dosageChildLocalDrugId);
        newDto.setHkRegNo(null);
        newDto.setVtm(srcDto.getVtm());
        newDto.setVtmId(srcDto.getVtmId());
        newDto.setBaseUnit(srcDto.getBaseUnit());
        newDto.setPrescribeUnit(srcDto.getPrescribeUnit());
        newDto.setDispenseUnit(srcDto.getDispenseUnit());
        newDto.setLegalClassId(srcDto.getLegalClassId());
        newDto.setLegalClass(srcDto.getLegalClass());
        newDto.setGenericIndicator(srcDto.getGenericIndicator());
        newDto.setStrengthCompulsory(srcDto.getStrengthCompulsory());
        newDto.setAllergyCheckFlag(srcDto.getAllergyCheckFlag());
        newDto.setAliasNames(srcDto.getAliasNames());
        newDto.setCommonDosages(srcDto.getCommonDosages());
        newDto.setForm(srcDto.getForm());
        newDto.setFormEng(srcDto.getFormEng());
        newDto.setDoseFormExtraInfoId(srcDto.getDoseFormExtraInfoId());
        newDto.setDoseFormExtraInfo(srcDto.getDoseFormExtraInfo());
        newDto.setRoute(srcDto.getRoute());
        newDto.setRouteEng(srcDto.getRouteEng());
        newDto.setScreenDisplay(srcDto.getScreenDisplay());
        newDto.setPrescribeUnitId(srcDto.getPrescribeUnitId());
        newDto.setBaseUnitId(srcDto.getBaseUnitId());
        newDto.setDispenseUnitId(srcDto.getDispenseUnitId());
        newDto.setStrengths(srcDto.getStrengths());
        newDto.setIngredientList(srcDto.getIngredientList());
        newDto.setVtmRouteFormId(srcDto.getVtmRouteFormId());
        newDto.setTerminologyName(srcDto.getTerminologyName());
        newDto.setDrugCategoryId(srcDto.getDrugCategoryId());
        newDto.setDangerDrug(srcDto.getDangerDrug());

        //GenericDto
        newDto.setDidYouMean(srcDto.isDidYouMean());
        newDto.setFreeText(srcDto.isFreeText());
        newDto.setDisplayNameType(srcDto.getDisplayNameType());
        newDto.setShortName(srcDto.getShortName());
        //newDto.setReplacementString(srcDto.getReplacementString());
        newDto.setDisplayString(srcDto.getDisplayString());
        newDto.setParent(false);
        newDto.setSwapDisplayFormat(srcDto.isSwapDisplayFormat());

        return newDto;
    }


    private List<MoeDrugDto> filterDrugs(List<MoeDrugDto> drugs) {
        List<MoeDrugDto> sink = new ArrayList<MoeDrugDto>();
        for (MoeDrugDto drug : drugs) {
            if (!MoeCommonHelper.isAliasNameTypeLocalDrug(drug.getDisplayNameType())    //"L"
                    && !MoeCommonHelper.isAliasNameTypeTradeNameAlias(drug.getDisplayNameType())    //"N"
                    && !MoeCommonHelper.isAliasNameTypeVtm(drug.getDisplayNameType())    //"V"
                    && matchedVTM.contains(Formatter.getDrugKey(drug))) {
                continue;
            }
            sink.add(drug);
        }
        return sink;
    }

    private void removeDuplicated(List<MoeDrugDto> drugs) {
        List<MoeDrugDto> listToRemove = new ArrayList<MoeDrugDto>();
        for (MoeDrugDto dto : drugs) {
            for (MoeDrugDto drug : drugs) {
                if (Formatter.getDrugKey(dto).equals(Formatter.getDrugKey(drug))
                        && MoeCommonHelper.isAliasNameTypeVtm(drug.getDisplayNameType())
                        && MoeCommonHelper.isAliasNameTypeVtm(dto.getDisplayNameType())) {
                    listToRemove.add(drug);
                }
            }
        }
        drugs.removeAll(listToRemove);

    }

    //Drug Type = MULTIINGR   &&   Drug Cat = DRUGS_CONTAINS_WITH / DOSAGE_DRUGS_CONTAINS_WITH
    private void searchMultiIngrByBanVtm(boolean isDosage, boolean enableCommonDosage) {

        List<MoeDrugDto> targetDrugs = null;

        if (isRedis) {
           /* if (drugsRedisResult != null && drugsRedisResult.get("DOSAGE_DRUGS_BY_TYPE") != null && drugsRedisResult.get("DRUGS_BY_TYPE") != null ) {
                targetDrugs = isDosage ?
                        drugsRedisResult.get("DOSAGE_DRUGS_BY_TYPE").get(DrugType.BAN.toString()) :
                        drugsRedisResult.get("DRUGS_BY_TYPE").get(DrugType.BAN.toString());
            }*/
           if (banDrugsRedisResult != null && banDrugsRedisResult.get("DOSAGE_DRUGS_BY_TYPE") != null && banDrugsRedisResult.get("DRUGS_BY_TYPE") != null) {
               targetDrugs = isDosage ?
                       banDrugsRedisResult.get("DOSAGE_DRUGS_BY_TYPE").get(DrugType.BAN.toString()) :
                       banDrugsRedisResult.get("DRUGS_BY_TYPE").get(DrugType.BAN.toString());
           }
        } else {
            targetDrugs = isDosage ?
                    DtoUtil.DOSAGE_DRUGS_BY_TYPE.get(DrugType.BAN.toString()) :
                    DtoUtil.DRUGS_BY_TYPE.get(DrugType.BAN.toString());
        }

        /*List<MoeDrugDto> targetDrugs = isDosage ?
                DtoUtil.DOSAGE_DRUGS_BY_TYPE.get(DrugType.BAN.toString()) :
                DtoUtil.DRUGS_BY_TYPE.get(DrugType.BAN.toString());*/

        if (targetDrugs == null || targetDrugs.size() == 0) return;

        Set<String> vtms = new HashSet<String>();
        for (MoeDrugDto drug : targetDrugs) {
            // Check keyword matches with BAN
            if (!vtms.add(drug.getVtm()) ||     //Is drug.vtm not duplicate
                    !StringUtil.containsFirstPattern(this.keyword, drug.getAliasNames().get(0).getAliasName().toLowerCase()))       //Match First Pattern of keyword
                continue;

            // Search Multiple Ingredient by BAN VTM
            String keyword = drug.getVtm();
            int idx = this.keyword.indexOf(" ");    //adal cap
            if (idx != -1) {
                keyword += this.keyword.substring(idx);
            }   //else : input keyword isn't contains " "

            List<MoeDrugDto> multiIngrs = searchBy(DrugType.MULTIINGR, keyword.toLowerCase(), isDosage, true, true, enableCommonDosage);
            if (multiIngrs.size() == 0) continue;

            // Add result
            if (isDosage) {
                addMatchedDrugsByType(DrugCat.DOSAGE_DRUGS_CONTAINS_WITH.toString(), DrugType.MULTIINGR.toString(), multiIngrs);
            } else {
                addMatchedDrugsByType(DrugCat.DRUGS_CONTAINS_WITH.toString(), DrugType.MULTIINGR.toString(), multiIngrs);
            }
        }
    }

}
