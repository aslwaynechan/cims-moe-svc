package hk.health.moe.util;

/**************************************************************************
 * NAME        : ResponseUtil.java
 * VERSION     : 1.0.0
 * DATE        : 03-JUL-2019
 * DESCRIPTION : Custom util for unified-return tool
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       03-JUL-2019  Initial Version
 * Chris Li         05-DEC-2019  Support Response Code
 **************************************************************************/

import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.pojo.dto.FieldErrorDto;
import hk.health.moe.pojo.vo.CimsResponseVo;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;

public class ResponseUtil {

    public static CimsResponseVo initSuccessResultVO() {
        CimsResponseVo responseVo = new CimsResponseVo();
//        responseVo.setRespCode(ServerConstant.SUCCESS_RESULT_CODE);
        responseVo.setRespCode(ResponseCode.CommonMessage.SUCCESS.getResponseCode());
        return responseVo;
    }

    public static<T> CimsResponseVo initSuccessResultVO(T data) {
        CimsResponseVo<T>responseVo = new CimsResponseVo<>();
//        responseVo.setRespCode(ServerConstant.SUCCESS_RESULT_CODE);
        responseVo.setRespCode(ResponseCode.CommonMessage.SUCCESS.getResponseCode());
        responseVo.setData(data);
        return responseVo;
    }

    public static CimsResponseVo initErrorResultVO() {
        CimsResponseVo responseVo = new CimsResponseVo();
        responseVo.setRespCode(ServerConstant.ERROR_RESULT_CODE);
        return responseVo;
    }

    public static CimsResponseVo initErrorResultVO(String msg) {
        CimsResponseVo responseVo = new CimsResponseVo();
        responseVo.setRespCode(ServerConstant.ERROR_RESULT_CODE);
        responseVo.setErrMsg(msg);
        return responseVo;
    }

    public static CimsResponseVo initErrorResultVO(BindingResult bindingResult,Integer responseCode, String errMsg) {
        CimsResponseVo responseVo = new CimsResponseVo();
//        responseVo.setRespCode(ServerConstant.ERROR_RESULT_CODE);
        responseVo.setRespCode(responseCode);
        responseVo.setErrMsg(errMsg);
        List<FieldErrorDto> list = new ArrayList<>();
        bindingResult.getAllErrors().forEach(error -> {
            FieldErrorDto dto = new FieldErrorDto();
            FieldError fieldError = (FieldError) error;
            dto.setFieldName(fieldError.getField());
            dto.setErrMsg(error.getDefaultMessage());
            list.add(dto);
        });
        responseVo.setData(list);

        return responseVo;
    }

    public static CimsResponseVo initErrorResultVO(List<FieldErrorDto> fieldErrorDtos, String errMsg) {
        CimsResponseVo responseVo = new CimsResponseVo();
        responseVo.setRespCode(ServerConstant.ERROR_RESULT_CODE);
        responseVo.setErrMsg(errMsg);
        responseVo.setData(fieldErrorDtos);
        return responseVo;
    }



    //Chris 20191205  -Start
    public static CimsResponseVo initErrorResultVO(Integer responseCode, String errMsg) {
        CimsResponseVo responseVo = new CimsResponseVo();
        //responseVo.setRespCode(ServerConstant.ERROR_RESULT_CODE);
        responseVo.setRespCode(responseCode);
        responseVo.setErrMsg(errMsg);
        return responseVo;
    }

    public static CimsResponseVo initErrorResultVO(int responseCode, List<String> dynamicMessage) {
        CimsResponseVo responseVo = new CimsResponseVo();
        //responseVo.setRespCode(ServerConstant.ERROR_RESULT_CODE);
        responseVo.setRespCode(responseCode);
        responseVo.setData(dynamicMessage);
        return responseVo;
    }
    //Chris 20191205  -End

    //eric 20191227 start--
    public static CimsResponseVo initErrorResultVO(List<FieldErrorDto> fieldErrorDtos,int responseCode,String errMsg) {
        CimsResponseVo responseVo = new CimsResponseVo();
        responseVo.setRespCode(responseCode);
        responseVo.setErrMsg(errMsg);
        responseVo.setData(fieldErrorDtos);
        return responseVo;
    }
    //eric 20191227 end--
}
