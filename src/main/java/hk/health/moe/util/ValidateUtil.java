/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  MOE Drug Maintenance
 *
 * PROGRAM NAME    :  ValidateUtil.java
 *
 * PURPOSE         :  Defines a set of methods that perform common, often re-used functions
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.util;

import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.ParamException;
import hk.health.moe.pojo.dto.AuthenticationDto;
import hk.health.moe.pojo.dto.FieldErrorDto;
import hk.health.moe.pojo.dto.MoeBaseUnitDto;
import hk.health.moe.pojo.dto.MoeCommonDosageDto;
import hk.health.moe.pojo.dto.MoeCommonDosageTypeDto;
import hk.health.moe.pojo.dto.MoeFormDto;
import hk.health.moe.pojo.dto.MoeFreqDto;
import hk.health.moe.pojo.dto.MoeRouteDto;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.pojo.dto.inner.InnerSaveCommonDosageDto;
import hk.health.moe.pojo.dto.inner.InnerSaveDrugDto;
import hk.health.moe.pojo.po.MoeDoseFormExtraInfoPo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ValidateUtil {

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    public void validate(InnerSaveDrugDto innerSaveDrugDto) throws ParamException {

        if (validateRoute(innerSaveDrugDto)) {
            throw new ParamException("Error Route: " + innerSaveDrugDto.getMoeDrugLocal().getRouteEng());
        }
        if (validateDoseForm(innerSaveDrugDto)) {
            throw new ParamException("Error Form: " + innerSaveDrugDto.getMoeDrugLocal().getFormEng());
        }
        if (validateDoseFormExtraInfo(innerSaveDrugDto)) {
            throw new ParamException("Error Dose Form Extra Info: " + innerSaveDrugDto.getMoeDrugLocal().getDoseFormExtraInfo());
        }
        if (validateBaseUnit(innerSaveDrugDto)) {
            throw new ParamException("Error Base Unit: " + innerSaveDrugDto.getMoeDrugLocal().getBaseUnit());
        }
        if (validatePrescribingUnit(innerSaveDrugDto)) {
            throw new ParamException("Error Prescribing Unit: " + innerSaveDrugDto.getMoeDrugLocal().getPrescribeUnit());
        }
        if (validateDispensingUnit(innerSaveDrugDto)) {
            throw new ParamException("Error Dispensing Unit: " + innerSaveDrugDto.getMoeDrugLocal().getDispenseUnit());
        }
        if (validateDosageType(innerSaveDrugDto)) {
            throw new ParamException("Error Common Dosage Type");
        }
        if (validateFreq(innerSaveDrugDto)) {
            throw new ParamException("Error Common Dosage Frequency");
        }
    }

    public boolean validateRoute(InnerSaveDrugDto innerSaveDrugDto) {
        if (innerSaveDrugDto.getMoeDrugLocal().getRouteEng() == null) {
            return false;
        }
        for (MoeRouteDto dto : DtoUtil.ROUTE_LIST) {
            if (dto.getRouteEng().equals(innerSaveDrugDto.getMoeDrugLocal().getRouteEng())
                    && dto.getRouteId().intValue() == innerSaveDrugDto.getMoeDrugLocal().getRouteId().intValue()) {
                return false;
            }
        }
        return true;
    }

    public boolean validateDoseForm(InnerSaveDrugDto innerSaveDrugDto) {
        if (innerSaveDrugDto.getMoeDrugLocal().getFormEng() == null) {
            return false;
        }
        for (MoeFormDto dto : DtoUtil.FORM_LIST) {
            if (dto.getFormEng().equals(innerSaveDrugDto.getMoeDrugLocal().getFormEng())
                    && dto.getFormId().intValue() == innerSaveDrugDto.getMoeDrugLocal().getFormId().intValue()) {
                return false;
            }
        }
        return true;
    }

    public boolean validateDoseFormExtraInfo(InnerSaveDrugDto innerSaveDrugDto) {
        if (innerSaveDrugDto.getMoeDrugLocal().getDoseFormExtraInfo() == null) {
            return false;
        }
        for (MoeDoseFormExtraInfoPo po : DtoUtil.DOSE_FORM_EXTRA_INFO_LIST) {
            if (po.getDoseFormExtraInfo().equals(innerSaveDrugDto.getMoeDrugLocal().getDoseFormExtraInfo())
                    && po.getDoseFormExtraInfoId() == innerSaveDrugDto.getMoeDrugLocal().getDoseFormExtraInfoId().longValue()) {
                return false;
            }
        }
        return true;
    }

    public boolean validateBaseUnit(InnerSaveDrugDto innerSaveDrugDto) {
        if (innerSaveDrugDto.getMoeDrugLocal().getBaseUnit() == null) {
            return false;
        }
        for (MoeBaseUnitDto dto : DtoUtil.BASE_UNIT_LIST) {
            if (dto.getBaseUnit().equals(innerSaveDrugDto.getMoeDrugLocal().getBaseUnit())
                    && dto.getBaseUnitId().intValue() == innerSaveDrugDto.getMoeDrugLocal().getBaseUnitId().intValue()) {
                return false;
            }
        }
        return true;
    }

    public boolean validatePrescribingUnit(InnerSaveDrugDto innerSaveDrugDto) {
        if (innerSaveDrugDto.getMoeDrugLocal().getPrescribeUnit() == null) {
            return false;
        }
        for (MoeBaseUnitDto dto : DtoUtil.PRESCRIBE_UNIT_LIST) {
            if (dto.getBaseUnit().equals(innerSaveDrugDto.getMoeDrugLocal().getPrescribeUnit())
                    && dto.getBaseUnitId() == innerSaveDrugDto.getMoeDrugLocal().getPrescribeUnitId().intValue()) {
                return false;
            }
        }
        return true;
    }

    public boolean validateDispensingUnit(InnerSaveDrugDto innerSaveDrugDto) {
        if (innerSaveDrugDto.getMoeDrugLocal().getDispenseUnit() == null) {
            return false;
        }
        for (MoeBaseUnitDto dto : DtoUtil.DISPENSE_UNIT_LIST) {
            if (dto.getBaseUnit().equals(innerSaveDrugDto.getMoeDrugLocal().getDispenseUnit())
                    && dto.getBaseUnitId() == innerSaveDrugDto.getMoeDrugLocal().getDispenseUnitId().intValue()) {
                return false;
            }
        }
        return true;
    }

    public boolean validateDosageType(InnerSaveDrugDto innerSaveDrugDto) {
        if (innerSaveDrugDto.getPredefineDosageItems().size() == 0) {
            return false;
        }
        for (InnerSaveCommonDosageDto dto : innerSaveDrugDto.getPredefineDosageItems()) {
            if (ServerConstant.RECORD_DELETE.equalsIgnoreCase(dto.getActionType())) {
                continue;
            }
            MoeCommonDosageDto moeCommonDosageDto = dto.getMoeCommonDosage();
            for (MoeCommonDosageTypeDto metaDosageType : DtoUtil.COMMON_DOSAGE_TYPE_LIST) {
                if (metaDosageType.getCommonDosageType().equalsIgnoreCase(moeCommonDosageDto.getStrCommonDosageType())) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean validateFreq(InnerSaveDrugDto innerSaveDrugDto) {
        if (innerSaveDrugDto.getPredefineDosageItems().size() == 0) {
            return false;
        }
        for (InnerSaveCommonDosageDto dto : innerSaveDrugDto.getPredefineDosageItems()) {
            if (ServerConstant.RECORD_DELETE.equalsIgnoreCase(dto.getActionType())) {
                continue;
            }
            MoeCommonDosageDto moeCommonDosageDto = dto.getMoeCommonDosage();
            for (MoeFreqDto freq : DtoUtil.FREQS) {
                if (freq.getFreqEng().equalsIgnoreCase(moeCommonDosageDto.getFreqEng())
                        && freq.getFreqCode().equalsIgnoreCase(moeCommonDosageDto.getFreqCode())) {
                    return false;
                }
            }
        }
        return true;
    }

    // Ricci 20190925 start --
    public List<FieldErrorDto> validateLoginByMoe(AuthenticationDto dto) {

        FieldErrorDto fieldErrorDto = null;
        List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();

        if (null == dto.getApptDate()) {
            fieldErrorDto = new FieldErrorDto("apptDate", localeMessageSourceUtil.getMessage("param.notBlank"));
            fieldErrorDtos.add(fieldErrorDto);
        }
        if (StringUtils.isBlank(dto.getApptHospitalCd())) {
            fieldErrorDto = new FieldErrorDto("hospitalCd", localeMessageSourceUtil.getMessage("param.notBlank"));
            fieldErrorDtos.add(fieldErrorDto);
        }
        if (StringUtils.isBlank(dto.getApptSpec())) {
            fieldErrorDto = new FieldErrorDto("apptSpec", localeMessageSourceUtil.getMessage("param.notBlank"));
            fieldErrorDtos.add(fieldErrorDto);
        }

        UserDto userDto = dto.getUser();
        if (StringUtils.isBlank(userDto.getHospitalCd())) {
            fieldErrorDto = new FieldErrorDto("hospitalCd", localeMessageSourceUtil.getMessage("param.notBlank"));
            fieldErrorDtos.add(fieldErrorDto);
        }
        if (StringUtils.isBlank(userDto.getMrnPatientIdentity())) {
            fieldErrorDto = new FieldErrorDto("mrnPatientIdentity", localeMessageSourceUtil.getMessage("param.notBlank"));
            fieldErrorDtos.add(fieldErrorDto);
        }
        if (StringUtils.isBlank(userDto.getMrnPatientEncounterNum())) {
            fieldErrorDto = new FieldErrorDto("mrnPatientEncounterNum", localeMessageSourceUtil.getMessage("param.notBlank"));
            fieldErrorDtos.add(fieldErrorDto);
        }
        if (StringUtils.isBlank(userDto.getPatientTypeCd())) {
            fieldErrorDto = new FieldErrorDto("patientTypeCd", localeMessageSourceUtil.getMessage("param.notBlank"));
            fieldErrorDtos.add(fieldErrorDto);
        }
        if (StringUtils.isBlank(userDto.getPrescTypeCd())) {
            fieldErrorDto = new FieldErrorDto("prescTypeCd", localeMessageSourceUtil.getMessage("param.notBlank"));
            fieldErrorDtos.add(fieldErrorDto);
        }
        if (StringUtils.isBlank(userDto.getSpec())) {
            fieldErrorDto = new FieldErrorDto("spec", localeMessageSourceUtil.getMessage("param.notBlank"));
            fieldErrorDtos.add(fieldErrorDto);
        }

        return fieldErrorDtos;
    }
    // Ricci 20190925 end --

    // Ricci 20191010 start --
    public List<FieldErrorDto> buildFieldErrorList(String fieldName, String msgCode, List<FieldErrorDto> list) {
        String msg = "";
        switch (msgCode) {
            case ServerConstant.FIELD_ERROR_NOT_NULL:
                msg = localeMessageSourceUtil.getMessage("param.notBlank");
                break;

            default:
                break;
        }
        FieldErrorDto fieldErrorDto = new FieldErrorDto(fieldName, msg);
        list.add(fieldErrorDto);

        return list;
    }
    // Ricci 20191010 end --

}
