package hk.health.moe.util;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/8/13
 */
public class NumberToWordUtil {

    private static final String[] TEENS_NAMES = {
            "",
            " ten",
            " twenty",
            " thirty",
            " forty",
            " fifty",
            " sixty",
            " seventy",
            " eighty",
            " ninety"
    };

    private static final String[] NUM_NAMES = {
            "",
            " one",
            " two",
            " three",
            " four",
            " five",
            " six",
            " seven",
            " eight",
            " nine",
            " ten",
            " eleven",
            " twelve",
            " thirteen",
            " fourteen",
            " fifteen",
            " sixteen",
            " seventeen",
            " eighteen",
            " nineteen"
    };


    /**
     * This method is to convert number less than 1000 to words
     *
     * @param input
     * @return english word for the input number
     */
    public static String convert(int input) {
        int number = input;

        String soFar;

        if (number % 100 < 20){
            soFar = NUM_NAMES[number % 100];
            number /= 100;
        }
        else {
            soFar = NUM_NAMES[number % 10];
            number /= 10;

            soFar = TEENS_NAMES[number % 10] + soFar;
            number /= 10;
        }
        if (number == 0) return soFar;
        return NUM_NAMES[number] + " hundred" + soFar;
    }

}
