package hk.health.moe.util;


import hk.health.moe.common.MoeCommonHelper;
import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.exception.ParamException;
import hk.health.moe.pojo.dto.MoeActionStatusDto;
import hk.health.moe.pojo.dto.MoeBaseUnitDto;
import hk.health.moe.pojo.dto.MoeClinicalAlertRuleDto;
import hk.health.moe.pojo.dto.MoeCommonDosageDto;
import hk.health.moe.pojo.dto.MoeCommonDosageTypeDto;
import hk.health.moe.pojo.dto.MoeDrugAliasNameDto;
import hk.health.moe.pojo.dto.MoeDrugCategoryDto;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.MoeDrugGenericNameLocalDto;
import hk.health.moe.pojo.dto.MoeDrugOrderPropertyDto;
import hk.health.moe.pojo.dto.MoeDrugStrengthDto;
import hk.health.moe.pojo.dto.MoeDurationUnitMultiplierDto;
import hk.health.moe.pojo.dto.MoeEhrAcceptDrugCheckingDto;
import hk.health.moe.pojo.dto.MoeEhrMedAllergenDto;
import hk.health.moe.pojo.dto.MoeEhrMedMultDoseDto;
import hk.health.moe.pojo.dto.MoeEhrMedProfileDto;
import hk.health.moe.pojo.dto.MoeEhrOrderDto;
import hk.health.moe.pojo.dto.MoeEhrRxImageLogDto;
import hk.health.moe.pojo.dto.MoeFormDto;
import hk.health.moe.pojo.dto.MoeFreqDto;
import hk.health.moe.pojo.dto.MoeHospitalSettingDto;
import hk.health.moe.pojo.dto.MoeMedMultDoseDto;
import hk.health.moe.pojo.dto.MoeMedProfileDto;
import hk.health.moe.pojo.dto.MoeMyFavouriteHdrDto;
import hk.health.moe.pojo.dto.MoeOrderDto;
import hk.health.moe.pojo.dto.MoeOverrideReasonDto;
import hk.health.moe.pojo.dto.MoeRegimenDto;
import hk.health.moe.pojo.dto.MoeRegimenUnitDto;
import hk.health.moe.pojo.dto.MoeRouteDto;
import hk.health.moe.pojo.dto.MoeSiteDto;
import hk.health.moe.pojo.dto.MoeSupplFreqDescDto;
import hk.health.moe.pojo.dto.MoeSupplFreqDto;
import hk.health.moe.pojo.dto.MoeSupplFreqSelectionDto;
import hk.health.moe.pojo.dto.MoeSystemSettingDto;
import hk.health.moe.pojo.dto.MoeUserSettingDto;
import hk.health.moe.pojo.dto.PreparationDto;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.pojo.dto.inner.InnerSpecialIntervalDto;
import hk.health.moe.pojo.po.MoeActionStatusPo;
import hk.health.moe.pojo.po.MoeBaseUnitPo;
import hk.health.moe.pojo.po.MoeClinicalAlertRulePo;
import hk.health.moe.pojo.po.MoeCommonDosagePo;
import hk.health.moe.pojo.po.MoeDrugAliasNameLocalPo;
import hk.health.moe.pojo.po.MoeDrugAliasNamePo;
import hk.health.moe.pojo.po.MoeDrugCategoryPo;
import hk.health.moe.pojo.po.MoeDrugGenericNameLocalPo;
import hk.health.moe.pojo.po.MoeDrugLocalPo;
import hk.health.moe.pojo.po.MoeDrugOrdPropertyLocalPo;
import hk.health.moe.pojo.po.MoeDrugPo;
import hk.health.moe.pojo.po.MoeDrugStrengthLocalPo;
import hk.health.moe.pojo.po.MoeDrugStrengthPo;
import hk.health.moe.pojo.po.MoeDurationUnitMultiplierPo;
import hk.health.moe.pojo.po.MoeEhrAcceptDrugCheckingPo;
import hk.health.moe.pojo.po.MoeEhrMedAllergenPo;
import hk.health.moe.pojo.po.MoeEhrMedMultDosePo;
import hk.health.moe.pojo.po.MoeEhrMedProfilePo;
import hk.health.moe.pojo.po.MoeEhrOrderPo;
import hk.health.moe.pojo.po.MoeEhrRxImageLogPo;
import hk.health.moe.pojo.po.MoeFormPo;
import hk.health.moe.pojo.po.MoeFreqLocalPo;
import hk.health.moe.pojo.po.MoeFreqPo;
import hk.health.moe.pojo.po.MoeHospitalSettingPo;
import hk.health.moe.pojo.po.MoeMedMultDosePo;
import hk.health.moe.pojo.po.MoeMedProfilePo;
import hk.health.moe.pojo.po.MoeMyFavouriteDetailPo;
import hk.health.moe.pojo.po.MoeMyFavouriteHdrPo;
import hk.health.moe.pojo.po.MoeMyFavouriteMultDosePo;
import hk.health.moe.pojo.po.MoeOrderPo;
import hk.health.moe.pojo.po.MoeOverrideReasonPo;
import hk.health.moe.pojo.po.MoePatientCasePo;
import hk.health.moe.pojo.po.MoePatientPo;
import hk.health.moe.pojo.po.MoeRegimenPo;
import hk.health.moe.pojo.po.MoeRegimenUnitPo;
import hk.health.moe.pojo.po.MoeRoutePo;
import hk.health.moe.pojo.po.MoeSitePo;
import hk.health.moe.pojo.po.MoeSupplFreqDescPo;
import hk.health.moe.pojo.po.MoeSupplFreqPo;
import hk.health.moe.pojo.po.MoeSupplFreqSelectionPo;
import hk.health.moe.pojo.po.MoeSystemSettingPo;
import hk.health.moe.pojo.po.MoeUserSettingDefaultPo;
import hk.health.moe.pojo.po.MoeUserSettingPo;
import hk.health.moe.service.impl.MoeEhrOrderServiceImpl;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class DtoMapUtil {

    public static MoeDrugDto convertMoeDrugDto(MoeDrugPo moeDrug, String aliasNameType, boolean isDidYouMean) {
        MoeDrugDto dto = new MoeDrugDto();
        dto.setDidYouMean(isDidYouMean);
        dto.setHkRegNo(moeDrug.getHkRegNo());
        dto.setGenericIndicator(moeDrug.getGenericIndicator());
        dto.setParent(true);
        dto.setDisplayNameType(aliasNameType);
        dto.setTradeName(moeDrug.getTradeName());
        dto.setVtm(moeDrug.getVtm());
        dto.setVtmId(moeDrug.getVtmId());
        dto.setManufacturer(moeDrug.getManufacturer());
        dto.setStrengthCompulsory(moeDrug.getStrengthCompulsory());
        if (!isDidYouMean) {
            // Form
            if (moeDrug.getMoeForm() != null) {
                MoeFormDto form = new MoeFormDto();
                form.setFormId(moeDrug.getMoeForm().getFormId());
                form.setFormEng(moeDrug.getFormEng());
                dto.setForm(form);
            }
            if (moeDrug.getMoeDoseFormExtraInfo() != null) {
                dto.setDoseFormExtraInfoId(moeDrug.getMoeDoseFormExtraInfo().getDoseFormExtraInfoId());
            }
            dto.setDoseFormExtraInfo(moeDrug.getDoseFormExtraInfo());
            // Route
            if (moeDrug.getMoeRoute() != null) {
                MoeRouteDto route = new MoeRouteDto();
                route.setRouteId(moeDrug.getMoeRoute().getRouteId());
                route.setRouteEng(moeDrug.getRouteEng());
                dto.setRoute(route);
            }
            dto.setFormEng(moeDrug.getFormEng());
            dto.setRouteEng(moeDrug.getRouteEng());
            dto.setBaseUnit(moeDrug.getBaseUnit());
            dto.setPrescribeUnit(moeDrug.getPrescribeUnit());
            dto.setDispenseUnit(moeDrug.getDispenseUnit());
            dto.setLegalClassId(moeDrug.getLegalClassId());
            dto.setLegalClass(moeDrug.getLegalClass());
            if (moeDrug.getMoeBaseUnitByBaseUnitId() != null) {
                MoeBaseUnitDto base = new MoeBaseUnitDto();
                base.setBaseUnitId(moeDrug.getMoeBaseUnitByBaseUnitId().getBaseUnitId());
                base.setBaseUnit(moeDrug.getBaseUnit());
                dto.setBaseUnitId(base);
            }
            if (moeDrug.getMoeBaseUnitByPrescribeUnitId() != null) {
                MoeBaseUnitDto base = new MoeBaseUnitDto();
                base.setBaseUnitId(moeDrug.getMoeBaseUnitByPrescribeUnitId().getBaseUnitId());
                base.setBaseUnit(moeDrug.getPrescribeUnit());
                dto.setPrescribeUnitId(base);
            }
            if (moeDrug.getMoeBaseUnitByDispenseUnitId() != null) {
                MoeBaseUnitDto base = new MoeBaseUnitDto();
                base.setBaseUnitId(moeDrug.getMoeBaseUnitByDispenseUnitId().getBaseUnitId());
                base.setBaseUnit(moeDrug.getDispenseUnit());
                dto.setDispenseUnitId(base);
            }
            // ID
            dto.setVtmRouteFormId(moeDrug.getVtmRouteFormId());
            dto.setTradeVtmRouteFormId(moeDrug.getTradeVtmRouteFormId());
        }
        return dto;
    }

    public static List<MoeDrugDto> convertMoeDrugDto(MoeDrugPo moeDrug, boolean isDidYouMean) {
        List<MoeDrugDto> dtos = new ArrayList<MoeDrugDto>();

        //VTM
        MoeDrugDto dto = convertMoeDrugDto(moeDrug, ServerConstant.ALIAS_NAME_TYPE_VTM, isDidYouMean);
        dtos.add(dto);

        if (!MoeDrugDto.isMultiIngr(dto.getVtm())) { // ignores alias name for multiple ingredient
            for (MoeDrugAliasNamePo name : moeDrug.getMoeDrugAliasNames()) {
                String aliasNameType = name.getMoeDrugAliasNameType().getAliasNameType();
                dto = convertMoeDrugDto(moeDrug, aliasNameType, isDidYouMean);

                MoeDrugAliasNameDto aliasName = new MoeDrugAliasNameDto();
                aliasName.setAliasName(name.getAliasName());
                aliasName.setAliasNameType(name.getMoeDrugAliasNameType().getAliasNameType());
                aliasName.setAliasNameDesc(name.getMoeDrugAliasNameType().getAliasNameDesc());

                dto.addAliasName(aliasName);

                if ((MoeCommonHelper.isAliasNameTypeAbb(aliasNameType) ||
                        MoeCommonHelper.isAliasNameTypeOther(aliasNameType)) &&
                        DtoUtil.DUP_BASIC_NAMES.contains(moeDrug.getTradeName() + name.getAliasName())) {
                    dto.setSwapDisplayFormat(true);
                }

                dtos.add(dto);
            }
        }

        return dtos;
    }

    public static List<MoeDrugDto> convertMoeDrugDTOWithAllStrengths(MoeDrugPo moeDrug, MoeDrugStrengthPo strength) {
        List<MoeDrugDto> dtos = convertMoeDrugDto(moeDrug, false);
        for (MoeDrugDto dto : dtos) {
            // Strength
            dto.addStrength(convertMoeDrugStrengthDto(strength));
        }

        return dtos;
    }

    public static List<MoeDrugDto> convertMoeDrugDTOWithAllCommonDosage(MoeDrugPo moeDrug, MoeDrugStrengthPo strength) {
        List<MoeDrugDto> dtos = new ArrayList<MoeDrugDto>();
        for (MoeCommonDosagePo dosage : moeDrug.getMoeCommonDosages()) {
            List<MoeDrugDto> list = convertMoeDrugDto(moeDrug, false);
            for (MoeDrugDto dto : list) {
                dto.setParent(false);
                dto.addStrength(convertMoeDrugStrengthDto(strength));
                // Common Dosage
                MoeCommonDosageDto commonDosage = new MoeCommonDosageDto();
                if (dosage.getDosage().floatValue() == dosage.getDosage().intValue()) {
                    commonDosage.setDosageEng(dosage.getDosage().intValue() + " " + dosage.getDosageUnit());
                } else {
                    commonDosage.setDosageEng(dosage.getDosage().floatValue() + " " + dosage.getDosageUnit());
                }
                commonDosage.setDosageUnitId(dosage.getDosageUnitId());
                commonDosage.setDosage(dosage.getDosage());
                commonDosage.setDosageUnit(dosage.getDosageUnit());
                commonDosage.setPrn(dosage.getPrn());
                commonDosage.setSupplFreqId(dosage.getSupplFreqId());
                commonDosage.setSupplFreqEng(dosage.getSupplFreqEng());
                commonDosage.setSupplFreq1(dosage.getSupplFreq1());
                commonDosage.setSupplFreq2(dosage.getSupplFreq2());
                // Common Dosage Type
                MoeCommonDosageTypeDto dosageType = new MoeCommonDosageTypeDto();
                dosageType.setCommonDosageType(dosage.getMoeCommonDosageType().getCommonDosageType());
                commonDosage.setCommonDosageType(dosageType);
                // Freq
                MoeFreqDto freq = convertMoeFreqDto(dosage.getMoeFreq());
                commonDosage.setFreq(freq);
                commonDosage.setFreqCode(dosage.getFreqCode());
                commonDosage.setFreqEng(dosage.getFreqEng());
                commonDosage.setFreq1(dosage.getFreq1());
                commonDosage.setRouteEng(dosage.getRouteEng());
                commonDosage.setRouteId(dosage.getRouteId());
                dto.addCommonDosage(commonDosage);

                dtos.add(dto);
            }
        }
        return dtos;
    }

    public static MoeDrugDto convertMoeDrugDto(MoeDrugLocalPo moeDrug, String aliasNameType, boolean isDidYouMean) {
        MoeDrugDto dto = new MoeDrugDto();
        dto.setDidYouMean(isDidYouMean);
        dto.setHkRegNo(moeDrug.getHkRegNo());
        dto.setLocalDrugId(moeDrug.getLocalDrugId());
        dto.setGenericIndicator(moeDrug.getGenericIndicator());
        dto.setParent(true);
        dto.setDisplayNameType(aliasNameType);
        dto.setTradeName(moeDrug.getTradeName());
        dto.setTradeNameVtmId(moeDrug.getTradeVtmId());
        dto.setVtm(moeDrug.getVtm());
        dto.setVtmId(moeDrug.getVtmId());
        dto.setManufacturer(moeDrug.getManufacturer());
        dto.setStrengthCompulsory(moeDrug.getStrengthCompulsory());
        dto.setAllergyCheckFlag(moeDrug.getAllergyCheckFlag());
        if (!isDidYouMean) {
            // Form
            if (moeDrug.getFormId() != null) {
                MoeFormDto form = new MoeFormDto();
                form.setFormId(moeDrug.getFormId());
                form.setFormEng(moeDrug.getFormEng());
                dto.setForm(form);
            }
            dto.setDoseFormExtraInfoId(moeDrug.getDoseFormExtraInfoId());
            dto.setDoseFormExtraInfo(moeDrug.getDoseFormExtraInfo());
            // Route
            if (moeDrug.getRouteId() != null) {
                MoeRouteDto route = new MoeRouteDto();
                route.setRouteId(moeDrug.getRouteId());
                route.setRouteEng(moeDrug.getRouteEng());
                dto.setRoute(route);
            }
            dto.setFormEng(moeDrug.getFormEng());
            dto.setRouteEng(moeDrug.getRouteEng());
            dto.setBaseUnit(moeDrug.getBaseUnit());
            dto.setPrescribeUnit(moeDrug.getPrescribeUnit());
            dto.setDispenseUnit(moeDrug.getDispenseUnit());
            dto.setLegalClassId(moeDrug.getLegalClassId());
            dto.setLegalClass(moeDrug.getLegalClass());
            if (moeDrug.getBaseUnitId() != null) {
                MoeBaseUnitDto unit = new MoeBaseUnitDto();
                unit.setBaseUnit(moeDrug.getBaseUnit());
                unit.setBaseUnitId(moeDrug.getBaseUnitId());
                dto.setBaseUnitId(unit);
            }
            if (moeDrug.getPrescribeUnitId() != null) {
                MoeBaseUnitDto unit = new MoeBaseUnitDto();
                unit.setBaseUnit(moeDrug.getPrescribeUnit());
                unit.setBaseUnitId(moeDrug.getPrescribeUnitId());
                dto.setPrescribeUnitId(unit);
            }
            if (moeDrug.getDispenseUnitId() != null) {
                MoeBaseUnitDto unit = new MoeBaseUnitDto();
                unit.setBaseUnit(moeDrug.getDispenseUnit());
                unit.setBaseUnitId(moeDrug.getDispenseUnitId());
                dto.setDispenseUnitId(unit);
            }
        }
        // ID
        dto.setVtmRouteFormId(moeDrug.getVtmRouteFormId());
        dto.setTradeVtmRouteFormId(moeDrug.getTradeVtmRouteFormId());

        dto.setTerminologyName(moeDrug.getTerminologyName());

        if (moeDrug.getMoeDrugOrdPropertyLocal() != null) {
            dto.setTradeNameAlias(moeDrug.getMoeDrugOrdPropertyLocal().getTradeNameAlias());
            dto.setDrugCategoryId(moeDrug.getMoeDrugOrdPropertyLocal().getDrugCategoryId());
        }
        return dto;
    }

    public static List<MoeDrugDto> convertMoeDrugDto(MoeDrugLocalPo moeDrug, boolean isDidYouMean) {
        List<MoeDrugDto> dtos = new ArrayList<MoeDrugDto>();

        //VTM
        MoeDrugDto dto = convertMoeDrugDto(moeDrug, ServerConstant.ALIAS_NAME_TYPE_VTM, isDidYouMean);
        dtos.add(dto);

        for (MoeDrugAliasNameLocalPo name : moeDrug.getMoeDrugAliasNames()) {
            String aliasNameType = name.getAliasNameType();
            dto = convertMoeDrugDto(moeDrug, aliasNameType, isDidYouMean);

            MoeDrugAliasNameDto aliasName = new MoeDrugAliasNameDto();
            aliasName.setAliasName(name.getAliasName());
            aliasName.setAliasNameType(name.getAliasNameType());

            dto.addAliasName(aliasName);

            if ((MoeCommonHelper.isAliasNameTypeAbb(aliasNameType)
                    || MoeCommonHelper.isAliasNameTypeOther(aliasNameType)
                    && DtoUtil.DUP_BASIC_NAMES.contains(moeDrug.getTradeName() + name.getAliasName()))) {
                dto.setSwapDisplayFormat(true);
            }

            dtos.add(dto);
        }

        if (moeDrug.getMoeDrugOrdPropertyLocal() != null && moeDrug.getMoeDrugOrdPropertyLocal().getTradeNameAlias() != null) {
            dto = convertMoeDrugDto(moeDrug, ServerConstant.ALIAS_NAME_TYPE_TRADE_NAME_ALIAS, isDidYouMean);
            dtos.add(dto);
        }

        return dtos;
    }

    //Chris 20200113  Supplement vmpId from moe_drug_strength  -Start
    //public static List<MoeDrugDto> convertMoeDrugDTOWithAllStrengths(MoeDrugLocalPo moeDrug, MoeDrugStrengthLocalPo strength) {
    public static List<MoeDrugDto> convertMoeDrugDTOWithAllStrengths(MoeDrugLocalPo moeDrug, MoeDrugStrengthLocalPo strengthLocal, MoeDrugStrengthPo strength) {
        Long vmpId = null;
        //String vmp = null;
        if (strength != null) {
            vmpId = strength.getVmpId();
            //vmp = strength.getVmp();
        }

        List<MoeDrugDto> dtos = convertMoeDrugDto(moeDrug, false);
        for (MoeDrugDto dto : dtos) {
            // Strength
            MoeDrugStrengthDto strDto = new MoeDrugStrengthDto();
            strDto.setStrength(strengthLocal.getStrength());
            strDto.setStrengthLevelExtraInfo(strengthLocal.getStrengthLevelExtraInfo());
            strDto.setAmp(strengthLocal.getAmp());
            strDto.setAmpId(strengthLocal.getAmpId());
            strDto.setVmp(strengthLocal.getVmp());
            strDto.setVmpId(strengthLocal.getVmpId());
            //Chris Supplement VmpId from moe_drug_strength  -Start
            if (vmpId != null && vmpId != 0L) {
                strDto.setVmpId(vmpId);
            }
            //Chris Supplement VmpId from moe_drug_strength  -End

            dto.addStrength(strDto);
        }

        return dtos;
    }

    public static List<MoeDrugDto> convertMoeDrugDTOWithAllCommonDosage(MoeDrugLocalPo moeDrug, MoeDrugStrengthLocalPo strength) {
        List<MoeDrugDto> dtos = new ArrayList<MoeDrugDto>();
        for (MoeCommonDosagePo dosage : moeDrug.getMoeCommonDosages()) {
            if (MoeCommonHelper.isFlagTrue(dosage.getSuspend())
                    || !StringUtil.stripToEmpty(moeDrug.getPrescribeUnit())
                    .equalsIgnoreCase(StringUtil.stripToEmpty(dosage.getDosageUnit()))
                    || DtoUtil.getFreq(dosage.getMoeFreq().getFreqCode()) == null) continue;
            List<MoeDrugDto> list = convertMoeDrugDto(moeDrug, false);
            for (MoeDrugDto dto : list) {
                dto.setParent(false);
                // Strength
                MoeDrugStrengthDto strDto = new MoeDrugStrengthDto();
                strDto.setStrength(strength.getStrength());
                strDto.setStrengthLevelExtraInfo(strength.getStrengthLevelExtraInfo());
                strDto.setAmp(strength.getAmp());
                strDto.setAmpId(strength.getAmpId());
                strDto.setVmp(strength.getVmp());
                strDto.setVmpId(strength.getVmpId());
                dto.addStrength(strDto);
                // Common Dosage
                MoeCommonDosageDto commonDosage = new MoeCommonDosageDto();
                if (dosage.getDosage() != null) {
                    /*if (dosage.getDosage().floatValue() == dosage.getDosage().intValue()) {
                        commonDosage.setDosageEng(dosage.getDosage().intValue() + " " + dosage.getDosageUnit());
                    } else {
                        commonDosage.setDosageEng(dosage.getDosage().floatValue() + " " + dosage.getDosageUnit());
                    }*/
                    commonDosage.setDosageEng(dosage.getDosage() + " " + dosage.getDosageUnit());
                    commonDosage.setDosage(dosage.getDosage());
                }
                if (dosage.getDosageUnitId() != null) {
                    commonDosage.setDosageUnitId(dosage.getDosageUnitId());
                }
                commonDosage.setDosageUnit(dosage.getDosageUnit());
                commonDosage.setPrn(dosage.getPrn());
                commonDosage.setSupplFreqId(dosage.getSupplFreqId());
                commonDosage.setSupplFreqEng(dosage.getSupplFreqEng());
                commonDosage.setSupplFreq1(dosage.getSupplFreq1());
                commonDosage.setSupplFreq2(dosage.getSupplFreq2());
                // Common Dosage Type
                MoeCommonDosageTypeDto dosageType = new MoeCommonDosageTypeDto();
                dosageType.setCommonDosageType(dosage.getMoeCommonDosageType().getCommonDosageType());
                commonDosage.setCommonDosageType(dosageType);
                // Freq
                MoeFreqDto freq = convertMoeFreqDto(dosage.getMoeFreq());
                commonDosage.setFreq(freq);
                commonDosage.setFreqCode(dosage.getFreqCode());
                commonDosage.setFreqEng(dosage.getFreqEng());
                commonDosage.setFreq1(dosage.getFreq1());
                commonDosage.setRouteId(dosage.getRouteId());
                commonDosage.setRouteEng(dosage.getRouteEng());
                dto.addCommonDosage(commonDosage);

                dtos.add(dto);
            }
        }
        return dtos;
    }

    public static MoeFreqDto convertMoeFreqDto(MoeFreqPo moeFreq) {
        MoeFreqDto dto = new MoeFreqDto();
        dto.setFreqCode(moeFreq.getFreqCode());
        dto.setFreqId(moeFreq.getFreqId());
        dto.setFreqEng(moeFreq.getFreqEng());
        dto.setFreqChi(moeFreq.getFreqChi());
        dto.setFreqMultiplier(moeFreq.getFreqMultiplier());
        dto.setLowerLimit(moeFreq.getLowerLimit());
        dto.setUpperLimit(moeFreq.getUpperLimit());
        dto.setDurationUnit(moeFreq.getDurationUnit());
        dto.setDurationValue(moeFreq.getDurationValue());
        dto.setUseInputMethod(moeFreq.getUseInputMethod());
        dto.setUseInputValue(moeFreq.getUseInputValue());
        dto.setFreqMultiplierDenominator(moeFreq.getFreqMultiplierDenominator());
        dto.setFreqDescEng(moeFreq.getFreqDescEng());
        return dto;
    }

    public static MoeFreqDto convertMoeFreqDto(MoeFreqLocalPo moeFreq) {
        MoeFreqDto dto = new MoeFreqDto();
        dto.setFreqCode(moeFreq.getFreqCode());
        dto.setFreqId(moeFreq.getFreqId());
        dto.setFreqEng(moeFreq.getFreqEng());
        dto.setFreqChi(moeFreq.getFreqChi());
        dto.setFreqMultiplier(moeFreq.getFreqMultiplier());
        dto.setLowerLimit(moeFreq.getLowerLimit());
        dto.setUpperLimit(moeFreq.getUpperLimit());
        dto.setDurationUnit(moeFreq.getDurationUnit());
        dto.setDurationValue(moeFreq.getDurationValue());
        dto.setUseInputMethod(moeFreq.getUseInputMethod());
        dto.setUseInputValue(moeFreq.getUseInputValue());
        dto.setFreqMultiplierDenominator(moeFreq.getFreqMultiplierDenominator());
        dto.setFreqDescEng(moeFreq.getFreqDescEng());
        return dto;
    }

    public static MoeActionStatusDto convertMoeActionStatusDto(MoeActionStatusPo moeActionStatus) {
        MoeActionStatusDto dto = new MoeActionStatusDto();
        dto.setActionStatus(moeActionStatus.getActionStatus());
        dto.setActionStatusType(moeActionStatus.getActionStatusType());
        dto.setRank(moeActionStatus.getRank());
        return dto;
    }

    public static MoeSiteDto convertMoeSiteDto(MoeSitePo moeSite) {
        MoeSiteDto dto = new MoeSiteDto();
        dto.setSiteId(moeSite.getSiteId());
        dto.setSiteEng(moeSite.getSiteEng());
        dto.setSiteMultiplier(moeSite.getSiteMultiplier());
        return dto;
    }

    public static MoeSupplFreqSelectionDto convertMoeSupplFreqSelectionDto(MoeSupplFreqSelectionPo selection) {
        MoeSupplFreqSelectionDto dto = new MoeSupplFreqSelectionDto();
        dto.setSupplFreqDisplay(selection.getSupplFreqDisplay());
        return dto;
    }

    public static MoeSupplFreqDescDto convertMoeSupplFreqDescDto(MoeSupplFreqDescPo desc) {
        MoeSupplFreqDescDto dto = new MoeSupplFreqDescDto();
        dto.setFreqCode(desc.getFreqCode());
        dto.setSupplFreq1(desc.getSupplFreq1());
        dto.setDisplayWithFreq(desc.getDisplayWithFreq());
        dto.setSupplFreqDescEng(desc.getSupplFreqDescEng());
        dto.setSupplFreqSelectionValue(desc.getSupplFreqSelectionValue());
        return dto;
    }

    public static MoeSupplFreqDto convertMoeSupplFreqDto(MoeSupplFreqPo moeSupplFreq) {
        MoeSupplFreqDto dto = new MoeSupplFreqDto();
        dto.setSupplFreqId(moeSupplFreq.getSupplFreqId());
        dto.setSupplFreqEng(moeSupplFreq.getSupplFreqEng());
        dto.setPrefixSupplFreqEng(moeSupplFreq.getPrefixSupplFreqEng());
        dto.setRegimenType(moeSupplFreq.getMoeRegimen().getRegimenType());
        dto.setUpperLimit(moeSupplFreq.getUpperLimit());
        dto.setLowerLimit(moeSupplFreq.getLowerLimit());
        dto.setUseInputValue(moeSupplFreq.getUseInputValue());
        dto.setUseInputMethod(moeSupplFreq.getUseInputMethod());
        dto.setPerDurationMultiplier(moeSupplFreq.getPerDurationMultiplier());
        dto.setSupplFreqMultiplierValue(moeSupplFreq.getSupplFreqMultiplier().floatValue());
        if (moeSupplFreq.getMoeSupplFreqDescs() != null) {
            for (MoeSupplFreqDescPo desc : moeSupplFreq.getMoeSupplFreqDescs()) {
                dto.addMoeSupplFreqDescDTO(convertMoeSupplFreqDescDto(desc));
            }
        }
        if (moeSupplFreq.getMoeSupplFreqSelections() != null) {
            for (MoeSupplFreqSelectionPo selection : moeSupplFreq.getMoeSupplFreqSelections()) {
                dto.addMoeSupplFreqSelectionDTO(convertMoeSupplFreqSelectionDto(selection));
            }
        }
        return dto;
    }

    public static MoeSystemSettingDto convertMoeSystemSettingDto(MoeSystemSettingPo moeSystemSetting) {
        MoeSystemSettingDto dto = new MoeSystemSettingDto();
        //dto.setParamDisplay(moeSystemSetting.getParamDisplay());
        //dto.setParamLevel(moeSystemSetting.getParamLevel());
        dto.setParamName(moeSystemSetting.getParamName());
        //dto.setParamNameDesc(moeSystemSetting.getParamNameDesc());
        //dto.setParamPossibleValue(moeSystemSetting.getParamPossibleValue());
        dto.setParamValue(moeSystemSetting.getParamValue());
        //dto.setParamValueType(moeSystemSetting.getParamValueType());
        return dto;
    }

    public static MoeHospitalSettingDto convertMoeHospitalSettingDto(MoeHospitalSettingPo setting) {
        MoeHospitalSettingDto dto = new MoeHospitalSettingDto();
        //dto.setParamDisplay(setting.getParamDisplay());
        //dto.setParamLevel(setting.getParamLevel());
        dto.setParamName(setting.getParamName());
        //dto.setParamNameDesc(setting.getParamNameDesc());
        //dto.setParamPossibleValue(setting.getParamPossibleValue());
        dto.setParamValue(setting.getParamValue());
        //dto.setParamValueType(setting.getParamValueType());
        return dto;
    }

    public static MoeUserSettingDto convertMoeUserSettingDto(MoeUserSettingPo setting) {
        MoeUserSettingDto dto = new MoeUserSettingDto();
        //dto.setParamDisplay(setting.getParamDisplay());
        //dto.setParamLevel(setting.getParamLevel());
        dto.setParamName(setting.getParamName());
        //dto.setParamNameDesc(setting.getParamNameDesc());
        //dto.setParamPossibleValue(setting.getParamPossibleValue());
        dto.setParamValue(setting.getParamValue());
        //dto.setParamValueType(setting.getParamValueType());
        return dto;
    }

    public static MoeEhrMedMultDoseDto convertMoeEhrMedMultDoseDto(MoeEhrMedMultDosePo dose) {
        MoeEhrMedMultDoseDto dto = new MoeEhrMedMultDoseDto();
        dto.setFreqId(dose.getFreqId());
        dto.setSupplFreqId(dose.getSupplFreqId());
        dto.setMoQtyUnitId(dose.getMoQtyUnitId());
        return dto;
    }

    public static MoeEhrMedProfileDto convertMoeEhrMedProfileDto(MoeEhrMedProfilePo profile) {
        MoeEhrMedProfileDto dto = new MoeEhrMedProfileDto();
        dto.setVtm(profile.getVtm());
        dto.setVtmId(profile.getVtmId());
        dto.setTradeName(profile.getTradeName());
        dto.setTradeNameVtmId(profile.getTradeNameVtmId());
        dto.setAliasName(profile.getAliasName());
        dto.setAliasNameType(profile.getAliasNameType());
        dto.setManufacturer(profile.getManufacturer());
        dto.setScreenDisplay(profile.getScreenDisplay());
        //chris 20190725 start
        if (profile.getOrderLineType() != null) {
            dto.setOrderLineType(profile.getOrderLineType().toUpperCase());
        }
        //chris 20190725 end
        dto.setGenericIndicator(profile.getGenericIndicator());
        dto.setCycleMultiplier(profile.getCycleMultiplier());
        dto.setStrengthCompulsory(profile.getStrengthCompulsory());
        dto.setFormId(profile.getFormId());
        dto.setDoseFormExtraInfoId(profile.getDoseFormExtraInfoId());
        dto.setDoseFormExtraInfo(profile.getDoseFormExtraInfo());
        dto.setRouteId(profile.getRouteId());
        dto.setFreqId(profile.getFreqId());
        dto.setSupplFreqId(profile.getSupplFreqId());
        dto.setMoQtyUnitId(profile.getMoQtyUnitId());
        dto.setModuId(profile.getModuId());
        dto.setSiteId(profile.getSiteId());
        dto.setCreateUserId(profile.getCreateUserId());
        dto.setCreateUser(profile.getCreateUser());
        dto.setCreateHosp(profile.getCreateHosp());
        dto.setCreateRank(profile.getCreateRank());
        dto.setCreateRankDesc(profile.getCreateRankDesc());
        dto.setUpdateUserId(profile.getUpdateUserId());
        dto.setUpdateUser(profile.getUpdateUser());
        dto.setUpdateHosp(profile.getUpdateHosp());
        dto.setUpdateRank(profile.getUpdateRank());
        dto.setUpdateRankDesc(profile.getUpdateRankDesc());
        dto.setDrugId(profile.getDrugId());
        dto.setFormId(profile.getFormId());
        dto.setRouteId(profile.getRouteId());
        dto.setFreqId(profile.getFreqId());
        dto.setSupplFreqId(profile.getSupplFreqId());
        dto.setMoQtyUnitId(profile.getMoQtyUnitId());
        dto.setModuId(profile.getModuId());
        dto.setSiteId(profile.getSiteId());
        dto.setDrugRouteEng(profile.getDrugRouteEng());
        dto.setDrugRouteId(profile.getDrugRouteId());
        dto.setStrengthLevelExtraInfo(profile.getStrengthLevelExtraInfo());
        dto.setMedDiscontFlag(profile.getMedDiscontFlag());
        dto.setMedDiscontReasonCode(profile.getMedDiscontReasonCode());
        dto.setMedDiscontInfo(profile.getMedDiscontInformation());
        dto.setMedDiscontUserId(profile.getMedDiscontUserId());
        dto.setMedDiscontUser(profile.getMedDiscontUser());
        dto.setMedDiscontHosp(profile.getMedDiscontHosp());
        dto.setMedDiscontDtm(profile.getMedDiscontDtm());
        return dto;
    }

    public static MoeEhrMedProfileDto convertMoeEhrMedProfileDtoAllParam(MoeEhrMedProfilePo profile) {
        MoeEhrMedProfileDto dto = new MoeEhrMedProfileDto();
        dto.setDrugId(profile.getDrugId());
        dto.setVtm(profile.getVtm());
        dto.setVtmId(profile.getVtmId());
        dto.setTradeName(profile.getTradeName());
        dto.setTradeNameVtmId(profile.getTradeNameVtmId());
        dto.setAliasName(profile.getAliasName());
        dto.setAliasNameType(profile.getAliasNameType());
        dto.setManufacturer(profile.getManufacturer());
        dto.setScreenDisplay(profile.getScreenDisplay());
        //chris 20190725 start
        if (profile.getOrderLineType() != null) {
            dto.setOrderLineType(profile.getOrderLineType().toUpperCase());
        }
        //chris 20190725 end
        dto.setGenericIndicator(profile.getGenericIndicator());
        dto.setStrengthCompulsory(profile.getStrengthCompulsory());
        dto.setCycleMultiplier(profile.getCycleMultiplier());
        dto.setFormId(profile.getFormId());
        dto.setDoseFormExtraInfoId(profile.getDoseFormExtraInfoId());
        dto.setDoseFormExtraInfo(profile.getDoseFormExtraInfo());
        dto.setRouteId(profile.getRouteId());
        dto.setFreqId(profile.getFreqId());
        dto.setSupplFreqId(profile.getSupplFreqId());
        dto.setMoQtyUnitId(profile.getMoQtyUnitId());
        dto.setModuId(profile.getModuId());
        dto.setSiteId(profile.getSiteId());
        dto.setDrugRouteEng(profile.getDrugRouteEng());
        dto.setDrugRouteId(profile.getDrugRouteId());
        dto.setStrengthLevelExtraInfo(profile.getStrengthLevelExtraInfo());
        dto.setCreateUserId(profile.getCreateUserId());
        dto.setCreateUser(profile.getCreateUser());
        dto.setCreateHosp(profile.getCreateHosp());
        dto.setCreateRank(profile.getCreateRank());
        dto.setCreateRankDesc(profile.getCreateRankDesc());
        dto.setCreateDtm(profile.getCreateDtm());
        dto.setUpdateUserId(profile.getUpdateUserId());
        dto.setUpdateUser(profile.getUpdateUser());
        dto.setUpdateHosp(profile.getUpdateHosp());
        dto.setUpdateRank(profile.getUpdateRank());
        dto.setUpdateRankDesc(profile.getUpdateRankDesc());
        dto.setUpdateDtm(profile.getUpdateDtm());
        dto.setMedDiscontFlag(profile.getMedDiscontFlag());
        dto.setMedDiscontReasonCode(profile.getMedDiscontReasonCode());
        dto.setMedDiscontInfo(profile.getMedDiscontInformation());
        dto.setMedDiscontUserId(profile.getMedDiscontUserId());
        dto.setMedDiscontUser(profile.getMedDiscontUser());
        dto.setMedDiscontHosp(profile.getMedDiscontHosp());
        dto.setMedDiscontDtm(profile.getMedDiscontDtm());
        return dto;
    }

    public static MoeMedMultDoseDto convertMoeMedMultDoseDto(MoeMedMultDosePo dose, MoeMedProfilePo profile) {
        MoeMedMultDoseDto dto = new MoeMedMultDoseDto();
        dto.setStepNo(dose.getStepNo());
        dto.setMultDoseNo(dose.getMultDoseNo());
        dto.setPatHospcode(profile.getPatHospcode());
        if (dose.getDosage() != null) {
            dto.setDosage(WsFormatter.toDouble(dose.getDosage()));
        }
        dto.setFreqCode(dose.getFreqCode());
        dto.setFreq1(dose.getFreq1());
        dto.setSupFreqCode(dose.getSupFreqCode());
        dto.setSupFreq1(dose.getSupFreq1());
        dto.setSupFreq2(dose.getSupFreq2());
        dto.setDayOfWeek(dose.getDayOfWeek());
        dto.setAdminTimeCode(dose.getAdminTimeCode());
        dto.setPrn(dose.getPrn());
        dto.setPrnPercent(dose.getPrnPercent());
        dto.setSiteCode(dose.getSiteCode());
        dto.setSupSiteDesc(dose.getSupSiteDesc());
        dto.setDuration(dose.getDuration());
        dto.setDurationUnit(dose.getDurationUnit());
        dto.setStartDate(dose.getStartDate());
        dto.setEndDate(dose.getEndDate());
        dto.setMoQty(dose.getMoQty());
        dto.setMoQtyUnit(dose.getMoQtyUnit());
        dto.setUpdateBy(dose.getUpdateBy());
        dto.setUpdateTime(dose.getUpdateTime());
        dto.setFreqText(dose.getFreqText());
        dto.setSupFreqText(dose.getSupFreqText());
        dto.setCapdSystem(dose.getCapdSystem());
        dto.setCapdCalcium(dose.getCapdCalcium());
        dto.setCapdConc(dose.getCapdConc());
        dto.setItemcode(dose.getItemcode());
        dto.setCapdBaseunit(dose.getCapdBaseunit());
        dto.setSiteDesc(dose.getSiteDesc());
        dto.setTrueConc(dose.getTrueConc());
        dto.setAdminTimeDesc(dose.getAdminTimeDesc());
        dto.setCapdChargeInd(dose.getCapdChargeInd());
        dto.setCapdChargeableUnit(dose.getCapdChargeableUnit());
        dto.setCapdChargeableAmount(dose.getCapdChargeableAmount());
        dto.setConfirmCapdChargeInd(dose.getConfirmCapdChargeInd());
        dto.setConfirmCapdChargeUnit(dose.getConfirmCapdChargeUnit());

        if (dose.getMoeEhrMedMultDose() != null) {
            dto.setMoeEhrMedMultDose(convertMoeEhrMedMultDoseDto(dose.getMoeEhrMedMultDose()));
        }
        return dto;
    }

    public static List<MoeMedMultDoseDto> convertMoeMedMultDoseDtos(Set<MoeMedMultDosePo> doses, MoeMedProfilePo profile) {
        List<MoeMedMultDoseDto> dtos = new ArrayList<MoeMedMultDoseDto>(0);
        if (doses != null) {
            for (MoeMedMultDosePo dose : doses) {
                dtos.add(convertMoeMedMultDoseDto(dose, profile));
            }
        }
        return dtos;
    }

    public static MoeMedProfileDto convertMoeMedProfileDto(MoeMedProfilePo profile, UserDto userDto) {
        MoeMedProfileDto dto = new MoeMedProfileDto();

        if (profile.getMoeEhrMedProfile() != null) {
            dto.setMoeEhrMedProfile(convertMoeEhrMedProfileDto(profile.getMoeEhrMedProfile()));
        }

        if (profile.getMoeEhrMedAllergens() != null) {
            dto.setMoeEhrMedAllergens(convertMoeEhrMedAllergenDtos(profile.getMoeEhrMedAllergens(), profile));
        }

        if (profile.getMoeMedMultDoses() != null) {
            dto.setMoeMedMultDoses(convertMoeMedMultDoseDtos(profile.getMoeMedMultDoses(), profile));
        }

        if (profile.getMoeOrder() != null) {
            dto.setOrdNo(profile.getMoeOrder().getOrdNo());
        }
        dto.setCmsItemNo(profile.getCmsItemNo());
        dto.setPatHospcode(profile.getPatHospcode());
        dto.setCaseNo(profile.getCaseNo());
        dto.setRegimen(profile.getRegimen());
        dto.setMultDose(profile.getMultDose());
        dto.setVolValue(profile.getVolValue());
        dto.setVolUnit(profile.getVolUnit());
        dto.setVolText(profile.getVolText());
        dto.setItemcode(profile.getItemcode());
        dto.setFirstDisplayname(profile.getFirstDisplayname());
        dto.setTradename(profile.getTradename());
        dto.setStrength(profile.getStrength());
        dto.setBaseunit(profile.getBaseunit());
        dto.setFormcode(profile.getFormcode());
        dto.setRouteCode(profile.getRouteCode());
        dto.setRouteDesc(profile.getRouteDesc());
        dto.setFormDesc(profile.getFormDesc());
        dto.setSalt(profile.getSalt());
        dto.setExtraInfo(profile.getExtraInfo());
        dto.setDangerdrug(profile.getDangerdrug());
        dto.setExternalUse(profile.getExternalUse());
        dto.setNameType(profile.getNameType());
        dto.setSecondDisplayname(profile.getSecondDisplayname());
        if (profile.getDosage() != null) {
            dto.setDosage(profile.getDosage());
        }
        dto.setModu(profile.getModu());
        dto.setFreqCode(profile.getFreqCode());
        dto.setFreq1(profile.getFreq1());
        dto.setSupFreqCode(profile.getSupFreqCode());
        dto.setSupFreq1(profile.getSupFreq1());
        dto.setSupFreq2(profile.getSupFreq2());
        dto.setDayOfWeek(profile.getDayOfWeek());
        dto.setAdminTimeCode(profile.getAdminTimeCode());
        dto.setPrn(profile.getPrn());
        dto.setPrnPercent(profile.getPrnPercent());
        dto.setSiteCode(profile.getSiteCode());
        dto.setSupSiteDesc(profile.getSupSiteDesc());
        dto.setDuration(profile.getDuration());
        dto.setDurationUnit(profile.getDurationUnit());
        dto.setStartDate(profile.getStartDate());
        dto.setEndDate(profile.getEndDate());
        dto.setMoQty(profile.getMoQty());
        dto.setMoQtyUnit(profile.getMoQtyUnit());
        dto.setActionStatus(profile.getActionStatus());
        dto.setFormulStatus(profile.getFormulStatus());
        dto.setFormulStatus2(profile.getFormulStatus2());
        dto.setPatType(profile.getPatType());
        dto.setItemStatus(profile.getItemStatus());
        dto.setSpecInstruct(profile.getSpecInstruct());
        dto.setSpecNote(profile.getSpecNote());
        dto.setTranslate(profile.getTranslate());
        dto.setFixPeriod(profile.getFixPeriod());
        dto.setRestricted(profile.getRestricted());
        dto.setSingleUse(profile.getSingleUse());
        dto.setMoCreate(profile.getMoCreate());
        dto.setDisplayRoutedesc(profile.getDisplayRoutedesc());
        dto.setTrueDisplayname(profile.getTrueDisplayname());
        dto.setOrgItemNo(profile.getOrgItemNo());
        dto.setAllowRepeat(profile.getAllowRepeat());
        dto.setFreqText(profile.getFreqText());
        dto.setSupFreqText(profile.getSupFreqText());
        dto.setTrueAliasname(profile.getTrueAliasname());
        dto.setCapdSystem(profile.getCapdSystem());
        dto.setCapdCalcium(profile.getCapdCalcium());
        dto.setCapdConc(profile.getCapdConc());
        dto.setDurationInputType(profile.getDurationInputType());
        dto.setCapdBaseunit(profile.getCapdBaseunit());
        dto.setSiteDesc(profile.getSiteDesc());
        dto.setRemarkCreateBy(profile.getRemarkCreateBy());
        dto.setRemarkCreateDate(profile.getRemarkCreateDate());
        dto.setRemarkText(profile.getRemarkText());
        dto.setRemarkConfirmBy(profile.getRemarkConfirmBy());
        dto.setRemarkConfirmDate(profile.getRemarkConfirmDate());
        dto.setTrueConc(profile.getTrueConc());
        dto.setMdsInfo(profile.getMdsInfo());
        dto.setAdminTimeDesc(profile.getAdminTimeDesc());
        dto.setCommentCreateBy(profile.getCommentCreateBy());
        dto.setCommentCreateDate(profile.getCommentCreateDate());
        dto.setUpdateBy(profile.getUpdateBy());
        dto.setUpdateTime(profile.getUpdateTime());

        //Chris 20191010 For getOrder/listDrugHistory return max duration  -Start
        dto.setMaxDuration(getMaxDuration(dto) != null ? getMaxDuration(dto) : null);
        //Chris 20191010 For getOrder/listDrugHistory return max duration  -End

        //Chris 20191010 For getOrder/listDrugHistory return min dosage  -Start
        dto.setMinDosages(getMinDosage(dto) != null ? getMinDosage(dto) : null);
        //Chris 20191010 For getOrder/listDrugHistory return min dosage  -End

        //Chris 20191010 For getOrder/listDrugHistory return min dosages message  -Start
        dto.setMinDosagesMessage(getMinDosageString(dto) != null ? getMinDosageString(dto) : null);
        //Chris 20191010 For getOrder/listDrugHistory return min dosages message  -End

        //Chris 20190909 For getOrder return Special Interval's Info (support to "odd/even days" case)  -Start
        if (profile.getSupFreqCode() != null && !"".equals(profile.getSupFreqCode())) {
            //Special Interval Model

            InnerSpecialIntervalDto specialIntervalDto = new InnerSpecialIntervalDto();
            List<String> freqCodeList = new ArrayList<String>();

            specialIntervalDto.setSupFreqCode(profile.getSupFreqCode());
            specialIntervalDto.setRegimen(profile.getRegimen());
            specialIntervalDto.setSupFreq1(profile.getSupFreq1());
            specialIntervalDto.setSupFreq2(profile.getSupFreq2());
            specialIntervalDto.setDayOfWeek(profile.getDayOfWeek());
            if (profile.getMoeEhrMedProfile() != null) {
                specialIntervalDto.setCycleMultiplier(profile.getMoeEhrMedProfile().getCycleMultiplier());
            }

            if (profile.getMoeMedMultDoses() != null && profile.getMoeMedMultDoses().size() == 2) {
                //Odd Day / Even Day Case  (2 freqCode from moeMedMultDose)
                Set<MoeMedMultDosePo> moeMedMultDosesSet = profile.getMoeMedMultDoses();
                for (MoeMedMultDosePo moeMedMultDose : moeMedMultDosesSet) {
                    freqCodeList.add(moeMedMultDose.getFreqCode());
                }

            } else {
                //Others Case (1 freqCode from moeMedProfile)
                freqCodeList.add(profile.getFreqCode());
            }

            specialIntervalDto.setFreqCode(freqCodeList);

            try {
                MoeEhrOrderServiceImpl orderService = new MoeEhrOrderServiceImpl();

                Map<String, Object> resultMap = orderService.generateSpecialIntervalText(specialIntervalDto);

                if (resultMap.get("displayWithFreq") != null) {
                    dto.setDisplayWithFreq((List<String>) resultMap.get("displayWithFreq"));
                }

                if (resultMap.get("specialIntervalText") != null) {
                    dto.setSpecialIntervalText((List<String>) resultMap.get("specialIntervalText"));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        //Chris 20190909 For getOrder return Special Interval's Info (support to "odd/even days" case)  -End

        MoeDrugLocalPo local = DtoUtil.getCorrespondingLocalDrug(dto);
        if (local != null) {
            if ((MoeCommonHelper.isFlagTrue(profile.getMoeEhrMedProfile().getStrengthCompulsory())) ||
                    (MoeCommonHelper.isFlagTrue(dto.getMoeEhrMedProfile().getStrengthCompulsory())
                            && dto.getStrength() != null)) {
                dto.setItemcode(local.getLocalDrugId());
            }
            if (dto.getMoeEhrMedProfile().getVtmId() == null) {
                dto.getMoeEhrMedProfile().setVtmId(local.getVtmId());
            }
            if (dto.getMoeEhrMedProfile().getDrugId() == null) {
                if (MoeCommonHelper.isStrengthCompulsoryYes(local.getStrengthCompulsory())) {
                    Set<MoeDrugStrengthLocalPo> strength = local.getMoeDrugStrengths();
                    for (MoeDrugStrengthLocalPo s : strength) {
                        dto.getMoeEhrMedProfile().setDrugId(s.getAmpId());
                    }
                } else {
                    dto.getMoeEhrMedProfile().setDrugId(local.getTradeVtmRouteFormId());
                }
            }
            if (dto.getMoeEhrMedProfile().getTradeNameVtmId() == null
                    && MoeCommonHelper.isAliasNameTypeFreeText(profile.getNameType())
                    && !MoeCommonHelper.isAliasNameTypeLocalDrug(profile.getNameType())) {
                dto.getMoeEhrMedProfile().setTradeNameVtmId(local.getTradeVtmId());
            }
        }
        //Simon 20190814 start -- add preparation list
        if (ServerConstant.STRENGTH_COMPULSORY_YES.equalsIgnoreCase(profile.getMoeEhrMedProfile().getStrengthCompulsory())) {
            dto.setStrength(profile.getStrength());
            if (dto.getStrengths() != null && dto.getStrengths().size() > 0) {
                List<PreparationDto> preparationDtos = new ArrayList<>();
                for (int i = 0; i < dto.getStrengths().size(); i++) {
                    PreparationDto preparationDto = convertPreparationDto(dto.getStrengths().get(i));
                    if (i == 0)
                        preparationDto.setSelected(true);
                    else
                        preparationDto.setSelected(false);
                    preparationDtos.add(preparationDto);
                }
                dto.setPrep(preparationDtos);
            }
        } else {
            List<PreparationDto> strengths = setSelectedPreparation(getStrengths(profile.getMoeEhrMedProfile().getVtm(),
                    profile.getFormDesc(), profile.getMoeEhrMedProfile().getDrugRouteEng(),
                    profile.getTradename(), userDto), profile.getStrength(), profile.getExtraInfo());
            dto.setPrep(strengths);
        }

        //Simon 20190814 end -- add preparation list
        return dto;
    }

    public static List<MoeMedProfileDto> convertMoeMedProfileDtos(Set<MoeMedProfilePo> profiles, UserDto userDto) {
        List<MoeMedProfileDto> dtos = new ArrayList<MoeMedProfileDto>(0);
        if (profiles != null) {
            for (MoeMedProfilePo profile : profiles) {
                dtos.add(convertMoeMedProfileDto(profile, userDto));
            }
        }
        return dtos;
    }

    public static MoeOrderDto convertMoeOrderDto(MoeOrderPo order, boolean orderOnly, UserDto userDto) {
        MoeOrderDto dto = new MoeOrderDto();

        //if(order.getMoeEhrOrder() != null) {
        //dto.setMoeEhrOrder(convertMoeEhrOrderDto(order.getMoeEhrOrder()));
        //}
        if (!orderOnly) {
            dto.setMoeMedProfiles(convertMoeMedProfileDtos(order.getMoeMedProfiles(), userDto));
        }

        dto.setHospcode(order.getHospcode());
        dto.setWorkstore(order.getWorkstore());
        dto.setPatHospcode(order.getPatHospcode());
        dto.setOrdNo(order.getOrdNo());
        dto.setOrdDate(order.getOrdDate());
        dto.setOrdType(order.getOrdType());
        dto.setOrdSubtype(order.getOrdSubtype());
        dto.setOrdStatus(order.getOrdStatus());
        dto.setRefNo(Formatter.getRefNo(order.getOrdNo(), order.getVersion()));
        dto.setMoCode(order.getMoCode());
        dto.setPrevOrdNo(order.getPrevOrdNo());
        dto.setMaxItemNo(order.getMaxItemNo());
        dto.setEndDate(order.getEndDate());
        dto.setSuspend(order.getSuspend());
        dto.setPrivatePatient(order.getPrivatePatient());
        dto.setTransferPatient(order.getTransferPatient());
        dto.setAllowModify(order.getAllowModify());
        dto.setPrescType(order.getPrescType());
        dto.setMoeOrder(order.getMoeOrder());
        dto.setCaseNo(order.getCaseNo());
        dto.setSrcSpecialty(order.getSrcSpecialty());
        dto.setSrcSubspecialty(order.getSrcSubspecialty());
        dto.setIpasWard(order.getIpasWard());
        dto.setBedNo(order.getBedNo());
        dto.setSpecialty(order.getSpecialty());
        dto.setSubspecialty(order.getSubspecialty());
        dto.setPhsWard(order.getPhsWard());
        dto.setEisSpecCode(order.getEisSpecCode());
        dto.setDispHospcode(order.getDispHospcode());
        dto.setDispWorkstore(order.getDispWorkstore());
        dto.setTicknum(order.getTicknum());
        dto.setTickdate(order.getTickdate());
        dto.setWkstatcode(order.getWkstatcode());
        dto.setLastUpdDate(order.getLastUpdDate());
        String user = order.getMoeEhrOrder().getUpdateUser();
        if (StringUtils.isEmpty(user)) {
            user = order.getLastUpdBy();
        }
        dto.setLastUpdBy(user);
        dto.setDispOrdNo(order.getDispOrdNo());
        dto.setDischargeReason(order.getDischargeReason());
        dto.setRemarkCreateBy(order.getRemarkCreateBy());
        dto.setRemarkCreateDate(order.getRemarkCreateDate());
        dto.setRemarkText(order.getRemarkText());
        dto.setRemarkConfirmBy(order.getRemarkConfirmBy());
        dto.setRemarkConfirmDate(order.getRemarkConfirmDate());
        dto.setRemarkStatus(order.getRemarkStatus());
        dto.setLastPrescType(order.getLastPrescType());
        dto.setUncollect(order.getUncollect());
        dto.setAllowPostComment(order.getAllowPostComment());
        dto.setPrintType(order.getPrintType());
        dto.setPrevHospcode(order.getPrevHospcode());
        dto.setPrevWorkstore(order.getPrevWorkstore());
        dto.setPrevDispDate(order.getPrevDispDate());
        dto.setPrevTicknum(order.getPrevTicknum());
        dto.setVersion(order.getVersion());
        return dto;
    }

    public static MoeEhrOrderDto convertMoeEhrOrderDto(MoeEhrOrderPo order, boolean orderOnly, UserDto userDto) {
        MoeEhrOrderDto dto = new MoeEhrOrderDto();

        dto.setMoeOrder(convertMoeOrderDto(order.getMoeOrder(), orderOnly, userDto));

        dto.setOrdNo(order.getOrdNo());
        dto.setDisplayOrdNo(Formatter.getOrdNo(order.getOrdNo()));
        dto.setMoePatientKey(order.getMoePatient().getMoePatientKey());
        dto.setMoeCaseNo(order.getMoePatientCase().getMoeCaseNo());
        dto.setHospcode(order.getHospcode());
        dto.setCreateUserId(order.getCreateUserId());
        dto.setCreateUser(order.getCreateUser());
        dto.setCreateHosp(order.getCreateHosp());
        dto.setCreateRank(order.getCreateRank());
        dto.setCreateRankDesc(order.getCreateRankDesc());
        dto.setUpdateUserId(order.getUpdateUserId());
        dto.setUpdateUser(order.getUpdateUser());
        dto.setUpdateHosp(order.getUpdateHosp());
        dto.setUpdateRank(order.getUpdateRank());
        dto.setUpdateRankDesc(order.getUpdateRankDesc());
        dto.setVersion(order.getMoeOrder().getVersion());

        return dto;
    }

    public static List<MoeEhrOrderDto> convertMoeEhrOrderDtos(List<MoeEhrOrderPo> orders, boolean orderOnly, UserDto userDto) {
        List<MoeEhrOrderDto> dtos = new ArrayList<MoeEhrOrderDto>(0);
        if (orders != null) {
            for (MoeEhrOrderPo order : orders) {
                dtos.add(convertMoeEhrOrderDto(order, orderOnly, userDto));
            }
        }
        return dtos;
    }

    public static MoeEhrAcceptDrugCheckingPo convertMoeEhrAcceptDrugChecking(MoeEhrAcceptDrugCheckingDto dto, MoeOrderPo order) {
        MoeEhrAcceptDrugCheckingPo checking = new MoeEhrAcceptDrugCheckingPo();
        checking.setAcceptId(dto.getAcceptId());
        checking.setButtonLabel(dto.getButtonLabel());
        checking.setRecordDtm(new Date());
        checking.setMoeOrder(order);
        checking.setAllergen(dto.getAllergen());
        checking.setDrugVtm(dto.getDrugVtm());
        checking.setScreenMsg(dto.getScreenMsg());
        return checking;
    }

    public static Set<MoeEhrAcceptDrugCheckingPo> convertMoeEhrAcceptDrugCheckings(List<MoeEhrAcceptDrugCheckingDto> dtos, MoeOrderPo order) {
        Set<MoeEhrAcceptDrugCheckingPo> checkings = new HashSet<MoeEhrAcceptDrugCheckingPo>();
        if (dtos != null) {
            for (MoeEhrAcceptDrugCheckingDto dto : dtos) {
                checkings.add(convertMoeEhrAcceptDrugChecking(dto, order));
            }
        }
        return checkings;
    }

    public static MoeEhrMedAllergenPo convertMoeEhrMedAllergen(MoeEhrMedAllergenDto dto, MoeMedProfilePo profile) {
        MoeEhrMedAllergenPo allergen = new MoeEhrMedAllergenPo();

        allergen.setMoeMedProfile(profile);
        allergen.setHospcode(profile.getHospcode());
        allergen.setOrdNo(profile.getOrdNo());
        allergen.setCmsItemNo(profile.getCmsItemNo());
        allergen.setRowNo(profile.getOrdNo());
        allergen.setAllergen(dto.getAllergen());
        allergen.setMatchType(dto.getMatchType());
        allergen.setScreenMsg(dto.getScreenMsg());
        allergen.setCertainty(dto.getCertainty());
        allergen.setAdditionInfo(dto.getAdditionInfo());
        allergen.setOverrideReason(dto.getOverrideReason());
        allergen.setAckDate(dto.getAckDate());
        allergen.setAckBy(dto.getAckBy());
        allergen.setManifestation(dto.getManifestation());
        allergen.setOverrideStatus(dto.getOverrideStatus());
        return allergen;
    }

    public static Set<MoeEhrMedAllergenPo> convertMoeEhrMedAllergens(List<MoeEhrMedAllergenDto> dtos, MoeMedProfilePo profile) {
        Set<MoeEhrMedAllergenPo> allergens = new TreeSet<MoeEhrMedAllergenPo>();
        if (dtos != null) {
            int count = 1;
            for (MoeEhrMedAllergenDto dto : dtos) {
                MoeEhrMedAllergenPo allergen = convertMoeEhrMedAllergen(dto, profile);
                allergen.setRowNo(count++);
                allergens.add(allergen);
            }
        }
        return allergens;
    }

    public static MoeEhrMedProfilePo convertMoeEhrMedProfile(MoeEhrMedProfileDto dto, MoeMedProfilePo parent) {
        MoeEhrMedProfilePo profile = new MoeEhrMedProfilePo();
        profile.setMoeMedProfile(parent);
        profile.setHospcode(parent.getHospcode());
        profile.setOrdNo(parent.getOrdNo());
        profile.setCmsItemNo(parent.getCmsItemNo());
        profile.setVtm(dto.getVtm());
        profile.setVtmId(dto.getVtmId());
        profile.setTradeName(dto.getTradeName());
        profile.setTradeNameVtmId(dto.getTradeNameVtmId());
        profile.setAliasName(dto.getAliasName());
        profile.setAliasNameType(dto.getAliasNameType());
        profile.setManufacturer(dto.getManufacturer());
        profile.setScreenDisplay(dto.getScreenDisplay());
        //chris 20190725 start
        if (dto.getOrderLineType() != null) {
            profile.setOrderLineType(dto.getOrderLineType());
        }
        //chris 20190725 end
        profile.setGenericIndicator(dto.getGenericIndicator());
        profile.setCycleMultiplier(dto.getCycleMultiplier());
        profile.setStrengthCompulsory(dto.getStrengthCompulsory());
        profile.setCreateUserId(dto.getCreateUserId());
        profile.setCreateUser(dto.getCreateUser());
        profile.setCreateHosp(dto.getCreateHosp());
        profile.setCreateRank(dto.getCreateRank());
        profile.setCreateRankDesc(dto.getCreateRankDesc());
        profile.setUpdateUserId(dto.getUpdateUserId());
        profile.setUpdateUser(dto.getUpdateUser());
        profile.setUpdateHosp(dto.getUpdateHosp());
        profile.setUpdateRank(dto.getUpdateRank());
        profile.setUpdateRankDesc(dto.getUpdateRankDesc());
        profile.setDrugId(dto.getDrugId());
        profile.setFormId(dto.getFormId());
        profile.setDoseFormExtraInfoId(dto.getDoseFormExtraInfoId());
        profile.setDoseFormExtraInfo(dto.getDoseFormExtraInfo());
        profile.setRouteId(dto.getRouteId());
        profile.setFreqId(dto.getFreqId());
        profile.setSupplFreqId(dto.getSupplFreqId());
        profile.setMoQtyUnitId(dto.getMoQtyUnitId());
        profile.setModuId(dto.getModuId());
        profile.setSiteId(dto.getSiteId());
        profile.setDrugRouteEng(dto.getDrugRouteEng());
        profile.setDrugRouteId(dto.getDrugRouteId());
        profile.setStrengthLevelExtraInfo(dto.getStrengthLevelExtraInfo());
        profile.setMedDiscontFlag(dto.getMedDiscontFlag());
        profile.setMedDiscontReasonCode(dto.getMedDiscontReasonCode());
        profile.setMedDiscontInformation(dto.getMedDiscontInfo());
        profile.setMedDiscontUserId(dto.getMedDiscontUserId());
        profile.setMedDiscontUser(dto.getMedDiscontUser());
        profile.setMedDiscontHosp(dto.getMedDiscontHosp());
        profile.setMedDiscontDtm(dto.getMedDiscontDtm());
        return profile;
    }

    public static MoeEhrMedProfilePo convertMoeEhrMedProfileAllParam(MoeEhrMedProfileDto dto, MoeMedProfilePo parent) {
        MoeEhrMedProfilePo profile = new MoeEhrMedProfilePo();
        profile.setHospcode(parent.getHospcode());
        profile.setOrdNo(parent.getOrdNo());
        profile.setCmsItemNo(parent.getCmsItemNo());
        profile.setMoeMedProfile(parent);
        profile.setDrugId(dto.getDrugId());
        profile.setVtm(dto.getVtm());
        profile.setVtmId(dto.getVtmId());
        profile.setTradeName(dto.getTradeName());
        profile.setTradeNameVtmId(dto.getTradeNameVtmId());
        profile.setAliasName(dto.getAliasName());
        profile.setAliasNameType(dto.getAliasNameType());
        profile.setManufacturer(dto.getManufacturer());
        profile.setScreenDisplay(dto.getScreenDisplay());
        //chris 20190725 start
        if (dto.getOrderLineType() != null) {
            profile.setOrderLineType(dto.getOrderLineType());
        }
        //chris 20190725 end
        profile.setGenericIndicator(dto.getGenericIndicator());
        profile.setStrengthCompulsory(dto.getStrengthCompulsory());
        profile.setCycleMultiplier(dto.getCycleMultiplier());
        profile.setFormId(dto.getFormId());
        profile.setDoseFormExtraInfoId(dto.getDoseFormExtraInfoId());
        profile.setDoseFormExtraInfo(dto.getDoseFormExtraInfo());
        profile.setRouteId(dto.getRouteId());
        profile.setFreqId(dto.getFreqId());
        profile.setSupplFreqId(dto.getSupplFreqId());
        profile.setMoQtyUnitId(dto.getMoQtyUnitId());
        profile.setModuId(dto.getModuId());
        profile.setSiteId(dto.getSiteId());
        profile.setDrugRouteEng(dto.getDrugRouteEng());
        profile.setDrugRouteId(dto.getDrugRouteId());
        profile.setStrengthLevelExtraInfo(dto.getStrengthLevelExtraInfo());
        profile.setCreateUserId(dto.getCreateUserId());
        profile.setCreateUser(dto.getCreateUser());
        profile.setCreateHosp(dto.getCreateHosp());
        profile.setCreateRank(dto.getCreateRank());
        profile.setCreateRankDesc(dto.getCreateRankDesc());
        profile.setCreateDtm(dto.getCreateDtm());
        profile.setUpdateUserId(dto.getUpdateUserId());
        profile.setUpdateUser(dto.getUpdateUser());
        profile.setUpdateHosp(dto.getUpdateHosp());
        profile.setUpdateRank(dto.getUpdateRank());
        profile.setUpdateRankDesc(dto.getUpdateRankDesc());
        profile.setUpdateDtm(dto.getUpdateDtm());
        profile.setMedDiscontFlag(dto.getMedDiscontFlag());
        profile.setMedDiscontReasonCode(dto.getMedDiscontReasonCode());
        profile.setMedDiscontInformation(dto.getMedDiscontInfo());
        profile.setMedDiscontUserId(dto.getMedDiscontUserId());
        profile.setMedDiscontUser(dto.getMedDiscontUser());
        profile.setMedDiscontHosp(dto.getMedDiscontHosp());
        profile.setMedDiscontDtm(dto.getMedDiscontDtm());

        return profile;
    }

    public static MoeEhrMedMultDosePo convertMoeEhrMedMultDose(MoeEhrMedMultDoseDto dto, MoeMedMultDosePo parent) {
        MoeEhrMedMultDosePo dose = new MoeEhrMedMultDosePo();
        dose.setMoeMedMultDose(parent);
        dose.setHospcode(parent.getHospcode());
        dose.setOrdNo(parent.getOrdNo());
        dose.setCmsItemNo(parent.getCmsItemNo());
        dose.setStepNo(parent.getStepNo());
        dose.setMultDoseNo(parent.getMultDoseNo());
        dose.setFreqId(dto.getFreqId());
        dose.setSupplFreqId(dto.getSupplFreqId());
        dose.setMoQtyUnitId(dto.getMoQtyUnitId());
        return dose;
    }

    //chris 20190802 Add SupFreqText generate from Formatter  -Start
    //public static MoeMedMultDosePo convertMoeMedMultDose(MoeMedMultDoseDto dto, MoeMedProfilePo parent, int stepNo, MoeMedProfileDto profileDto) {
    //chris 20190802 Add SupFreqText generate from Formatter  -End
    public static MoeMedMultDosePo convertMoeMedMultDose(MoeMedMultDoseDto dto, MoeMedProfilePo parent, int stepNo) {
        MoeMedMultDosePo dose = new MoeMedMultDosePo();

        dose.setMoeMedProfile(parent);
        dose.setHospcode(parent.getHospcode());
        dose.setOrdNo(parent.getOrdNo());
        dose.setCmsItemNo(parent.getCmsItemNo());
        dose.setStepNo(stepNo);
        dose.setMultDoseNo(stepNo);

        dose.setPatHospcode(parent.getPatHospcode());
        if (dto.getDosage() != null) {
            dose.setDosage(BigDecimal.valueOf(dto.getDosage()));
        }
        dose.setFreqCode(dto.getFreqCode());
        dose.setFreq1(dto.getFreq1());
        dose.setSupFreqCode(dto.getSupFreqCode());
        dose.setSupFreq1(dto.getSupFreq1());
        dose.setSupFreq2(dto.getSupFreq2());
        dose.setDayOfWeek(dto.getDayOfWeek());
        dose.setAdminTimeCode(dto.getAdminTimeCode());
        dose.setPrn(dto.getPrn());
        dose.setPrnPercent(dto.getPrnPercent());
        dose.setSiteCode(dto.getSiteCode());
        dose.setSupSiteDesc(dto.getSupSiteDesc());
        dose.setDuration(dto.getDuration());
        dose.setDurationUnit(dto.getDurationUnit());
        dose.setStartDate(dto.getStartDate());
        dose.setEndDate(dto.getEndDate());
        dose.setMoQty(dto.getMoQty());
        dose.setMoQtyUnit(dto.getMoQtyUnit());
        dose.setUpdateBy(dto.getUpdateBy());
        dose.setUpdateTime(dto.getUpdateTime());
        dose.setFreqText(dto.getFreqText());

        dose.setSupFreqText(dto.getSupFreqText());
        //chris 20190802 Add SupFreqText generate from Formatter  -Start
        //dose.setSupFreqText(Formatter.toRecurText(profileDto).get(1));
        //chris 20190802 Add SupFreqText generate from Formatter  -Start

        dose.setCapdSystem(dto.getCapdSystem());
        dose.setCapdCalcium(dto.getCapdCalcium());
        dose.setCapdConc(dto.getCapdConc());
        dose.setItemcode(dto.getItemcode());
        dose.setCapdBaseunit(dto.getCapdBaseunit());
        dose.setSiteDesc(dto.getSiteDesc());
        dose.setTrueConc(dto.getTrueConc());
        dose.setAdminTimeDesc(dto.getAdminTimeDesc());
        dose.setCapdChargeInd(dto.getCapdChargeInd());
        dose.setCapdChargeableUnit(dto.getCapdChargeableUnit());
        dose.setCapdChargeableAmount(dto.getCapdChargeableAmount());
        dose.setConfirmCapdChargeInd(dto.getConfirmCapdChargeInd());
        dose.setConfirmCapdChargeUnit(dto.getConfirmCapdChargeUnit());

        if (dto.getMoeEhrMedMultDose() != null) {
            dose.setMoeEhrMedMultDose(convertMoeEhrMedMultDose(dto.getMoeEhrMedMultDose(), dose));
        }

        return dose;
    }

    //chris 20190802 For SupFreqText Formatter  -Start
    //public static Set<MoeMedMultDosePo> convertMoeMedMultDoses(List<MoeMedMultDoseDto> dtos, MoeMedProfilePo parent, MoeMedProfileDto profileDto) {
    //chris 20190802 For SupFreqText Formatter  -End
    public static Set<MoeMedMultDosePo> convertMoeMedMultDoses(List<MoeMedMultDoseDto> dtos, MoeMedProfilePo parent) {
        Set<MoeMedMultDosePo> doses = new TreeSet<MoeMedMultDosePo>();
        if (dtos != null) {
            int count = 1;
            for (MoeMedMultDoseDto dto : dtos) {
                doses.add(convertMoeMedMultDose(dto, parent, count++));
                //chris 20190802 For SupFreqText Formatter  -Start
                //doses.add(convertMoeMedMultDose(dto, parent, count++, profileDto));
                //chris 20190802 For SupFreqText Formatter  -End
            }
        }
        return doses;
    }

    public static MoeMedProfilePo convertMoeMedProfile(MoeMedProfileDto dto, MoeOrderPo order) {
        MoeMedProfilePo profile = new MoeMedProfilePo();

        //Chris 20190926  order.getHospcode()=null  -Start
        //profile.setHospcode(order.getHospcode());
        profile.setHospcode(order.getHospcode() != null ? order.getHospcode() : dto.getHospcode());
        //Chris 20190926  order.getHospcode()=null  -End
        profile.setCmsItemNo(dto.getCmsItemNo());
        profile.setOrdNo(order.getOrdNo());
        profile.setMoeOrder(order);
        profile.setPatHospcode(order.getPatHospcode());
        profile.setCaseNo(order.getCaseNo());
        profile.setRegimen(dto.getRegimen());
        profile.setMultDose(dto.getMultDose());
        profile.setVolValue(dto.getVolValue());
        profile.setVolUnit(dto.getVolUnit());
        profile.setVolText(dto.getVolText());
        if (dto.getMoeEhrMedProfile().getStrengthCompulsory() != null &&
                (MoeCommonHelper.isStrengthCompulsoryYes(dto.getMoeEhrMedProfile().getStrengthCompulsory())
                        || (MoeCommonHelper.isStrengthCompulsoryNo(dto.getMoeEhrMedProfile().getStrengthCompulsory())
                        && !(dto.getStrength() == null
                        && dto.getMoeEhrMedProfile().getStrengthLevelExtraInfo() == null)))) {
            MoeDrugLocalPo local = DtoUtil.getCorrespondingLocalDrug(dto);
            if (local != null) {
                profile.setItemcode(local.getLocalDrugId());
            }
        }
        profile.setFirstDisplayname(dto.getFirstDisplayname());
        profile.setTradename(dto.getTradename());
        profile.setStrength(dto.getStrength());
        profile.setBaseunit(dto.getBaseunit());
        profile.setFormcode(dto.getFormcode());
        profile.setRouteCode(dto.getRouteCode());
        profile.setRouteDesc(dto.getRouteDesc());
        profile.setFormDesc(dto.getFormDesc());
        profile.setSalt(dto.getSalt());
        profile.setExtraInfo(dto.getExtraInfo());
        profile.setDangerdrug(dto.getDangerdrug());
        profile.setExternalUse(dto.getExternalUse());
        profile.setNameType(dto.getNameType());
        profile.setSecondDisplayname(dto.getSecondDisplayname());
        if (dto.getDosage() != null) {
            profile.setDosage(dto.getDosage());
            //profile.setDosage(BigDecimal.valueOf((long)(dto.getDosage().doubleValue() * 10000), 4));
        }
        profile.setModu(dto.getModu());
        profile.setFreqCode(dto.getFreqCode());
        profile.setFreq1(dto.getFreq1());
        profile.setSupFreqCode(dto.getSupFreqCode());
        profile.setSupFreq1(dto.getSupFreq1());
        profile.setSupFreq2(dto.getSupFreq2());
        profile.setDayOfWeek(dto.getDayOfWeek());
        profile.setAdminTimeCode(dto.getAdminTimeCode());
        profile.setPrn(dto.getPrn());
        profile.setPrnPercent(dto.getPrnPercent());
        profile.setSiteCode(dto.getSiteCode());
        profile.setSupSiteDesc(dto.getSupSiteDesc());
        profile.setDuration(dto.getDuration());
        profile.setDurationUnit(dto.getDurationUnit());
        profile.setStartDate(dto.getStartDate());
        profile.setEndDate(dto.getEndDate());
        profile.setMoQty(dto.getMoQty());
        profile.setMoQtyUnit(dto.getMoQtyUnit());
        profile.setActionStatus(dto.getActionStatus());
        profile.setFormulStatus(dto.getFormulStatus());
        profile.setFormulStatus2(dto.getFormulStatus2());
        profile.setPatType(dto.getPatType());
        profile.setItemStatus(dto.getItemStatus());
        profile.setSpecInstruct(dto.getSpecInstruct());
        profile.setSpecNote(dto.getSpecNote());
        profile.setTranslate(dto.getTranslate());
        profile.setFixPeriod(dto.getFixPeriod());
        profile.setRestricted(dto.getRestricted());
        profile.setSingleUse(dto.getSingleUse());
        profile.setMoCreate(dto.getMoCreate());
        profile.setDisplayRoutedesc(dto.getDisplayRoutedesc());
        profile.setTrueDisplayname(dto.getTrueDisplayname());
        profile.setOrgItemNo(dto.getOrgItemNo());
        profile.setAllowRepeat(dto.getAllowRepeat());
        profile.setFreqText(dto.getFreqText());

        profile.setSupFreqText(dto.getSupFreqText());
        //chris 20190802 Add SupFreqText generate from GWT Formatter  -Start
        //profile.setSupFreqText(Formatter.toRecurText(dto).get(0));
        //chris 20190802 Add SupFreqText generate from GWT Formatter  -End

        profile.setTrueAliasname(dto.getTrueAliasname());
        profile.setCapdSystem(dto.getCapdSystem());
        profile.setCapdCalcium(dto.getCapdCalcium());
        profile.setCapdConc(dto.getCapdConc());
        profile.setDurationInputType(dto.getDurationInputType());
        profile.setCapdBaseunit(dto.getCapdBaseunit());
        profile.setSiteDesc(dto.getSiteDesc());
        profile.setRemarkCreateBy(dto.getRemarkCreateBy());
        profile.setRemarkCreateDate(dto.getRemarkCreateDate());
        profile.setRemarkText(dto.getRemarkText());
        profile.setRemarkConfirmBy(dto.getRemarkConfirmBy());
        profile.setRemarkConfirmDate(dto.getRemarkConfirmDate());
        profile.setTrueConc(dto.getTrueConc());
        profile.setMdsInfo(dto.getMdsInfo());
        profile.setAdminTimeDesc(dto.getAdminTimeDesc());
        profile.setCommentCreateBy(dto.getCommentCreateBy());
        profile.setCommentCreateDate(dto.getCommentCreateDate());
        profile.setUpdateBy(dto.getUpdateBy());
        profile.setUpdateTime(dto.getUpdateTime());

        profile.setMoeEhrMedProfile(convertMoeEhrMedProfile(dto.getMoeEhrMedProfile(), profile));

        if (dto.getMoeEhrMedAllergens() != null) {
            profile.setMoeEhrMedAllergens(convertMoeEhrMedAllergens(dto.getMoeEhrMedAllergens(), profile));
        }

        if (dto.getMoeMedMultDoses() != null) {
            profile.setMoeMedMultDoses(convertMoeMedMultDoses(dto.getMoeMedMultDoses(), profile));
            //Chris 20190802 For SupFreqText Formatter  -Start
            //profile.setMoeMedMultDoses(convertMoeMedMultDoses(dto.getMoeMedMultDoses(), profile, dto));
            //Chris 20190802 For SupFreqText Formatter  -End
        }

        return profile;
    }

    public static Set<MoeMedProfilePo> convertMoeMedProfiles(List<MoeMedProfileDto> dtos, MoeOrderPo order) {
        Set<MoeMedProfilePo> profiles = new TreeSet<MoeMedProfilePo>();
        if (dtos != null) {
            long count = 1;
            for (MoeMedProfileDto dto : dtos) {
                dto.setOrgItemNo(count++);
                profiles.add(convertMoeMedProfile(dto, order));
            }
        }
        return profiles;
    }

    public static MoeOrderPo convertMoeOrder(MoeOrderDto dto) {
        MoeOrderPo order = new MoeOrderPo();

        order.setHospcode(dto.getHospcode());
        order.setWorkstore(dto.getWorkstore());
        order.setPatHospcode(dto.getPatHospcode());
        order.setOrdNo(dto.getOrdNo());
        order.setOrdType(dto.getOrdType());
        order.setOrdSubtype(dto.getOrdSubtype());
        order.setOrdStatus(dto.getOrdStatus());
        //order.setRefNo(dto.getRefNo());
        order.setMoCode(dto.getMoCode());
        order.setPrevOrdNo(dto.getPrevOrdNo());
        order.setMaxItemNo(dto.getMaxItemNo());
        order.setEndDate(dto.getEndDate());
        order.setSuspend(dto.getSuspend());
        order.setPrivatePatient(dto.getPrivatePatient());
        order.setTransferPatient(dto.getTransferPatient());
        order.setAllowModify(dto.getAllowModify());
        order.setPrescType(dto.getPrescType());
        order.setMoeOrder(dto.getMoeOrder());
        order.setCaseNo(dto.getCaseNo());
        order.setSrcSpecialty(dto.getSrcSpecialty());
        order.setSrcSubspecialty(dto.getSrcSubspecialty());
        order.setIpasWard(dto.getIpasWard());
        order.setBedNo(dto.getBedNo());
        order.setSpecialty(dto.getSpecialty());
        order.setSubspecialty(dto.getSubspecialty());
        order.setPhsWard(dto.getPhsWard());
        order.setEisSpecCode(dto.getEisSpecCode());
        order.setDispHospcode(dto.getDispHospcode());
        order.setDispWorkstore(dto.getDispWorkstore());
        order.setTicknum(dto.getTicknum());
        order.setTickdate(dto.getTickdate());
        order.setWkstatcode(dto.getWkstatcode());
        order.setLastUpdDate(dto.getLastUpdDate());
        order.setLastUpdBy(dto.getLastUpdBy());
        order.setDispOrdNo(dto.getDispOrdNo());
        order.setDischargeReason(dto.getDischargeReason());
        order.setRemarkCreateBy(dto.getRemarkCreateBy());
        order.setRemarkCreateDate(dto.getRemarkCreateDate());
        order.setRemarkText(dto.getRemarkText());
        order.setRemarkConfirmBy(dto.getRemarkConfirmBy());
        order.setRemarkConfirmDate(dto.getRemarkConfirmDate());
        order.setRemarkStatus(dto.getRemarkStatus());
        order.setLastPrescType(dto.getLastPrescType());
        order.setUncollect(dto.getUncollect());
        order.setAllowPostComment(dto.getAllowPostComment());
        order.setPrintType(dto.getPrintType());
        order.setPrevHospcode(dto.getPrevHospcode());
        order.setPrevWorkstore(dto.getPrevWorkstore());
        order.setPrevDispDate(dto.getPrevDispDate());
        order.setPrevTicknum(dto.getPrevTicknum());
        order.setVersion(dto.getVersion());
        order.setBackDate(dto.getBackDate());
        order.setMoeMedProfiles(convertMoeMedProfiles(dto.getMoeMedProfiles(), order));

/*		if(dto.getMoeEhrAcceptDrugCheckings() != null) {
			order.setMoeEhrAcceptDrugCheckings(convertMoeEhrAcceptDrugCheckings(dto.getMoeEhrAcceptDrugCheckings(), order));
		}
		*/
        return order;
    }

    public static MoeEhrOrderPo convertMoeEhrOrder(MoeEhrOrderDto dto) {
        MoeEhrOrderPo order = new MoeEhrOrderPo();

        order.setMoeOrder(convertMoeOrder(dto.getMoeOrder()));

        order.setOrdNo(dto.getOrdNo());
        MoePatientPo patient = new MoePatientPo();
        patient.setMoePatientKey(dto.getMoePatientKey());
        order.setMoePatient(patient);
        MoePatientCasePo patientCase = new MoePatientCasePo();
        patientCase.setMoeCaseNo(dto.getMoeCaseNo());
        order.setMoePatientCase(patientCase);
        order.setHospcode(dto.getHospcode());
        order.setCreateUserId(dto.getCreateUserId());
        order.setCreateUser(dto.getCreateUser());
        order.setCreateHosp(dto.getCreateHosp());
        order.setCreateRank(dto.getCreateRank());
        order.setCreateRankDesc(dto.getCreateRankDesc());
        order.setUpdateUserId(dto.getUpdateUserId());
        order.setUpdateUser(dto.getUpdateUser());
        order.setUpdateHosp(dto.getUpdateHosp());
        order.setUpdateRank(dto.getUpdateRank());
        order.setUpdateRankDesc(dto.getUpdateRankDesc());
        order.getMoeOrder().setMoeEhrOrder(order);

        return order;
    }

    public static MoeRouteDto convertMoeRouteDto(MoeRoutePo route) {
        MoeRouteDto dto = new MoeRouteDto();
        dto.setRouteId(route.getRouteId());
        dto.setRouteEng(route.getRouteEng());
        dto.setRouteChi(route.getRouteChi());
        dto.setRouteOtherName(route.getRouteOtherName());
        dto.setRouteMultiplier(route.getRouteMultiplier());
        dto.setEnableInRegimen(route.getEnableInRegimen());
        return dto;
    }

    public static MoeBaseUnitDto convertMoeBaseUnitDto(MoeBaseUnitPo baseUnit) {
        MoeBaseUnitDto dto = new MoeBaseUnitDto();
        dto.setBaseUnitId(baseUnit.getBaseUnitId());
        dto.setBaseUnit(baseUnit.getBaseUnit());
        return dto;
    }

    public static MoeDrugStrengthDto convertMoeDrugStrengthDto(MoeDrugStrengthPo str) {
        MoeDrugStrengthDto dto = new MoeDrugStrengthDto();
        dto.setStrength(str.getStrength());
        dto.setStrengthLevelExtraInfo(str.getStrengthLevelExtraInfo());
        dto.setAmp(str.getAmp());
        dto.setAmpId(str.getAmpId());
        dto.setVmp(str.getVmp());
        dto.setVmpId(str.getVmpId());
        return dto;
    }

    public static MoeMedMultDoseDto convertMoeMedMultDoseDto(MoeMyFavouriteMultDosePo dose) {
        MoeMedMultDoseDto dto = new MoeMedMultDoseDto();

        dto.setStepNo(dose.getStepNo());
        dto.setMultDoseNo(dose.getMultDoseNo());

        if (dose.getDosage() != null) {
            dto.setDosage(WsFormatter.toDouble(dose.getDosage()));
        }

        dto.setFreqText(dose.getFreqText());
        dto.setSupFreqText(dose.getSupplFreqText());
        dto.setFreqCode(dose.getFreqCode());
        dto.setFreq1(dose.getFreqValue1());
        dto.setSupFreqCode(dose.getSupplFreqEng());
        dto.setSupFreq1(dose.getSupplFreqValue1());
        dto.setSupFreq2(dose.getSupplFreqValue2());

        dto.setDuration(dose.getDuration());
        dto.setDurationUnit(dose.getDurationUnit());

        dto.setDayOfWeek(dose.getDayOfWeek());
        dto.setPrn(dose.getPrn());
        dto.setSiteCode(dose.getSiteEng());
        dto.setSupSiteDesc(dose.getSupSiteEng());

        dto.setStartDate(dose.getStartDate());
        dto.setEndDate(dose.getEndDate());
        dto.setMoQty(dose.getMoQty());
        dto.setMoQtyUnit(dose.getMoQtyUnit());
        //dto.setUpdateBy(dose.getUpdateBy());

        dto.setCapdSystem(dose.getCapdSystem());
        dto.setCapdCalcium(dose.getCapdCalcium());
        dto.setCapdConc(dose.getCapdConc());
        dto.setCapdBaseunit(dose.getCapdBaseunit());
        dto.setSiteDesc(dose.getSiteEng());
        dto.setTrueConc(dose.getTrueConc());
        dto.setCapdChargeInd(dose.getCapdChargeInd());
        dto.setCapdChargeableUnit(dose.getCapdChargeableUnit());
        dto.setCapdChargeableAmount(dose.getCapdChargeableAmount());
        dto.setConfirmCapdChargeInd(dose.getConfirmCapdChargeInd());
        dto.setConfirmCapdChargeUnit(dose.getConfirmCapdChargeUnit());

        dto.getMoeEhrMedMultDose().setFreqId(dose.getFreqId());
        dto.getMoeEhrMedMultDose().setSupplFreqId(dose.getSupplFreqId());

        return dto;
    }

    public static MoeMedProfileDto convertMoeMedProfileDto(MoeMyFavouriteDetailPo detail, String enableHkmttIndicator, Integer updateIndicator, UserDto userDto) {
        MoeMedProfileDto dto = new MoeMedProfileDto();

        dto.setOrgItemNo(detail.getItemNo());
        dto.setNameType(detail.getNameType());
        dto.setTradename(detail.getTradeName());
        dto.setPrn(detail.getPrn());
        dto.setStrength(detail.getPreparation());
        dto.setDangerdrug(detail.getDangerDrug());
        dto.setDuration(detail.getDuration());
        dto.setDurationUnit(detail.getDurationUnit());

        dto.setTrueDisplayname(detail.getVtm());
        dto.setDayOfWeek(detail.getDayOfWeek());
        dto.setSpecInstruct(detail.getSpecInstruct());
        dto.setCapdSystem(detail.getCapdSystem());
        dto.setCapdCalcium(detail.getCapdCalcium());
        dto.setCapdConc(detail.getCapdConc());
        dto.setTrueConc(detail.getCapdTrueConc());
        dto.setRegimen(detail.getRegimen());
        dto.setCapdBaseunit(detail.getCapdBaseunit());
        dto.setActionStatus(detail.getActionStatus());

        // form
        dto.setFormDesc(detail.getFormEng());
        // route
        dto.setRouteDesc(detail.getRouteEng());
        //Simon 20190801--start add for front end;
        dto.setRouteCode(detail.getRouteId() + "");
        //Simon 20190801--end add for front end;
        // site
        dto.setSiteDesc(detail.getSiteEng());
        //Simon 20190808--start add for front end;
        dto.setSiteCode(detail.getSiteId() + "");
        //Simon 20190808--end add for front end;

        // prescribe unit
        if (detail.getDosage() != null) {
//            if (detail.getDosage().intValue() == detail.getDosage().floatValue()) {
//                dto.setDosage(BigDecimal.valueOf(detail.getDosage().intValue()));
//            } else {
//                float v = detail.getDosage().floatValue();
//                dto.setDosage(BigDecimal.valueOf(v));
//            }
            dto.setDosage(detail.getDosage());

        }
        dto.setModu(detail.getDosageUnit());
        // basic unit
        dto.setMoQty(detail.getMoQty());
        dto.setMoQtyUnit(detail.getMoQtyUnit());
        // freq
        dto.setFreqCode(detail.getFreqCode());
        dto.setFreqText(detail.getFreqEng());
        dto.setFreq1(detail.getFreqValue1());

        dto.setSupFreqCode(detail.getSupplFreqEng());
        dto.setSupFreq1(detail.getSupplFreqValue1());
        dto.setSupFreq2(detail.getSupplFreqValue2());
        dto.setSupFreqText(detail.getSupplFreqText());
        // strengthCompulsory
        dto.getMoeEhrMedProfile().setStrengthCompulsory(detail.getStrengthCompulsory());
        // ehr med profile
        dto.getMoeEhrMedProfile().setFormId(detail.getFormId());
        dto.getMoeEhrMedProfile().setRouteId(detail.getRouteId());
        dto.getMoeEhrMedProfile().setSiteId(detail.getSiteId());
        dto.getMoeEhrMedProfile().setFreqId(detail.getFreqId());
        dto.getMoeEhrMedProfile().setSupplFreqId(detail.getSupplFreqId());
        dto.getMoeEhrMedProfile().setMoQtyUnitId(detail.getMoQtyUnitId());
        dto.getMoeEhrMedProfile().setModuId(detail.getDosageUnitId());
        dto.getMoeEhrMedProfile().setDrugRouteId(detail.getDrugRouteId());
        dto.getMoeEhrMedProfile().setDrugRouteEng(detail.getDrugRouteEng());
        if (detail.getNameType() != null &&
                !MoeCommonHelper.isAliasNameTypeVtm(detail.getNameType()) &&
                !MoeCommonHelper.isAliasNameTypeLocalDrug(detail.getNameType()))
            dto.getMoeEhrMedProfile().setAliasNameType(detail.getNameType());
        dto.getMoeEhrMedProfile().setOrderLineType(detail.getOrderLineType());
        dto.getMoeEhrMedProfile().setVtm(detail.getVtm());
        dto.getMoeEhrMedProfile().setTradeName(detail.getTradeName());
        dto.getMoeEhrMedProfile().setAliasName(detail.getAliasName());
        dto.getMoeEhrMedProfile().setGenericIndicator(detail.getGenericIndicator());
        dto.getMoeEhrMedProfile().setManufacturer(detail.getManufacturer());
        dto.getMoeEhrMedProfile().setCycleMultiplier(detail.getCycleMultiplier());
        dto.getMoeEhrMedProfile().setScreenDisplay(detail.getScreenDisplay());
        dto.getMoeEhrMedProfile().setDoseFormExtraInfoId(detail.getDoseFormExtraInfoId());
        dto.getMoeEhrMedProfile().setDoseFormExtraInfo(detail.getDoseFormExtraInfo());
        dto.getMoeEhrMedProfile().setStrengthLevelExtraInfo(detail.getStrengthLevelExtraInfo());

        dto.getMoeEhrMedProfile().setCreateUser(detail.getCreateUser());
        dto.getMoeEhrMedProfile().setCreateUserId(detail.getCreateUserId());
        dto.getMoeEhrMedProfile().setCreateHosp(detail.getCreateHosp());
        dto.getMoeEhrMedProfile().setCreateRank(detail.getCreateRank());
        dto.getMoeEhrMedProfile().setCreateRankDesc(detail.getCreateRankDesc());

        dto.getMoeEhrMedProfile().setUpdateUser(detail.getUpdateUser());
        dto.getMoeEhrMedProfile().setUpdateUserId(detail.getUpdateUserId());
        dto.getMoeEhrMedProfile().setUpdateHosp(detail.getUpdateHosp());
        dto.getMoeEhrMedProfile().setUpdateRank(detail.getUpdateRank());
        dto.getMoeEhrMedProfile().setUpdateRankDesc(detail.getUpdateRankDesc());
        if (detail.getHkmttUpdateDate() != null && "Y".equalsIgnoreCase(enableHkmttIndicator) && updateIndicator != null) {
            if ((new Date().getTime() - detail.getHkmttUpdateDate().getTime()) / (1000 * 60 * 60 * 24) <= updateIndicator) {
                dto.getMoeEhrMedProfile().setHkmttUpdateDate(detail.getHkmttUpdateDate());
            }
        }
        // dosage
        if (detail.getMoeMyFavouriteMultDoses() != null) {
            for (MoeMyFavouriteMultDosePo dose : detail.getMoeMyFavouriteMultDoses()) {
                dto.addMoeMedMultDose(convertMoeMedMultDoseDto(dose));
            }
        }

        // Simon 20190906 add preparation list start--
        if (ServerConstant.STRENGTH_COMPULSORY_YES.equalsIgnoreCase(detail.getStrengthCompulsory())) {
            dto.setStrength(detail.getPreparation());
            if (dto.getStrengths() != null && dto.getStrengths().size() > 0) {
                List<PreparationDto> preparationDtos = new ArrayList<>();
                for (int i = 0; i < dto.getStrengths().size(); i++) {
                    PreparationDto preparationDto = convertPreparationDto(dto.getStrengths().get(i));
                    if (i == 0)
                        preparationDto.setSelected(true);
                    else
                        preparationDto.setSelected(false);
                    preparationDtos.add(preparationDto);
                }
                dto.setPrep(preparationDtos);
            }
        } else {
            List<PreparationDto> strengths = setSelectedPreparation(getStrengths(detail.getVtm(),
                    detail.getFormEng(), detail.getDrugRouteEng(),
                    detail.getTradeName(), userDto), detail.getPreparation(), detail.getStrengthLevelExtraInfo());
            dto.setPrep(strengths);
        }
        //Simon 20190814 add preparation list end --


        //Chris 20191010 For ListMyFavourite return max duration  -Start
        dto.setMaxDuration(getMaxDuration(dto) != null ? getMaxDuration(dto) : null);
        //Chris 20191010 For ListMyFavourite return max duration  -End

        //Chris 20191010 For ListMyFavourite return min dosage  -Start
        dto.setMinDosages(getMinDosage(dto) != null ? getMinDosage(dto) : null);
        //Chris 20191010 For ListMyFavourite return min dosage  -End

        //Chris 20191014 For ListMyFavourite return min dosage message  -Start
        dto.setMinDosagesMessage(getMinDosageString(dto) != null ? getMinDosageString(dto) : null);
        //Chris 20191014 For ListMyFavourite return min dosage message  -End


        //Chris 20190919 For ListMyFavourite return Special Interval's Info (support to "odd/even days" case)  -Start
        if (detail.getSupplFreqEng() != null && !"".equals(detail.getSupplFreqEng())) {
            //Special Interval Model

            InnerSpecialIntervalDto specialIntervalDto = new InnerSpecialIntervalDto();
            List<String> freqCodeList = new ArrayList<String>();

            specialIntervalDto.setSupFreqCode(detail.getSupplFreqEng());
            specialIntervalDto.setRegimen(detail.getRegimen());
            specialIntervalDto.setSupFreq1(detail.getSupplFreqValue1());
            specialIntervalDto.setSupFreq2(detail.getSupplFreqValue2());
            specialIntervalDto.setDayOfWeek(detail.getDayOfWeek());
            specialIntervalDto.setCycleMultiplier(detail.getCycleMultiplier());

            if (detail.getMoeMyFavouriteMultDoses() != null && detail.getMoeMyFavouriteMultDoses().size() == 2) {
                //Odd Day / Even Day Case  (2 freqCode from moeMedMultDose)
                Set<MoeMyFavouriteMultDosePo> moeMyFavouriteMultDosesSet = detail.getMoeMyFavouriteMultDoses();
                for (MoeMyFavouriteMultDosePo moeMyFavouriteMultDose : moeMyFavouriteMultDosesSet) {
                    freqCodeList.add(moeMyFavouriteMultDose.getFreqCode());
                }
            } else {
                //Others Case (1 freqCode from moeMedProfile)
                freqCodeList.add(detail.getFreqCode());
            }

            specialIntervalDto.setFreqCode(freqCodeList);

            try {
                MoeEhrOrderServiceImpl orderService = new MoeEhrOrderServiceImpl();

                Map<String, Object> resultMap = orderService.generateSpecialIntervalText(specialIntervalDto);

                if (resultMap.get("displayWithFreq") != null) {
                    dto.setDisplayWithFreq((List<String>) resultMap.get("displayWithFreq"));
                }

                if (resultMap.get("specialIntervalText") != null) {
                    dto.setSpecialIntervalText((List<String>) resultMap.get("specialIntervalText"));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        //Chris 20190919 For ListMyFavourite return Special Interval's Info (support to "odd/even days" case)  -End

        return dto;
    }

    public static MoeMyFavouriteHdrDto convertMoeMyFavouriteHdrDto(MoeMyFavouriteHdrPo hdr, String enableHkmttIndicator, Integer updateIndicator, UserDto userDto) {
        MoeMyFavouriteHdrDto dto = new MoeMyFavouriteHdrDto();
        dto.setFrontMyFavouriteId(hdr.getFrontMyFavouriteId());
        dto.setMyFavouriteId(hdr.getMyFavouriteId());
        dto.setCreateUserId(hdr.getCreateUserId());
        dto.setMyFavouriteName(hdr.getMyFavouriteName());
        dto.setCreateUser(hdr.getCreateUser());
        dto.setCreateHosp(hdr.getCreateHosp());
        dto.setCreateRank(hdr.getCreateRank());
        dto.setCreateRankDesc(hdr.getCreateRankDesc());
        dto.setCreateDate(hdr.getCreateDate());
        dto.setUpdateUserId(hdr.getUpdateUserId());
        dto.setUpdateUser(hdr.getUpdateUser());
        dto.setUpdateHosp(hdr.getUpdateHosp());
        dto.setUpdateRank(hdr.getUpdateRank());
        dto.setUpdateDate(hdr.getUpdateDate());
        dto.setUpdateRankDesc(hdr.getUpdateRankDesc());
        dto.setVersion(hdr.getVersion());
        dto.setSpecialtyForMyFavourite(hdr.getSpecialtyForMyFavourite());
        // Ricci 20191111 start --
        dto.setCacheId(hdr.getCacheId());
        dto.setIsCache(hdr.getIsCache());
        // Ricci 20191111 end --

        if (hdr.getMoeMyFavouriteDetails() != null) {
            for (MoeMyFavouriteDetailPo detail : hdr.getMoeMyFavouriteDetails()) {
                dto.addMoeMedProfile(convertMoeMedProfileDto(detail, enableHkmttIndicator, updateIndicator, userDto));
            }
        }
        return dto;
    }

    public static List<MoeMyFavouriteHdrDto> convertMoeMyFavouriteHdrDtos(List<MoeMyFavouriteHdrPo> hdrs, String enableHkmttIndicator, Integer updateIndicator, UserDto userDto) {
        List<MoeMyFavouriteHdrDto> dtos = new ArrayList<MoeMyFavouriteHdrDto>(0);
        if (hdrs != null) {
            for (MoeMyFavouriteHdrPo hdr : hdrs) {
                MoeMyFavouriteHdrDto dto = convertMoeMyFavouriteHdrDto(hdr, enableHkmttIndicator, updateIndicator, userDto);
                for (MoeMedProfileDto med : dto.getMoeMedProfiles()) {
                    if (med.getMoeEhrMedProfile().isCurrentExist()) {
                        MoeDrugLocalPo local = DtoUtil.getCorrespondingLocalDrug(med);
                        if (local != null) {
                            if ((MoeCommonHelper.isFlagTrue(med.getMoeEhrMedProfile().getStrengthCompulsory())) ||
                                    (MoeCommonHelper.isFlagFalse(med.getMoeEhrMedProfile().getStrengthCompulsory())
                                            && med.getStrength() != null)) {
                                med.setItemcode(local.getLocalDrugId());
                            }
                            med.getMoeEhrMedProfile().setVtmId(local.getVtmId());
                            if (MoeCommonHelper.isStrengthCompulsoryYes(local.getStrengthCompulsory())
                                    || med.getStrength() != null
                                    || med.getMoeEhrMedProfile().getStrengthLevelExtraInfo() != null) {
                                Set<MoeDrugStrengthLocalPo> strength = local.getMoeDrugStrengths();
                                for (MoeDrugStrengthLocalPo s : strength) {
                                    med.getMoeEhrMedProfile().setDrugId(s.getAmpId());
                                }
                            } else {
                                med.getMoeEhrMedProfile().setDrugId(local.getTradeVtmRouteFormId());
                            }
                            med.getMoeEhrMedProfile().setTradeNameVtmId(local.getTradeVtmId());
                        }
                    }
                }
                dtos.add(dto);
            }
        }
        return dtos;
    }

    public static MoeMyFavouriteMultDosePo convertMoeMyFavouriteMultDose(MoeMedMultDoseDto dto) {
        MoeMyFavouriteMultDosePo dose = new MoeMyFavouriteMultDosePo();
        if (dto.getDosage() != null) {
            dose.setDosage(BigDecimal.valueOf(dto.getDosage()));
        }
        // Simon 20190719 start-- add
        //dose.setStepNo(dto.getStepNo());
        //dose.setMultDoseNo(dto.getMultDoseNo());
        // Simon 20190719 end-- add
        dose.setFreqId(dto.getMoeEhrMedMultDose().getFreqId());
        dose.setSupplFreqId(dto.getMoeEhrMedMultDose().getSupplFreqId());

        dose.setFreqText(dto.getFreqText());
        dose.setSupplFreqText(dto.getSupFreqText());
        //dose.setFreqEng(dto.getFreqText());
        dose.setFreqCode(dto.getFreqCode());
        dose.setFreqValue1(dto.getFreq1());
        dose.setSupplFreqEng(dto.getSupFreqCode());
        dose.setSupplFreqValue1(dto.getSupFreq1());
        dose.setSupplFreqValue2(dto.getSupFreq2());

        dose.setDuration(dto.getDuration());
        dose.setDurationUnit(dto.getDurationUnit());

        dose.setDayOfWeek(dto.getDayOfWeek());
        dose.setPrn(dto.getPrn());
        dose.setSiteEng(dto.getSiteDesc());
        dose.setSupSiteEng(dto.getSupSiteDesc());

        //dose.setStartDate(dto.getStartDate());
        //dose.setEndDate(dto.getEndDate());
        dose.setMoQty(dto.getMoQty());
        dose.setMoQtyUnit(dto.getMoQtyUnit());
        //dose.setUpdateBy(dto.getUpdateBy());

        dose.setCapdSystem(dto.getCapdSystem());
        dose.setCapdCalcium(dto.getCapdCalcium());
        dose.setCapdConc(dto.getCapdConc());
        dose.setCapdBaseunit(dto.getCapdBaseunit());
        //dose.setSiteEng(dto.getSiteDesc());
        dose.setTrueConc(dto.getTrueConc());
        dose.setCapdChargeInd(dto.getCapdChargeInd());
        dose.setCapdChargeableUnit(dto.getCapdChargeableUnit());
        dose.setCapdChargeableAmount(dto.getCapdChargeableAmount());
        dose.setConfirmCapdChargeInd(dto.getConfirmCapdChargeInd());
        dose.setConfirmCapdChargeUnit(dto.getConfirmCapdChargeUnit());

        return dose;
    }

    public static MoeMyFavouriteDetailPo convertMoeMyFavouriteDetail(MoeMedProfileDto dto) {
        MoeMyFavouriteDetailPo detail = new MoeMyFavouriteDetailPo();
        // Simon 20190719 start-- add
        //detail.setMyFavouriteId(dto.getMyFavouriteId());
        //detail.setItemNo(dto.getOrgItemNo());
        // Simon 20190719 end-- add
        detail.setNameType(dto.getNameType());
        detail.setTradeName(dto.getTradename());
        detail.setPrn(dto.getPrn());
        detail.setPreparation(dto.getStrength());
        detail.setDangerDrug(dto.getDangerdrug());
        detail.setDuration(dto.getDuration());
        detail.setDurationUnit(dto.getDurationUnit());

        detail.setVtm(dto.getTrueDisplayname());
        detail.setDayOfWeek(dto.getDayOfWeek());
        detail.setSpecInstruct(dto.getSpecInstruct());
        detail.setCapdSystem(dto.getCapdSystem());
        detail.setCapdCalcium(dto.getCapdCalcium());
        detail.setCapdConc(dto.getCapdConc());
        detail.setCapdTrueConc(dto.getTrueConc());
        detail.setRegimen(dto.getRegimen());
        detail.setCapdBaseunit(dto.getCapdBaseunit());
        detail.setActionStatus(dto.getActionStatus());

        // form
        detail.setFormEng(dto.getFormDesc());
        // route
        detail.setRouteEng(dto.getRouteDesc());


        // site
        detail.setSiteEng(dto.getSiteDesc());
        // prescribe unit
        if (dto.getDosage() != null) {
            detail.setDosage(dto.getDosage());
        }
        detail.setDosageUnit(dto.getModu());
        // basic unit
        detail.setMoQty(dto.getMoQty());
        detail.setMoQtyUnit(dto.getMoQtyUnit());
        // freq
        detail.setFreqCode(dto.getFreqCode());
        detail.setFreqEng(dto.getFreqText());
        detail.setFreqValue1(dto.getFreq1());

        detail.setSupplFreqEng(dto.getSupFreqCode());
        detail.setSupplFreqValue1(dto.getSupFreq1());
        detail.setSupplFreqValue2(dto.getSupFreq2());
        detail.setSupplFreqText(dto.getSupFreqText());
        // strengthCompulsory
        detail.setStrengthCompulsory(dto.getMoeEhrMedProfile().getStrengthCompulsory());
        // ehr med profile
        detail.setFormId(dto.getMoeEhrMedProfile().getFormId());
        detail.setRouteId(dto.getMoeEhrMedProfile().getRouteId());
        detail.setSiteId(dto.getMoeEhrMedProfile().getSiteId());
        detail.setFreqId(dto.getMoeEhrMedProfile().getFreqId());
        detail.setSupplFreqId(dto.getMoeEhrMedProfile().getSupplFreqId());
        detail.setMoQtyUnitId(dto.getMoeEhrMedProfile().getMoQtyUnitId());
        detail.setDosageUnitId(dto.getMoeEhrMedProfile().getModuId());
        detail.setDrugRouteId(dto.getMoeEhrMedProfile().getDrugRouteId());
        detail.setDrugRouteEng(dto.getMoeEhrMedProfile().getDrugRouteEng());

        detail.setOrderLineType(dto.getMoeEhrMedProfile().getOrderLineType());
        detail.setVtm(dto.getMoeEhrMedProfile().getVtm());
        detail.setTradeName(dto.getMoeEhrMedProfile().getTradeName());
        detail.setAliasName(dto.getMoeEhrMedProfile().getAliasName());
        detail.setGenericIndicator(dto.getMoeEhrMedProfile().getGenericIndicator());
        detail.setManufacturer(dto.getMoeEhrMedProfile().getManufacturer());
        detail.setCycleMultiplier(dto.getMoeEhrMedProfile().getCycleMultiplier());
        detail.setScreenDisplay(dto.getMoeEhrMedProfile().getScreenDisplay());
        detail.setDoseFormExtraInfoId(dto.getMoeEhrMedProfile().getDoseFormExtraInfoId());
        detail.setDoseFormExtraInfo(dto.getMoeEhrMedProfile().getDoseFormExtraInfo());
        detail.setStrengthLevelExtraInfo(dto.getMoeEhrMedProfile().getStrengthLevelExtraInfo());

        detail.setCreateUser(dto.getMoeEhrMedProfile().getCreateUser());
        detail.setCreateUserId(dto.getMoeEhrMedProfile().getCreateUserId());
        detail.setCreateHosp(dto.getMoeEhrMedProfile().getCreateHosp());
        detail.setCreateRank(dto.getMoeEhrMedProfile().getCreateRank());
        detail.setCreateRankDesc(dto.getMoeEhrMedProfile().getCreateRankDesc());

        detail.setUpdateUser(dto.getMoeEhrMedProfile().getUpdateUser());
        detail.setUpdateUserId(dto.getMoeEhrMedProfile().getUpdateUserId());
        detail.setUpdateHosp(dto.getMoeEhrMedProfile().getUpdateHosp());
        detail.setUpdateRank(dto.getMoeEhrMedProfile().getUpdateRank());
        detail.setUpdateRankDesc(dto.getMoeEhrMedProfile().getUpdateRankDesc());


        //Simon 20190805 --start add for front-end
        if (null == detail.getRouteId()) {  // Ricci 20191112
            detail.setRouteId(Long.parseLong(StringUtils.isNotBlank(dto.getRouteCode()) ? dto.getRouteCode() : "0"));
        }
        //Simon 20190805 --end add for front-end

        //Simon 20190808 --start add for front-end
        if (null == detail.getSiteId()) {     // Ricci 20191112
            detail.setSiteId(Long.parseLong(StringUtils.isNotBlank(dto.getSiteCode()) ? dto.getSiteCode() : "0"));
        }
        //Simon 20190808 --end add for front-end

        return detail;
    }

    // Ricci 20191111 start --
    public static MoeMyFavouriteDetailPo convertMoeMyFavouriteDetailForCache(MoeMedProfileDto dto) {
        MoeMyFavouriteDetailPo detail = convertMoeMyFavouriteDetail(dto);
        detail.setItemNo(dto.getOrgItemNo());
        // dosage
        if (dto.getMoeMedMultDoses() != null) {
            for (MoeMedMultDoseDto dose : dto.getMoeMedMultDoses()) {
                MoeMyFavouriteMultDosePo cacheDose = convertMoeMyFavouriteMultDose(dose);
                detail.addMoeMyFavouriteMultDose(cacheDose);
                cacheDose.setMoeMyFavouriteDetail(null);
            }
        }

        detail.setMoeMyFavouriteHdr(null);
        return detail;
    }

    public static MoeMyFavouriteDetailPo convertMoeMyFavouriteDetailForPersistence(MoeMedProfileDto dto) {
        MoeMyFavouriteDetailPo detail = convertMoeMyFavouriteDetail(dto);
        // dosage
        if (dto.getMoeMedMultDoses() != null) {
            for (MoeMedMultDoseDto dose : dto.getMoeMedMultDoses()) {
                detail.addMoeMyFavouriteMultDose(convertMoeMyFavouriteMultDose(dose));
            }
        }

        return detail;
    }
    // Ricci 20191111 end --

    public static MoeMyFavouriteHdrPo convertMoeMyFavouriteHdr(MoeMyFavouriteHdrDto dto) {
        MoeMyFavouriteHdrPo hdr = new MoeMyFavouriteHdrPo();
        hdr.setMyFavouriteId(dto.getMyFavouriteId());
        hdr.setFrontMyFavouriteId(dto.getFrontMyFavouriteId());
        hdr.setCreateUserId(dto.getCreateUserId());
        hdr.setMyFavouriteName(dto.getMyFavouriteName());
        hdr.setCreateUser(dto.getCreateUser());
        hdr.setCreateHosp(dto.getCreateHosp());
        hdr.setCreateRank(dto.getCreateRank());
        hdr.setCreateRankDesc(dto.getCreateRankDesc());
        hdr.setCreateDate(dto.getCreateDate());
        hdr.setUpdateUserId(dto.getUpdateUserId());
        hdr.setUpdateUser(dto.getUpdateUser());
        hdr.setUpdateHosp(dto.getUpdateHosp());
        hdr.setUpdateRank(dto.getUpdateRank());
        hdr.setUpdateRankDesc(dto.getUpdateRankDesc());
        hdr.setUpdateDate(dto.getUpdateDate());
        hdr.setVersion(dto.getVersion());
        hdr.setSpecialtyForMyFavourite(dto.getSpecialtyForMyFavourite());

        return hdr;
    }

    // Ricci 20191111 start --
    public static MoeMyFavouriteHdrPo convertMoeMyFavouriteHdrForCache(MoeMyFavouriteHdrDto dto, boolean hdrOnly) {
        MoeMyFavouriteHdrPo hdr = convertMoeMyFavouriteHdr(dto);
        hdr.setCacheId(dto.getCacheId());
        hdr.setIsCache(dto.getIsCache());
        if (!hdrOnly && dto.getMoeMedProfiles() != null) {
            MoeMyFavouriteDetailPo cachePo = null;
            Set<MoeMyFavouriteDetailPo> cachePoList = hdr.getMoeMyFavouriteDetails() != null ? hdr.getMoeMyFavouriteDetails() : new TreeSet<>();
            for (MoeMedProfileDto profile : dto.getMoeMedProfiles()) {
                cachePo = convertMoeMyFavouriteDetailForCache(profile);
                cachePoList.add(cachePo);
            }
            hdr.setMoeMyFavouriteDetails(cachePoList);
        }

        return hdr;
    }

    public static MoeMyFavouriteHdrPo convertMoeMyFavouriteHdrForPersistence(MoeMyFavouriteHdrDto dto, boolean hdrOnly) {
        MoeMyFavouriteHdrPo hdr = convertMoeMyFavouriteHdr(dto);
        if (!hdrOnly && dto.getMoeMedProfiles() != null) {
            for (MoeMedProfileDto profile : dto.getMoeMedProfiles()) {
                hdr.addMoeMyFavouriteDetail(convertMoeMyFavouriteDetailForPersistence(profile));
            }
        }

        return hdr;
    }
    // Ricci 20191111 end --

    public static List<MoeMyFavouriteHdrPo> convertMoeMyFavouriteHdrs(List<MoeMyFavouriteHdrDto> dtos, boolean hdrOnly) {
        List<MoeMyFavouriteHdrPo> hdrs = new ArrayList<MoeMyFavouriteHdrPo>(dtos.size());
        for (MoeMyFavouriteHdrDto dto : dtos) {
            hdrs.add(convertMoeMyFavouriteHdrForPersistence(dto, hdrOnly));
        }
        return hdrs;
    }

    public static MoeRegimenDto convertMoeRegimenDto(MoeRegimenPo regimen) {
        MoeRegimenDto dto = new MoeRegimenDto();
        dto.setRegimen(regimen.getRegimen());
        dto.setRegimenType(regimen.getRegimenType());
        return dto;
    }

    public static MoeRegimenUnitDto convertMoeRegimenUnitDto(MoeRegimenUnitPo regimenUnit) {
        MoeRegimenUnitDto dto = new MoeRegimenUnitDto();
        dto.setDurationUnit(regimenUnit.getDurationUnit());
        dto.setRegimenType(regimenUnit.getRegimenType());
        return dto;
    }

    public static MoeEhrMedAllergenDto convertMoeEhrMedAllergenDto(MoeEhrMedAllergenPo allergen, MoeMedProfilePo profile) {
        MoeEhrMedAllergenDto dto = new MoeEhrMedAllergenDto();
        dto.setAckDate(allergen.getAckDate());
        dto.setAllergen(allergen.getAllergen());
        dto.setMatchType(allergen.getMatchType());
        if (!MoeCommonHelper.isStrengthCompulsoryYes(profile.getMoeEhrMedProfile().getStrengthCompulsory())) {
            String screenMsg = allergen.getScreenMsg();
            //Chris 20190926 Handle NullProintException for screenMsg  -Start
            if (screenMsg != null) {
                screenMsg = screenMsg.replace("&nbsp;" + profile.getStrength(), "");
                if (profile.getMoeEhrMedProfile().getStrengthLevelExtraInfo() != null) {
                    screenMsg = screenMsg.replace("&nbsp;(" + profile.getMoeEhrMedProfile().getStrengthLevelExtraInfo() + ")", "");
                }
            }
            //Chris 20190926 Handle NullProintException for screenMsg  -End
            dto.setScreenMsg(screenMsg);
        } else {
            dto.setScreenMsg(allergen.getScreenMsg());
        }

        dto.setCertainty(allergen.getCertainty());
        dto.setAdditionInfo(allergen.getAdditionInfo());
        dto.setOverrideReason(allergen.getOverrideReason());
        dto.setAckBy(allergen.getAckBy());
        dto.setManifestation(allergen.getManifestation());
        dto.setOverrideStatus(allergen.getOverrideStatus());
        return dto;
    }

    public static List<MoeEhrMedAllergenDto> convertMoeEhrMedAllergenDtos(Set<MoeEhrMedAllergenPo> allergens, MoeMedProfilePo profile) {
        List<MoeEhrMedAllergenDto> dtos = new ArrayList<MoeEhrMedAllergenDto>(0);
        if (allergens != null) {
            for (MoeEhrMedAllergenPo allergen : allergens) {
                dtos.add(convertMoeEhrMedAllergenDto(allergen, profile));
            }
        }
        return dtos;
    }

    public static MoeFormDto convertMoeFormDto(MoeFormPo form) {
        MoeFormDto dto = new MoeFormDto();
        dto.setFormId(form.getFormId());
        dto.setFormEng(form.getFormEng());
        dto.setFormChi(form.getFormChi());
        dto.setFormMultiplier(form.getFormMultiplier().floatValue());
        return dto;
    }

    public static MoeClinicalAlertRuleDto convertMoeClinicalAlertRuleDto(MoeClinicalAlertRulePo rule) {
        MoeClinicalAlertRuleDto dto = new MoeClinicalAlertRuleDto();
        dto.setVtm(rule.getVtm());
        dto.setAlertDesc(rule.getAlertDesc());
        dto.setMatchType(rule.getMatchType());
        return dto;
    }

    public static MoeDrugOrderPropertyDto convertMoeDrugOrderPropertyDto(MoeDrugOrdPropertyLocalPo prop) {
        MoeDrugOrderPropertyDto dto = new MoeDrugOrderPropertyDto();
        dto.setMaximumDuration(prop.getMaximumDuration());
        if (prop.getMinimumDosage() != null) {
            dto.setMinimumDosage(Double.valueOf(prop.getMinimumDosage().floatValue()));
        }
        return dto;
    }

    public static MoeDrugOrdPropertyLocalPo convertMoeDrugOrderProperty(MoeDrugOrderPropertyDto dto) {
        MoeDrugOrdPropertyLocalPo prop = new MoeDrugOrdPropertyLocalPo();
        prop.setMaximumDuration(dto.getMaximumDuration());
        if (dto.getMinimumDosage() != null) {
            prop.setMinimumDosage(BigDecimal.valueOf(dto.getMinimumDosage()));
        }
        return prop;
    }

    public static MoeEhrOrderPo cloneMoeEhrOrder(MoeEhrOrderPo order, UserDto userDto) {
        MoeEhrOrderDto ehrOrderDto = DtoMapUtil.convertMoeEhrOrderDto(order, false, userDto);
        MoeEhrOrderPo tempOrder = DtoMapUtil.convertMoeEhrOrder(ehrOrderDto);
        return tempOrder;
    }

    public static MoeDrugLocalPo createMoeDrugLocalForKey(MoeDrugLocalPo local) {
        MoeDrugLocalPo newDrug = new MoeDrugLocalPo();
        newDrug.setTradeVtmRouteFormId(local.getTradeVtmRouteFormId());
        newDrug.setVtmId(local.getVtmId());
        newDrug.setTradeVtmId(local.getTradeVtmId());
        newDrug.setDoseFormExtraInfoId(local.getDoseFormExtraInfoId());
        newDrug.setRouteId(local.getRouteId());
        newDrug.setFormId(local.getFormId());
        newDrug.setPrescribeUnitId(local.getPrescribeUnitId());
        newDrug.setBaseUnitId(local.getBaseUnitId());
        newDrug.setBaseUnit(local.getBaseUnit());
        return newDrug;
    }

    public static MoeUserSettingPo convertUserSettingDefault(MoeUserSettingDefaultPo userSettingDefault, String paramName, String loginId, String updateBy, Date actionDate) {
        MoeUserSettingPo setting = new MoeUserSettingPo();
        setting.setLoginId(loginId);
        setting.setParamName(paramName);
        setting.setLoginName(loginId);
        setting.setParamNameDesc(userSettingDefault.getParamNameDesc());
        setting.setParamValue(userSettingDefault.getDefaultParamValue());
        setting.setParamValueType(userSettingDefault.getParamValueType());
        setting.setParamPossibleValue(userSettingDefault.getParamPossibleValue());
        setting.setParamLevel(userSettingDefault.getParamLevel());
        setting.setParamDisplay(userSettingDefault.getParamDisplay());
        setting.setCreateBy(updateBy);
        setting.setCreateDtm(actionDate);
        setting.setUpdateBy(updateBy);
        setting.setUpdateDtm(actionDate);
        return setting;
    }

    public static MoeDrugCategoryDto convertMoeDrugCategoryDto(MoeDrugCategoryPo moeDrugCategory) {
        MoeDrugCategoryDto dto = new MoeDrugCategoryDto();

        dto.setDrugCategoryId(moeDrugCategory.getDrugCategoryId());
        dto.setDrugCategoryDesc(moeDrugCategory.getDrugCategoryDesc());
        if (moeDrugCategory.getImage() != null) {
            dto.setImage("data:image/png;base64," + Base64.encodeBase64String(moeDrugCategory.getImage()));
        }
        dto.setRank(moeDrugCategory.getRank());

        return dto;
    }

    public static MoeOverrideReasonDto convertMoeOverrideReasonDto(MoeOverrideReasonPo moeOverrideReason) {
        MoeOverrideReasonDto dto = new MoeOverrideReasonDto();
        dto.setReasonType(moeOverrideReason.getReasonType());
        dto.setReasonCode(moeOverrideReason.getReasonCode());
        dto.setReasonCodeType(moeOverrideReason.getReasonCodeType());
        dto.setReasonDesc(moeOverrideReason.getReasonDesc());
        dto.setRank(moeOverrideReason.getRank());
        return dto;
    }

    // Ricci 20190719 start --
    public static MoeDurationUnitMultiplierDto convertMoeDurationUnitDto(MoeDurationUnitMultiplierPo po) {
        MoeDurationUnitMultiplierDto dto = new MoeDurationUnitMultiplierDto();
        dto.setDurationUnit(po.getDurationUnit());
        dto.setDurationUnitDesc(po.getDurationUnitDesc());
        return dto;
    }
    // Ricci 20190719 end --

    // Ricci 20190814 start --
    public static MoeEhrRxImageLogPo convertMoeEhrRxImageLog(MoeEhrRxImageLogDto dto) {
        MoeEhrRxImageLogPo moeEhrRxImageLogPo = new MoeEhrRxImageLogPo();
        moeEhrRxImageLogPo.setRxXml(dto.getRxXml());
        moeEhrRxImageLogPo.setRxImage(dto.getRxImage());
        moeEhrRxImageLogPo.setLastPrintDtm(dto.getLastPrintDtm());
        moeEhrRxImageLogPo.setCreateDtm(dto.getCreateDtm());
        moeEhrRxImageLogPo.setDirectPrint(dto.getDirectPrint());
        moeEhrRxImageLogPo.setRefNo(dto.getRefNo());
        moeEhrRxImageLogPo.setHospcode(dto.getHospcode());
        moeEhrRxImageLogPo.setOrdNo(dto.getOrdNo());
        moeEhrRxImageLogPo.setReportType(dto.getReportType());
        moeEhrRxImageLogPo.setReportId(dto.getReportId());
        return moeEhrRxImageLogPo;
    }
    // Ricci 20190814

    //Simon 20190814 start -- add preparation list from GWT
    public static MoeMedProfileDto convertMedProfileDTO(MoeDrugDto drugDTO, UserDto userDto) throws Exception {
        MoeMedProfileDto medProfileDTO = new MoeMedProfileDto();
        medProfileDTO.getMoeEhrMedProfile().setOrderLineType("N");
        medProfileDTO.setNameType(drugDTO.getDisplayNameType());
        medProfileDTO.setTradename(drugDTO.getTradeName());
        medProfileDTO.setModu(drugDTO.getPrescribeUnit());
        if (ServerConstant.STRENGTH_COMPULSORY_YES.equalsIgnoreCase(drugDTO.getStrengthCompulsory())) {     //StrengthCompulsory = Y
            medProfileDTO.setItemcode(drugDTO.getLocalDrugId());
            //Chris 20191228  Save Drug Id with VmpId  -Start
            //medProfileDTO.getMoeEhrMedProfile().setDrugId(drugDTO.getStrengths().get(0).getAmpId());
            medProfileDTO.getMoeEhrMedProfile().setDrugId(drugDTO.getStrengths().get(0).getVmpId());
            //Chris 20191228  Save Drug Id with VmpId  -End
        } else {    //StrengthCompulsory = N
            //Chris 20191228  Save Drug Id with VtmRouoteFormId  -Start
            //medProfileDTO.getMoeEhrMedProfile().setDrugId(drugDTO.getTradeVtmRouteFormId());
            medProfileDTO.getMoeEhrMedProfile().setDrugId(drugDTO.getVtmRouteFormId());
            //Chris 20191228  Save Drug Id with VtmRouoteFormId  -End
        }
        if (drugDTO.getPrescribeUnitId() != null) {
            medProfileDTO.getMoeEhrMedProfile().setModuId(drugDTO.getPrescribeUnitId().getBaseUnitId());
        }
        medProfileDTO.setMoQtyUnit(drugDTO.getBaseUnit());
        medProfileDTO.setCurrentMoQtyUnit(drugDTO.getBaseUnit());
        if (drugDTO.getBaseUnitId() != null) {
            medProfileDTO.getMoeEhrMedProfile().setMoQtyUnitId(drugDTO.getBaseUnitId().getBaseUnitId());
            medProfileDTO.getMoeEhrMedProfile().setCurrentMoQtyUnitId(drugDTO.getBaseUnitId().getBaseUnitId());
        }

        if (drugDTO.getDisplayNameType() != null) {
            medProfileDTO.setActionStatus(ServerConstant.ACTION_TYPE_DISPENSE_IN_PHARMACY);
        }

        if (drugDTO.getStrengths() != null && drugDTO.getStrengths().size() > 0 && drugDTO.getStrengthCompulsory().equals(ServerConstant.STRENGTH_COMPULSORY_YES)) {
            if (drugDTO.getStrengths().get(0).getStrength() != null) {
                medProfileDTO.setStrength(drugDTO.getStrengths().get(0).getStrength());
            }
            medProfileDTO.getMoeEhrMedProfile().setStrengthLevelExtraInfo(drugDTO.getStrengths().get(0).getStrengthLevelExtraInfo());
        }

        if (drugDTO.getForm() != null) {
            medProfileDTO.setFormDesc(drugDTO.getForm().getFormEng());
            medProfileDTO.getMoeEhrMedProfile().setFormId(drugDTO.getForm().getFormId());
        }

        // if strength compulsory is on and this is single ingredient, set the strength
/*		if (drugDTO.getStrengthCompulsory() != null && drugDTO.getStrengthCompulsory() == ServerConstant.DB_FLAG_TRUE && drugDTO.getStrengths() !=  null && drugDTO.getStrengths().size() > 0) {
			if (drugDTO.getStrengths().size() > 1 && Formatter.isMultipleIngredient(drugDTO.getVtm())) {
				medProfileDTO.setStrength(drugDTO.getStrengths().get(0).getStrength());
			} else {
				String strength = "";
				for (int i=0; i<drugDTO.getStrengths().size(); i++) {
					strength += drugDTO.getStrengths().get(i).getStrength();
					if (i != drugDTO.getStrengths().size()-1) strength += " + ";
				}
				medProfileDTO.setStrength(strength);
			}
		}*/

        //if (drugDTO.getStrengths() !=  null && drugDTO.getStrengths().size() > 1 && Formatter.isMultipleIngredient(drugDTO.getVtm())) {
        if (Formatter.isMultipleIngredient(drugDTO.getVtm())) {
            String screenDisplay = drugDTO.getStrengths().get(0).getAmp();
            if (drugDTO.getDisplayNameType().equals("N")) {
                screenDisplay = "[" + drugDTO.getTradeNameAlias() + "] " + screenDisplay;
            }
            medProfileDTO.getMoeEhrMedProfile().setScreenDisplay(screenDisplay);
        } else {
            medProfileDTO.getMoeEhrMedProfile().setScreenDisplay(drugDTO.getScreenDisplay());
        }

        medProfileDTO.setStrengths(drugDTO.getStrengths());
        medProfileDTO.getMoeEhrMedProfile().setIngredientList(drugDTO.getIngredientList());

        medProfileDTO.setTrueDisplayname(drugDTO.isFreeText() ? drugDTO.getReplacementString() : drugDTO.getVtm());

        //	medProfileDTO.setStrength(drugDTO.getStrength());
        //	medProfileDTO.setDangerdrug((drugDTO.getLegalClass() != null && drugDTO.getLegalClass().contains(ServerConstant.DANGER_DRUG))?'Y':'N');
        medProfileDTO.setDangerdrug(DtoUtil.DANGEROUS_DRUG_LEGAL_CLASS.contains(drugDTO.getLegalClass()) ? "Y" : "N");

        //TODO: From Chi Wai: Drug Id is drugDTO's termId
        //medProfileDTO.getMoeEhrMedProfile().setDrugId(drugDTO.get)
        medProfileDTO.getMoeEhrMedProfile().setVtm(drugDTO.getVtm());
        medProfileDTO.getMoeEhrMedProfile().setVtmId(drugDTO.getVtmId());
        medProfileDTO.getMoeEhrMedProfile().setTradeName(drugDTO.getTradeName());
        medProfileDTO.getMoeEhrMedProfile().setTradeNameVtmId(drugDTO.getTradeNameVtmId());
        medProfileDTO.getMoeEhrMedProfile().setGenericIndicator(drugDTO.getGenericIndicator());
        medProfileDTO.getMoeEhrMedProfile().setManufacturer(drugDTO.getManufacturer());

        if (drugDTO.getDisplayNameType() != null &&
                ServerConstant.ALIAS_NAME_TYPE_TRADE_NAME_ALIAS.equals(drugDTO.getDisplayNameType())) {
            medProfileDTO.getMoeEhrMedProfile().setAliasNameType("N");
        } else {
            medProfileDTO.getMoeEhrMedProfile().setAliasNameType(drugDTO.getAliasNames().size() == 0 ? null : drugDTO.getAliasNames().get(0).getAliasNameType());
        }

        if (drugDTO.isFreeText()) {
            medProfileDTO.getMoeEhrMedProfile().setAliasName(drugDTO.getReplacementString());
            medProfileDTO.setNameType(ServerConstant.ALIAS_NAME_TYPE_FREE_TEXT);
        } else {
            if (ServerConstant.ALIAS_NAME_TYPE_TRADE_NAME_ALIAS.equals(drugDTO.getDisplayNameType())) {
                medProfileDTO.getMoeEhrMedProfile().setAliasName(drugDTO.getTradeNameAlias());
            } else {
                medProfileDTO.getMoeEhrMedProfile().setAliasName((drugDTO.getAliasNames().size() > 0) ? drugDTO.getAliasNames().get(0).getAliasName() : null);
            }
        }

        List<MoeCommonDosageDto> commonDosageDTOList = drugDTO.getCommonDosages();
        for (int i = 0; i < commonDosageDTOList.size(); i++) {
            MoeCommonDosageDto commonDosageDto = commonDosageDTOList.get(i);

            if (commonDosageDto != null) {
                try {
                    medProfileDTO.setDosage(commonDosageDto.getDosage());
                } catch (Exception e) {
                    //eric 20191219 start--
//                    throw new ParamException("param.Invalid");
                    throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(),ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
                    //eric 20191219 end--

                }
                medProfileDTO.setFreqCode(commonDosageDto.getFreq().getFreqCode());
                if (commonDosageDto.getFreq1() != null) {
                    medProfileDTO.setFreqText(commonDosageDto.getFreq().getFreqEng().replace(ServerConstant.BLANKS_IND, NumberToWordUtil.convert(commonDosageDto.getFreq1().intValue()).trim()));
                } else {
                    medProfileDTO.setFreqText(commonDosageDto.getFreq().getFreqEng());
                }
                medProfileDTO.getMoeEhrMedProfile().setFreqId(commonDosageDto.getFreq().getFreqId());
                medProfileDTO.setPrn(commonDosageDto.getPrn());
                medProfileDTO.setFreq1(commonDosageDto.getFreq1());
                medProfileDTO.setRouteDesc(commonDosageDto.getRouteEng());
                medProfileDTO.getMoeEhrMedProfile().setRouteId(commonDosageDto.getRouteId());
            }

        }
        medProfileDTO.getMoeEhrMedProfile().setStrengthCompulsory(drugDTO.getStrengthCompulsory());
        if (drugDTO.getRoute() != null) {
            medProfileDTO.getMoeEhrMedProfile().setDrugRouteId(drugDTO.getRoute().getRouteId());
            medProfileDTO.getMoeEhrMedProfile().setDrugRouteEng(drugDTO.getRoute().getRouteEng());
            if (medProfileDTO.getRouteDesc() == null) {
                medProfileDTO.setRouteDesc(drugDTO.getRoute().getRouteEng());
                medProfileDTO.getMoeEhrMedProfile().setRouteId(drugDTO.getRoute().getRouteId());
            }
        }

        medProfileDTO.getMoeEhrMedProfile().setDoseFormExtraInfo(drugDTO.getDoseFormExtraInfo());
        medProfileDTO.getMoeEhrMedProfile().setDoseFormExtraInfoId(drugDTO.getDoseFormExtraInfoId());

        medProfileDTO.setFormulStatus(drugDTO.getDrugCategoryId());
        medProfileDTO.setCurrentFormulStatus(drugDTO.getDrugCategoryId());
        if (drugDTO.getStrengthCompulsory().equalsIgnoreCase(ServerConstant.STRENGTH_COMPULSORY_YES)) {
            if (drugDTO.getStrengths() != null && drugDTO.getStrengths().size() > 0) {
                List<PreparationDto> preparationDtos = new ArrayList<>();
                for (int i = 0; i < drugDTO.getStrengths().size(); i++) {
                    PreparationDto dto = convertPreparationDto(drugDTO.getStrengths().get(i));
                    if (i == 0)
                        dto.setSelected(true);
                    else
                        dto.setSelected(false);
                    preparationDtos.add(dto);
                }
                medProfileDTO.setPrep(preparationDtos);
            }
        } else {
            List<PreparationDto> preparationDtos = getStrengths(drugDTO.getVtm(),
                    drugDTO.getFormEng(),
                    drugDTO.getRouteEng(),
                    drugDTO.getTradeName(),
                    userDto);
            //Simon 20190906 comment start--
            // no need to set selected when STRENGTH_COMPULSORY in N
            /*medProfileDTO.setPrep(setSelectedPreparation(preparationDtos,
                    drugDTO.getStrengths().get(0).getStrength(),
                    drugDTO.getStrengths().get(0).getStrengthLevelExtraInfo()));*/
            //Simon 20190906 end--
            medProfileDTO.setPrep(preparationDtos);

        }
        return medProfileDTO;
    }


    public static List<PreparationDto> getStrengths(String vtm, String formEng, String routeEng, String tradeName, UserDto userDto) {
        String key = getStrengthKey(vtm, formEng, routeEng, tradeName);
        List<PreparationDto> preparationDtos = DtoUtil.PREPARATION_MAP.get(key);
        if (userDto == null){
            return new ArrayList<PreparationDto>();
        }

        List<String> strings = DtoUtil.HKREG_BY_SPECIALTY.get(userDto.getHospitalCd() + " " + userDto.getSpec());
        List<PreparationDto> resultDtos = new ArrayList<>();
        if (preparationDtos == null)
            return resultDtos;
        for (PreparationDto dto : preparationDtos) {
            if (strings == null || (strings != null || !strings.contains(dto.getLocalDrugId()))) {
                PreparationDto tempDto = new PreparationDto();
                tempDto.setAmpId(dto.getAmpId());
                tempDto.setLocalDrugId(dto.getLocalDrugId());
                tempDto.setStrength(dto.getStrength());
                tempDto.setStrengthLevelExtraInfo(dto.getStrengthLevelExtraInfo());
                resultDtos.add(tempDto);
            }
        }
        return resultDtos;
    }

    public static String getStrengthKey(String vtm, String formEng, String routeEng, String tradeName) {
        StringBuilder sb = new StringBuilder();
        if (vtm != null && !vtm.equals("")) sb.append(vtm);
        if (formEng != null && !formEng.equals("")) sb.append(" " + formEng);
        if (routeEng != null && !routeEng.equals("")) sb.append(" " + routeEng);
        if (tradeName != null && !tradeName.equals("")) sb.append(" " + tradeName);
        return sb.toString();
    }

    public static List<PreparationDto> setSelectedPreparation(List<PreparationDto> sourceList, String strength, String strengthExtraInfo) {
        for (PreparationDto preparationDto : sourceList) {
            if (strengthExtraInfo != null) {
                if (preparationDto.getStrength().equals(strength) && strengthExtraInfo.equals(preparationDto.getStrengthLevelExtraInfo())) {
                    preparationDto.setSelected(true);
                    break;
                }
            } else {
                if (preparationDto.getStrength().equals(strength)) {
                    preparationDto.setSelected(true);
                    break;
                }
            }
        }
        return sourceList;
    }
    //Simon 20190814 end -- add preparation list

    //Simon 20190903 start--
    public static PreparationDto convertPreparationDto(MoeDrugStrengthDto moeDrugStrengthDto) {
        PreparationDto dto = new PreparationDto();
        dto.setStrength(moeDrugStrengthDto.getStrength());
        dto.setStrengthLevelExtraInfo(moeDrugStrengthDto.getStrengthLevelExtraInfo());
        dto.setAmpId(moeDrugStrengthDto.getAmpId());
        return dto;
    }
    //Simon 20190903 end--


    //Chris 20191009 Get max duration  From MOE1 GWT:EhrMoeModule-MinMaxValidator  -Start
    public static Long getMaxDuration(MoeMedProfileDto medProfile) {
        MoeEhrMedProfileDto ehrMedProfile = medProfile.getMoeEhrMedProfile();

        Boolean isStrengthCompulsory = ServerConstant.DB_FLAG_TRUE.equals(ehrMedProfile.getStrengthCompulsory());
        //Chris 20191101  Get TradeName from MoeEhrMedProfileDto or MoeMedProfileDto  -Start
        String tradeName = ehrMedProfile.getTradeName() != null ? ehrMedProfile.getTradeName() : medProfile.getTradename();
        //String tradeName = ehrMedProfile.getTradeName();
        //Chris 20191101  Get TradeName from MoeEhrMedProfileDto or MoeMedProfileDto  -End
        String vtm = ehrMedProfile.getVtm();
        String route = ehrMedProfile.getDrugRouteEng();
        String form = medProfile.getFormDesc();
        String formExtraInfo = ehrMedProfile.getDoseFormExtraInfo();
        String strength = medProfile.getStrength();

        String key = Formatter.toKeyString(isStrengthCompulsory, tradeName, vtm, route, form, formExtraInfo, strength);

        if (!isStrengthCompulsory) {
            if (DtoUtil.MAX_DURATION_MAP.get(key) != null) {
                return DtoUtil.MAX_DURATION_MAP.get(key);
            }
        } else {
            if (Formatter.isMultipleIngredient(vtm)) {
                if (DtoUtil.MULT_DRUG_ORDER_PROPERTY_MAP.get(key) != null
                        && DtoUtil.MULT_DRUG_ORDER_PROPERTY_MAP.get(key).getMaximumDuration() != null) {
                    return DtoUtil.MULT_DRUG_ORDER_PROPERTY_MAP.get(key).getMaximumDuration();
                }
            } else {
                if (DtoUtil.SINGLE_DRUG_ORDER_PROPERTY_MAP.get(key) != null
                        && DtoUtil.SINGLE_DRUG_ORDER_PROPERTY_MAP.get(key).getMaximumDuration() != null) {
                    return DtoUtil.SINGLE_DRUG_ORDER_PROPERTY_MAP.get(key).getMaximumDuration();
                }
            }
            return null;
        }

        return null;

    }
    //Chris 20191009 Get max duration  From MOE1 GWT:EhrMoeModule-MinMaxValidator  -Start


    //Chris 20191010 Get min dosage  From MOE1 GWT:EhrMoeModule-MinMaxValidator  -Start
    public static List<Double> getMinDosage(MoeMedProfileDto medProfile) {

        MoeEhrMedProfileDto ehrMedProfile = medProfile.getMoeEhrMedProfile();

        Boolean isStrengthCompulsory = ServerConstant.DB_FLAG_TRUE.equals(ehrMedProfile.getStrengthCompulsory());
        //Chris 20191101  Get TradeName from MoeEhrMedProfileDto or MoeMedProfileDto  -Start
        String tradeName = ehrMedProfile.getTradeName() != null ? ehrMedProfile.getTradeName() : medProfile.getTradename();
        //String tradeName = ehrMedProfile.getTradeName();
        //Chris 20191101  Get TradeName from MoeEhrMedProfileDto or MoeMedProfileDto  -End
        String vtm = ehrMedProfile.getVtm();
        String route = ehrMedProfile.getDrugRouteEng();
        String form = medProfile.getFormDesc();
        String formExtraInfo = ehrMedProfile.getDoseFormExtraInfo();
        String strength = medProfile.getStrength();

        String key = Formatter.toKeyString(isStrengthCompulsory, tradeName, vtm, route, form, formExtraInfo, strength);

        if (!isStrengthCompulsory) {
            if (DtoUtil.MIN_DOSAGE_MAP.get(key) != null) {
                return DtoUtil.MIN_DOSAGE_MAP.get(key);
            }
        } else {
            List<Double> minDosage = new ArrayList<Double>();

            if (Formatter.isMultipleIngredient(vtm)) {
                if (DtoUtil.MULT_DRUG_ORDER_PROPERTY_MAP.get(key) != null
                        && DtoUtil.MULT_DRUG_ORDER_PROPERTY_MAP.get(key).getMinimumDosage() != null) {
                    minDosage.add(DtoUtil.MULT_DRUG_ORDER_PROPERTY_MAP.get(key).getMinimumDosage());
                    return minDosage;
                }
            } else {
                if (DtoUtil.SINGLE_DRUG_ORDER_PROPERTY_MAP.get(key) != null
                        && DtoUtil.SINGLE_DRUG_ORDER_PROPERTY_MAP.get(key).getMinimumDosage() != null) {
                    minDosage.add(DtoUtil.SINGLE_DRUG_ORDER_PROPERTY_MAP.get(key).getMinimumDosage());
                    return minDosage;
                }
            }
            return null;
        }
        return null;
    }
    //Chris 20191010 Get min dosage  From MOE1 GWT:EhrMoeModule-MinMaxValidator  -End


    //Chris 20191014 For return frontend minDosageString  From MOE1 GWT:EhrMoeModule-MinMaxValidator  -Start
    public static String getMinDosageString(MoeMedProfileDto moeMedProfile) {

        String modu = moeMedProfile.getModu();
        List<Double> minDosage = getMinDosage(moeMedProfile);

        if (minDosage == null) {
            return null;
        }

        String minDosageString = "";
        for (int i = 0; i < minDosage.size(); i++) {
            minDosageString += BigDecimal.valueOf(minDosage.get(i)).stripTrailingZeros().toPlainString() + " " + modu;
            if (minDosage.size() != 1 && i < minDosage.size() - 2) {
                minDosageString += ", ";
            } else if (i + 1 == minDosage.size() - 1) {
                minDosageString += " and ";
            }
        }
        return minDosageString;
    }
    //Chris 20191014 For return frontend minDosageString  From MOE1 GWT:EhrMoeModule-MinMaxValidator  -End


    //Chris 20200110  -Start
//    public static MoeDrugGenericNameLocalDto convertMoeDrugGenericNameLocalDto(MoeDrugGenericNameLocalPo moeDrugGenericNameLocalPo) {
//        MoeDrugGenericNameLocalDto dto = new MoeDrugGenericNameLocalDto();
//
//        dto.setLocalDrugId(moeDrugGenericNameLocalPo.getLocalDrugId());
//        dto.setTerminologyName(moeDrugGenericNameLocalPo.getTerminologyName());
//        dto.setVtm(moeDrugGenericNameLocalPo.getVtm());
//        dto.setFormId(moeDrugGenericNameLocalPo.getFormId());
//        dto.setFormEng(moeDrugGenericNameLocalPo.getFormEng());
//        dto.setDoseFormExtraInfoId(moeDrugGenericNameLocalPo.getDoseFormExtraInfoId());
//        dto.setDoseFormExtraInfo(moeDrugGenericNameLocalPo.getDoseFormExtraInfo());
//        dto.setRouteId(moeDrugGenericNameLocalPo.getRouteId());
//        dto.setRouteEng(moeDrugGenericNameLocalPo.getRouteEng());
//        dto.setVmpId(moeDrugGenericNameLocalPo.getVmpId());
//        dto.setVtmRouteId(moeDrugGenericNameLocalPo.getVtmRouteId());
//        dto.setVtmRouteFormId(moeDrugGenericNameLocalPo.getVtmRouteFormId());
//        dto.setLegalClassId(moeDrugGenericNameLocalPo.getLegalClassId());
//        dto.setLegalClass(moeDrugGenericNameLocalPo.getLegalClass());
//        dto.setStrengthCompulsory(moeDrugGenericNameLocalPo.getStrengthCompulsory());
//        dto.setVmpId(moeDrugGenericNameLocalPo.getVmpId());
//        dto.setVmp(moeDrugGenericNameLocalPo.getVmp());
//        //Create Info
//        //Update Info
//        dto.setVersion(moeDrugGenericNameLocalPo.getVersion());
//
//        return dto;
//    }

    public static MoeDrugDto convertMoeDrugDto(MoeDrugGenericNameLocalPo moeDrugGenericNameLocalPo) {
        MoeDrugDto dto = new MoeDrugDto();

        dto.setHkRegNo(null);
        dto.setLocalDrugId(moeDrugGenericNameLocalPo.getLocalDrugId());
        dto.setTerminologyName(moeDrugGenericNameLocalPo.getTerminologyName());
        dto.setVtm(moeDrugGenericNameLocalPo.getVtm());
        dto.setVtmId(moeDrugGenericNameLocalPo.getVtmId());


        dto.setLegalClassId(moeDrugGenericNameLocalPo.getLegalClassId());
        dto.setLegalClass(moeDrugGenericNameLocalPo.getLegalClass());

        dto.setStrengthCompulsory(moeDrugGenericNameLocalPo.getStrengthCompulsory());

        dto.setAllergyCheckFlag("N");

        MoeFormDto form = new MoeFormDto();
        form.setFormId(moeDrugGenericNameLocalPo.getFormId());
        form.setFormEng(moeDrugGenericNameLocalPo.getFormEng());
        dto.setForm(form);
        dto.setFormEng(moeDrugGenericNameLocalPo.getFormEng());

        dto.setDoseFormExtraInfoId(moeDrugGenericNameLocalPo.getDoseFormExtraInfoId());
        dto.setDoseFormExtraInfo(moeDrugGenericNameLocalPo.getDoseFormExtraInfo());

        MoeRouteDto route = new MoeRouteDto();
        route.setRouteId(moeDrugGenericNameLocalPo.getRouteId());
        route.setRouteEng(moeDrugGenericNameLocalPo.getRouteEng());
        dto.setRoute(route);
        dto.setRouteEng(moeDrugGenericNameLocalPo.getRouteEng());

        //TODO
        dto.setScreenDisplay(moeDrugGenericNameLocalPo.getVtm());

        MoeDrugStrengthDto strengthDto = new MoeDrugStrengthDto();
        List<MoeDrugStrengthDto> strengths = new ArrayList<MoeDrugStrengthDto>();

        strengthDto.setStrength(moeDrugGenericNameLocalPo.getStrength());
        strengthDto.setStrengthLevelExtraInfo(moeDrugGenericNameLocalPo.getStrengthLevelExtraInfo());
        strengthDto.setVmp(moeDrugGenericNameLocalPo.getVmp());
        strengthDto.setVmpId(moeDrugGenericNameLocalPo.getVmpId());
        strengthDto.setHkRegNo(null);
        strengthDto.setRank(0);
        strengths.add(strengthDto);
        dto.setStrengths(strengths);


        MoeBaseUnitDto prescribeUnitId = new MoeBaseUnitDto();
        prescribeUnitId.setBaseUnit(moeDrugGenericNameLocalPo.getPrescribeUnit());
        prescribeUnitId.setBaseUnitId(moeDrugGenericNameLocalPo.getPrescribeUnitId());
        dto.setPrescribeUnitId(prescribeUnitId);
        dto.setPrescribeUnit(moeDrugGenericNameLocalPo.getPrescribeUnit());

        MoeBaseUnitDto baseUnitId = new MoeBaseUnitDto();
        baseUnitId.setBaseUnit(moeDrugGenericNameLocalPo.getBaseUnit());
        baseUnitId.setBaseUnitId(moeDrugGenericNameLocalPo.getBaseUnitId());
        dto.setBaseUnitId(baseUnitId);
        dto.setBaseUnit(moeDrugGenericNameLocalPo.getBaseUnit());

        MoeBaseUnitDto dispenseUnitId = new MoeBaseUnitDto();
        dispenseUnitId.setBaseUnit(moeDrugGenericNameLocalPo.getDispenseUnit());
        dispenseUnitId.setBaseUnitId(moeDrugGenericNameLocalPo.getDispenseUnitId());
        dto.setDispenseUnitId(dispenseUnitId);
        dto.setDispenseUnit(moeDrugGenericNameLocalPo.getDispenseUnit());


        dto.setVtmRouteFormId(moeDrugGenericNameLocalPo.getVtmRouteFormId());
        //dto.setVtmRouteId(moeDrugGenericNameLocalPo.getVtmRouteId());

        dto.setVersion(moeDrugGenericNameLocalPo.getVersion());
        dto.setFreeText(false);

        //GenericDto
        dto.setDidYouMean(false);
        //TODO
        dto.setDisplayNameType(ServerConstant.ALIAS_NAME_TYPE_VTM);
        dto.setShortName("Y".equalsIgnoreCase(moeDrugGenericNameLocalPo.getStrengthCompulsory()) ?
                moeDrugGenericNameLocalPo.getVmp() : moeDrugGenericNameLocalPo.getVtm() + " " + moeDrugGenericNameLocalPo.getFormEng());
//        dto.setReplacementString();
        dto.setDisplayString("Y".equalsIgnoreCase(moeDrugGenericNameLocalPo.getStrengthCompulsory()) ?
                moeDrugGenericNameLocalPo.getVmp() : moeDrugGenericNameLocalPo.getVtm() + " " + moeDrugGenericNameLocalPo.getFormEng());
        dto.setParent(true);
//        dto.setSwapDisplayFormat();

        return dto;
    }
    //Chris 20200110  -End


}
