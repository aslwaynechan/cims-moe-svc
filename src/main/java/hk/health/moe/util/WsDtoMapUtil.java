package hk.health.moe.util;

import hk.health.moe.common.ServerConstant;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.pojo.dto.InnerMoePatientAllergyDto;
import hk.health.moe.pojo.dto.MoeMedicationDecisionDto;
import hk.health.moe.pojo.dto.MoePatientMedicationDto;
import hk.health.moe.restservice.dto.dac.MedicationDecisionRequestDto;
import hk.health.moe.restservice.dto.dac.PatientAllergyDto;
import hk.health.moe.restservice.dto.dac.PatientMedicationDto;

import java.util.List;

public class WsDtoMapUtil {
    public static MedicationDecisionRequestDto convertMedicationRequest(MoeMedicationDecisionDto dto) {
        MedicationDecisionRequestDto request = new MedicationDecisionRequestDto();
        List<InnerMoePatientAllergyDto> patientAllergies = dto.getPatientAllergies();
        List<MoePatientMedicationDto> patientMedications = dto.getPatientMedications();

        for (InnerMoePatientAllergyDto moePatientAllergyDto : patientAllergies) {
            PatientAllergyDto patientAllergyDTO = new PatientAllergyDto();
            patientAllergyDTO.setAllergen(moePatientAllergyDto.getAllergen());
            patientAllergyDTO.setAllergenTermID(moePatientAllergyDto.getAllergenTermID());
            patientAllergyDTO.setAllergenType(moePatientAllergyDto.getAllergenType());
            patientAllergyDTO.setAllergySeqNo(moePatientAllergyDto.getAllergySeqNo());
            request.getPatientAllergies().add(patientAllergyDTO);

        }
        for (MoePatientMedicationDto moePatientMedicationDto : patientMedications) {
            PatientMedicationDto patientMedicationDto = new PatientMedicationDto();
            patientMedicationDto.setOrderLineRowNum(moePatientMedicationDto.getOrderLineRowNum());
            if (ServerConstant.DB_FLAG_TRUE.equalsIgnoreCase(moePatientMedicationDto.getGenericIndicator())) {
                patientMedicationDto.setTradeNameVtm(moePatientMedicationDto.getTradeName() + " (" + moePatientMedicationDto.getVtm() + ")");
            } else {
                patientMedicationDto.setTradeNameVtm(moePatientMedicationDto.getTradeName());
            }
            patientMedicationDto.setVtm(moePatientMedicationDto.getVtm());
            patientMedicationDto.setFormDesc(moePatientMedicationDto.getFormDesc());
            patientMedicationDto.setDoseFormExtraInfo(moePatientMedicationDto.getDoseFormExtraInfo());
            patientMedicationDto.setRouteDesc(moePatientMedicationDto.getRouteEng());
            patientMedicationDto.setStrength(moePatientMedicationDto.getStrength());
            patientMedicationDto.setStrengthCompulsory(moePatientMedicationDto.getStrengthCompulsory());
            //moePatientMedicationDto.setScreenDisplay(WsFormatter.getDrugName(moePatientMedicationDto.getMedProfile(), false, false));
            request.getPatientMedications().add(patientMedicationDto);
        }
        request.setUserId(dto.getUserId());
        request.setWorkstationIp(dto.getWorkStationIp());
        return request;
    }

/*    public static List<MoeMedicationDecisionResultDto> convertMoeDacList(List<MedicationDecisionResultDto> dacResponseData, MoeMedicationDecisionDto moeDto, String acceptId) {
        if (dacResponseData == null) {
            return null;
        }

        List<MoeMedicationDecisionResultDto> returnList = new ArrayList<>();
        for (MedicationDecisionResultDto dto : dacResponseData) {
            if (dto != null) {
                returnList.add(WsDtoMapUtil.convertMoeMedicationDecisionResultDto(dto, moeDto, acceptId));
            }
        }
        return returnList;
    }

    public static MoeMedicationDecisionResultDto convertMoeMedicationDecisionResultDto(MedicationDecisionResultDto dacResponseData, MoeMedicationDecisionDto moeDto, String acceptId) {
        MoeMedicationDecisionResultDto resultDTO = new MoeMedicationDecisionResultDto();
        resultDTO.setOrderLineRowNum(dacResponseData.getOrderLineRowNum());
        //resultDTO.setReturnMessage(WsFormatter.toWarningDisplayMsg(dacResponseData, moeDto, true));
        //resultDTO.setReturnMessageWithoutStrength(WsFormatter.toWarningDisplayMsg(dacResponseData, moeDto, false));
        List<ReactionAllergenDto> reactionAllergenDtoList = dacResponseData.getReactionAllergens();

        MoeReactionAllergenDto moeReactionAllergenDto = null;
        List<MoeReactionAllergenDto> moeReactionAllergenDtoList = new ArrayList<MoeReactionAllergenDto>();

        for (ReactionAllergenDto reactionAllergenDto : reactionAllergenDtoList) {
            moeReactionAllergenDto = new MoeReactionAllergenDto();
            moeReactionAllergenDto.setAllergenVTM(reactionAllergenDto.getAllergenVTM());
            moeReactionAllergenDto.setAllergenVTMTermID(reactionAllergenDto.getAllergenVTMTermID());
            moeReactionAllergenDto.setReactionLevel(reactionAllergenDto.getReactionLevel());

            moeReactionAllergenDtoList.add(moeReactionAllergenDto);
        }
        resultDTO.setReactionAllergens(moeReactionAllergenDtoList);
        resultDTO.setAcceptId(acceptId);

        return resultDTO;
    }*/

    public static UserDto convertUserDto(hk.health.moe.pojo.dto.webservice.UserDto wsUserDto){
        UserDto userDto = new UserDto();
        userDto.setLoginId(wsUserDto.getLoginId());
        userDto.setLoginName(wsUserDto.getLoginName());
        userDto.setHospitalCd(wsUserDto.getHospCode());
        userDto.setUserRankCd(wsUserDto.getUserRank());
        userDto.setUserRankDesc(wsUserDto.getUserRankDesc());
        return userDto;
    }
}
