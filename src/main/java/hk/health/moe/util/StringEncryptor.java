/*********************************************************************
* Copyright (c) eHR HK Limited 2013. 
* All Rights Reserved.
* 
*********************************************************************
* PROJECT NAME    :  eHR Adaptation
*
* MODULE NAME     :  Medication Order Entry (MOE)
*
* PROGRAM NAME    :  StringEncryptor.java
*
* PURPOSE         :  Defines a set of methods that perform common, often re-used functions
*
**********************************************************************
* VERSION         :   1.01.001
* REF NO          :  
* DATE CREATED    :   7 June 2013
* 
*********************************************************************
*
* CHANGES         :
* REF NO           DATE        DETAIL
* ------           ----        ------
*
*********************************************************************
*/
package hk.health.moe.util;

import hk.health.moe.common.ServerConstant;
import org.apache.commons.codec.binary.Base64;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.PBEConfig;
import org.jasypt.exceptions.EncryptionInitializationException;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class StringEncryptor {

	private StandardPBEStringEncryptor encryptor;
	
	public StringEncryptor(PBEConfig config) {
		encryptor = new StandardPBEStringEncryptor();
		encryptor.setConfig(config);
		encryptor.initialize();
	}
	
	public static String encrypt(String str, PBEConfig config) throws EncryptionOperationNotPossibleException, EncryptionInitializationException {
		
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setConfig(config);
		encryptor.initialize();
		
		return encryptor.encrypt(str);
	
	}
	
	public static String decrypt(String str, PBEConfig config) throws EncryptionOperationNotPossibleException, EncryptionInitializationException {
		
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setConfig(config);
		encryptor.initialize();
		
		return encryptor.decrypt(str);
	
	}
	
	public static String encryptByAES(String str) throws Exception {
         		          
        SecretKeySpec skeySpec = new SecretKeySpec(ServerConstant.AES_KEY, "AES");
        
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);

        byte[] paramBytes = str.getBytes();
        byte[] encrypted = cipher.doFinal(paramBytes);          
        return Base64.encodeBase64URLSafeString(encrypted);
	}
	
	public static String decryptByAES(String input) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(ServerConstant.AES_KEY, "AES");			 
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec);
				
		byte[] decrypted = cipher.doFinal((byte[])new Base64().decode(input));
		return new String(decrypted);
	}
	
	public String encrypt(String str) throws EncryptionOperationNotPossibleException, EncryptionInitializationException {
		return encryptor.encrypt(str);
	}
	
	public String decrypt(String str) throws EncryptionOperationNotPossibleException, EncryptionInitializationException {
		return encryptor.decrypt(str);
	}
	
}
