package hk.health.moe.util;

import hk.health.moe.common.ServerConstant;
import hk.health.moe.pojo.dto.MoeHospitalSettingDto;
import hk.health.moe.pojo.dto.MoeSystemSettingDto;
import hk.health.moe.pojo.po.MoeSystemSettingPo;
import hk.health.moe.repository.SystemSettingRepository;
import org.jasypt.encryption.pbe.config.SimplePBEConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

@Component
public class SystemSettingUtil {

    public static HashMap<String, MoeSystemSettingDto> systemSettingMap = new HashMap<>();
    public static HashMap<String, MoeSystemSettingDto> displaySettingMap = new HashMap<>();
    public static HashMap<String, HashMap<String, MoeHospitalSettingDto>> hospitalSettingMaps = new HashMap<>();
    public static HashMap<String, HashMap<String, MoeHospitalSettingDto>> hospitalDisplaySettingMaps = new HashMap<>();
    @Autowired
    private SimplePBEConfig pbeConfig;


    public static String getParamValue(String paramName) {
        return (systemSettingMap.get(paramName) != null) ? systemSettingMap.get(paramName).getParamValue() : null;
    }

    public static boolean isSettingEnabled(String paramName) {
        return ServerConstant.SETTING_ENABLED.equals(getParamValue(paramName));
    }

    public static String getHospParamValue(String key, String paramName) {
        if (hospitalSettingMaps.get(key) != null && hospitalSettingMaps.get(key).get(paramName) != null) {
            return hospitalSettingMaps.get(key).get(paramName).getParamValue();
        }
        return null;
    }

    public  boolean isApplicationExpired(){
        // check whether application is expired
        try {
            String trialExpiryDate = getParamValue(ServerConstant.TRIAL_EXPIRY_DATE);
            if (trialExpiryDate == null){
//                LOGGER.info("trial_expiry_date is not found.");
                return true;
            }
            trialExpiryDate = StringEncryptor.decrypt(trialExpiryDate, pbeConfig);

            if (!trialExpiryDate.equals(ServerConstant.PRODUCTION)) {
                DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                Date today = (Date)formatter.parse(formatter.format(new Date()));
                Date expiryDate = (Date)formatter.parse(trialExpiryDate);
                if (today.after(expiryDate)) {
//                    LOGGER.info("Application has been expired: " + trialExpiryDate);
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
//            LOGGER.error(e.getMessage(), e);
            return true;
        }
        return false;
    }



}
