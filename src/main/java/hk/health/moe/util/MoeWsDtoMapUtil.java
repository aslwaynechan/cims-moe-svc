package hk.health.moe.util;

import hk.health.moe.common.ServerConstant;
import hk.health.moe.pojo.dto.webservice.MedAllergenDto;
import hk.health.moe.pojo.dto.webservice.MedMultDoseDto;
import hk.health.moe.pojo.dto.webservice.MedProfileDto;
import hk.health.moe.pojo.dto.webservice.OrderListDto;
import hk.health.moe.pojo.dto.webservice.OrderListResponse;
import hk.health.moe.pojo.dto.webservice.PrescriptionDto;
import hk.health.moe.pojo.dto.MoeActionStatusDto;
import hk.health.moe.pojo.po.MoeEhrMedAllergenPo;
import hk.health.moe.pojo.po.MoeEhrMedProfilePo;
import hk.health.moe.pojo.po.MoeEhrOrderPo;
import hk.health.moe.pojo.po.MoeFreqTermIdMapPo;
import hk.health.moe.pojo.po.MoeMedMultDosePo;
import hk.health.moe.pojo.po.MoeMedProfilePo;
import hk.health.moe.pojo.po.MoeOrderPo;
import hk.health.moe.pojo.po.MoePatientPo;
import hk.health.moe.util.comparator.MoePrescriptionComparator;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MoeWsDtoMapUtil {


	public static PrescriptionDto convertMoeEhrOrderDto(MoeEhrOrderPo order) {
		PrescriptionDto dto = new PrescriptionDto();
		MoePatientPo p = order.getMoePatient();
		dto.setOrderNo(order.getOrdNo());
		dto.setPatientKey(order.getMoePatient().getPatientRefKey());
		dto.setEpisodeNo(order.getMoePatientCase().getCaseRefNo());
		dto.setHospCode(order.getHospcode());
		MoeOrderPo moeOrder = order.getMoeOrder();
		if (moeOrder !=null) {

			if (moeOrder.getRefNo() == null || moeOrder.getRefNo().trim().equals("")) {
				String orderNo = String.valueOf(order.getOrdNo());
				if (orderNo.length() > 4 ) {
					dto.setRefNo(orderNo.substring(orderNo.length()-4));
				}
				else {
					dto.setRefNo(StringUtils.leftPad(orderNo, 4, '0'));
				}
			}
			else {
				dto.setRefNo(moeOrder.getRefNo());
			}
			dto.setPrescType(moeOrder.getPrescType().toString());
			dto.setOrdSubType(moeOrder.getOrdSubtype()==null?null:moeOrder.getOrdSubtype().toString());
			dto.setSpecialty(moeOrder.getSpecialty());
			dto.setWard(moeOrder.getIpasWard());
			dto.setHospName(SystemSettingUtil.getHospParamValue(moeOrder.getHospcode()+" "+moeOrder.getSpecialty(),
					ServerConstant.PARAM_NAME_HOSPITAL_NAME_ENG));
			dto.setCreateDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(moeOrder.getOrdDate()));
			dto.setUpdateDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(moeOrder.getLastUpdDate()));
			dto.getMedProfileDto().addAll(convertMoeMedProfileDtos(moeOrder.getMoeMedProfiles()));
		}

		dto.setCreateUserId(order.getCreateUserId());
		dto.setCreateUser(order.getCreateUser());
		dto.setCreateHosp(order.getCreateHosp());
		dto.setCreateRank(order.getCreateRank());
		dto.setCreateRankDesc(order.getCreateRankDesc());
		dto.setUpdateUserId(order.getUpdateUserId());
		dto.setUpdateUser(order.getUpdateUser());
		dto.setUpdateHosp(order.getUpdateHosp());
		dto.setUpdateRank(order.getUpdateRank());
		dto.setUpdateRankDesc(order.getUpdateRankDesc());

		return dto;
	}


	public static List<MedProfileDto> convertMoeMedProfileDtos(Set<MoeMedProfilePo> profiles) {
		List<MedProfileDto> dtos = new ArrayList<MedProfileDto>(0);
		if(profiles != null) {
			Map<Long, Long> prescriptionSortingSeqMap = new HashMap<Long, Long>();
			List<MoeMedProfilePo> profileList = new ArrayList<MoeMedProfilePo>(profiles.size());
			profileList.addAll(profiles);
			Collections.sort(profileList, new MoePrescriptionComparator());

			long seqNo = 1L;
			for(MoeMedProfilePo profile : profileList) {
				prescriptionSortingSeqMap.put(profile.getCmsItemNo(), seqNo++);
			}

			for(MoeMedProfilePo profile : profiles) {
				MedProfileDto dto = convertMoeMedProfileDto(profile);
				dto.setPrescriptionSortingSeq(prescriptionSortingSeqMap.get(dto.getItemNo()));
				dtos.add(dto);
			}
		}
		return dtos;
	}



	public static MedProfileDto convertMoeMedProfileDto(MoeMedProfilePo profile) {
		MedProfileDto dto = new MedProfileDto();

		if(profile.getMoeEhrMedAllergens() != null) {
			dto.getMedAllergenDto().addAll(convertMoeEhrMedAllergenDtos(profile.getMoeEhrMedAllergens(), profile));
		}

		if(profile.getMoeMedMultDoses() != null) {
			dto.getMedMultDoseDto().addAll(convertMoeMedMultDoseDtos(profile.getMoeMedMultDoses(), profile));
		}

		dto.setItemNo(profile.getCmsItemNo());
		String [] doseInstruction = WsFormatter.toMedProfileDisplayString(DtoMapUtil.convertMoeMedProfileDto(profile,null), profile.getMoeOrder().getOrdDate(), true);
		dto.setDoseInstruction(doseInstruction[0]);
		dto.setDoseInstructionWithStyle(doseInstruction[1]);
		dto.setDoseInstructionPrinting(doseInstruction[2]);

		MoeEhrMedProfilePo ehrMedProfile = profile.getMoeEhrMedProfile();

		if(ehrMedProfile != null) {

			if (ehrMedProfile.getDrugId() != null ){
				dto.setDrugTermId(ehrMedProfile.getDrugId());
			}
			dto.setDrugRoute(ehrMedProfile.getDrugRouteEng());
			dto.setVtm(ehrMedProfile.getVtm());
			if (ehrMedProfile.getVtmId()!=null) {
				dto.setVtmId(ehrMedProfile.getVtmId());
			}
			dto.setDangerousDrug(ehrMedProfile.getMoeMedProfile().getDangerdrug());
			dto.setAliasName(ehrMedProfile.getAliasName());
			dto.setAliasNameType(ehrMedProfile.getAliasNameType()==null?null:ehrMedProfile.getAliasNameType().toString());
			dto.setManufacturer(ehrMedProfile.getManufacturer());
			if (ehrMedProfile.getGenericIndicator()!=null) {
				dto.setGenericIndicator(ehrMedProfile.getGenericIndicator().toString());
			}
			dto.setDrugName(ehrMedProfile.getScreenDisplay());
			if (ehrMedProfile.getOrderLineType()!=null){
				dto.setOrderLineType(ehrMedProfile.getOrderLineType().toString());
			}
			if (ehrMedProfile.getCycleMultiplier() != null){
				dto.setCycleMultiplier(ehrMedProfile.getCycleMultiplier());
			}
			dto.setFormExtraInfo(ehrMedProfile.getDoseFormExtraInfo());
			dto.setStrengthExtraInfo(ehrMedProfile.getStrengthLevelExtraInfo());

			dto.setCreateDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(ehrMedProfile.getCreateDtm()));
			dto.setCreateBy(ehrMedProfile.getCreateUserId());
			dto.setCreateUser(ehrMedProfile.getCreateUser());
			dto.setCreateHosp(ehrMedProfile.getCreateHosp());
			dto.setCreateRank(ehrMedProfile.getCreateRank());
			dto.setCreateRankDesc(ehrMedProfile.getCreateRankDesc());

			dto.setUpdateDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(ehrMedProfile.getUpdateDtm()));
			dto.setUpdateBy(ehrMedProfile.getUpdateUserId());
			dto.setUpdateUser(ehrMedProfile.getUpdateUser());
			dto.setUpdateHosp(ehrMedProfile.getUpdateHosp());
			dto.setUpdateRank(ehrMedProfile.getUpdateRank());
			dto.setUpdateRankDesc(ehrMedProfile.getUpdateRankDesc());

			if (ehrMedProfile.getStrengthCompulsory()!=null) {
				dto.setStrengthCompulsory(ehrMedProfile.getStrengthCompulsory().toString());
			}

			if (ehrMedProfile.getMedDiscontFlag() != null) {
//				dto.setMedDiscontDtm(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(ehrMedProfile.getMedDiscontDtm()));
//				dto.setMedDiscontFlag(ehrMedProfile.getMedDiscontFlag().toString());
//				dto.setMedDiscontReasonCode(ehrMedProfile.getMedDiscontReasonCode());
//				dto.setMedDiscontInfo(ServerConstant.MED_DISC_INFO_PREFIX + ehrMedProfile.getMedDiscontInformation());
//				dto.setMedDiscontUserId(ehrMedProfile.getMedDiscontUserId());
//				dto.setMedDiscontUser(ehrMedProfile.getMedDiscontUser());
//				dto.setMedDiscontHosp(ehrMedProfile.getMedDiscontHosp());
			}
		}
		dto.setTradeName(profile.getTradename());
		dto.setDrugNameType(String.valueOf(profile.getNameType()));

		//dto.setDosage(profile.getDosage());
		dto.setDosage(new BigDecimal(WsFormatter.formatDosage(profile.getDosage())));
		dto.setDosageUnit(profile.getModu());
		dto.setFreqCode(profile.getFreqCode());
		dto.setFreqText(profile.getFreqText());
		if (profile.getFreq1()!=null) {
			dto.setFreqValue(profile.getFreq1());
		}
		MoeFreqTermIdMapPo freqMap = null;
		if (dto.getFreqCode() != null) {
			String freq1 = (profile.getFreq1() != null && profile.getFreq1() > 0) ? profile.getFreq1().toString() : "";
			freqMap = DtoUtil.FREQ_TERM_ID_MAP.get(dto.getFreqCode() + " " + freq1);
		}
		if (freqMap != null && "Y".equalsIgnoreCase(freqMap.getMapToSupplFreq())) {
			dto.setSupplFreqCode(freqMap.getSupplFreqDescEng());
			dto.setSupplFreqText(freqMap.getSupplFreqDescEng());
			dto.setSupplFreqDesc(freqMap.getEhrSupplFreqDesc());
			dto.setSupplFreqId(freqMap.getEhrSupplFreqTermId());
		} else {
			dto.setSupplFreqCode(profile.getSupFreqCode());
			if (profile.getSupFreq1() != null) {
				dto.setSupplFreqValue1(profile.getSupFreq1());
			}
			if (profile.getSupFreq2() != null) {
				dto.setSupplFreqValue2(profile.getSupFreq2());
			}
			if (profile.getSupFreqText() != null) {
				dto.setSupplFreqText(profile.getSupFreqText());
			}
		}
		if (profile.getDayOfWeek()!=null){
			dto.setDayOfWeek(profile.getDayOfWeek());
		}
		if (profile.getPrn() != null){
			dto.setPrn(String.valueOf(profile.getPrn()));
		}
		dto.setForm(profile.getFormDesc());
		dto.setRoute(profile.getRouteDesc());
		if (profile.getDuration()!=null){
			dto.setDuration(profile.getDuration());
		}
		dto.setDurationUnit(profile.getDurationUnit()==null?null:profile.getDurationUnit().toString());
		if (profile.getMoQty()!=null) {
			dto.setTotalQuantity(profile.getMoQty());
		}
		dto.setTotalQuantityUnit(profile.getMoQtyUnit());
		dto.setStrength(profile.getStrength());
		dto.setActionStatus(String.valueOf(profile.getActionStatus()));

		for (MoeActionStatusDto moeActionStatusDto : DtoUtil.ACTIONS_STATUS_LIST) {
			if (moeActionStatusDto.getActionStatusType().equals(profile.getActionStatus())){
				dto.setActionStatusDesc(moeActionStatusDto.getActionStatus());
				break;
			}
		}

		dto.setSite(profile.getSiteDesc());
		dto.setStartDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(profile.getStartDate()));
		dto.setEndDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(profile.getEndDate()));
		dto.setSpecialInstruction(profile.getSpecInstruct());
		dto.setLocalDrugKey(profile.getItemcode());

		return dto;
	}

	public static List<MedMultDoseDto> convertMoeMedMultDoseDtos(Set<MoeMedMultDosePo> doses, MoeMedProfilePo profile) {
		List<MedMultDoseDto> dtos = new ArrayList<MedMultDoseDto>(0);
		if(doses != null) {
			for(MoeMedMultDosePo dose : doses) {
				dtos.add(convertMoeMedMultDoseDto(dose, profile));
			}
		}
		return dtos;
	}


	public static MedMultDoseDto convertMoeMedMultDoseDto(MoeMedMultDosePo dose, MoeMedProfilePo profile) {
		MedMultDoseDto dto = new MedMultDoseDto();
		dto.setStepNo(dose.getStepNo());
		dto.setMultDoseNo(dose.getMultDoseNo());
		if (dose.getPrn() != null){
			dto.setPrn(String.valueOf(dose.getPrn()));
		}
		//dto.setDosage(dose.getDosage());
		dto.setDosage(new BigDecimal(WsFormatter.formatDosage(dose.getDosage())));
		dto.setDosageUnit(profile.getModu());
		dto.setFreqCode(dose.getFreqCode());
		dto.setFreqText(dose.getFreqText());
		dto.setFreqValue(dose.getFreq1());
		MoeFreqTermIdMapPo freqMap = null;
		if (dto.getFreqCode() != null) {
			String freq1 = (dose.getFreq1() != null && dose.getFreq1() > 0) ? dose.getFreq1().toString() : "";
			freqMap = DtoUtil.FREQ_TERM_ID_MAP.get(dto.getFreqCode() + " " + freq1);
		}
		if (freqMap != null && "Y".equalsIgnoreCase(freqMap.getMapToSupplFreq())) {
			dto.setSupplFreqCode(freqMap.getSupplFreqDescEng());
			dto.setSupplFreqText(freqMap.getSupplFreqDescEng());
			dto.setSupplFreqDesc(freqMap.getEhrSupplFreqDesc());
			dto.setSupplFreqId(freqMap.getEhrSupplFreqTermId());
		} else {
			dto.setSupplFreqCode(dose.getSupFreqCode());
			dto.setSupplFreqText(dose.getSupFreqText());
			dto.setSupplFreqValue1(dose.getSupFreq1());
			dto.setSupplFreqValue2(dose.getSupFreq2());
		}
		dto.setDuration(dose.getDuration());
		dto.setDurationUnit(String.valueOf(dose.getDurationUnit()));
		dto.setStartDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(dose.getStartDate()));
		dto.setEndDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(dose.getEndDate()));
		dto.setTotalQuantity(dose.getMoQty());
		dto.setTotalQuantityUnit(dose.getMoQtyUnit());
		return dto;
	}

	public static List<MedAllergenDto> convertMoeEhrMedAllergenDtos(Set<MoeEhrMedAllergenPo> allergens, MoeMedProfilePo profile) {
		List<MedAllergenDto> dtos = new ArrayList<MedAllergenDto>(0);
		if(allergens != null) {
			for(MoeEhrMedAllergenPo allergen : allergens) {
				dtos.add(convertMoeEhrMedAllergenDto(allergen, profile));
			}
		}
		return dtos;
	}

	public static MedAllergenDto convertMoeEhrMedAllergenDto(MoeEhrMedAllergenPo allergen, MoeMedProfilePo profile) {
		MedAllergenDto dto = new MedAllergenDto();
		dto.setRowNo(allergen.getRowNo());
		dto.setAllergen(allergen.getAllergen());
		dto.setMatchType(String.valueOf(allergen.getMatchType()));
		dto.setScreenMsg(allergen.getScreenMsg());
		dto.setCertainty(allergen.getCertainty()==null?null:allergen.getCertainty().toString());
		dto.setManifestation(allergen.getManifestation());
		dto.setAdditionInfo(allergen.getAdditionInfo());
		dto.setOverrideReason(allergen.getOverrideReason());
		return dto;
	}

	public static OrderListResponse convertMoeEhrOrderDtos(List<MoeEhrOrderPo> orderList) {
		OrderListResponse response = new OrderListResponse();
		for (MoeEhrOrderPo order : orderList){
			response.getOrderListDto().add(convertEhrOrderDto(order));
		}
		return response;
	}

	public static OrderListDto convertEhrOrderDto(MoeEhrOrderPo order) {
		OrderListDto dto = new OrderListDto();

		dto.setOrderNo(order.getOrdNo());
		dto.setPatientKey(order.getMoePatient().getPatientRefKey());
		dto.setEpisodeNo(order.getMoePatientCase().getCaseRefNo());
		dto.setHospCode(order.getHospcode());
		MoeOrderPo moeOrder = order.getMoeOrder();
		if (moeOrder !=null) {
			dto.setPrescType(moeOrder.getPrescType().toString());
			if (moeOrder.getOrdSubtype() != null){
				dto.setOrdSubType(moeOrder.getOrdSubtype().toString());
			}
			dto.setSpecialty(moeOrder.getSpecialty());
			dto.setWard(moeOrder.getIpasWard());
			dto.setHospName(SystemSettingUtil.getHospParamValue(moeOrder.getHospcode()+" "+moeOrder.getSpecialty(),
					ServerConstant.PARAM_NAME_HOSPITAL_NAME_ENG));
			dto.setCreateDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(moeOrder.getOrdDate()));
			dto.setUpdateDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(moeOrder.getLastUpdDate()));
		}

		dto.setCreateUserId(order.getCreateUserId());
		dto.setCreateUser(order.getCreateUser());
		dto.setCreateHosp(order.getCreateHosp());
		dto.setCreateRank(order.getCreateRank());
		dto.setCreateRankDesc(order.getCreateRankDesc());
		dto.setUpdateUserId(order.getUpdateUserId());
		dto.setUpdateUser(order.getUpdateUser());
		dto.setUpdateHosp(order.getUpdateHosp());
		dto.setUpdateRank(order.getUpdateRank());
		dto.setUpdateRankDesc(order.getUpdateRankDesc());

		return dto;
	}
}