/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  DtoUtil.java
 *
 * PURPOSE         :  Defines a set of methods that perform common, often re-used functions
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.util;


import hk.health.moe.common.MoeCommonHelper;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.pojo.dto.MoeActionStatusDto;
import hk.health.moe.pojo.dto.MoeBaseUnitDto;
import hk.health.moe.pojo.dto.MoeClinicalAlertRuleDto;
import hk.health.moe.pojo.dto.MoeCommonDosageTypeDto;
import hk.health.moe.pojo.dto.MoeDrugCategoryDto;
import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.dto.MoeDrugGenericNameLocalDto;
import hk.health.moe.pojo.dto.MoeDrugOrderPropertyDto;
import hk.health.moe.pojo.dto.MoeDurationUnitMultiplierDto;
import hk.health.moe.pojo.dto.MoeFormDto;
import hk.health.moe.pojo.dto.MoeFreqDto;
import hk.health.moe.pojo.dto.MoeMedMultDoseDto;
import hk.health.moe.pojo.dto.MoeMedProfileDto;
import hk.health.moe.pojo.dto.MoeOverrideReasonDto;
import hk.health.moe.pojo.dto.MoeRegimenDto;
import hk.health.moe.pojo.dto.MoeRegimenUnitDto;
import hk.health.moe.pojo.dto.MoeRouteDto;
import hk.health.moe.pojo.dto.MoeSiteDto;
import hk.health.moe.pojo.dto.MoeSupplFreqDescDto;
import hk.health.moe.pojo.dto.MoeSupplFreqDto;
import hk.health.moe.pojo.dto.PreparationDto;
import hk.health.moe.pojo.po.MoeBaseUnitTermIdMapPo;
import hk.health.moe.pojo.po.MoeCommonDosageTypePo;
import hk.health.moe.pojo.po.MoeConjunctionTermIdMapPo;
import hk.health.moe.pojo.po.MoeDoseFormExtraInfoPo;
import hk.health.moe.pojo.po.MoeDrugGenericNameLocalPo;
import hk.health.moe.pojo.po.MoeDrugLocalPo;
import hk.health.moe.pojo.po.MoeDurationTermIdMapPo;
import hk.health.moe.pojo.po.MoeFreqPo;
import hk.health.moe.pojo.po.MoeFreqTermIdMapPo;
import hk.health.moe.pojo.po.MoePrescribeUnitTermIdMapPo;
import hk.health.moe.pojo.po.MoePrnTermIdMapPo;
import hk.health.moe.pojo.po.MoeRegimenPo;
import hk.health.moe.pojo.po.MoeRoutePo;
import hk.health.moe.pojo.po.MoeRouteTermIdMapPo;
import hk.health.moe.pojo.po.MoeSiteTermIdMapPo;
import hk.health.moe.pojo.po.MoeSupplFreqTermIdMapPo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class DtoUtil {

    public static final Map<String, String> DRUGS_BY_HKREGNO = new HashMap<String, String>();
    public static final Map<String, MoeDrugDto> DRUGS_BY_DRUGNAME = new HashMap<String, MoeDrugDto>();
    public static final Map<String, List<MoeDrugDto>> DRUGS_BY_TYPE = new HashMap<String, List<MoeDrugDto>>();
    public static final Map<String, List<MoeDrugDto>> DOSAGE_DRUGS_BY_TYPE = new HashMap<String, List<MoeDrugDto>>();
    public static final Map<String, List<MoeDrugDto>> DRUGS_BY_VTM = new HashMap<String, List<MoeDrugDto>>();
    public static final Map<String, List<MoeDrugDto>> DRUGS_BY_TRADE_NAME = new HashMap<String, List<MoeDrugDto>>();
    public static final Map<String, List<MoeDrugDto>> DRUGS_BY_BAN = new HashMap<String, List<MoeDrugDto>>();
    public static final Map<String, List<MoeDrugDto>> DRUGS_BY_ABB = new HashMap<String, List<MoeDrugDto>>();
    public static final Map<String, List<MoeDrugDto>> DRUGS_BY_OTHERS = new HashMap<String, List<MoeDrugDto>>();
    public static final Map<String, List<String>> HKREG_BY_SPECIALTY = new HashMap<String, List<String>>();
    public static final List<MoeFreqDto> FREQS_LOCAL = new ArrayList<MoeFreqDto>();
    public static final List<MoeFreqDto> FREQS = new ArrayList<MoeFreqDto>();
    public static final Map<String, Integer> FREQ_ORDER = new HashMap<String, Integer>();
    public static final List<MoeActionStatusDto> ACTIONS_STATUS_LIST = new ArrayList<MoeActionStatusDto>();
    public static final HashMap<String, MoeActionStatusDto> ACTIONS_STATUS_MAP = new HashMap<String, MoeActionStatusDto>();
    public static final HashMap<String, List<MoeSupplFreqDto>> REGIMEN_TYPE_MAP = new HashMap<String, List<MoeSupplFreqDto>>();
    public static final HashMap<String, List<MoeRegimenUnitDto>> REGIMENT_UNIT_MAP = new HashMap<String, List<MoeRegimenUnitDto>>();
    public static final HashMap<String, List<MoeSiteDto>> ROUTE_MAP = new HashMap<String, List<MoeSiteDto>>();
    public static final HashMap<String, String> ROUTE_SITE_MAP = new HashMap<String, String>();
    public static final HashMap<String, String> FORM_ROUTE_MAP = new HashMap<String, String>();
    public static final List<String> DUP_BASIC_NAMES = new ArrayList<String>();
    public static final List<MoeRouteDto> ROUTE_LIST = new ArrayList<MoeRouteDto>();
    public static final List<MoeSiteDto> SITE_LIST = new ArrayList<MoeSiteDto>();
    public static final List<MoeBaseUnitDto> BASE_UNIT_LIST = new ArrayList<MoeBaseUnitDto>();
    public static final List<MoeBaseUnitDto> PRESCRIBE_UNIT_LIST = new ArrayList<MoeBaseUnitDto>();
    public static final List<MoeBaseUnitDto> DISPENSE_UNIT_LIST = new ArrayList<MoeBaseUnitDto>();
    public static final List<MoeRegimenDto> REGIMENT_LIST = new ArrayList<MoeRegimenDto>();
    public static final List<MoeFormDto> FORM_LIST = new ArrayList<MoeFormDto>();
    public static final List<MoeDoseFormExtraInfoPo> DOSE_FORM_EXTRA_INFO_LIST = new ArrayList<MoeDoseFormExtraInfoPo>();
    public static final HashMap<String, Long> DURATION_UNIT_MAP = new HashMap<String, Long>();
    public static final HashMap<String, String> DURATION_UNIT_DESC_MAP = new HashMap<String, String>();
    public static final HashMap<String, MoeClinicalAlertRuleDto> CLINICAL_ALERT_MAP = new HashMap<String, MoeClinicalAlertRuleDto>();
    public static final HashMap<String, List<Double>> MIN_DOSAGE_MAP = new HashMap<String, List<Double>>();
    public static final HashMap<String, Long> MAX_DURATION_MAP = new HashMap<String, Long>();
    public static final List<String> SPECIFY_QUANTITY_FLAG = new ArrayList<String>();
    public static final List<String> STRENGTH_COMP_SPECIFY_QUANTITY_FLAG = new ArrayList<String>();
    public static final HashMap<String, MoeDrugOrderPropertyDto> SINGLE_DRUG_ORDER_PROPERTY_MAP = new HashMap<String, MoeDrugOrderPropertyDto>();
    public static final HashMap<String, MoeDrugOrderPropertyDto> MULT_DRUG_ORDER_PROPERTY_MAP = new HashMap<String, MoeDrugOrderPropertyDto>();
    public static final HashMap<String, Long> LEGAL_CLASS_MAP = new HashMap<String, Long>();
    public static final HashMap<String, List<PreparationDto>> PREPARATION_MAP = new HashMap<String, List<PreparationDto>>();
    public static final HashMap<String, MoeDrugLocalPo> EXIST_DRUGS_MAP = new HashMap<String, MoeDrugLocalPo>();
    public static final HashMap<String, MoeDrugLocalPo> EXIST_DRUGS_MAP_WITH_STRENGTH = new HashMap<String, MoeDrugLocalPo>();
    public static final HashMap<String, MoeDrugLocalPo> EXIST_DRUGS_WITH_TRADE_ALIAS_MAP = new HashMap<String, MoeDrugLocalPo>();
    public static final HashSet<String> EXIST_DANGER_DRUGS = new HashSet<String>();
    public static final List<String> SUSPEND_DRUGS_MAP = new ArrayList<String>();
    public static final List<String> EXIST_DRUGS_WITH_ALIAS_LIST = new ArrayList<String>();
    public static final HashMap<String, List<String>> EXIST_SPECIALTY_DRUGS = new HashMap<String, List<String>>();
    public static final HashMap<String, List<String>> EXIST_NS_SPECIALTY_DRUGS = new HashMap<String, List<String>>();
    public static final ArrayList<String> DANGEROUS_DRUG_LEGAL_CLASS = new ArrayList<String>();
    public static final HashMap<String, MoeDrugCategoryDto> DRUG_CATEGORY_MAP = new HashMap<String, MoeDrugCategoryDto>();
    public static long baseUnitDoseId;
    public static final HashMap<String, MoeBaseUnitTermIdMapPo> BASE_UNIT_TERM_ID_MAP = new HashMap<String, MoeBaseUnitTermIdMapPo>();
    public static final HashMap<String, MoePrescribeUnitTermIdMapPo> PRESCRIBE_UNIT_TERM_ID_MAP = new HashMap<String, MoePrescribeUnitTermIdMapPo>();
    public static final HashMap<String, MoeConjunctionTermIdMapPo> CONJUNCTION_TERM_ID_MAP = new HashMap<String, MoeConjunctionTermIdMapPo>();
    public static final HashMap<String, MoeDurationTermIdMapPo> DURATION_TERM_ID_MAP = new HashMap<String, MoeDurationTermIdMapPo>();
    public static final HashMap<String, MoeFreqTermIdMapPo> FREQ_TERM_ID_MAP = new HashMap<String, MoeFreqTermIdMapPo>();
    public static final HashMap<String, MoePrnTermIdMapPo> PRN_TERM_ID_MAP = new HashMap<String, MoePrnTermIdMapPo>();
    public static final HashMap<String, MoeRouteTermIdMapPo> ROUTE_TERM_ID_MAP = new HashMap<String, MoeRouteTermIdMapPo>();
    public static final HashMap<String, MoeSiteTermIdMapPo> SITE_TERM_ID_MAP = new HashMap<String, MoeSiteTermIdMapPo>();
    public static final HashMap<String, MoeSupplFreqTermIdMapPo> SUPPL_FREQ_TERM_ID_MAP = new HashMap<String, MoeSupplFreqTermIdMapPo>();
    public static final List<MoeOverrideReasonDto> OVERRIDE_REASON_LIST = new ArrayList<MoeOverrideReasonDto>();
    // Ricci 20190719 start --
    public static final List<MoeDurationUnitMultiplierDto> DURATION_UNIT_LIST = new ArrayList();
    // Ricci 20190719 end --
    // Ricci 20190822 moe_drug_server start --
    public static final List<MoeCommonDosageTypeDto> COMMON_DOSAGE_TYPE_LIST = new ArrayList();
    // Ricci 20190822 moe_drug_server end --
    public static final HashMap<String, MoeRegimenPo> REGIMEN_ID_MAP = new HashMap();
    public static final HashMap<Long, MoeFreqPo> FREQ_ID_MAP = new HashMap();
    public static final HashMap<String, MoeCommonDosageTypePo> COMMON_DOSAGE_TYPE_ID_MAP = new HashMap();
    public static final HashMap<Long, MoeRoutePo> ROUTE_ID_MAP = new HashMap();
    //Chris 20200110  -Start
//    public static final Map<String, List<MoeDrugDto>> GENERIC_NAME_DRUGS = new HashMap<String, List<MoeDrugDto>>();
//    public static final List<MoeDrugGenericNameLocalDto> GENERIC_NAME_DRUGS = new ArrayList<>();
    public static final List<MoeDrugDto> GENERIC_NAME_DRUGS = new ArrayList<>();
    //Chris 20200110  -End


    public static void clearAll() {
        DRUGS_BY_HKREGNO.clear();
        DRUGS_BY_DRUGNAME.clear();
        DRUGS_BY_TYPE.clear();
        DOSAGE_DRUGS_BY_TYPE.clear();
        DRUGS_BY_VTM.clear();
        DRUGS_BY_TRADE_NAME.clear();
        DRUGS_BY_BAN.clear();
        DRUGS_BY_ABB.clear();
        DRUGS_BY_OTHERS.clear();
        HKREG_BY_SPECIALTY.clear();
        FREQS_LOCAL.clear();
        FREQS.clear();
        FREQ_ORDER.clear();
        REGIMEN_TYPE_MAP.clear();
        ROUTE_MAP.clear();
        ROUTE_SITE_MAP.clear();
        FORM_ROUTE_MAP.clear();
        ROUTE_LIST.clear();
        SITE_LIST.clear();
        BASE_UNIT_LIST.clear();
        PRESCRIBE_UNIT_LIST.clear();
        FORM_LIST.clear();
        DOSE_FORM_EXTRA_INFO_LIST.clear();
        MIN_DOSAGE_MAP.clear();
        MAX_DURATION_MAP.clear();
        SINGLE_DRUG_ORDER_PROPERTY_MAP.clear();
        MULT_DRUG_ORDER_PROPERTY_MAP.clear();
        PREPARATION_MAP.clear();
        EXIST_DRUGS_MAP.clear();
        EXIST_DRUGS_WITH_ALIAS_LIST.clear();
        EXIST_DRUGS_MAP_WITH_STRENGTH.clear();
        EXIST_DANGER_DRUGS.clear();
        EXIST_DRUGS_WITH_TRADE_ALIAS_MAP.clear();
        SUSPEND_DRUGS_MAP.clear();
        EXIST_SPECIALTY_DRUGS.clear();
        EXIST_NS_SPECIALTY_DRUGS.clear();
        STRENGTH_COMP_SPECIFY_QUANTITY_FLAG.clear();
        SPECIFY_QUANTITY_FLAG.clear();
        DRUG_CATEGORY_MAP.clear();
        BASE_UNIT_TERM_ID_MAP.clear();
        PRESCRIBE_UNIT_TERM_ID_MAP.clear();
        CONJUNCTION_TERM_ID_MAP.clear();
        DURATION_TERM_ID_MAP.clear();
        FREQ_TERM_ID_MAP.clear();
        PRN_TERM_ID_MAP.clear();
        ROUTE_TERM_ID_MAP.clear();
        SITE_TERM_ID_MAP.clear();
        SUPPL_FREQ_TERM_ID_MAP.clear();
        OVERRIDE_REASON_LIST.clear();

        // Ricci 20190722 start --
        ACTIONS_STATUS_LIST.clear();
        ACTIONS_STATUS_MAP.clear();
        REGIMENT_UNIT_MAP.clear();
        DUP_BASIC_NAMES.clear();
        DISPENSE_UNIT_LIST.clear();
        REGIMENT_LIST.clear();
        DURATION_UNIT_MAP.clear();
        DURATION_UNIT_DESC_MAP.clear();
        CLINICAL_ALERT_MAP.clear();
        LEGAL_CLASS_MAP.clear();
        DANGEROUS_DRUG_LEGAL_CLASS.clear();
        DURATION_UNIT_LIST.clear(); // needed
        // Ricci 20190722 end --

        // Ricci 20190822 moe_drug_server start --
        COMMON_DOSAGE_TYPE_LIST.clear();
        // Ricci 20190822 moe_drug_server end
        REGIMEN_ID_MAP.clear();
        FREQ_ID_MAP.clear();
        COMMON_DOSAGE_TYPE_ID_MAP.clear();
        ROUTE_ID_MAP.clear();

        //Chris 20200110  -Start
        GENERIC_NAME_DRUGS.clear();
        //Chris 20200110  -End
    }

    /**
     * This method makes a "deep clone" of any Java object it is given.
     */
    public static Object deepClone(Object object) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            return ois.readObject();
        } catch (Exception e) {
            return null;
        }
    }


    public static void addDtoToMap(Map<String, List<MoeDrugDto>> map, String key, MoeDrugDto dto) {
        List<MoeDrugDto> value = map.get(key);
        if (value == null) value = new ArrayList<MoeDrugDto>();
        value.add(dto);
        map.put(key, value);
    }

    public static Map<MoeDrugDto, List<MoeDrugDto>> convertDTOListToMap(List<MoeDrugDto> dtos) {
        Map<MoeDrugDto, List<MoeDrugDto>> map = new HashMap<MoeDrugDto, List<MoeDrugDto>>();
        Map<String, MoeDrugDto> parentMap = new HashMap<String, MoeDrugDto>();
        for (MoeDrugDto dto : dtos) {
            if (dto.isParent()) {
                parentMap.put(dto.getShortName(), dto);
            } else {
                MoeDrugDto key = parentMap.get(dto.getShortName());
                if (key != null) {
                    List<MoeDrugDto> value = map.get(key);
                    if (value == null) value = new ArrayList<MoeDrugDto>();
                    value.add(dto);
                    map.put(key, value);
                }
            }
        }
        return map;
    }

    public static void addDtoToCollection(Map<String, List<MoeDrugDto>> map, List<String> keys, Collection<MoeDrugDto> collection) {
        if (keys != null && keys.size() > 0) {
            for (String drugName : keys) {
                List<MoeDrugDto> list = map.get(drugName);
                if (list != null) {
                    collection.addAll(list);
                }
            }
        }
    }

    public static boolean formExist(String formEng) {
        for (MoeFormDto dto : FORM_LIST) {
            if (dto.getFormEng().equals(formEng))
                return true;
        }
        return false;
    }

    public static boolean routeExist(String routeEng) {
        for (MoeRouteDto dto : ROUTE_LIST) {
            if (dto.getRouteEng().equals(routeEng))
                return true;
        }
        return false;
    }

    public static boolean siteExist(String siteEng) {
        for (MoeSiteDto dto : SITE_LIST) {
            if (dto.getSiteEng().equals(siteEng))
                return true;
        }
        return false;
    }

    public static MoeFreqDto getFreq(String freqCode) {
        for (MoeFreqDto dto : FREQS) {
            if (dto.getFreqCode().equals(freqCode))
                return dto;
        }
        return null;
    }

    public static boolean baseUnitExist(String baseUnit) {
        for (MoeBaseUnitDto dto : BASE_UNIT_LIST) {
            if (dto.getBaseUnit().equals(baseUnit))
                return true;
        }
        return false;
    }

    public static boolean prescribeUnitExist(String prescribeUnit) {
        for (MoeBaseUnitDto dto : PRESCRIBE_UNIT_LIST) {
            if (dto.getBaseUnit().equals(prescribeUnit))
                return true;
        }
        return false;
    }

    public static boolean doseFormExtraInfoExist(String doseFormExtraInfo) {
        for (MoeDoseFormExtraInfoPo dto : DOSE_FORM_EXTRA_INFO_LIST) {
            if (dto.getDoseFormExtraInfo().equals(doseFormExtraInfo)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isDrugExist(String drugKey) {
        return EXIST_DRUGS_MAP.containsKey(drugKey);
    }

    public static boolean isDrugWithAliasExist(String drugKey) {
        return EXIST_DRUGS_WITH_ALIAS_LIST.contains(drugKey);
    }

    public static boolean isDrugSuspend(String drugKey) {
        return SUSPEND_DRUGS_MAP.contains(drugKey);
    }

    public static MoeBaseUnitDto getCurrentBaseUnit(MoeMedProfileDto profile) {
        boolean isTradeNameAlias = MoeCommonHelper.isAliasNameTypeTradeNameAlias(profile.getNameType());
        String keyString = DtoUtil.getKeyString(profile, false);

        if (isTradeNameAlias) {
            keyString = profile.getMoeEhrMedProfile().getAliasName() + " " + keyString;
            MoeDrugLocalPo local = DtoUtil.EXIST_DRUGS_WITH_TRADE_ALIAS_MAP.get(keyString);

            if (local != null && local.getBaseUnit() != null) {
                return new MoeBaseUnitDto(local.getBaseUnitId(), local.getBaseUnit());
            }
        } else {
            MoeDrugLocalPo local = DtoUtil.EXIST_DRUGS_MAP.get(keyString);

            if (local != null && local.getBaseUnit() != null && local.getBaseUnitId() != null) {
                return new MoeBaseUnitDto(local.getBaseUnitId(), local.getBaseUnit());
            }
        }

        return null;
    }

    public static String getCurrentDrugCategoryId(MoeMedProfileDto profile) {
        String keyString = DtoUtil.getKeyString(profile, false);
        MoeDrugLocalPo local = DtoUtil.EXIST_DRUGS_MAP.get(keyString);

        if (local != null && local.getMoeDrugOrdPropertyLocal() != null) {
            return local.getMoeDrugOrdPropertyLocal().getDrugCategoryId();
        }

        return null;
    }

    public static Long getCurrentRouteId(String routeEng) {
        if (routeEng != null) {
            for (MoeRouteDto dto : ROUTE_LIST) {
                if (routeEng.equals(dto.getRouteEng())) {
                    return dto.getRouteId();
                }
            }
        }
        return null;
    }

    public static boolean isDrugExistWithTradeAlias(String drugKey) {
        return EXIST_DRUGS_WITH_TRADE_ALIAS_MAP.containsKey(drugKey);
    }

    public static boolean isDangerDrugExist(String drugKey) {
        return EXIST_DANGER_DRUGS.contains(drugKey);
    }

    public static boolean isSpecialtyDrugExist(String specKey, String drugKey, boolean isNonStrengthCompulsory) {
        List<String> hkregs = HKREG_BY_SPECIALTY.get(specKey);
        if (hkregs != null) {
            // for strength compulsory or non-strength compulsory drug with strength
            for (String hkreg : hkregs) {
                if (EXIST_SPECIALTY_DRUGS.get(hkreg).contains(drugKey)) {
                    return true;
                }
            }

            // for non-strength compulsory drug without strength
            if (isNonStrengthCompulsory && EXIST_NS_SPECIALTY_DRUGS.get(specKey) != null) {
                return EXIST_NS_SPECIALTY_DRUGS.get(specKey).contains(drugKey);
            }
        }
        return false;
    }

    public static String getKeyString(MoeMedProfileDto profile, boolean withStrength) {
        StringBuilder sb = new StringBuilder();
        sb.append(profile.getTradename());
        sb.append(" " + profile.getMoeEhrMedProfile().getVtm());
        sb.append(" " + profile.getMoeEhrMedProfile().getDrugRouteEng());
        sb.append(" " + profile.getFormDesc());
        if (profile.getMoeEhrMedProfile().getDoseFormExtraInfo() != null) {
            sb.append(" " + profile.getMoeEhrMedProfile().getDoseFormExtraInfo());
        }
        if (profile.getModu() != null) {
            sb.append(" " + profile.getModu());
        }
        if (MoeCommonHelper.isAliasNameTypeLocalDrug(profile.getNameType())) {
            sb.append(" " + profile.getMoeEhrMedProfile().getScreenDisplay());
        } else if (withStrength || (profile.getMoeEhrMedProfile().getStrengthCompulsory() != null &&
                ServerConstant.DB_FLAG_TRUE.equalsIgnoreCase(profile.getMoeEhrMedProfile().getStrengthCompulsory()))) {
            if (MoeDrugDto.isMultiIngr(profile.getMoeEhrMedProfile().getVtm()) &&
                    profile.getMoeEhrMedProfile().getScreenDisplay() != null) {
                sb.append(" " + profile.getMoeEhrMedProfile().getScreenDisplay());
            }
        }
        if (profile.getStrength() != null) {
            sb.append(" " + profile.getStrength());
        }
        if (profile.getMoeEhrMedProfile().getStrengthLevelExtraInfo() != null) {
            sb.append(" " + profile.getMoeEhrMedProfile().getStrengthLevelExtraInfo());
        }

        sb.append(" " + profile.getMoeEhrMedProfile().getStrengthCompulsory());

		/*if(profile.getDangerdrug() == ServerConstant.DB_FLAG_FALSE){ // non-danger drug
			if(profile.getMoQtyUnit() != null) {
				sb.append(" "+profile.getMoQtyUnit());
			}
		}*/
        return sb.toString();
    }

    public static boolean isCurrentExist(MoeMedProfileDto profile, String specKey) {

        String keyString = getKeyString(profile, false);

        String currentDanger = DtoUtil.isDangerDrugExist(keyString) ? "Y" : "N";

        if (!profile.getDangerdrug().equals(currentDanger)) {
            return false;
        }

        boolean isNonStrengthCompulsory = MoeCommonHelper.isStrengthCompulsoryNo(profile.getMoeEhrMedProfile().getStrengthCompulsory());
        if (DtoUtil.isSpecialtyDrugExist(specKey, keyString, isNonStrengthCompulsory)) {
            return false;
        }

        // ask lau chi wai for the explanation of checking trade name alias on old records b4 20161130 release - synchronzie drug maintenance data to my favourite record
        if (MoeCommonHelper.isAliasNameTypeTradeNameAlias(profile.getMoeEhrMedProfile().getAliasNameType())) {
            if (!DtoUtil.isDrugExistWithTradeAlias(profile.getMoeEhrMedProfile().getAliasName() + " " + keyString)) {
                return false;
            }
        } else {
            if (profile.getMoeEhrMedProfile().getAliasName() == null) {
                if (!DtoUtil.isDrugExist(keyString)) {
                    return false;
                }
            } else if (!DtoUtil.isDrugWithAliasExist(keyString + " " + profile.getMoeEhrMedProfile().getAliasName())) {
                return false;
            }
        }

        return true;
    }

    public static boolean notCurrentSuspend(MoeMedProfileDto profile, String specKey) {
        String keyString = getKeyString(profile, false);
        if (DtoUtil.isDrugSuspend(keyString)) {
            return false;
        }
        return true;
    }

    public static MoeDrugLocalPo getCorrespondingLocalDrug(MoeMedProfileDto profile) {
        MoeDrugLocalPo local = null;
        if (!MoeCommonHelper.isAliasNameTypeTradeNameAlias(profile.getNameType())) {
            local = DtoUtil.EXIST_DRUGS_MAP.get(getKeyString(profile, false));
        } else {
            String keyString = profile.getMoeEhrMedProfile().getAliasName() + " " + getKeyString(profile, false);
            local = DtoUtil.EXIST_DRUGS_WITH_TRADE_ALIAS_MAP.get(keyString);
        }
        return local;
    }

    public static MoeSupplFreqDto getMoeSupplFreqDto(String regimen, String freqCode) {
        List<MoeSupplFreqDto> list = REGIMEN_TYPE_MAP.get(regimen);
        if (list != null) {
            for (MoeSupplFreqDto suppl : list) {
                if (suppl.getSupplFreqEng().equals(freqCode)) {
                    return suppl;
                }
            }
        }
        return null;
    }

    public static HashMap<String, String> convertTradeNameAliasMap(List<Object[]> tradeNameAlias) {
        HashMap<String, String> tradeNameAliasList = new HashMap<String, String>();
        for (Object[] t : tradeNameAlias) {
            tradeNameAliasList.put((String) t[0], (String) t[1]);
        }
        return tradeNameAliasList;
    }

    public static long getBaseUnitDoseId() {
        return baseUnitDoseId;
    }

    public static void setBaseUnitDoseId(long baseUnitDoseIdInput) {
        baseUnitDoseId = baseUnitDoseIdInput;
    }

    public static void setCurrentFreqAndSupFreqText(MoeMedProfileDto moeMedProfileDto) {
        String freqCode = moeMedProfileDto.getFreqCode();
        String freqText = moeMedProfileDto.getFreqText();
        Long freq1 = moeMedProfileDto.getFreq1();

        String regiment = moeMedProfileDto.getRegimen();
        String supFreqCode = moeMedProfileDto.getSupFreqCode();
        String supFreqText = moeMedProfileDto.getSupFreqText();
        Long suppFreq1 = moeMedProfileDto.getSupFreq1();
        Long suppFreq2 = moeMedProfileDto.getSupFreq2();
        Long cycleMultiplier = moeMedProfileDto.getMoeEhrMedProfile().getCycleMultiplier();

        List<MoeMedMultDoseDto> medMultDoses = moeMedProfileDto.getMoeMedMultDoses();

        String orderLineType = moeMedProfileDto.getMoeEhrMedProfile().getOrderLineType();

        if (medMultDoses != null && medMultDoses.size() > 0) {
            for (MoeMedMultDoseDto medMultDoseDto : medMultDoses) {
                freqCode = medMultDoseDto.getFreqCode();
                freqText = medMultDoseDto.getFreqText();
                freq1 = medMultDoseDto.getFreq1();

                supFreqCode = medMultDoseDto.getSupFreqCode();
                supFreqText = medMultDoseDto.getSupFreqText();
                suppFreq1 = medMultDoseDto.getSupFreq1();
                suppFreq2 = medMultDoseDto.getSupFreq2();

                medMultDoseDto.setCurrentFreqText(getCurFreqText(freqCode, freqText, freq1));
                medMultDoseDto.setCurrentSupFreqText(getCurSupFreqText(regiment, freqCode, supFreqCode, supFreqText, suppFreq1, suppFreq2, cycleMultiplier));
            }
        } else {
            moeMedProfileDto.setCurrentFreqText(getCurFreqText(freqCode, freqText, freq1));
            moeMedProfileDto.setCurrentSupFreqText(getCurSupFreqText(regiment, freqCode, supFreqCode, supFreqText, suppFreq1, suppFreq2, cycleMultiplier));
        }
    }

    public static String getCurFreqText(String freqCode, String freqText, Long freq1) {
        String curFreqText = null;

        if (freqCode != null && freqText != null) {
            MoeFreqDto curFreqDto = getCurFreqDto(freqCode);

            if (curFreqDto != null) {
                curFreqText = curFreqDto.getFreqEng();

                if (freqCode.contains(ServerConstant.BLANKS_IND)) {
                    if (freqCode.equals(ServerConstant.FREQ_CHANGE_NUM_TO_WORD)) {
                        curFreqText = freqText;
                    } else if (freq1 != null) {
                        curFreqText = curFreqText.replaceAll(ServerConstant.BLANKS_IND, freq1.toString());
                    }
                }
            }
        }

        return curFreqText;
    }

    public static String getCurSupFreqText(String regiment, String freqCode, String supFreqCode, String supFreqText, Long suppFreq1, Long suppFreq2, Long cycleMultiplier) {
        String curSupFreqText = null;

        if (supFreqCode != null && regiment != null) {
            if (supFreqCode.equals(ServerConstant.ON_EVEN_ODD) || supFreqCode.equals(ServerConstant.ON_WEEK_DAYS)) {
                curSupFreqText = supFreqText;
            } else {
                List<MoeSupplFreqDto> suppFreqDTOList = REGIMEN_TYPE_MAP.get(regiment);
                MoeSupplFreqDto moeSupplFreqDto = null;
                for (MoeSupplFreqDto dto : suppFreqDTOList) {
                    if (dto != null && supFreqCode.equals(dto.getSupplFreqEng())) {
                        moeSupplFreqDto = dto;
                        break;
                    }
                }

                if (moeSupplFreqDto != null && moeSupplFreqDto.getMoeSupplFreqDescs() != null) {
                    List<MoeSupplFreqDescDto> moeSupplFreqDescDTOList = moeSupplFreqDto.getMoeSupplFreqDescs();
                    ArrayList<MoeSupplFreqDescDto> targetMoeSupplFreqDescDTOList = new ArrayList<MoeSupplFreqDescDto>();
                    ArrayList<MoeSupplFreqDescDto> defaultSameFreqMoeSupplFreqDescDTOList = new ArrayList<MoeSupplFreqDescDto>();
                    ArrayList<MoeSupplFreqDescDto> defaultMoeSupplFreqDescDTOList = new ArrayList<MoeSupplFreqDescDto>();
                    MoeSupplFreqDescDto targetMoeSupplFreqDescDTO = null;

                    if (moeSupplFreqDto.getMoeSupplFreqSelections().size() == 1) {
                        for (MoeSupplFreqDescDto dto : moeSupplFreqDescDTOList) {
                            if (dto.getFreqCode() != null && dto.getFreqCode().equals(freqCode)) {
                                if (StringUtil.nullToZero(suppFreq1).equals(dto.getSupplFreq1())) {
                                    targetMoeSupplFreqDescDTOList.add(dto);
                                } else if (dto.getSupplFreq1() == null) {
                                    defaultSameFreqMoeSupplFreqDescDTOList.add(dto);
                                }
                            } else if (dto.getFreqCode() == null && StringUtil.nullToZero(suppFreq1).equals(dto.getSupplFreq1())) {
                                targetMoeSupplFreqDescDTOList.add(dto);
                            } else if (dto.getFreqCode() == null && dto.getSupplFreq1() == null) {
                                defaultMoeSupplFreqDescDTOList.add(dto);
                            }
                        }
                    }

                    if (!targetMoeSupplFreqDescDTOList.isEmpty()) {
                        targetMoeSupplFreqDescDTO = targetMoeSupplFreqDescDTOList.get(0);
                    } else if (!defaultSameFreqMoeSupplFreqDescDTOList.isEmpty()) {
                        targetMoeSupplFreqDescDTO = defaultSameFreqMoeSupplFreqDescDTOList.get(0);
                    } else if (!defaultMoeSupplFreqDescDTOList.isEmpty()) {
                        targetMoeSupplFreqDescDTO = defaultMoeSupplFreqDescDTOList.get(0);
                    }

                    //replace __ to value
                    String suppFreqText = moeSupplFreqDto.getPrefixSupplFreqEng() != null ? moeSupplFreqDto.getPrefixSupplFreqEng() + " " + moeSupplFreqDto.getSupplFreqEng() : moeSupplFreqDto.getSupplFreqEng();
                    if (suppFreqText.indexOf(ServerConstant.BLANKS_IND) > 0) {
                        suppFreqText = (suppFreq1 == null) ? suppFreqText : suppFreqText.replaceFirst(ServerConstant.BLANKS_IND, " " + Long.toString(suppFreq1) + " ");
                        suppFreqText = (suppFreq2 == null) ? suppFreqText : suppFreqText.replaceFirst(ServerConstant.BLANKS_IND, " " + Long.toString(suppFreq2) + " ");
                        //suppFreqText = (cycleMultiplier == null) ? suppFreqText : suppFreqText.replaceFirst(ServerConstant.BLANKS_IND, " " + Integer.toString(cycleMultiplier)+ " ");
                    }

                    curSupFreqText = suppFreqText;
                }
            }
        }

        return curSupFreqText;
    }

    public static MoeFreqDto getCurFreqDto(String freqCode) {
        MoeFreqDto curFreqDto = null;

        if (freqCode != null) {
            for (MoeFreqDto moeFreqDto : FREQS_LOCAL) {
                if (freqCode.equals(moeFreqDto.getFreqCode())) {
                    return moeFreqDto;
                }
            }
        }

        return curFreqDto;
    }


}
