package hk.health.moe.util;

import hk.health.moe.common.MoeCommonHelper;
import hk.health.moe.pojo.dto.MoeMedProfileDto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

    public static final ThreadLocal<DateFormat> sdfThreadLocal = new ThreadLocal<DateFormat>() {
        @Override
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
    };

    public static String formatDate(Date date) {
        return sdfThreadLocal.get().format(date);
    }

    public static void calEndDate(MoeMedProfileDto profile, Date orderDate) {
        long regimenMultipler = 0;

        if (!MoeCommonHelper.isAliasNameTypeFreeText(profile.getNameType())) {

            if (profile.getStartDate() == null) {
                profile.setStartDate(new Date());
            }
        } else {
            if (orderDate != null) {
                profile.setStartDate(orderDate);
            } else {
                profile.setStartDate(new Date());
            }
        }


        if (profile.getMoeEhrMedProfile().getCycleMultiplier() != null) {
            regimenMultipler = profile.getMoeEhrMedProfile().getCycleMultiplier();
        } else {
            Long dum = DtoUtil.DURATION_UNIT_MAP.get(profile.getDurationUnit());
            if (dum != null) {
                regimenMultipler = dum.intValue();
            }
        }

        if (profile.getDuration() != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(clearTime(profile.getStartDate()));
            cal.add(Calendar.DAY_OF_MONTH, (int) (regimenMultipler * profile.getDuration()) - 1);
            profile.setEndDate(cal.getTime());
        } else {
            profile.setEndDate(profile.getStartDate());
        }

        if (profile.getStartDate() != null) {
            profile.setStartDate(clearTime(profile.getStartDate()));
        }
        if (profile.getEndDate() != null) {
            profile.setEndDate(clearTime(profile.getEndDate()));
        }
    }

    public static Date clearTime(Date date) {
        if (date == null) {
            return null;
        }
        Calendar c = Calendar.getInstance(Locale.US);
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }


    public static String formatDate(Date dt, String dateFormat) {
        if (dt == null) {
            return "";
        }
        SimpleDateFormat format = new SimpleDateFormat(dateFormat, Locale.US);
        return format.format(dt);
    }

    public void unload() {
        sdfThreadLocal.remove();
    }

}
