package hk.health.moe.util;

import hk.health.moe.common.ServerConstant;
import hk.health.moe.pojo.dto.MoeEhrMedAllergenDto;
import hk.health.moe.pojo.dto.MoeEhrMedMultDoseDto;
import hk.health.moe.pojo.dto.MoeEhrMedProfileDto;
import hk.health.moe.pojo.dto.MoeMedMultDoseDto;
import hk.health.moe.pojo.dto.MoeMedProfileDto;
import hk.health.moe.pojo.dto.UserDto;
import hk.health.moe.pojo.po.MoeEhrMedAllergenLogPo;
import hk.health.moe.pojo.po.MoeEhrMedAllergenPo;
import hk.health.moe.pojo.po.MoeEhrMedMultDoseLogPo;
import hk.health.moe.pojo.po.MoeEhrMedMultDosePo;
import hk.health.moe.pojo.po.MoeEhrMedProfileLogPo;
import hk.health.moe.pojo.po.MoeEhrMedProfilePo;
import hk.health.moe.pojo.po.MoeEhrOrderLogPo;
import hk.health.moe.pojo.po.MoeEhrOrderPo;
import hk.health.moe.pojo.po.MoeMedMultDoseLogPo;
import hk.health.moe.pojo.po.MoeMedMultDosePo;
import hk.health.moe.pojo.po.MoeMedProfileLogPo;
import hk.health.moe.pojo.po.MoeMedProfilePo;
import hk.health.moe.pojo.po.MoeOrderLogPo;
import hk.health.moe.pojo.po.MoeOrderPo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class OrderLogUtil {

    //public static MoeEhrOrderLogPo convertMoeEhrOrderLog(final MoeEhrOrderPo dto, UserDto userDto, char trxType, boolean convertMedProfile) {
    public static MoeEhrOrderLogPo convertMoeEhrOrderLog(final MoeEhrOrderPo dto, UserDto userDto, String trxType, boolean convertMedProfile) {
        MoeEhrOrderLogPo order = new MoeEhrOrderLogPo();

        order.setMoeOrderLog(convertMoeOrderLog(dto.getMoeOrder(), convertMedProfile));
        //chris 20190724 start
        // order.setId(order.getMoeOrderLog().getId());
        order.setOrdNo(order.getMoeOrderLog().getOrdNo());
        order.setRefNo(order.getMoeOrderLog().getRefNo());
        //chris 20190724 end
        order.setMoePatient(dto.getMoePatient());
        order.setMoePatientCase(dto.getMoePatientCase());

        order.setHospcode(dto.getHospcode());
        order.setCreateUserId(dto.getCreateUserId());
        order.setCreateUser(dto.getCreateUser());
        order.setCreateHosp(dto.getCreateHosp());
        order.setCreateRank(dto.getCreateRank());
        order.setCreateRankDesc(dto.getCreateRankDesc());
        order.setUpdateUserId(dto.getUpdateUserId());
        order.setUpdateUser(dto.getUpdateUser());
        order.setUpdateHosp(dto.getUpdateHosp());
        order.setUpdateRank(dto.getUpdateRank());
        order.setUpdateRankDesc(dto.getUpdateRankDesc());
        //if (userDto.getActionCd() != null && userDto.getActionCd().equalsIgnoreCase(ServerConstant.MODE_EDIT_WITH_REMARK) && trxType == 'D'){
        if (userDto.getActionCd() != null && userDto.getActionCd().equalsIgnoreCase(ServerConstant.MODE_EDIT_WITH_REMARK) && trxType.equals(ServerConstant.ORDER_TRANSACTION_TYPE_DELETE)){
            order.setRemarkUserId(userDto.getLoginId());
            order.setRemarkUser(userDto.getLoginName());
            order.setRemarkHosp(userDto.getHospitalCd());
            order.setRemarkRank(userDto.getUserRankCd());
            order.setRemarkRankDesc(userDto.getUserRankDesc());
        }

        for(MoeMedProfileLogPo profileLog : order.getMoeOrderLog().getMoeMedProfileLogs()) {
            //chris 20190724 start
            //profileLog.getMoeEhrMedProfileLog().setTrxType(trxType + "");
            profileLog.getMoeEhrMedProfileLog().setTrxType(trxType);
            //chris 20190724 end
        }

        return order;
    }


    public static MoeOrderLogPo convertMoeOrderLog(final MoeOrderPo dto, boolean convertMedProfile) {
        MoeOrderLogPo order = new MoeOrderLogPo();

//        MoeOrderLogId id = new MoeOrderLogId();
//        id.setOrdNo(dto.getOrdNo());
//        id.setRefNo(dto.getRefNo());
//        order.setId(id);
        order.setOrdNo(dto.getOrdNo());
        order.setRefNo(dto.getRefNo());

        order.setHospcode(dto.getHospcode());
        order.setWorkstore(dto.getWorkstore());
        order.setPatHospcode(dto.getPatHospcode());
        order.setOrdDate(dto.getOrdDate());
        order.setOrdType(dto.getOrdType());
        order.setOrdSubtype(dto.getOrdSubtype());
        order.setOrdStatus(dto.getOrdStatus());
        order.setMoCode(dto.getMoCode());
        order.setPrevOrdNo(dto.getPrevOrdNo());
        order.setMaxItemNo(dto.getMaxItemNo());
        order.setEndDate(dto.getEndDate());
        order.setSuspend(dto.getSuspend());
        order.setPrivatePatient(dto.getPrivatePatient());
        order.setTransferPatient(dto.getTransferPatient());
        order.setAllowModify(dto.getAllowModify());
        order.setPrescType(dto.getPrescType());
        order.setMoeOrder(dto.getMoeOrder());
        order.setCaseNo(dto.getCaseNo());
        order.setSrcSpecialty(dto.getSrcSpecialty());
        order.setSrcSubspecialty(dto.getSrcSubspecialty());
        order.setIpasWard(dto.getIpasWard());
        order.setBedNo(dto.getBedNo());
        order.setSpecialty(dto.getSpecialty());
        order.setSubspecialty(dto.getSubspecialty());
        order.setPhsWard(dto.getPhsWard());
        order.setEisSpecCode(dto.getEisSpecCode());
        order.setDispHospcode(dto.getDispHospcode());
        order.setDispWorkstore(dto.getDispWorkstore());
        order.setTicknum(dto.getTicknum());
        order.setTickdate(dto.getTickdate());
        order.setWkstatcode(dto.getWkstatcode());
        order.setLastUpdDate(dto.getLastUpdDate());
        order.setLastUpdBy(dto.getLastUpdBy());
        order.setDispOrdNo(dto.getDispOrdNo());
        order.setDischargeReason(dto.getDischargeReason());
        order.setRemarkCreateBy(dto.getRemarkCreateBy());
        order.setRemarkCreateDate(dto.getRemarkCreateDate());
        order.setRemarkText(dto.getRemarkText());
        order.setRemarkConfirmBy(dto.getRemarkConfirmBy());
        order.setRemarkConfirmDate(dto.getRemarkConfirmDate());
        order.setRemarkStatus(dto.getRemarkStatus());
        order.setLastPrescType(dto.getLastPrescType());
        order.setUncollect(dto.getUncollect());
        order.setAllowPostComment(dto.getAllowPostComment());
        order.setPrintType(dto.getPrintType());
        order.setPrevHospcode(dto.getPrevHospcode());
        order.setPrevWorkstore(dto.getPrevWorkstore());
        order.setPrevDispDate(dto.getPrevDispDate());
        order.setPrevTicknum(dto.getPrevTicknum());
        order.setVersion(dto.getVersion());

        if(convertMedProfile) {
            order.setMoeMedProfileLogs(convertMoeMedProfileLogs(dto.getMoeMedProfiles(), order));
        }

        return order;
    }


    public static Set<MoeMedProfileLogPo> convertMoeMedProfileLogs(final Set<MoeMedProfilePo> dtos, MoeOrderLogPo order) {
        Set<MoeMedProfileLogPo> profiles = new TreeSet<MoeMedProfileLogPo>();
        if(dtos != null) {
            for(MoeMedProfilePo dto : dtos) {
                profiles.add(convertMoeMedProfileLog(dto, order));
            }
        }
        return profiles;
    }


    public static MoeMedProfileLogPo convertMoeMedProfileLog(final MoeMedProfilePo dto, MoeOrderLogPo order) {
        MoeMedProfileLogPo profile = new MoeMedProfileLogPo();

//        MoeMedProfileLogId id = new MoeMedProfileLogId();
//        id.setOrderLogId(order.getId());
//        id.setHospcode(order.getHospcode());
//        id.setCmsItemNo(dto.getId().getCmsItemNo());
//        profile.setId(id);

        profile.setOrdNo(order.getOrdNo());
        profile.setHospcode(order.getHospcode());
        profile.setCmsItemNo(dto.getCmsItemNo());
        profile.setRefNo(order.getRefNo());

        profile.setMoeOrderLog(order);

        profile.setPatHospcode(order.getPatHospcode());
        profile.setCaseNo(order.getCaseNo());
        profile.setRegimen(dto.getRegimen());
        profile.setMultDose(dto.getMultDose());
        profile.setVolValue(dto.getVolValue());
        profile.setVolUnit(dto.getVolUnit());
        profile.setVolText(dto.getVolText());
        profile.setItemcode(dto.getItemcode());
        profile.setFirstDisplayname(dto.getFirstDisplayname());
        profile.setTradename(dto.getTradename());
        profile.setStrength(dto.getStrength());
        profile.setBaseunit(dto.getBaseunit());
        profile.setFormcode(dto.getFormcode());
        profile.setRouteCode(dto.getRouteCode());
        profile.setRouteDesc(dto.getRouteDesc());
        profile.setFormDesc(dto.getFormDesc());
        profile.setSalt(dto.getSalt());
        profile.setExtraInfo(dto.getExtraInfo());
        profile.setDangerdrug(dto.getDangerdrug());
        profile.setExternalUse(dto.getExternalUse());
        profile.setNameType(dto.getNameType());
        profile.setSecondDisplayname(dto.getSecondDisplayname());
        profile.setDosage(dto.getDosage());
        profile.setModu(dto.getModu());
        profile.setFreqCode(dto.getFreqCode());
        profile.setFreq1(dto.getFreq1());
        profile.setSupFreqCode(dto.getSupFreqCode());
        profile.setSupFreq1(dto.getSupFreq1());
        profile.setSupFreq2(dto.getSupFreq2());
        profile.setDayOfWeek(dto.getDayOfWeek());
        profile.setAdminTimeCode(dto.getAdminTimeCode());
        profile.setPrn(dto.getPrn());
        profile.setPrnPercent(dto.getPrnPercent());
        profile.setSiteCode(dto.getSiteCode());
        profile.setSupSiteDesc(dto.getSupSiteDesc());
        profile.setDuration(dto.getDuration());
        profile.setDurationUnit(dto.getDurationUnit());
        profile.setStartDate(dto.getStartDate());
        profile.setEndDate(dto.getEndDate());
        profile.setMoQty(dto.getMoQty());
        profile.setMoQtyUnit(dto.getMoQtyUnit());
        profile.setActionStatus(dto.getActionStatus());
        profile.setFormulStatus(dto.getFormulStatus());
        profile.setFormulStatus2(dto.getFormulStatus2());
        profile.setPatType(dto.getPatType());
        profile.setItemStatus(dto.getItemStatus());
        profile.setSpecInstruct(dto.getSpecInstruct());
        profile.setSpecNote(dto.getSpecNote());
        profile.setTranslate(dto.getTranslate());
        profile.setFixPeriod(dto.getFixPeriod());
        profile.setRestricted(dto.getRestricted());
        profile.setSingleUse(dto.getSingleUse());
        profile.setMoCreate(dto.getMoCreate());
        profile.setDisplayRoutedesc(dto.getDisplayRoutedesc());
        profile.setTrueDisplayname(dto.getTrueDisplayname());
        profile.setOrgItemNo(dto.getOrgItemNo());
        profile.setAllowRepeat(dto.getAllowRepeat());
        profile.setFreqText(dto.getFreqText());
        profile.setSupFreqText(dto.getSupFreqText());
        profile.setTrueAliasname(dto.getTrueAliasname());
        profile.setCapdSystem(dto.getCapdSystem());
        profile.setCapdCalcium(dto.getCapdCalcium());
        profile.setCapdConc(dto.getCapdConc());
        profile.setDurationInputType(dto.getDurationInputType());
        profile.setCapdBaseunit(dto.getCapdBaseunit());
        profile.setSiteDesc(dto.getSiteDesc());
        profile.setRemarkCreateBy(dto.getRemarkCreateBy());
        profile.setRemarkCreateDate(dto.getRemarkCreateDate());
        profile.setRemarkText(dto.getRemarkText());
        profile.setRemarkConfirmBy(dto.getRemarkConfirmBy());
        profile.setRemarkConfirmDate(dto.getRemarkConfirmDate());
        profile.setTrueConc(dto.getTrueConc());
        profile.setMdsInfo(dto.getMdsInfo());
        profile.setAdminTimeDesc(dto.getAdminTimeDesc());
        profile.setCommentCreateBy(dto.getCommentCreateBy());
        profile.setCommentCreateDate(dto.getCommentCreateDate());
        profile.setUpdateBy(dto.getUpdateBy());
        profile.setUpdateTime(dto.getUpdateTime());

        profile.setMoeEhrMedProfileLog(convertMoeEhrMedProfileLog(dto.getMoeEhrMedProfile(), profile));

        if(dto.getMoeEhrMedAllergens() != null) {
            profile.setMoeEhrMedAllergenLogs(convertMoeEhrMedAllergenLogs(dto.getMoeEhrMedAllergens(), profile));
        }

        if(dto.getMoeMedMultDoses() != null) {
            profile.setMoeMedMultDoseLogs(convertMoeMedMultDoseLogs(dto.getMoeMedMultDoses(), profile));
        }

        return profile;
    }


    public static MoeEhrMedProfileLogPo convertMoeEhrMedProfileLog(final MoeEhrMedProfilePo dto, MoeMedProfileLogPo parent) {
        MoeEhrMedProfileLogPo profile = new MoeEhrMedProfileLogPo();
        profile.setMoeMedProfileLog(parent);
//        profile.setId(parent.getId());
        profile.setHospcode(parent.getHospcode());
        profile.setOrdNo(parent.getOrdNo());
        profile.setCmsItemNo(parent.getCmsItemNo());
        profile.setRefNo(parent.getRefNo());

        profile.setVtm(dto.getVtm());
        profile.setVtmId(dto.getVtmId());
        profile.setTradeName(dto.getTradeName());
        profile.setTradeNameVtmId(dto.getTradeNameVtmId());
        profile.setAliasName(dto.getAliasName());
        profile.setAliasNameType(dto.getAliasNameType());
        profile.setManufacturer(dto.getManufacturer());
        profile.setScreenDisplay(dto.getScreenDisplay());
        //chris 20190725 start
        if (dto.getOrderLineType() != null) {
            profile.setOrderLineType(dto.getOrderLineType());
        }
        //chris 20190725 end
        profile.setGenericIndicator(dto.getGenericIndicator());
        profile.setCycleMultiplier(dto.getCycleMultiplier());
        profile.setStrengthCompulsory(dto.getStrengthCompulsory());
        profile.setCreateUserId(dto.getCreateUserId());
        profile.setCreateUser(dto.getCreateUser());
        profile.setCreateHosp(dto.getCreateHosp());
        profile.setCreateRank(dto.getCreateRank());
        profile.setCreateRankDesc(dto.getCreateRankDesc());
        profile.setCreateDtm(dto.getCreateDtm());
        profile.setUpdateUserId(dto.getUpdateUserId());
        profile.setUpdateUser(dto.getUpdateUser());
        profile.setUpdateHosp(dto.getUpdateHosp());
        profile.setUpdateRank(dto.getUpdateRank());
        profile.setUpdateRankDesc(dto.getUpdateRankDesc());
        profile.setUpdateDtm(dto.getUpdateDtm());
        profile.setDrugId(dto.getDrugId());
        profile.setFormId(dto.getFormId());
        profile.setDoseFormExtraInfoId(dto.getDoseFormExtraInfoId());
        profile.setDoseFormExtraInfo(dto.getDoseFormExtraInfo());
        profile.setRouteId(dto.getRouteId());
        profile.setFreqId(dto.getFreqId());
        profile.setSupplFreqId(dto.getSupplFreqId());
        profile.setMoQtyUnitId(dto.getMoQtyUnitId());
        profile.setModuId(dto.getModuId());
        profile.setSiteId(dto.getSiteId());
        profile.setDrugRouteEng(dto.getDrugRouteEng());
        profile.setDrugRouteId(dto.getDrugRouteId());
        profile.setStrengthLevelExtraInfo(dto.getStrengthLevelExtraInfo());
        profile.setMedDiscontFlag(dto.getMedDiscontFlag());
        profile.setMedDiscontReasonCode(dto.getMedDiscontReasonCode());
        profile.setMedDiscontInformation(dto.getMedDiscontInformation());
        profile.setMedDiscontUserId(dto.getMedDiscontUserId());
        profile.setMedDiscontUser(dto.getMedDiscontUser());
        profile.setMedDiscontHosp(dto.getMedDiscontHosp());
        profile.setMedDiscontDtm(dto.getMedDiscontDtm());

        return profile;
    }


    public static Set<MoeEhrMedAllergenLogPo> convertMoeEhrMedAllergenLogs(final Set<MoeEhrMedAllergenPo> dtos, MoeMedProfileLogPo profile) {
        Set<MoeEhrMedAllergenLogPo> allergens = new TreeSet<MoeEhrMedAllergenLogPo>();
        if(dtos != null) {
            for(MoeEhrMedAllergenPo dto : dtos) {
                allergens.add(convertMoeEhrMedAllergenLog(dto, profile));
            }
        }
        return allergens;
    }


    public static MoeEhrMedAllergenLogPo convertMoeEhrMedAllergenLog(final MoeEhrMedAllergenPo dto, MoeMedProfileLogPo profile) {
        MoeEhrMedAllergenLogPo allergen = new MoeEhrMedAllergenLogPo();

        allergen.setMoeMedProfileLog(profile);

//        MoeEhrMedAllergenLogId allergenId = new MoeEhrMedAllergenLogId();
//        allergenId.setProfileLogId(profile.getId());
//        allergenId.setRowNo(dto.getId().getRowNo());
//
//        allergen.setId(allergenId);
        allergen.setHospcode(profile.getHospcode());
        allergen.setOrdNo(profile.getOrdNo());
        allergen.setCmsItemNo(profile.getCmsItemNo());
        //Chris 20190926  Fix for setRefNo  -Start
        allergen.setRefNo(profile.getRefNo());
        //Chris 20190926  Fix for setRefNo  -End

        //Chris 20190927  Fix for setRowNo  -Start
        allergen.setRowNo(dto.getRowNo());
        //Chris 20190927  Fix for setRowNo  -End

        allergen.setAllergen(dto.getAllergen());
        allergen.setMatchType(dto.getMatchType());
        allergen.setScreenMsg(dto.getScreenMsg());
        allergen.setCertainty(dto.getCertainty());
        allergen.setAdditionInfo(dto.getAdditionInfo());
        allergen.setOverrideReason(dto.getOverrideReason());
        allergen.setAckDate(dto.getAckDate());
        allergen.setAckBy(dto.getAckBy());
        allergen.setManifestation(dto.getManifestation());
        allergen.setOverrideStatus(dto.getOverrideStatus());
        return allergen;
    }


    public static Set<MoeMedMultDoseLogPo> convertMoeMedMultDoseLogs(final Set<MoeMedMultDosePo> dtos, MoeMedProfileLogPo parent) {
        Set<MoeMedMultDoseLogPo> doses = new TreeSet<MoeMedMultDoseLogPo>();
        if(dtos != null) {
            for(MoeMedMultDosePo dto : dtos) {
                doses.add(convertMoeMedMultDoseLog(dto, parent));
            }
        }
        return doses;
    }


    public static MoeMedMultDoseLogPo convertMoeMedMultDoseLog(final MoeMedMultDosePo dto, MoeMedProfileLogPo parent) {
        MoeMedMultDoseLogPo dose = new MoeMedMultDoseLogPo();

        dose.setMoeMedProfileLog(parent);

//        MoeMedMultDoseLogId id = new MoeMedMultDoseLogId();
//        id.setProfileLogId(parent.getId());
//        id.setStepNo(dto.getId().getStepNo());
//        id.setMultDoseNo(dto.getId().getMultDoseNo());
//        dose.setId(id);
        dose.setHospcode(parent.getHospcode());
        dose.setOrdNo(parent.getOrdNo());
        dose.setCmsItemNo(parent.getCmsItemNo());
        dose.setRefNo(parent.getRefNo());
        dose.setStepNo(dto.getStepNo());
        dose.setMultDoseNo(dto.getMultDoseNo());

        dose.setPatHospcode(parent.getPatHospcode());
        dose.setDosage(dto.getDosage());
        dose.setFreqCode(dto.getFreqCode());
        dose.setFreq1(dto.getFreq1());
        dose.setSupFreqCode(dto.getSupFreqCode());
        dose.setSupFreq1(dto.getSupFreq1());
        dose.setSupFreq2(dto.getSupFreq2());
        dose.setDayOfWeek(dto.getDayOfWeek());
        dose.setAdminTimeCode(dto.getAdminTimeCode());
        dose.setPrn(dto.getPrn());
        dose.setPrnPercent(dto.getPrnPercent());
        dose.setSiteCode(dto.getSiteCode());
        dose.setSupSiteDesc(dto.getSupSiteDesc());
        dose.setDuration(dto.getDuration());
        dose.setDurationUnit(dto.getDurationUnit());
        dose.setStartDate(dto.getStartDate());
        dose.setEndDate(dto.getEndDate());
        dose.setMoQty(dto.getMoQty());
        dose.setMoQtyUnit(dto.getMoQtyUnit());
        dose.setUpdateBy(dto.getUpdateBy());
        dose.setUpdateTime(dto.getUpdateTime());
        dose.setFreqText(dto.getFreqText());
        dose.setSupFreqText(dto.getSupFreqText());
        dose.setCapdSystem(dto.getCapdSystem());
        dose.setCapdCalcium(dto.getCapdCalcium());
        dose.setCapdConc(dto.getCapdConc());
        dose.setItemcode(dto.getItemcode());
        dose.setCapdBaseunit(dto.getCapdBaseunit());
        dose.setSiteDesc(dto.getSiteDesc());
        dose.setTrueConc(dto.getTrueConc());
        dose.setAdminTimeDesc(dto.getAdminTimeDesc());
        dose.setCapdChargeInd(dto.getCapdChargeInd());
        dose.setCapdChargeableUnit(dto.getCapdChargeableUnit());
        dose.setCapdChargeableAmount(dto.getCapdChargeableAmount());
        dose.setConfirmCapdChargeInd(dto.getConfirmCapdChargeInd());
        dose.setConfirmCapdChargeUnit(dto.getConfirmCapdChargeUnit());

        if(dto.getMoeEhrMedMultDose() != null) {
            dose.setMoeEhrMedMultDoseLog(convertMoeEhrMedMultDoseLog(dto.getMoeEhrMedMultDose(), dose));
        }

        return dose;
    }


    public static MoeEhrMedMultDoseLogPo convertMoeEhrMedMultDoseLog(final MoeEhrMedMultDosePo dto, MoeMedMultDoseLogPo parent) {
        MoeEhrMedMultDoseLogPo dose = new MoeEhrMedMultDoseLogPo();
        dose.setMoeMedMultDoseLog(parent);
//        dose.setId(parent.getId());
        dose.setHospcode(parent.getHospcode());
        dose.setOrdNo(parent.getOrdNo());
        dose.setCmsItemNo(parent.getCmsItemNo());
        dose.setStepNo(parent.getStepNo());
        dose.setMultDoseNo(parent.getMultDoseNo());
        dose.setRefNo(parent.getRefNo());

        dose.setFreqId(dto.getFreqId());
        dose.setSupplFreqId(dto.getSupplFreqId());
        dose.setMoQtyUnitId(dto.getMoQtyUnitId());
        return dose;
    }


    public static MoeEhrMedProfileDto convertMoeEhrMedProfileDTO(MoeEhrMedProfileLogPo profile) {
        MoeEhrMedProfileDto dto = new MoeEhrMedProfileDto();
        dto.setVtm(profile.getVtm());
        dto.setVtmId(profile.getVtmId());
        dto.setTradeName(profile.getTradeName());
        dto.setTradeNameVtmId(profile.getTradeNameVtmId());
        dto.setAliasName(profile.getAliasName());
        dto.setAliasNameType(profile.getAliasNameType());
        dto.setManufacturer(profile.getManufacturer());
        dto.setScreenDisplay(profile.getScreenDisplay());
        dto.setOrderLineType(profile.getOrderLineType());
        dto.setGenericIndicator(profile.getGenericIndicator());
        dto.setCycleMultiplier(profile.getCycleMultiplier());
        dto.setStrengthCompulsory(profile.getStrengthCompulsory());
        dto.setFormId(profile.getFormId());
        dto.setDoseFormExtraInfoId(profile.getDoseFormExtraInfoId());
        dto.setDoseFormExtraInfo(profile.getDoseFormExtraInfo());
        dto.setRouteId(profile.getRouteId());
        dto.setFreqId(profile.getFreqId());
        dto.setSupplFreqId(profile.getSupplFreqId());
        dto.setMoQtyUnitId(profile.getMoQtyUnitId());
        dto.setModuId(profile.getModuId());
        dto.setSiteId(profile.getSiteId());
        dto.setCreateUserId(profile.getCreateUserId());
        dto.setCreateUser(profile.getCreateUser());
        dto.setCreateHosp(profile.getCreateHosp());
        dto.setCreateRank(profile.getCreateRank());
        dto.setCreateRankDesc(profile.getCreateRankDesc());
        dto.setCreateDtm(profile.getCreateDtm());
        dto.setUpdateUserId(profile.getUpdateUserId());
        dto.setUpdateUser(profile.getUpdateUser());
        dto.setUpdateHosp(profile.getUpdateHosp());
        dto.setUpdateRank(profile.getUpdateRank());
        dto.setUpdateRankDesc(profile.getUpdateRankDesc());
        dto.setUpdateDtm(profile.getUpdateDtm());
        dto.setDrugId(profile.getDrugId());
        dto.setFormId(profile.getFormId());
        dto.setRouteId(profile.getRouteId());
        dto.setFreqId(profile.getFreqId());
        dto.setSupplFreqId(profile.getSupplFreqId());
        dto.setMoQtyUnitId(profile.getMoQtyUnitId());
        dto.setModuId(profile.getModuId());
        dto.setSiteId(profile.getSiteId());
        dto.setDrugRouteEng(profile.getDrugRouteEng());
        dto.setDrugRouteId(profile.getDrugRouteId());
        dto.setStrengthLevelExtraInfo(profile.getStrengthLevelExtraInfo());
        return dto;
    }

    public static MoeEhrMedAllergenDto convertMoeEhrMedAllergenDTO(MoeEhrMedAllergenLogPo allergen, MoeMedProfileLogPo profile) {
        MoeEhrMedAllergenDto dto = new MoeEhrMedAllergenDto();
        dto.setAckDate(allergen.getAckDate());
        dto.setAllergen(allergen.getAllergen());
        dto.setMatchType(allergen.getMatchType());
        dto.setScreenMsg(allergen.getScreenMsg());
        dto.setCertainty(allergen.getCertainty());
        dto.setAdditionInfo(allergen.getAdditionInfo());
        dto.setOverrideReason(allergen.getOverrideReason());
        dto.setAckBy(allergen.getAckBy());
        dto.setManifestation(allergen.getManifestation());
        dto.setOverrideStatus(allergen.getOverrideStatus());
        return dto;
    }

    public static List<MoeEhrMedAllergenDto> convertMoeEhrMedAllergenDTOs(Set<MoeEhrMedAllergenLogPo> allergens, MoeMedProfileLogPo profile) {
        List<MoeEhrMedAllergenDto> dtos = new ArrayList<MoeEhrMedAllergenDto>(0);
        if(allergens != null) {
            for(MoeEhrMedAllergenLogPo allergen : allergens) {
                dtos.add(convertMoeEhrMedAllergenDTO(allergen, profile));
            }
        }
        return dtos;
    }


    public static List<MoeMedMultDoseDto> convertMoeMedMultDoseDTOs(Set<MoeMedMultDoseLogPo> doses, MoeMedProfileLogPo profile) {
        List<MoeMedMultDoseDto> dtos = new ArrayList<MoeMedMultDoseDto>(0);
        if(doses != null) {
            for(MoeMedMultDoseLogPo dose : doses) {
                dtos.add(convertMoeMedMultDoseDTO(dose, profile));
            }
        }
        return dtos;
    }

    public static MoeMedMultDoseDto convertMoeMedMultDoseDTO(MoeMedMultDoseLogPo dose, MoeMedProfileLogPo profile) {
        MoeMedMultDoseDto dto = new MoeMedMultDoseDto();
        dto.setStepNo(dose.getStepNo());
        dto.setMultDoseNo(dose.getMultDoseNo());
        dto.setPatHospcode(profile.getPatHospcode());
        if (dose.getDosage() != null){
            dto.setDosage(new Double(dose.getDosage().floatValue()));
        }
        dto.setFreqCode(dose.getFreqCode());
        dto.setFreq1(dose.getFreq1());
        dto.setSupFreqCode(dose.getSupFreqCode());
        dto.setSupFreq1(dose.getSupFreq1());
        dto.setSupFreq2(dose.getSupFreq2());
        dto.setDayOfWeek(dose.getDayOfWeek());
        dto.setAdminTimeCode(dose.getAdminTimeCode());
        dto.setPrn(dose.getPrn());
        dto.setPrnPercent(dose.getPrnPercent());
        dto.setSiteCode(dose.getSiteCode());
        dto.setSupSiteDesc(dose.getSupSiteDesc());
        dto.setDuration(dose.getDuration());
        dto.setDurationUnit(dose.getDurationUnit());
        dto.setStartDate(dose.getStartDate());
        dto.setEndDate(dose.getEndDate());
        dto.setMoQty(dose.getMoQty());
        dto.setMoQtyUnit(dose.getMoQtyUnit());
        dto.setUpdateBy(dose.getUpdateBy());
        dto.setUpdateTime(dose.getUpdateTime());
        dto.setFreqText(dose.getFreqText());
        dto.setSupFreqText(dose.getSupFreqText());
        dto.setCapdSystem(dose.getCapdSystem());
        dto.setCapdCalcium(dose.getCapdCalcium());
        dto.setCapdConc(dose.getCapdConc());
        dto.setItemcode(dose.getItemcode());
        dto.setCapdBaseunit(dose.getCapdBaseunit());
        dto.setSiteDesc(dose.getSiteDesc());
        dto.setTrueConc(dose.getTrueConc());
        dto.setAdminTimeDesc(dose.getAdminTimeDesc());
        dto.setCapdChargeInd(dose.getCapdChargeInd());
        dto.setCapdChargeableUnit(dose.getCapdChargeableUnit());
        dto.setCapdChargeableAmount(dose.getCapdChargeableAmount());
        dto.setConfirmCapdChargeInd(dose.getConfirmCapdChargeInd());
        dto.setConfirmCapdChargeUnit(dose.getConfirmCapdChargeUnit());

        if(dose.getMoeEhrMedMultDoseLog() != null) {
            dto.setMoeEhrMedMultDose(convertMoeEhrMedMultDoseDTO(dose.getMoeEhrMedMultDoseLog()));
        }
        return dto;
    }

    public static MoeEhrMedMultDoseDto convertMoeEhrMedMultDoseDTO(MoeEhrMedMultDoseLogPo dose) {
        MoeEhrMedMultDoseDto dto = new MoeEhrMedMultDoseDto();
        dto.setFreqId(dose.getFreqId());
        dto.setSupplFreqId(dose.getSupplFreqId());
        dto.setMoQtyUnitId(dose.getMoQtyUnitId());
        return dto;
    }

    public static MoeMedProfileDto convertMoeMedProfileDTO(MoeMedProfileLogPo profile) {
        MoeMedProfileDto dto = new MoeMedProfileDto();

        if(profile.getMoeEhrMedProfileLog() != null) {
            dto.setMoeEhrMedProfile(convertMoeEhrMedProfileDTO(profile.getMoeEhrMedProfileLog()));
        }

        if(profile.getMoeEhrMedAllergenLogs() != null) {
            dto.setMoeEhrMedAllergens(convertMoeEhrMedAllergenDTOs(profile.getMoeEhrMedAllergenLogs(), profile));
        }

        if(profile.getMoeMedMultDoseLogs() != null) {
            dto.setMoeMedMultDoses(convertMoeMedMultDoseDTOs(profile.getMoeMedMultDoseLogs(), profile));
        }

        dto.setPatHospcode(profile.getPatHospcode());
        dto.setCaseNo(profile.getCaseNo());
        dto.setRegimen(profile.getRegimen());
        dto.setMultDose(profile.getMultDose());
        dto.setVolValue(profile.getVolValue());
        dto.setVolUnit(profile.getVolUnit());
        dto.setVolText(profile.getVolText());
        dto.setItemcode(profile.getItemcode());
        dto.setFirstDisplayname(profile.getFirstDisplayname());
        dto.setTradename(profile.getTradename());
        dto.setStrength(profile.getStrength());
        dto.setBaseunit(profile.getBaseunit());
        dto.setFormcode(profile.getFormcode());
        dto.setRouteCode(profile.getRouteCode());
        dto.setRouteDesc(profile.getRouteDesc());
        dto.setFormDesc(profile.getFormDesc());
        dto.setSalt(profile.getSalt());
        dto.setExtraInfo(profile.getExtraInfo());
        dto.setDangerdrug(profile.getDangerdrug());
        dto.setExternalUse(profile.getExternalUse());
        dto.setNameType(profile.getNameType());
        dto.setSecondDisplayname(profile.getSecondDisplayname());
        if(profile.getDosage() != null) {
            dto.setDosage(BigDecimal.valueOf(profile.getDosage().floatValue()));
        }
        dto.setModu(profile.getModu());
        dto.setFreqCode(profile.getFreqCode());
        dto.setFreq1(profile.getFreq1());
        dto.setSupFreqCode(profile.getSupFreqCode());
        dto.setSupFreq1(profile.getSupFreq1());
        dto.setSupFreq2(profile.getSupFreq2());
        dto.setDayOfWeek(profile.getDayOfWeek());
        dto.setAdminTimeCode(profile.getAdminTimeCode());
        dto.setPrn(profile.getPrn());
        dto.setPrnPercent(profile.getPrnPercent());
        dto.setSiteCode(profile.getSiteCode());
        dto.setSupSiteDesc(profile.getSupSiteDesc());
        dto.setDuration(profile.getDuration());
        dto.setDurationUnit(profile.getDurationUnit());
        dto.setStartDate(profile.getStartDate());
        dto.setEndDate(profile.getEndDate());
        dto.setMoQty(profile.getMoQty());
        dto.setMoQtyUnit(profile.getMoQtyUnit());
        dto.setActionStatus(profile.getActionStatus());
        dto.setFormulStatus(profile.getFormulStatus());
        dto.setFormulStatus2(profile.getFormulStatus2());
        dto.setPatType(profile.getPatType());
        dto.setItemStatus(profile.getItemStatus());
        dto.setSpecInstruct(profile.getSpecInstruct());
        dto.setSpecNote(profile.getSpecNote());
        dto.setTranslate(profile.getTranslate());
        dto.setFixPeriod(profile.getFixPeriod());
        dto.setRestricted(profile.getRestricted());
        dto.setSingleUse(profile.getSingleUse());
        dto.setMoCreate(profile.getMoCreate());
        dto.setDisplayRoutedesc(profile.getDisplayRoutedesc());
        dto.setTrueDisplayname(profile.getTrueDisplayname());
        dto.setOrgItemNo(profile.getOrgItemNo());
        dto.setAllowRepeat(profile.getAllowRepeat());
        dto.setFreqText(profile.getFreqText());
        dto.setSupFreqText(profile.getSupFreqText());
        dto.setTrueAliasname(profile.getTrueAliasname());
        dto.setCapdSystem(profile.getCapdSystem());
        dto.setCapdCalcium(profile.getCapdCalcium());
        dto.setCapdConc(profile.getCapdConc());
        dto.setDurationInputType(profile.getDurationInputType());
        dto.setCapdBaseunit(profile.getCapdBaseunit());
        dto.setSiteDesc(profile.getSiteDesc());
        dto.setRemarkCreateBy(profile.getRemarkCreateBy());
        dto.setRemarkCreateDate(profile.getRemarkCreateDate());
        dto.setRemarkText(profile.getRemarkText());
        dto.setRemarkConfirmBy(profile.getRemarkConfirmBy());
        dto.setRemarkConfirmDate(profile.getRemarkConfirmDate());
        dto.setTrueConc(profile.getTrueConc());
        dto.setMdsInfo(profile.getMdsInfo());
        dto.setAdminTimeDesc(profile.getAdminTimeDesc());
        dto.setCommentCreateBy(profile.getCommentCreateBy());
        dto.setCommentCreateDate(profile.getCommentCreateDate());
        dto.setUpdateBy(profile.getUpdateBy());
        dto.setUpdateTime(profile.getUpdateTime());
        return dto;
    }


}
