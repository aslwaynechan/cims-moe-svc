package hk.health.moe.util;

import hk.health.moe.common.ServerConstant;
import hk.health.moe.pojo.dto.webservice.MedMultDoseDto;
import hk.health.moe.pojo.dto.webservice.MedProfileDto;
import hk.health.moe.pojo.dto.webservice.PrescriptionDto;
import hk.health.moe.pojo.dto.MoeActionStatusDto;
import hk.health.moe.pojo.po.MoeBaseUnitTermIdMapPo;
import hk.health.moe.pojo.po.MoeConjunctionTermIdMapPo;
import hk.health.moe.pojo.po.MoeDurationTermIdMapPo;
import hk.health.moe.pojo.po.MoeEhrMedProfileLogPo;
import hk.health.moe.pojo.po.MoeEhrOrderLogPo;
import hk.health.moe.pojo.po.MoeFreqTermIdMapPo;
import hk.health.moe.pojo.po.MoeMedMultDoseLogPo;
import hk.health.moe.pojo.po.MoeMedProfileLogPo;
import hk.health.moe.pojo.po.MoeOrderLogPo;
import hk.health.moe.pojo.po.MoePrescribeUnitTermIdMapPo;
import hk.health.moe.pojo.po.MoeRouteTermIdMapPo;
import hk.health.moe.pojo.po.MoeSiteTermIdMapPo;
import hk.health.moe.pojo.po.MoeSupplFreqTermIdMapPo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @Author Simon
 * @Date 2019/11/11
 */
public class EhrWsDtoMapUtil {

    public static PrescriptionDto convertMoeEhrOrderDto(MoeEhrOrderLogPo order, String uploadMode) {
        PrescriptionDto dto = new PrescriptionDto();

        dto.setOrderNo(order.getOrdNo());
        //Dto.setPatientKey(order.getMoePatient().getPatientRefKey());
        dto.setEpisodeNo(order.getMoeOrderLog().getCaseNo());
        dto.setHospCode(order.getHospcode());
        MoeOrderLogPo moeOrder = order.getMoeOrderLog();

        if (moeOrder !=null) {
            if ("CO".equals(moeOrder.getOrdStatus())){
                dto.setRefNo("CO");
            }
            else{
                dto.setRefNo(moeOrder.getRefNo());
            }

            dto.setPrescType(moeOrder.getPrescType().toString());
            dto.setOrdSubType(moeOrder.getOrdSubtype()==null?null:moeOrder.getOrdSubtype().toString());
            dto.setSpecialty(moeOrder.getSpecialty());
            dto.setWard(moeOrder.getIpasWard());
            dto.setHospName(SystemSettingUtil.getHospParamValue(moeOrder.getHospcode()+" "+moeOrder.getSpecialty(),
                    ServerConstant.PARAM_NAME_HOSPITAL_NAME_ENG));
            dto.setCreateDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(moeOrder.getOrdDate()));
            dto.setUpdateDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(moeOrder.getLastUpdDate()));
            dto.getMedProfileDto().addAll(convertMoeMedProfileDtos(moeOrder.getMoeMedProfileLogs(), uploadMode));
            dto.setTransactionDtm(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(moeOrder.getLastUpdDate()));
        }

        dto.setCreateUserId(order.getCreateUserId());
        dto.setCreateUser(order.getCreateUser());
        dto.setCreateHosp(order.getCreateHosp());
        dto.setCreateRank(order.getCreateRank());
        dto.setCreateRankDesc(order.getCreateRankDesc());
        dto.setUpdateUserId(order.getUpdateUserId());
        dto.setUpdateUser(order.getUpdateUser());
        dto.setUpdateHosp(order.getUpdateHosp());
        dto.setUpdateRank(order.getUpdateRank());
        dto.setUpdateRankDesc(order.getUpdateRankDesc());


        return dto;
    }

    public static List<MedProfileDto> convertMoeMedProfileDtos(Set<MoeMedProfileLogPo> profiles, String uploadMode) {
        List<MedProfileDto> dtos = new ArrayList<MedProfileDto>(0);
        if(profiles != null) {
            for(MoeMedProfileLogPo profile : profiles) {
                dtos.add(convertMoeMedProfileDto(profile, uploadMode));
            }
        }
        return dtos;
    }

    public static MedProfileDto convertMoeMedProfileDto(MoeMedProfileLogPo profile, String uploadMode) {
        MedProfileDto dto = new MedProfileDto();

        if(profile.getMoeMedMultDoseLogs() != null && profile.getMoeMedMultDoseLogs().size() > 0) {
            dto.getMedMultDoseDto().addAll(convertMoeMedMultDoseDtos(profile.getMoeMedMultDoseLogs(), profile));
        }

        dto.setLocalDrugKey(profile.getItemcode());
        dto.setItemNo(profile.getCmsItemNo());
        String [] doseInstruction = WsFormatter.toMedProfileDisplayString(OrderLogUtil.convertMoeMedProfileDTO(profile), profile.getMoeOrderLog().getOrdDate(), false);
        dto.setDoseInstruction(doseInstruction[0]);
        dto.setDoseInstructionWithStyle(doseInstruction[1]);
        dto.setDoseInstructionPrinting(doseInstruction[2]);
//        dto.setRemarkCreateBy(profile.getRemarkCreateBy());
//        dto.setRemarkCreateDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(profile.getRemarkCreateDate()));
//        dto.setRemarkText(profile.getRemarkText());

        MoeEhrMedProfileLogPo ehrMedProfile = profile.getMoeEhrMedProfileLog();

        if(ehrMedProfile != null) {

            if (ehrMedProfile.getDrugId() != null ){
                dto.setDrugTermId(ehrMedProfile.getDrugId());
            }
            dto.setDrugRoute(ehrMedProfile.getDrugRouteEng());
            if (ehrMedProfile.getMoeMedProfileLog().getMoeEhrMedProfileLog().getMoeDrugTermDescIdMap() != null){
                dto.setScreenDisplay(ehrMedProfile.getMoeMedProfileLog().getMoeEhrMedProfileLog().getMoeDrugTermDescIdMap().getTermDesc());
            }
            dto.setVtm(ehrMedProfile.getVtm());
            if (ehrMedProfile.getVtmId()!=null) {
                dto.setVtmId(ehrMedProfile.getVtmId());
            }
            dto.setDangerousDrug(ehrMedProfile.getMoeMedProfileLog().getDangerdrug());
            dto.setAliasName(ehrMedProfile.getAliasName());
            dto.setAliasNameType(ehrMedProfile.getAliasNameType()==null?null:ehrMedProfile.getAliasNameType().toString());
            dto.setManufacturer(ehrMedProfile.getManufacturer());
            if (ehrMedProfile.getGenericIndicator()!=null) {
                dto.setGenericIndicator(ehrMedProfile.getGenericIndicator().toString());
            }
            dto.setDrugName(ehrMedProfile.getScreenDisplay());
            if (ehrMedProfile.getOrderLineType()!=null){
                dto.setOrderLineType(ehrMedProfile.getOrderLineType().toString());
            }
            dto.setFormExtraInfo(ehrMedProfile.getDoseFormExtraInfo());
            dto.setStrengthExtraInfo(ehrMedProfile.getStrengthLevelExtraInfo());

            dto.setCreateDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(ehrMedProfile.getCreateDtm()));
            dto.setCreateBy(ehrMedProfile.getCreateUserId());
            dto.setCreateUser(ehrMedProfile.getCreateUser());
            dto.setCreateHosp(ehrMedProfile.getCreateHosp());
            dto.setCreateRank(ehrMedProfile.getCreateRank());
            dto.setCreateRankDesc(ehrMedProfile.getCreateRankDesc());

            dto.setUpdateDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(ehrMedProfile.getUpdateDtm()));
            dto.setUpdateBy(ehrMedProfile.getUpdateUserId());
            dto.setUpdateUser(ehrMedProfile.getUpdateUser());
            dto.setUpdateHosp(ehrMedProfile.getUpdateHosp());
            dto.setUpdateRank(ehrMedProfile.getUpdateRank());
            dto.setUpdateRankDesc(ehrMedProfile.getUpdateRankDesc());

            if ("DM".equals(uploadMode)){
                dto.setTrxType("I");
            }
            else{
                if ("N".equalsIgnoreCase(ehrMedProfile.getTrxType())){
                    dto.setTrxType("U");
                }
                else{
                    dto.setTrxType(String.valueOf(ehrMedProfile.getTrxType()));
                }
            }
            if (ehrMedProfile.getMedDiscontFlag() != null) {
//                dto.setMedDiscontDtm(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(ehrMedProfile.getMedDiscontDtm()));
//                dto.setMedDiscontFlag(ehrMedProfile.getMedDiscontFlag().toString());
//                dto.setMedDiscontReasonCode(ehrMedProfile.getMedDiscontReasonCode());
//                dto.setMedDiscontInfo(ServerConstant.MED_DISC_INFO_PREFIX + ehrMedProfile.getMedDiscontInformation());
//                dto.setMedDiscontUserId(ehrMedProfile.getMedDiscontUserId());
//                dto.setMedDiscontUser(ehrMedProfile.getMedDiscontUser());
//                dto.setMedDiscontHosp(ehrMedProfile.getMedDiscontHosp());
            }
        }

        dto.setTradeName(profile.getTradename());
        dto.setDrugNameType(String.valueOf(profile.getNameType()));

        dto.setDosage(profile.getDosage());
        if(dto.getDosage() != null && dto.getDosage().compareTo(BigDecimal.ZERO) > 0) {
            dto.setDosageUnit(profile.getModu());
        }
        if(dto.getDosageUnit() != null) {
            MoePrescribeUnitTermIdMapPo prescribeUnit = DtoUtil.PRESCRIBE_UNIT_TERM_ID_MAP.get(dto.getDosageUnit());
            if(prescribeUnit != null) {
                dto.setDosageUnitId(prescribeUnit.getEhrTermId());
                dto.setDosageUnitDesc(prescribeUnit.getEhrPrescribeUnitDesc());
            }
        }

        dto.setFreqCode(profile.getFreqCode());
        dto.setFreqText(profile.getFreqText());
        if (profile.getFreq1()!=null) {
            dto.setFreqValue(profile.getFreq1());
        }
        MoeFreqTermIdMapPo freq = null;
        if (dto.getFreqCode() != null) {
            String freq1 = (profile.getFreq1() != null && profile.getFreq1() > 0) ? profile.getFreq1().toString() : "";
            freq = DtoUtil.FREQ_TERM_ID_MAP.get(dto.getFreqCode() + " " + freq1);
            if (freq != null) {
                dto.setFreqId(freq.getEhrTermId());
                dto.setFreqDesc(freq.getEhrFreqDesc());
            }
        }

        if (freq != null && "Y".equalsIgnoreCase(freq.getMapToSupplFreq())) {
            dto.setSupplFreqCode(freq.getSupplFreqDescEng());
            dto.setSupplFreqText(freq.getSupplFreqDescEng());
            dto.setSupplFreqDesc(freq.getEhrSupplFreqDesc());
            dto.setSupplFreqId(freq.getEhrSupplFreqTermId());
        } else {
            dto.setSupplFreqCode(profile.getSupFreqCode());
            dto.setSupplFreqText(profile.getSupFreqText());
            if (profile.getSupFreq1() != null) {
                dto.setSupplFreqValue1(profile.getSupFreq1());
            }
            if (profile.getSupFreq2() != null) {
                dto.setSupplFreqValue2(profile.getSupFreq2());
            }
            if (dto.getSupplFreqCode() != null) {
                String supplFreqText = "";
                if ("on odd / even days".equals(profile.getSupFreqCode())) {
                    supplFreqText = profile.getSupFreqText() != null ? profile.getSupFreqText() : "";
                }
                String freq1 = profile.getSupFreq1() != null ? profile.getSupFreq1().toString() : "";
                String freq2 = profile.getSupFreq2() != null ? profile.getSupFreq2().toString() : "";
                String dow = profile.getDayOfWeek() != null ? profile.getDayOfWeek() : "";
                MoeSupplFreqTermIdMapPo supplFreq = DtoUtil.SUPPL_FREQ_TERM_ID_MAP.get(dto.getSupplFreqCode() + " " + supplFreqText + " " + freq1 + " " + freq2 + " " + dow);
                if (supplFreq != null) {
                    dto.setSupplFreqId(supplFreq.getEhrTermId());
                    dto.setSupplFreqDesc(supplFreq.getEhrSupplFreqDesc());
                }
            }
        }

        if (profile.getPrn() != null){
            dto.setPrn(String.valueOf(profile.getPrn()));
        }
        dto.setForm(profile.getFormDesc());
        dto.setRoute(profile.getRouteDesc());
        if(dto.getRoute() != null) {
            MoeRouteTermIdMapPo route = DtoUtil.ROUTE_TERM_ID_MAP.get(dto.getRoute());
            if(route != null) {
                dto.setRouteId(route.getEhrTermId());
                dto.setRouteDesc(route.getEhrRouteDesc());
            }
        }
        if (profile.getDuration()!=null){
            dto.setDuration(profile.getDuration());
        }
        dto.setDurationUnit(profile.getDurationUnit()==null?null:profile.getDurationUnit().toString());
        if(dto.getDurationUnit() != null) {
            MoeDurationTermIdMapPo duration = DtoUtil.DURATION_TERM_ID_MAP.get(dto.getDurationUnit());
            if(duration != null) {
                dto.setDurationUnitId(duration.getEhrTermId());
                dto.setDurationUnitDesc(duration.getEhrDurationUnitDesc());
            }
        }
        if (profile.getMoQty()!=null && profile.getMoQtyUnit() != null) {
            dto.setTotalQuantity(profile.getMoQty());
            dto.setTotalQuantityUnit(profile.getMoQtyUnit());
            MoeBaseUnitTermIdMapPo baseUnit = DtoUtil.BASE_UNIT_TERM_ID_MAP.get(dto.getTotalQuantityUnit());
            if(baseUnit != null) {
                dto.setTotalQuantityUnitId(baseUnit.getEhrTermId());
                dto.setTotalQuantityUnitDesc(baseUnit.getEhrBaseUnitDesc());
            }
        }

        dto.setStrength(profile.getStrength());
        dto.setActionStatus(String.valueOf(profile.getActionStatus()));

        for (MoeActionStatusDto moeActionStatusDto : DtoUtil.ACTIONS_STATUS_LIST) {
            if (moeActionStatusDto.getActionStatusType().equalsIgnoreCase(profile.getActionStatus())){
                dto.setActionStatusDesc(moeActionStatusDto.getActionStatus());
                break;
            }
        }

        dto.setSite(profile.getSiteDesc());
        if(dto.getSite() != null) {
            MoeSiteTermIdMapPo site = DtoUtil.SITE_TERM_ID_MAP.get(dto.getSite());
            if(site != null) {
                dto.setSiteId(site.getEhrTermId());
                dto.setSiteDesc(site.getEhrSiteDesc());
            }
        }
        dto.setStartDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(profile.getStartDate()));
        dto.setEndDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(profile.getEndDate()));
        dto.setSpecialInstruction(profile.getSpecInstruct());

        return dto;
    }

    public static List<MedMultDoseDto> convertMoeMedMultDoseDtos(Set<MoeMedMultDoseLogPo> doses, MoeMedProfileLogPo profile) {
        List<MedMultDoseDto> dtos = new ArrayList<MedMultDoseDto>(0);
        if(doses != null) {
            String key = "";
            if("M".equalsIgnoreCase(profile.getMoeEhrMedProfileLog().getOrderLineType()) ||
                    ("R".equalsIgnoreCase(profile.getMoeEhrMedProfileLog().getOrderLineType()) && "on odd / even days".equals(profile.getSupFreqCode())) ) {
                key = "M";
            } else if("S".equalsIgnoreCase(profile.getMoeEhrMedProfileLog().getOrderLineType())) {
                key = "S";
            }
            MoeConjunctionTermIdMapPo conjunction = DtoUtil.CONJUNCTION_TERM_ID_MAP.get(key);
            MedMultDoseDto lastline = null;
            for(MoeMedMultDoseLogPo dose : doses) {
                MedMultDoseDto dto = convertMoeMedMultDoseDto(dose, profile);
                if(conjunction != null) {
                    if(lastline != null && dto.getStepNo() > lastline.getStepNo()) {
                        lastline.setDoseInstrConjtnCode(conjunction.getEhrTermId());
                        lastline.setDoseInstrConjtnDesc(conjunction.getEhrConjunctionDesc());
                        lastline.setDoseInstrConjtnLocalDesc(conjunction.getEhrConjunctionDesc());
                        lastline = dto;
                    } else {
                        lastline = dto;
                    }
                }
                dtos.add(dto);
            }
        }
        return dtos;
    }

    public static MedMultDoseDto convertMoeMedMultDoseDto(MoeMedMultDoseLogPo dose, MoeMedProfileLogPo profile) {
        MedMultDoseDto dto = new MedMultDoseDto();
        dto.setStepNo(dose.getStepNo());
        dto.setMultDoseNo(dose.getMultDoseNo());
        if (dose.getPrn()!= null){
            dto.setPrn(String.valueOf(dose.getPrn()));
        }
        dto.setDosage(dose.getDosage());
        dto.setDosageUnit(profile.getModu());
        if(dto.getDosageUnit() != null) {
            MoePrescribeUnitTermIdMapPo prescribeUnit = DtoUtil.PRESCRIBE_UNIT_TERM_ID_MAP.get(dto.getDosageUnit());
            if(prescribeUnit != null) {
                dto.setDosageUnitId(prescribeUnit.getEhrTermId());
                dto.setDosageUnitDesc(prescribeUnit.getEhrPrescribeUnitDesc());
            }
        }
        dto.setFreqCode(dose.getFreqCode());
        dto.setFreqText(dose.getFreqText());
        dto.setFreqValue(dose.getFreq1());
        MoeFreqTermIdMapPo freq = null;
        if(dto.getFreqCode() != null) {
            String freq1 = (dto.getFreqValue() != null && dto.getFreqValue() > 0) ? dto.getFreqValue().toString():"";
            freq = DtoUtil.FREQ_TERM_ID_MAP.get(dto.getFreqCode()+" "+freq1);
            if(freq != null) {
                dto.setFreqId(freq.getEhrTermId());
                dto.setFreqDesc(freq.getEhrFreqDesc());
            }
        }

        if (freq != null && "Y".equalsIgnoreCase(freq.getMapToSupplFreq())) {
            dto.setSupplFreqCode(freq.getSupplFreqDescEng());
            dto.setSupplFreqText(freq.getSupplFreqDescEng());
            dto.setSupplFreqDesc(freq.getEhrSupplFreqDesc());
            dto.setSupplFreqId(freq.getEhrSupplFreqTermId());
        } else {
            dto.setSupplFreqCode(dose.getSupFreqCode());
            dto.setSupplFreqText(dose.getSupFreqText());
            dto.setSupplFreqValue1(dose.getSupFreq1());
            dto.setSupplFreqValue2(dose.getSupFreq2());
            if (dto.getSupplFreqCode() != null) {
                String supplFreqText = "";
                if ("on odd / even days".equals(dose.getSupFreqCode())) {
                    supplFreqText = dose.getSupFreqText() != null ? dose.getSupFreqText() : "";
                }
                String freq1 = dose.getSupFreq1() != null ? dose.getSupFreq1().toString() : "";
                String freq2 = dose.getSupFreq2() != null ? dose.getSupFreq2().toString() : "";
                String dow = dose.getDayOfWeek() != null ? dose.getDayOfWeek() : "";
                MoeSupplFreqTermIdMapPo supplFreq = DtoUtil.SUPPL_FREQ_TERM_ID_MAP.get(dto.getSupplFreqCode() + " " + supplFreqText + " " + freq1 + " " + freq2 + " " + dow);
                if (supplFreq != null) {
                    dto.setSupplFreqId(supplFreq.getEhrTermId());
                    dto.setSupplFreqDesc(supplFreq.getEhrSupplFreqDesc());
                }
            }
        }
        dto.setDuration(dose.getDuration());
        if (dose.getDurationUnit() != null){
            dto.setDurationUnit(String.valueOf(dose.getDurationUnit()));
        }
        if(dto.getDurationUnit() != null) {
            MoeDurationTermIdMapPo duration = DtoUtil.DURATION_TERM_ID_MAP.get(dto.getDurationUnit());
            if(duration != null) {
                dto.setDurationUnitId(duration.getEhrTermId());
                dto.setDurationUnitDesc(duration.getEhrDurationUnitDesc());
            }
        }
        dto.setStartDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(dose.getStartDate()));
        dto.setEndDate(XmlGregorianCalendarConversionUtil.asXMLGregorianCalendar(dose.getEndDate()));
        if (profile.getMoQty()!=null && dto.getTotalQuantityUnit() != null) {
            dto.setTotalQuantity(dose.getMoQty());
            dto.setTotalQuantityUnit(dose.getMoQtyUnit());
            MoeBaseUnitTermIdMapPo baseUnit = DtoUtil.BASE_UNIT_TERM_ID_MAP.get(dto.getTotalQuantityUnit());
            if(baseUnit != null) {
                dto.setTotalQuantityUnitId(baseUnit.getEhrTermId());
                dto.setTotalQuantityUnitDesc(baseUnit.getEhrBaseUnitDesc());
            }
        }
        return dto;
    }
}
