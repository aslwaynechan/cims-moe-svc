/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  StringUtil.java
 *
 * PURPOSE         :  Defines a set of methods that perform common, often re-used functions
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.util;

import com.lowagie.text.Chunk;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.FontSelector;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.WordUtils;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

public class StringUtil {

    private static final String DRUG_OPEN_BRACKET = " (";
    private static final String DRUG_CLOSE_BRACKET = ")";

    public static String convertStringsToLikeString(String input) {

        StringBuilder result = new StringBuilder();
        result.append("%");
        if (input == null) return null;
        String[] words = StringUtils.split(input);
        for (String s : words) {
            result.append(s);
            result.append("%");

        }
        return result.toString();
    }

    public static boolean startsWithFirstPattern(String input, String target) {
        String[] words = StringUtils.split(input);

        return target.startsWith(words[0]);
    }

    public static boolean startsWithAndContainsAllPattern(String input, String target) {
        String[] words = StringUtils.split(input);

        if (!target.startsWith(words[0])) return false;

        for (int i = 1; i < words.length; i++) {
            String regEx = buildRegularExpressionForLikePattern(words[i]);
            if (!Pattern.matches(regEx, target)) {
                return false;
            }
        }
        return true;
    }

    public static boolean containsFirstPattern(String input, String target) {
        String[] words = StringUtils.split(input);

        return target.contains(words[0]);
    }

    public static boolean containsAllPattern(String input, String target) {
        String[] words = StringUtils.split(input);

        for (int i = 0; i < words.length; i++) {
            String regEx = buildRegularExpressionForLikePattern(words[i]);
            if (!Pattern.matches(regEx, target)) {
                return false;
            }
        }
        return true;
    }

    public static String buildRegularExpressionForLikePattern(String input) {
        StringBuilder sb = new StringBuilder();
        String[] words = StringUtils.split(input);

        sb.append(".*");
        for (int i = 0; i < words.length; i++) {
            sb.append(escapeRegularExpressionLiteral(words[i]));
            sb.append(".*");
        }
        //sb.append("\\z");
        return sb.toString();
    }

    public static String escapeRegularExpressionLiteral(String s) {
        // According to the documentation in the Pattern class:
        //
        // The backslash character ('\') serves to introduce escaped constructs,
        // as defined in the table above, as well as to quote characters that
        // otherwise would be interpreted as unescaped constructs. Thus the
        // expression \\ matches a single backslash and \{ matches a left brace.
        //
        // It is an error to use a backslash prior to any alphabetic character
        // that does not denote an escaped construct; these are reserved for future
        // extensions to the regular-expression language. A backslash may be used
        // prior to a non-alphabetic character regardless of whether that character
        // is part of an unescaped construct.
        //
        // As a result, escape everything except [0-9a-zA-Z]

        int length = s.length();
        int newLength = length;
        // first check for characters that might
        // be dangerous and calculate a length
        // of the string that has escapes.
        for (int i = 0; i < length; i++) {
            char c = s.charAt(i);
            if (!((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))) {
                newLength += 1;
            }
        }
        if (length == newLength) {
            // nothing to escape in the string
            return s;
        }
        StringBuilder sb = new StringBuilder(newLength);
        for (int i = 0; i < length; i++) {
            char c = s.charAt(i);
            if (!((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))) {
                sb.append('\\');
            }
            sb.append(c);
        }
        return sb.toString();
    }

    public static String capitalize(String str) {
        return WordUtils.capitalize(StringUtils.lowerCase(str));
    }

    public static String upperInBetween(String str, String open, String close) {
        int openIndex = StringUtils.indexOf(str, open);
        int closeIndex = StringUtils.lastIndexOf(str, close);
        if (openIndex > -1 && closeIndex > -1 && closeIndex > openIndex) {
            return StringUtils.substring(str, 0, openIndex)
                    + StringUtils.upperCase(StringUtils.mid(str, openIndex, closeIndex - openIndex))
                    + StringUtils.substring(str, closeIndex);
        }
        return str;
    }

    public static String strip(String str) {
        return StringUtils.strip(str);
    }

    public static String stripToEmpty(String str) {
        return StringUtils.stripToEmpty(str);
    }

    public static String formatDrug(String genericName, String tradeName) {
        if (genericName == null || tradeName == null) return genericName;
        return StringUtils.join(new String[]{genericName, DRUG_OPEN_BRACKET, tradeName, DRUG_CLOSE_BRACKET});
    }

    public static String getTradeNameByDisplayName(String displayName) {
        int openIndex = StringUtils.indexOf(displayName, DRUG_OPEN_BRACKET);
        int closeIndex = StringUtils.lastIndexOf(displayName, DRUG_CLOSE_BRACKET);
        if (openIndex > -1 && closeIndex > -1 && closeIndex > openIndex) {
            return StringUtils.substring(displayName, 0, openIndex);
        }
        return null;
    }

    public static String getGenericNameByDisplayName(String displayName) {
        int openIndex = StringUtils.indexOf(displayName, DRUG_OPEN_BRACKET);
        int closeIndex = StringUtils.lastIndexOf(displayName, DRUG_CLOSE_BRACKET);
        if (openIndex > -1 && closeIndex > -1 && closeIndex > openIndex) {
            return StringUtils.mid(displayName, openIndex + DRUG_OPEN_BRACKET.length(), closeIndex - (openIndex + DRUG_OPEN_BRACKET.length()));
        }
        return null;
    }

    public static String formatStringWithComma(List<String> stringList) {

        StringBuilder record = new StringBuilder();
        Collections.sort(stringList, String.CASE_INSENSITIVE_ORDER);
        for (int j = 0; j < stringList.size(); j++) {

            record.append(stringList.get(j));
            if (j != stringList.size() - 1)
                record.append(", ");

        }
        return record.toString();

    }

    public static String formatDate(Date dt, String dateFormat) {
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        return format.format(dt);
    }

    public static int nthOccurrence(String str, String c, int n) {
        int pos = str.indexOf(c, 0);
        int n1 = n;
        while (n1-- > 0 && pos != -1)
            pos = str.indexOf(c, pos + 1);
        return pos;
    }

    public static String trim(String input) {
        if (input == null) {
            return null;
        } else {
            return input.trim();
        }
    }

    public static boolean containsChineseCharacter(String str) {
        try {
            char[] cArray = str.toCharArray();
            String cString;
            byte[] cByteArray;

            for (char c : cArray) {
                cString = Character.toString(c);
                cByteArray = cString.getBytes("UTF-8");
                if (cByteArray.length > 1) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isAscii(String s) {

        boolean ret = true;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) >= 128) {
                ret = false;
                break;
            }
        }
        return ret;

    }


    public static String uuid() {
        return UUID.randomUUID().toString().replaceAll("-", "").substring(0, 32);
    }

    public static Long nullToZero(Long i) {
        if (i == null) return 0L;
        return i;
    }

    public static String nullToEmpty(String str) {
        return str != null ? str : "";
    }

    public static String toHtmlTag(String input){
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("NotoSansCJKTC", "NotoSansCJKTC");

        StringUtil checker = new StringUtil();
        StringBuffer output = new StringBuffer();
        //FontFactory.registerDirectories();
        FontFactory.register("fonts/NotoSansCJKtc-Regular.ttf", "NotoSansCJKTC");
        FontSelector selector = checker.setFontSelector(9);
        Phrase phrase = selector.process(input);
        ArrayList<Chunk> chunks = phrase.getChunks();
        for(int i = 0; i<chunks.size(); i++) {
            Chunk chunk = chunks.get(i);
            Font font = chunk.getFont();
            output.append("<font face = '");
            output.append(map.get(font.getFamilyname()));
            output.append("'>");
            output.append(chunk.getContent());
            output.append("</font>");
        }
        return output.toString();
    }

    private FontSelector setFontSelector (int fontSize) {
        FontSelector selector = new FontSelector();
        String[] fontName = {"NotoSansCJKTC"};

        for (String font: fontName) {
            selector.addFont(FontFactory.getFont(font, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, fontSize, Font.NORMAL, new Color(0, 0, 0)));
        }
        return selector;
    }

}
