package hk.health.moe.util;

/**************************************************************************
 * NAME        : HttpContextUtil.java
 * VERSION     : 1.0.0
 * DATE        : 03-JUL-2019
 * DESCRIPTION : Custom util for http context information tool
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       03-JUL-2019  Initial Version
 **************************************************************************/

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

public class HttpContextUtil {

    public static HttpServletRequest getHttpServletRequest() {
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        assert sra != null;
        HttpServletRequest request = sra.getRequest();

        return request;
    }

/*    public static Map<String, Object> getCimsRequest(Map<String, Object> logMap) throws Exception {
        HttpServletRequest httpServletRequest = HttpContextUtil.getHttpServletRequest();
        Date date = new Date();
        StringBuffer sbBrowser = new StringBuffer();

        String ua = httpServletRequest.getHeader("user-agent");
        UserAgent userAgent = UserAgent.parseUserAgentString(ua);
        Browser browser = userAgent.getBrowser();
        OperatingSystem os = userAgent.getOperatingSystem();
        InetAddress inetAddress = InetAddress.getLocalHost();
        String serverHostName = inetAddress.getHostName();

        String ip = httpServletRequest.getRemoteAddr();
        String sid = httpServletRequest.getRequestedSessionId();
        String strBrowser = browser != null ?
                sbBrowser.append(browser.getName()).append(" ").append(browser.getVersion(ua)).toString() : sbBrowser.toString();
        String strOs = os != null ? os.getName() : "";

        logMap.put("date", date);
        logMap.put("ip", ip);
        logMap.put("sid", sid);
        logMap.put("browser", strBrowser);
        logMap.put("os", strOs);
        logMap.put("serverHostName", serverHostName);

        return logMap;
    }*/

    // Ricci 20190805 start -- change to token -- comment
/*    public static Object getSessionAttribute(String name) {
        Object result = WebUtils.getSessionAttribute(getHttpServletRequest(), name);

        return result;
    }*/

/*    public static void setSessionAttribute(String name, Object value) {
        WebUtils.setSessionAttribute(getHttpServletRequest(), name, value);
    }*/
    // Ricci 20190805 end --
}
