package hk.health.moe.util;

/*************************************************************************
 * NAME        : JwtUtil.java
 * VERSION     : 1.0.0
 * DATE        : 03-JUL-2019
 * DESCRIPTION : Custom util for JWT information tool
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       03-JUL-2019  Initial Version
 *************************************************************************/


import hk.health.moe.common.ResponseCode;
import hk.health.moe.common.ServerConstant;
import hk.health.moe.pojo.dto.UserDto;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import hk.health.moe.exception.SecurityException ;


@Component
public class JwtUtil {

    private PrivateKey privateKeyObject;

    private PublicKey publicKeyObject;

    @Value("${moe.constant.token.expireTime}")
    private Long jwtExpirationInMin;

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    {
        try {
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(ServerConstant.JWT_PRIVATE_KEY));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            privateKeyObject = keyFactory.generatePrivate(keySpec);
            X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(ServerConstant.JWT_PUBLIC_KEY));
            KeyFactory x509KeyFactory = KeyFactory.getInstance("RSA");
            publicKeyObject = x509KeyFactory.generatePublic(x509KeySpec);

        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }

    }

    public String generateToken(UserDto userDto) {

        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + jwtExpirationInMin * 1000 * 60);

        HashMap<String, Object> claims = new HashMap<>();
        claims.put(ServerConstant.SESS_ATTR_USER_INFO, userDto);

        String token = Jwts.builder()
                .setClaims(claims)                                      // Custom properties
                .setSubject(userDto.getLoginId())                       // Theme
                .setIssuedAt(new Date())                                // Time
                .setIssuer(ServerConstant.JWT_ISSUER)                   // Issuer
                .setExpiration(expiryDate)                              // Expiration date
                .signWith(privateKeyObject)                             // Signature algorithm and key
                .compact();

        return token;
    }

    public String getJwtFromRequest(HttpServletRequest requests) throws SecurityException {
        if (requests != null) {
            String bearerToken = requests.getHeader("Authorization");
            if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(ServerConstant.JWT_ISSUER + " ")) {
                String jwt = bearerToken.substring(4);
                return jwt;
            } else {
                //eric 20191226 start--
//                throw new SecurityException(localeMessageSourceUtil.getMessage("system.token.illegal"));
                throw new SecurityException(ResponseCode.CommonMessage.TOKEN_EXPIRE_ERROR.getResponseCode(),ResponseCode.CommonMessage.TOKEN_EXPIRE_ERROR.getResponseMessage());
                //eric 20191226 end--
            }
        }
        //eric 20191226 start--
//                throw new SecurityException(localeMessageSourceUtil.getMessage("system.token.illegal"));
        throw new SecurityException(ResponseCode.CommonMessage.TOKEN_EXPIRE_ERROR.getResponseCode(),ResponseCode.CommonMessage.TOKEN_EXPIRE_ERROR.getResponseMessage());
        //eric 20191226 end--
    }

/*    public void validateToken(String authToken, HttpServletRequest request) throws Exception {
        if (!StringUtils.isEmpty(ServerConstant.JWT_PUBLIC_KEY)) {
            X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(ServerConstant.JWT_PUBLIC_KEY));
            KeyFactory x509KeyFactory = KeyFactory.getInstance("RSA");
            publicKeyObject = x509KeyFactory.generatePublic(x509KeySpec);
        }
        Jwts.parser().setSigningKey(publicKeyObject).parseClaimsJws(authToken).getBody();
    }*/

    public Map<String, Object> getClaimsFromJWT(String jwt) throws InvalidKeySpecException, NoSuchAlgorithmException {
        String publicKey = ServerConstant.JWT_PUBLIC_KEY;
        if (!StringUtils.isEmpty(publicKey)) {
            X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKey));
            KeyFactory x509KeyFactory = KeyFactory.getInstance("RSA");
            publicKeyObject = x509KeyFactory.generatePublic(x509KeySpec);
        }
        Claims claims = Jwts.parser().setSigningKey(publicKeyObject).parseClaimsJws(jwt).getBody();
        return claims;
    }

    public Map<String, Object> refreshToken(String oldToken) throws SecurityException {

        Map<String, Object> result = new HashMap<>();
        try {
            Claims claims = (Claims) getClaimsFromJWT(oldToken);
            String refreshToken = generateTokenByClaim(claims);
            result.put("refresh", true);
            result.put("token", ServerConstant.JWT_ISSUER + " " + refreshToken);
        } catch (Exception e) {
            //eric 20191226 start--
//                throw new SecurityException(localeMessageSourceUtil.getMessage("system.token.illegal"));
            throw new SecurityException(ResponseCode.CommonMessage.TOKEN_EXPIRE_ERROR.getResponseCode(),ResponseCode.CommonMessage.TOKEN_EXPIRE_ERROR.getResponseMessage());
            //eric 20191226 end--
        }

        return result;
    }

    private String generateTokenByClaim(Claims claims) {
        Map<String, Object> mapUserDto = (Map) claims.get(ServerConstant.SESS_ATTR_USER_INFO);
        UserDto userDto = (UserDto) JSONObject.toBean(JSONObject.fromObject(mapUserDto), UserDto.class);

        return generateToken(userDto);
    }

}
