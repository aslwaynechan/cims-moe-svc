package hk.health.moe.util;

import hk.health.moe.common.EhrOrderLineTypeEnum;
import hk.health.moe.common.ModuleConstant;
import hk.health.moe.common.ResponseCode;
import hk.health.moe.exception.ParamException;
import hk.health.moe.pojo.dto.MoeFreqDto;
import hk.health.moe.pojo.dto.MoeSiteDto;
import hk.health.moe.pojo.dto.MoeSupplFreqDto;
import hk.health.moe.pojo.dto.inner.InnerMoeMedMultDoseDto;
import hk.health.moe.pojo.dto.inner.InnerMoeMedProfileDto;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/7/31
 */
public class QuantityCalculator {
    public static Double calculateQuantity(InnerMoeMedProfileDto dto, int index) throws Exception {
        if (dto.getMoeEhrMedProfile().getOrderLineType().equals(EhrOrderLineTypeEnum.ADVANCED) ||
                dto.getMoeEhrMedProfile().getOrderLineType().equals(EhrOrderLineTypeEnum.NORMAL)) {
            if (dto.getFreqCode() == null || dto.getDuration() == null || dto.getDurationUnit() == null) {
                //eric 20191224 start--
//        throw new ParamException("param.wrongInputSubmission");
                throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
                //eric 20191224 end--
            }
        } else if (dto.getMoeEhrMedProfile().getOrderLineType().equals(EhrOrderLineTypeEnum.MULTIPLE_LINE)) {
            List<InnerMoeMedMultDoseDto> dtoList = dto.getMoeMedMultDoses();
            InnerMoeMedMultDoseDto dto1 = dtoList.get(index);

            if (dto1.getFreqCode() == null || dto.getDuration() == null || dto.getDurationUnit() == null) {
                //eric 20191224 start--
//        throw new ParamException("param.wrongInputSubmission");
                throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
                //eric 20191224 end--
            }

        } else if (dto.getMoeEhrMedProfile().getOrderLineType().equals(EhrOrderLineTypeEnum.STEP_UP_DOWN)) {
            List<InnerMoeMedMultDoseDto> dtoList = dto.getMoeMedMultDoses();
            InnerMoeMedMultDoseDto dto1 = dtoList.get(index);

            if (dto1.getFreqCode() == null || dto1.getDuration() == null || dto1.getDurationUnit() == null) {
                //eric 20191224 start--
//        throw new ParamException("param.wrongInputSubmission");
                throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
                //eric 20191224 end--
            }

        } else if (dto.getMoeEhrMedProfile().getOrderLineType().equals(EhrOrderLineTypeEnum.RECURRENT)) {
            if (!dto.getSupFreqCode().equals(ModuleConstant.ON_EVEN_ODD)) {
                if (dto.getFreqCode() == null || dto.getDuration() == null || dto.getDurationUnit() == null) {
                    //eric 20191224 start--
//        throw new ParamException("param.wrongInputSubmission");
                    throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
                    //eric 20191224 end--
                }
            } else {
                //ON EVEN ODD
                List<InnerMoeMedMultDoseDto> dtoList = dto.getMoeMedMultDoses();
                InnerMoeMedMultDoseDto dto1 = dtoList.get(index);
                if (dto1.getFreqCode() == null || dto.getDuration() == null || dto.getDurationUnit() == null) {
                    //eric 20191224 start--
//        throw new ParamException("param.wrongInputSubmission");
                    throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
                    //eric 20191224 end--
                }
            }
        }
        try {
            return calculate(dto, index);
        } catch (NullPointerException ex) {
            ex.printStackTrace();
            //eric 20191224 start--
//        throw new ParamException("param.wrongInputSubmission");
            throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
            //eric 20191224 end--
        }
    }

    public static Double calculate(InnerMoeMedProfileDto dto, int index) throws Exception {
        //for site of person multiplier in [1,2]
        Long siteMultiplier = getSiteMultiplier(dto);

        if (dto.getMoeEhrMedProfile().getOrderLineType().equals(EhrOrderLineTypeEnum.ADVANCED) ||
                dto.getMoeEhrMedProfile().getOrderLineType().equals(EhrOrderLineTypeEnum.NORMAL)) {
            //use duration unit and duration to calculate how many days will last
            //durationMultiplier is total days
            Long durationMultiplier = getDurationMultiplier(String.valueOf(dto.getDurationUnit()).toLowerCase(), dto.getDuration());

            Double freqMultiplier = getFrequencyMultiplier(dto);
            if (siteMultiplier != null)
                return Math.ceil(roundDecimals(freqMultiplier * durationMultiplier * siteMultiplier));
            else
                return Math.ceil(roundDecimals(freqMultiplier * durationMultiplier));
        } else if (DtoModeChangeHelper.isMultiLine(dto)) {
            Long durationMultiplier = getDurationMultiplier(String.valueOf(dto.getDurationUnit()).toLowerCase(), dto.getDuration());
            Double freqMultiplier = 0.0;
            List<InnerMoeMedMultDoseDto> doseDTOList = dto.getMoeMedMultDoses();

            InnerMoeMedMultDoseDto moeMedMultDoseDto = doseDTOList.get(index);

            freqMultiplier = getFrequencyMultiplier(moeMedMultDoseDto);

            if (siteMultiplier != null) {
                return Math.ceil(roundDecimals(freqMultiplier * durationMultiplier * siteMultiplier));
            } else return Math.ceil(roundDecimals(freqMultiplier * durationMultiplier));

        } else if (DtoModeChangeHelper.isStepUpDown(dto)) {
            Double freqMultiplier = 0.0;

            List<InnerMoeMedMultDoseDto> doseDTOList = dto.getMoeMedMultDoses();
            InnerMoeMedMultDoseDto moeMedMultDoseDto = doseDTOList.get(index);

            Long durationMultiplier = getDurationMultiplier(String.valueOf(moeMedMultDoseDto.getDurationUnit()).toLowerCase(), moeMedMultDoseDto.getDuration());
            freqMultiplier = (getFrequencyMultiplier(moeMedMultDoseDto) * durationMultiplier);
            if (siteMultiplier != null) {
                freqMultiplier = freqMultiplier * siteMultiplier;
            }

            return Math.ceil(roundDecimals(freqMultiplier));
        } else if (DtoModeChangeHelper.isRecurrent(dto)) {
            Long durationMultiplier = getDurationMultiplier(String.valueOf(dto.getDurationUnit()).toLowerCase(), dto.getDuration());

            if (dto.getSupFreqCode().equals(ModuleConstant.ON_EVEN_ODD)) {

                List<InnerMoeMedMultDoseDto> doseDTOList = dto.getMoeMedMultDoses();
                InnerMoeMedMultDoseDto moeMedMultDoseDto = doseDTOList.get(index);

                if (siteMultiplier != null) {
                    return Math.ceil((getFrequencyMultiplier(moeMedMultDoseDto) * Math.ceil(getSpecialInvervalMultiplier(dto, moeMedMultDoseDto, null) * durationMultiplier)) * siteMultiplier);
                } else {
                    return Math.ceil((getFrequencyMultiplier(moeMedMultDoseDto) * Math.ceil(getSpecialInvervalMultiplier(dto, moeMedMultDoseDto, null) * durationMultiplier)));
                }
            } else {
                if (siteMultiplier != null) {
                    return Math.ceil(roundDecimals(getFrequencyMultiplier(dto) * getSpecialInvervalMultiplier(dto, dto, durationMultiplier) * siteMultiplier));
                } else {
                    return Math.ceil(roundDecimals(getFrequencyMultiplier(dto) * getSpecialInvervalMultiplier(dto, dto, durationMultiplier)));
                }
            }
        }
        return 0.0;
    }

    private static Long getSiteMultiplier(InnerMoeMedProfileDto dto) {

        List<MoeSiteDto> siteList = DtoUtil.SITE_LIST;
        for (MoeSiteDto site : siteList) {
            if (dto.getMoeEhrMedProfile().getSiteId() != null && site.getSiteId() == dto.getMoeEhrMedProfile().getSiteId().intValue())
                return site.getSiteMultiplier();
        }
        return null;
    }

    private static Long getDurationMultiplier(String regimenType, Long durationValue) {
//        return durationValue * EhrMoeRegistry.get().getMetaDataDTO().getDurationUnitMap().get(regimenType);
        return durationValue * DtoUtil.DURATION_UNIT_MAP.get(regimenType);
    }

    private static Double getFrequencyMultiplier(Object o) throws Exception {
//        List<MoeFreqDto> freqList = metaDTO.getFreqDtoList();
        List<MoeFreqDto> freqList = DtoUtil.FREQS_LOCAL;
        MoeFreqDto selectedFreq = null;
        if (o instanceof InnerMoeMedProfileDto) {
            for (MoeFreqDto freq : freqList) {
                if (freq.getFreqCode().equals(((InnerMoeMedProfileDto) o).getFreqCode())) {
                    selectedFreq = freq;
                    break;
                }
            }
        } else {
            for (MoeFreqDto freq : freqList) {
                if (freq.getFreqCode().equals(((InnerMoeMedMultDoseDto) o).getFreqCode())) {
                    selectedFreq = freq;
                    break;
                }
            }
        }

        if (selectedFreq == null)         //eric 20191224 start--
//        throw new ParamException("param.wrongInputSubmission");
            throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        //eric 20191224 end--

        if (ModuleConstant.USE_INPUT_VALUE_N.equals(selectedFreq.getUseInputValue())) {
            return selectedFreq.getFreqMultiplier().doubleValue() / selectedFreq.getFreqMultiplierDenominator();
        } else {
            Long inputFreqValue = 0L;
            if (o instanceof InnerMoeMedProfileDto) {
                inputFreqValue = ((InnerMoeMedProfileDto) o).getFreq1();
            } else {
                inputFreqValue = ((InnerMoeMedMultDoseDto) o).getFreq1();
            }
            if (ModuleConstant.USE_INPUT_METHOD_DEVIDE.equals(selectedFreq.getUseInputMethod())) {

                return (selectedFreq.getFreqMultiplier().doubleValue() / selectedFreq.getFreqMultiplierDenominator()) / inputFreqValue;
            } else if (ModuleConstant.USE_INPUT_METHOD_MULTIPLE.equals(selectedFreq.getUseInputMethod())) {

                return (selectedFreq.getFreqMultiplier().doubleValue() / selectedFreq.getFreqMultiplierDenominator()) * inputFreqValue;

            }
            //Simon 20190806--start add for freq_code at__a.m. at__a.m.
            if (selectedFreq.getFreqCode().contains("a.m.") || selectedFreq.getFreqCode().contains("p.m."))
                return selectedFreq.getFreqMultiplier().doubleValue();
            //Simon 20190806--end add for freq_code at__a.m. at__a.m.
        }
        //eric 20191224 start--
//                //eric 20191224 start--
//        throw new ParamException("param.wrongInputSubmission");
        throw new ParamException(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseMessage());
        //eric 20191224 end--
    }

    private static double roundDecimals(double d) {
//        return Double.parseDouble(NumberFormat.getFormat("#.#############").format(d));
        return Double.parseDouble(new DecimalFormat("#.#############").format(d));
    }

    private static double getSpecialInvervalMultiplier(InnerMoeMedProfileDto dto, Object o, Long durationMultiplier) throws Exception {
        double multiplier = 0.0;
        String regimenType = dto.getRegimen();

//        HashMap<Character, List<MoeSupplFreqDto>> regimenTypeMap = metaDTO.getRegimenTypeMap();
        HashMap<String, List<MoeSupplFreqDto>> regimenTypeMap = DtoUtil.REGIMEN_TYPE_MAP;

        List<MoeSupplFreqDto> freqList = regimenTypeMap.get(regimenType);
        MoeSupplFreqDto selectedFreq = null;

        Long subFreqId = null;
        if (o instanceof InnerMoeMedProfileDto) {
            subFreqId = ((InnerMoeMedProfileDto) o).getMoeEhrMedProfile().getSupplFreqId();
        } else {
            subFreqId = ((InnerMoeMedMultDoseDto) o).getMoeEhrMedMultDose().getSupplFreqId();
        }
        for (MoeSupplFreqDto freq : freqList) {
            if (freq.getSupplFreqId().equals(subFreqId)) {
                selectedFreq = freq;
                if (ModuleConstant.USE_INPUT_VALUE_N.equals(selectedFreq.getUseInputValue())) {
                    multiplier = selectedFreq.getSupplFreqMultiplierValue();
                } else {
                    Long inputFreqValue = 0L;
                    Long inputFreqValue2 = 0L;

                    if (o instanceof InnerMoeMedProfileDto) {
                        inputFreqValue = ((InnerMoeMedProfileDto) o).getSupFreq1();
                        inputFreqValue2 = ((InnerMoeMedProfileDto) o).getSupFreq2();
                    } else {
                        inputFreqValue = ((InnerMoeMedMultDoseDto) o).getSupFreq1();
                        inputFreqValue2 = ((InnerMoeMedMultDoseDto) o).getSupFreq2();
                    }

                    if (dto.getDayOfWeek() == null) {
                        if (ModuleConstant.USE_INPUT_METHOD_DEVIDE.equals(selectedFreq.getUseInputMethod())) {
                            multiplier = selectedFreq.getSupplFreqMultiplierValue() / inputFreqValue;
                        } else if (ModuleConstant.USE_INPUT_METHOD_MULTIPLE.equals(selectedFreq.getUseInputMethod())) {
                            multiplier = selectedFreq.getSupplFreqMultiplierValue() * inputFreqValue;
                        } else if (ModuleConstant.USE_INPUT_METHOD_SUBTRAHEND.equals(selectedFreq.getUseInputMethod())) {
                            multiplier = selectedFreq.getSupplFreqMultiplierValue() * (Math.abs(inputFreqValue - inputFreqValue2) + 1);
                        } else if (ModuleConstant.USE_INPUT_METHOD_SUM.equals(selectedFreq.getUseInputMethod())) {
                            multiplier = selectedFreq.getSupplFreqMultiplierValue() + inputFreqValue;
                        }
                    } else {
                        multiplier = Formatter.countOccurrences(dto.getDayOfWeek(), ModuleConstant.DAY_OF_WEEK_SELECTED);
                    }
                }

                if (ModuleConstant.PER_DURATION_MULTIPLIER_Y.equals(selectedFreq.getPerDurationMultiplier())) {

                    if (regimenType.equals(ModuleConstant.REGIMENT_TYPE_CYCLE)) {
                        // Simon 20190917 Comment ，bug in moe1 start--
                        //return (double) Math.ceil(roundDecimals(multiplier / dto.getMoeEhrMedProfile().getCycleMultiplier() * durationMultiplier));
                        durationMultiplier = durationMultiplier / DtoUtil.DURATION_UNIT_MAP.get(dto.getRegimen().toLowerCase());
                        multiplier = Math.ceil(roundDecimals(multiplier * durationMultiplier));
                        //Simon 20190917 end--
                    } else {
                        multiplier = Math.ceil(roundDecimals(multiplier / getDurationMultiplier(String.valueOf(dto.getDurationUnit()).toLowerCase()) * durationMultiplier));
                    }
                }
                break;
            }
        }

        return multiplier;
    }

    private static Long getDurationMultiplier(String regimenType) {
//        return  EhrMoeRegistry.get().getMetaDataDTO().getDurationUnitMap().get(regimenType);
        return DtoUtil.DURATION_UNIT_MAP.get(regimenType);
    }


}
