package hk.health.moe.security;

import org.springframework.security.core.GrantedAuthority;

/**
 * @Author Simon
 * @Date 2019/11/15
 */
public class GrantedAuthorityImpl implements GrantedAuthority {
    private String role;
    public GrantedAuthorityImpl() {
    }

    public GrantedAuthorityImpl(String role){
        this.role = role;
    }


    @Override
    public String getAuthority() {
        return role;
    }
}
