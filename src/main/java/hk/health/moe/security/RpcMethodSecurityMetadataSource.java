/*********************************************************************
* Copyright (c) eHR HK Limited 2013. 
* All Rights Reserved.
* 
*********************************************************************
* PROJECT NAME    :  eHR Adaptation
*
* MODULE NAME     :  Medication Order Entry (MOE)
*
* PROGRAM NAME    :  RpcMethodSecurityMetadataSource.java
*
* PURPOSE         :  Configuration of Spring Security
*
**********************************************************************
* VERSION         :   1.01.001
* REF NO          :  
* DATE CREATED    :   7 June 2013
* 
*********************************************************************
*
* CHANGES         :
* REF NO           DATE        DETAIL
* ------           ----        ------
*
*********************************************************************
*/
package hk.health.moe.security;

import hk.health.moe.common.ServerConstant;
import hk.health.moe.facade.AuthenticationFacade;
import org.springframework.aop.framework.ReflectiveMethodInvocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.access.method.MethodSecurityMetadataSource;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class RpcMethodSecurityMetadataSource implements MethodSecurityMetadataSource {
	
//	private static final Logger LOGGER = Logger.getLogger(RpcMethodSecurityMetadataSource.class);
	
	private static final String PERMISSION = "permission";
	
	@Autowired
	AuthenticationFacade authenticationFacade;
	
	public RpcMethodSecurityMetadataSource() {
	}
	
	@Override
	public Collection<ConfigAttribute> getAttributes(Method method, Class<?> targetClass) {
		String str = targetClass.getName() + "." + method.getName();
		return get(str);
	}

	@Override
	public Collection<ConfigAttribute> getAllConfigAttributes() {
		return null;
	}

	@Override
	public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
		ReflectiveMethodInvocation rmi = (ReflectiveMethodInvocation) object;
		String method = rmi.getMethod().getDeclaringClass().getName() + "." + rmi.getMethod().getName();
		return get(method);
	}

	@Override
	public boolean supports(Class<?> targetClass) {
		return true;
	}
	
	@SuppressWarnings("unchecked")
	protected Collection<ConfigAttribute> get (String method) {
		Collection<ConfigAttribute> configAttributes = null;
		
		try {
		
			if (method.equals("java.lang.Object.toString")) {
				return null;
			}// no rpc in moe
			/*else if (method.equals(AuthenticationService.class.getName()  + ".authenticate")
					|| method.equals(MoeService.class.getName() + ".accessRequest")) {
				if (SecurityContextHolder.getContext() != null) {
					if (SecurityContextHolder.getContext().getAuthentication() instanceof UsernamePasswordAuthenticationToken) {
						// do nothing
					} else {
						// authentication
						configAttributes = new ArrayList<ConfigAttribute>();
						configAttributes.add(new SecurityConfig("ROLE_ANONYMOUS"));
					}
				} else {
					// authentication
					configAttributes = new ArrayList<ConfigAttribute>();
					configAttributes.add(new SecurityConfig("ROLE_ANONYMOUS"));
				}


			} */else {
				// search the access rights in session
				SecurityContext securityContext = SecurityContextHolder.getContext();
				if ( securityContext != null ) {
					AbstractAuthenticationToken authenticationToken = (AbstractAuthenticationToken) securityContext.getAuthentication();
					if ( authenticationToken != null ) {
						
						// check if user has ROLE_ALL permission, it can access all methods
						if(authenticationToken.getAuthorities() != null) {
							for (GrantedAuthority authority : authenticationToken.getAuthorities()) {
								if (ServerConstant.ROLE_ALL.equals(authority.getAuthority())) {
									configAttributes = new ArrayList<ConfigAttribute>();
									configAttributes.add(new SecurityConfig(ServerConstant.ROLE_ALL));
									return configAttributes; 
								}
							}
						}
						
						Map<String, Map<String, Collection<ConfigAttribute>>> userProfile = null;
						if(authenticationToken.getDetails() instanceof HashMap)
							userProfile = (HashMap<String, Map<String, Collection<ConfigAttribute>>>) authenticationToken.getDetails();
						if ( userProfile != null && userProfile.get(PERMISSION) == null ) {
							
							// not find in session, retrieve from database and set it in session
							Map<String, Collection<ConfigAttribute>> methodMap = authenticationFacade.getOperationMethodMap();
							userProfile.put(PERMISSION, methodMap);
							
						}
						
						if ( userProfile != null && userProfile.get(PERMISSION) != null ) {
							configAttributes = userProfile.get(PERMISSION).get(method);
						}
						
					}
				}
				
				if (configAttributes == null) {
					// if still not find, then set the role as "FORBID" as to disallow the access
					configAttributes = new ArrayList<ConfigAttribute>();
					configAttributes.add(new SecurityConfig("FORBID"));
				} 
			}
		} catch (Exception e) {
//			LOGGER.error("Authentication error: "+e.getMessage());
			configAttributes = new ArrayList<ConfigAttribute>();
			if (method.equals("AuthenticationService" +  ".authenticate") || method.equals("AuthenticationService"  + ".logout")) {
				configAttributes.add(new SecurityConfig("ROLE_ANONYMOUS"));
			} else {
				configAttributes.add(new SecurityConfig("FORBID"));
			}
		}
		
		return configAttributes;
	}
	
}
