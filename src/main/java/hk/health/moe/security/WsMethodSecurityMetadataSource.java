package hk.health.moe.security;

import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Iterator;

/**
 * @Author Simon
 * @Date 2019/11/14
 */
@Service("WsMethodSecurityMetadataSource")
public class WsMethodSecurityMetadataSource extends RpcMethodSecurityMetadataSource {

    public WsMethodSecurityMetadataSource () {
    }

    public boolean isAuthorized(String method, Class<?> targetClass) {

        String str = targetClass.getName() + "." + method;
        Collection<ConfigAttribute> config = this.get(str);
        if (config == null) {
            return false;
        }

        // search the access rights in session
        SecurityContext securityContext = SecurityContextHolder.getContext();
        if ( securityContext != null ) {
            AbstractAuthenticationToken authenticationToken = (AbstractAuthenticationToken) securityContext.getAuthentication();

            if ( authenticationToken != null && authenticationToken.getAuthorities() != null ) {

                for (GrantedAuthority authority : authenticationToken.getAuthorities()) {

                    Iterator<ConfigAttribute> itr = config.iterator();

                    while(itr.hasNext()) {
                        ConfigAttribute configAttribute = (ConfigAttribute) itr.next();

                        if (configAttribute.getAttribute().equals(authority.getAuthority())) {

                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}
