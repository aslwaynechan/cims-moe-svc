/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  SpringPasswordCallback.java
 *
 * PURPOSE         :  Business Logic of webservice
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.security;

import hk.health.moe.common.ServerConstant;
import hk.health.moe.common.WebServiceConstant;
import hk.health.moe.exception.MoeWebServiceException;
import hk.health.moe.pojo.po.MoeAppPo;
import hk.health.moe.pojo.po.MoeOperationPo;
import hk.health.moe.repository.MoeAppRepository;
import hk.health.moe.util.SystemSettingUtil;
import org.apache.ws.security.WSPasswordCallback;
import org.apache.ws.security.WSSecurityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.ws.soap.security.wss4j.callback.SimplePasswordValidationCallbackHandler;

import javax.security.auth.callback.UnsupportedCallbackException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import org.springframework.security.core.authority.GrantedAuthorityImpl;


public class SpringPasswordCallback extends SimplePasswordValidationCallbackHandler {

    //	private static final Logger LOGGER = Logger.getLogger(SpringPasswordCallback.class);
    @Autowired
    private MoeAppRepository appRepository;
    @Autowired
    SystemSettingUtil systemSettingUtil;

//	public SpringPasswordCallback() {
//		super();
//	}

    @Override
    public void handleUsernameToken(WSPasswordCallback callback)
            throws IOException, UnsupportedCallbackException {
        // check whether application is expired
        if (systemSettingUtil.isApplicationExpired()) {
            throw new MoeWebServiceException(WebServiceConstant.WS_APPLICATION_EXPIRED_ERR_CODE, WebServiceConstant.WS_APPLICATION_EXPIRED_ERR_MSG);
        }

        List<MoeAppPo> apps = appRepository.findByAppName(callback.getIdentifier());

        if (apps == null || apps.size() == 0) {
//        	LOGGER.error("Web service authentication fail. Password not match.");
            throw new MoeWebServiceException(ServerConstant.WS_AUTHENTICATION_ERR_CODE, ServerConstant.WS_AUTHENTICATION_ERR_NOT_EXIST_MSG);
        }
        MoeAppPo app = apps.get(0);

        String storedPassword = null;
        if (app != null) {
            storedPassword = app.getPassword();
        }
        if ((storedPassword == null) /*|| (!(storedPassword.equals(givenPassword)))*/) {
            throw new MoeWebServiceException(ServerConstant.WS_AUTHENTICATION_ERR_CODE, ServerConstant.WS_AUTHENTICATION_ERR_MSG);
        }
        setSecurityContext(callback, app);
    }

    public void setSecurityContext(WSPasswordCallback callback, MoeAppPo app) {

        Collection<MoeOperationPo> operations = app.getMoeOperations();
        List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
        for (MoeOperationPo operation : operations) {
            GrantedAuthority authority = new GrantedAuthorityImpl(operation.getOperationName());
            grantedAuthorities.add(authority);
        }

        callback.setPassword(app.getPassword());
        Authentication authentication = new UsernamePasswordAuthenticationToken(
                new org.springframework.security.core.userdetails.User(
                        callback.getIdentifier(), callback.getIdentifier(), true, true, true, true, grantedAuthorities)
                , "", grantedAuthorities);
        SecurityContext securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(authentication);

        // set user profile into user session
        SecurityContextHolder.setContext(securityContext);
        Map<String, Object> appProfile = new HashMap<String, Object>();
        // current access clinic
        AbstractAuthenticationToken authenticationToken = (AbstractAuthenticationToken) securityContext.getAuthentication();
        authenticationToken.setDetails(appProfile);

    }
}
