package hk.health.moe.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.time.Duration;

@Configuration
public class RedisConfig {

    // default for redis start --
    @Value("${redis.default.database}")
    private int defaultDatabase;
    @Value("${redis.default.host}")
    private String defaultHost;
    @Value("${redis.default.password}")
    private String defaultPassword;
    @Value("${redis.default.port}")
    private int defaultPort;
    @Value("${redis.default.timeout}")
    private int defaultTimeout;
    @Value("${redis.default.pool.max-idle}")
    private int defaultMaxIdle;
    @Value("${redis.default.pool.min-idle}")
    private int defaultMinIdle;
    @Value("${redis.default.pool.max-active}")
    private int defaultMaxActive;
    @Value("${redis.default.pool.max-wait}")
    private long defaultMaxWait;

    // order for redis start --
    @Value("${redis.order.database}")
    private int orderDatabase;
    @Value("${redis.order.host}")
    private String orderHost;
    @Value("${redis.order.password}")
    private String orderPassword;
    @Value("${redis.order.port}")
    private int orderPort;
    @Value("${redis.order.timeout}")
    private int orderTimeout;
    @Value("${redis.order.pool.max-idle}")
    private int orderMaxIdle;
    @Value("${redis.order.pool.min-idle}")
    private int orderMinIdle;
    @Value("${redis.order.pool.max-active}")
    private int orderMaxActive;
    @Value("${redis.order.pool.max-wait}")
    private long orderMaxWait;

    // fav for redis start --
    @Value("${redis.fav.database}")
    private int favDatabase;
    @Value("${redis.fav.host}")
    private String favHost;
    @Value("${redis.fav.password}")
    private String favPassword;
    @Value("${redis.fav.port}")
    private int favPort;
    @Value("${redis.fav.timeout}")
    private int favTimeout;
    @Value("${redis.fav.pool.max-idle}")
    private int favMaxIdle;
    @Value("${redis.fav.pool.min-idle}")
    private int favMinIdle;
    @Value("${redis.fav.pool.max-active}")
    private int favMaxActive;
    @Value("${redis.fav.pool.max-wait}")
    private long favMaxWait;

    // token for redis start --
    @Value("${redis.token.database}")
    private int tokenDatabase;
    @Value("${redis.token.host}")
    private String tokenHost;
    @Value("${redis.token.password}")
    private String tokenPassword;
    @Value("${redis.token.port}")
    private int tokenPort;
    @Value("${redis.token.timeout}")
    private int tokenTimeout;
    @Value("${redis.token.pool.max-idle}")
    private int tokenMaxIdle;
    @Value("${redis.token.pool.min-idle}")
    private int tokenMinIdle;
    @Value("${redis.token.pool.max-active}")
    private int tokenMaxActive;
    @Value("${redis.token.pool.max-wait}")
    private long tokenMaxWait;

    //Chris For DrugSearch from Redis -Start
    //test for redis start --
    //Redis DB: 15
    @Value("${redis.test.database}")
    private int testDatabase;
    @Value("${redis.test.host}")
    private String testHost;
    @Value("${redis.test.password}")
    private String testPassword;
    @Value("${redis.test.port}")
    private int testPort;
    @Value("${redis.test.timeout}")
    private int testTimeout;
    @Value("${redis.test.pool.max-idle}")
    private int testMaxIdle;
    @Value("${redis.test.pool.min-idle}")
    private int testMinIdle;
    @Value("${redis.test.pool.max-active}")
    private int testMaxActive;
    @Value("${redis.test.pool.max-wait}")
    private long testMaxWait;
    //test for redis end --

    //Drug Search by Redis start --
    //Redis DB: 14
    @Value("${redis.drugSearch.database}")
    private int drugSearchDatabase;
    @Value("${redis.drugSearch.host}")
    private String drugSearchHost;
    @Value("${redis.drugSearch.password}")
    private String drugSearchPassword;
    @Value("${redis.drugSearch.port}")
    private int drugSearchPort;
    @Value("${redis.drugSearch.timeout}")
    private int drugSearchTimeout;
    @Value("${redis.drugSearch.pool.max-idle}")
    private int drugSearchMaxIdle;
    @Value("${redis.drugSearch.pool.min-idle}")
    private int drugSearchMinIdle;
    @Value("${redis.drugSearch.pool.max-active}")
    private int drugSearchMaxActive;
    @Value("${redis.drugSearch.pool.max-wait}")
    private long drugSearchMaxWait;
    //Drug Search by Redis end --

    //Drug Suggestion by Redis start --
    //Redis DB: 13
    @Value("${redis.drugSuggestion.database}")
    private int drugSuggestionDatabase;
    @Value("${redis.drugSuggestion.host}")
    private String drugSuggestionHost;
    @Value("${redis.drugSuggestion.password}")
    private String drugSuggestionPassword;
    @Value("${redis.drugSuggestion.port}")
    private int drugSuggestionPort;
    @Value("${redis.drugSuggestion.timeout}")
    private int drugSuggestionTimeout;
    @Value("${redis.drugSuggestion.pool.max-idle}")
    private int drugSuggestionMaxIdle;
    @Value("${redis.drugSuggestion.pool.min-idle}")
    private int drugSuggestionMinIdle;
    @Value("${redis.drugSuggestion.pool.max-active}")
    private int drugSuggestionMaxActive;
    @Value("${redis.drugSuggestion.pool.max-wait}")
    private long drugSuggestionMaxWait;
    //Drug Search by Redis end --
    //Chris For DrugSearch from Redis -End

    @Bean
    public RedisTemplate redisTemplate() {
        return getRedisTemplate(
                defaultDatabase, defaultHost, defaultPassword,
                defaultPort, defaultMaxIdle, defaultMaxActive,
                defaultMinIdle, defaultTimeout
        );
    }

    @Bean
    public RedisTemplate orderRedisTemplate() {
        return getRedisTemplate(
                orderDatabase, orderHost, orderPassword,
                orderPort, orderMaxIdle, orderMaxActive,
                orderMinIdle, orderTimeout);
    }

    @Bean
    public RedisTemplate favRedisTemplate() {
        return getRedisTemplate(
                favDatabase, favHost, favPassword,
                favPort, favMaxIdle, favMaxActive,
                favMinIdle, favTimeout);
    }

    @Bean
    public RedisTemplate tokenRedisTemplate() {
        return getRedisTemplate(
                tokenDatabase, tokenHost, tokenPassword,
                tokenPort, tokenMaxIdle, tokenMaxActive,
                tokenMinIdle, tokenTimeout);
    }

    //Test
    //Redis DB: 15
    @Bean
    public RedisTemplate drugSearchTemplate() {
        return getRedisTemplate(
                testDatabase, testHost, testPassword,
                testPort, testMaxIdle, testMaxActive,
                testMinIdle, testTimeout);
    }

    //Test
    //Redis DB: 15
    @Bean
    public StringRedisTemplate stringRedisTemplate() {
        return getStringRedisTemplate(
                testDatabase, testHost, testPassword,
                testPort, testMaxIdle, testMaxActive,
                testMinIdle, testTimeout);
    }

    //Test operate all drugs  RedisTemplate
    //Redis DB: 14
    @Bean
    public RedisTemplate allDrugsRedisTemplate() {
        return getRedisTemplate(
                drugSearchDatabase, drugSearchHost, drugSearchPassword,
                drugSearchPort, drugSearchMaxIdle, drugSearchMaxActive,
                drugSearchMinIdle, drugSearchTimeout
        );
    }

    //Test operate all drugs (Fuzzy Query)  StringRedisTemplate
    //Redis DB: 14
    @Bean
    public StringRedisTemplate allDrugsStringRedisTemplate() {
        return getStringRedisTemplate(
                drugSearchDatabase, drugSearchHost, drugSearchPassword,
                drugSearchPort, drugSearchMaxIdle, drugSearchMaxActive,
                drugSearchMinIdle, drugSearchTimeout
        );
    }

    //Test Drug Suggestion version 3
    //Redis DB: 13
    @Bean
    public RedisTemplate drugMapRedisTemplate() {
        return getRedisTemplate(
                drugSuggestionDatabase, drugSuggestionHost, drugSuggestionPassword,
                drugSuggestionPort, drugSuggestionMaxIdle, drugSuggestionMaxActive,
                drugSuggestionMinIdle, drugSuggestionTimeout
        );
    }

    private RedisTemplate getRedisTemplate(int defaultDatabase, String defaultHost, String defaultPassword, int defaultPort, int defaultMaxIdle, int defaultMaxActive, int defaultMinIdle, int defaultTimeout) {
        RedisStandaloneConfiguration configuration = createRedisStandaloneConfiguration(defaultDatabase, defaultHost, defaultPassword, defaultPort);
        GenericObjectPoolConfig config = createGenericObjectPoolConfig(defaultMaxIdle, defaultMaxActive, defaultMinIdle, defaultTimeout);

        LettucePoolingClientConfiguration.LettucePoolingClientConfigurationBuilder builder = LettucePoolingClientConfiguration.builder();
        builder.poolConfig(config);
        builder.commandTimeout(Duration.ofSeconds(defaultTimeout));
        LettuceConnectionFactory connectionFactory = new LettuceConnectionFactory(configuration, builder.build());
        connectionFactory.afterPropertiesSet();

        return createRedisTemplate(connectionFactory);
    }

    private GenericObjectPoolConfig createGenericObjectPoolConfig(int maxIdle, int maxActive, int minIdle, long timeout) {
        GenericObjectPoolConfig genericObjectPoolConfig = new GenericObjectPoolConfig();
        genericObjectPoolConfig.setMaxIdle(maxIdle);
        genericObjectPoolConfig.setMaxTotal(maxActive);
        genericObjectPoolConfig.setMinIdle(minIdle);
        genericObjectPoolConfig.setMaxWaitMillis(timeout);

        return genericObjectPoolConfig;
    }

    private RedisStandaloneConfiguration createRedisStandaloneConfiguration(int database, String host, String password, int port) {
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
        redisStandaloneConfiguration.setDatabase(database);
        redisStandaloneConfiguration.setHostName(host);
        redisStandaloneConfiguration.setPassword(password);
        redisStandaloneConfiguration.setPort(port);

        return redisStandaloneConfiguration;
    }

    private RedisTemplate createRedisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);

        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<>(Object.class);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(objectMapper);

        redisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }


    //Chris StringRedisTemplate  -Start
    private StringRedisTemplate getStringRedisTemplate(int defaultDatabase, String defaultHost, String defaultPassword, int defaultPort, int defaultMaxIdle, int defaultMaxActive, int defaultMinIdle, int defaultTimeout) {
        RedisStandaloneConfiguration configuration = createRedisStandaloneConfiguration(defaultDatabase, defaultHost, defaultPassword, defaultPort);
        GenericObjectPoolConfig config = createGenericObjectPoolConfig(defaultMaxIdle, defaultMaxActive, defaultMinIdle, defaultTimeout);

        LettucePoolingClientConfiguration.LettucePoolingClientConfigurationBuilder builder = LettucePoolingClientConfiguration.builder();
        builder.poolConfig(config);
        builder.commandTimeout(Duration.ofSeconds(defaultTimeout));
        LettuceConnectionFactory connectionFactory = new LettuceConnectionFactory(configuration, builder.build());
        connectionFactory.afterPropertiesSet();

        return createStringRedisTemplate(connectionFactory);
    }
    private StringRedisTemplate createStringRedisTemplate(RedisConnectionFactory redisConnectionFactory) {
        StringRedisTemplate stringRedisTemplate = new StringRedisTemplate();
        stringRedisTemplate.setConnectionFactory(redisConnectionFactory);

        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<>(Object.class);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(objectMapper);

        stringRedisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
        stringRedisTemplate.setKeySerializer(new StringRedisSerializer());
        stringRedisTemplate.afterPropertiesSet();
        return stringRedisTemplate;
    }
    //Chris StringRedisTemplate  -End

}
