package hk.health.moe.config;

import hk.health.moe.common.ServerConstant;
import hk.health.moe.security.SpringPasswordCallback;
import hk.health.moe.util.StringEncryptor;
import org.jasypt.encryption.pbe.config.SimplePBEConfig;
import org.jasypt.salt.StringFixedSaltGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.server.endpoint.mapping.PayloadRootAnnotationMethodEndpointMapping;
import org.springframework.ws.soap.security.wss4j.Wss4jSecurityInterceptor;
import org.springframework.ws.soap.server.endpoint.interceptor.PayloadValidatingInterceptor;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

import java.util.List;

@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {

    private final static Logger logger = LoggerFactory.getLogger(WebServiceConfig.class);

    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, "/ws/MoeWebS/*");
    }


    @Bean(name = "MoeWebS")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema moeDrugServiceSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("MoeResource");
        wsdl11Definition.setLocationUri("/ws/MoeWebS");
        wsdl11Definition.setSchema(moeDrugServiceSchema);
        wsdl11Definition.setTargetNamespace("http://ehr.gov.hk/moe/ws/beans");
        return wsdl11Definition;
    }

    @Override
    public void addInterceptors(List<EndpointInterceptor> interceptors) {
        PayloadValidatingInterceptor validatingInterceptor = new PayloadValidatingInterceptor();
        PayloadRootAnnotationMethodEndpointMapping mapping = new PayloadRootAnnotationMethodEndpointMapping();
        validatingInterceptor.setXsdSchema(moeDrugServiceSchema());
        validatingInterceptor.setValidateRequest(false);
//        validatingInterceptor.setValidateResponse(true);
        interceptors.add(validatingInterceptor);
        try {
            interceptors.add(wss4jSecurityInterceptor());
        } catch (Exception e) {
            logger.error("Exception", e);
        }
        EndpointInterceptor[] endpointInterceptors = new EndpointInterceptor[interceptors.size()];
        for (int i = 0; i < interceptors.size(); i++) {
            endpointInterceptors[i] = interceptors.get(i);
        }
        mapping.setInterceptors(endpointInterceptors);
    }

    @Bean
    public Wss4jSecurityInterceptor wss4jSecurityInterceptor() throws Exception {
        Wss4jSecurityInterceptor interceptor = new Wss4jSecurityInterceptor();
        interceptor.setValidationActions("UsernameToken Timestamp");
        interceptor.setTimestampStrict(false);
        interceptor.setValidationTimeToLive(300);
        interceptor.setSecurementActions("NoSecurity");
        interceptor.setTimestampPrecisionInMilliseconds(false);
        interceptor.setValidationCallbackHandler(springPasswordCallback());
        return interceptor;
    }


    @Bean
    public XsdSchema moeDrugServiceSchema() {
        return new SimpleXsdSchema(new ClassPathResource("moeDrugService.xsd"));
    }


    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public SimplePBEConfig simplePBEConfig() throws Exception{
        SimplePBEConfig simplePBEConfig = new SimplePBEConfig();
        simplePBEConfig.setAlgorithm(StringEncryptor.decryptByAES(ServerConstant.JASYPT_ALOGORITHM));
        simplePBEConfig.setAlgorithm(StringEncryptor.decryptByAES(ServerConstant.JASYPT_ALOGORITHM));
        simplePBEConfig.setPassword(StringEncryptor.decryptByAES(ServerConstant.JASYPT_KEY));
        StringFixedSaltGenerator generator = new StringFixedSaltGenerator(StringEncryptor.decryptByAES(ServerConstant.JASYPT_SALT));
        simplePBEConfig.setSaltGenerator(generator);
        simplePBEConfig.setPoolSize(4);

        return simplePBEConfig;


    }

    @Bean
    public SpringPasswordCallback springPasswordCallback() {
        return new SpringPasswordCallback();
    }
}
