package hk.health.moe.repository;

import hk.health.moe.pojo.dto.DrugInfoDto;
import hk.health.moe.pojo.dto.MoeDrugLocalDto;
import hk.health.moe.pojo.po.MoeDrugLocalPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MoeDrugLocalRepository extends JpaRepository<MoeDrugLocalPo, String> {

    @Query("from MoeDrugLocalPo d " +
            "join fetch d.moeDrugStrengths " +
            "left join fetch d.moeCommonDosages cd " +
            "left join fetch cd.moeFreq z " +
            "left join fetch d.moeDrugOrdPropertyLocal")
    List<MoeDrugLocalPo> getAllDistinctBasicAttributes();

    @Query("select a.localDrugId from MoeDrugOrdPropertyLocalPo a" +
            " where a.suspend = :suspend")
    List<String> getAllSuspendedDrugs(@Param(value = "suspend") String suspend);

    @Query(value = "select distinct s.hospcode, s.userSpecialty, d.tradeName, d.vtm, d.routeEng, d.formEng, d.doseFormExtraInfo, d.prescribeUnit from MoeDrugLocalPo d " +
            "join d.moeDrugBySpecialties s " +
            "where d.strengthCompulsory = 'N' " +
            "and not exists (select 1 from MoeLocationUserGroupMapPo u, MoeDrugLocalPo d2 " +
            "where d.tradeName = d2.tradeName and d.vtm = d2.vtm and d.routeEng = d2.routeEng " +
            "and d.formEng = d2.formEng and COALESCE(d.doseFormExtraInfo, '#') = COALESCE(d2.doseFormExtraInfo, '#') and d.strengthCompulsory = d2.strengthCompulsory " +
            "and u.hospcode = s.hospcode and u.userSpecialty = s.userSpecialty " +
            "and not exists (select 1 from MoeDrugBySpecialtyPo where d2.localDrugId = localDrugId and hospcode = u.hospcode and userSpecialty = u.userSpecialty)) ")
    List<Object[]> findBySpecialtyDrug();

    @Query(value = "select distinct s.hospcode, s.userSpecialty, d.tradeName, d.vtm, d.routeEng, d.formEng, d.doseFormExtraInfo, d.prescribeUnit from MoeDrugLocalPo d " +
            "join d.moeDrugBySpecialties s " +
            "where d.strengthCompulsory = 'N'" +
            "and s.hospcode = :hospcode and s.userSpecialty = :userSpecialty " +
            "and not exists (select 1 from MoeLocationUserGroupMapPo u, MoeDrugLocalPo d2 " +
            "where d.tradeName = d2.tradeName and d.vtm = d2.vtm and d.routeEng = d2.routeEng " +
            "and d.formEng = d2.formEng and COALESCE(d.doseFormExtraInfo, '#') = COALESCE(d2.doseFormExtraInfo, '#') and d.strengthCompulsory = d2.strengthCompulsory " +
            "and u.hospcode = s.hospcode and u.userSpecialty = s.userSpecialty " +
            "and not exists (select 1 from MoeDrugBySpecialtyPo where d2.localDrugId = localDrugId and hospcode = u.hospcode and userSpecialty = u.userSpecialty)) ")
    List<Object[]> findBySpecialtyDrug(@Param("hospcode") String hospcode, @Param("userSpecialty") String userSpecialty);


    //Simon 20190822 for moe_drug_server start --
    @Query(value = "select count(a.localDrugId) from MoeDrugLocalPo a where upper(a.localDrugId) = :localDrugId ")
    Integer isLocalDrugIdExist(@Param("localDrugId") String localDrugId);

    @Query("select count(a.localDrugId) from MoeDrugLocalPo a where upper(a.localDrugId) = :localDrugId and a.hkRegNo = :hkRegNo")
    Integer isLocalDrugIdHkRegNoCombinationExist(@Param("localDrugId") String localDrugId,
                                                 @Param("hkRegNo") String hkRegNo);

    @Query("select count(a.localDrugId) from MoeDrugLocalPo a, MoeDrugStrengthLocalPo b " +
            "where a.localDrugId = b.localDrugId " +
            "and a.hkRegNo = :hkRegNo and b.strength = :strength " +
            "and b.strengthLevelExtraInfo = :strengthLevelExtraInfo ")
    Integer isLocalStrengthExist(@Param("hkRegNo") String hkRegNo,
                                 @Param("strength") String strength,
                                 @Param("strengthLevelExtraInfo") String strengthLevelExtraInfo);

    @Query("select count(a.hkRegNo ) from MoeDrugLocalPo a " +
            "where a.hkRegNo = :hkRegNo  ")
    Integer getNumRecordsByHkRegNo(@Param("hkRegNo") String hkRegNo);
    //Simon 20190822 for moe_drug_server end--

    //Simon 20190823 start--
    List<MoeDrugLocalPo> findByHkRegNo(String hkRegNo);

    //Simon 20190823 end--
    @Query(value = "Select new hk.health.moe.pojo.dto.DrugInfoDto(a.localDrugId, a.hkRegNo) " +
            "From MoeDrugLocalPo a " +
            "Where COALESCE(a.tradeName, '#') = :tradeName " +
            "and COALESCE(a.vtm, '#') = :vtm " +
            "and COALESCE(a.routeId, 0) = :routeId " +
            "and COALESCE(a.formId, 0) = :formId " +
            "and COALESCE(a.doseFormExtraInfoId, 0) = :doseFormExtraInfoId ")
    List<DrugInfoDto> findSameTypeLocalDrugId(@Param("tradeName") String tradeName,
                                              @Param("vtm") String vtm,
                                              @Param("routeId") Long routeId,
                                              @Param("formId") Long formId,
                                              @Param("doseFormExtraInfoId") Long doseFormExtraInfoId);

    //Simon 20190828 start--
    @Query("select distinct d from MoeDrugLocalPo d " +
            "left join fetch d.moeDrugStrengths " +
            "left join fetch d.moeDrugOrdPropertyLocal " +
            "left join fetch d.moeCommonDosages" +
            " where d.localDrugId <> :localDrugId " +
            " and COALESCE(d.vtm, '#') = :vtm " +
            " and COALESCE(d.tradeName, '#') = :tradeName " +
            " and COALESCE(d.routeEng, '#') = :routeEng " +
            " and COALESCE(d.formEng, '#') = :formEng " +
            " and COALESCE(d.doseFormExtraInfo, '#') = :doseFormExtraInfo " +
            " and d.strengthCompulsory = 'N' " +
            " and exists (select 1 from MoeLocationUserGroupMapPo c where not exists " +
            "(select 1 from MoeDrugBySpecialtyPo b " +
            "where c.hospcode = b.hospcode " +
            "and c.userSpecialty = b.userSpecialty " +
            "and b.localDrugId = d.localDrugId" +
            ") " +
            "and c.hospcode = :hospCode ) ")
    List<MoeDrugLocalPo> listRelatedDrugWithHospitalCode(@Param("localDrugId") String localDrugId,
                                                         @Param("vtm") String vtm,
                                                         @Param("tradeName") String tradeName,
                                                         @Param("routeEng") String routeEng,
                                                         @Param("formEng") String formEng,
                                                         @Param("doseFormExtraInfo") String doseFormExtraInfo,
                                                         @Param("hospCode") String hospCode);

    @Query("select distinct d from MoeDrugLocalPo d " +
            " left join fetch d.moeDrugStrengths" +
            " left join fetch d.moeDrugOrdPropertyLocal" +
            " left join fetch d.moeCommonDosages" +
            " where d.localDrugId <> :localDrugId " +
            " and COALESCE(d.vtm, '#') = :vtm " +
            " and COALESCE(d.tradeName, '#') = :tradeName " +
            " and COALESCE(d.routeEng, '#') = :routeEng " +
            " and COALESCE(d.formEng, '#') = :formEng " +
            " and COALESCE(d.doseFormExtraInfo, '#') = :doseFormExtraInfo " +
            " and d.strengthCompulsory = 'N' ")
    List<MoeDrugLocalPo> listRelatedDrugWithoutHospitalCode(@Param("localDrugId") String localDrugId,
                                                            @Param("vtm") String vtm,
                                                            @Param("tradeName") String tradeName,
                                                            @Param("routeEng") String routeEng,
                                                            @Param("formEng") String formEng,
                                                            @Param("doseFormExtraInfo") String doseFormExtraInfo);
    //Simon 20190828 end--

    // Ricci 20190902 start --
    @Query(value = "select count(a.localDrugId) from MoeDrugLocalPo a where exists " +
            "(select 1 from MoeDrugPo where hkRegNo = :hkRegNo " +
            "and a.tradeName = tradeName " +
            "and a.vtm = vtm " +
            "and a.formEng = formEng " +
            "and COALESCE(a.doseFormExtraInfo,'') = COALESCE(doseFormExtraInfo,'') " +
            "and a.routeEng = routeEng " +
            "and strengthCompulsory = 'N' " +
            "and a.strengthCompulsory = strengthCompulsory " +
            ") and a.localDrugId <> :localDrugId ")
    int checkRelatedDrugExist(@Param("hkRegNo") String hkRegNo,
                              @Param("localDrugId") String localDrugId);
    // Ricci 20190902 end --

    //Chris 20190829 From moe_drug_server  --Start
    @Query("select d from MoeDrugLocalPo d " +
            " , MoeDrugStrengthLocalPo s" +
            " where d.localDrugId = s.localDrugId" +
            " and ((case when d.vtm like '% + %'  or d.terminologyName is null then lower(s.amp) else '' end) like :amp" +
            " or lower(d.moeDrugOrdPropertyLocal.tradeNameAlias) like :amp " +
            " or ((d.vtm not like '% + %' and d.terminologyName is not null)" +
            "     and (lower(d.vtm) like :amp" +
            "        or lower(d.tradeName) like :amp" +
            "        or (case when exists (select 1 from MoeFormRouteMapPo map where map.displayRoute = 'N'" +
            "            and map.routeId = d.routeId and map.moeForm.formId = d.formId) then '' else concat('', lower(d.routeEng)) end) like :amp" +
            "            or lower(d.formEng) like :amp" +
            "            or lower(d.doseFormExtraInfo) like :amp" +
            "            or (d.strengthCompulsory = 'Y' and (lower(s.strength) like :amp or lower(s.strengthLevelExtraInfo) like :amp))" +
            "          )" +
            "    )" +
            ") order by d.tradeName asc, d.vtm asc, s.strength asc, s.strengthLevelExtraInfo asc")
    List<MoeDrugLocalPo> findByAmp(@Param("amp") String amp);
    //Chris 20190829 From moe_drug_server  --End

    @Query(value = "select new hk.health.moe.pojo.dto.MoeDrugLocalDto(po.createUserId, po.createUser, po.createHosp," +
            "po.createRank, po.createRankDesc, po.createDtm) from MoeDrugLocalPo po where po.localDrugId = :localDrugId")
    MoeDrugLocalDto findCreateInfoByLocalDrugId(@Param("localDrugId") String localDrugId);

}
