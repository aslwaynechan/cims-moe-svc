package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeHospImagePo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeHospImageRepository extends JpaRepository<MoeHospImagePo, String> {
}
