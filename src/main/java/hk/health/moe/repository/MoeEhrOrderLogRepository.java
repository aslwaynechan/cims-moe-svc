package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeEhrOrderLogPo;
import hk.health.moe.pojo.po.MoeOrderLogPoPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeEhrOrderLogRepository extends JpaRepository<MoeEhrOrderLogPo, MoeOrderLogPoPK> {



}
