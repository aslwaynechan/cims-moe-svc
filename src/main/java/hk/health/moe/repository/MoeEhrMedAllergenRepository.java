package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeEhrMedAllergenPo;
import hk.health.moe.pojo.po.MoeEhrMedAllergenPoPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MoeEhrMedAllergenRepository extends JpaRepository<MoeEhrMedAllergenPo, MoeEhrMedAllergenPoPK> {


    @Query(value = "from MoeEhrMedAllergenPo as a where a.ordNo = :ordNo")
    List<MoeEhrMedAllergenPo> findAllByOrdNo(@Param("ordNo") Long ordNo);

}
