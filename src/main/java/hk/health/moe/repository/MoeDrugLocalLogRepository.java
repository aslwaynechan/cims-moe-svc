package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeDrugLocalLogPo;
import hk.health.moe.pojo.po.MoeDrugLocalLogPoPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeDrugLocalLogRepository extends JpaRepository<MoeDrugLocalLogPo, MoeDrugLocalLogPoPK> {
}
