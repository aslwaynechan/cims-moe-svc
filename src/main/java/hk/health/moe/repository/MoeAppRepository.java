package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeAppPo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MoeAppRepository extends JpaRepository<MoeAppPo, String> {

    List<MoeAppPo> findByAppName(String appName);

}
