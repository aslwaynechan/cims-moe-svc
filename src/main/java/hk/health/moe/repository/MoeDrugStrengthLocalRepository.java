package hk.health.moe.repository;

import hk.health.moe.pojo.dto.MoeDrugStrengthLocalDto;
import hk.health.moe.pojo.po.MoeDrugStrengthLocalPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MoeDrugStrengthLocalRepository extends JpaRepository<MoeDrugStrengthLocalPo, String> {


    @Query(value = "select b.vtm, b.form_Eng, b.dose_Form_Extra_Info, b.route_Eng, a.strength, a.strength_Level_Extra_Info, to_char(a.amp_Id), a.local_Drug_Id, b.trade_Name " +
            "from Moe_Drug_Strength_Local a " +
            "inner join moe_Drug_Local b on b.local_drug_id = a.local_drug_id " +
            "where b.strength_Compulsory = 'N' and a.strength is not null " +
            "and exists (select * from Moe_Drug_Ord_Property_Local c where c.suspend = 'N' and a.local_Drug_Id = c.local_Drug_Id)", nativeQuery = true)
    List<Object[]> getAllNonStrengthCompulsory();


    //Simon 20190823 for moe_drug_server start--
    @Query("select count(a.amp) from MoeDrugStrengthLocalPo a where a.amp = :amp")
    Integer isAmpExist(@Param("amp") String amp);

    @Query("from MoeDrugStrengthLocalPo a " +
            "Where exists (Select 1 from MoeDrugLocalPo b " +
            "Where a.localDrugId = b.localDrugId " +
            "and b.hkRegNo = :hkRegNo)")
    List<MoeDrugStrengthLocalPo> findByHkRegNo(@Param("hkRegNo") String hkRegNo);
    //Simon 20190823 for moe_drug_server start--


    @Query(value = "select new hk.health.moe.pojo.dto.MoeDrugStrengthLocalDto(po.createUserId, po.createUser, po.createHosp, " +
            "po.createRank, po.createRankDesc, po.createDtm) from MoeDrugStrengthLocalPo po where po.localDrugId = :localDrugId")
    MoeDrugStrengthLocalDto findCreateInfoByLocalDrugId(@Param("localDrugId") String localDrugId);

}
