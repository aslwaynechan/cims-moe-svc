package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeActionStatusPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MoeActionStatusRepository extends JpaRepository<MoeActionStatusPo, String> {

    MoeActionStatusPo findByActionStatusType(String statusType);

    @Query(value = "from MoeActionStatusPo order by rank asc")
    List<MoeActionStatusPo> getAllOrderByRank();

}
