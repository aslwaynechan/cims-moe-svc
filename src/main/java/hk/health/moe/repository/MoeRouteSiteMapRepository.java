package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeRouteSiteMapPo;
import hk.health.moe.pojo.po.MoeRouteSiteMapPoPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MoeRouteSiteMapRepository extends JpaRepository<MoeRouteSiteMapPo, MoeRouteSiteMapPoPK> {

    @Query(value = "select distinct m from MoeRouteSiteMapPo m " +
            "join fetch m.moeSite " +
            "join fetch m.moeRoute " +
            "order by m.routeId asc, m.rank asc")
    List<MoeRouteSiteMapPo> findAllOrderByRouteAndRank();

}
