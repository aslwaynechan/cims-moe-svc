package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeDurationUnitMultiplierPo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeDurationUnitMultiplierRepository extends JpaRepository<MoeDurationUnitMultiplierPo, String> {
}
