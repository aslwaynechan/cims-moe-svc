package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoePatientPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MoePatientRepository extends JpaRepository<MoePatientPo,String> {

    @Query(value = "from MoePatientPo m where m.patientRefKey = :patientRefKey")
    List<MoePatientPo> getPatientByRefKey(@Param("patientRefKey") String patientRefKey);

}
