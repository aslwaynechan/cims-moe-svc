package hk.health.moe.repository.Impl;

import hk.health.moe.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository(value="BaseRepository")
@SuppressWarnings("unchecked")
public class BaseRepositoryImpl<T> implements BaseRepository<T> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<T> findAll(Class<T> entityClass){

        return em.createQuery("from " + entityClass.getSimpleName()).getResultList();
    }

    @Override
    public void save(T entityClass) {
        em.persist(entityClass);
    }

}
