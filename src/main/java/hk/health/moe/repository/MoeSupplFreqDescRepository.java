package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeSupplFreqDescPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MoeSupplFreqDescRepository extends JpaRepository<MoeSupplFreqDescPo, Long> {

    @Query(value = "from MoeSupplFreqDescPo a where a.moeSupplFreq.supplFreqId = :supplFreqId order by a.rank")
    List<MoeSupplFreqDescPo> findRecordBySupplFreqId(@Param("supplFreqId") Long supplFreqId);

    @Query(value = "select a.supplFreqDescEng From MoeSupplFreqDescPo a where a.moeSupplFreq.supplFreqId = :supplFreqId " +
            "and a.supplFreqSelectionValue = :supplFreqSelectionValue")
    List<String> findDescEngBySupplFreqIdSelectionValue(@Param("supplFreqId") Long supplFreqId,
                                                  @Param("supplFreqSelectionValue") Long supplFreqSelectionValue);

}
