package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeCommonDosageTypePo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MoeCommonDosageTypeRepository extends JpaRepository<MoeCommonDosageTypePo, String> {

    @Query(value = "from MoeCommonDosageTypePo a Where a.display = :display order by a.rank asc")
    List<MoeCommonDosageTypePo> findAllByDisplay(@Param("display") String display);

}
