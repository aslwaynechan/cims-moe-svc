package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeMethodPo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author Simon
 * @Date 2019/11/14
 */
public interface MoeMethodRepository extends JpaRepository<MoeMethodPo,String> {
}
