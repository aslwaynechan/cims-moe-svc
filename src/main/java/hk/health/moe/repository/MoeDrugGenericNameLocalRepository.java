package hk.health.moe.repository;


import hk.health.moe.pojo.po.MoeDrugGenericNameLocalPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2020/01/07
 */
public interface MoeDrugGenericNameLocalRepository extends JpaRepository<MoeDrugGenericNameLocalPo, String> {

    @Query("select count(l.localDrugId) from MoeDrugGenericNameLocalPo l where l.vtmRouteFormId = :vtmRouteFormId and l.strengthCompulsory = 'N'")
    Integer isVtmRouteFormIdExist(@Param("vtmRouteFormId") Long vtmRouteFormId);

    @Query("select count(l.localDrugId) from MoeDrugGenericNameLocalPo l where l.vmpId = :vmpId and l.strengthCompulsory = 'Y'")
    Integer isVmpIdExist(@Param("vmpId") Long vmpId);

    @Query("select l from MoeDrugGenericNameLocalPo l where l.vtmRouteFormId = :vtmRouteFormId and l.strengthCompulsory = 'N'")
    List<MoeDrugGenericNameLocalPo> findByVtmRouteFormId(@Param("vtmRouteFormId") Long vtmRouteFormId);

    @Query("select l from MoeDrugGenericNameLocalPo l where l.vmpId = :vmpId and l.strengthCompulsory = 'Y'")
    List<MoeDrugGenericNameLocalPo> findByVmpId(@Param("vmpId") Long vmpId);

}
