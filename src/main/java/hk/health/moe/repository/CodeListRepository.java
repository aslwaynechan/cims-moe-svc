package hk.health.moe.repository;

/**************************************************************************
 * NAME        : CodeListRepository.java
 * VERSION     : 1.0.0
 * DATE        : 04-JUL-2019
 * DESCRIPTION : Get code list table
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       04-JUL-2019  Initial Version
 * Ricci Liao       04-JUL-2019  Support to list code-list in prescript page
 **************************************************************************/

import hk.health.moe.pojo.dto.CodeListDto;
import hk.health.moe.pojo.po.MoeFreqPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CodeListRepository  extends JpaRepository<MoeFreqPo, String> {

    // Ricci 20190706 start --
    @Query(value = "select new hk.health.moe.pojo.dto.CodeListDto(po.freqCode, po.freqEng, po.rank, po.useInputValue) from MoeFreqPo po order by po.rank")
    List<CodeListDto> listMoeFreqCode();

    @Query(value = "select new hk.health.moe.pojo.dto.CodeListDto(po.routeId, po.routeEng, po.rank) from MoeRoutePo po order by po.rank")
    List<CodeListDto> listMoeRouteCode();

    @Query(value = "select new hk.health.moe.pojo.dto.CodeListDto(po.durationUnit, po.durationUnitDesc) from MoeDurationUnitMultiplierPo po")
    List<CodeListDto> listMoeDurationUnitMultiplierCode();

    @Query(value = "select new hk.health.moe.pojo.dto.CodeListDto(po.actionStatusType, po.actionStatus, po.rank) from MoeActionStatusPo po order by po.rank")
    List<CodeListDto> listMoeActionStatusCode();

    @Query(value = "select new hk.health.moe.pojo.dto.CodeListDto(po.baseUnitId, po.baseUnit) from MoeBaseUnitPo po")
    List<CodeListDto> listMoeBaseUnitCode();
    // Ricci 20190706 end --

}
