package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeRegimenPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MoeRegimenRepository extends JpaRepository<MoeRegimenPo, String> {

    @Query(value = "from MoeRegimenPo m order by m.rank")
    List<MoeRegimenPo> getAllOrderByRank();

}
