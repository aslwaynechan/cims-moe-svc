package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeMedProfilePo;
import hk.health.moe.pojo.po.MoeMedProfilePoPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MoeMedProfileRepository extends JpaRepository<MoeMedProfilePo, MoeMedProfilePoPK> {

    @Query(value = "from MoeMedProfilePo a where a.hospcode = :hospitalCode and a.ordNo = :orderNo and a.itemStatus <> 'D' " +
            "order By a.orgItemNo")
    List<MoeMedProfilePo> findByHospCodeOrdno(@Param("hospitalCode") String hospitalCode,
                                              @Param("orderNo") Long orderNo);

}
