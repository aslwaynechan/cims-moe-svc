package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeEhrAcceptDrugCheckingPo;
import hk.health.moe.pojo.po.MoeEhrAcceptDrugCheckingPoPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeEhrAcceptDrugCheckingRepository extends JpaRepository<MoeEhrAcceptDrugCheckingPo, MoeEhrAcceptDrugCheckingPoPK> {
}
