package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeDoseFormExtraInfoPo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeDoseFormExtraInfoRepository extends JpaRepository<MoeDoseFormExtraInfoPo, Long> {



}
