package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeFormPo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeFormRepository extends JpaRepository<MoeFormPo, Long> {
}
