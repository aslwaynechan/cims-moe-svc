package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeBaseUnitPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MoeBaseUnitRepository extends JpaRepository<MoeBaseUnitPo, Long> {

    @Query(value = "from MoeBaseUnitPo where forBaseUnit = :forBaseUnit")
    List<MoeBaseUnitPo> findAllBaseUnit(@Param("forBaseUnit") String forBaseUnit);

    @Query(value = "from MoeBaseUnitPo where forPrescribeUnit = :forPrescribeUnit")
    List<MoeBaseUnitPo> findAllPrescribeUnit(@Param("forPrescribeUnit") String forPrescribeUnit);

    @Query(value = "from MoeBaseUnitPo where forDispenseUnit = :forDispenseUnit")
    List<MoeBaseUnitPo> findAllDispenseUnit(@Param("forDispenseUnit") String forDispenseUnit);

}
