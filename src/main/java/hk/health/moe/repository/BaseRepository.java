package hk.health.moe.repository;

import java.util.List;

public interface BaseRepository<T> {

    List<T> findAll(Class<T> entityClass);

    void save(T entityClass);

}
