package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeDrugTherapeuticLocalPo;
import hk.health.moe.pojo.po.MoeDrugTherapeuticLocalPoPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MoeDrugTherapeuticLocalRepository extends JpaRepository<MoeDrugTherapeuticLocalPo, MoeDrugTherapeuticLocalPoPK> {

    List<MoeDrugTherapeuticLocalPo> findByLocalDrugIdOrderByRank(String localDrugId);

}
