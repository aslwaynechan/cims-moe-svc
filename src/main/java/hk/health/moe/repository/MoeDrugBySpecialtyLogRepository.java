package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeDrugBySpecialtyLogPo;
import hk.health.moe.pojo.po.MoeDrugBySpecialtyLogPoPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeDrugBySpecialtyLogRepository extends JpaRepository<MoeDrugBySpecialtyLogPo, MoeDrugBySpecialtyLogPoPK> {
}
