package hk.health.moe.repository;

import hk.health.moe.pojo.dto.MoeDrugDto;
import hk.health.moe.pojo.po.MoeDrugPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MoeDrugRepository extends JpaRepository<MoeDrugPo, String> {

    // Ricci 20191024 start -- deal java heap space
    List<MoeDrugPo> findAll();

    @Query("select new hk.health.moe.pojo.dto.MoeDrugDto(po.hkRegNo, po.vtm) from MoeDrugPo po")
    List<MoeDrugDto> findHkRegNoAndVtm();
    // Ricci 20191024 end --

    //Simon 20190822 for moe_drug_server drug maintenance start--
    @Query("select count(a.hkRegNo) from MoeDrugPo a where a.hkRegNo = :hkRegNo")
    Integer isHkRegNoExist(@Param("hkRegNo") String hkRegNo);

    @Query("select distinct a from MoeDrugPo a " +
            " where a.hkRegNo = :hkRegNo and a.rank = :rank")
    MoeDrugPo findByHkRegNoAndRank(@Param("hkRegNo") String hkRegNo, @Param("rank") Long rank);

    @Query("select distinct a from MoeDrugPo a where a.hkRegNo = :hkRegNo ")
    List<MoeDrugPo> findByHkRegNo(String hkRegNo);
    //Simon 20190822 for moe_drug_server drug maintenance end--

}
