package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeRoutePo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MoeRouteRepository extends JpaRepository<MoeRoutePo, Long> {

    @Query(value = "from MoeRoutePo order by rank asc")
    List<MoeRoutePo> getAllOrderByRank();

}
