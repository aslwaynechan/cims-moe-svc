package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeOrderPo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeOrderRepository extends JpaRepository<MoeOrderPo, Long> {
}
