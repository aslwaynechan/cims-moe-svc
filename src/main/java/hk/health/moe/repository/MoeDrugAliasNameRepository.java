package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeDrugAliasNamePo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/8/22
 * @Description for moe_drug_server
 */
public interface MoeDrugAliasNameRepository extends JpaRepository<MoeDrugAliasNamePo, String> {

    //Simon 20190822 for moe_drug_server start--
    @Query("from MoeDrugAliasNamePo a where a.hkRegNo = :hkRegNo " +
            "order by a.aliasName asc")
    List<MoeDrugAliasNamePo> findByHkRegNo(@Param("hkRegNo") String hkRegNo);
    //Simon 20190822 for moe_drug_server end--

}
