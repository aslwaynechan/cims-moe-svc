package hk.health.moe.repository;

import hk.health.moe.pojo.dto.MoeDrugAliasNameTypeDto;
import hk.health.moe.pojo.po.MoeDrugAliasNameTypePo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MoeDrugAliasNameTypeRepository extends JpaRepository<MoeDrugAliasNameTypePo, String> {

    @Query(value = "select new hk.health.moe.pojo.dto.MoeDrugAliasNameTypeDto(po.aliasNameType, po.aliasNameDesc) from MoeDrugAliasNameTypePo po")
    List<MoeDrugAliasNameTypeDto> findAllAliasNameType();
}
