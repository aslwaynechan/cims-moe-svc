package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeClinicalAlertRulePo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeClinicalAlertRuleRepository extends JpaRepository<MoeClinicalAlertRulePo, Long> {
}
