package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeCommonDosagePo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface MoeCommonDosageRepository extends JpaRepository<MoeCommonDosagePo, Long> {

    List<MoeCommonDosagePo> findByLocalDrugId(String localDrugId);

    @Query(value = "select count (a.commonDosageId) from MoeCommonDosagePo a " +
            "where a.suspend = :suspend " +
            "and a.moeCommonDosageType.commonDosageType = :commonDosageType " +
            "and COALESCE(a.dosage, 0) = :dosage " +
            "and COALESCE(a.dosageUnit, '#') = :dosageUnit " +
            "and a.moeFreq.freqId = :freqId " +
            "and a.prn = :prn " +
            "and a.hkRegNo = :hkRegNo " +
            "and COALESCE(a.localDrugId, '#') = :localDrugId " +
            "and COALESCE(a.routeEng, '#') = :routeEng " +
            "and COALESCE(a.freq1, 0) = :freq1 " +
            "and a.commonDosageId not in (:deleteMoeCommonDosagesIds)")
    Long checkExistence(@Param("suspend") String suspend,
                              @Param("commonDosageType") String commonDosageType,
                              @Param("dosage") BigDecimal dosage,
                              @Param("dosageUnit") String dosageUnit,
                              @Param("freqId") Long freqId,
                              @Param("prn") String prn,
                              @Param("hkRegNo") String hkRegNo,
                              @Param("localDrugId") String localDrugId,
                              @Param("routeEng") String routeEng,
                              @Param("freq1") Long freq1,
                              @Param("deleteMoeCommonDosagesIds") List<Long> deleteMoeCommonDosagesIds);

/*    @Query(value = "select max(commonDosageId) from MoeCommonDosagePo")
    Integer getId();*/

    @Query(value = "from MoeCommonDosagePo a " +
            "where a.localDrugId = :localDrugId " +
            "order by a.moeRegimen.rank, a.dosage, " +
            "a.moeFreq.rank, a.moeFreq.freqId, a.freq1, " +
            "a.supplFreqEng, a.supplFreq1, " +
            "a.supplFreq2, a.prn, a.routeEng")
    List<MoeCommonDosagePo> findAllByLocalDrugId(@Param("localDrugId") String localDrugId);

    @Query(value = "from MoeCommonDosagePo a " +
            "where a.localDrugId in (:localDrugIds) " +
            "and a.moeCommonDosageType.commonDosageType = :dosageType " +
            "and COALESCE(a.dosage, 0) = :dosage " +
            "and a.moeFreq.freqId = :freqId " +
            "and a.prn = :prn " +
            "and COALESCE(a.freq1, 0) = :freq1 " +
            "and COALESCE(a.routeEng, '#') = :routeEng ")
    List<MoeCommonDosagePo> findMoeCommonDosageWithSameMoeDrug(@Param("localDrugIds") List<String> localDrugIds,
                                                               @Param("dosageType") String dosageType,
                                                               @Param("dosage") BigDecimal dosage,
                                                               @Param("freqId") Long freqId,
                                                               @Param("prn") String prn,
                                                               @Param("freq1") Long freq1,
                                                               @Param("routeEng") String routeEng);


    @Query(value = "from MoeCommonDosagePo commonDosage where commonDosage.localDrugId = " +
            "(select max(a.localDrugId) from MoeDrugLocalPo a where exists " +
            "(select 1 from MoeDrugPo where hkRegNo = :hkRegNo " +
            "and a.tradeName = tradeName " +
            "and a.vtm = vtm " +
            "and a.formEng = formEng " +
            "and COALESCE(a.doseFormExtraInfo,'') = COALESCE(doseFormExtraInfo,'') " +
            "and a.routeEng = routeEng " +
            "and strengthCompulsory = 'N' " +
            "and a.strengthCompulsory = strengthCompulsory) " +
            "and a.localDrugId <> :localDrugId) " +
            "order by commonDosage.moeRegimen.rank, commonDosage.dosage, " +
            "commonDosage.moeFreq.rank, commonDosage.moeFreq.freqId, " +
            "commonDosage.freq1, commonDosage.supplFreqEng, " +
            "commonDosage.supplFreq1, commonDosage.supplFreq2, " +
            "commonDosage.prn, commonDosage.hkRegNo, commonDosage.routeEng")
    List<MoeCommonDosagePo> findAllRelatedCommonDosageByHkRegNo(@Param("hkRegNo") String hkRegNo,
                                                                @Param("localDrugId") String localDrugId);

    @Query(value = "from MoeCommonDosagePo a where a.hkRegNo = :hkRegNo")
    List<MoeCommonDosagePo> findByHkRegNo(@Param("hkRegNo") String hkRegNo);

    @Query(value = "from MoeCommonDosagePo po where po.localDrugId in (:localDrugIds)")
    List<MoeCommonDosagePo> listByLocalDrugIds(@Param("localDrugIds") List<String> localDrugIds);

}
