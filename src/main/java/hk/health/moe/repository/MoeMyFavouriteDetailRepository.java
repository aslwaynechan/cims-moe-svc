package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeMyFavouriteDetailPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface MoeMyFavouriteDetailRepository extends JpaRepository<MoeMyFavouriteDetailPo, String> {
/*    @Query("from MoeMyFavouriteDetailPo d " +
            "where d.moeMyFavouriteHdr.myFavouriteId = :myFavouriteId " +
            "and d.itemNo = :itemNo")
    List<MoeMyFavouriteDetailPo> findByMyFavouriteIdAndItemNo(@Param("myFavouriteId") String myFavouriteId,
                                                              @Param("itemNo") Long itemNo);*/


    @Query("from MoeMyFavouriteDetailPo d " +
            "where d.moeMyFavouriteHdr.myFavouriteId = :myFavouriteId ")
    List<MoeMyFavouriteDetailPo> findByMyFavouriteIdOrderByItemNoAsc(@Param("myFavouriteId") String myFavouriteId);

    // Ricci 20191108 start --
    @Query("select new hk.health.moe.pojo.po.MoeMyFavouriteDetailPo" +
            "(po.itemNo, po.createUserId, po.createUser, po.createHosp, po.createRank, po.createRankDesc) " +
            "from MoeMyFavouriteDetailPo po " +
            "where po.moeMyFavouriteHdr.myFavouriteId = :myFavouriteId")
    Set<MoeMyFavouriteDetailPo> findCreateInfoByMyFavouriteId(@Param("myFavouriteId") String myFavouriteId);
    // Ricci 20191108 end --

}