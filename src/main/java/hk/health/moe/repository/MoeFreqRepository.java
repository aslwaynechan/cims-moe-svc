package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeFreqPo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MoeFreqRepository extends JpaRepository<MoeFreqPo,Long> {

    List<MoeFreqPo> findAll();
}
