package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeUserSettingPo;
import hk.health.moe.pojo.po.MoeUserSettingPoPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @Author Simon
 * @Date 2019/11/14
 */
public interface MoeUserSettingRepository extends JpaRepository<MoeUserSettingPo,MoeUserSettingPoPK> {


    @Query("from MoeUserSettingPo where loginId = :loginId")
    List<MoeUserSettingPo> findByLoginId(@Param("loginId")String loginId);
}
