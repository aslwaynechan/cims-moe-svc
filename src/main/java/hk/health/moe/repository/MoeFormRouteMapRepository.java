package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeFormRouteMapPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MoeFormRouteMapRepository extends JpaRepository<MoeFormRouteMapPo,String> {

    @Query("select distinct m from MoeFormRouteMapPo m join fetch m.moeRoute join fetch m.moeForm")
    List<MoeFormRouteMapPo> findAll();
}
