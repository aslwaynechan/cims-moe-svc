package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeMyFavouriteHdrPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface MoeMyFavouriteHdrRepository extends JpaRepository<MoeMyFavouriteHdrPo, String> {

    @Query("select distinct f from MoeMyFavouriteHdrPo as f" +
            " left join fetch f.moeMyFavouriteDetails as d" +
            " left join fetch d.moeMyFavouriteMultDoses" +
            " where f.createUserId = :createUserId and f.userSpecialty is null")
    List<MoeMyFavouriteHdrPo> findAllByUserId(@Param(value = "createUserId") String createUserId);


    @Query("select distinct f from MoeMyFavouriteHdrPo as f" +
            " left join fetch f.moeMyFavouriteDetails as d " +
            " left join fetch d.moeMyFavouriteMultDoses " +
            " where f.createUserId = :createUserId and f.userSpecialty is null" +
            " and (f.specialtyForMyFavourite is null or f.specialtyForMyFavourite = :myFavSpecialty) ")
    List<MoeMyFavouriteHdrPo> findAllByUserId(@Param(value = "createUserId") String createUserId,
                                              @Param(value = "myFavSpecialty") String myFavSpecialty);

    @Query("select distinct f from MoeMyFavouriteHdrPo as f" +
            " left join fetch f.moeMyFavouriteDetails as d" +
            " left join fetch d.moeMyFavouriteMultDoses" +
            " where f.userSpecialty = :userSpecialty")
    List<MoeMyFavouriteHdrPo> findAllByUserSpecialty(@Param(value = "userSpecialty") String userSpecialty);

    @Query("select distinct myFavouriteId, version from MoeMyFavouriteHdrPo where userSpecialty = :userSpecialty")
    List<Object[]> getDeptMyFavouriteVersionMapping(@Param(value = "userSpecialty") String userSpecialty);

    ;

    @Query("select distinct myFavouriteId, version from MoeMyFavouriteHdrPo where createUserId = :createUserId")
    List<Object[]> getMyFavouriteVersionMapping(@Param(value = "createUserId") String createUserId);

    @Query("select distinct f from MoeMyFavouriteHdrPo as f" +
            " left join fetch f.moeMyFavouriteDetails as d" +
            " left join fetch d.moeMyFavouriteMultDoses " +
            " where f.myFavouriteId = :myFavouriteId ")
    List<MoeMyFavouriteHdrPo> getAllByMyFavouriteId(@Param("myFavouriteId") String myFavouriteId);

    @Query("from MoeMyFavouriteHdrPo as f" +
            " where f.createUserId = :createUserId and f.userSpecialty is null")
    List<MoeMyFavouriteHdrPo> findByUserId(@Param("createUserId") String createUserId);

    @Query("select distinct f from MoeMyFavouriteHdrPo as f" +
            " where f.createUserId = :createUserId and f.userSpecialty is null" +
            " and (f.specialtyForMyFavourite is null or f.specialtyForMyFavourite = :myFavSpecialty) ")
    List<MoeMyFavouriteHdrPo> findByUserId(@Param(value = "createUserId") String createUserId,
                                           @Param(value = "myFavSpecialty") String myFavSpecialty);

    @Query("from MoeMyFavouriteHdrPo as f where f.frontMyFavouriteId = :myFrontFavouriteId")
    List<MoeMyFavouriteHdrPo> getByMyFrontFavouriteId(@Param("myFrontFavouriteId") String myFrontFavouriteId);

    @Query("from MoeMyFavouriteHdrPo as f where f.myFavouriteId = :myFavouriteId")
    List<MoeMyFavouriteHdrPo> getByMyFavouriteId(@Param("myFavouriteId") String myFavouriteId);

    @Query(value = "select distinct f from MoeMyFavouriteHdrPo as f" +
            " left join fetch f.moeMyFavouriteDetails as d" +
            " left join fetch d.moeMyFavouriteMultDoses" +
            " where d.nameType = 'L'" +
            " and (d.tradeName = :tradeName or :tradeName = '' or :tradeName is null)" +
            " and (d.vtm = :vtm or :vtm = '' or :vtm is null)" +
            " and (d.preparation = :strength or :strength = '' or :strength is null)" +
            " and (d.strengthLevelExtraInfo = :strengthLevelExtraInfo or :strengthLevelExtraInfo = '' or :strengthLevelExtraInfo is null)" +
            " and (d.formId = :formId or :formId = 0 or :formId is null)" +
            " and (d.formEng = :formEng or :formEng = '' or :formEng is null)" +
            " and (d.doseFormExtraInfo = :doseFormExtraInfo or :doseFormExtraInfo = '' or :doseFormExtraInfo is null)" +
            " and (d.doseFormExtraInfoId = :doseFormExtraInfoId or :doseFormExtraInfoId = 0 or :doseFormExtraInfoId is null)" +
            " and (d.routeEng = :routeEng or :routeEng = '' or :routeEng is null)" +
            " and (d.routeId = :routeId or :routeId = 0 or :routeId is null)" +
            " and (d.screenDisplay = :screenDisplay or :screenDisplay = '' or :screenDisplay is null)")
    List<MoeMyFavouriteHdrPo> findAllByLocalDrug(@Param("tradeName") String tradeName,
                                                 @Param("vtm") String vtm,
                                                 @Param("strength") String strength,
                                                 @Param("strengthLevelExtraInfo") String strengthLevelExtraInfo,
                                                 @Param("formId") Long formId,
                                                 @Param("formEng") String formEng,
                                                 @Param("doseFormExtraInfo") String doseFormExtraInfo,
                                                 @Param("doseFormExtraInfoId") Long doseFormExtraInfoId,
                                                 @Param("routeEng") String routeEng,
                                                 @Param("routeId") Long routeId,
                                                 @Param("screenDisplay") String screenDisplay);

    @Query(value = "select distinct f from MoeMyFavouriteHdrPo as f" +
            " left join fetch f.moeMyFavouriteDetails as d" +
            " left join fetch d.moeMyFavouriteMultDoses" +
            " where d.nameType = 'N'" +
            " and (d.tradeName = :tradeName or :tradeName = '' or :tradeName is null)" +
            " and (d.vtm = :vtm or :vtm = '' or :vtm is null)" +
            " and (d.formId = :formId or :formId = 0 or :formId is null)" +
            " and (d.formEng = :formEng or :formEng = '' or :formEng is null)" +
            " and (d.doseFormExtraInfo = :doseFormExtraInfo or :doseFormExtraInfo = '' or :doseFormExtraInfo is null)" +
            " and (d.doseFormExtraInfoId = :doseFormExtraInfoId or :doseFormExtraInfoId = 0 or :doseFormExtraInfoId is null)" +
            " and (d.routeEng = :routeEng or :routeEng = '' or :routeEng is null)" +
            " and (d.routeId = :routeId or :routeId = 0 or :routeId is null)" +
            " and d.strengthCompulsory = 'N'" +
            " and (d.aliasName = :aliasName or :aliasName = '' or :aliasName is null)")
    List<MoeMyFavouriteHdrPo> findAllByMttDrugN(@Param("tradeName") String tradeName,
                                                @Param("vtm") String vtm,
                                                @Param("formId") Long formId,
                                                @Param("formEng") String formEng,
                                                @Param("doseFormExtraInfo") String doseFormExtraInfo,
                                                @Param("doseFormExtraInfoId") Long doseFormExtraInfoId,
                                                @Param("routeEng") String routeEng,
                                                @Param("routeId") Long routeId,
                                                @Param("aliasName") String aliasName);

    @Query(value = "select distinct f from MoeMyFavouriteHdrPo as f" +
            " left join fetch f.moeMyFavouriteDetails as d" +
            " left join fetch d.moeMyFavouriteMultDoses" +
            " where d.nameType = 'N'" +
            " and (d.tradeName = :tradeName or :tradeName = '' or :tradeName is null)" +
            " and (d.vtm = :vtm or :vtm = '' or :vtm is null)" +
            " and (d.preparation = :strength or :strength = '' or :strength is null)" +
            " and (d.strengthLevelExtraInfo = :strengthLevelExtraInfo or :strengthLevelExtraInfo = '' or :strengthLevelExtraInfo is null)" +
            " and (d.formId = :formId or :formId = 0 or :formId is null)" +
            " and (d.formEng = :formEng or :formEng = '' or :formEng is null)" +
            " and (d.doseFormExtraInfo = :doseFormExtraInfo or :doseFormExtraInfo = '' or :doseFormExtraInfo is null)" +
            " and (d.doseFormExtraInfoId = :doseFormExtraInfoId or :doseFormExtraInfoId = 0 or :doseFormExtraInfoId is null)" +
            " and (d.drugRouteEng = :drugRouteEng or :drugRouteEng = '' or :drugRouteEng is null)" +
            " and (d.drugRouteId = :drugRouteId or :drugRouteId = 0 or :drugRouteId is null)" +
            " and (d.screenDisplay = :screenDisplay or :screenDisplay = '' or :screenDisplay is null)" +
            " and d.strengthCompulsory = 'Y'" +
            " and (d.aliasName = :aliasName or :aliasName = '' or d.aliasName is null)")
    List<MoeMyFavouriteHdrPo> findAllByMttDrugY(@Param("tradeName") String tradeName,
                                                @Param("vtm") String vtm,
                                                @Param("strength") String strength,
                                                @Param("strengthLevelExtraInfo") String strengthLevelExtraInfo,
                                                @Param("formId") Long formId,
                                                @Param("formEng") String formEng,
                                                @Param("doseFormExtraInfo") String doseFormExtraInfo,
                                                @Param("doseFormExtraInfoId") Long doseFormExtraInfoId,
                                                @Param("drugRouteEng") String drugRouteEng,
                                                @Param("drugRouteId") Long drugRouteId,
                                                @Param("screenDisplay") String screenDisplay,
                                                @Param("aliasName") String aliasName);

    @Query(value = "select distinct f from MoeMyFavouriteHdrPo as f" +
            " left join fetch f.moeMyFavouriteDetails as d" +
            " left join fetch d.moeMyFavouriteMultDoses" +
            " where d.nameType = 'N'" +
            " and (d.tradeName = :tradeName or :tradeName = '' or :tradeName is null)" +
            " and (d.vtm = :vtm or :vtm = '' or :vtm is null)" +
            " and (d.preparation = :strength or :strength = '' or :strength is null)" +
            " and (d.strengthLevelExtraInfo = :strengthLevelExtraInfo or :strengthLevelExtraInfo = '' or :strengthLevelExtraInfo is null)" +
            " and (d.formId = :formId or :formId = 0 or :formId is null)" +
            " and (d.formEng = :formEng or :formEng = '' or :formEng is null)" +
            " and (d.doseFormExtraInfo = :doseFormExtraInfo or :doseFormExtraInfo = '' or :doseFormExtraInfo is null)" +
            " and (d.doseFormExtraInfoId = :doseFormExtraInfoId or :doseFormExtraInfoId = 0 or :doseFormExtraInfoId is null)" +
            " and (d.drugRouteEng = :drugRouteEng or :drugRouteEng = '' or :drugRouteEng is null)" +
            " and (d.drugRouteId = :drugRouteId or :drugRouteId = 0 or :drugRouteId is null)" +
            " and d.strengthCompulsory = 'Y'" +
            " and (d.aliasName = :aliasName or :aliasName = '' or d.aliasName is null)")
    List<MoeMyFavouriteHdrPo> findAllBySingleMttDrugY(@Param("tradeName") String tradeName,
                                                      @Param("vtm") String vtm,
                                                      @Param("strength") String strength,
                                                      @Param("strengthLevelExtraInfo") String strengthLevelExtraInfo,
                                                      @Param("formId") Long formId,
                                                      @Param("formEng") String formEng,
                                                      @Param("doseFormExtraInfo") String doseFormExtraInfo,
                                                      @Param("doseFormExtraInfoId") Long doseFormExtraInfoId,
                                                      @Param("drugRouteEng") String drugRouteEng,
                                                      @Param("drugRouteId") Long drugRouteId,
                                                      @Param("aliasName") String aliasName);

    // Ricci 20191107 start --
    @Query("select distinct f from MoeMyFavouriteHdrPo as f" +
            " left join fetch f.moeMyFavouriteDetails as d" +
            " left join fetch d.moeMyFavouriteMultDoses" +
            " where f.userSpecialty = :userSpecialty " +
            "and f.myFavouriteId not in (:excludeFavIdList)")
    List<MoeMyFavouriteHdrPo> findAllByUserSpecialtyExcludeIdSet(@Param("userSpecialty") String userSpecialty,
                                                                 @Param("excludeFavIdList") Set excludeFavIdSet);
    // Ricci 20191107 end --

    @Query("select new hk.health.moe.pojo.po.MoeMyFavouriteHdrPo" +
            "(po.createUser, po.createHosp, po.createRank, po.createRankDesc, po.createDate, po.createUserId) " +
            "from MoeMyFavouriteHdrPo po " +
            "where po.myFavouriteId = :myFavouriteId")
    MoeMyFavouriteHdrPo findCreateInfoByMyFavouriteId(@Param("myFavouriteId") String myFavouriteId);

}
