package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeLegalClassPo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeLegalClassRepository extends JpaRepository<MoeLegalClassPo, Long> {
}
