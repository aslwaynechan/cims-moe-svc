package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeDrugStrengthLocalLogPo;
import hk.health.moe.pojo.po.MoeDrugStrengthLocalLogPoPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeDrugStrengthLocalLogRepository extends JpaRepository<MoeDrugStrengthLocalLogPo, MoeDrugStrengthLocalLogPoPK> {
}
