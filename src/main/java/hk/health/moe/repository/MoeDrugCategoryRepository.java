package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeDrugCategoryPo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeDrugCategoryRepository extends JpaRepository<MoeDrugCategoryPo, String> {
}
