package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeCommonDosageLogPo;
import hk.health.moe.pojo.po.MoeCommonDosageLogPoPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeCommonDosageLogRepository extends JpaRepository<MoeCommonDosageLogPo, MoeCommonDosageLogPoPK> {
}
