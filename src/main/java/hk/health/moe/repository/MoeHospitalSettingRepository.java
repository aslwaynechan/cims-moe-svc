package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeHospitalSettingPo;
import hk.health.moe.pojo.po.MoeHospitalSettingPoPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MoeHospitalSettingRepository extends JpaRepository<MoeHospitalSettingPo, MoeHospitalSettingPoPK> {

    @Query(value = "from MoeHospitalSettingPo a where a.paramName = 'default_drug_by_specialty_availability'")
    List<MoeHospitalSettingPo> getAllSpecialtyAvailability();

    @Query("select a from MoeHospitalSettingPo a, MoeLocationUserGroupMapPo b " +
            "where a.hospcode = b.hospcode and a.userSpecialty = b.userSpecialty " +
            "and a.paramName = 'hospital_name_eng' " +
            "order by a.userSpecialty asc, a.hospcode asc ")
    List<MoeHospitalSettingPo> getAllLocationByUserGroupMap();

    @Query("select a from MoeHospitalSettingPo a, MoeLocationUserGroupMapPo b " +
            "where a.hospcode = b.hospcode and a.userSpecialty = b.userSpecialty " +
            "and a.paramName = 'hospital_name_eng' and a.hospcode = :hospCode " +
            "order by a.userSpecialty asc ")
    List<MoeHospitalSettingPo> getAllLocationByUserGroupMap(@Param("hospCode") String hospCode);

}
