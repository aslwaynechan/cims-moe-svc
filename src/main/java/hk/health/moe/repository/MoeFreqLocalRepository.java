package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeFreqLocalPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MoeFreqLocalRepository extends JpaRepository<MoeFreqLocalPo, String> {

    @Query("from MoeFreqLocalPo mf order by mf.rank, mf.freqCode")
    List<MoeFreqLocalPo> getAllOrderByRank();

    @Query(value = "select a.trade_name, b.alias_name, a.vtm from moe_drug_local a " +
            "inner join moe_drug_alias_name_local b on a.hk_reg_no = b.hk_reg_no  " +
            "where a.trade_name is not null and (b.alias_name_type = ? or b.alias_name_type = ?)", nativeQuery = true)
    List<Object[]> getAllTradeNameAliasNamePairs(String aliasNameTypeFor1, String aliasNameTypeFor2);
}
