package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoePatientCasePo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MoePatientCaseRepository extends JpaRepository<MoePatientCasePo,String> {

    @Query(value = "from MoePatientCasePo m where m.caseRefNo = :caseRefNo")
    List<MoePatientCasePo> getPatientCaseByRefKey(@Param("caseRefNo") String caseRefNo);

}
