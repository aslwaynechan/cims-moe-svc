package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeSystemSettingPo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SystemSettingRepository extends JpaRepository<MoeSystemSettingPo, String> {

    List<MoeSystemSettingPo> findAllByParamName(String paramName);
}
