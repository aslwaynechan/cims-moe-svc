package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeDrugAliasNameLocalLogPo;
import hk.health.moe.pojo.po.MoeDrugAliasNameLocalLogPoPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeDrugAliasNameLocalLogRepository extends JpaRepository<MoeDrugAliasNameLocalLogPo, MoeDrugAliasNameLocalLogPoPK> {
}
