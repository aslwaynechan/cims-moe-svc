package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeMedMultDosePo;
import hk.health.moe.pojo.po.MoeMedMultDosePoPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MoeMedMultDoseRepository extends JpaRepository<MoeMedMultDosePo, MoeMedMultDosePoPK> {

    @Query(value = "from MoeMedMultDosePo a " +
            "where a.hospcode = :hospitalCode " +
            "and a.ordNo = :orderNo " +
            "and a.cmsItemNo = :cmsItemNo " +
            "order by a.stepNo asc, a.multDoseNo asc")
    List<MoeMedMultDosePo> findByHospcodeAndOrdNoAndCmsItemNo(@Param("hospitalCode") String hospitalCode,
                                                              @Param("orderNo") Long orderNo,
                                                              @Param("cmsItemNo") Long cmsItemNo);

}
