package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeEhrMedProfilePo;
import hk.health.moe.pojo.po.MoeMedProfilePoPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeEhrMedProfileRepository extends JpaRepository<MoeEhrMedProfilePo, MoeMedProfilePoPK> {
}
