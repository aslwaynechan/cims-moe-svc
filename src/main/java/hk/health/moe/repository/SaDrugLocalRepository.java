package hk.health.moe.repository;

import hk.health.moe.pojo.po.SaDrugLocalPo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SaDrugLocalRepository extends JpaRepository<SaDrugLocalPo, String> {
}
