package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeDrugTherapeuticLocalLogPo;
import hk.health.moe.pojo.po.MoeDrugTherapeuticLocalLogPoPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeDrugTherapeuticLocalLogRepository extends JpaRepository<MoeDrugTherapeuticLocalLogPo, MoeDrugTherapeuticLocalLogPoPK> {
}
