package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeDrugBySpecialtyPo;
import hk.health.moe.pojo.po.MoeDrugBySpecialtyPoPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MoeDrugBySpecialtyRepository extends JpaRepository<MoeDrugBySpecialtyPo, MoeDrugBySpecialtyPoPK> {

    @Query(value = "select distinct hospcode, userSpecialty from MoeDrugBySpecialtyPo")
    List<Object[]> findAllHospcodeSpecialty();

    @Query(value = "from MoeDrugBySpecialtyPo a where a.localDrugId = :localDrugId " +
            "order by a.userSpecialty asc, a.hospcode asc")
    List<MoeDrugBySpecialtyPo> findByLocalDrugId(@Param("localDrugId") String localDrugId);

    @Query("from MoeDrugBySpecialtyPo a where a.localDrugId = :localDrugId and a.hospcode = :hospitalCode " +
            "order by a.userSpecialty asc")
    List<MoeDrugBySpecialtyPo> findByLocalDrugIdHospCode(@Param("localDrugId") String localDrugId,
                                                         @Param("hospitalCode") String hospitalCode);

}
