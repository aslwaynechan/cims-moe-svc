package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeDrugStrengthPo;
import hk.health.moe.pojo.po.MoeDrugStrengthPoPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/8/22
 */
public interface MoeDrugStrengthRepository extends JpaRepository<MoeDrugStrengthPo, MoeDrugStrengthPoPK> {

    //Simon 20190822 for moe_drug_server drug maintenance start--
    @Query("from MoeDrugStrengthPo a where a.hkRegNo= :hkRegNo")
    List<MoeDrugStrengthPo> findByHkRegNo (@Param("hkRegNo") String hkRegNo);


    @Query("Select count(a.hkRegNo) from MoeDrugStrengthPo a " +
            "Where not exists (Select 1 from MoeDrugLocalPo b, MoeDrugStrengthLocalPo c " +
            "Where b.localDrugId = c.localDrugId " +
            "and a.hkRegNo = b.hkRegNo " +
            "and COALESCE(a.strength, '') = COALESCE(c.strength, '') " +
            "and COALESCE(a.strengthLevelExtraInfo, '') = COALESCE(c.strengthLevelExtraInfo, '')) " +
            "and a.hkRegNo = :hkRegNo")
    Integer getNumRecordNotInLocal(@Param("hkRegNo")String hkRegNo);

    @Query("from MoeDrugStrengthPo a " +
            "Where not exists (Select 1 from MoeDrugLocalPo b, MoeDrugStrengthLocalPo c " +
            "Where b.localDrugId = c.localDrugId " +
            "and a.hkRegNo = b.hkRegNo " +
            "and COALESCE(a.strength, '') = COALESCE(c.strength, '') " +
            "and COALESCE(a.strengthLevelExtraInfo, '') = COALESCE(c.strengthLevelExtraInfo, '')) " +
            "and a.hkRegNo = :hkRegNo")
    List<MoeDrugStrengthPo> findRecordNotInLocal(@Param("hkRegNo") String hkRegNo);

    @Query("from MoeDrugStrengthPo a where a.hkRegNo = :hkRegNo and a.rank = :rank")
    MoeDrugStrengthPo findByHkRegNoAndRank (@Param("hkRegNo")String hkRegNo, @Param("rank")Long rank);

    @Query("from MoeDrugStrengthPo a where a.hkRegNo = :hkRegNo and a.amp = :amp")
    MoeDrugStrengthPo findByHkRegNoAndAmp (@Param("hkRegNo")String hkRegNo,@Param("amp") String amp);
    //Simon 20190822 for moe_drug_server end--

    //Chris 20200113 For Search vmpId from moe_drug_strength  -Start
    @Query("from MoeDrugStrengthPo a where a.hkRegNo = :hkRegNo and a.ampId = :ampId")
    MoeDrugStrengthPo findByHkRegNoAndAmpId (@Param("hkRegNo")String hkRegNo, @Param("ampId")Long ampId);
    //Chris 20200113 For Search vmpId from moe_drug_strength  -End

}
