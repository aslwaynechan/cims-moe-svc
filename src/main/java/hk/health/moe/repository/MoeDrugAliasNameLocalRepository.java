package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeDrugAliasNameLocalPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MoeDrugAliasNameLocalRepository extends JpaRepository<MoeDrugAliasNameLocalPo, String> {

   // Simon 20190822 for moe_drug_server start --
    @Query("from MoeDrugAliasNameLocalPo a where a.hkRegNo = :hkRegNo " +
            "order by a.aliasNameId asc")
    List<MoeDrugAliasNameLocalPo> findByHkRegNo (@Param("hkRegNo") String hkRegNo);
    // Simon 20190822 for moe_drug_server end --
}
