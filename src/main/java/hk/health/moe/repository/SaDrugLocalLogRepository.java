package hk.health.moe.repository;

import hk.health.moe.pojo.po.SaDrugLocalLogPo;
import hk.health.moe.pojo.po.SaDrugLocalLogPoPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SaDrugLocalLogRepository extends JpaRepository<SaDrugLocalLogPo, SaDrugLocalLogPoPK> {
}
