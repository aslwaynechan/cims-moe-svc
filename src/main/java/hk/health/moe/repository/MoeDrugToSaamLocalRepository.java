package hk.health.moe.repository;


import hk.health.moe.pojo.po.MoeDrugToSaamLocalPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/8/23
 */
public interface MoeDrugToSaamLocalRepository extends JpaRepository<MoeDrugToSaamLocalPo,String> {
    @Query("select count(a.localDrugId) from MoeDrugToSaamLocalPo a where upper(a.localDrugId) = :localDrugId")
    Integer isExist(@Param("localDrugId") String localDrugId);
}
