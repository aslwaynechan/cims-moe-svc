package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeOrderLogPo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeOrderLogRepository extends JpaRepository<MoeOrderLogPo,Long> {
}
