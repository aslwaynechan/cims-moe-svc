package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeDrugOrdPropertyLocLogPo;
import hk.health.moe.pojo.po.MoeDrugOrdPropertyLocLogPoPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeDrugOrdPropertyLocLogRepository extends JpaRepository<MoeDrugOrdPropertyLocLogPo, MoeDrugOrdPropertyLocLogPoPK> {

}
