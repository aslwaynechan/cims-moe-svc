package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeEhrRxImageLogPo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeEhrRxImageLogRepository extends JpaRepository<MoeEhrRxImageLogPo, String> {

    MoeEhrRxImageLogPo findFirstByOrdNoAndRefNoOrderByCreateDtm(Long ordNo, String refNo);

}
