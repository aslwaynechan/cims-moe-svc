package hk.health.moe.repository;

import hk.health.moe.pojo.dto.MoeDrugOrdPropertyLocalDto;
import hk.health.moe.pojo.po.MoeDrugOrdPropertyLocalPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MoeDrugOrdPropertyLocalRepository extends JpaRepository<MoeDrugOrdPropertyLocalPo, String> {

    @Query(value = "select distinct b.strengthCompulsory, b.tradeName, b.vtm, b.routeEng, b.formEng, b.doseFormExtraInfo, c.strength, a.minimumDosage, a.maximumDuration, a.specifyQuantityFlag " +
            "from MoeDrugOrdPropertyLocalPo a join a.moeDrugLocal b " +
            "join b.moeDrugStrengths c " +
            "where a.suspend = 'N' and (a.minimumDosage is not null or a.maximumDuration is not null or a.specifyQuantityFlag = 'Y') " +
            "order by a.minimumDosage")
    List<Object[]> getMinDosageAndMaxDurationByVtmRouteForm();

    @Query(value = "from MoeDrugOrdPropertyLocalPo a Where Exists " +
            "(select 1 from MoeDrugLocalPo b " +
            "where a.localDrugId = b.localDrugId " +
            "and COALESCE(b.tradeName, '#') = :tradeName " +
            "and COALESCE(b.vtm, '#') = :vtm " +
            "and COALESCE(b.routeId, 0) = :routeId " +
            "and COALESCE(b.formId, 0) = :formId " +
            "and COALESCE(b.doseFormExtraInfo, '#') = :doseFormExtraInfo ) ")
    List<MoeDrugOrdPropertyLocalPo> getRelatedDrugOrdPropertyLocal(@Param("tradeName") String tradeName,
                                                                   @Param("vtm") String vtm,
                                                                   @Param("routeId") Long routeId,
                                                                   @Param("formId") Long formId,
                                                                   @Param("doseFormExtraInfo") String doseFormExtraInfo);

    @Query(value = "select new hk.health.moe.pojo.dto.MoeDrugOrdPropertyLocalDto(po.createUserId, po.createUser, po.createHosp, " +
            "po.createRank, po.createRankDesc, po.createDtm, po.maximumDuration) from MoeDrugOrdPropertyLocalPo po where po.localDrugId = :localDrugId")
    MoeDrugOrdPropertyLocalDto findCreateInfoAndMaximumDurationByLocalDrugId(@Param("localDrugId") String localDrugId);

    @Query(value = "from MoeDrugOrdPropertyLocalPo po where po.localDrugId in (:localDrugIds)")
    List<MoeDrugOrdPropertyLocalPo> listLocalDrugByLocalDrugIds(@Param("localDrugIds") List<String> localDrugIds);

}
