package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeSupplFreqPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MoeSupplFreqRepository extends JpaRepository<MoeSupplFreqPo, Integer> {

    @Query("select distinct f from MoeSupplFreqPo f " +
            "left join fetch f.moeSupplFreqDescs d " +
            "left join fetch f.moeSupplFreqSelections s " +
            "left join fetch f.moeRegimen r " +
            "order by r.rank asc, f.rank asc, d.rank asc, s.rank asc")
    List<MoeSupplFreqPo> getAllOrderByTypeAndRank();
}
