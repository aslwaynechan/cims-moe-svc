package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeEhrMedMultDosePo;
import hk.health.moe.pojo.po.MoeMedMultDosePoPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeEhrMedMultDoseRepository extends JpaRepository<MoeEhrMedMultDosePo, MoeMedMultDosePoPK> {
}
