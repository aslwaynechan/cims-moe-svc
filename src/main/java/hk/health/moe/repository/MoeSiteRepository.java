package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeSitePo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MoeSiteRepository extends JpaRepository<MoeSitePo, Long> {

    @Query(value = "from MoeSitePo order by siteEng asc")
    List<MoeSitePo> getAllOrderByRankAndSite();

}
