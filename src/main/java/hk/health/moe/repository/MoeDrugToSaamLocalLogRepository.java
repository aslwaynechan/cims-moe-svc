package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeDrugToSaamLocalLogPo;
import hk.health.moe.pojo.po.MoeDrugToSaamLocalLogPoPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoeDrugToSaamLocalLogRepository extends JpaRepository<MoeDrugToSaamLocalLogPo, MoeDrugToSaamLocalLogPoPK> {
}
