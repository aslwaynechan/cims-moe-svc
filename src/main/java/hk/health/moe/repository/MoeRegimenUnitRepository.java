package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeRegimenUnitPo;
import hk.health.moe.pojo.po.MoeRegimenUnitPoPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MoeRegimenUnitRepository extends JpaRepository<MoeRegimenUnitPo, MoeRegimenUnitPoPK> {

    @Query(value = "from MoeRegimenUnitPo m order by m.regimenType,m.rank")
    List<MoeRegimenUnitPo> getAllOrderByRank();

}
