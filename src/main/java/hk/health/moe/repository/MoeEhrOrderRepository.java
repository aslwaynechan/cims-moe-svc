package hk.health.moe.repository;

import hk.health.moe.pojo.po.MoeEhrOrderPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface MoeEhrOrderRepository extends JpaRepository<MoeEhrOrderPo, Long> {

    @Query(value = "select distinct o from MoeEhrOrderPo as o " +
            " join fetch o.moeOrder as o2 " +
            //Chris 20190821  -Start
            " left join fetch o2.moeMedProfiles as p " +
            " left join fetch p.moeEhrMedProfile " +
            //Chris 20190821  -End
            " left join fetch p.moeMedMultDoses as m " +
            " left join fetch p.moeEhrMedAllergens " +
            " left join fetch m.moeEhrMedMultDose " +
            " where o.moePatient.moePatientKey = :patientKey and o.ordNo = :ordNo and o2.ordStatus = 'O' " +
            " order by o2.ordDate desc, o.ordNo desc ")
    List<MoeEhrOrderPo> findByPatientKeyAndOrderNo(@Param("patientKey") String patientKey,
                                                   @Param("ordNo") long ordNo);

    //Chris Get order no sequence -Start
    @Query(value = "select seq_order_id.nextval from dual", nativeQuery = true)
    Long getNextVal();
    //Chris Get order no sequence -End

    //Chris 20190730 Find order without profile -Start
    @Query(value = "select distinct o from MoeEhrOrderPo  as o " +
            " join fetch o.moeOrder as o2 " +
            " where o.moePatient.moePatientKey = :patientKey and o.ordNo = :ordNo and o2.ordStatus = 'O' " +
            " order by o2.ordDate desc, o.ordNo desc ")
    List<MoeEhrOrderPo> findOrderWithoutProfileByPatientKeyAndOrderNo(@Param("patientKey") String patientKey,
                                                                      @Param("ordNo") long ordNo);
    //Chris 20190730 Find order without profile -End

    // Simon 20190730 --start add for drug history
    @Query("select distinct o from MoeEhrOrderPo as o" +
            " left join fetch o.moeOrder as o2" +
            " left join fetch o2.moeMedProfiles as p" +
            " left join fetch p.moeEhrMedProfile" +
            " left join fetch p.moeMedMultDoses as m" +
            " left join fetch p.moeEhrMedAllergens" +
            " left join fetch m.moeEhrMedMultDose" +
            " where o.moePatient.moePatientKey = :patientKey and o2.ordStatus = :ordStatus" +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findAllByPatient(@Param("patientKey") String patientKey, @Param("ordStatus") String ordStatus);

    @Query("select distinct o from MoeEhrOrderPo as o" +
            " left join fetch o.moeOrder as o2" +
            " left join fetch o2.moeMedProfiles as p" +
            " left join fetch p.moeEhrMedProfile" +
            " left join fetch p.moeMedMultDoses as m" +
            " left join fetch p.moeEhrMedAllergens" +
            " left join fetch m.moeEhrMedMultDose" +
            " where o.moePatient.moePatientKey = :patientKey and o2.ordStatus = :ordStatus" +
            " and o2.hospcode = :hospcode" +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findAllByPatient(@Param("patientKey") String patientKey, @Param("ordStatus") String ordStatus, @Param("hospcode") String hospcode);

    @Query("select distinct o from MoeEhrOrderPo as o" +
            " left join fetch o.moeOrder as o2" +
            " left join fetch o2.moeMedProfiles as p" +
            " left join fetch p.moeEhrMedProfile" +
            " left join fetch p.moeMedMultDoses as m" +
            " left join fetch p.moeEhrMedAllergens" +
            " left join fetch m.moeEhrMedMultDose" +
            " where o.moePatient.moePatientKey = :patientKey and o2.ordDate >= :orderDate and o2.ordStatus = :ordStatus" +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findAllByPatient(@Param("patientKey") String patientKey, @Param("orderDate") Date orderDate, @Param("ordStatus") String ordStatus);

    @Query("select distinct o from MoeEhrOrderPo as o" +
            " left join fetch o.moeOrder as o2" +
            " left join fetch o2.moeMedProfiles as p" +
            " left join fetch p.moeEhrMedProfile" +
            " left join fetch p.moeMedMultDoses as m" +
            " left join fetch p.moeEhrMedAllergens" +
            " left join fetch m.moeEhrMedMultDose" +
            " where o.moePatient.moePatientKey = :patientKey and o2.ordDate >= :afterDate and o2.ordStatus = :ordStatus" +
            " and o2.hospcode = :hospcode" +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findAllByPatient(@Param("patientKey") String patientKey, @Param("afterDate") Date afterDate, @Param("ordStatus") String ordStatus, @Param("hospcode") String hospcode);

    @Query("select distinct o from MoeEhrOrderPo as o" +
            " left join fetch o.moeOrder as o2" +
            " left join fetch o2.moeMedProfiles as p" +
            " left join fetch p.moeEhrMedProfile" +
            " left join fetch p.moeMedMultDoses as m" +
            " left join fetch p.moeEhrMedAllergens" +
            " left join fetch m.moeEhrMedMultDose" +
            " where o.moePatient.moePatientKey = :patientKey and o2.ordDate >= :afterDate and o2.prescType = :prescType and o2.ordStatus = :ordStatus" +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findAllByPatientAndPrescType(@Param("patientKey") String patientKey, @Param("afterDate") Date afterDate, @Param("prescType") String prescType, @Param("ordStatus") String ordStatus);

    @Query("select distinct o from MoeEhrOrderPo as o" +
            " left join fetch o.moeOrder as o2" +
            " left join fetch o2.moeMedProfiles as p" +
            " left join fetch p.moeEhrMedProfile" +
            " left join fetch p.moeMedMultDoses as m" +
            " left join fetch p.moeEhrMedAllergens" +
            " left join fetch m.moeEhrMedMultDose" +
            " where o.moePatient.moePatientKey = :patientKey and o2.ordDate >= :afterDate and o2.prescType = :prescType and o2.ordStatus = :ordStatus" +
            " and o2.hospcode = :hospcode" +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findAllByPatientAndPrescType(@Param("patientKey") String patientKey, @Param("afterDate") Date afterDate, @Param("prescType") String prescType, @Param("ordStatus") String ordStatus, @Param("hospcode") String hospcode);

    @Query("select distinct o from MoeEhrOrderPo as o" +
            " left join fetch o.moeOrder as o2" +
            " left join fetch o2.moeMedProfiles as p" +
            " left join fetch p.moeEhrMedProfile" +
            " left join fetch p.moeMedMultDoses as m" +
            " left join fetch p.moeEhrMedAllergens" +
            " left join fetch m.moeEhrMedMultDose" +
            " where o.moePatient.moePatientKey = :patientKey and o2.prescType = :prescType and o2.ordStatus = :ordStatus" +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findAllByPatientAndPrescType(@Param("patientKey") String patientKey, @Param("prescType") String prescType, @Param("ordStatus") String ordStatus);

    @Query("select distinct o from MoeEhrOrderPo as o" +
            " left join fetch o.moeOrder as o2" +
            " left join fetch o2.moeMedProfiles as p" +
            " left join fetch p.moeEhrMedProfile" +
            " left join fetch p.moeMedMultDoses as m" +
            " left join fetch p.moeEhrMedAllergens" +
            " left join fetch m.moeEhrMedMultDose" +
            " where o.moePatient.moePatientKey = :patientKey and o2.prescType = :prescType and o2.ordStatus = :ordStatus" +
            " and o2.hospcode = :hospcode" +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findAllByPatientAndPrescType(@Param("patientKey") String patientKey, @Param("prescType") String prescType, @Param("ordStatus") String ordStatus, @Param("hospcode") String hospcode);
    // Simon 20190730 --end add for drug history

    // Ricci 20190814 start --
    @Query(value = "SELECT EO.ORD_NO, O.LAST_UPD_DATE, O.ORD_DATE " +
            "FROM MOE_EHR_ORDER EO " +
            "INNER JOIN MOE_ORDER O ON EO.ORD_NO = O.ORD_NO " +
            "WHERE EO.MOE_PATIENT_KEY =?2 " +
            "AND EO.MOE_CASE_NO =?3 " +
            "AND O.ORD_STATUS ='O' " +
            "AND O.HOSPCODE =?1 " +
            "ORDER BY O.ORD_DATE DESC, EO.ORD_NO DESC", nativeQuery = true)
    List<Object[]> findMoeOrderNoForLogin(String hospitalCd, String moePatientKey, String moeCaseNo);
    // Ricci 20190814 end --

    // Ricci 20190814 start --
    @Query(value = "SELECT EO.ORD_NO, O.LAST_UPD_DATE, O.ORD_DATE " +
            "FROM MOE_EHR_ORDER EO " +
            "INNER JOIN MOE_ORDER O ON EO.ORD_NO = O.ORD_NO " +
            "WHERE EO.MOE_PATIENT_KEY =?2 " +
            "AND EO.MOE_CASE_NO =?3 " +
            "AND O.ORD_STATUS ='O' " +
            "AND O.HOSPCODE =?1 " +
            "AND O.ORD_NO =?4 " +
            "ORDER BY O.ORD_DATE DESC, EO.ORD_NO DESC", nativeQuery = true)
    List<Object[]> findMoeOrderNoForLogin(String hospitalCd, String moePatientKey, String moeCaseNo, long orderNo);
    // Ricci 20190814 end --


    // Simon 20191105 start--

    @Query("select distinct o from MoeEhrOrderPo as o" +
            " join fetch o.moeOrder as o2" +
            " join fetch o2.moeMedProfiles as p" +
            " join fetch p.moeEhrMedProfile" +
            " left join fetch p.moeMedMultDoses as m" +
            " left join fetch p.moeEhrMedAllergens" +
            " left join fetch m.moeEhrMedMultDose" +
            " where o.hospcode = :hospcode and o.ordNo = :ordNo and o2.ordStatus = 'O'" +
            " order by o2.ordDate desc, o.ordNo desc")
    MoeEhrOrderPo findByHospCodeAndOrdNo(@Param("hospcode") String hospcode, @Param("ordNo") long ordNo);


    @Query("select distinct o from MoeEhrOrderPo as o" +
            " join fetch o.moeOrder as o2" +
            " where o.hospcode = :hospcode and o.moePatient.patientRefKey = :patientKey and o2.ordStatus = 'O'" +
            " and o2.ordDate >= :startDate and o2.ordDate <= :endDate " +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findAllByHospcodeAndPatientRefKeyAndOrdEndDate(@Param("hospcode") String hospcode,
                                                                       @Param("patientKey") String patientKey,
                                                                       @Param("startDate") Date startDate,
                                                                       @Param("endDate") Date endDate);

    @Query("select distinct o from MoeEhrOrderPo as o" +
            " join fetch o.moeOrder as o2" +
            " where o.hospcode = :hospcode and o.moePatient.patientRefKey = :patientKey and o2.ordStatus = 'O' " +
            " and o2.ordDate >= :startDate " +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findAllByHospcodeAndMoePatient(@Param("hospcode") String hospcode,
                                                       @Param("patientKey") String patientKey,
                                                       @Param("startDate") Date startDate);

    @Query("select distinct o from MoeEhrOrderPo as o" +
            " join fetch o.moeOrder as o2" +
            " where o.hospcode = :hospcode and o.moePatient.patientRefKey = :patientKey" +
            " and o2.prescType = :prescType and o2.ordStatus = 'O'" +
            " and o2.ordDate >= :startDate and o2.ordDate <= :endDate " +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findAllByHospcodeAndPatientRefKeyAndPrescTypeAndOrdEndDate(@Param("hospcode") String hospcode,
                                                                                   @Param("patientKey") String patientKey,
                                                                                   @Param("prescType") String prescType,
                                                                                   @Param("startDate") Date startDate,
                                                                                   @Param("endDate") Date endDate);

    @Query("select distinct o from MoeEhrOrderPo as o" +
            " join fetch o.moeOrder as o2" +
            " where o.hospcode = :hospcode and o.moePatient.patientRefKey = :patientKey " +
            " and o2.prescType = :prescType and o2.ordStatus = 'O'" +
            " and o2.ordDate >= :startDate " +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findAllByHospcodeAndPatientRefKeyAndPrescType(@Param("hospcode") String hospcode,
                                                                      @Param("patientKey") String patientKey,
                                                                      @Param("prescType") String prescType,
                                                                      @Param("startDate") Date startDate);


    @Query("select distinct o from MoeEhrOrderPo as o" +
            " join fetch o.moeOrder as o2" +
            " where o.hospcode = :hospcode and o.moePatient.patientRefKey = :patientRefKey " +
            " and o.moePatientCase.caseRefNo = :episodeNo and o2.ordStatus = 'O'" +
            " and o2.ordDate >= :startDate and o2.ordDate <= :endDate " +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findAllByEpisodeNoAndOrderEndDate(@Param("hospcode") String hospcode,
                                                          @Param("patientRefKey") String patientRefKey,
                                                          @Param("episodeNo") String episodeNo,
                                                          @Param("startDate") Date startDate,
                                                          @Param("endDate") Date endDate);


    @Query("select distinct o from MoeEhrOrderPo as o" +
            " join fetch o.moeOrder as o2" +
            " where o.hospcode = :hospcode and o.moePatient.patientRefKey = :patientRefKey" +
            " and o.moePatientCase.caseRefNo = :episodeNo and o2.ordStatus = 'O'" +
            " and o2.ordDate >= :startDate " +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findAllByEpisodeNo(@Param("hospcode") String hospcode,
                                           @Param("patientRefKey") String patientRefKey,
                                           @Param("episodeNo") String episodeNo,
                                           @Param("startDate") Date startDate);

    @Query("select distinct o from MoeEhrOrderPo as o" +
            " join fetch o.moeOrder as o2" +
            " where o.hospcode = :hospcode and o.moePatient.patientRefKey = :patientRefKey" +
            " and o.moePatientCase.caseRefNo = :episodeNo" +
            " and o2.prescType = :prescType and o2.ordStatus = 'O'" +
            " and o2.ordDate >= :startDate and o2.ordDate <= :endDate " +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findAllByEpisodeNoAndPrescTypeAndOrdEndDate(@Param("hospcode") String hospcode,
                                                                    @Param("patientRefKey") String patientRefKey,
                                                                    @Param("episodeNo") String episodeNo,
                                                                    @Param("prescType") String prescType,
                                                                    @Param("startDate") Date startDate,
                                                                    @Param("endDate") Date endDate);


    @Query("select distinct o from MoeEhrOrderPo as o" +
            " join fetch o.moeOrder as o2" +
            " where o.hospcode = :hospcode and o.moePatient.patientRefKey = :patientRefKey" +
            " and o.moePatientCase.caseRefNo = :episodeNo and o2.prescType = :prescType and o2.ordStatus = 'O'" +
            " and o2.ordDate >= :startDate " +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findAllByEpisodeNoAndPrescType(@Param("hospcode") String hospcode,
                                                       @Param("patientRefKey") String patientRefKey,
                                                       @Param("episodeNo") String episodeNo,
                                                       @Param("prescType") String prescType,
                                                       @Param("startDate") Date startDate);


    @Query("select distinct o from MoeEhrOrderPo as o" +
            " join fetch o.moeOrder as o2" +
            " where o.hospcode = :hospcode and o.moePatient.patientRefKey = :patientRefKey" +
            " and o.moePatientCase.caseRefNo = :caseRefNo and o2.ordStatus = 'O'" +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findPatientOrderDetail(@Param("hospcode") String hospcode,
                                               @Param("patientRefKey") String patientRefKey,
                                               @Param("caseRefNo") String caseRefNo);

    @Query("select distinct o from MoeEhrOrderPo as o" +
            " join fetch o.moeOrder as o2" +
            " where o.hospcode = :hospcode and o.moePatient.patientRefKey = :patientRefKey " +
            " and o.moePatientCase.caseRefNo = :caseRefNo and" +
            " o2.prescType = :prescType and o2.ordStatus = 'O'" +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findPatientOrderDetailByPrescType(@Param("hospcode") String hospcode,
                                                          @Param("patientRefKey") String patientRefKey,
                                                          @Param("caseRefNo") String caseRefNo,
                                                          @Param("prescType") String prescType);

    @Query("select distinct o from MoeEhrOrderPo as o" +
            " join fetch o.moeOrder as o2" +
            " where o.createUserId = :createUserId" +
            " and o.hospcode = :hospcode " +
            " and o.moePatient.patientRefKey = :patientRefKey " +
            " and o.moePatientCase.caseRefNo = :caseRefNo and o2.ordStatus = 'O'" +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findPatientOrderDetailByLoginId(@Param("createUserId") String createUserId,
                                                        @Param("hospcode") String hospcode,
                                                        @Param("patientRefKey") String patientRefKey,
                                                        @Param("caseRefNo") String caseRefNo);

    @Query("select distinct o from MoeEhrOrderPo as o" +
            " join fetch o.moeOrder as o2" +
            " where o.createUserId = :createUserId" +
            " and o.hospcode = :hospcode and" +
            " o.moePatient.patientRefKey = :patientRefKey " +
            " and o.moePatientCase.caseRefNo = :caseRefNo and " +
            " o2.prescType = :prescType and o2.ordStatus = 'O'" +
            " order by o2.ordDate desc, o.ordNo desc")
    List<MoeEhrOrderPo> findPatientOrderDetailByLoginIdAndPrescType(@Param("createUserId") String createUserId,
                                                                    @Param("hospcode") String hospcode,
                                                                    @Param("patientRefKey") String patientRefKey,
                                                                    @Param("caseRefNo") String caseRefNo,
                                                                    @Param("prescType") String prescType);

    //Simon 20191105 end--
    @Query("select o from MoeEhrOrderPo as o" +
            " join fetch o.moeOrder as o2" +
            " join fetch o2.moeMedProfiles as p" +
            " join fetch p.moeEhrMedProfile" +
            " left join fetch p.moeMedMultDoses as m" +
            " left join fetch p.moeEhrMedAllergens" +
            " left join fetch m.moeEhrMedMultDose" +
            " where o.moePatient.patientRefKey = :patientRefKey and o.ordNo = :ordNo and o2.ordStatus = 'O'" +
            " order by o2.ordDate desc, o.ordNo desc")
    MoeEhrOrderPo findByPatientRefKeyAndOrderNo(@Param("patientRefKey") String patientRefKey,
                                                @Param("ordNo") Long ordNo);

}
