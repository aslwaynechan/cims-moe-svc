package hk.health.moe.filter;

/**************************************************************************
 * NAME        : HttpServletWrapperFilter.java
 * VERSION     : 1.0.0
 * DATE        : 03-JUL-2019
 * DESCRIPTION : Custom HttpServletRequest & HttpServletResponse
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       21-Oct-2019  Initial Version
 **************************************************************************/

import hk.health.moe.common.MoeAsync;
import hk.health.moe.common.MoeCommonHelper;
import hk.health.moe.pojo.bo.AspectLogBo;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HttpServletWrapperFilter implements Filter {

    @Autowired
    private MoeAsync moeAsync;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        ContentCachingRequestWrapper httpServletRequestWrapper = new ContentCachingRequestWrapper(httpServletRequest);
        ContentCachingResponseWrapper httpServletResponseWrapper = new ContentCachingResponseWrapper(httpServletResponse);

        // Set default is UTF-8
        if (StringUtils.isBlank(httpServletRequest.getCharacterEncoding())) {
            httpServletRequestWrapper.setCharacterEncoding("UTF-8");
            httpServletResponse.setCharacterEncoding("UTF-8");
        }

        StopWatch timer = new StopWatch();
        AspectLogBo logBo = new AspectLogBo();
        httpServletRequestWrapper.setAttribute("ASPECT_LOG", logBo);
        try {
            timer.start();
            chain.doFilter(httpServletRequestWrapper, httpServletResponseWrapper);
        } finally {
            MoeCommonHelper.buildAspectLog(logBo, httpServletRequestWrapper);
            MoeCommonHelper.buildAspectLog(logBo, timer, httpServletResponseWrapper);
            /**
             *@Description: Copy the complete cached body content to the response.
             */
            httpServletResponseWrapper.copyBodyToResponse();
            timer.stop();
            moeAsync.buildAspectLog(logBo);
        }
    }

    @Override
    public void destroy() {

    }
}




























