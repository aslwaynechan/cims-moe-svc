package hk.health.moe.pojo.bo;

public class ReportListItemBo {
	//--- common variable ---
	private int level;
	
	//--- level 1 ---
	private String labelName;
	
	//--- level 2 ---
	private String drugName;
	private String drugNameDetail;
	//private String totalValue;
	//private String specialInstruction;
	
	//--- level 3 ---
	private String drugValue;
	private String drugValueDetail;
	
	//--- constructor ---
	public ReportListItemBo() {
	}
	
	public ReportListItemBo(int level, String labelName, String drugName,
                            String drugNameDetail, String drugValue,
                            String drugValueDetail) {
		super();
		this.level = level;
		this.labelName = labelName;
		this.drugName = drugName;
		this.drugNameDetail = drugNameDetail;
		//this.totalValue = totalValue;
		//this.specialInstruction = specialInstruction;
		this.drugValue = drugValue;
		this.drugValueDetail = drugValueDetail;
	}
	
	public ReportListItemBo(ReportListItemBo listItem) {
		this(listItem.getLevel(), listItem.getLabelName(), listItem.getDrugName(), listItem.getDrugNameDetail(), listItem.getDrugValue(), listItem.getDrugValueDetail());
	}

	//--- getter setter ---
	public int getLevel() {
		return level;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	public String getLabelName() {
		return labelName;
	}
	
	public void setLabelName(String labelName) {
		this.labelName = labelName;
	}
	
	public String getDrugName() {
		return drugName;
	}
	
	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}
	
	public String getDrugNameDetail() {
		return drugNameDetail;
	}
	
	public void setDrugNameDetail(String drugNameDetail) {
		this.drugNameDetail = drugNameDetail;
	}
	
	/*
	public String getTotalValue() {
		return totalValue;
	}
	
	public void setTotalValue(String totalValue) {
		this.totalValue = totalValue;
	}
	
	public String getSpecialInstruction() {
		return specialInstruction;
	}
	
	public void setSpecialInstruction(String specialInstruction) {
		this.specialInstruction = specialInstruction;
	}
	*/
	
	public String getDrugValue() {
		return drugValue;
	}
	
	public void setDrugValue(String drugValue) {
		this.drugValue = drugValue;
	}
	
	public String getDrugValueDetail() {
		return drugValueDetail;
	}
	
	public void setDrugValueDetail(String drugValueDetail) {
		this.drugValueDetail = drugValueDetail;
	}
	
}
