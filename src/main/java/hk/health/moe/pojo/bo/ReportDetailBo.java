package hk.health.moe.pojo.bo;

import com.fasterxml.jackson.annotation.JsonInclude;
import hk.health.moe.pojo.po.MoeEhrRxImageLogPo;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReportDetailBo {

    private String dischargeReportId;
    private String dischargeReportImageId;
    private byte[] dischargeByte;
    private String purchaseReportId;
    private String purchaseReportImageId;
    private byte[] purchaseByte;
    private String directPrintFlag;
    private String dischargeBase64;
    private String purchaseBase64;
    private String reportBase64;
    private MoeEhrRxImageLogPo dischargeReportLog;
    private MoeEhrRxImageLogPo purchaseReportLog;

    public MoeEhrRxImageLogPo getDischargeReportLog() {
        return dischargeReportLog;
    }

    public void setDischargeReportLog(MoeEhrRxImageLogPo dischargeReportLog) {
        this.dischargeReportLog = dischargeReportLog;
    }

    public MoeEhrRxImageLogPo getPurchaseReportLog() {
        return purchaseReportLog;
    }

    public void setPurchaseReportLog(MoeEhrRxImageLogPo purchaseReportLog) {
        this.purchaseReportLog = purchaseReportLog;
    }

    public String getReportBase64() {
        return reportBase64;
    }

    public void setReportBase64(String reportBase64) {
        this.reportBase64 = reportBase64;
    }

    public String getDischargeBase64() {
        return dischargeBase64;
    }

    public void setDischargeBase64(String dischargeBase64) {
        this.dischargeBase64 = dischargeBase64;
    }

    public String getPurchaseBase64() {
        return purchaseBase64;
    }

    public void setPurchaseBase64(String purchaseBase64) {
        this.purchaseBase64 = purchaseBase64;
    }

    public String getDischargeReportId() {
        return dischargeReportId;
    }

    public void setDischargeReportId(String dischargeReportId) {
        this.dischargeReportId = dischargeReportId;
    }

    public byte[] getDischargeByte() {
        return dischargeByte;
    }

    public void setDischargeByte(byte[] dischargeByte) {
        this.dischargeByte = dischargeByte;
    }

    public String getPurchaseReportId() {
        return purchaseReportId;
    }

    public void setPurchaseReportId(String purchaseReportId) {
        this.purchaseReportId = purchaseReportId;
    }

    public byte[] getPurchaseByte() {
        return purchaseByte;
    }

    public void setPurchaseByte(byte[] purchaseByte) {
        this.purchaseByte = purchaseByte;
    }

    public String getDirectPrintFlag() {
        return directPrintFlag;
    }

    public void setDirectPrintFlag(String directPrintFlag) {
        this.directPrintFlag = directPrintFlag;
    }

    public String getDischargeReportImageId() {
        return dischargeReportImageId;
    }

    public void setDischargeReportImageId(String dischargeReportImageId) {
        this.dischargeReportImageId = dischargeReportImageId;
    }

    public String getPurchaseReportImageId() {
        return purchaseReportImageId;
    }

    public void setPurchaseReportImageId(String purchaseReportImageId) {
        this.purchaseReportImageId = purchaseReportImageId;
    }
}
