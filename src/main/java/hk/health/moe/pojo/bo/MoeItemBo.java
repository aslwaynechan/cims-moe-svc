package hk.health.moe.pojo.bo;

import java.io.Serializable;
import java.util.List;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

@SuppressWarnings("serial")
public class MoeItemBo implements Serializable/*, IsSerializable*/ {
	String labelName;
	List<SubItemBo> subItemList;
	
	public MoeItemBo() {
	}
	
	public String getLabelName() {
		return labelName;
	}
	public void setLabelName(String labelName) {
		this.labelName = labelName;
	}
	public List<SubItemBo> getSubItemList() {
		return subItemList;
	}
	public void setSubItemList(List<SubItemBo> subItemList) {
		this.subItemList = subItemList;
	}
	
}
