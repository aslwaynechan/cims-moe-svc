package hk.health.moe.pojo.bo;

import java.util.ArrayList;
import java.util.List;

public class PrescriptionDrugDosageListItemBo {
	private List<ChunkBo> drugDosageList;
	private List<ChunkBo> drugFreqList;
	private List<ChunkBo> drugExtInfoList;
	
	public List<ChunkBo> getDrugDosageList() {
		if(drugDosageList == null) {
			drugDosageList = new ArrayList<ChunkBo>();
		}
		
		return drugDosageList;
	}
	public void setDrugDosageList(List<ChunkBo> drugDosageList) {
		this.drugDosageList = drugDosageList;
	}
	public List<ChunkBo> getDrugFreqList() {
		if(drugFreqList == null) {
			drugFreqList = new ArrayList<ChunkBo>();
		}		
		
		return drugFreqList;
	}
	public void setDrugFreqList(List<ChunkBo> drugFreqList) {
		this.drugFreqList = drugFreqList;
	}
	public List<ChunkBo> getDrugExtInfoList() {
		if(drugExtInfoList == null) {
			drugExtInfoList = new ArrayList<ChunkBo>();
		}		
		
		return drugExtInfoList;
	}
	public void setDrugExtInfoList(List<ChunkBo> drugExtInfoList) {
		this.drugExtInfoList = drugExtInfoList;
	}	
}
