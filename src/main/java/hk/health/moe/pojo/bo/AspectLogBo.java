package hk.health.moe.pojo.bo;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Date;

public class AspectLogBo implements Serializable {

    private static final long serialVersionUID = 2142402155710244694L;
    private Date startDtm;
    private Date endDtm;
    private String requestUrl;
    private String requestBody;
    private String remoteIp;
    private String remoteBrowser;
    private String remoteOs;
    private String httpMethod;
    private String characterEncoding;
    private String functionName;
    private String serviceHostname;
    private String duration;
    private Integer responseStatusCode;
    private String responseBody;
    private String errorId;

    public String getErrorId() {
        return errorId;
    }

    public void setErrorId(String errorId) {
        this.errorId = errorId;
    }

    public Integer getResponseStatusCode() {
        return responseStatusCode;
    }

    public void setResponseStatusCode(Integer responseStatusCode) {
        this.responseStatusCode = responseStatusCode;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getServiceHostname() {
        return serviceHostname;
    }

    public void setServiceHostname(String serviceHostname) {
        this.serviceHostname = serviceHostname;
    }

    public Date getStartDtm() {
        return startDtm;
    }

    public void setStartDtm(Date startDtm) {
        this.startDtm = startDtm;
    }

    public Date getEndDtm() {
        return endDtm;
    }

    public void setEndDtm(Date endDtm) {
        this.endDtm = endDtm;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public String getRemoteIp() {
        return remoteIp;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }

    public String getRemoteBrowser() {
        return remoteBrowser;
    }

    public void setRemoteBrowser(String remoteBrowser) {
        this.remoteBrowser = remoteBrowser;
    }

    public String getRemoteOs() {
        return remoteOs;
    }

    public void setRemoteOs(String remoteOs) {
        this.remoteOs = remoteOs;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getCharacterEncoding() {
        return characterEncoding;
    }

    public void setCharacterEncoding(String characterEncoding) {
        this.characterEncoding = characterEncoding;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public String toLogString() {
        StringBuffer sb = new StringBuffer("\r\n");
        sb.append("#####################################################################################################################################").append("\r\n");
        sb.append("Start Time: ").append(startDtm).append("\r\n");
        sb.append("End Time: ").append(endDtm).append("\r\n");
        sb.append("Request URL: ").append(StringUtils.isNotEmpty(this.requestUrl) ? this.requestUrl : "").append("\r\n");
        sb.append("Request Body: ").append(StringUtils.isNotEmpty(this.requestBody) ? this.requestBody : "").append("\r\n");
        sb.append("Request IP Address: ").append(StringUtils.isNotEmpty(this.remoteIp) ? this.remoteIp : "").append("\r\n");
        sb.append("Request Browser: ").append(StringUtils.isNotEmpty(this.remoteBrowser) ? this.remoteBrowser : "").append("\r\n");
        sb.append("Request OS: ").append(StringUtils.isNotEmpty(this.remoteOs) ? this.remoteOs : "").append("\r\n");
        sb.append("Http Method: ").append(StringUtils.isNotEmpty(this.httpMethod) ? this.httpMethod : "").append("\r\n");
        sb.append("Character Encoding: ").append(StringUtils.isNotEmpty(this.characterEncoding) ? this.characterEncoding : "").append("\r\n");
        sb.append("API Function Name: ").append(StringUtils.isNotEmpty(this.functionName) ? this.functionName : "").append("\r\n");
        sb.append("Service Hostname: ").append(StringUtils.isNotEmpty(this.serviceHostname) ? this.serviceHostname : "").append("\r\n");
        sb.append("Duration: ").append(StringUtils.isNotEmpty(this.duration) ? this.duration : "").append("\r\n");
        sb.append("Response Status Code: ").append(responseStatusCode).append("\r\n");
        sb.append("Response Body: ").append(StringUtils.isNotEmpty(this.responseBody) ? this.responseBody : "").append("\r\n");
        sb.append("Exception Code: ").append(StringUtils.isNotEmpty(this.errorId) ? this.errorId : "").append("\r\n");

        return sb.toString();
    }
}
