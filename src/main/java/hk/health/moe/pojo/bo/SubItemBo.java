package hk.health.moe.pojo.bo;

import java.io.Serializable;
import java.util.List;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

@SuppressWarnings("serial")
public class SubItemBo implements Serializable/*, IsSerializable*/ {
	String drugName;
	String drugNameDetail;
	List<DrugQuantityBo> drugQuantityList;
	String totalValue;
	String specialInstruction;
	
	public SubItemBo() {
	}
	
	public String getDrugName() {
		return drugName;
	}
	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}
	public String getDrugNameDetail() {
		return drugNameDetail;
	}
	public void setDrugNameDetail(String drugNameDetail) {
		this.drugNameDetail = drugNameDetail;
	}
	public List<DrugQuantityBo> getDrugQuantityList() {
		return drugQuantityList;
	}
	public void setDrugQuantityList(List<DrugQuantityBo> drugQuantityList) {
		this.drugQuantityList = drugQuantityList;
	}
	public String getTotalValue() {
		return totalValue;
	}
	public void setTotalValue(String totalValue) {
		this.totalValue = totalValue;
	}
	public String getSpecialInstruction() {
		return specialInstruction;
	}
	public void setSpecialInstruction(String specialInstruction) {
		this.specialInstruction = specialInstruction;
	}
	
}
