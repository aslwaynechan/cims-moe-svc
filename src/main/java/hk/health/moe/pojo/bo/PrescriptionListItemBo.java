package hk.health.moe.pojo.bo;

import java.util.ArrayList;
import java.util.List;

public class PrescriptionListItemBo {
	private String actionStatus;
	
	private List<ChunkBo> drugNameList;
	private List<ChunkBo> drugNameDetailList;
	private List<ChunkBo> specInstructList;
	private List<ChunkBo> remarkList;
	private List<ChunkBo> discontinuationList;
	
	private Boolean withOverridingReason = false;

	private List<PrescriptionDrugDosageListItemBo> prescriptionDrugDosageList;
	
	private Boolean isDangerDrug = false;
	
	private Boolean isFreeText = false;
	
	public PrescriptionListItemBo() {
		
	}

	public PrescriptionListItemBo(String actionStatus, List<ChunkBo> drugNameList, List<ChunkBo> drugNameDetailList, List<ChunkBo> specInstructList, Boolean withOverridingReason, List<PrescriptionDrugDosageListItemBo> prescriptionDrugDosageList, Boolean isDangerDrug, Boolean isFreeText) {
		super();
		this.actionStatus = actionStatus;
		this.drugNameList = drugNameList;
		this.drugNameDetailList = drugNameDetailList;
		this.specInstructList = specInstructList;
		this.withOverridingReason = withOverridingReason;
		this.prescriptionDrugDosageList = prescriptionDrugDosageList;
		this.isDangerDrug = isDangerDrug;
		this.isFreeText = isFreeText;
	}

	public PrescriptionListItemBo(PrescriptionListItemBo listItem) {
		this(listItem.getActionStatus(), listItem.getDrugNameList(), listItem.getDrugNameDetailList(), listItem.getSpecInstructList(), listItem.getWithOverridingReason(), listItem.getPrescriptionDrugDosageList(), listItem.getIsDangerDrug(), listItem.getIsFreeText());
	}

	public String getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(String actionStatus) {
		this.actionStatus = actionStatus;
	}

	public List<ChunkBo> getDrugNameList() {
		if(drugNameList == null) {
			drugNameList = new ArrayList<ChunkBo>();
		}
		
		return drugNameList;
	}

	public void setDrugNameList(List<ChunkBo> drugNameList) {
		this.drugNameList = drugNameList;
	}

	public List<ChunkBo> getDrugNameDetailList() {
		if(drugNameDetailList == null) {
			drugNameDetailList = new ArrayList<ChunkBo>();
		}
		
		return drugNameDetailList;
	}

	public void setDrugNameDetailList(List<ChunkBo> drugNameDetailList) {
		this.drugNameDetailList = drugNameDetailList;
	}

	public List<ChunkBo> getSpecInstructList() {
		if(specInstructList == null) {
			specInstructList = new ArrayList<ChunkBo>();
		}
		
		return specInstructList;
	}

	public void setSpecInstructList(List<ChunkBo> specInstructList) {
		this.specInstructList = specInstructList;
	}

	public List<ChunkBo> getRemarkList() {
		if(remarkList == null) {
			remarkList = new ArrayList<ChunkBo>();
		}
		
		return remarkList;
	}

	public void setRemarkList(List<ChunkBo> remarkList) {
		this.remarkList = remarkList;
	}

	public List<ChunkBo> getDiscontinuationList() {
		if(discontinuationList == null) {
			discontinuationList = new ArrayList<ChunkBo>();
		}
		
		return discontinuationList;
	}

	public void setDiscontinuationList(List<ChunkBo> discontinuationList) {
		this.discontinuationList = discontinuationList;
	}

	public Boolean getWithOverridingReason() {
		return withOverridingReason;
	}

	public void setWithOverridingReason(Boolean withOverridingReason) {
		this.withOverridingReason = withOverridingReason;
	}
	
	public List<PrescriptionDrugDosageListItemBo> getPrescriptionDrugDosageList() {
		if(prescriptionDrugDosageList == null) {
			prescriptionDrugDosageList = new ArrayList<PrescriptionDrugDosageListItemBo>();
		}
		
		return prescriptionDrugDosageList;
	}

	public void setPrescriptionDrugDosageList(
			List<PrescriptionDrugDosageListItemBo> prescriptionDrugDosageList) {
		this.prescriptionDrugDosageList = prescriptionDrugDosageList;
	}

	public PrescriptionDrugDosageListItemBo getLastPrescriptionDrugDosageListItem() {
		PrescriptionDrugDosageListItemBo drugDosageListItem = null;
		
		if(prescriptionDrugDosageList != null && !prescriptionDrugDosageList.isEmpty()) {
			drugDosageListItem = prescriptionDrugDosageList.get(prescriptionDrugDosageList.size() - 1);
		}
		
		return drugDosageListItem;
	}
	
	public Boolean getIsDangerDrug() {
		return isDangerDrug;
	}

	public void setIsDangerDrug(Boolean isDangerDrug) {
		this.isDangerDrug = isDangerDrug;
	}

	public Boolean getIsFreeText() {
		return isFreeText;
	}

	public void setIsFreeText(Boolean isFreeText) {
		this.isFreeText = isFreeText;
	}	
}
