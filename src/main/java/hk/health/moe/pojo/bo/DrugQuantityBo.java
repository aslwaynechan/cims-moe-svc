package hk.health.moe.pojo.bo;

import java.io.Serializable;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

@SuppressWarnings("serial")
public class DrugQuantityBo implements Serializable/*, IsSerializable*/ {
	String drugValue;
	String drugValueDetail;
	
	public DrugQuantityBo() {
	}
	
	public String getDrugValue() {
		return drugValue;
	}
	public void setDrugValue(String drugValue) {
		this.drugValue = drugValue;
	}
	public String getDrugValueDetail() {
		return drugValueDetail;
	}
	public void setDrugValueDetail(String drugValueDetail) {
		this.drugValueDetail = drugValueDetail;
	}
	
}
