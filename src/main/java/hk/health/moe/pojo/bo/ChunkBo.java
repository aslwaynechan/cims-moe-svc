/*********************************************************************
* Copyright (c) eHR HK Limited 2013. 
* All Rights Reserved.
* 
*********************************************************************
* PROJECT NAME    :  eHR Adaptation
*
* MODULE NAME     :  Medication Order Entry (MOE)
*
* PROGRAM NAME    :  Chunk.java
*
* PURPOSE         :  Object for report printing
*
**********************************************************************
* VERSION         :   1.01.001
* REF NO          :  
* DATE CREATED    :   27 July 2015
* 
*********************************************************************
*
* CHANGES         :
* REF NO           DATE        DETAIL
* ------           ----        ------
*
*********************************************************************
*/
package hk.health.moe.pojo.bo;

public class ChunkBo {
	private Boolean isBold = false;
	private Boolean isUnderline = false;
	private Boolean isItalic = false;
	private Boolean isStrikethru = false;
	
	private String content;

	public ChunkBo(Boolean isBold, Boolean isUnderline, Boolean isItalic, String content) {
		this.isBold = isBold;
		this.isUnderline = isUnderline;
		this.isItalic = isItalic;
		
		this.content = content;
	}
	
	public Boolean getIsBold() {
		return isBold;
	}

	public void setIsBold(Boolean isBold) {
		this.isBold = isBold;
	}

	public Boolean getIsUnderline() {
		return isUnderline;
	}

	public void setIsUnderline(Boolean isUnderline) {
		this.isUnderline = isUnderline;
	}

	public Boolean getIsItalic() {
		return isItalic;
	}

	public void setIsItalic(Boolean isItalic) {
		this.isItalic = isItalic;
	}

	public Boolean getIsStrikethru() {
		return isStrikethru;
	}

	public void setIsStrikethru(Boolean isStrikethru) {
		this.isStrikethru = isStrikethru;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}		
}
