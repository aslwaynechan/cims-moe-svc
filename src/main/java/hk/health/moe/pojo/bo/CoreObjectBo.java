package hk.health.moe.pojo.bo;

import java.io.Serializable;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

@SuppressWarnings("serial")
public class CoreObjectBo implements Serializable/*, IsSerializable*/ {
    String hkic;
    String hospitalCode;
    Long orderNo;
    String directPrintFlag;
    boolean isReprint;

    public CoreObjectBo() {
        super();
    }

    public String getHkic() {
        return hkic;
    }

    public void setHkic(String hkic) {
        this.hkic = hkic;
    }

    public String getHospitalCode() {
        return hospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
    }

    public Long getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Long orderNo) {
        this.orderNo = orderNo;
    }

    public String getDirectPrintFlag() {
        return directPrintFlag;
    }

    public void setDirectPrintFlag(String directPrintFlag) {
        this.directPrintFlag = directPrintFlag;
    }

    public boolean isReprint() {
        return isReprint;
    }

    public void setReprint(boolean isReprint) {
        this.isReprint = isReprint;
    }

}
