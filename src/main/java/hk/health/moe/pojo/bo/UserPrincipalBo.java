/*
package hk.health.moe.pojo.bo;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Objects;

*/
/**
 * Security的封装对象
 *
 * @author jules
 * @date 2019/7/1
 *//*

public class UserPrincipalBo implements UserDetails {
    private static final long serialVersionUID = 3931777102284307739L;

    private Integer userId;

    private String loginName;

    private Collection<? extends GrantedAuthority> authorities;

    public UserPrincipalBo(Integer userId, String loginName, Collection<? extends GrantedAuthority> authorities) {
        this.userId = userId;
        this.loginName = loginName;
        this.authorities = authorities;
    }

    */
/**
     * 保存User的Access Right权限
     *//*

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    */
/**
     * User是否过期
     *//*

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    */
/**
     * User是否被锁定
     *//*

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    */
/**
     * 验证信息是否过期
     *//*

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    */
/**
     * User是否可以使用
     *//*

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        String username = loginName;
        return username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserPrincipal that = (UserPrincipal) o;
        return Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(userId,loginName);
    }
}
*/
