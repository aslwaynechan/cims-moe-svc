package hk.health.moe.pojo.bo;

import java.awt.Image;
import java.util.List;

public class ReportDataValueBo {

    private Image hospitalIcon;
    private Image ordNoBarcode128Icon;
    private Image horizontal128Icon;
    private Image hkicBarcode128Icon;
    private Image verticalBarcode128Icon;
    private Image paperClipIcon;

    private String prescType;
    private String refNo;
    private String medicalRecordNumber;

    private String orderDate;

    private String communityFlag;

    private String hospitalChineseName;
    private String hospitalEnglishName;
    private String hospitalChineseAddress;
    private String hospitalEnglishAddress;
    private String hospitalPhoneNumber;
    private String hospitalFaxNumber;
    private String hospitalWebAddress;
    private String prescriptionCutsomLine1;
    private String prescriptionCutsomLine2;

    private String hospitalEmailAddress;
    private String patientEnglishName;
    private String patientChineseName;
    private String patientAddress;
    private String dateOfBirth;

    private String sex;
    private String docTypeLabel;
    private String hkic;
    private String docNumWithoutType;
    private String ward;
    private String age;
    private String patientStatus;

    private String drugAllergyItem;
    private String adverseDrugReactionItem;
    private String alertItem;

    private List<ReportListItemBo> moeHospitalItemList;
    private List<ReportListItemBo> moeCommunityItemList;
    private List<PrescriptionListItemBo> prescriptionItemList;
    private List<PrescriptionListItemBo> purchaseInCommunityItemList;

    private String doctorName;
    private String remarkBy;
    private String firstPrintDate;
    private String lastPrintDate;
    private String ordNo;

    private String verticalBarcodeLabel;
    private String horizontalBarcodeLabel;
    private String ordNoLabel;

    private String purchasePrescriptionEncounterNo;
    private String purchasePrescriptionOrderNo;
    private String purchaseCaseNo;

    private String templatePath;

    // add reprint flag
    private String reprintFlag; // to determine if reprint watermark is needed
    private String patientPhoneNo;
    private String weight;

    private String drugIngredientMoreThanThreeMsg;
    private String drugIngredientPurchaseMoreThanThreeMsg;

    private boolean failToCallSaam;

    private String addressContainsChineseChr = "N";

    private String purchaseInCommunityRxBottomStatement;

    private String enableDentalDisclaimer;
    private boolean hasDischargeDangerDrug;
    private boolean hasDischargeNonDangerDrug;
    private boolean hasPurchaseDangerDrug;
    private boolean hasPurchaseNonDangerDrug;
    private boolean hasDischargeFreeTextDrug;
    private boolean hasPurchaseFreeTextDrug;

    public Image getHospitalIcon() {
        return hospitalIcon;
    }

    public void setHospitalIcon(Image hospitalIcon) {
        this.hospitalIcon = hospitalIcon;
    }

    public Image getOrdNoBarcode128Icon() {
        return ordNoBarcode128Icon;
    }

    public void setOrdNoBarcode128Icon(Image ordNoBarcode128Icon) {
        this.ordNoBarcode128Icon = ordNoBarcode128Icon;
    }

    public Image getHorizontal128Icon() {
        return horizontal128Icon;
    }

    public void setHorizontal128Icon(Image horizontal128Icon) {
        this.horizontal128Icon = horizontal128Icon;
    }

    public Image getHkicBarcode128Icon() {
        return hkicBarcode128Icon;
    }

    public void setHkicBarcode128Icon(Image hkicBarcode128Icon) {
        this.hkicBarcode128Icon = hkicBarcode128Icon;
    }

    public Image getVerticalBarcode128Icon() {
        return verticalBarcode128Icon;
    }

    public void setVerticalBarcode128Icon(Image verticalBarcode128Icon) {
        this.verticalBarcode128Icon = verticalBarcode128Icon;
    }

    public Image getPaperClipIcon() {
        return paperClipIcon;
    }

    public void setPaperClipIcon(Image paperClipIcon) {
        this.paperClipIcon = paperClipIcon;
    }

    public String getPrescType() {
        return prescType;
    }

    public void setPrescType(String prescType) {
        this.prescType = prescType;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getMedicalRecordNumber() {
        return medicalRecordNumber;
    }

    public void setMedicalRecordNumber(String medicalRecordNumber) {
        this.medicalRecordNumber = medicalRecordNumber;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getCommunityFlag() {
        return communityFlag;
    }

    public void setCommunityFlag(String communityFlag) {
        this.communityFlag = communityFlag;
    }

    public String getHospitalChineseName() {
        return hospitalChineseName;
    }

    public void setHospitalChineseName(String hospitalChineseName) {
        this.hospitalChineseName = hospitalChineseName;
    }

    public String getHospitalEnglishName() {
        return hospitalEnglishName;
    }

    public void setHospitalEnglishName(String hospitalEnglishName) {
        this.hospitalEnglishName = hospitalEnglishName;
    }

    public String getHospitalChineseAddress() {
        return hospitalChineseAddress;
    }

    public void setHospitalChineseAddress(String hospitalChineseAddress) {
        this.hospitalChineseAddress = hospitalChineseAddress;
    }

    public String getHospitalEnglishAddress() {
        return hospitalEnglishAddress;
    }

    public void setHospitalEnglishAddress(String hospitalEnglishAddress) {
        this.hospitalEnglishAddress = hospitalEnglishAddress;
    }

    public String getHospitalPhoneNumber() {
        return hospitalPhoneNumber;
    }

    public void setHospitalPhoneNumber(String hospitalPhoneNumber) {
        this.hospitalPhoneNumber = hospitalPhoneNumber;
    }

    public String getHospitalFaxNumber() {
        return hospitalFaxNumber;
    }

    public void setHospitalFaxNumber(String hospitalFaxNumber) {
        this.hospitalFaxNumber = hospitalFaxNumber;
    }

    public String getHospitalWebAddress() {
        return hospitalWebAddress;
    }

    public void setHospitalWebAddress(String hospitalWebAddress) {
        this.hospitalWebAddress = hospitalWebAddress;
    }

    public String getPrescriptionCutsomLine1() {
        return prescriptionCutsomLine1;
    }

    public void setPrescriptionCutsomLine1(String prescriptionCutsomLine1) {
        this.prescriptionCutsomLine1 = prescriptionCutsomLine1;
    }

    public String getPrescriptionCutsomLine2() {
        return prescriptionCutsomLine2;
    }

    public void setPrescriptionCutsomLine2(String prescriptionCutsomLine2) {
        this.prescriptionCutsomLine2 = prescriptionCutsomLine2;
    }

    public String getHospitalEmailAddress() {
        return hospitalEmailAddress;
    }

    public void setHospitalEmailAddress(String hospitalEmailAddress) {
        this.hospitalEmailAddress = hospitalEmailAddress;
    }

    public String getPatientEnglishName() {
        return patientEnglishName;
    }

    public void setPatientEnglishName(String patientEnglishName) {
        this.patientEnglishName = patientEnglishName;
    }

    public String getPatientChineseName() {
        return patientChineseName;
    }

    public void setPatientChineseName(String patientChineseName) {
        this.patientChineseName = patientChineseName;
    }

    public String getPatientAddress() {
        return patientAddress;
    }

    public void setPatientAddress(String patientAddress) {
        this.patientAddress = patientAddress;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDocTypeLabel() {
        return docTypeLabel;
    }

    public void setDocTypeLabel(String docTypeLabel) {
        this.docTypeLabel = docTypeLabel;
    }

    public String getHkic() {
        return hkic;
    }

    public void setHkic(String hkic) {
        this.hkic = hkic;
    }

    public String getDocNumWithoutType() {
        return docNumWithoutType;
    }

    public void setDocNumWithoutType(String docNumWithoutType) {
        this.docNumWithoutType = docNumWithoutType;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPatientStatus() {
        return patientStatus;
    }

    public void setPatientStatus(String patientStatus) {
        this.patientStatus = patientStatus;
    }

    public String getDrugAllergyItem() {
        return drugAllergyItem;
    }

    public void setDrugAllergyItem(String drugAllergyItem) {
        this.drugAllergyItem = drugAllergyItem;
    }

    public String getAdverseDrugReactionItem() {
        return adverseDrugReactionItem;
    }

    public void setAdverseDrugReactionItem(String adverseDrugReactionItem) {
        this.adverseDrugReactionItem = adverseDrugReactionItem;
    }

    public String getAlertItem() {
        return alertItem;
    }

    public void setAlertItem(String alertItem) {
        this.alertItem = alertItem;
    }

    public List<ReportListItemBo> getMoeHospitalItemList() {
        return moeHospitalItemList;
    }

    public void setMoeHospitalItemList(List<ReportListItemBo> moeHospitalItemList) {
        this.moeHospitalItemList = moeHospitalItemList;
    }

    public List<ReportListItemBo> getMoeCommunityItemList() {
        return moeCommunityItemList;
    }

    public void setMoeCommunityItemList(List<ReportListItemBo> moeCommunityItemList) {
        this.moeCommunityItemList = moeCommunityItemList;
    }

    public List<PrescriptionListItemBo> getPrescriptionItemList() {
        return prescriptionItemList;
    }

    public void setPrescriptionItemList(List<PrescriptionListItemBo> prescriptionItemList) {
        this.prescriptionItemList = prescriptionItemList;
    }

    public List<PrescriptionListItemBo> getPurchaseInCommunityItemList() {
        return purchaseInCommunityItemList;
    }

    public void setPurchaseInCommunityItemList(List<PrescriptionListItemBo> purchaseInCommunityItemList) {
        this.purchaseInCommunityItemList = purchaseInCommunityItemList;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getRemarkBy() {
        return remarkBy;
    }

    public void setRemarkBy(String remarkBy) {
        this.remarkBy = remarkBy;
    }

    public String getFirstPrintDate() {
        return firstPrintDate;
    }

    public void setFirstPrintDate(String firstPrintDate) {
        this.firstPrintDate = firstPrintDate;
    }

    public String getLastPrintDate() {
        return lastPrintDate;
    }

    public void setLastPrintDate(String lastPrintDate) {
        this.lastPrintDate = lastPrintDate;
    }

    public String getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(String ordNo) {
        this.ordNo = ordNo;
    }

    public String getVerticalBarcodeLabel() {
        return verticalBarcodeLabel;
    }

    public void setVerticalBarcodeLabel(String verticalBarcodeLabel) {
        this.verticalBarcodeLabel = verticalBarcodeLabel;
    }

    public String getHorizontalBarcodeLabel() {
        return horizontalBarcodeLabel;
    }

    public void setHorizontalBarcodeLabel(String horizontalBarcodeLabel) {
        this.horizontalBarcodeLabel = horizontalBarcodeLabel;
    }

    public String getOrdNoLabel() {
        return ordNoLabel;
    }

    public void setOrdNoLabel(String ordNoLabel) {
        this.ordNoLabel = ordNoLabel;
    }

    public String getPurchasePrescriptionEncounterNo() {
        return purchasePrescriptionEncounterNo;
    }

    public void setPurchasePrescriptionEncounterNo(String purchasePrescriptionEncounterNo) {
        this.purchasePrescriptionEncounterNo = purchasePrescriptionEncounterNo;
    }

    public String getPurchasePrescriptionOrderNo() {
        return purchasePrescriptionOrderNo;
    }

    public void setPurchasePrescriptionOrderNo(String purchasePrescriptionOrderNo) {
        this.purchasePrescriptionOrderNo = purchasePrescriptionOrderNo;
    }

    public String getPurchaseCaseNo() {
        return purchaseCaseNo;
    }

    public void setPurchaseCaseNo(String purchaseCaseNo) {
        this.purchaseCaseNo = purchaseCaseNo;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }

    public String getReprintFlag() {
        return reprintFlag;
    }

    public void setReprintFlag(String reprintFlag) {
        this.reprintFlag = reprintFlag;
    }

    public String getPatientPhoneNo() {
        return patientPhoneNo;
    }

    public void setPatientPhoneNo(String patientPhoneNo) {
        this.patientPhoneNo = patientPhoneNo;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getDrugIngredientMoreThanThreeMsg() {
        return drugIngredientMoreThanThreeMsg;
    }

    public void setDrugIngredientMoreThanThreeMsg(String drugIngredientMoreThanThreeMsg) {
        this.drugIngredientMoreThanThreeMsg = drugIngredientMoreThanThreeMsg;
    }

    public String getDrugIngredientPurchaseMoreThanThreeMsg() {
        return drugIngredientPurchaseMoreThanThreeMsg;
    }

    public void setDrugIngredientPurchaseMoreThanThreeMsg(String drugIngredientPurchaseMoreThanThreeMsg) {
        this.drugIngredientPurchaseMoreThanThreeMsg = drugIngredientPurchaseMoreThanThreeMsg;
    }

    public boolean isFailToCallSaam() {
        return failToCallSaam;
    }

    public void setFailToCallSaam(boolean failToCallSaam) {
        this.failToCallSaam = failToCallSaam;
    }

    public String getAddressContainsChineseChr() {
        return addressContainsChineseChr;
    }

    public void setAddressContainsChineseChr(String addressContainsChineseChr) {
        this.addressContainsChineseChr = addressContainsChineseChr;
    }

    public String getPurchaseInCommunityRxBottomStatement() {
        return purchaseInCommunityRxBottomStatement;
    }

    public void setPurchaseInCommunityRxBottomStatement(String purchaseInCommunityRxBottomStatement) {
        this.purchaseInCommunityRxBottomStatement = purchaseInCommunityRxBottomStatement;
    }

    public String getEnableDentalDisclaimer() {
        return enableDentalDisclaimer;
    }

    public void setEnableDentalDisclaimer(String enableDentalDisclaimer) {
        this.enableDentalDisclaimer = enableDentalDisclaimer;
    }

    public boolean isHasDischargeDangerDrug() {
        return hasDischargeDangerDrug;
    }

    public void setHasDischargeDangerDrug(boolean hasDischargeDangerDrug) {
        this.hasDischargeDangerDrug = hasDischargeDangerDrug;
    }

    public boolean isHasDischargeNonDangerDrug() {
        return hasDischargeNonDangerDrug;
    }

    public void setHasDischargeNonDangerDrug(boolean hasDischargeNonDangerDrug) {
        this.hasDischargeNonDangerDrug = hasDischargeNonDangerDrug;
    }

    public boolean isHasPurchaseDangerDrug() {
        return hasPurchaseDangerDrug;
    }

    public void setHasPurchaseDangerDrug(boolean hasPurchaseDangerDrug) {
        this.hasPurchaseDangerDrug = hasPurchaseDangerDrug;
    }

    public boolean isHasPurchaseNonDangerDrug() {
        return hasPurchaseNonDangerDrug;
    }

    public void setHasPurchaseNonDangerDrug(boolean hasPurchaseNonDangerDrug) {
        this.hasPurchaseNonDangerDrug = hasPurchaseNonDangerDrug;
    }

    public boolean isHasDischargeFreeTextDrug() {
        return hasDischargeFreeTextDrug;
    }

    public void setHasDischargeFreeTextDrug(boolean hasDischargeFreeTextDrug) {
        this.hasDischargeFreeTextDrug = hasDischargeFreeTextDrug;
    }

    public boolean isHasPurchaseFreeTextDrug() {
        return hasPurchaseFreeTextDrug;
    }

    public void setHasPurchaseFreeTextDrug(boolean hasPurchaseFreeTextDrug) {
        this.hasPurchaseFreeTextDrug = hasPurchaseFreeTextDrug;
    }
}
