package hk.health.moe.pojo.vo;

/**************************************************************************
 * NAME        : CimsResponseVo.java
 * VERSION     : 1.0.0
 * DATE        : 03-JUL-2019
 * DESCRIPTION : Unified-return object
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       03-JUL-2019  Initial Version
 **************************************************************************/

import io.swagger.annotations.ApiModelProperty;

public class CimsResponseVo<T> {

    @ApiModelProperty(required = true)
    private Integer respCode;

    @ApiModelProperty(notes = "only return when respCode = 1")
    private String errMsg;

    private T data;

    public int getRespCode() {
        return respCode;
    }

    public void setRespCode(int respCode) {
        this.respCode = respCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
