package hk.health.moe.pojo.dto.inner;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author Chris
 * @version 1.0
 * @date 2019/08/29
 */
public class InnerGetDrugByNameDto implements Serializable {

    private static final long serialVersionUID = -9174448269276010566L;

    @NotBlank
    String amp;

    public String getAmp() {
        return amp;
    }

    public void setAmp(String amp) {
        this.amp = amp;
    }

}
