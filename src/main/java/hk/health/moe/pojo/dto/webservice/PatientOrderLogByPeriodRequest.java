
package hk.health.moe.pojo.dto.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 *
 * <pre>
 * &lt;complexType name="PatientOrderLogByPeriodRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="patientKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="startTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="endTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="uploadMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PatientOrderLogByPeriodRequest", namespace = "http://ehr.gov.hk/moe/ws/beans", propOrder = {
    "patientKey",
    "startTime",
    "endTime",
    "uploadMode"
})
public class PatientOrderLogByPeriodRequest {

    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    protected String patientKey;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startTime;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endTime;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    protected String uploadMode;


    public String getPatientKey() {
        return patientKey;
    }


    public void setPatientKey(String value) {
        this.patientKey = value;
    }


    public XMLGregorianCalendar getStartTime() {
        return startTime;
    }


    public void setStartTime(XMLGregorianCalendar value) {
        this.startTime = value;
    }


    public XMLGregorianCalendar getEndTime() {
        return endTime;
    }


    public void setEndTime(XMLGregorianCalendar value) {
        this.endTime = value;
    }


    public String getUploadMode() {
        return uploadMode;
    }


    public void setUploadMode(String value) {
        this.uploadMode = value;
    }

}
