package hk.health.moe.pojo.dto.inner;

import hk.health.moe.pojo.dto.MoeDrugBySpecialtyDto;

import java.io.Serializable;

public class InnerSaveDrugBySpecialtyDto implements Serializable {
    private static final long serialVersionUID = -2208326556476313542L;

   private MoeDrugBySpecialtyDto moeDrugBySpecialty;
   private String actionType;

    public MoeDrugBySpecialtyDto getMoeDrugBySpecialty() {
        return moeDrugBySpecialty;
    }

    public void setMoeDrugBySpecialty(MoeDrugBySpecialtyDto moeDrugBySpecialty) {
        this.moeDrugBySpecialty = moeDrugBySpecialty;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }
}
