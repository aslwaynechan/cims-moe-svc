
package hk.health.moe.pojo.dto.webservice;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 *
 * <pre>
 * &lt;complexType name="MedMultDoseDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="stepNo" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="multDoseNo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="prn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dosage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="dosageUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dosageUnitId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="dosageUnitDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="freqCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="freqText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="freqValue" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="freqId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="freqDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="supplFreqCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="supplFreqText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="supplFreqValue1" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="supplFreqValue2" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="supplFreqId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="supplFreqDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="doseInstrConjtnCode" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="doseInstrConjtnDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="doseInstrConjtnLocalDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="duration" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="durationUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="durationUnitId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="durationUnitDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="totalQuantity" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="totalQuantityUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totalQuantityUnitId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="totalQuantityUnitDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MedMultDoseDto", namespace = "http://ehr.gov.hk/moe/ws/beans", propOrder = {
    "stepNo",
    "multDoseNo",
    "prn",
    "dosage",
    "dosageUnit",
    "dosageUnitId",
    "dosageUnitDesc",
    "freqCode",
    "freqText",
    "freqValue",
    "freqId",
    "freqDesc",
    "supplFreqCode",
    "supplFreqText",
    "supplFreqValue1",
    "supplFreqValue2",
    "supplFreqId",
    "supplFreqDesc",
    "doseInstrConjtnCode",
    "doseInstrConjtnDesc",
    "doseInstrConjtnLocalDesc",
    "duration",
    "durationUnit",
    "durationUnitId",
    "durationUnitDesc",
    "startDate",
    "endDate",
    "totalQuantity",
    "totalQuantityUnit",
    "totalQuantityUnitId",
    "totalQuantityUnitDesc"
})
public class MedMultDoseDto {

    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected long stepNo;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected Long multDoseNo;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String prn;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected BigDecimal dosage;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String dosageUnit;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected Long dosageUnitId;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String dosageUnitDesc;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String freqCode;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String freqText;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected Long freqValue;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected Long freqId;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String freqDesc;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String supplFreqCode;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String supplFreqText;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected Long supplFreqValue1;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected Long supplFreqValue2;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected Long supplFreqId;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String supplFreqDesc;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected Long doseInstrConjtnCode;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String doseInstrConjtnDesc;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String doseInstrConjtnLocalDesc;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected Long duration;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String durationUnit;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected Long durationUnitId;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String durationUnitDesc;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDate;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endDate;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected Long totalQuantity;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String totalQuantityUnit;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected Long totalQuantityUnitId;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String totalQuantityUnitDesc;


    public long getStepNo() {
        return stepNo;
    }

    public void setStepNo(long stepNo) {
        this.stepNo = stepNo;
    }

    public Long getMultDoseNo() {
        return multDoseNo;
    }

    public void setMultDoseNo(Long multDoseNo) {
        this.multDoseNo = multDoseNo;
    }

    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    public BigDecimal getDosage() {
        return dosage;
    }

    public void setDosage(BigDecimal dosage) {
        this.dosage = dosage;
    }

    public String getDosageUnit() {
        return dosageUnit;
    }

    public void setDosageUnit(String dosageUnit) {
        this.dosageUnit = dosageUnit;
    }

    public Long getDosageUnitId() {
        return dosageUnitId;
    }

    public void setDosageUnitId(Long dosageUnitId) {
        this.dosageUnitId = dosageUnitId;
    }

    public String getDosageUnitDesc() {
        return dosageUnitDesc;
    }

    public void setDosageUnitDesc(String dosageUnitDesc) {
        this.dosageUnitDesc = dosageUnitDesc;
    }

    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    public String getFreqText() {
        return freqText;
    }

    public void setFreqText(String freqText) {
        this.freqText = freqText;
    }

    public Long getFreqValue() {
        return freqValue;
    }

    public void setFreqValue(Long freqValue) {
        this.freqValue = freqValue;
    }

    public Long getFreqId() {
        return freqId;
    }

    public void setFreqId(Long freqId) {
        this.freqId = freqId;
    }

    public String getFreqDesc() {
        return freqDesc;
    }

    public void setFreqDesc(String freqDesc) {
        this.freqDesc = freqDesc;
    }

    public String getSupplFreqCode() {
        return supplFreqCode;
    }

    public void setSupplFreqCode(String supplFreqCode) {
        this.supplFreqCode = supplFreqCode;
    }

    public String getSupplFreqText() {
        return supplFreqText;
    }

    public void setSupplFreqText(String supplFreqText) {
        this.supplFreqText = supplFreqText;
    }

    public Long getSupplFreqValue1() {
        return supplFreqValue1;
    }

    public void setSupplFreqValue1(Long supplFreqValue1) {
        this.supplFreqValue1 = supplFreqValue1;
    }

    public Long getSupplFreqValue2() {
        return supplFreqValue2;
    }

    public void setSupplFreqValue2(Long supplFreqValue2) {
        this.supplFreqValue2 = supplFreqValue2;
    }

    public Long getSupplFreqId() {
        return supplFreqId;
    }

    public void setSupplFreqId(Long supplFreqId) {
        this.supplFreqId = supplFreqId;
    }

    public String getSupplFreqDesc() {
        return supplFreqDesc;
    }

    public void setSupplFreqDesc(String supplFreqDesc) {
        this.supplFreqDesc = supplFreqDesc;
    }

    public Long getDoseInstrConjtnCode() {
        return doseInstrConjtnCode;
    }

    public void setDoseInstrConjtnCode(Long doseInstrConjtnCode) {
        this.doseInstrConjtnCode = doseInstrConjtnCode;
    }

    public String getDoseInstrConjtnDesc() {
        return doseInstrConjtnDesc;
    }

    public void setDoseInstrConjtnDesc(String doseInstrConjtnDesc) {
        this.doseInstrConjtnDesc = doseInstrConjtnDesc;
    }

    public String getDoseInstrConjtnLocalDesc() {
        return doseInstrConjtnLocalDesc;
    }

    public void setDoseInstrConjtnLocalDesc(String doseInstrConjtnLocalDesc) {
        this.doseInstrConjtnLocalDesc = doseInstrConjtnLocalDesc;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    public Long getDurationUnitId() {
        return durationUnitId;
    }

    public void setDurationUnitId(Long durationUnitId) {
        this.durationUnitId = durationUnitId;
    }

    public String getDurationUnitDesc() {
        return durationUnitDesc;
    }

    public void setDurationUnitDesc(String durationUnitDesc) {
        this.durationUnitDesc = durationUnitDesc;
    }

    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    public void setStartDate(XMLGregorianCalendar startDate) {
        this.startDate = startDate;
    }

    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    public void setEndDate(XMLGregorianCalendar endDate) {
        this.endDate = endDate;
    }

    public Long getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getTotalQuantityUnit() {
        return totalQuantityUnit;
    }

    public void setTotalQuantityUnit(String totalQuantityUnit) {
        this.totalQuantityUnit = totalQuantityUnit;
    }

    public Long getTotalQuantityUnitId() {
        return totalQuantityUnitId;
    }

    public void setTotalQuantityUnitId(Long totalQuantityUnitId) {
        this.totalQuantityUnitId = totalQuantityUnitId;
    }

    public String getTotalQuantityUnitDesc() {
        return totalQuantityUnitDesc;
    }

    public void setTotalQuantityUnitDesc(String totalQuantityUnitDesc) {
        this.totalQuantityUnitDesc = totalQuantityUnitDesc;
    }
}
