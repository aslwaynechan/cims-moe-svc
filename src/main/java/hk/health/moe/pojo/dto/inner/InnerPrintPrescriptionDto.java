package hk.health.moe.pojo.dto.inner;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class InnerPrintPrescriptionDto {
    @NotNull
    @Min(value = 1)
    private Long ordNo;
    @NotBlank
    @Length(max = 1)
    private String isReprint;

    public String getIsReprint() {
        return isReprint;
    }

    public void setIsReprint(String isReprint) {
        this.isReprint = isReprint;
    }

    public Long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(Long ordNo) {
        this.ordNo = ordNo;
    }
}
