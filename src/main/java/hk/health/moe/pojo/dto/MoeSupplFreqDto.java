/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeSupplFreqDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MoeSupplFreqDto implements Serializable/*, IsSerializable*/ {

    private static final long serialVersionUID = -1944020725012468364L;

    private Long supplFreqId;
    private String supplFreqEng;
    private String regimenType;
    private String supplFreqChi;
    private String prefixSupplFreqEng;
    private String prefixSupplFreqChi;
    private Long upperLimit;
    private Long lowerLimit;
    private float supplFreqMultiplierValue;
    private String supplFreqMultiplier;
    private String useInputValue;
    private String useInputMethod;
    private String perDurationMultiplier;
    private List<MoeSupplFreqDescDto> moeSupplFreqDescs = new ArrayList<MoeSupplFreqDescDto>();
    private List<MoeSupplFreqSelectionDto> moeSupplFreqSelections = new ArrayList<MoeSupplFreqSelectionDto>();

    public Long getSupplFreqId() {
        return supplFreqId;
    }

    public void setSupplFreqId(Long supplFreqId) {
        this.supplFreqId = supplFreqId;
    }

    public String getSupplFreqEng() {
        return supplFreqEng;
    }

    public void setSupplFreqEng(String supplFreqEng) {
        this.supplFreqEng = supplFreqEng;
    }

    public String getSupplFreqChi() {
        return supplFreqChi;
    }

    public void setSupplFreqChi(String supplFreqChi) {
        this.supplFreqChi = supplFreqChi;
    }

    public String getPrefixSupplFreqEng() {
        return prefixSupplFreqEng;
    }

    public void setPrefixSupplFreqEng(String prefixSupplFreqEng) {
        this.prefixSupplFreqEng = prefixSupplFreqEng;
    }

    public String getPrefixSupplFreqChi() {
        return prefixSupplFreqChi;
    }

    public void setPrefixSupplFreqChi(String prefixSupplFreqChi) {
        this.prefixSupplFreqChi = prefixSupplFreqChi;
    }

    public String getRegimenType() {
        return regimenType;
    }

    public void setRegimenType(String regimenType) {
        this.regimenType = regimenType;
    }

    public Long getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(Long upperLimit) {
        this.upperLimit = upperLimit;
    }

    public Long getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(Long lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public List<MoeSupplFreqDescDto> getMoeSupplFreqDescs() {
        return moeSupplFreqDescs;
    }

    public void setMoeSupplFreqDescs(List<MoeSupplFreqDescDto> moeSupplFreqDescs) {
        if (moeSupplFreqDescs != null)
            this.moeSupplFreqDescs = moeSupplFreqDescs;
    }

    public void addMoeSupplFreqDescDTO(MoeSupplFreqDescDto moeSupplFreqDescDto) {
        this.moeSupplFreqDescs.add(moeSupplFreqDescDto);
    }

    public List<MoeSupplFreqSelectionDto> getMoeSupplFreqSelections() {
        return moeSupplFreqSelections;
    }

    public void setMoeSupplFreqSelections(
            List<MoeSupplFreqSelectionDto> moeSupplFreqSelections) {
        if (moeSupplFreqSelections != null)
            this.moeSupplFreqSelections = moeSupplFreqSelections;
    }

    public void addMoeSupplFreqSelectionDTO(MoeSupplFreqSelectionDto moeSupplFreqSelectionDto) {
        this.moeSupplFreqSelections.add(moeSupplFreqSelectionDto);
    }

    public float getSupplFreqMultiplierValue() {
        return supplFreqMultiplierValue;
    }

    public void setSupplFreqMultiplierValue(float supplFreqMultiplierValue) {
        this.supplFreqMultiplierValue = supplFreqMultiplierValue;
    }

    public String getSupplFreqMultiplier() {
        return supplFreqMultiplier;
    }

    public void setSupplFreqMultiplier(String supplFreqMultiplier) {
        this.supplFreqMultiplier = supplFreqMultiplier;
    }

    public String getUseInputValue() {
        return useInputValue;
    }

    public void setUseInputValue(String useInputValue) {
        this.useInputValue = useInputValue;
    }

    public String getUseInputMethod() {
        return useInputMethod;
    }

    public void setUseInputMethod(String useInputMethod) {
        this.useInputMethod = useInputMethod;
    }

    public String getPerDurationMultiplier() {
        return perDurationMultiplier;
    }

    public void setPerDurationMultiplier(String perDurationMultiplier) {
        this.perDurationMultiplier = perDurationMultiplier;
    }
}
