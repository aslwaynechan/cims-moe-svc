package hk.health.moe.pojo.dto.inner;

import javax.validation.constraints.NotBlank;

/**
 * @Author Simon
 * @Date 2019/10/14
 */
public class InnerDeleteDrugDto {

    @NotBlank
    private String localDrugId;

    public String getLocalDrugId() {
        return localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        this.localDrugId = localDrugId.toUpperCase();
    }
}
