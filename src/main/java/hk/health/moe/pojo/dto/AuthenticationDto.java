package hk.health.moe.pojo.dto;

import javax.validation.Valid;
import java.io.Serializable;
import java.time.LocalDate;

public class AuthenticationDto implements Serializable {

    private static final long serialVersionUID = -7155486405243135519L;
    private String appName;
    private String version;
    @Valid
    private UserDto user;
    private String token;
    //@NotNull
    private LocalDate apptDate;
    //@NotBlank
    private String apptHospitalCd;
    //@NotBlank
    private String apptSpec;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDate getApptDate() {
        return apptDate;
    }

    public void setApptDate(LocalDate apptDate) {
        this.apptDate = apptDate;
    }

    public String getApptHospitalCd() {
        return apptHospitalCd;
    }

    public void setApptHospitalCd(String apptHospitalCd) {
        this.apptHospitalCd = apptHospitalCd;
    }

    public String getApptSpec() {
        return apptSpec;
    }

    public void setApptSpec(String apptSpec) {
        this.apptSpec = apptSpec;
    }
}
