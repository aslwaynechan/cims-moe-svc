package hk.health.moe.pojo.dto;

/**
 * @Author Simon
 * @Date 2019/9/17
 */
public class MoeLegalClassDto {
    private long legalClassId;
    private String legalClass;
    private String dangerousDrug;

    public long getLegalClassId() {
        return legalClassId;
    }

    public void setLegalClassId(long legalClassId) {
        this.legalClassId = legalClassId;
    }

    public String getLegalClass() {
        return legalClass;
    }

    public void setLegalClass(String legalClass) {
        this.legalClass = legalClass;
    }

    public String getDangerousDrug() {
        return dangerousDrug;
    }

    public void setDangerousDrug(String dangerousDrug) {
        this.dangerousDrug = dangerousDrug;
    }
}
