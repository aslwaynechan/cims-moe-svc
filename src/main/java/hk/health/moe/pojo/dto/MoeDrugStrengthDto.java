/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeDrugStrengthDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;

public class MoeDrugStrengthDto implements Serializable/*, IsSerializable*/ {

    /**
     *
     */
    private static final long serialVersionUID = 8768970035430253324L;

    private String strength;
    private String strengthLevelExtraInfo;
    private String amp;
    private Long ampId;
    private String vmp;
    private Long vmpId;

    //Simon 20190822 for moe_drug_server start--
    private String hkRegNo;
    private int rank;
    public String getHkRegNo() {
        return hkRegNo;
    }

    public void setHkRegNo(String hkRegNo) {
        this.hkRegNo = hkRegNo;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
    //Simon 20190822 for moe_drug_server end--


    public MoeDrugStrengthDto() {

    }

    public MoeDrugStrengthDto(String strength) {
        setStrength(strength);
    }

    public final void setStrength(String strength) {
        this.strength = strength;
    }

    public String getStrength() {
        return strength;
    }

    public String getStrengthLevelExtraInfo() {
        return strengthLevelExtraInfo;
    }

    public void setStrengthLevelExtraInfo(String strengthLevelExtraInfo) {
        this.strengthLevelExtraInfo = strengthLevelExtraInfo;
    }

    public String getAmp() {
        return amp;
    }

    public void setAmp(String amp) {
        this.amp = amp;
    }

    public Long getAmpId() {
        return ampId;
    }

    public void setAmpId(Long ampId) {
        this.ampId = ampId;
    }

    public String getVmp() {
        return vmp;
    }

    public void setVmp(String vmp) {
        this.vmp = vmp;
    }

    public void setVmpId(Long vmpId) {
        this.vmpId = vmpId;
    }

    public Long getVmpId() {
        return vmpId;
    }

}
