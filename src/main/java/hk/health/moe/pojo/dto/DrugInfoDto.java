package hk.health.moe.pojo.dto;

import org.apache.commons.lang3.StringUtils;

public class DrugInfoDto implements java.io.Serializable {

    private static final long serialVersionUID = 710887921212466203L;
    //--- drug info ---
    private String localDrugId;
    private String hkRegNo;

    public DrugInfoDto(){
        super();
    }

    public DrugInfoDto(String localDrugId, String hkRegNo) {
        super();
        // Ricci 20191009 start --
        this.localDrugId = StringUtils.isNotBlank(localDrugId) ? localDrugId.toUpperCase() : localDrugId;
        // Ricci 20191009 end --
        this.hkRegNo = hkRegNo;
    }

    public String getHkRegNo() {
        return hkRegNo;
    }

    public void setHkRegNo(String hkRegNo) {
        this.hkRegNo = hkRegNo;
    }

    public String getLocalDrugId() {
        return localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        // Ricci 20191009 start --
        this.localDrugId = StringUtils.isNotBlank(localDrugId) ? localDrugId.toUpperCase() : localDrugId;
        // Ricci 20191009 end --
    }

}
