
package hk.health.moe.pojo.dto.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the hk.health.moe.model package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetPatientOrderDetailResponse_QNAME = new QName(/*"http://ehr.gov.hk/moe/ws/beans",*/ "getPatientOrderDetailResponse");
    private final static QName _GetPatientPrescriptionByOrderNoRequest_QNAME = new QName("http://ehr.gov.hk/moe/ws/beans", "getPatientPrescriptionByOrderNoRequest");
    private final static QName _GetPatientOrderListResponse_QNAME = new QName(/*"http://ehr.gov.hk/moe/ws/beans", */"getPatientOrderListResponse");
    private final static QName _GetPatientOrderDetailRequest_QNAME = new QName("http://ehr.gov.hk/moe/ws/beans", "getPatientOrderDetailRequest");
    private final static QName _GetPatientOrderLogByPeriodRequest_QNAME = new QName("http://ehr.gov.hk/moe/ws/beans", "getPatientOrderLogByPeriodRequest");
    private final static QName _GetPatientOrderLogByPeriodResponse_QNAME = new QName(/*"http://ehr.gov.hk/moe/ws/beans",*/ "getPatientOrderLogByPeriodResponse");
    private final static QName _DeletePatientOrderDetailRequest_QNAME = new QName("http://ehr.gov.hk/moe/ws/beans", "deletePatientOrderDetailRequest");
    private final static QName _GetPatientPrescriptionByOrderNoResponse_QNAME = new QName(/*"http://ehr.gov.hk/moe/ws/beans",*/"getPatientPrescriptionByOrderNoResponse");
    private final static QName _GetPatientOrderListRequest_QNAME = new QName("http://ehr.gov.hk/moe/ws/beans", "getPatientOrderListRequest");
    private final static QName _DeletePatientOrderDetailResponse_QNAME = new QName(/*"http://ehr.gov.hk/moe/ws/beans",*/ "deletePatientOrderDetailResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: hk.health.moe.model
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PrescriptionResponse }
     * 
     */
//    public PrescriptionResponse createPrescriptionResponse() {
//        return new PrescriptionResponse();
//    }
//
//    /**
//     * Create an instance of {@link OrderListRequest }
//     *
//     */
//    public OrderListRequest createOrderListRequest() {
//        return new OrderListRequest();
//    }
//
//    /**
//     * Create an instance of {@link DeleteOrderDetailResponse }
//     *
//     */
//    public DeleteOrderDetailResponse createDeleteOrderDetailResponse() {
//        return new DeleteOrderDetailResponse();
//    }
//
//    /**
//     * Create an instance of {@link DeleteOrderDetailRequest }
//     *
//     */
//    public DeleteOrderDetailRequest createDeleteOrderDetailRequest() {
//        return new DeleteOrderDetailRequest();
//    }
//
//    /**
//     * Create an instance of {@link OrderDetailRequest }
//     *
//     */
//    public OrderDetailRequest createOrderDetailRequest() {
//        return new OrderDetailRequest();
//    }
//
//    /**
//     * Create an instance of {@link PatientOrderLogByPeriodRequest }
//     *
//     */
//    public PatientOrderLogByPeriodRequest createPatientOrderLogByPeriodRequest() {
//        return new PatientOrderLogByPeriodRequest();
//    }
//
//    /**
//     * Create an instance of {@link OrderListResponse }
//     *
//     */
//    public OrderListResponse createOrderListResponse() {
//        return new OrderListResponse();
//    }
//
//    /**
//     * Create an instance of {@link PrescriptionRequest }
//     *
//     */
//    public PrescriptionRequest createPrescriptionRequest() {
//        return new PrescriptionRequest();
//    }
//
//    /**
//     * Create an instance of {@link OrderListDto }
//     *
//     */
//    public OrderListDto createOrderListDto() {
//        return new OrderListDto();
//    }
//
//    /**
//     * Create an instance of {@link MedMultDoseDto }
//     *
//     */
//    public MedMultDoseDto createMedMultDoseDto() {
//        return new MedMultDoseDto();
//    }
//
//    /**
//     * Create an instance of {@link UserDto }
//     *
//     */
//    public UserDto createUserDto() {
//        return new UserDto();
//    }
//
//    /**
//     * Create an instance of {@link MedAllergenDto }
//     *
//     */
//    public MedAllergenDto createMedAllergenDto() {
//        return new MedAllergenDto();
//    }
//
//    /**
//     * Create an instance of {@link MedProfileDto }
//     *
//     */
//    public MedProfileDto createMedProfileDto() {
//        return new MedProfileDto();
//    }
//
//    /**
//     * Create an instance of {@link PrescriptionDto }
//     *
//     */
//    public PrescriptionDto createPrescriptionDto() {
//        return new PrescriptionDto();
//    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrescriptionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ehr.gov.hk/moe/ws/beans", name = "getPatientOrderDetailResponse")
    public JAXBElement<PrescriptionResponse> createGetPatientOrderDetailResponse(PrescriptionResponse value) {
        return new JAXBElement<PrescriptionResponse>(_GetPatientOrderDetailResponse_QNAME, PrescriptionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrescriptionRequest }{@code >}}
     * 
     */
//    @XmlElementDecl(namespace = "http://ehr.gov.hk/moe/ws/beans", name = "getPatientPrescriptionByOrderNoRequest")
//    public JAXBElement<PrescriptionRequest> createGetPatientPrescriptionByOrderNoRequest(PrescriptionRequest value) {
//        return new JAXBElement<PrescriptionRequest>(_GetPatientPrescriptionByOrderNoRequest_QNAME, PrescriptionRequest.class, null, value);
//    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ehr.gov.hk/moe/ws/beans", name = "getPatientOrderListResponse")
    public JAXBElement<OrderListResponse> createGetPatientOrderListResponse(OrderListResponse value) {
        return new JAXBElement<OrderListResponse>(_GetPatientOrderListResponse_QNAME, OrderListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderDetailRequest }{@code >}}
     * 
     */
//    @XmlElementDecl(namespace = "http://ehr.gov.hk/moe/ws/beans", name = "getPatientOrderDetailRequest")
//    public JAXBElement<OrderDetailRequest> createGetPatientOrderDetailRequest(OrderDetailRequest value) {
//        return new JAXBElement<OrderDetailRequest>(_GetPatientOrderDetailRequest_QNAME, OrderDetailRequest.class, null, value);
//    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PatientOrderLogByPeriodRequest }{@code >}}
     * 
     */
//    @XmlElementDecl(namespace = "http://ehr.gov.hk/moe/ws/beans", name = "getPatientOrderLogByPeriodRequest")
//    public JAXBElement<PatientOrderLogByPeriodRequest> createGetPatientOrderLogByPeriodRequest(PatientOrderLogByPeriodRequest value) {
//        return new JAXBElement<PatientOrderLogByPeriodRequest>(_GetPatientOrderLogByPeriodRequest_QNAME, PatientOrderLogByPeriodRequest.class, null, value);
//    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrescriptionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ehr.gov.hk/moe/ws/beans", name = "getPatientOrderLogByPeriodResponse")
    public JAXBElement<PrescriptionResponse> createGetPatientOrderLogByPeriodResponse(PrescriptionResponse value) {
        return new JAXBElement<PrescriptionResponse>(_GetPatientOrderLogByPeriodResponse_QNAME, PrescriptionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteOrderDetailRequest }{@code >}}
     * 
     */
//    @XmlElementDecl(namespace = "http://ehr.gov.hk/moe/ws/beans", name = "deletePatientOrderDetailRequest")
//    public JAXBElement<DeleteOrderDetailRequest> createDeletePatientOrderDetailRequest(DeleteOrderDetailRequest value) {
//        return new JAXBElement<DeleteOrderDetailRequest>(_DeletePatientOrderDetailRequest_QNAME, DeleteOrderDetailRequest.class, null, value);
//    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrescriptionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ehr.gov.hk/moe/ws/beans", name = "getPatientPrescriptionByOrderNoResponse")
    public JAXBElement<PrescriptionResponse> createGetPatientPrescriptionByOrderNoResponse(PrescriptionResponse value) {
        return new JAXBElement<PrescriptionResponse>(_GetPatientPrescriptionByOrderNoResponse_QNAME, PrescriptionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderListRequest }{@code >}}
     * 
     */
//    @XmlElementDecl(namespace = "http://ehr.gov.hk/moe/ws/beans", name = "getPatientOrderListRequest")
//    public JAXBElement<OrderListRequest> createGetPatientOrderListRequest(OrderListRequest value) {
//        return new JAXBElement<OrderListRequest>(_GetPatientOrderListRequest_QNAME, OrderListRequest.class, null, value);
//    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteOrderDetailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ehr.gov.hk/moe/ws/beans", name = "deletePatientOrderDetailResponse")
    public JAXBElement<DeleteOrderDetailResponse> createDeletePatientOrderDetailResponse(DeleteOrderDetailResponse value) {
        return new JAXBElement<DeleteOrderDetailResponse>(_DeletePatientOrderDetailResponse_QNAME, DeleteOrderDetailResponse.class, null, value);
    }

}
