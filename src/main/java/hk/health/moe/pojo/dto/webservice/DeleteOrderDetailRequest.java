
package hk.health.moe.pojo.dto.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 *
 * <pre>
 * &lt;complexType name="DeleteOrderDetailRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userDto" type="{http://ehr.gov.hk/moe/ws/beans}UserDto"/>
 *         &lt;element name="patientKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="episodeNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="episodeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeleteOrderDetailRequest",  propOrder = {
    "userDto",
    "patientKey",
    "episodeNo",
    "episodeType"
})
public class DeleteOrderDetailRequest {

    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    protected UserDto userDto;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    protected String patientKey;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    protected String episodeNo;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    protected String episodeType;

    public UserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }

    public String getPatientKey() {
        return patientKey;
    }

    public void setPatientKey(String patientKey) {
        this.patientKey = patientKey;
    }

    public String getEpisodeNo() {
        return episodeNo;
    }

    public void setEpisodeNo(String episodeNo) {
        this.episodeNo = episodeNo;
    }

    public String getEpisodeType() {
        return episodeType;
    }

    public void setEpisodeType(String episodeType) {
        this.episodeType = episodeType;
    }
}
