package hk.health.moe.pojo.dto;

import java.io.Serializable;
import java.util.List;
public class MyFavouriteSortDto implements Serializable {

    private List<MoeMyFavouriteHdrDto> favouriteHdrs;
    private boolean department;

    public List<MoeMyFavouriteHdrDto> getFavouriteHdrs() {
        return favouriteHdrs;
    }

    public void setFavouriteHdrs(List<MoeMyFavouriteHdrDto> favouriteHdrDtos) {
        this.favouriteHdrs = favouriteHdrDtos;
    }

    public boolean isDepartment() {
        return department;
    }

    public void setDepartment(boolean department) {
        this.department = department;
    }
}
