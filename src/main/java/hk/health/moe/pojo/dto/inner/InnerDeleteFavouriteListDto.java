package hk.health.moe.pojo.dto.inner;

import hk.health.moe.pojo.dto.MoeMyFavouriteHdrDto;

import java.util.List;
import java.util.Objects;

/**
 * @author Eric Ning
 * @version 1.0
 * @date 2019/12/11 12:02
 */
public class InnerDeleteFavouriteListDto {
    private List<MoeMyFavouriteHdrDto> moeMyFavouriteHdrDtos;

    public InnerDeleteFavouriteListDto() {
    }

    public InnerDeleteFavouriteListDto(List<MoeMyFavouriteHdrDto> moeMyFavouriteHdrDtos) {
        this.moeMyFavouriteHdrDtos = moeMyFavouriteHdrDtos;
    }

    public List<MoeMyFavouriteHdrDto> getMoeMyFavouriteHdrDtos() {
        return moeMyFavouriteHdrDtos;
    }

    public void setMoeMyFavouriteHdrDtos(List<MoeMyFavouriteHdrDto> moeMyFavouriteHdrDtos) {
        this.moeMyFavouriteHdrDtos = moeMyFavouriteHdrDtos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InnerDeleteFavouriteListDto)) return false;
        InnerDeleteFavouriteListDto that = (InnerDeleteFavouriteListDto) o;
        return getMoeMyFavouriteHdrDtos().equals(that.getMoeMyFavouriteHdrDtos());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMoeMyFavouriteHdrDtos());
    }

    @Override
    public String toString() {
        return "InnerDeleteFavouriteListDto{" +
                "moeMyFavouriteHdrDtos=" + moeMyFavouriteHdrDtos +
                '}';
    }
}
