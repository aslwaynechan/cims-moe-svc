
package hk.health.moe.pojo.dto.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 *
 *
 * <pre>
 * &lt;complexType name="OrderListDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="orderNo" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="patientKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="episodeNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hospCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hospName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prescType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ordSubType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="specialty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ward" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="createDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="createHosp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="createRank" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="createRankDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="createUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="createUserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="updateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="updateHosp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="updateRank" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="updateRankDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="updateUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="updateUserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderListDto",/* namespace = "http://ehr.gov.hk/moe/ws/beans",*/ propOrder = {
    "orderNo",
    "patientKey",
    "episodeNo",
    "hospCode",
    "hospName",
    "prescType",
    "ordSubType",
    "specialty",
    "ward",
    "createDate",
    "createHosp",
    "createRank",
    "createRankDesc",
    "createUser",
    "createUserId",
    "updateDate",
    "updateHosp",
    "updateRank",
    "updateRankDesc",
    "updateUser",
    "updateUserId"
})
public class OrderListDto {

    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected long orderNo;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String patientKey;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String episodeNo;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String hospCode;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String hospName;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String prescType;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String ordSubType;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String specialty;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String ward;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createDate;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String createHosp;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String createRank;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String createRankDesc;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String createUser;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String createUserId;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar updateDate;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String updateHosp;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String updateRank;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String updateRankDesc;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String updateUser;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String updateUserId;


    public long getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(long orderNo) {
        this.orderNo = orderNo;
    }

    public String getPatientKey() {
        return patientKey;
    }

    public void setPatientKey(String patientKey) {
        this.patientKey = patientKey;
    }

    public String getEpisodeNo() {
        return episodeNo;
    }

    public void setEpisodeNo(String episodeNo) {
        this.episodeNo = episodeNo;
    }

    public String getHospCode() {
        return hospCode;
    }

    public void setHospCode(String hospCode) {
        this.hospCode = hospCode;
    }

    public String getHospName() {
        return hospName;
    }

    public void setHospName(String hospName) {
        this.hospName = hospName;
    }

    public String getPrescType() {
        return prescType;
    }

    public void setPrescType(String prescType) {
        this.prescType = prescType;
    }

    public String getOrdSubType() {
        return ordSubType;
    }

    public void setOrdSubType(String ordSubType) {
        this.ordSubType = ordSubType;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public XMLGregorianCalendar getCreateDate() {
        return createDate;
    }

    public void setCreateDate(XMLGregorianCalendar createDate) {
        this.createDate = createDate;
    }

    public String getCreateHosp() {
        return createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    public String getCreateRank() {
        return createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    public String getCreateRankDesc() {
        return createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public XMLGregorianCalendar getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(XMLGregorianCalendar updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateHosp() {
        return updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    public String getUpdateRank() {
        return updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }
}
