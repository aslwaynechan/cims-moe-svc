package hk.health.moe.pojo.dto.webservice;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @Author Simon
 * @Date 2019/11/13
 */
public class OrderListRequestDto {
    @NotBlank
    private String hospCode;
    @NotBlank
    private String patientKey;
    private String episodeNo;
    @NotBlank
    private String episodeType;
    private Date startDate;
    private Date endDate;

    public String getHospCode() {
        return hospCode;
    }

    public void setHospCode(String hospCode) {
        this.hospCode = hospCode;
    }

    public String getPatientKey() {
        return patientKey;
    }

    public void setPatientKey(String patientKey) {
        this.patientKey = patientKey;
    }

    public String getEpisodeNo() {
        return episodeNo;
    }

    public void setEpisodeNo(String episodeNo) {
        this.episodeNo = episodeNo;
    }

    public String getEpisodeType() {
        return episodeType;
    }

    public void setEpisodeType(String episodeType) {
        this.episodeType = episodeType;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
