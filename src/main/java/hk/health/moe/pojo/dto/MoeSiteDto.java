/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeSiteDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;

public class MoeSiteDto implements Serializable/*, IsSerializable*/ {

    private static final long serialVersionUID = -2087565434070347992L;

    private Long siteId;
    private String siteEng;
    private Long siteMultiplier;

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public String getSiteEng() {
        return siteEng;
    }

    public void setSiteEng(String siteEng) {
        this.siteEng = siteEng;
    }

    public Long getSiteMultiplier() {
        return siteMultiplier;
    }

    public void setSiteMultiplier(Long siteMultiplier) {
        this.siteMultiplier = siteMultiplier;
    }


}
