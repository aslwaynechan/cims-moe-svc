package hk.health.moe.pojo.dto.inner;

import java.util.List;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/8/5
 */
public class InnerMaxDosOfDdDto {

    private Long totalMaxDosage;
    private List<InnerMaxDosDto> maxDosage;

    public Long getTotalMaxDosage() {
        return totalMaxDosage;
    }

    public void setTotalMaxDosage(Long totalMaxDosage) {
        this.totalMaxDosage = totalMaxDosage;
    }

    public List<InnerMaxDosDto> getMaxDosage() {
        return maxDosage;
    }

    public void setMaxDosage(List<InnerMaxDosDto> maxDosage) {
        this.maxDosage = maxDosage;
    }
}
