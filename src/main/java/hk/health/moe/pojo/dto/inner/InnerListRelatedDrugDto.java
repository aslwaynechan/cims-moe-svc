package hk.health.moe.pojo.dto.inner;

import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/8/28
 */
public class InnerListRelatedDrugDto implements Serializable {

    private static final long serialVersionUID = 2023594307502137854L;
    @NotBlank
    private String localDrugId;
    private String vtm;
    private String tradeName;
    private String routeEng;
    private String formEng;
    private String doseFormExtraInfo;
    private String hospCode;



    public String getLocalDrugId() {
        return localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        // Ricci 20191009 start --
        this.localDrugId = StringUtils.isNotBlank(localDrugId) ? localDrugId.toUpperCase() : localDrugId;
        // Ricci 20191009 end --
    }

    public String getVtm() {
        return vtm;
    }

    public void setVtm(String vtm) {
        this.vtm = vtm;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getRouteEng() {
        return routeEng;
    }

    public void setRouteEng(String routeEng) {
        this.routeEng = routeEng;
    }

    public String getFormEng() {
        return formEng;
    }

    public void setFormEng(String formEng) {
        this.formEng = formEng;
    }

    public String getDoseFormExtraInfo() {
        return doseFormExtraInfo;
    }

    public void setDoseFormExtraInfo(String doseFormExtraInfo) {
        this.doseFormExtraInfo = doseFormExtraInfo;
    }

    public String getHospCode() {
        return hospCode;
    }

    public void setHospCode(String hospCode) {
        this.hospCode = hospCode;
    }


}
