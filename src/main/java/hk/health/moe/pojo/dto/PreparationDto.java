/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  PreparationDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

public class PreparationDto implements Serializable/*, IsSerializable*/ {

    private String strength;
    private String strengthLevelExtraInfo;
    private Long ampId;
    private String localDrugId;

    //Simon 20190813 start-- add
    //to identify which strength is selected
    private boolean selected = false;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    //Simon 20190813 end-- add

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public String getStrengthLevelExtraInfo() {
        return strengthLevelExtraInfo;
    }

    public void setStrengthLevelExtraInfo(String strengthLevelExtraInfo) {
        this.strengthLevelExtraInfo = strengthLevelExtraInfo;
    }

    public Long getAmpId() {
        return ampId;
    }

    public void setAmpId(Long ampId) {
        this.ampId = ampId;
    }

    public String getLocalDrugId() {
        return localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        // Ricci 20191009 start --
        this.localDrugId = StringUtils.isNotBlank(localDrugId) ? localDrugId.toUpperCase() : localDrugId;
        // Ricci 20191009 end --
    }

    @Override
    public String toString() {
        return "PreparationDto{" +
                "strength='" + strength + '\'' +
                ", strengthLevelExtraInfo='" + strengthLevelExtraInfo + '\'' +
                ", ampId=" + ampId +
                ", localDrugId='" + localDrugId + '\'' +
                ", selected=" + selected +
                '}';
    }

    public static void main(String[] args){
        PreparationDto p = new PreparationDto();
        System.out.print(p.toString());
    }
}
