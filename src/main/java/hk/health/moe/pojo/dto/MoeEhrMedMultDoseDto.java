/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeEhrMedMultDoseDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;

public class MoeEhrMedMultDoseDto implements /*IsSerializable,*/ Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3862161936981536854L;

    private Long freqId;
    private Long supplFreqId;
    private Long moQtyUnitId;

    public Long getFreqId() {
        return freqId;
    }

    public void setFreqId(Long freqId) {
        this.freqId = freqId;
    }

    public Long getSupplFreqId() {
        return supplFreqId;
    }

    public void setSupplFreqId(Long supplFreqId) {
        this.supplFreqId = supplFreqId;
    }

    public Long getMoQtyUnitId() {
        return moQtyUnitId;
    }

    public void setMoQtyUnitId(Long moQtyUnitId) {
        this.moQtyUnitId = moQtyUnitId;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MoeEhrMedMultDoseDTO [freqId=");
        builder.append(freqId);
        builder.append(", supplFreqId=");
        builder.append(supplFreqId);
        builder.append(", moQtyUnitId=");
        builder.append(moQtyUnitId);
        builder.append("]");
        return builder.toString();
    }

}
