package hk.health.moe.pojo.dto.inner;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/7/31
 */
public class InnerMoeMedMultDoseDto {
    private Long multDoseNo;
    private String freqCode;
    private Long duration;
    private String durationUnit;
    private Long freq1;
    private Long supFreq1;
    private Long supFreq2;
    private InnerMoeEhrMedMultDoseDto moeEhrMedMultDose;

    public Long getMultDoseNo() {
        return multDoseNo;
    }

    public void setMultDoseNo(Long multDoseNo) {
        this.multDoseNo = multDoseNo;
    }

    public String getFreqCode() {
        return freqCode;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    public Long getFreq1() {
        return freq1;
    }

    public void setFreq1(Long freq1) {
        this.freq1 = freq1;
    }

    public InnerMoeEhrMedMultDoseDto getMoeEhrMedMultDose() {
        return moeEhrMedMultDose;
    }

    public void setMoeEhrMedMultDose(InnerMoeEhrMedMultDoseDto moeEhrMedMultDose) {
        this.moeEhrMedMultDose = moeEhrMedMultDose;
    }

    public Long getSupFreq1() {
        return supFreq1;
    }

    public void setSupFreq1(Long supFreq1) {
        this.supFreq1 = supFreq1;
    }

    public Long getSupFreq2() {
        return supFreq2;
    }

    public void setSupFreq2(Long supFreq2) {
        this.supFreq2 = supFreq2;
    }
}
