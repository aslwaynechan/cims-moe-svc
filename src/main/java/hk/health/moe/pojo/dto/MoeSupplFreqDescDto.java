/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeSupplFreqDescDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;

public class MoeSupplFreqDescDto implements /*IsSerializable,*/ Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3700848933489877415L;

    private String freqCode;
    private Long supplFreq1;
    private String displayWithFreq;
    private String supplFreqDescEng;
    private String supplFreqDescChi;
    private Long supplFreqSelectionValue;

    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    public Long getSupplFreq1() {
        return supplFreq1;
    }

    public void setSupplFreq1(Long supplFreq1) {
        this.supplFreq1 = supplFreq1;
    }

    public String getDisplayWithFreq() {
        return displayWithFreq;
    }

    public void setDisplayWithFreq(String displayWithFreq) {
        this.displayWithFreq = displayWithFreq;
    }

    public String getSupplFreqDescEng() {
        return supplFreqDescEng;
    }

    public void setSupplFreqDescEng(String supplFreqDescEng) {
        this.supplFreqDescEng = supplFreqDescEng;
    }

    public String getSupplFreqDescChi() {
        return supplFreqDescChi;
    }

    public void setSupplFreqDescChi(String supplFreqDescChi) {
        this.supplFreqDescChi = supplFreqDescChi;
    }

    public void setSupplFreqSelectionValue(Long supplFreqSelectionValue) {
        this.supplFreqSelectionValue = supplFreqSelectionValue;
    }

    public Long getSupplFreqSelectionValue() {
        return supplFreqSelectionValue;
    }
}
