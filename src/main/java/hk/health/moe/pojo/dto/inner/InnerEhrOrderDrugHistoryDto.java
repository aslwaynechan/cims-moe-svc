package hk.health.moe.pojo.dto.inner;

import javax.validation.constraints.Pattern;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/7/30
 */
public class InnerEhrOrderDrugHistoryDto {

    @Pattern(regexp = "[A,1,3,6]")
    private String withinMonths; //only in "A","1","3","6"
    @Pattern(regexp = "[A,H,O,D]")
    private String prescType; //only in "A","H","O","D"

    public String getWithinMonths() {
        return withinMonths;
    }

    public void setWithinMonths(String withinMonths) {
        this.withinMonths = withinMonths;
    }

    public String getPrescType() {
        return prescType;
    }

    public void setPrescType(String prescType) {
        this.prescType = prescType;
    }
}
