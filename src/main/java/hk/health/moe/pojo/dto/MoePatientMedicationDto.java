package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;
import java.util.List;

public class MoePatientMedicationDto implements /*IsSerializable,*/ Serializable {

    private static final long serialVersionUID = -7782299919095507035L;

    private Boolean isFormat;
    private Boolean isMyFavourite;
    private String vtm;
    private List<String> ingredients;
    private List<String> strengths;
    private String screenDisplay;
    private String nameType;
    private String genericIndicator;
    private String aliasName;
    private String tradeName;
    private String manufacturer;
    private String strength;
    private String routeEng;
    private String formDesc;
    private String doseFormExtraInfo;
    private String strengthLevelExtraInfo;
    private String orderLineRowNum;
    private String strengthCompulsory;

    public Boolean getFormat() {
        return isFormat;
    }

    public void setFormat(Boolean format) {
        isFormat = format;
    }

    public Boolean getMyFavourite() {
        return isMyFavourite;
    }

    public void setMyFavourite(Boolean myFavourite) {
        isMyFavourite = myFavourite;
    }

    public String getVtm() {
        return vtm;
    }

    public void setVtm(String vtm) {
        this.vtm = vtm;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public List<String> getStrengths() {
        return strengths;
    }

    public void setStrengths(List<String> strengths) {
        this.strengths = strengths;
    }

    public String getScreenDisplay() {
        return screenDisplay;
    }

    public void setScreenDisplay(String screenDisplay) {
        this.screenDisplay = screenDisplay;
    }

    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    public String getGenericIndicator() {
        return genericIndicator;
    }

    public void setGenericIndicator(String genericIndicator) {
        this.genericIndicator = genericIndicator;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public String getRouteEng() {
        return routeEng;
    }

    public void setRouteEng(String routeEng) {
        this.routeEng = routeEng;
    }

    public String getFormDesc() {
        return formDesc;
    }

    public void setFormDesc(String formDesc) {
        this.formDesc = formDesc;
    }

    public String getDoseFormExtraInfo() {
        return doseFormExtraInfo;
    }

    public void setDoseFormExtraInfo(String doseFormExtraInfo) {
        this.doseFormExtraInfo = doseFormExtraInfo;
    }

    public String getStrengthLevelExtraInfo() {
        return strengthLevelExtraInfo;
    }

    public void setStrengthLevelExtraInfo(String strengthLevelExtraInfo) {
        this.strengthLevelExtraInfo = strengthLevelExtraInfo;
    }

    public String getOrderLineRowNum() {
        return orderLineRowNum;
    }

    public void setOrderLineRowNum(String orderLineRowNum) {
        this.orderLineRowNum = orderLineRowNum;
    }

    public String getStrengthCompulsory() {
        return strengthCompulsory;
    }

    public void setStrengthCompulsory(String strengthCompulsory) {
        this.strengthCompulsory = strengthCompulsory;
    }
}
