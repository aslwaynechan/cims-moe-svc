package hk.health.moe.pojo.dto.inner;

import org.apache.commons.lang3.StringUtils;

public class InnerListCommonDosageDto {
    private boolean newDrug;
    private String localDrugId;
    private String hkRegNo;

    public boolean isNewDrug() {
        return newDrug;
    }

    public void setNewDrug(boolean newDrug) {
        this.newDrug = newDrug;
    }

    public String getLocalDrugId() {
        return localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        // Ricci 20191009 start --
        this.localDrugId = StringUtils.isNotBlank(localDrugId) ? localDrugId.toUpperCase() : localDrugId;
        // Ricci 20191009 end --
    }

    public String getHkRegNo() {
        return hkRegNo;
    }

    public void setHkRegNo(String hkRegNo) {
        this.hkRegNo = hkRegNo;
    }
}
