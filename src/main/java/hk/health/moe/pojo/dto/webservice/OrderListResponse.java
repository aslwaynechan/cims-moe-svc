
package hk.health.moe.pojo.dto.webservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 *
 *
 * <pre>
 * &lt;complexType name="OrderListResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="orderListDto" type="{http://ehr.gov.hk/moe/ws/beans}OrderListDto" maxOccurs="1000" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderListResponse", namespace = "http://ehr.gov.hk/moe/ws/beans", propOrder = {
    "orderListDto"
})
@XmlRootElement(name = "getPatientOrderListResponse",namespace = "http://ehr.gov.hk/moe/ws/beans")
public class OrderListResponse {

    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected List<OrderListDto> orderListDto;

    /**
     * Gets the value of the orderListDto property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderListDto property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderListDto().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderListDto }
     * 
     * 
     */
    public List<OrderListDto> getOrderListDto() {
        if (orderListDto == null) {
            orderListDto = new ArrayList<OrderListDto>();
        }
        return this.orderListDto;
    }

}
