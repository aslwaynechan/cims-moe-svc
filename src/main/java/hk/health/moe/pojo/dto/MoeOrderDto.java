/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeOrderDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import javax.validation.Valid;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MoeOrderDto implements /*IsSerializable,*/ Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 851486043231411735L;

    private Long ordNo;
    private String hospcode;
    private String workstore;
    private String patHospcode;
    private Date ordDate;
    private String ordType = "P";
    private String ordSubtype;
    private String ordStatus = "O";
    private String refNo;
    private String moCode;
    private Long prevOrdNo;
    private Long maxItemNo;
    private Date endDate;
    private String suspend;
    private String privatePatient;
    private String transferPatient;
    private String allowModify;
    private String prescType;
    private String moeOrder;
    private String caseNo;
    private String srcSpecialty;
    private String srcSubspecialty;
    private String ipasWard;
    private String bedNo;
    private String specialty;
    private String subspecialty;
    private String phsWard;
    private String eisSpecCode;
    private String dispHospcode;
    private String dispWorkstore;
    private String ticknum;
    private Date tickdate;
    private String wkstatcode;
    private Date lastUpdDate;
    private String lastUpdBy;
    private Long dispOrdNo;
    private String dischargeReason;
    private String remarkCreateBy;
    private Date remarkCreateDate;
    private String remarkText;
    private String remarkConfirmBy;
    private Date remarkConfirmDate;
    private String remarkStatus;
    private String lastPrescType;
    private String uncollect;
    private String allowPostComment;
    private String printType;
    private String prevHospcode;
    private String prevWorkstore;
    private Date prevDispDate;
    private String prevTicknum;
    @Valid
    private List<MoeMedProfileDto> moeMedProfiles = new ArrayList<MoeMedProfileDto>();
    private List<MoeEhrAcceptDrugCheckingDto> moeEhrAcceptDrugCheckings = new ArrayList<MoeEhrAcceptDrugCheckingDto>();
    private MoeEhrOrderDto moeEhrOrder;
    //@NotEmpty
    private Long version;
    private String backDate;

    public Long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(Long ordNo) {
        this.ordNo = ordNo;
    }

    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    public String getWorkstore() {
        return workstore;
    }

    public void setWorkstore(String workstore) {
        this.workstore = workstore;
    }

    public String getPatHospcode() {
        return patHospcode;
    }

    public void setPatHospcode(String patHospcode) {
        this.patHospcode = patHospcode;
    }

    public Date getOrdDate() {
        return ordDate;
    }

    public void setOrdDate(Date ordDate) {
        this.ordDate = ordDate;
    }

    public String getOrdType() {
        return ordType;
    }

    public void setOrdType(String ordType) {
        this.ordType = ordType;
    }

    public String getOrdSubtype() {
        return ordSubtype;
    }

    public void setOrdSubtype(String ordSubtype) {
        this.ordSubtype = ordSubtype;
    }

    public String getOrdStatus() {
        return ordStatus;
    }

    public void setOrdStatus(String ordStatus) {
        this.ordStatus = ordStatus;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getMoCode() {
        return moCode;
    }

    public void setMoCode(String moCode) {
        this.moCode = moCode;
    }

    public Long getPrevOrdNo() {
        return prevOrdNo;
    }

    public void setPrevOrdNo(Long prevOrdNo) {
        this.prevOrdNo = prevOrdNo;
    }

    public Long getMaxItemNo() {
        return maxItemNo;
    }

    public void setMaxItemNo(Long maxItemNo) {
        this.maxItemNo = maxItemNo;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getSuspend() {
        return suspend;
    }

    public void setSuspend(String suspend) {
        this.suspend = suspend;
    }

    public String getPrivatePatient() {
        return privatePatient;
    }

    public void setPrivatePatient(String privatePatient) {
        this.privatePatient = privatePatient;
    }

    public String getTransferPatient() {
        return transferPatient;
    }

    public void setTransferPatient(String transferPatient) {
        this.transferPatient = transferPatient;
    }

    public String getAllowModify() {
        return allowModify;
    }

    public void setAllowModify(String allowModify) {
        this.allowModify = allowModify;
    }

    public String getPrescType() {
        return prescType;
    }

    public void setPrescType(String prescType) {
        this.prescType = prescType;
    }

    public String getMoeOrder() {
        return moeOrder;
    }

    public void setMoeOrder(String moeOrder) {
        this.moeOrder = moeOrder;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getSrcSpecialty() {
        return srcSpecialty;
    }

    public void setSrcSpecialty(String srcSpecialty) {
        this.srcSpecialty = srcSpecialty;
    }

    public String getSrcSubspecialty() {
        return srcSubspecialty;
    }

    public void setSrcSubspecialty(String srcSubspecialty) {
        this.srcSubspecialty = srcSubspecialty;
    }

    public String getIpasWard() {
        return ipasWard;
    }

    public void setIpasWard(String ipasWard) {
        this.ipasWard = ipasWard;
    }

    public String getBedNo() {
        return bedNo;
    }

    public void setBedNo(String bedNo) {
        this.bedNo = bedNo;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getSubspecialty() {
        return subspecialty;
    }

    public void setSubspecialty(String subspecialty) {
        this.subspecialty = subspecialty;
    }

    public String getPhsWard() {
        return phsWard;
    }

    public void setPhsWard(String phsWard) {
        this.phsWard = phsWard;
    }

    public String getEisSpecCode() {
        return eisSpecCode;
    }

    public void setEisSpecCode(String eisSpecCode) {
        this.eisSpecCode = eisSpecCode;
    }

    public String getDispHospcode() {
        return dispHospcode;
    }

    public void setDispHospcode(String dispHospcode) {
        this.dispHospcode = dispHospcode;
    }

    public String getDispWorkstore() {
        return dispWorkstore;
    }

    public void setDispWorkstore(String dispWorkstore) {
        this.dispWorkstore = dispWorkstore;
    }

    public String getTicknum() {
        return ticknum;
    }

    public void setTicknum(String ticknum) {
        this.ticknum = ticknum;
    }

    public Date getTickdate() {
        return tickdate;
    }

    public void setTickdate(Date tickdate) {
        this.tickdate = tickdate;
    }

    public String getWkstatcode() {
        return wkstatcode;
    }

    public void setWkstatcode(String wkstatcode) {
        this.wkstatcode = wkstatcode;
    }

    public Date getLastUpdDate() {
        return lastUpdDate;
    }

    public void setLastUpdDate(Date lastUpdDate) {
        this.lastUpdDate = lastUpdDate;
    }

    public String getLastUpdBy() {
        return lastUpdBy;
    }

    public void setLastUpdBy(String lastUpdBy) {
        this.lastUpdBy = lastUpdBy;
    }

    public Long getDispOrdNo() {
        return dispOrdNo;
    }

    public void setDispOrdNo(Long dispOrdNo) {
        this.dispOrdNo = dispOrdNo;
    }

    public String getDischargeReason() {
        return dischargeReason;
    }

    public void setDischargeReason(String dischargeReason) {
        this.dischargeReason = dischargeReason;
    }

    public String getRemarkCreateBy() {
        return remarkCreateBy;
    }

    public void setRemarkCreateBy(String remarkCreateBy) {
        this.remarkCreateBy = remarkCreateBy;
    }

    public Date getRemarkCreateDate() {
        return remarkCreateDate;
    }

    public void setRemarkCreateDate(Date remarkCreateDate) {
        this.remarkCreateDate = remarkCreateDate;
    }

    public String getRemarkText() {
        return remarkText;
    }

    public void setRemarkText(String remarkText) {
        this.remarkText = remarkText;
    }

    public String getRemarkConfirmBy() {
        return remarkConfirmBy;
    }

    public void setRemarkConfirmBy(String remarkConfirmBy) {
        this.remarkConfirmBy = remarkConfirmBy;
    }

    public Date getRemarkConfirmDate() {
        return remarkConfirmDate;
    }

    public void setRemarkConfirmDate(Date remarkConfirmDate) {
        this.remarkConfirmDate = remarkConfirmDate;
    }

    public String getRemarkStatus() {
        return remarkStatus;
    }

    public void setRemarkStatus(String remarkStatus) {
        this.remarkStatus = remarkStatus;
    }

    public String getLastPrescType() {
        return lastPrescType;
    }

    public void setLastPrescType(String lastPrescType) {
        this.lastPrescType = lastPrescType;
    }

    public String getUncollect() {
        return uncollect;
    }

    public void setUncollect(String uncollect) {
        this.uncollect = uncollect;
    }

    public String getAllowPostComment() {
        return allowPostComment;
    }

    public void setAllowPostComment(String allowPostComment) {
        this.allowPostComment = allowPostComment;
    }

    public String getPrintType() {
        return printType;
    }

    public void setPrintType(String printType) {
        this.printType = printType;
    }

    public String getPrevHospcode() {
        return prevHospcode;
    }

    public void setPrevHospcode(String prevHospcode) {
        this.prevHospcode = prevHospcode;
    }

    public String getPrevWorkstore() {
        return prevWorkstore;
    }

    public void setPrevWorkstore(String prevWorkstore) {
        this.prevWorkstore = prevWorkstore;
    }

    public Date getPrevDispDate() {
        return prevDispDate;
    }

    public void setPrevDispDate(Date prevDispDate) {
        this.prevDispDate = prevDispDate;
    }

    public String getPrevTicknum() {
        return prevTicknum;
    }

    public void setPrevTicknum(String prevTicknum) {
        this.prevTicknum = prevTicknum;
    }

    public List<MoeMedProfileDto> getMoeMedProfiles() {
        return moeMedProfiles;
    }

    public void addMoeMedProfiles(MoeMedProfileDto moeMedProfileDto) {
        this.moeMedProfiles.add(moeMedProfileDto);
    }

    public void setMoeMedProfiles(List<MoeMedProfileDto> moeMedProfiles) {
        if (moeMedProfiles != null)
            this.moeMedProfiles = moeMedProfiles;
    }

    public List<MoeEhrAcceptDrugCheckingDto> getMoeEhrAcceptDrugCheckings() {
        return moeEhrAcceptDrugCheckings;
    }

    public void addMoeEhrAcceptDrugCheckingDTO(MoeEhrAcceptDrugCheckingDto moeEhrAcceptDrugChecking) {
        this.moeEhrAcceptDrugCheckings.add(moeEhrAcceptDrugChecking);
    }

    public void setMoeEhrAcceptDrugCheckings(
            List<MoeEhrAcceptDrugCheckingDto> moeEhrAcceptDrugCheckings) {
        if (moeEhrAcceptDrugCheckings != null)
            this.moeEhrAcceptDrugCheckings = moeEhrAcceptDrugCheckings;
    }

    public MoeEhrOrderDto getMoeEhrOrder() {
        return moeEhrOrder;
    }

    public void setMoeEhrOrder(MoeEhrOrderDto moeEhrOrder) {
        this.moeEhrOrder = moeEhrOrder;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getVersion() {
        return version;
    }

    public String getBackDate() {
        return backDate;
    }

    public void setBackDate(String backDate) {
        this.backDate = backDate;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MoeOrderDTO [allowModify=");
        builder.append(allowModify);
        builder.append(", allowPostComment=");
        builder.append(allowPostComment);
        builder.append(", bedNo=");
        builder.append(bedNo);
        builder.append(", caseNo=");
        builder.append(caseNo);
        builder.append(", dischargeReason=");
        builder.append(dischargeReason);
        builder.append(", dispHospcode=");
        builder.append(dispHospcode);
        builder.append(", dispOrdNo=");
        builder.append(dispOrdNo);
        builder.append(", dispWorkstore=");
        builder.append(dispWorkstore);
        builder.append(", eisSpecCode=");
        builder.append(eisSpecCode);
        builder.append(", endDate=");
        builder.append(endDate);
        builder.append(", hospcode=");
        builder.append(hospcode);
        builder.append(", ipasWard=");
        builder.append(ipasWard);
        builder.append(", lastPrescType=");
        builder.append(lastPrescType);
        builder.append(", lastUpdBy=");
        builder.append(lastUpdBy);
        builder.append(", lastUpdDate=");
        builder.append(lastUpdDate);
        builder.append(", maxItemNo=");
        builder.append(maxItemNo);
        builder.append(", moCode=");
        builder.append(moCode);
        builder.append(", moeEhrOrder=");
        builder.append(moeEhrOrder);
        builder.append(", moeMedProfiles=");
        builder.append(moeMedProfiles);
        builder.append(", moeEhrAcceptDrugCheckings=");
        builder.append(moeEhrAcceptDrugCheckings);
        builder.append(", moeOrder=");
        builder.append(moeOrder);
        builder.append(", ordDate=");
        builder.append(ordDate);
        builder.append(", ordNo=");
        builder.append(ordNo);
        builder.append(", ordStatus=");
        builder.append(ordStatus);
        builder.append(", ordSubtype=");
        builder.append(ordSubtype);
        builder.append(", ordType=");
        builder.append(ordType);
        builder.append(", patHospcode=");
        builder.append(patHospcode);
        builder.append(", phsWard=");
        builder.append(phsWard);
        builder.append(", prescType=");
        builder.append(prescType);
        builder.append(", prevDispDate=");
        builder.append(prevDispDate);
        builder.append(", prevHospcode=");
        builder.append(prevHospcode);
        builder.append(", prevOrdNo=");
        builder.append(prevOrdNo);
        builder.append(", prevTicknum=");
        builder.append(prevTicknum);
        builder.append(", prevWorkstore=");
        builder.append(prevWorkstore);
        builder.append(", printType=");
        builder.append(printType);
        builder.append(", privatePatient=");
        builder.append(privatePatient);
        builder.append(", refNo=");
        builder.append(refNo);
        builder.append(", remarkConfirmBy=");
        builder.append(remarkConfirmBy);
        builder.append(", remarkConfirmDate=");
        builder.append(remarkConfirmDate);
        builder.append(", remarkCreateBy=");
        builder.append(remarkCreateBy);
        builder.append(", remarkCreateDate=");
        builder.append(remarkCreateDate);
        builder.append(", remarkStatus=");
        builder.append(remarkStatus);
        builder.append(", remarkText=");
        builder.append(remarkText);
        builder.append(", specialty=");
        builder.append(specialty);
        builder.append(", srcSpecialty=");
        builder.append(srcSpecialty);
        builder.append(", srcSubspecialty=");
        builder.append(srcSubspecialty);
        builder.append(", subspecialty=");
        builder.append(subspecialty);
        builder.append(", suspend=");
        builder.append(suspend);
        builder.append(", tickdate=");
        builder.append(tickdate);
        builder.append(", ticknum=");
        builder.append(ticknum);
        builder.append(", transferPatient=");
        builder.append(transferPatient);
        builder.append(", uncollect=");
        builder.append(uncollect);
        builder.append(", version=");
        builder.append(version);
        builder.append(", wkstatcode=");
        builder.append(wkstatcode);
        builder.append(", workstore=");
        builder.append(workstore);
        builder.append("]");
        return builder.toString();
    }
}
