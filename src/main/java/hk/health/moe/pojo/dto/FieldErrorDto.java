package hk.health.moe.pojo.dto;

/**************************************************************************
 * NAME        : FieldErrorDto.java
 * VERSION     : 1.0.0
 * DATE        : 03-JUL-2019
 * DESCRIPTION : Work for ParamException
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       03-JUL-2019  Initial Version
 **************************************************************************/

public class FieldErrorDto {

    private String fieldName;
    private String errMsg;
    //eric 20191223 start--
    private Integer responseCode;
    //eric 20191223 end--

    public FieldErrorDto(){
    }

    public FieldErrorDto(String fieldName, String errMsg){
        this.fieldName = fieldName;
        this.errMsg = errMsg;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
    //eric 20191223 start--
    public FieldErrorDto(String fieldName, Integer responseCode,String errMsg){
        this.fieldName = fieldName;
        this.errMsg = errMsg;
        this.responseCode=responseCode;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }
    //eric 20191223 end--
}
