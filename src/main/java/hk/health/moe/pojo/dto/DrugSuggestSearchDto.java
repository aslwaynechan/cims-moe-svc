package hk.health.moe.pojo.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class DrugSuggestSearchDto {

    @NotBlank
    @Length(max = 256)
    private String searchString;
    private Integer limit;
    private String loginId;
    private boolean showDosage;

    private boolean showFav;
    @Pattern(regexp = "[P,A,C]")
    private String dosageType; //"A" for adult ,"P" for Paediatric,"C" for all、

    //Chris Add get from Redis flag 20191127  -Start
    private boolean isRedis;
    //Chris Add get from Redis flag 20191127  -End

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public boolean isShowDosage() {
        return showDosage;
    }

    public void setShowDosage(boolean showDosage) {
        this.showDosage = showDosage;
    }

    public boolean isShowFav() {
        return showFav;
    }

    public void setShowFav(boolean showFav) {
        this.showFav = showFav;
    }

    public String getDosageType() {
        return dosageType;
    }

    public void setDosageType(String dosageType) {
        this.dosageType = dosageType;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public boolean isRedis() {
        return isRedis;
    }

    public void setRedis(boolean redis) {
        isRedis = redis;
    }

}
