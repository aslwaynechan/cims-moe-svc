/**
 * Copyright © 2017 eHR HK Limited. All rights reserved.
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;

/**
 * @author TSL838
 */
public class MoeHtmlPrintResultDto implements Serializable/*, IsSerializable*/ {

    /**
     *
     */
    private static final long serialVersionUID = -6767072631807344751L;

    private String proxyUrl;
    private String url;

    public String getProxyUrl() {
        return proxyUrl;
    }

    public void setProxyUrl(String proxyUrl) {
        this.proxyUrl = proxyUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "MoeHtmlPrintResultDto [proxyUrl=" + proxyUrl + ", url=" + url
                + "]";
    }

}
