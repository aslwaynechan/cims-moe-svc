/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeMyFavouriteHdrDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MoeMyFavouriteHdrDto extends GenericDto implements Serializable/*, IsSerializable*/ {

    /**
     *
     */
    private static final long serialVersionUID = 6253994724489695330L;
    private String myFavouriteId;
    private String frontMyFavouriteId;
    private String createUserId;
    private String myFavouriteName;
    private Long rank;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private Date createDate;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private Date updateDate;
    private Long version;
    private List<MoeMedProfileDto> moeMedProfiles = new ArrayList<MoeMedProfileDto>();
    private String specialtyForMyFavourite;
    // Ricci 20191107 start --
    private String isCache;
    private String cacheId;
    // Ricci 20191107 end --

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getMyFavouriteName() {
        return myFavouriteName;
    }

    public void setMyFavouriteName(String myFavouriteName) {
        this.myFavouriteName = myFavouriteName;
    }

    public Long getRank() {
        return rank;
    }

    public void setRank(Long rank) {
        this.rank = rank;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateHosp() {
        return createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    public String getCreateRank() {
        return createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    public String getCreateRankDesc() {
        return createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateHosp() {
        return updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    public String getUpdateRank() {
        return updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public List<MoeMedProfileDto> getMoeMedProfiles() {
        return moeMedProfiles;
    }

    public void setMoeMedProfiles(List<MoeMedProfileDto> moeMedProfiles) {
        if (moeMedProfiles != null)
            this.moeMedProfiles = moeMedProfiles;
    }

    public void addMoeMedProfile(MoeMedProfileDto dto) {
        this.moeMedProfiles.add(dto);
    }

    public void setMyFavouriteId(String myFavouriteId) {
        this.myFavouriteId = myFavouriteId;
    }

    public String getMyFavouriteId() {
        return myFavouriteId;
    }

    public String getFrontMyFavouriteId() {
        return frontMyFavouriteId;
    }

    public void setFrontMyFavouriteId(String frontMyFavouriteId) {
        this.frontMyFavouriteId = frontMyFavouriteId;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getVersion() {
        return version;
    }

    public String getSpecialtyForMyFavourite() {
        return specialtyForMyFavourite;
    }

    public void setSpecialtyForMyFavourite(String specialtyForMyFavourite) {
        this.specialtyForMyFavourite = specialtyForMyFavourite;
    }

    public String getIsCache() {
        return isCache;
    }

    public void setIsCache(String isCache) {
        this.isCache = isCache;
    }

    public String getCacheId() {
        return cacheId;
    }

    public void setCacheId(String cacheId) {
        this.cacheId = cacheId;
    }


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MoeMyFavouriteHdrDto [myFavouriteId=");
        builder.append(myFavouriteId);
        builder.append(", frontMyFavouriteId=");
        builder.append(frontMyFavouriteId);
        builder.append(", createUserId=");
        builder.append(createUserId);
        builder.append(", myFavouriteName=");
        builder.append(myFavouriteName);
        builder.append(", rank=");
        builder.append(rank);
        builder.append(", createUser=");
        builder.append(createUser);
        builder.append(", createHosp=");
        builder.append(createHosp);
        builder.append(", createRank=");
        builder.append(createRank);
        builder.append(", createRankDesc=");
        builder.append(createRankDesc);
        builder.append(", createDate=");
        builder.append(createDate);
        builder.append(", updateUserId=");
        builder.append(updateUserId);
        builder.append(", updateUser=");
        builder.append(updateUser);
        builder.append(", updateHosp=");
        builder.append(updateHosp);
        builder.append(", updateRank=");
        builder.append(updateRank);
        builder.append(", updateRankDesc=");
        builder.append(updateRankDesc);
        builder.append(", updateDate=");
        builder.append(updateDate);
        builder.append(", version=");
        builder.append(version);
        builder.append(", moeMedProfiles=");
        builder.append(moeMedProfiles);
        builder.append(", specialtyForMyFavourite=");
        builder.append(specialtyForMyFavourite);
        builder.append("]");
        return builder.toString();
    }
}
