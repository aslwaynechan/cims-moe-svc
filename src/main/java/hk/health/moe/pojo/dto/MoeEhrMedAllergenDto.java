/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeEhrMedAllergenDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import org.hibernate.validator.constraints.Length;

import java.io.Serializable;
import java.util.Date;

public class MoeEhrMedAllergenDto implements Serializable/*, IsSerializable*/ {

    /**
     *
     */
    private static final long serialVersionUID = 4878171910486290048L;

    private Date ackDate;
    private String allergen;
    @Length(max = 1)
    private String matchType;
    private String screenMsg;
    @Length(max = 1)
    private String certainty;
    private String additionInfo;
    @Length(max = 255)
    private String overrideReason;
    private String ackBy;
    private String manifestation;
    @Length(max = 1)
    private String overrideStatus = "A";
    private Long version;

    public Date getAckDate() {
        return ackDate;
    }

    public void setAckDate(Date ackDate) {
        this.ackDate = ackDate;
    }

    public String getAllergen() {
        return allergen;
    }

    public void setAllergen(String allergen) {
        this.allergen = allergen;
    }

    public String getMatchType() {
        return matchType;
    }

    public void setMatchType(String matchType) {
        this.matchType = matchType;
    }

    public String getScreenMsg() {
        return screenMsg;
    }

    public void setScreenMsg(String screenMsg) {
        this.screenMsg = screenMsg;
    }

    public String getCertainty() {
        return certainty;
    }

    public void setCertainty(String certainty) {
        this.certainty = certainty;
    }

    public String getAdditionInfo() {
        return additionInfo;
    }

    public void setAdditionInfo(String additionInfo) {
        this.additionInfo = additionInfo;
    }

    public String getOverrideReason() {
        return overrideReason;
    }

    public void setOverrideReason(String overrideReason) {
        this.overrideReason = overrideReason;
    }

    public String getAckBy() {
        return ackBy;
    }

    public void setAckBy(String ackBy) {
        this.ackBy = ackBy;
    }

    public String getManifestation() {
        return manifestation;
    }

    public void setManifestation(String manifestation) {
        this.manifestation = manifestation;
    }

    public String getOverrideStatus() {
        return overrideStatus;
    }

    public void setOverrideStatus(String overrideStatus) {
        this.overrideStatus = overrideStatus;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getVersion() {
        return version;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MoeEhrMedAllergenDTO [ackBy=");
        builder.append(ackBy);
        builder.append(", ackDate=");
        builder.append(ackDate);
        builder.append(", additionInfo=");
        builder.append(additionInfo);
        builder.append(", allergen=");
        builder.append(allergen);
        builder.append(", certainty=");
        builder.append(certainty);
        builder.append(", manifestation=");
        builder.append(manifestation);
        builder.append(", matchType=");
        builder.append(matchType);
        builder.append(", overrideReason=");
        builder.append(overrideReason);
        builder.append(", overrideStatus=");
        builder.append(overrideStatus);
        builder.append(", screenMsg=");
        builder.append(screenMsg);
        builder.append(", version=");
        builder.append(version);
        builder.append("]");
        return builder.toString();
    }
}
