/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeDrugOrderPropertyDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;

public class MoeDrugOrderPropertyDto implements Serializable/*, IsSerializable*/ {

    /**
     *
     */
    private static final long serialVersionUID = 377068836029874177L;

    private Double minimumDosage;
    private Long maximumDuration;

    public MoeDrugOrderPropertyDto() {
    }

    public MoeDrugOrderPropertyDto(Double minimumDosage, Long maximumDuration) {
        this.minimumDosage = minimumDosage;
        this.maximumDuration = maximumDuration;
    }

    public Double getMinimumDosage() {
        return minimumDosage;
    }

    public void setMinimumDosage(Double minimumDosage) {
        this.minimumDosage = minimumDosage;
    }

    public Long getMaximumDuration() {
        return maximumDuration;
    }

    public void setMaximumDuration(Long maximumDuration) {
        this.maximumDuration = maximumDuration;
    }
}
