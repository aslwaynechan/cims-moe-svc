package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MoeMedicationDecisionResultDto implements /*IsSerializable,*/ Serializable {

    private static final long serialVersionUID = 1513210070593921675L;
    //private String orderLineRowNum;
    private MoePatientMedicationDto patientMedication;
    private List<InnerMoePatientAllergyDto> patientAllergies;
    //private String returnMessage;
    //private String returnMessageWithoutStrength;
    private String acceptId;

    public MoePatientMedicationDto getPatientMedication() {
        return patientMedication;
    }

    public void setPatientMedication(MoePatientMedicationDto patientMedication) {
        this.patientMedication = patientMedication;
    }

    public List<InnerMoePatientAllergyDto> getPatientAllergies() {
        if (null == patientAllergies) {
            patientAllergies = new ArrayList<>();
        }
        return patientAllergies;
    }

    public void setPatientAllergies(List<InnerMoePatientAllergyDto> patientAllergies) {
        this.patientAllergies = patientAllergies;
    }

/*    public String getOrderLineRowNum() {
        return orderLineRowNum;
    }

    public void setOrderLineRowNum(String orderLineRowNum) {
        this.orderLineRowNum = orderLineRowNum;
    }*/


/*    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public String getReturnMessageWithoutStrength() {
        return returnMessageWithoutStrength;
    }

    public void setReturnMessageWithoutStrength(String returnMessageWithoutStrength) {
        this.returnMessageWithoutStrength = returnMessageWithoutStrength;
    }*/

    public String getAcceptId() {
        return acceptId;
    }

    public void setAcceptId(String acceptId) {
        this.acceptId = acceptId;
    }
}
