
package hk.health.moe.pojo.dto.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 *
 *
 * <pre>
 * &lt;complexType name="OrderListRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hospCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="patientKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="episodeNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="episodeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="startSearchDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="endSearchDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderListRequest", namespace = "http://ehr.gov.hk/moe/ws/beans", propOrder = {
    "hospCode",
    "patientKey",
    "episodeNo",
    "episodeType",
    "startSearchDate",
    "endSearchDate"
})
public class OrderListRequest {

    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    protected String hospCode;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    protected String patientKey;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    protected String episodeNo;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    protected String episodeType;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startSearchDate;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endSearchDate;

    public String getHospCode() {
        return hospCode;
    }


    public void setHospCode(String value) {
        this.hospCode = value;
    }


    public String getPatientKey() {
        return patientKey;
    }


    public void setPatientKey(String value) {
        this.patientKey = value;
    }


    public String getEpisodeNo() {
        return episodeNo;
    }


    public void setEpisodeNo(String value) {
        this.episodeNo = value;
    }


    public String getEpisodeType() {
        return episodeType;
    }


    public void setEpisodeType(String value) {
        this.episodeType = value;
    }


    public XMLGregorianCalendar getStartSearchDate() {
        return startSearchDate;
    }


    public void setStartSearchDate(XMLGregorianCalendar value) {
        this.startSearchDate = value;
    }


    public XMLGregorianCalendar getEndSearchDate() {
        return endSearchDate;
    }


    public void setEndSearchDate(XMLGregorianCalendar value) {
        this.endSearchDate = value;
    }

}
