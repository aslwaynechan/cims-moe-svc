/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  MOE Drug Maintenance
 *
 * PROGRAM NAME    :  MoeDrugAliasNameLocalDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

import java.util.Date;

public class MoeDrugAliasNameLocalDto implements java.io.Serializable {

    private String aliasNameId;
    private Long version;
    private String hkRegNo;
    private String aliasName;
    private String aliasNameType;
    private String createUserId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private Date createDtm;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private Date updateDtm;

    public MoeDrugAliasNameLocalDto() {
    }

    public MoeDrugAliasNameLocalDto(String aliasNameId, String hkRegNo,
                                    String aliasName, String aliasNameType, String createHosp,
                                    Date createDtm, Date updateDtm) {
        this.aliasNameId = aliasNameId;
        this.hkRegNo = hkRegNo;
        this.aliasName = aliasName;
        this.aliasNameType = aliasNameType;
        this.createHosp = createHosp;
        this.createDtm = createDtm;
        this.updateDtm = updateDtm;
    }

    public MoeDrugAliasNameLocalDto(String aliasNameId, String hkRegNo,
                                    String aliasName, String aliasNameType, String createUserId,
                                    String createUser, String createHosp, String createRank,
                                    String createRankDesc, Date createDtm, String updateUserId,
                                    String updateUser, String updateHosp, String updateRank,
                                    String updateRankDesc, Date updateDtm) {
        this.aliasNameId = aliasNameId;
        this.hkRegNo = hkRegNo;
        this.aliasName = aliasName;
        this.aliasNameType = aliasNameType;
        this.createUserId = createUserId;
        this.createUser = createUser;
        this.createHosp = createHosp;
        this.createRank = createRank;
        this.createRankDesc = createRankDesc;
        this.createDtm = createDtm;
        this.updateUserId = updateUserId;
        this.updateUser = updateUser;
        this.updateHosp = updateHosp;
        this.updateRank = updateRank;
        this.updateRankDesc = updateRankDesc;
        this.updateDtm = updateDtm;
    }

    public String getAliasNameId() {
        return this.aliasNameId;
    }

    public void setAliasNameId(String aliasNameId) {
        this.aliasNameId = aliasNameId;
    }

    public Long getVersion() {
        return this.version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getHkRegNo() {
        return this.hkRegNo;
    }

    public void setHkRegNo(String hkRegNo) {
        this.hkRegNo = hkRegNo;
    }

    public String getAliasName() {
        return this.aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getAliasNameType() {
        return this.aliasNameType;
    }

    public void setAliasNameType(String aliasNameType) {
        this.aliasNameType = aliasNameType;
    }

    public String getCreateUserId() {
        return this.createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUser() {
        return this.createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateHosp() {
        return this.createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    public String getCreateRank() {
        return this.createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    public String getCreateRankDesc() {
        return this.createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    public Date getCreateDtm() {
        return this.createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    public String getUpdateUserId() {
        return this.updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getUpdateUser() {
        return this.updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateHosp() {
        return this.updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    public String getUpdateRank() {
        return this.updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    public String getUpdateRankDesc() {
        return this.updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    public Date getUpdateDtm() {
        return this.updateDtm;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }

}
