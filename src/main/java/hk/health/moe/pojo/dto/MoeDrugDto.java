/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeDrugDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MoeDrugDto extends GenericDto implements Serializable/*, IsSerializable*/ {

    public MoeDrugDto() {
        super();
    }

    public MoeDrugDto(String hkRegNo, String vtm) {
        super();
        setHkRegNo(hkRegNo);
        setVtm(vtm);
    }

    /**
     *
     */
    private static final long serialVersionUID = 6821673874533005461L;

    // Moe Drug
    private String hkRegNo;
    private String localDrugId;
    private String tradeName;
    private Long tradeNameVtmId;
    private String vtm;
    private Long vtmId;
    private String baseUnit;
    private String prescribeUnit;
    private String dispenseUnit;
    private Long legalClassId;
    private String legalClass;
    private String manufacturer;
    private String genericIndicator;
    private String strengthCompulsory;
    private String allergyCheckFlag;
    // Moe Drug Alias Name
    private List<MoeDrugAliasNameDto> aliasNames = new ArrayList<MoeDrugAliasNameDto>();
    // Moe Common Dosage
    private List<MoeCommonDosageDto> commonDosages = new ArrayList<MoeCommonDosageDto>();
    // Moe Form
    private MoeFormDto form;
    private String formEng;
    private Long doseFormExtraInfoId;
    private String doseFormExtraInfo;
    // Moe Route
    private MoeRouteDto route;
    private String routeEng;
    // Moe Screen Display for med profile use
    private String screenDisplay;
    // Moe Base Unit
    private MoeBaseUnitDto prescribeUnitId;
    private MoeBaseUnitDto baseUnitId;
    private MoeBaseUnitDto dispenseUnitId;
    // Moe Drug Strength
    private List<MoeDrugStrengthDto> strengths = new ArrayList<MoeDrugStrengthDto>();
    private List<String> ingredientList = null;
    // Drug id
    private Long vtmRouteFormId;
    private Long tradeVtmRouteFormId;
    private String tradeNameAlias;
    private String terminologyName;
    // Moe Drug Ord Property
    private String drugCategoryId;

    //Simon 20190730 --strat for dangerous drug
    private String dangerDrug;

    //Chris 20191119 For Redis Id  &  Drug Category  &  Drug Type  --Start
    //    @JsonIgnore
    private String redisKey;

    //    @JsonIgnore
    private String drugCategory;

    //    @JsonIgnore
    private String drugType;
    //Chris 20191119 For Redis Id  &  Drug Category  &  Drug Type  --End

    //Chris For keep the sort of drugSearch's result from Redis  -Start
    private int indexInType;
    //Chris For keep the sort of drugSearch's result from Redis  -End

    public int getIndexInType() {
        return indexInType;
    }

    public void setIndexInType(int indexInType) {
        this.indexInType = indexInType;
    }

    public String getRedisKey() {
        return redisKey;
    }

    public void setRedisKey(String redisKey) {
        this.redisKey = redisKey;
    }

    public String getDrugCategory() {
        return drugCategory;
    }

    public void setDrugCategory(String drugCategory) {
        this.drugCategory = drugCategory;
    }

    public String getDrugType() {
        return drugType;
    }

    public void setDrugType(String drugType) {
        this.drugType = drugType;
    }

    public String getDangerDrug() {
        return dangerDrug;
    }

    public void setDangerDrug(String dangerDrug) {
        this.dangerDrug = dangerDrug;
    }

    //Simon 20190730 --end for dangerous drug
    //Simon 20190828 start--
    private MoeDrugOrdPropertyLocalDto moeDrugOrdPropertyLocal;

    public MoeDrugOrdPropertyLocalDto getMoeDrugOrdPropertyLocal() {
        return moeDrugOrdPropertyLocal;
    }

    public void setMoeDrugOrdPropertyLocal(MoeDrugOrdPropertyLocalDto moeDrugOrdPropertyLocal) {
        this.moeDrugOrdPropertyLocal = moeDrugOrdPropertyLocal;
    }
    //Simon 20190828 end--

    // Simon 20191028 start--
    private Long version;

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
    //Simon 20191028 end--

    public List<String> getIngredientList() {
        return ingredientList;
    }

    public void setIngredientList(List<String> ingredientList) {
        this.ingredientList = ingredientList;
    }

    public boolean hasCommonDosage() {
        return commonDosages.size() > 0;
    }

    public String getHkRegNo() {
        return hkRegNo;
    }

    public void setHkRegNo(String hkRegNo) {
        this.hkRegNo = hkRegNo;
    }

    public String getLocalDrugId() {
        return localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        // Ricci 20191009 start --
        this.localDrugId = StringUtils.isNotBlank(localDrugId) ? localDrugId.toUpperCase() : localDrugId;
        // Ricci 20191009 end --
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public Long getTradeNameVtmId() {
        return tradeNameVtmId;
    }

    public void setTradeNameVtmId(Long tradeNameVtmId) {
        this.tradeNameVtmId = tradeNameVtmId;
    }

    public String getVtm() {
        return vtm;
    }

    public void setVtm(String vtm) {
        this.vtm = vtm;
        if (vtm != null && vtm.indexOf("+") > -1) {
            String[] ingredients = vtm.split("\\+");
            ingredientList = new ArrayList<String>();
            for (int i = 0; i < ingredients.length; i++) {
                ingredientList.add(ingredients[i].trim());
            }
        }
    }

    public Long getVtmId() {
        return vtmId;
    }

    public void setVtmId(Long vtmId) {
        this.vtmId = vtmId;
    }

    public String getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(String baseUnit) {
        this.baseUnit = baseUnit;
    }

    public String getPrescribeUnit() {
        return prescribeUnit;
    }

    public void setPrescribeUnit(String prescribeUnit) {
        this.prescribeUnit = prescribeUnit;
    }

    public String getDispenseUnit() {
        return dispenseUnit;
    }

    public void setDispenseUnit(String dispenseUnit) {
        this.dispenseUnit = dispenseUnit;
    }

    public String getLegalClass() {
        return legalClass;
    }

    public void setLegalClass(String legalClass) {
        this.legalClass = legalClass;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getGenericIndicator() {
        return genericIndicator;
    }

    public void setGenericIndicator(String genericIndicator) {
        this.genericIndicator = genericIndicator;
    }

    public void setStrengthCompulsory(String strengthCompulsory) {
        this.strengthCompulsory = strengthCompulsory;
    }

    public String getStrengthCompulsory() {
        return strengthCompulsory;
    }

    public void setAllergyCheckFlag(String allergyCheckFlag) {
        this.allergyCheckFlag = allergyCheckFlag;
    }

    public String getAllergyCheckFlag() {
        return allergyCheckFlag;
    }

    public List<MoeDrugAliasNameDto> getAliasNames() {
        return aliasNames;
    }

    public void setAliasNames(List<MoeDrugAliasNameDto> aliasNames) {
        if (aliasNames != null)
            this.aliasNames = aliasNames;
    }

    public void addAliasName(MoeDrugAliasNameDto aliasName) {
        this.aliasNames.add(aliasName);
    }

    public List<MoeCommonDosageDto> getCommonDosages() {
        return commonDosages;
    }

    public void setCommonDosages(List<MoeCommonDosageDto> commonDosages) {
        if (commonDosages != null)
            this.commonDosages = commonDosages;
    }

    public void addCommonDosage(MoeCommonDosageDto commonDosage) {
        this.commonDosages.add(commonDosage);
    }

    public MoeFormDto getForm() {
        return form;
    }

    public void setForm(MoeFormDto form) {
        this.form = form;
    }

    public MoeRouteDto getRoute() {
        return route;
    }

    public void setRoute(MoeRouteDto route) {
        this.route = route;
    }

    public String getScreenDisplay() {
        return screenDisplay;
    }

    public void setScreenDisplay(String screenDisplay) {
        this.screenDisplay = screenDisplay;
    }

    public Long getLegalClassId() {
        return legalClassId;
    }

    public void setLegalClassId(Long legalClassId) {
        this.legalClassId = legalClassId;
    }

    public String getFormEng() {
        return formEng;
    }

    public void setFormEng(String formEng) {
        this.formEng = formEng;
    }

    public void setDoseFormExtraInfoId(Long doseFormExtraInfoId) {
        this.doseFormExtraInfoId = doseFormExtraInfoId;
    }

    public Long getDoseFormExtraInfoId() {
        return doseFormExtraInfoId;
    }

    public String getDoseFormExtraInfo() {
        return doseFormExtraInfo;
    }

    public void setDoseFormExtraInfo(String doseFormExtraInfo) {
        this.doseFormExtraInfo = doseFormExtraInfo;
    }

    public String getRouteEng() {
        return routeEng;
    }

    public void setRouteEng(String routeEng) {
        this.routeEng = routeEng;
    }

    public MoeBaseUnitDto getPrescribeUnitId() {
        return prescribeUnitId;
    }

    public void setPrescribeUnitId(MoeBaseUnitDto prescribeUnitId) {
        this.prescribeUnitId = prescribeUnitId;
    }

    public MoeBaseUnitDto getBaseUnitId() {
        return baseUnitId;
    }

    public void setBaseUnitId(MoeBaseUnitDto baseUnitId) {
        this.baseUnitId = baseUnitId;
    }

    public MoeBaseUnitDto getDispenseUnitId() {
        return dispenseUnitId;
    }

    public void setDispenseUnitId(MoeBaseUnitDto dispenseUnitId) {
        this.dispenseUnitId = dispenseUnitId;
    }

    public List<MoeDrugStrengthDto> getStrengths() {
        return strengths;
    }

    public void setStrengths(List<MoeDrugStrengthDto> strengths) {
        if (strengths != null)
            this.strengths = strengths;
    }

    public void addStrength(MoeDrugStrengthDto str) {
        this.strengths.add(str);
    }

    public Long getVtmRouteFormId() {
        return vtmRouteFormId;
    }

    public void setVtmRouteFormId(Long vtmRouteFormId) {
        this.vtmRouteFormId = vtmRouteFormId;
    }

    public Long getTradeVtmRouteFormId() {
        return tradeVtmRouteFormId;
    }

    public void setTradeVtmRouteFormId(Long tradeVtmRouteFormId) {
        this.tradeVtmRouteFormId = tradeVtmRouteFormId;
    }

    public static boolean isMultiIngr(String vtm) {
        if (vtm == null) return false;
        if (vtm.indexOf("+") == -1) return false;
        if (vtm.indexOf("(") < vtm.indexOf("+") && vtm.indexOf("+") < vtm.indexOf(")")) return false;
        return true;
    }

    public String getTradeNameAlias() {
        return tradeNameAlias;
    }

    public void setTradeNameAlias(String tradeNameAlias) {
        this.tradeNameAlias = tradeNameAlias;
    }

    public String getTerminologyName() {
        return terminologyName;
    }

    public void setTerminologyName(String terminologyName) {
        this.terminologyName = terminologyName;
    }

    public String getDrugCategoryId() {
        return drugCategoryId;
    }

    public void setDrugCategoryId(String drugCategoryId) {
        this.drugCategoryId = drugCategoryId;
    }

   /* @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDrugDto that = (MoeDrugDto) o;

        if (!hkRegNo.equals(that.hkRegNo)) return false;
        if (!localDrugId.equals(that.localDrugId)) return false;
        if (tradeName != null ? !tradeName.equals(that.tradeName) : that.tradeName != null) return false;
        if (tradeNameVtmId != null ? !tradeNameVtmId.equals(that.tradeNameVtmId) : that.tradeNameVtmId != null)
            return false;
        if (vtm != null ? !vtm.equals(that.vtm) : that.vtm != null) return false;
        if (vtmId != null ? !vtmId.equals(that.vtmId) : that.vtmId != null) return false;
        if (baseUnit != null ? !baseUnit.equals(that.baseUnit) : that.baseUnit != null) return false;
        if (prescribeUnit != null ? !prescribeUnit.equals(that.prescribeUnit) : that.prescribeUnit != null)
            return false;
        if (dispenseUnit != null ? !dispenseUnit.equals(that.dispenseUnit) : that.dispenseUnit != null) return false;
        if (legalClassId != null ? !legalClassId.equals(that.legalClassId) : that.legalClassId != null) return false;
        if (legalClass != null ? !legalClass.equals(that.legalClass) : that.legalClass != null) return false;
        if (manufacturer != null ? !manufacturer.equals(that.manufacturer) : that.manufacturer != null) return false;
        if (genericIndicator != null ? !genericIndicator.equals(that.genericIndicator) : that.genericIndicator != null)
            return false;
        if (strengthCompulsory != null ? !strengthCompulsory.equals(that.strengthCompulsory) : that.strengthCompulsory != null)
            return false;
        if (allergyCheckFlag != null ? !allergyCheckFlag.equals(that.allergyCheckFlag) : that.allergyCheckFlag != null)
            return false;
//        if (aliasNames != null ? !aliasNames.equals(that.aliasNames) : that.aliasNames != null) return false;
//        if (commonDosages != null ? !commonDosages.equals(that.commonDosages) : that.commonDosages != null) return false;
        //if (form != null ? !form.equals(that.form) : that.form != null) return false;
        if (formEng != null ? !formEng.equals(that.formEng) : that.formEng != null) return false;
        if (doseFormExtraInfoId != null ? !doseFormExtraInfoId.equals(that.doseFormExtraInfoId) : that.doseFormExtraInfoId != null)
            return false;
        if (doseFormExtraInfo != null ? !doseFormExtraInfo.equals(that.doseFormExtraInfo) : that.doseFormExtraInfo != null)
            return false;
        if (route != null ? !route.equals(that.route) : that.route != null) return false;
        if (routeEng != null ? !routeEng.equals(that.routeEng) : that.routeEng != null) return false;
        if (screenDisplay != null ? !screenDisplay.equals(that.screenDisplay) : that.screenDisplay != null)
            return false;
        if (prescribeUnitId != null ? !prescribeUnitId.equals(that.prescribeUnitId) : that.prescribeUnitId != null)
            return false;
        if (baseUnitId != null ? !baseUnitId.equals(that.baseUnitId) : that.baseUnitId != null) return false;
        if (dispenseUnitId != null ? !dispenseUnitId.equals(that.dispenseUnitId) : that.dispenseUnitId != null)
            return false;
//        if (strengths != null ? !strengths.equals(that.strengths) : that.strengths != null) return false;
//        if (ingredientList != null ? !ingredientList.equals(that.ingredientList) : that.ingredientList != null) return false;
        if (vtmRouteFormId != null ? !vtmRouteFormId.equals(that.vtmRouteFormId) : that.vtmRouteFormId != null)
            return false;
        if (tradeVtmRouteFormId != null ? !tradeVtmRouteFormId.equals(that.tradeVtmRouteFormId) : that.tradeVtmRouteFormId != null)
            return false;
        if (tradeNameAlias != null ? !tradeNameAlias.equals(that.tradeNameAlias) : that.tradeNameAlias != null)
            return false;
        if (terminologyName != null ? !terminologyName.equals(that.terminologyName) : that.terminologyName != null)
            return false;
        if (drugCategoryId != null ? !drugCategoryId.equals(that.drugCategoryId) : that.drugCategoryId != null)
            return false;
//        if (dangerDrug != null ? !dangerDrug.equals(that.dangerDrug) : that.dangerDrug != null) return false;
        return dangerDrug != null ? dangerDrug.equals(that.dangerDrug) : that.dangerDrug == null;
//        return moeDrugOrdPropertyLocal != null ? moeDrugOrdPropertyLocal.equals(that.moeDrugOrdPropertyLocal) : that.moeDrugOrdPropertyLocal == null;
    }

    @Override
    public int hashCode() {
        int result = hkRegNo.hashCode();
        result = 31 * result + localDrugId.hashCode();
        result = 31 * result + (tradeName != null ? tradeName.hashCode() : 0);
        result = 31 * result + (tradeNameVtmId != null ? tradeNameVtmId.hashCode() : 0);
        result = 31 * result + (vtm != null ? vtm.hashCode() : 0);
        result = 31 * result + (vtmId != null ? vtmId.hashCode() : 0);
        result = 31 * result + (baseUnit != null ? baseUnit.hashCode() : 0);
        result = 31 * result + (prescribeUnit != null ? prescribeUnit.hashCode() : 0);
        result = 31 * result + (dispenseUnit != null ? dispenseUnit.hashCode() : 0);
        result = 31 * result + (legalClassId != null ? legalClassId.hashCode() : 0);
        result = 31 * result + (legalClass != null ? legalClass.hashCode() : 0);
        result = 31 * result + (manufacturer != null ? manufacturer.hashCode() : 0);
        result = 31 * result + (genericIndicator != null ? genericIndicator.hashCode() : 0);
        result = 31 * result + (strengthCompulsory != null ? strengthCompulsory.hashCode() : 0);
        result = 31 * result + (allergyCheckFlag != null ? allergyCheckFlag.hashCode() : 0);
        result = 31 * result + (aliasNames != null ? aliasNames.hashCode() : 0);
        result = 31 * result + (commonDosages != null ? commonDosages.hashCode() : 0);
        result = 31 * result + (form != null ? form.hashCode() : 0);
        result = 31 * result + (formEng != null ? formEng.hashCode() : 0);
        result = 31 * result + (doseFormExtraInfoId != null ? doseFormExtraInfoId.hashCode() : 0);
        result = 31 * result + (doseFormExtraInfo != null ? doseFormExtraInfo.hashCode() : 0);
        result = 31 * result + (route != null ? route.hashCode() : 0);
        result = 31 * result + (routeEng != null ? routeEng.hashCode() : 0);
        result = 31 * result + (screenDisplay != null ? screenDisplay.hashCode() : 0);
        result = 31 * result + (prescribeUnitId != null ? prescribeUnitId.hashCode() : 0);
        result = 31 * result + (baseUnitId != null ? baseUnitId.hashCode() : 0);
        result = 31 * result + (dispenseUnitId != null ? dispenseUnitId.hashCode() : 0);
        result = 31 * result + (strengths != null ? strengths.hashCode() : 0);
        result = 31 * result + (ingredientList != null ? ingredientList.hashCode() : 0);
        result = 31 * result + (vtmRouteFormId != null ? vtmRouteFormId.hashCode() : 0);
        result = 31 * result + (tradeVtmRouteFormId != null ? tradeVtmRouteFormId.hashCode() : 0);
        result = 31 * result + (tradeNameAlias != null ? tradeNameAlias.hashCode() : 0);
        result = 31 * result + (terminologyName != null ? terminologyName.hashCode() : 0);
        result = 31 * result + (drugCategoryId != null ? drugCategoryId.hashCode() : 0);
        result = 31 * result + (dangerDrug != null ? dangerDrug.hashCode() : 0);
        result = 31 * result + (moeDrugOrdPropertyLocal != null ? moeDrugOrdPropertyLocal.hashCode() : 0);
        return result;
    }*/

    @Override
    public int hashCode() {
        int result = hkRegNo != null ? hkRegNo.hashCode() : 0;
        result = 31 * result + (localDrugId != null ? localDrugId.hashCode() : 0);
        result = 31 * result + (tradeName != null ? tradeName.hashCode() : 0);
        result = 31 * result + (tradeNameVtmId != null ? tradeNameVtmId.hashCode() : 0);
        result = 31 * result + (vtm != null ? vtm.hashCode() : 0);
        result = 31 * result + (vtmId != null ? vtmId.hashCode() : 0);
        result = 31 * result + (baseUnit != null ? baseUnit.hashCode() : 0);
        result = 31 * result + (prescribeUnit != null ? prescribeUnit.hashCode() : 0);
        result = 31 * result + (dispenseUnit != null ? dispenseUnit.hashCode() : 0);
//            result = 31 * result + (strengthCompulsory != null ? strengthCompulsory.hashCode() : 0);
//            result = 31 * result + (allergyCheckFlag != null ? allergyCheckFlag.hashCode() : 0);
//            result = 31 * result + (formEng != null ? formEng.hashCode() : 0);
//            result = 31 * result + (routeEng != null ? routeEng.hashCode() : 0);
//            result = 31 * result + (screenDisplay != null ? screenDisplay.hashCode() : 0);
//            result = 31 * result + (tradeVtmRouteFormId != null ? tradeVtmRouteFormId.hashCode() : 0);
//            result = 31 * result + (terminologyName != null ? terminologyName.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDrugDto that = (MoeDrugDto) o;

        //TODO
        if (hkRegNo != null ? !hkRegNo.equals(that.hkRegNo) : that.hkRegNo != null) return false;
        if (localDrugId != null ? !localDrugId.equals(that.localDrugId) : that.localDrugId != null) return false;

        if (tradeName != null ? !tradeName.equals(that.tradeName) : that.tradeName != null) return false;
        if (tradeNameVtmId != null ? !tradeNameVtmId.equals(that.tradeNameVtmId) : that.tradeNameVtmId != null) return false;
        if (vtm != null ? !vtm.equals(that.vtm) : that.vtm != null) return false;
        if (vtmId != null ? !vtmId.equals(that.vtmId) : that.vtmId != null) return false;
        if (baseUnit != null ? !baseUnit.equals(that.baseUnit) : that.baseUnit != null) return false;
//        if (prescribeUnit != null ? !prescribeUnit.equals(that.prescribeUnit) : that.prescribeUnit != null) return false;
        return prescribeUnit != null ? prescribeUnit.equals(that.prescribeUnit) : that.prescribeUnit == null;
//        if (dispenseUnit != null ? !dispenseUnit.equals(that.dispenseUnit) : that.dispenseUnit != null) return false;
//        if (strengthCompulsory != null ? !strengthCompulsory.equals(that.strengthCompulsory) : that.strengthCompulsory != null) return false;
//        if (allergyCheckFlag != null ? !allergyCheckFlag.equals(that.allergyCheckFlag) : that.allergyCheckFlag != null) return false;
//        if (formEng != null ? !formEng.equals(that.formEng) : that.formEng != null) return false;
//        if (routeEng != null ? !routeEng.equals(that.routeEng) : that.routeEng != null) return false;
//        if (screenDisplay != null ? !screenDisplay.equals(that.screenDisplay) : that.screenDisplay != null) return false;
//        if (tradeVtmRouteFormId != null ? !tradeVtmRouteFormId.equals(that.tradeVtmRouteFormId) : that.tradeVtmRouteFormId != null) return false;
//        return terminologyName != null ? terminologyName.equals(that.terminologyName) : that.terminologyName == null;
    }


    //Chris 20191213 For Sort after match data from Redis  -Start
/*    @Override
    public int compareTo(MoeDrugDto o) {
        int thisVal = this.getIndexInType();
        int anotherVal = o.getIndexInType();
        return (thisVal<anotherVal ? -1 : (thisVal==anotherVal ? 0 : 1));
    }*/
    //Chris 20191213 For Sort after match data from Redis  -End

}
