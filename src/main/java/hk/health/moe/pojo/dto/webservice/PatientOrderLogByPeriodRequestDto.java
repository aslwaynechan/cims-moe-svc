package hk.health.moe.pojo.dto.webservice;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @Author Simon
 * @Date 2019/11/13
 */
public class PatientOrderLogByPeriodRequestDto {
    @NotBlank
    private String patientKey;
    private Date searchStartTime;
    private Date searchEndTime;
    private String uploadMode;

    public Date getSearchStartTime() {
        return searchStartTime;
    }

    public void setSearchStartTime(Date searchStartTime) {
        this.searchStartTime = searchStartTime;
    }

    public Date getSearchEndTime() {
        return searchEndTime;
    }

    public void setSearchEndTime(Date searchEndTime) {
        this.searchEndTime = searchEndTime;
    }

    public String getPatientKey() {
        return patientKey;
    }

    public void setPatientKey(String patientKey) {
        this.patientKey = patientKey;
    }

    public String getUploadMode() {
        return uploadMode;
    }

    public void setUploadMode(String uploadMode) {
        this.uploadMode = uploadMode;
    }
}
