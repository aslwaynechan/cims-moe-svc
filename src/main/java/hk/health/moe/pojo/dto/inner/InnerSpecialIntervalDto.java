package hk.health.moe.pojo.dto.inner;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2019/08/06
 */
public class InnerSpecialIntervalDto {

    //MoeMedProfileDTO
    @NotBlank
    private String supFreqCode;
    @NotBlank
    private String regimen;
    //Chris 20190909 Change type String to List<String> for "on odd/even day" case  -Start
    //private String freqCode;
    private List<String> freqCode;
    //Chris 20190909 Change type String to List<String> for "on odd/even day" case  -End
    private Long supFreq1;
    private Long supFreq2;
    private String dayOfWeek;
    //MoeEhrMedProfileDTO
    private Long cycleMultiplier;


    public String getSupFreqCode() {
        return supFreqCode;
    }

    public void setSupFreqCode(String supFreqCode) {
        this.supFreqCode = supFreqCode;
    }

    public String getRegimen() {
        return regimen;
    }

    public void setRegimen(String regimen) {
        this.regimen = regimen;
    }

    public List<String> getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(List<String> freqCode) {
        this.freqCode = freqCode;
    }

    public Long getSupFreq1() {
        return supFreq1;
    }

    public void setSupFreq1(Long supFreq1) {
        this.supFreq1 = supFreq1;
    }

    public Long getSupFreq2() {
        return supFreq2;
    }

    public void setSupFreq2(Long supFreq2) {
        this.supFreq2 = supFreq2;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Long getCycleMultiplier() {
        return cycleMultiplier;
    }

    public void setCycleMultiplier(Long cycleMultiplier) {
        this.cycleMultiplier = cycleMultiplier;
    }
}
