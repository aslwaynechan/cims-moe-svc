package hk.health.moe.pojo.dto.inner;

import java.io.Serializable;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/7/30
 */
public class InnerListMyFavouriteDto implements Serializable {
    private static final long serialVersionUID = 4406787762721454797L;
    private String userId;

    private String searchString;

    private boolean department = false;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public boolean isDepartment() {
        return department;
    }

    public void setDepartment(boolean department) {
        this.department = department;
    }

}
