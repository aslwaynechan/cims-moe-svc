package hk.health.moe.pojo.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.HashMap;

public class UserDto {

    // Service Information start --
    @NotBlank
    @Length(max = 100)
    private String systemLogin;
    @Length(max = 5)
    private String sourceSystem;
    // Service Information end --
    // User Information start --
    @NotBlank
    @Length(max = 50)
    private String loginId;
    @Length(max = 255)
    private String loginName;
    @Length(max = 10)
    private String userRankCd;
    @Length(max = 48)
    private String userRankDesc;
    //@NotBlank
    @Length(max = 20)
    private String hospitalCd;
    @Length(max = 255)
    private String hospitalName;
    @Length(max = 100)
    private String actionCd;
    @Length(max = 1)
    private String enableHtmlPrinting;
    // User Information end --
    // Patient Information start --
    @Length(max = 20)
    private String mrnDisplayLabelP;
    @Length(max = 20)
    private String mrnDisplayLabelE;
    @Length(max = 1)
    private String mrnDisplayTypeCd;
    @Length(max = 30)
    private String hkid;
    //@NotBlank
    @Length(max = 50)
    private String mrnPatientIdentity;
    //@NotBlank
    @Length(max = 20)
    private String mrnPatientEncounterNum;
    @Min(value = 0)
    private Long orderNum;
    //@NotBlank
    @Length(max = 1)
    private String patientTypeCd;
    @Length(max = 50)
    private String patientStatus;
    @Length(max = 20)
    private String patientStatusLabel;
    //@NotBlank
    @Length(max = 1)
    private String prescTypeCd;
    @Length(max = 12)
    private String ehrNum;
    @Length(max = 6)
    private String docTypeCd;
    @Length(max = 30)
    private String docNum;
    @Length(max = 5)
    private String nationCd;
    @Length(max = 40)
    private String engSurName;
    @Length(max = 40)
    private String engGivenName;
    @Length(max = 40)
    private String chiName;
    @Length(max = 160)
    private String otherName;
    @Length(max = 10)
    private String maritalCd;
    @Length(max = 3)
    private String religionCd;
    @Length(max = 1)
    private String genderCd;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String dob;
    @Length(max = 10)
    private String age;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String dod;
    @Length(max = 30)
    private String homePhone;
    @Length(max = 30)
    private String mobilePhone;
    @Length(max = 30)
    private String officePhone;
    @Length(max = 30)
    private String otherPhone;
    @Length(max = 565)
    private String residentialAddress;
    private String photoContent;
    @Length(max = 20)
    private String photoContentType;
    // Patient Information end --
    // Patient Admission Information start --
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String admDtm;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String disDtm;
    @Length(max = 40)
    private String attDrName;
    @Length(max = 40)
    private String conDrName;
    //@NotBlank
    @Length(max = 80)
    private String spec;
    @Length(max = 20)
    private String classNum;
    @Length(max = 20)
    private String ward;
    @Length(max = 20)
    private String bedNum;
    @Length(max = 1)
    private String detailsCd;
    @Length(max = 11)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String backDate;
    // Patient Admission Information end --
    // for MOE start--
    private String moePatientKey;
    private String moeCaseNo;
    // for MOE end --
    private HashMap<String, MoeUserSettingDto> setting = new HashMap<>();

    public String getSystemLogin() {
        return systemLogin;
    }

    public void setSystemLogin(String systemLogin) {
        this.systemLogin = systemLogin;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getUserRankCd() {
        return userRankCd;
    }

    public void setUserRankCd(String userRankCd) {
        this.userRankCd = userRankCd;
    }

    public String getUserRankDesc() {
        return userRankDesc;
    }

    public void setUserRankDesc(String userRankDesc) {
        this.userRankDesc = userRankDesc;
    }

    public String getHospitalCd() {
        return hospitalCd;
    }

    public void setHospitalCd(String hospitalCd) {
        this.hospitalCd = hospitalCd;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getActionCd() {
        return actionCd;
    }

    public void setActionCd(String actionCd) {
        this.actionCd = actionCd;
    }

    public String getEnableHtmlPrinting() {
        return enableHtmlPrinting;
    }

    public void setEnableHtmlPrinting(String enableHtmlPrinting) {
        this.enableHtmlPrinting = enableHtmlPrinting;
    }

    public String getMrnDisplayLabelP() {
        return mrnDisplayLabelP;
    }

    public void setMrnDisplayLabelP(String mrnDisplayLabelP) {
        this.mrnDisplayLabelP = mrnDisplayLabelP;
    }

    public String getMrnDisplayLabelE() {
        return mrnDisplayLabelE;
    }

    public void setMrnDisplayLabelE(String mrnDisplayLabelE) {
        this.mrnDisplayLabelE = mrnDisplayLabelE;
    }

    public String getMrnDisplayTypeCd() {
        return mrnDisplayTypeCd;
    }

    public void setMrnDisplayTypeCd(String mrnDisplayTypeCd) {
        this.mrnDisplayTypeCd = mrnDisplayTypeCd;
    }

    public String getHkid() {
        return hkid;
    }

    public void setHkid(String hkid) {
        this.hkid = hkid;
    }

    public String getMrnPatientIdentity() {
        return mrnPatientIdentity;
    }

    public void setMrnPatientIdentity(String mrnPatientIdentity) {
        this.mrnPatientIdentity = mrnPatientIdentity;
    }

    public String getMrnPatientEncounterNum() {
        return mrnPatientEncounterNum;
    }

    public void setMrnPatientEncounterNum(String mrnPatientEncounterNum) {
        this.mrnPatientEncounterNum = mrnPatientEncounterNum;
    }

    public Long getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Long orderNum) {
        this.orderNum = orderNum;
    }

    public String getPatientTypeCd() {
        return patientTypeCd;
    }

    public void setPatientTypeCd(String patientTypeCd) {
        this.patientTypeCd = patientTypeCd;
    }

    public String getPatientStatus() {
        return patientStatus;
    }

    public void setPatientStatus(String patientStatus) {
        this.patientStatus = patientStatus;
    }

    public String getPatientStatusLabel() {
        return patientStatusLabel;
    }

    public void setPatientStatusLabel(String patientStatusLabel) {
        this.patientStatusLabel = patientStatusLabel;
    }

    public String getPrescTypeCd() {
        return prescTypeCd;
    }

    public void setPrescTypeCd(String prescTypeCd) {
        this.prescTypeCd = prescTypeCd;
    }

    public String getEhrNum() {
        return ehrNum;
    }

    public void setEhrNum(String ehrNum) {
        this.ehrNum = ehrNum;
    }

    public String getDocTypeCd() {
        return docTypeCd;
    }

    public void setDocTypeCd(String docTypeCd) {
        this.docTypeCd = docTypeCd;
    }

    public String getDocNum() {
        return docNum;
    }

    public void setDocNum(String docNum) {
        this.docNum = docNum;
    }

    public String getNationCd() {
        return nationCd;
    }

    public void setNationCd(String nationCd) {
        this.nationCd = nationCd;
    }

    public String getEngSurName() {
        return engSurName;
    }

    public void setEngSurName(String engSurName) {
        this.engSurName = engSurName;
    }

    public String getEngGivenName() {
        return engGivenName;
    }

    public void setEngGivenName(String engGivenName) {
        this.engGivenName = engGivenName;
    }

    public String getChiName() {
        return chiName;
    }

    public void setChiName(String chiName) {
        this.chiName = chiName;
    }

    public String getOtherName() {
        return otherName;
    }

    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }

    public String getMaritalCd() {
        return maritalCd;
    }

    public void setMaritalCd(String maritalCd) {
        this.maritalCd = maritalCd;
    }

    public String getReligionCd() {
        return religionCd;
    }

    public void setReligionCd(String religionCd) {
        this.religionCd = religionCd;
    }

    public String getGenderCd() {
        return genderCd;
    }

    public void setGenderCd(String genderCd) {
        this.genderCd = genderCd;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDod() {
        return dod;
    }

    public void setDod(String dod) {
        this.dod = dod;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getOfficePhone() {
        return officePhone;
    }

    public void setOfficePhone(String officePhone) {
        this.officePhone = officePhone;
    }

    public String getOtherPhone() {
        return otherPhone;
    }

    public void setOtherPhone(String otherPhone) {
        this.otherPhone = otherPhone;
    }

    public String getResidentialAddress() {
        return residentialAddress;
    }

    public void setResidentialAddress(String residentialAddress) {
        this.residentialAddress = residentialAddress;
    }

    public String getPhotoContent() {
        return photoContent;
    }

    public void setPhotoContent(String photoContent) {
        this.photoContent = photoContent;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public String getAdmDtm() {
        return admDtm;
    }

    public void setAdmDtm(String admDtm) {
        this.admDtm = admDtm;
    }

    public String getDisDtm() {
        return disDtm;
    }

    public void setDisDtm(String disDtm) {
        this.disDtm = disDtm;
    }

    public String getAttDrName() {
        return attDrName;
    }

    public void setAttDrName(String attDrName) {
        this.attDrName = attDrName;
    }

    public String getConDrName() {
        return conDrName;
    }

    public void setConDrName(String conDrName) {
        this.conDrName = conDrName;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getClassNum() {
        return classNum;
    }

    public void setClassNum(String classNum) {
        this.classNum = classNum;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getBedNum() {
        return bedNum;
    }

    public void setBedNum(String bedNum) {
        this.bedNum = bedNum;
    }

    public String getDetailsCd() {
        return detailsCd;
    }

    public void setDetailsCd(String detailsCd) {
        this.detailsCd = detailsCd;
    }

    public String getBackDate() {
        return backDate;
    }

    public void setBackDate(String backDate) {
        this.backDate = backDate;
    }

    public HashMap<String, MoeUserSettingDto> getSetting() {
        return setting;
    }

    public void setSetting(HashMap<String, MoeUserSettingDto> setting) {
        this.setting = setting;
    }

    public String getMoePatientKey() {
        return moePatientKey;
    }

    public void setMoePatientKey(String moePatientKey) {
        this.moePatientKey = moePatientKey;
    }

    public String getMoeCaseNo() {
        return moeCaseNo;
    }

    public void setMoeCaseNo(String moeCaseNo) {
        this.moeCaseNo = moeCaseNo;
    }


}
