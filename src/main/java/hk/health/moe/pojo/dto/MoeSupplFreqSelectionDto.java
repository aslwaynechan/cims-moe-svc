/*********************************************************************
* Copyright (c) eHR HK Limited 2013. 
* All Rights Reserved.
* 
*********************************************************************
* PROJECT NAME    :  eHR Adaptation
*
* MODULE NAME     :  Medication Order Entry (MOE)
*
* PROGRAM NAME    :  MoeSupplFreqSelectionDto.java
*
* PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
*
**********************************************************************
* VERSION         :   1.01.001
* REF NO          :  
* DATE CREATED    :   7 June 2013
* 
*********************************************************************
*
* CHANGES         :
* REF NO           DATE        DETAIL
* ------           ----        ------
*
*********************************************************************
*/
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;

public class MoeSupplFreqSelectionDto implements /*IsSerializable,*/ Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -755266321111775481L;

	private String supplFreqDisplay;
	private Long supplFreqSelectionId;

	public void setSupplFreqDisplay(String supplFreqDisplay) {
		this.supplFreqDisplay = supplFreqDisplay;
	}

	public String getSupplFreqDisplay() {
		return supplFreqDisplay;
	}

	public Long getSupplFreqSelectionId() {
		return supplFreqSelectionId;
	}

	public void setSupplFreqSelectionId(Long supplFreqSelectionId) {
		this.supplFreqSelectionId = supplFreqSelectionId;
	}
}
