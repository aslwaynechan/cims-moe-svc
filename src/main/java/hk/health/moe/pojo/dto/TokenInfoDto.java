package hk.health.moe.pojo.dto;

/**************************************************************************
 * NAME        : TokenInfoDto.java
 * VERSION     : 1.0.0
 * DATE        : 03-JUL-2019
 * DESCRIPTION : Work for JWT
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       03-JUL-2019  Initial Version
 * Ricci Liao		25-JUL-2019  replace fields with UserDto
 **************************************************************************/

public class TokenInfoDto {

	private UserDto userDto;

	public UserDto getUserDto() {
		return userDto;
	}

	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}
}
