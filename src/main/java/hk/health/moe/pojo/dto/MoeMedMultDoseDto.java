/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeMedMultDoseDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class MoeMedMultDoseDto implements /*IsSerializable,*/ Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1913216712933735027L;

    private Long stepNo;
    private Long multDoseNo;
    private String patHospcode;
    private Double dosage;
    private String freqCode;
    private Long freq1;
    private String supFreqCode;
    private Long supFreq1;
    private Long supFreq2;
    private String dayOfWeek;
    private String adminTimeCode;
    private String prn;
    private BigDecimal prnPercent;
    private String siteCode;
    private String supSiteDesc;
    private Long duration;
    private String durationUnit;
    private Date startDate;
    private Date endDate;
    private Long moQty;
    private String moQtyUnit;
    private String updateBy;
    private Date updateTime;
    private String freqText;
    private String supFreqText;
    private String capdSystem;
    private String capdCalcium;
    private String capdConc;
    private String itemcode;
    private String capdBaseunit;
    private String siteDesc;
    private String trueConc;
    private String adminTimeDesc;
    private String capdChargeInd;
    private Long capdChargeableUnit;
    private BigDecimal capdChargeableAmount;
    private String confirmCapdChargeInd;
    private Long confirmCapdChargeUnit;
    private String currentFreqText;
    private String currentSupFreqText;
    private MoeEhrMedMultDoseDto moeEhrMedMultDose = new MoeEhrMedMultDoseDto();
    private Long version;

    public Long getStepNo() {
        return stepNo;
    }

    public void setStepNo(Long stepNo) {
        this.stepNo = stepNo;
    }

    public Long getMultDoseNo() {
        return multDoseNo;
    }

    public void setMultDoseNo(Long multDoseNo) {
        this.multDoseNo = multDoseNo;
    }

    public String getPatHospcode() {
        return patHospcode;
    }

    public void setPatHospcode(String patHospcode) {
        this.patHospcode = patHospcode;
    }

    public Double getDosage() {
        return dosage;
    }

    public void setDosage(Double dosage) {
        this.dosage = dosage;
    }

    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    public Long getFreq1() {
        return freq1;
    }

    public void setFreq1(Long freq1) {
        this.freq1 = freq1;
    }

    public String getSupFreqCode() {
        return supFreqCode;
    }

    public void setSupFreqCode(String supFreqCode) {
        this.supFreqCode = supFreqCode;
    }

    public Long getSupFreq1() {
        return supFreq1;
    }

    public void setSupFreq1(Long supFreq1) {
        this.supFreq1 = supFreq1;
    }

    public Long getSupFreq2() {
        return supFreq2;
    }

    public void setSupFreq2(Long supFreq2) {
        this.supFreq2 = supFreq2;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getAdminTimeCode() {
        return adminTimeCode;
    }

    public void setAdminTimeCode(String adminTimeCode) {
        this.adminTimeCode = adminTimeCode;
    }

    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    public BigDecimal getPrnPercent() {
        return prnPercent;
    }

    public void setPrnPercent(BigDecimal prnPercent) {
        this.prnPercent = prnPercent;
    }

    public String getSiteCode() {
        return siteCode;
    }

    public void setSiteCode(String siteCode) {
        this.siteCode = siteCode;
    }

    public String getSupSiteDesc() {
        return supSiteDesc;
    }

    public void setSupSiteDesc(String supSiteDesc) {
        this.supSiteDesc = supSiteDesc;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getMoQty() {
        return moQty;
    }

    public void setMoQty(Long moQty) {
        this.moQty = moQty;
    }

    public String getMoQtyUnit() {
        return moQtyUnit;
    }

    public void setMoQtyUnit(String moQtyUnit) {
        this.moQtyUnit = moQtyUnit;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getFreqText() {
        return freqText;
    }

    public void setFreqText(String freqText) {
        this.freqText = freqText;
    }

    public String getSupFreqText() {
        return supFreqText;
    }

    public void setSupFreqText(String supFreqText) {
        this.supFreqText = supFreqText;
    }

    public String getCapdSystem() {
        return capdSystem;
    }

    public void setCapdSystem(String capdSystem) {
        this.capdSystem = capdSystem;
    }

    public String getCapdCalcium() {
        return capdCalcium;
    }

    public void setCapdCalcium(String capdCalcium) {
        this.capdCalcium = capdCalcium;
    }

    public String getCapdConc() {
        return capdConc;
    }

    public void setCapdConc(String capdConc) {
        this.capdConc = capdConc;
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public String getCapdBaseunit() {
        return capdBaseunit;
    }

    public void setCapdBaseunit(String capdBaseunit) {
        this.capdBaseunit = capdBaseunit;
    }

    public String getSiteDesc() {
        return siteDesc;
    }

    public void setSiteDesc(String siteDesc) {
        this.siteDesc = siteDesc;
    }

    public String getTrueConc() {
        return trueConc;
    }

    public void setTrueConc(String trueConc) {
        this.trueConc = trueConc;
    }

    public String getAdminTimeDesc() {
        return adminTimeDesc;
    }

    public void setAdminTimeDesc(String adminTimeDesc) {
        this.adminTimeDesc = adminTimeDesc;
    }

    public String getCapdChargeInd() {
        return capdChargeInd;
    }

    public void setCapdChargeInd(String capdChargeInd) {
        this.capdChargeInd = capdChargeInd;
    }

    public Long getCapdChargeableUnit() {
        return capdChargeableUnit;
    }

    public void setCapdChargeableUnit(Long capdChargeableUnit) {
        this.capdChargeableUnit = capdChargeableUnit;
    }

    public BigDecimal getCapdChargeableAmount() {
        return capdChargeableAmount;
    }

    public void setCapdChargeableAmount(BigDecimal capdChargeableAmount) {
        this.capdChargeableAmount = capdChargeableAmount;
    }

    public String getConfirmCapdChargeInd() {
        return confirmCapdChargeInd;
    }

    public void setConfirmCapdChargeInd(String confirmCapdChargeInd) {
        this.confirmCapdChargeInd = confirmCapdChargeInd;
    }

    public Long getConfirmCapdChargeUnit() {
        return confirmCapdChargeUnit;
    }

    public void setConfirmCapdChargeUnit(Long confirmCapdChargeUnit) {
        this.confirmCapdChargeUnit = confirmCapdChargeUnit;
    }

    public String getCurrentFreqText() {
        return currentFreqText;
    }

    public void setCurrentFreqText(String currentFreqText) {
        this.currentFreqText = currentFreqText;
    }

    public String getCurrentSupFreqText() {
        return currentSupFreqText;
    }

    public void setCurrentSupFreqText(String currentSupFreqText) {
        this.currentSupFreqText = currentSupFreqText;
    }

    public void setMoeEhrMedMultDose(MoeEhrMedMultDoseDto moeEhrMedMultDoseDto) {
        this.moeEhrMedMultDose = moeEhrMedMultDoseDto;
    }

    public MoeEhrMedMultDoseDto getMoeEhrMedMultDose() {
        return moeEhrMedMultDose;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getVersion() {
        return version;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MoeMedMultDoseDTO [adminTimeCode=");
        builder.append(adminTimeCode);
        builder.append(", adminTimeDesc=");
        builder.append(adminTimeDesc);
        builder.append(", capdBaseunit=");
        builder.append(capdBaseunit);
        builder.append(", capdCalcium=");
        builder.append(capdCalcium);
        builder.append(", capdChargeInd=");
        builder.append(capdChargeInd);
        builder.append(", capdChargeableAmount=");
        builder.append(capdChargeableAmount);
        builder.append(", capdChargeableUnit=");
        builder.append(capdChargeableUnit);
        builder.append(", capdConc=");
        builder.append(capdConc);
        builder.append(", capdSystem=");
        builder.append(capdSystem);
        builder.append(", confirmCapdChargeInd=");
        builder.append(confirmCapdChargeInd);
        builder.append(", confirmCapdChargeUnit=");
        builder.append(confirmCapdChargeUnit);
        builder.append(", dayOfWeek=");
        builder.append(dayOfWeek);
        builder.append(", dosage=");
        builder.append(dosage);
        builder.append(", duration=");
        builder.append(duration);
        builder.append(", durationUnit=");
        builder.append(durationUnit);
        builder.append(", endDate=");
        builder.append(endDate);
        builder.append(", freq1=");
        builder.append(freq1);
        builder.append(", freqCode=");
        builder.append(freqCode);
        builder.append(", freqText=");
        builder.append(freqText);
        builder.append(", itemcode=");
        builder.append(itemcode);
        builder.append(", moQty=");
        builder.append(moQty);
        builder.append(", moQtyUnit=");
        builder.append(moQtyUnit);
        builder.append(", moeEhrMedMultDoseDTO=");
        builder.append(moeEhrMedMultDose);
        builder.append(", multDoseNo=");
        builder.append(multDoseNo);
        builder.append(", patHospcode=");
        builder.append(patHospcode);
        builder.append(", prn=");
        builder.append(prn);
        builder.append(", prnPercent=");
        builder.append(prnPercent);
        builder.append(", siteCode=");
        builder.append(siteCode);
        builder.append(", siteDesc=");
        builder.append(siteDesc);
        builder.append(", startDate=");
        builder.append(startDate);
        builder.append(", stepNo=");
        builder.append(stepNo);
        builder.append(", supFreq1=");
        builder.append(supFreq1);
        builder.append(", supFreq2=");
        builder.append(supFreq2);
        builder.append(", supFreqCode=");
        builder.append(supFreqCode);
        builder.append(", supFreqText=");
        builder.append(supFreqText);
        builder.append(", supSiteDesc=");
        builder.append(supSiteDesc);
        builder.append(", trueConc=");
        builder.append(trueConc);
        builder.append(", updateBy=");
        builder.append(updateBy);
        builder.append(", updateTime=");
        builder.append(updateTime);
        builder.append(", currentFreqText=");
        builder.append(currentFreqText);
        builder.append(", currentSupFreqText=");
        builder.append(currentSupFreqText);
        builder.append(", version=");
        builder.append(version);
        builder.append("]");
        return builder.toString();
    }
}
