/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeEhrAcceptDrugCheckingDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;

public class MoeEhrAcceptDrugCheckingDto implements /*IsSerializable,*/ Serializable {

    private String acceptId;
    private String allergen;
    private String drugVtm;
    private String screenMsg;
    private String buttonLabel;

    public String getAcceptId() {
        return acceptId;
    }

    public void setAcceptId(String acceptId) {
        this.acceptId = acceptId;
    }

    public String getAllergen() {
        return allergen;
    }

    public void setAllergen(String allergen) {
        this.allergen = allergen;
    }

    public String getDrugVtm() {
        return drugVtm;
    }

    public void setDrugVtm(String drugVtm) {
        this.drugVtm = drugVtm;
    }

    public String getScreenMsg() {
        return screenMsg;
    }

    public void setScreenMsg(String screenMsg) {
        this.screenMsg = screenMsg;
    }

    public String getButtonLabel() {
        return buttonLabel;
    }

    public void setButtonLabel(String buttonLabel) {
        this.buttonLabel = buttonLabel;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MoeEhrAcceptDrugCheckingDTO [acceptId=");
        builder.append(acceptId);
        builder.append(", allergen=");
        builder.append(allergen);
        builder.append(", drugVtm=");
        builder.append(drugVtm);
        builder.append(", screenMsg=");
        builder.append(screenMsg);
        builder.append(", buttonLabel=");
        builder.append(buttonLabel);
        builder.append("]");
        return builder.toString();
    }

}
