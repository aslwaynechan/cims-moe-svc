package hk.health.moe.pojo.dto;

import java.io.Serializable;
import java.util.Date;

public class MoeDrugGenericNameLocalDto implements Serializable {

    private static final long serialVersionUID = -2222765896707045456L;

    private String localDrugId;
    private String terminologyName;
    private String vtm;
    private Long formId;
    private String formEng;
    private Long doseFormExtraInfoId;
    private String doseFormExtraInfo;
    private Long routeId;
    private String routeEng;
    private Long vtmId;
    private Long vtmRouteId;
    private Long vtmRouteFormId;
    private Long legalClassId;
    private String legalClass;
    private String strengthCompulsory;
    private Long vmpId;
    private String vmp;
    private String strength;
    private String strengthLevelExtraInfo;
    private String baseUnit;
    private Long baseUnitId;
    private String prescribeUnit;
    private Long prescribeUnitId;
    private String dispenseUnit;
    private Long dispenseUnitId;
    private String createUserId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private Date createDtm;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private Date updateDtm;
    private long version;


    public String getLocalDrugId() {
        return localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        this.localDrugId = localDrugId;
    }

    public String getTerminologyName() {
        return terminologyName;
    }

    public void setTerminologyName(String terminologyName) {
        this.terminologyName = terminologyName;
    }

    public String getVtm() {
        return vtm;
    }

    public void setVtm(String vtm) {
        this.vtm = vtm;
    }

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public String getFormEng() {
        return formEng;
    }

    public void setFormEng(String formEng) {
        this.formEng = formEng;
    }

    public Long getDoseFormExtraInfoId() {
        return doseFormExtraInfoId;
    }

    public void setDoseFormExtraInfoId(Long doseFormExtraInfoId) {
        this.doseFormExtraInfoId = doseFormExtraInfoId;
    }

    public String getDoseFormExtraInfo() {
        return doseFormExtraInfo;
    }

    public void setDoseFormExtraInfo(String doseFormExtraInfo) {
        this.doseFormExtraInfo = doseFormExtraInfo;
    }

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public String getRouteEng() {
        return routeEng;
    }

    public void setRouteEng(String routeEng) {
        this.routeEng = routeEng;
    }

    public Long getVtmId() {
        return vtmId;
    }

    public void setVtmId(Long vtmId) {
        this.vtmId = vtmId;
    }

    public Long getVtmRouteId() {
        return vtmRouteId;
    }

    public void setVtmRouteId(Long vtmRouteId) {
        this.vtmRouteId = vtmRouteId;
    }

    public Long getVtmRouteFormId() {
        return vtmRouteFormId;
    }

    public void setVtmRouteFormId(Long vtmRouteFormId) {
        this.vtmRouteFormId = vtmRouteFormId;
    }

    public Long getLegalClassId() {
        return legalClassId;
    }

    public void setLegalClassId(Long legalClassId) {
        this.legalClassId = legalClassId;
    }

    public String getLegalClass() {
        return legalClass;
    }

    public void setLegalClass(String legalClass) {
        this.legalClass = legalClass;
    }

    public String getStrengthCompulsory() {
        return strengthCompulsory;
    }

    public void setStrengthCompulsory(String strengthCompulsory) {
        this.strengthCompulsory = strengthCompulsory;
    }

    public Long getVmpId() {
        return vmpId;
    }

    public void setVmpId(Long vmpId) {
        this.vmpId = vmpId;
    }

    public String getVmp() {
        return vmp;
    }

    public void setVmp(String vmp) {
        this.vmp = vmp;
    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public String getStrengthLevelExtraInfo() {
        return strengthLevelExtraInfo;
    }

    public void setStrengthLevelExtraInfo(String strengthLevelExtraInfo) {
        this.strengthLevelExtraInfo = strengthLevelExtraInfo;
    }

    public String getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(String baseUnit) {
        this.baseUnit = baseUnit;
    }

    public Long getBaseUnitId() {
        return baseUnitId;
    }

    public void setBaseUnitId(Long baseUnitId) {
        this.baseUnitId = baseUnitId;
    }

    public String getPrescribeUnit() {
        return prescribeUnit;
    }

    public void setPrescribeUnit(String prescribeUnit) {
        this.prescribeUnit = prescribeUnit;
    }

    public Long getPrescribeUnitId() {
        return prescribeUnitId;
    }

    public void setPrescribeUnitId(Long prescribeUnitId) {
        this.prescribeUnitId = prescribeUnitId;
    }

    public String getDispenseUnit() {
        return dispenseUnit;
    }

    public void setDispenseUnit(String dispenseUnit) {
        this.dispenseUnit = dispenseUnit;
    }

    public Long getDispenseUnitId() {
        return dispenseUnitId;
    }

    public void setDispenseUnitId(Long dispenseUnitId) {
        this.dispenseUnitId = dispenseUnitId;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateHosp() {
        return createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    public String getCreateRank() {
        return createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    public String getCreateRankDesc() {
        return createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    public Date getCreateDtm() {
        return createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateHosp() {
        return updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    public String getUpdateRank() {
        return updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    public Date getUpdateDtm() {
        return updateDtm;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDrugGenericNameLocalDto that = (MoeDrugGenericNameLocalDto) o;

        if (version != that.version) return false;
        if (localDrugId != null ? !localDrugId.equals(that.localDrugId) : that.localDrugId != null) return false;
        if (terminologyName != null ? !terminologyName.equals(that.terminologyName) : that.terminologyName != null)
            return false;
        if (vtm != null ? !vtm.equals(that.vtm) : that.vtm != null) return false;
        if (formId != null ? !formId.equals(that.formId) : that.formId != null) return false;
        if (formEng != null ? !formEng.equals(that.formEng) : that.formEng != null) return false;
        if (doseFormExtraInfoId != null ? !doseFormExtraInfoId.equals(that.doseFormExtraInfoId) : that.doseFormExtraInfoId != null)
            return false;
        if (doseFormExtraInfo != null ? !doseFormExtraInfo.equals(that.doseFormExtraInfo) : that.doseFormExtraInfo != null)
            return false;
        if (routeId != null ? !routeId.equals(that.routeId) : that.routeId != null) return false;
        if (routeEng != null ? !routeEng.equals(that.routeEng) : that.routeEng != null) return false;
        if (vtmId != null ? !vtmId.equals(that.vtmId) : that.vtmId != null) return false;
        if (vtmRouteId != null ? !vtmRouteId.equals(that.vtmRouteId) : that.vtmRouteId != null) return false;
        if (vtmRouteFormId != null ? !vtmRouteFormId.equals(that.vtmRouteFormId) : that.vtmRouteFormId != null)
            return false;
        if (legalClassId != null ? !legalClassId.equals(that.legalClassId) : that.legalClassId != null) return false;
        if (legalClass != null ? !legalClass.equals(that.legalClass) : that.legalClass != null) return false;
        if (strengthCompulsory != null ? !strengthCompulsory.equals(that.strengthCompulsory) : that.strengthCompulsory != null)
            return false;
        if (vmpId != null ? !vmpId.equals(that.vmpId) : that.vmpId != null) return false;
        if (vmp != null ? !vmp.equals(that.vmp) : that.vmp != null) return false;
        if (strength != null ? !strength.equals(that.strength) : that.strength != null) return false;
        if (strengthLevelExtraInfo != null ? !strengthLevelExtraInfo.equals(that.strengthLevelExtraInfo) : that.strengthLevelExtraInfo != null)
            return false;
        if (baseUnit != null ? !baseUnit.equals(that.baseUnit) : that.baseUnit != null) return false;
        if (baseUnitId != null ? !baseUnitId.equals(that.baseUnitId) : that.baseUnitId != null) return false;
        if (prescribeUnit != null ? !prescribeUnit.equals(that.prescribeUnit) : that.prescribeUnit != null)
            return false;
        if (prescribeUnitId != null ? !prescribeUnitId.equals(that.prescribeUnitId) : that.prescribeUnitId != null)
            return false;
        if (dispenseUnit != null ? !dispenseUnit.equals(that.dispenseUnit) : that.dispenseUnit != null) return false;
        if (dispenseUnitId != null ? !dispenseUnitId.equals(that.dispenseUnitId) : that.dispenseUnitId != null)
            return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null) return false;
        if (createUser != null ? !createUser.equals(that.createUser) : that.createUser != null) return false;
        if (createHosp != null ? !createHosp.equals(that.createHosp) : that.createHosp != null) return false;
        if (createRank != null ? !createRank.equals(that.createRank) : that.createRank != null) return false;
        if (createRankDesc != null ? !createRankDesc.equals(that.createRankDesc) : that.createRankDesc != null)
            return false;
        if (createDtm != null ? !createDtm.equals(that.createDtm) : that.createDtm != null) return false;
        if (updateUserId != null ? !updateUserId.equals(that.updateUserId) : that.updateUserId != null) return false;
        if (updateUser != null ? !updateUser.equals(that.updateUser) : that.updateUser != null) return false;
        if (updateHosp != null ? !updateHosp.equals(that.updateHosp) : that.updateHosp != null) return false;
        if (updateRank != null ? !updateRank.equals(that.updateRank) : that.updateRank != null) return false;
        if (updateRankDesc != null ? !updateRankDesc.equals(that.updateRankDesc) : that.updateRankDesc != null)
            return false;
        return updateDtm != null ? updateDtm.equals(that.updateDtm) : that.updateDtm == null;
    }

    @Override
    public int hashCode() {
        int result = localDrugId != null ? localDrugId.hashCode() : 0;
        result = 31 * result + (terminologyName != null ? terminologyName.hashCode() : 0);
        result = 31 * result + (vtm != null ? vtm.hashCode() : 0);
        result = 31 * result + (formId != null ? formId.hashCode() : 0);
        result = 31 * result + (formEng != null ? formEng.hashCode() : 0);
        result = 31 * result + (doseFormExtraInfoId != null ? doseFormExtraInfoId.hashCode() : 0);
        result = 31 * result + (doseFormExtraInfo != null ? doseFormExtraInfo.hashCode() : 0);
        result = 31 * result + (routeId != null ? routeId.hashCode() : 0);
        result = 31 * result + (routeEng != null ? routeEng.hashCode() : 0);
        result = 31 * result + (vtmId != null ? vtmId.hashCode() : 0);
        result = 31 * result + (vtmRouteId != null ? vtmRouteId.hashCode() : 0);
        result = 31 * result + (vtmRouteFormId != null ? vtmRouteFormId.hashCode() : 0);
        result = 31 * result + (legalClassId != null ? legalClassId.hashCode() : 0);
        result = 31 * result + (legalClass != null ? legalClass.hashCode() : 0);
        result = 31 * result + (strengthCompulsory != null ? strengthCompulsory.hashCode() : 0);
        result = 31 * result + (vmpId != null ? vmpId.hashCode() : 0);
        result = 31 * result + (vmp != null ? vmp.hashCode() : 0);
        result = 31 * result + (strength != null ? strength.hashCode() : 0);
        result = 31 * result + (strengthLevelExtraInfo != null ? strengthLevelExtraInfo.hashCode() : 0);
        result = 31 * result + (baseUnit != null ? baseUnit.hashCode() : 0);
        result = 31 * result + (baseUnitId != null ? baseUnitId.hashCode() : 0);
        result = 31 * result + (prescribeUnit != null ? prescribeUnit.hashCode() : 0);
        result = 31 * result + (prescribeUnitId != null ? prescribeUnitId.hashCode() : 0);
        result = 31 * result + (dispenseUnit != null ? dispenseUnit.hashCode() : 0);
        result = 31 * result + (dispenseUnitId != null ? dispenseUnitId.hashCode() : 0);
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (createUser != null ? createUser.hashCode() : 0);
        result = 31 * result + (createHosp != null ? createHosp.hashCode() : 0);
        result = 31 * result + (createRank != null ? createRank.hashCode() : 0);
        result = 31 * result + (createRankDesc != null ? createRankDesc.hashCode() : 0);
        result = 31 * result + (createDtm != null ? createDtm.hashCode() : 0);
        result = 31 * result + (updateUserId != null ? updateUserId.hashCode() : 0);
        result = 31 * result + (updateUser != null ? updateUser.hashCode() : 0);
        result = 31 * result + (updateHosp != null ? updateHosp.hashCode() : 0);
        result = 31 * result + (updateRank != null ? updateRank.hashCode() : 0);
        result = 31 * result + (updateRankDesc != null ? updateRankDesc.hashCode() : 0);
        result = 31 * result + (updateDtm != null ? updateDtm.hashCode() : 0);
        result = 31 * result + (int) (version ^ (version >>> 32));
        return result;
    }

}
