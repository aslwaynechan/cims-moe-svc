package hk.health.moe.pojo.dto;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;

public class MoeDrugLocalDto implements java.io.Serializable {

    private static final long serialVersionUID = 3523136731748464200L;

    private String localDrugId;
    private Long version;
    private String hkRegNo;
    private String terminologyName;
    private String tradeName;
    private String vtm;
    private Long formId;
    private String formEng;
    private Long doseFormExtraInfoId;
    private String doseFormExtraInfo;
    private Long routeId;
    private String routeEng;
    private Long vtmId;
    private Long tradeVtmId;
    private Long vtmRouteId;
    private Long tradeVtmRouteId;
    private Long vtmRouteFormId;
    private Long tradeVtmRouteFormId;
    private Long baseUnitId;
    private String baseUnit;
    private Long prescribeUnitId;
    private String prescribeUnit;
    private Long dispenseUnitId;
    private String dispenseUnit;
    private Long legalClassId;
    private String legalClass;
    private String manufacturer;
    private String genericIndicator;
    private String allergyCheckFlag;
    private String strengthCompulsory;
    private String createUserId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private Date createDtm;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private Date updateDtm;

    public MoeDrugLocalDto() {
        this.localDrugId = "";
        this.hkRegNo = "";
        this.terminologyName = "";
        this.genericIndicator = "N";
        this.allergyCheckFlag = "Y";
        this.strengthCompulsory = "Y";
        this.createHosp = "";
        this.createDtm = new Date();
        this.updateDtm = new Date();
    }

    public MoeDrugLocalDto(String createUserId, String createUser, String createHosp,
                                   String createRank, String createRankDesc, Date createDtm) {
        setCreateUser(createUser);
        setCreateUserId(createUserId);
        setCreateHosp(createHosp);
        setCreateRank(createRank);
        setCreateRankDesc(createRankDesc);
        setCreateDtm(createDtm);
    }

    public String getLocalDrugId() {
        return this.localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        // Ricci 20191009 start --
        this.localDrugId = StringUtils.isNotBlank(localDrugId) ? localDrugId.toUpperCase() : localDrugId;
        // Ricci 20191009 end --
    }

    public Long getVersion() {
        return this.version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getHkRegNo() {
        return this.hkRegNo;
    }

    public void setHkRegNo(String hkRegNo) {
        this.hkRegNo = hkRegNo;
    }

    public String getTerminologyName() {
        return this.terminologyName;
    }

    public void setTerminologyName(String terminologyName) {
        this.terminologyName = terminologyName;
    }

    public String getTradeName() {
        return this.tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getVtm() {
        return this.vtm;
    }

    public void setVtm(String vtm) {
        this.vtm = vtm;
    }

    public Long getFormId() {
        return this.formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public String getFormEng() {
        return this.formEng;
    }

    public void setFormEng(String formEng) {
        this.formEng = formEng;
    }

    public Long getDoseFormExtraInfoId() {
        return this.doseFormExtraInfoId;
    }

    public void setDoseFormExtraInfoId(Long doseFormExtraInfoId) {
        this.doseFormExtraInfoId = doseFormExtraInfoId;
    }

    public String getDoseFormExtraInfo() {
        return this.doseFormExtraInfo;
    }

    public void setDoseFormExtraInfo(String doseFormExtraInfo) {
        this.doseFormExtraInfo = doseFormExtraInfo;
    }

    public Long getRouteId() {
        return this.routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public String getRouteEng() {
        return this.routeEng;
    }

    public void setRouteEng(String routeEng) {
        this.routeEng = routeEng;
    }

    public Long getVtmId() {
        return this.vtmId;
    }

    public void setVtmId(Long vtmId) {
        this.vtmId = vtmId;
    }

    public Long getTradeVtmId() {
        return this.tradeVtmId;
    }

    public void setTradeVtmId(Long tradeVtmId) {
        this.tradeVtmId = tradeVtmId;
    }

    public Long getVtmRouteId() {
        return this.vtmRouteId;
    }

    public void setVtmRouteId(Long vtmRouteId) {
        this.vtmRouteId = vtmRouteId;
    }

    public Long getTradeVtmRouteId() {
        return this.tradeVtmRouteId;
    }

    public void setTradeVtmRouteId(Long tradeVtmRouteId) {
        this.tradeVtmRouteId = tradeVtmRouteId;
    }

    public Long getVtmRouteFormId() {
        return this.vtmRouteFormId;
    }

    public void setVtmRouteFormId(Long vtmRouteFormId) {
        this.vtmRouteFormId = vtmRouteFormId;
    }

    public Long getTradeVtmRouteFormId() {
        return this.tradeVtmRouteFormId;
    }

    public void setTradeVtmRouteFormId(Long tradeVtmRouteFormId) {
        this.tradeVtmRouteFormId = tradeVtmRouteFormId;
    }

    public Long getBaseUnitId() {
        return this.baseUnitId;
    }

    public void setBaseUnitId(Long baseUnitId) {
        this.baseUnitId = baseUnitId;
    }

    public String getBaseUnit() {
        return this.baseUnit;
    }

    public void setBaseUnit(String baseUnit) {
        this.baseUnit = baseUnit;
    }

    public Long getPrescribeUnitId() {
        return this.prescribeUnitId;
    }

    public void setPrescribeUnitId(Long prescribeUnitId) {
        this.prescribeUnitId = prescribeUnitId;
    }

    public String getPrescribeUnit() {
        return this.prescribeUnit;
    }

    public void setPrescribeUnit(String prescribeUnit) {
        this.prescribeUnit = prescribeUnit;
    }

    public Long getDispenseUnitId() {
        return this.dispenseUnitId;
    }

    public void setDispenseUnitId(Long dispenseUnitId) {
        this.dispenseUnitId = dispenseUnitId;
    }

    public String getDispenseUnit() {
        return this.dispenseUnit;
    }

    public void setDispenseUnit(String dispenseUnit) {
        this.dispenseUnit = dispenseUnit;
    }

    public Long getLegalClassId() {
        return this.legalClassId;
    }

    public void setLegalClassId(Long legalClassId) {
        this.legalClassId = legalClassId;
    }

    public String getLegalClass() {
        return this.legalClass;
    }

    public void setLegalClass(String legalClass) {
        this.legalClass = legalClass;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getGenericIndicator() {
        return this.genericIndicator;
    }

    public void setGenericIndicator(String genericIndicator) {
        this.genericIndicator = genericIndicator;
    }

    public String getAllergyCheckFlag() {
        return this.allergyCheckFlag;
    }

    public void setAllergyCheckFlag(String allergyCheckFlag) {
        this.allergyCheckFlag = allergyCheckFlag;
    }

    public String getStrengthCompulsory() {
        return this.strengthCompulsory;
    }

    public void setStrengthCompulsory(String strengthCompulsory) {
        this.strengthCompulsory = strengthCompulsory;
    }

    public String getCreateUserId() {
        return this.createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUser() {
        return this.createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateHosp() {
        return this.createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    public String getCreateRank() {
        return this.createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    public String getCreateRankDesc() {
        return this.createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    public Date getCreateDtm() {
        return this.createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    public String getUpdateUserId() {
        return this.updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getUpdateUser() {
        return this.updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateHosp() {
        return this.updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    public String getUpdateRank() {
        return this.updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    public String getUpdateRankDesc() {
        return this.updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    public Date getUpdateDtm() {
        return this.updateDtm;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }
}
