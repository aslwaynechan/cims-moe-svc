
package hk.health.moe.pojo.dto.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 *
 *
 * <pre>
 * &lt;complexType name="PrescriptionResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PrescriptionDto" type="{http://ehr.gov.hk/moe/ws/beans}PrescriptionDto" maxOccurs="1000"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrescriptionResponse", /*namespace = "http://ehr.gov.hk/moe/ws/beans",*/ propOrder = {
    "prescriptionDto"
})
//@XmlRootElement(name="getPatientPrescriptionByOrderNoResponse",namespace ="http://ehr.gov.hk/moe/ws/beans" )
public class PrescriptionResponse {

    @XmlElement(name = "PrescriptionDto", /*namespace = "http://ehr.gov.hk/moe/ws/beans", */required = true)
    protected List<PrescriptionDto> prescriptionDto;

    /**
     * Gets the value of the prescriptionDto property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prescriptionDto property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrescriptionDto().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrescriptionDto }
     * 
     * 
     */
    public List<PrescriptionDto> getPrescriptionDto() {
        if (prescriptionDto == null) {
            prescriptionDto = new ArrayList<PrescriptionDto>();
        }
        return this.prescriptionDto;
    }

}
