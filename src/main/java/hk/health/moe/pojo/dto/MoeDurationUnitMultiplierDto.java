package hk.health.moe.pojo.dto;

import java.io.Serializable;

public class MoeDurationUnitMultiplierDto implements Serializable {
    private static final long serialVersionUID = -9217888739349151817L;
    private String durationUnit;
    private String durationUnitDesc;
    private long durationMultiplier;

    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    public String getDurationUnitDesc() {
        return durationUnitDesc;
    }

    public void setDurationUnitDesc(String durationUnitDesc) {
        this.durationUnitDesc = durationUnitDesc;
    }

    public long getDurationMultiplier() {
        return durationMultiplier;
    }

    public void setDurationMultiplier(long durationMultiplier) {
        this.durationMultiplier = durationMultiplier;
    }

}
