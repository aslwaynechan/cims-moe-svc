/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeMedicationDecisionSupportDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;
import java.util.List;

public class MoeMedicationDecisionDto implements /*IsSerializable,*/ Serializable {

    private static final long serialVersionUID = -406091731122540521L;
    private List<InnerMoePatientAllergyDto> patientAllergies;
    private List<MoePatientMedicationDto> patientMedications;
    private String workStationIp;
    private String userId;

    public List<InnerMoePatientAllergyDto> getPatientAllergies() {
        return patientAllergies;
    }

    public void setPatientAllergies(List<InnerMoePatientAllergyDto> patientAllergies) {
        this.patientAllergies = patientAllergies;
    }

    public List<MoePatientMedicationDto> getPatientMedications() {
        return patientMedications;
    }

    public void setPatientMedications(List<MoePatientMedicationDto> patientMedications) {
        this.patientMedications = patientMedications;
    }

    public String getWorkStationIp() {
        return workStationIp;
    }

    public void setWorkStationIp(String workStationIp) {
        this.workStationIp = workStationIp;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
