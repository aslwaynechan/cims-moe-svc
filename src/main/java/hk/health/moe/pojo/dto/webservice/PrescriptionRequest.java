
package hk.health.moe.pojo.dto.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 *
 *
 * <pre>
 * &lt;complexType name="PrescriptionRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hospCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="orderNo" type="{http://www.w3.org/2001/XMLSchema}Long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrescriptionRequest", namespace = "http://ehr.gov.hk/moe/ws/beans", propOrder = {
    "hospCode",
    "orderNo"
})
public class PrescriptionRequest {

    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    protected String hospCode;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected Long orderNo;


    public String getHospCode() {
        return hospCode;
    }


    public void setHospCode(String value) {
        this.hospCode = value;
    }


    public Long getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Long value) {
        this.orderNo = value;
    }

}
