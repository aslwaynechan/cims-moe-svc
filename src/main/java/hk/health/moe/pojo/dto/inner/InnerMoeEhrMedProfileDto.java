package hk.health.moe.pojo.dto.inner;

import javax.validation.constraints.Pattern;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/7/31
 */
public class InnerMoeEhrMedProfileDto {
    @Pattern(regexp = "[N,A,M,S,R]")
    private String orderLineType;
    private Long siteId;
    private Long cycleMultiplier;
    private Long supplFreqId;
    @Pattern(regexp = "[Y,y]")
    private String dangerDrug;

    public String getOrderLineType() {
        return orderLineType;
    }

    public void setOrderLineType(String orderLineType) {
        this.orderLineType = orderLineType;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public Long getCycleMultiplier() {
        return cycleMultiplier;
    }

    public void setCycleMultiplier(Long cycleMultiplier) {
        this.cycleMultiplier = cycleMultiplier;
    }

    public Long getSupplFreqId() {
        return supplFreqId;
    }

    public void setSupplFreqId(Long supplFreqId) {
        this.supplFreqId = supplFreqId;
    }

    public String getDangerDrug() {
        return dangerDrug;
    }

    public void setDangerDrug(String dangerDrug) {
        this.dangerDrug = dangerDrug;
    }
}
