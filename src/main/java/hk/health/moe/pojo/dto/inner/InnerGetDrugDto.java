package hk.health.moe.pojo.dto.inner;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/8/22
 */
public class InnerGetDrugDto {
    String localDrugId;
    String hkRegNo;
    Long strengthRank;

    public String getLocalDrugId() {
        return localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        // Ricci 20191009 start --
        this.localDrugId = StringUtils.isNotBlank(localDrugId) ? localDrugId.toUpperCase() : localDrugId;
        // Ricci 20191009 end --
    }

    public String getHkRegNo() {
        return hkRegNo;
    }

    public void setHkRegNo(String hkRegNo) {
        this.hkRegNo = hkRegNo;
    }

    public Long getStrengthRank() {
        return strengthRank;
    }

    public void setStrengthRank(Long strengthRank) {
        this.strengthRank = strengthRank;
    }
}
