package hk.health.moe.pojo.dto.inner;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

public class InnerRefreshToken implements Serializable {
    private static final long serialVersionUID = 4746167420241668492L;
    @NotBlank
    String oldToken;

    public String getOldToken() {
        return oldToken;
    }

    public void setOldToken(String oldToken) {
        this.oldToken = oldToken;
    }
}
