/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeEhrOrderDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.Date;

public class MoeEhrOrderDto implements /*IsSerializable,*/ Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1012932753350920333L;

    private String moePatientKey;
    private String moeCaseNo;
    @Valid
    private MoeOrderDto moeOrder;
    private Long ordNo;
    private String displayOrdNo;
    private String hospcode;
    private String createUserId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private Long version;
    // Just for redis cache --
    private String isCache;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date cacheDtm;
    private String cacheBy;
    private String cacheId;

    public String getCacheId() {
        return cacheId;
    }

    public void setCacheId(String cacheId) {
        this.cacheId = cacheId;
    }

    public Date getCacheDtm() {
        return cacheDtm;
    }

    public void setCacheDtm(Date cacheDtm) {
        this.cacheDtm = cacheDtm;
    }

    public String getCacheBy() {
        return cacheBy;
    }

    public void setCacheBy(String cacheBy) {
        this.cacheBy = cacheBy;
    }

    public MoeEhrOrderDto() {
    }

    public MoeEhrOrderDto(Long ordNo) {
        this.setOrdNo(ordNo);
    }

    public String getMoePatientKey() {
        return moePatientKey;
    }

    public void setMoePatientKey(String moePatientKey) {
        this.moePatientKey = moePatientKey;
    }

    public String getMoeCaseNo() {
        return moeCaseNo;
    }

    public void setMoeCaseNo(String moeCaseNo) {
        this.moeCaseNo = moeCaseNo;
    }

    public void setMoeOrder(MoeOrderDto moeOrderDto) {
        this.moeOrder = moeOrderDto;
    }

    public MoeOrderDto getMoeOrder() {
        return moeOrder;
    }

    public Long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(Long ordNo) {
        this.ordNo = ordNo;
    }

    public void setDisplayOrdNo(String displayOrdNo) {
        this.displayOrdNo = displayOrdNo;
    }

    public String getDisplayOrdNo() {
        return displayOrdNo;
    }

    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateHosp() {
        return createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    public String getCreateRank() {
        return createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    public String getCreateRankDesc() {
        return createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateHosp() {
        return updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    public String getUpdateRank() {
        return updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getVersion() {
        return version;
    }

    public String getIsCache() {
        return isCache;
    }

    public void setIsCache(String isCache) {
        this.isCache = isCache;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MoeEhrOrderDTO [createHosp=");
        builder.append(createHosp);
        builder.append(", createRank=");
        builder.append(createRank);
        builder.append(", createRankDesc=");
        builder.append(createRankDesc);
        builder.append(", createUser=");
        builder.append(createUser);
        builder.append(", createUserId=");
        builder.append(createUserId);
        builder.append(", hospcode=");
        builder.append(hospcode);
        builder.append(", moeCaseNo=");
        builder.append(moeCaseNo);
        builder.append(", moeOrderDTO=");
        builder.append(moeOrder);
        builder.append(", moePatientKey=");
        builder.append(moePatientKey);
        builder.append(", ordNo=");
        builder.append(ordNo);
        builder.append(", updateHosp=");
        builder.append(updateHosp);
        builder.append(", updateRank=");
        builder.append(updateRank);
        builder.append(", updateRankDesc=");
        builder.append(updateRankDesc);
        builder.append(", updateUser=");
        builder.append(updateUser);
        builder.append(", updateUserId=");
        builder.append(updateUserId);
        builder.append(", version=");
        builder.append(version);
        builder.append("]");
        return builder.toString();
    }

}
