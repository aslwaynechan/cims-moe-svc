package hk.health.moe.pojo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MoeEhrRxImageLogDto implements Serializable {

    private static final long serialVersionUID = -1681082108002448108L;
    private String reportId;
    @NotBlank
    @Length(max = 20)
    private String hospcode;
    @NotNull
    private Long ordNo;
    @NotBlank
    @Length(max = 1)
    private String directPrint;
    @NotBlank
    @Length(max = 1)
    private String reportType;
    private byte[] rxImage;
    private String rxXml;
    @NotBlank
    @Length(max = 8)
    private String refNo;
    @NotNull
    private Date createDtm;
    @NotNull
    private Date lastPrintDtm;

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    public Long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(Long ordNo) {
        this.ordNo = ordNo;
    }

    public String getDirectPrint() {
        return directPrint;
    }

    public void setDirectPrint(String directPrint) {
        this.directPrint = directPrint;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public byte[] getRxImage() {
        return rxImage;
    }

    public void setRxImage(byte[] rxImage) {
        this.rxImage = rxImage;
    }

    public String getRxXml() {
        return rxXml;
    }

    public void setRxXml(String rxXml) {
        this.rxXml = rxXml;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public Date getCreateDtm() {
        return createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    public Date getLastPrintDtm() {
        return lastPrintDtm;
    }

    public void setLastPrintDtm(Date lastPrintDtm) {
        this.lastPrintDtm = lastPrintDtm;
    }
}
