package hk.health.moe.pojo.dto;

/**************************************************************************
 * NAME        : CodeListDto.java
 * VERSION     : 1.0.0
 * DATE        : 04-JUL-2019
 * DESCRIPTION : Work for code list table
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       04-JUL-2019  Initial Version
 * Ricci Liao       04-JUL-2019  Support to list code-list in prescript page
 **************************************************************************/

public class CodeListDto<T> {

    private T code;
    private String engDesc;
    private Long displaySeq;
    private String useInputValue;

    public CodeListDto() {
    }

    public CodeListDto(T code, Long displaySeq) {
        this.code = code;
        this.displaySeq = displaySeq;
    }

    public CodeListDto(T code, String engDesc) {
        this.code = code;
        this.engDesc = engDesc;
    }

    public CodeListDto(T code, String engDesc, Long displaySeq) {
        this.code = code;
        this.engDesc = engDesc;
        this.displaySeq = displaySeq;
    }

    public CodeListDto(T code, String engDesc, Long displaySeq, String useInputValue) {
        this.code = code;
        this.engDesc = engDesc;
        this.displaySeq = displaySeq;
        this.useInputValue = useInputValue;
    }

    public String getEngDesc() {
        return engDesc;
    }

    public void setEngDesc(String engDesc) {
        this.engDesc = engDesc;
    }

    public Long getDisplaySeq() {
        return displaySeq;
    }

    public void setDisplaySeq(Long displaySeq) {
        this.displaySeq = displaySeq;
    }

    public T getCode() {
        return code;
    }

    public void setCode(T code) {
        this.code = code;
    }

    public String getUseInputValue() {
        return useInputValue;
    }

    public void setUseInputValue(String useInputValue) {
        this.useInputValue = useInputValue;
    }

}
