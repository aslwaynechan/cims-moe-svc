/*********************************************************************
* Copyright (c) eHR HK Limited 2013. 
* All Rights Reserved.
* 
*********************************************************************
* PROJECT NAME    :  eHR Adaptation
*
* MODULE NAME     :  MOE Drug Maintenance
*
* PROGRAM NAME    :  DrugDetailDto.java
*
* PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
*
**********************************************************************
* VERSION         :   1.01.001
* REF NO          :  
* DATE CREATED    :   7 June 2013
* 
*********************************************************************
*
* CHANGES         :
* REF NO           DATE        DETAIL
* ------           ----        ------
*
*********************************************************************
*/
package hk.health.moe.pojo.dto;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/8/22
 * @Description for moe_drug_server
 */
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class DrugDetailDto implements java.io.Serializable{
	
	//--- drug info ---
	private String hkRegNo;
	private String localDrugId;
	private boolean localDrugOnly;
	
	//--- strength list ---
	// search by hkRegNo and strength local record more than 1
	private List<MoeDrugStrengthLocalDto> moeDrugStrengthLocalList;
	// new drug with hkRegNo and strength record more than 1
	private List<MoeDrugStrengthDto> moeDrugStrengthList;
	
	//--- drug detail ---
	private MoeDrugLocalDto moeDrugLocal;
	private List<MoeDrugAliasNameLocalDto> moeDrugAliasNameLocalList;
	private MoeDrugStrengthLocalDto moeDrugStrengthLocal;
	private MoeDrugOrdPropertyLocalDto moeDrugOrdPropertyLocal;
	
	private List<MoeDrugDto> moeDrugList;
	private boolean isDrugKeyAndHkRegValid = false;
	
	private boolean addToSAAM;

	//Simon 20190823 start--
	private String retreiveMethod;
	//Simon 20190823 end--


	public DrugDetailDto(){
		super();
	}

	public String getHkRegNo() {
		return hkRegNo;
	}

	public void setHkRegNo(String hkRegNo) {
		this.hkRegNo = hkRegNo;
	}

	public String getLocalDrugId() {
		return localDrugId;
	}

	public void setLocalDrugId(String localDrugId) {
		// Ricci 20191009 start --
		this.localDrugId = StringUtils.isNotBlank(localDrugId) ? localDrugId.toUpperCase() : localDrugId;
		// Ricci 20191009 end --
		//this.localDrugId = localDrugId;
	}

	public boolean isLocalDrugOnly() {
		return localDrugOnly;
	}

	public void setLocalDrugOnly(boolean localDrugOnly) {
		this.localDrugOnly = localDrugOnly;
	}

	public List<MoeDrugStrengthLocalDto> getMoeDrugStrengthLocalList() {
		return moeDrugStrengthLocalList;
	}

	public void setMoeDrugStrengthLocalList(
			List<MoeDrugStrengthLocalDto> moeDrugStrengthLocalList) {
		this.moeDrugStrengthLocalList = moeDrugStrengthLocalList;
	}

	public List<MoeDrugStrengthDto> getMoeDrugStrengthList() {
		return moeDrugStrengthList;
	}

	public void setMoeDrugStrengthList(
			List<MoeDrugStrengthDto> moeDrugStrengthList) {
		this.moeDrugStrengthList = moeDrugStrengthList;
	}

	public MoeDrugLocalDto getMoeDrugLocal() {
		return moeDrugLocal;
	}

	public void setMoeDrugLocal(MoeDrugLocalDto moeDrugLocalDto) {
		this.moeDrugLocal = moeDrugLocalDto;
	}

	public List<MoeDrugAliasNameLocalDto> getMoeDrugAliasNameLocalList() {
		return moeDrugAliasNameLocalList;
	}

	public void setMoeDrugAliasNameLocalList(
			List<MoeDrugAliasNameLocalDto> moeDrugAliasNameLocalList) {
		this.moeDrugAliasNameLocalList = moeDrugAliasNameLocalList;
	}

	public MoeDrugStrengthLocalDto getMoeDrugStrengthLocal() {
		return moeDrugStrengthLocal;
	}

	public void setMoeDrugStrengthLocal(
			MoeDrugStrengthLocalDto moeDrugStrengthLocalDto) {
		this.moeDrugStrengthLocal = moeDrugStrengthLocalDto;
	}

	public MoeDrugOrdPropertyLocalDto getMoeDrugOrdPropertyLocal() {
		return moeDrugOrdPropertyLocal;
	}

	public void setMoeDrugOrdPropertyLocal(
			MoeDrugOrdPropertyLocalDto moeDrugOrdPropertyLocalDto) {
		this.moeDrugOrdPropertyLocal = moeDrugOrdPropertyLocalDto;
	}

	public List<MoeDrugDto> getMoeDrugList() {
		return moeDrugList;
	}

	public void setMoeDrugList(List<MoeDrugDto> moeDrugList) {
		this.moeDrugList = moeDrugList;
	}

	public boolean isDrugKeyAndHkRegValid() {
		return isDrugKeyAndHkRegValid;
	}

	public void setDrugKeyAndHkRegValid(boolean isDrugKeyAndHkRegValid) {
		this.isDrugKeyAndHkRegValid = isDrugKeyAndHkRegValid;
	}
	
	public boolean isAddToSAAM() {
		return addToSAAM;
	}

	public void setAddToSAAM(boolean addToSAAM) {
		this.addToSAAM = addToSAAM;
	}


	public String getRetreiveMethod() {
		return retreiveMethod;
	}

	public void setRetreiveMethod(String retreiveMethod) {
		this.retreiveMethod = retreiveMethod;
	}

}
