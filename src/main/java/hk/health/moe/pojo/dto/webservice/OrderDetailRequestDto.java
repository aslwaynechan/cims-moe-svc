package hk.health.moe.pojo.dto.webservice;

import javax.validation.constraints.NotBlank;

/**
 * @Author Simon
 * @Date 2019/11/13
 */
public class OrderDetailRequestDto {
    @NotBlank
    private String hospCode;
    @NotBlank
    private String patientKey;
    @NotBlank
    private String episodeNo;
    @NotBlank
    private String episodeType;
    private String loginId;

    public String getHospCode() {
        return hospCode;
    }

    public void setHospCode(String hospCode) {
        this.hospCode = hospCode;
    }

    public String getPatientKey() {
        return patientKey;
    }

    public void setPatientKey(String patientKey) {
        this.patientKey = patientKey;
    }

    public String getEpisodeNo() {
        return episodeNo;
    }

    public void setEpisodeNo(String episodeNo) {
        this.episodeNo = episodeNo;
    }

    public String getEpisodeType() {
        return episodeType;
    }

    public void setEpisodeType(String episodeType) {
        this.episodeType = episodeType;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }
}
