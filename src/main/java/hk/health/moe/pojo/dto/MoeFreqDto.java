/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeFreqDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;
import java.math.BigDecimal;

public class MoeFreqDto implements Serializable/*, IsSerializable*/ {

    /**
     *
     */
    private static final long serialVersionUID = -5732812838702451214L;

    private Long freqId;
    private String freqCode;
    private String freqEng;
    private String freqChi;
    private Long upperLimit;
    private Long lowerLimit;
    private String durationUnit;
    private BigDecimal freqMultiplier;
    private Long durationValue;
    private String useInputValue;
    private String useInputMethod;
    private Long freqMultiplierDenominator;
    private String freqDescEng;

    public Long getFreqId() {
        return freqId;
    }

    public void setFreqId(Long freqId) {
        this.freqId = freqId;
    }

    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    public String getFreqEng() {
        return freqEng;
    }

    public void setFreqEng(String freqEng) {
        this.freqEng = freqEng;
    }

    public String getFreqChi() {
        return freqChi;
    }

    public void setFreqChi(String freqChi) {
        this.freqChi = freqChi;
    }

    public Long getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(Long upperLimit) {
        this.upperLimit = upperLimit;
    }

    public Long getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(Long lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    public BigDecimal getFreqMultiplier() {
        return freqMultiplier;
    }

    public void setFreqMultiplier(BigDecimal freqMultiplier) {
        this.freqMultiplier = freqMultiplier;
    }

    public Long getDurationValue() {
        return durationValue;
    }

    public void setDurationValue(Long durationValue) {
        this.durationValue = durationValue;
    }

    public String getUseInputValue() {
        return useInputValue;
    }

    public void setUseInputValue(String useInputValue) {
        this.useInputValue = useInputValue;
    }

    public String getUseInputMethod() {
        return useInputMethod;
    }

    public void setUseInputMethod(String useInputMethod) {
        this.useInputMethod = useInputMethod;
    }

    public Long getFreqMultiplierDenominator() {
        return freqMultiplierDenominator;
    }

    public void setFreqMultiplierDenominator(Long freqMultiplierDenominator) {
        this.freqMultiplierDenominator = freqMultiplierDenominator;
    }

    public String getFreqDescEng() {
        return freqDescEng;
    }

    public void setFreqDescEng(String freqDescEng) {
        this.freqDescEng = freqDescEng;
    }
}
