/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeReactionAllergenDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;

public class MoeReactionAllergenDto implements /*IsSerializable, */Serializable {

    private static final long serialVersionUID = -7986847939617110516L;

    private String allergenVTM;
    private String allergenVTMTermID;
    private String reactionLevel;

    public String getAllergenVTM() {
        return allergenVTM;
    }

    public void setAllergenVTM(String allergenVTM) {
        this.allergenVTM = allergenVTM;
    }

    public String getAllergenVTMTermID() {
        return allergenVTMTermID;
    }

    public void setAllergenVTMTermID(String allergenVTMTermID) {
        this.allergenVTMTermID = allergenVTMTermID;
    }

    public String getReactionLevel() {
        return reactionLevel;
    }

    public void setReactionLevel(String reactionLevel) {
        this.reactionLevel = reactionLevel;
    }
}
