package hk.health.moe.pojo.dto.webservice;

import javax.validation.constraints.NotBlank;

/**
 * @Author Simon
 * @Date 2019/11/13
 */
public class RestUserDto {
    private String loginId;
    private String loginName;
    @NotBlank
    private String hospCode;
    private String userRank;
    private String userRankDesc;

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getHospCode() {
        return hospCode;
    }

    public void setHospCode(String hospCode) {
        this.hospCode = hospCode;
    }

    public String getUserRank() {
        return userRank;
    }

    public void setUserRank(String userRank) {
        this.userRank = userRank;
    }

    public String getUserRankDesc() {
        return userRankDesc;
    }

    public void setUserRankDesc(String userRankDesc) {
        this.userRankDesc = userRankDesc;
    }
}
