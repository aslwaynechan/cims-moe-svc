
package hk.health.moe.pojo.dto.webservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 *
 *
 * <pre>
 * &lt;complexType name="PrescriptionDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="orderNo" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="refNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="patientKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="episodeNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hospCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hospName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prescType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ordSubType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="specialty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ward" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MedProfileDto" type="{http://ehr.gov.hk/moe/ws/beans}MedProfileDto" maxOccurs="1000" minOccurs="0"/>
 *         &lt;element name="createDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="createHosp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="createRank" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="createRankDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="createUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="createUserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="updateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="updateHosp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="updateRank" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="updateRankDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="updateUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="updateUserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionDtm" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrescriptionDto", /*namespace = "http://ehr.gov.hk/moe/ws/beans",*/ propOrder = {
    "orderNo",
    "refNo",
    "patientKey",
    "episodeNo",
    "hospCode",
    "hospName",
    "prescType",
    "ordSubType",
    "specialty",
    "ward",
    "medProfileDto",
    "createDate",
    "createHosp",
    "createRank",
    "createRankDesc",
    "createUser",
    "createUserId",
    "updateDate",
    "updateHosp",
    "updateRank",
    "updateRankDesc",
    "updateUser",
    "updateUserId",
    "transactionDtm"
})
public class PrescriptionDto {

    @XmlElement
    protected long orderNo;
    @XmlElement
    protected String refNo;
    @XmlElement
    protected String patientKey;
    @XmlElement
    protected String episodeNo;
    @XmlElement
    protected String hospCode;
    @XmlElement
    protected String hospName;
    @XmlElement
    protected String prescType;
    @XmlElement
    protected String ordSubType;
    @XmlElement
    protected String specialty;
    @XmlElement
    protected String ward;
    @XmlElement(name = "MedProfileDto")
    protected List<MedProfileDto> medProfileDto;
    @XmlElement
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createDate;
    @XmlElement
    protected String createHosp;
    @XmlElement
    protected String createRank;
    @XmlElement
    protected String createRankDesc;
    @XmlElement
    protected String createUser;
    @XmlElement
    protected String createUserId;
    @XmlElement
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar updateDate;
    @XmlElement
    protected String updateHosp;
    @XmlElement
    protected String updateRank;
    @XmlElement
    protected String updateRankDesc;
    @XmlElement
    protected String updateUser;
    @XmlElement
    protected String updateUserId;
    @XmlElement
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar transactionDtm;


    public long getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(long value) {
        this.orderNo = value;
    }


    public String getRefNo() {
        return refNo;
    }


    public void setRefNo(String value) {
        this.refNo = value;
    }


    public String getPatientKey() {
        return patientKey;
    }


    public void setPatientKey(String value) {
        this.patientKey = value;
    }


    public String getEpisodeNo() {
        return episodeNo;
    }


    public void setEpisodeNo(String value) {
        this.episodeNo = value;
    }


    public String getHospCode() {
        return hospCode;
    }

    public void setHospCode(String value) {
        this.hospCode = value;
    }

    public String getHospName() {
        return hospName;
    }


    public void setHospName(String value) {
        this.hospName = value;
    }


    public String getPrescType() {
        return prescType;
    }


    public void setPrescType(String value) {
        this.prescType = value;
    }


    public String getOrdSubType() {
        return ordSubType;
    }


    public void setOrdSubType(String value) {
        this.ordSubType = value;
    }


    public String getSpecialty() {
        return specialty;
    }


    public void setSpecialty(String value) {
        this.specialty = value;
    }


    public String getWard() {
        return ward;
    }


    public void setWard(String value) {
        this.ward = value;
    }


    public List<MedProfileDto> getMedProfileDto() {
        if (medProfileDto == null) {
            medProfileDto = new ArrayList<MedProfileDto>();
        }
        return this.medProfileDto;
    }


    public XMLGregorianCalendar getCreateDate() {
        return createDate;
    }


    public void setCreateDate(XMLGregorianCalendar value) {
        this.createDate = value;
    }


    public String getCreateHosp() {
        return createHosp;
    }


    public void setCreateHosp(String value) {
        this.createHosp = value;
    }


    public String getCreateRank() {
        return createRank;
    }


    public void setCreateRank(String value) {
        this.createRank = value;
    }


    public String getCreateRankDesc() {
        return createRankDesc;
    }


    public void setCreateRankDesc(String value) {
        this.createRankDesc = value;
    }


    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String value) {
        this.createUser = value;
    }


    public String getCreateUserId() {
        return createUserId;
    }


    public void setCreateUserId(String value) {
        this.createUserId = value;
    }


    public XMLGregorianCalendar getUpdateDate() {
        return updateDate;
    }


    public void setUpdateDate(XMLGregorianCalendar value) {
        this.updateDate = value;
    }


    public String getUpdateHosp() {
        return updateHosp;
    }


    public void setUpdateHosp(String value) {
        this.updateHosp = value;
    }


    public String getUpdateRank() {
        return updateRank;
    }


    public void setUpdateRank(String value) {
        this.updateRank = value;
    }


    public String getUpdateRankDesc() {
        return updateRankDesc;
    }


    public void setUpdateRankDesc(String value) {
        this.updateRankDesc = value;
    }


    public String getUpdateUser() {
        return updateUser;
    }


    public void setUpdateUser(String value) {
        this.updateUser = value;
    }


    public String getUpdateUserId() {
        return updateUserId;
    }


    public void setUpdateUserId(String value) {
        this.updateUserId = value;
    }


    public XMLGregorianCalendar getTransactionDtm() {
        return transactionDtm;
    }


    public void setTransactionDtm(XMLGregorianCalendar value) {
        this.transactionDtm = value;
    }

}
