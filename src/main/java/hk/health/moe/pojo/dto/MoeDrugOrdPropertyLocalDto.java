/*********************************************************************
 * Copyright (c) eHR HK Limited 2013. 
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  MOE Drug Maintenance
 *
 * PROGRAM NAME    :  MoeDrugOrdPropertyLocalDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :  
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Date;

public class MoeDrugOrdPropertyLocalDto implements java.io.Serializable {

    private String localDrugId;
    private Long version;
    private String suspend;
    private String outOfStock;
    private BigDecimal price;
    private BigDecimal minimumDosage;
    private Long maximumDuration;
    private String createUserId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private Date createDtm;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private Date updateDtm;
    private String tradeNameAlias;
    private String specifyQuantityFlag;
    private String drugCategoryId;
    //Chris 20200108 -Start
    private Boolean isGenericNameLevel;
    //Chris 20200108 -End
    //Chris 20200113 -Start
    private Boolean allowGenericNameCheckBox;
    //Chris 20200113 -End


    public MoeDrugOrdPropertyLocalDto() {
    }

    public MoeDrugOrdPropertyLocalDto(String createUserId, String createUser, String createHosp,
                                      String createRank, String createRankDesc, Date createDtm,
                                      Long maximumDuration) {
        setCreateUser(createUser);
        setCreateUserId(createUserId);
        setCreateHosp(createHosp);
        setCreateRank(createRank);
        setCreateRankDesc(createRankDesc);
        setCreateDtm(createDtm);
        setMaximumDuration(maximumDuration);
    }

    public MoeDrugOrdPropertyLocalDto(String localDrugId, String suspend,
                                      String outOfStock, BigDecimal price, BigDecimal minimumDosage,
                                      Long maximumDuration, String createUserId, String createUser,
                                      String createHosp, String createRank, String createRankDesc,
                                      Date createDtm, String updateUserId, String updateUser,
                                      String updateHosp, String updateRank, String updateRankDesc,
                                      Date updateDtm, Boolean isGenericNameLevel, Boolean allowGenericNameCheckBox) {
        // Ricci 20191009 start --
        this.localDrugId = StringUtils.isNotBlank(localDrugId) ? localDrugId.toUpperCase() : localDrugId;
        // Ricci 20191009 end --
        this.suspend = suspend;
        this.outOfStock = outOfStock;
        this.price = price;
        this.minimumDosage = minimumDosage;
        this.maximumDuration = maximumDuration;
        this.createUserId = createUserId;
        this.createUser = createUser;
        this.createHosp = createHosp;
        this.createRank = createRank;
        this.createRankDesc = createRankDesc;
        this.createDtm = createDtm;
        this.updateUserId = updateUserId;
        this.updateUser = updateUser;
        this.updateHosp = updateHosp;
        this.updateRank = updateRank;
        this.updateRankDesc = updateRankDesc;
        this.updateDtm = updateDtm;
        //Chris 20200108 -Start
        this.isGenericNameLevel = isGenericNameLevel;
        //Chris 20200108 -End
        //Chris 20200113 -Start
        this.allowGenericNameCheckBox = allowGenericNameCheckBox;
        //Chris 20200113 -End
    }

    public String getLocalDrugId() {
        return this.localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        // Ricci 20191009 start --
        this.localDrugId = StringUtils.isNotBlank(localDrugId) ? localDrugId.toUpperCase() : localDrugId;
        // Ricci 20191009 end --
    }

    public Long getVersion() {
        return this.version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getSuspend() {
        return this.suspend;
    }

    public void setSuspend(String suspend) {
        this.suspend = suspend;
    }

    public String getOutOfStock() {
        return this.outOfStock;
    }

    public void setOutOfStock(String outOfStock) {
        this.outOfStock = outOfStock;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getMinimumDosage() {
        return this.minimumDosage;
    }

    public void setMinimumDosage(BigDecimal minimumDosage) {
        this.minimumDosage = minimumDosage;
    }

    public Long getMaximumDuration() {
        return this.maximumDuration;
    }

    public void setMaximumDuration(Long maximumDuration) {
        this.maximumDuration = maximumDuration;
    }

    public String getCreateUserId() {
        return this.createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUser() {
        return this.createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateHosp() {
        return this.createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    public String getCreateRank() {
        return this.createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    public String getCreateRankDesc() {
        return this.createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    public Date getCreateDtm() {
        return this.createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    public String getUpdateUserId() {
        return this.updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getUpdateUser() {
        return this.updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateHosp() {
        return this.updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    public String getUpdateRank() {
        return this.updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    public String getUpdateRankDesc() {
        return this.updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    public Date getUpdateDtm() {
        return this.updateDtm;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }

    public String getTradeNameAlias() {
        return this.tradeNameAlias;
    }

    public void setTradeNameAlias(String tradeNameAlias) {
        this.tradeNameAlias = tradeNameAlias;
    }

    public String getSpecifyQuantityFlag() {
        return this.specifyQuantityFlag;
    }

    public void setSpecifyQuantityFlag(String specifyQuantityFlag) {
        this.specifyQuantityFlag = specifyQuantityFlag;
    }

    public String getDrugCategoryId() {
        return drugCategoryId;
    }

    public void setDrugCategoryId(String drugCategoryId) {
        this.drugCategoryId = drugCategoryId;
    }

    public Boolean isGenericNameLevel() {
        return isGenericNameLevel;
    }

    public void setGenericNameLevel(Boolean genericNameLevel) {
        isGenericNameLevel = genericNameLevel;
    }

    public Boolean getAllowGenericNameCheckBox() {
        return allowGenericNameCheckBox;
    }

    public void setAllowGenericNameCheckBox(Boolean allowGenericNameCheckBox) {
        this.allowGenericNameCheckBox = allowGenericNameCheckBox;
    }

}
