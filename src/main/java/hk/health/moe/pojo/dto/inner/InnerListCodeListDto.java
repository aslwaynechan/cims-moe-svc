package hk.health.moe.pojo.dto.inner;

import javax.validation.constraints.NotEmpty;
import java.util.Set;

public class InnerListCodeListDto {

    @NotEmpty
    private Set<String> codeType;

    public Set<String> getCodeType() {
        return codeType;
    }

    public void setCodeType(Set<String> codeType) {
        this.codeType = codeType;
    }

}
