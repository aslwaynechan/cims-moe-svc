package hk.health.moe.pojo.dto.inner;

import hk.health.moe.pojo.dto.MoeMyFavouriteHdrDto;

import java.io.Serializable;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/7/30
 */
public class InnerSaveMyFavouriteDto implements Serializable {

    private static final long serialVersionUID = -6522553625955595680L;
    private MoeMyFavouriteHdrDto moeMyFavouriteHdr;
    private boolean department;

    public MoeMyFavouriteHdrDto getMoeMyFavouriteHdr() {
        return moeMyFavouriteHdr;
    }

    public void setMoeMyFavouriteHdr(MoeMyFavouriteHdrDto moeMyFavouriteHdr) {
        this.moeMyFavouriteHdr = moeMyFavouriteHdr;
    }

    public boolean isDepartment() {
        return department;
    }

    public void setDepartment(boolean department) {
        this.department = department;
    }
}
