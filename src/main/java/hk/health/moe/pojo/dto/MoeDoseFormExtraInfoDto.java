package hk.health.moe.pojo.dto;

public class MoeDoseFormExtraInfoDto implements java.io.Serializable {

    private static final long serialVersionUID = -8412361465871656186L;

    private int doseFormExtraInfoId;
    private String doseFormExtraInfo;
    private Integer doseFormExtraInfoTermId;
    private int rank;

    public int getDoseFormExtraInfoId() {
        return this.doseFormExtraInfoId;
    }

    public void setDoseFormExtraInfoId(int doseFormExtraInfoId) {
        this.doseFormExtraInfoId = doseFormExtraInfoId;
    }

    public String getDoseFormExtraInfo() {
        return this.doseFormExtraInfo;
    }

    public void setDoseFormExtraInfo(String doseFormExtraInfo) {
        this.doseFormExtraInfo = doseFormExtraInfo;
    }

    public Integer getDoseFormExtraInfoTermId() {
        return this.doseFormExtraInfoTermId;
    }

    public void setDoseFormExtraInfoTermId(Integer doseFormExtraInfoTermId) {
        this.doseFormExtraInfoTermId = doseFormExtraInfoTermId;
    }

    public int getRank() {
        return this.rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

}
