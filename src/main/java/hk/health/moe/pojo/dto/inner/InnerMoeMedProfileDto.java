package hk.health.moe.pojo.dto.inner;

import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/7/31
 */
public class InnerMoeMedProfileDto {
    private String freqCode;
    private Long duration;
    @Pattern(regexp = "[c,d,m,s,w]")
    private String durationUnit;
    private String supFreqCode;
    private String regimen;
    private String dayOfWeek;
    private Long freq1;
    private Long supFreq1;
    private Long supFreq2;

    private List<InnerMoeMedMultDoseDto> moeMedMultDoses = new ArrayList<InnerMoeMedMultDoseDto>();
    private InnerMoeEhrMedProfileDto moeEhrMedProfile = new InnerMoeEhrMedProfileDto();



    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    public List<InnerMoeMedMultDoseDto> getMoeMedMultDoses() {
        return moeMedMultDoses;
    }

    public void setMoeMedMultDoses(List<InnerMoeMedMultDoseDto> moeMedMultDoses) {
        this.moeMedMultDoses = moeMedMultDoses;
    }

    public InnerMoeEhrMedProfileDto getMoeEhrMedProfile() {
        return moeEhrMedProfile;
    }

    public void setMoeEhrMedProfile(InnerMoeEhrMedProfileDto moeEhrMedProfile) {
        this.moeEhrMedProfile = moeEhrMedProfile;
    }

    public String getSupFreqCode() {
        return supFreqCode;
    }

    public void setSupFreqCode(String supFreqCode) {
        this.supFreqCode = supFreqCode;
    }

    public String getRegimen() {
        return regimen;
    }

    public void setRegimen(String regimen) {
        this.regimen = regimen;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Long getFreq1() {
        return freq1;
    }

    public void setFreq1(Long freq1) {
        this.freq1 = freq1;
    }

    public Long getSupFreq1() {
        return supFreq1;
    }

    public void setSupFreq1(Long supFreq1) {
        this.supFreq1 = supFreq1;
    }

    public Long getSupFreq2() {
        return supFreq2;
    }

    public void setSupFreq2(Long supFreq2) {
        this.supFreq2 = supFreq2;
    }
}
