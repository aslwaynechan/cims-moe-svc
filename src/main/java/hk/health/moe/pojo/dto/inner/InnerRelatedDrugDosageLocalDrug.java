package hk.health.moe.pojo.dto.inner;

import java.io.Serializable;

public class InnerRelatedDrugDosageLocalDrug implements Serializable {

    private String localDrugId;
    private Long version;

    public String getLocalDrugId() {
        return localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        this.localDrugId = localDrugId;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}
