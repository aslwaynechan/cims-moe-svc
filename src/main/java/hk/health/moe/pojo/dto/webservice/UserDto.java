
package hk.health.moe.pojo.dto.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 *
 *
 * <pre>
 * &lt;complexType name="UserDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="loginId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="loginName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hospCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="userRank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="userRankDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserDto", namespace = "http://ehr.gov.hk/moe/ws/beans", propOrder = {
    "loginId",
    "loginName",
    "hospCode",
    "userRank",
    "userRankDesc"
})
public class UserDto {

    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    protected String loginId;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String loginName;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    protected String hospCode;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    protected String userRank;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String userRankDesc;

    public String getLoginId() {
        return loginId;
    }


    public void setLoginId(String value) {
        this.loginId = value;
    }


    public String getLoginName() {
        return loginName;
    }


    public void setLoginName(String value) {
        this.loginName = value;
    }


    public String getHospCode() {
        return hospCode;
    }

    public void setHospCode(String value) {
        this.hospCode = value;
    }


    public String getUserRank() {
        return userRank;
    }



    public void setUserRank(String value) {
        this.userRank = value;
    }

    public String getUserRankDesc() {
        return userRankDesc;
    }


    public void setUserRankDesc(String value) {
        this.userRankDesc = value;
    }

}
