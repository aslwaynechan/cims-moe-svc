package hk.health.moe.pojo.dto.inner;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

public class InnerGetDrugBySpecialtyDto implements Serializable {

    private static final long serialVersionUID = -5978128020493617080L;
    boolean isNewDrug;
    String localDrugId;
    String hkRegNo;

    public boolean isNewDrug() {
        return isNewDrug;
    }

    public void setNewDrug(boolean newDrug) {
        isNewDrug = newDrug;
    }

    public String getLocalDrugId() {
        return localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        // Ricci 20191009 start --
        this.localDrugId = StringUtils.isNotBlank(localDrugId) ? localDrugId.toUpperCase() : localDrugId;
        // Ricci 20191009 end --
    }

    public String getHkRegNo() {
        return hkRegNo;
    }

    public void setHkRegNo(String hkRegNo) {
        this.hkRegNo = hkRegNo;
    }

}