package hk.health.moe.pojo.dto;

import javax.validation.constraints.NotBlank;

public class MoeOrderSearchDto {

    @NotBlank
    private String hospcode;
    @NotBlank
    private String patientKey;
    @NotBlank
    private String caseNo;  //MOE_PATIENT_CASE:moe_case_no
//    @Length(min = 1)
//    @Min(value = 1)
    private Long ordNo;

    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    public String getPatientKey() {
        return patientKey;
    }

    public void setPatientKey(String patientKey) {
        this.patientKey = patientKey;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public Long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(Long ordNo) {
        this.ordNo = ordNo;
    }


}
