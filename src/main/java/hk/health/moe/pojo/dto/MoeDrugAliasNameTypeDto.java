package hk.health.moe.pojo.dto;

import java.io.Serializable;

public class MoeDrugAliasNameTypeDto implements Serializable {

    private static final long serialVersionUID = 4625830780000348058L;
    private String aliasNameType;
    private String aliasNameDesc;

    public MoeDrugAliasNameTypeDto() {
    }

    public MoeDrugAliasNameTypeDto(String aliasNameType, String aliasNameDesc) {
        setAliasNameDesc(aliasNameDesc);
        setAliasNameType(aliasNameType);
    }


    public String getAliasNameType() {
        return aliasNameType;
    }

    public void setAliasNameType(String aliasNameType) {
        this.aliasNameType = aliasNameType;
    }

    public String getAliasNameDesc() {
        return aliasNameDesc;
    }

    public void setAliasNameDesc(String aliasNameDesc) {
        this.aliasNameDesc = aliasNameDesc;
    }
}
