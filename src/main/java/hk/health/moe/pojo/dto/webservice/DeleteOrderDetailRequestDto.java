package hk.health.moe.pojo.dto.webservice;

import javax.validation.constraints.NotBlank;

/**
 * @Author Simon
 * @Date 2019/11/13
 */
public class DeleteOrderDetailRequestDto {

    private RestUserDto userDto;
    @NotBlank
    private String patientKey;
    private String episodeNo;
    @NotBlank
    private String episodeType;

    public RestUserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(RestUserDto userDto) {
        this.userDto = userDto;
    }

    public String getPatientKey() {
        return patientKey;
    }

    public void setPatientKey(String patientKey) {
        this.patientKey = patientKey;
    }

    public String getEpisodeNo() {
        return episodeNo;
    }

    public void setEpisodeNo(String episodeNo) {
        this.episodeNo = episodeNo;
    }

    public String getEpisodeType() {
        return episodeType;
    }

    public void setEpisodeType(String episodeType) {
        this.episodeType = episodeType;
    }
}
