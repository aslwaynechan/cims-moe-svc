/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeUserSettingDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;

public class MoeUserSettingDto implements Serializable/*, IsSerializable*/ {

    /**
     *
     */
    private static final long serialVersionUID = -8571212777952629077L;

    private String paramName;
    private String paramNameDesc;
    private String paramValue;
    private String paramValueType;
    private String paramPossibleValue;
    private String paramLevel;
    private String paramDisplay;

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamNameDesc() {
        return paramNameDesc;
    }

    public void setParamNameDesc(String paramNameDesc) {
        this.paramNameDesc = paramNameDesc;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public String getParamValueType() {
        return paramValueType;
    }

    public void setParamValueType(String paramValueType) {
        this.paramValueType = paramValueType;
    }

    public String getParamPossibleValue() {
        return paramPossibleValue;
    }

    public void setParamPossibleValue(String paramPossibleValue) {
        this.paramPossibleValue = paramPossibleValue;
    }

    public String getParamLevel() {
        return paramLevel;
    }

    public void setParamLevel(String paramLevel) {
        this.paramLevel = paramLevel;
    }

    public String getParamDisplay() {
        return paramDisplay;
    }

    public void setParamDisplay(String paramDisplay) {
        this.paramDisplay = paramDisplay;
    }

}
