package hk.health.moe.pojo.dto.inner;

import hk.health.moe.pojo.dto.MoeCommonDosageDto;

import java.io.Serializable;

public class InnerSaveCommonDosageDto implements Serializable {
    private static final long serialVersionUID = 10749426790595871L;

    private MoeCommonDosageDto moeCommonDosage;
    private String actionType;

    public MoeCommonDosageDto getMoeCommonDosage() {
        return moeCommonDosage;
    }

    public void setMoeCommonDosage(MoeCommonDosageDto moeCommonDosage) {
        this.moeCommonDosage = moeCommonDosage;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }
}
