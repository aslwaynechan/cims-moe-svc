
package hk.health.moe.pojo.dto.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 *
 *
 * <pre>
 * &lt;complexType name="MedAllergenDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rowNo" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="allergen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="matchType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="screenMsg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="certainty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="manifestation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="additionInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="overrideReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MedAllergenDto", namespace = "http://ehr.gov.hk/moe/ws/beans", propOrder = {
    "rowNo",
    "allergen",
    "matchType",
    "screenMsg",
    "certainty",
    "manifestation",
    "additionInfo",
    "overrideReason"
})
public class MedAllergenDto {

    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected long rowNo;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String allergen;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String matchType;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String screenMsg;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String certainty;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String manifestation;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String additionInfo;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String overrideReason;

    public long getRowNo() {
        return rowNo;
    }

    public void setRowNo(long rowNo) {
        this.rowNo = rowNo;
    }

    public String getAllergen() {
        return allergen;
    }

    public void setAllergen(String allergen) {
        this.allergen = allergen;
    }

    public String getMatchType() {
        return matchType;
    }

    public void setMatchType(String matchType) {
        this.matchType = matchType;
    }

    public String getScreenMsg() {
        return screenMsg;
    }

    public void setScreenMsg(String screenMsg) {
        this.screenMsg = screenMsg;
    }

    public String getCertainty() {
        return certainty;
    }

    public void setCertainty(String certainty) {
        this.certainty = certainty;
    }

    public String getManifestation() {
        return manifestation;
    }

    public void setManifestation(String manifestation) {
        this.manifestation = manifestation;
    }

    public String getAdditionInfo() {
        return additionInfo;
    }

    public void setAdditionInfo(String additionInfo) {
        this.additionInfo = additionInfo;
    }

    public String getOverrideReason() {
        return overrideReason;
    }

    public void setOverrideReason(String overrideReason) {
        this.overrideReason = overrideReason;
    }
}
