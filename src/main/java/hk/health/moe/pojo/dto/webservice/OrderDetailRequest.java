
package hk.health.moe.pojo.dto.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**

 * <pre>
 * &lt;complexType name="OrderDetailRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hospCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="patientKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="episodeNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="episodeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="loginId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderDetailRequest", namespace = "http://ehr.gov.hk/moe/ws/beans", propOrder = {
    "hospCode",
    "patientKey",
    "episodeNo",
    "episodeType",
    "loginId"
})
public class OrderDetailRequest {

    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    protected String hospCode;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    protected String patientKey;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    protected String episodeNo;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans", required = true)
    protected String episodeType;
    @XmlElement(namespace = "http://ehr.gov.hk/moe/ws/beans")
    protected String loginId;

    public String getHospCode() {
        return hospCode;
    }

    public void setHospCode(String hospCode) {
        this.hospCode = hospCode;
    }

    public String getPatientKey() {
        return patientKey;
    }

    public void setPatientKey(String patientKey) {
        this.patientKey = patientKey;
    }

    public String getEpisodeNo() {
        return episodeNo;
    }

    public void setEpisodeNo(String episodeNo) {
        this.episodeNo = episodeNo;
    }

    public String getEpisodeType() {
        return episodeType;
    }

    public void setEpisodeType(String episodeType) {
        this.episodeType = episodeType;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }
}
