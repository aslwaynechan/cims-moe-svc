package hk.health.moe.pojo.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

public class GenericDto implements Serializable {


    private boolean isDidYouMean;
    private boolean isFreeText;

    private String displayNameType;
    private String shortName;
    //Chris For drug search match keyword from Redis  -Start
    //@JsonIgnore
    private String replacementString;
    //Chris For drug search match keyword from Redis  -End
    private String displayString;

    private boolean parent;
    private boolean swapDisplayFormat;

    public String toString() {
        return replacementString;
    }

    /**
     * Gets the display name type of this record.
     *
     * @return the display name type
     * <ul>
     * <li>A - Abberviation</li>
     * <li>F - Former Ban</li>
     * <li>O - Other name</li>
     * <li>V - VTM </li>
     * <li>N - Trade Name Alias </li>
     * </ul>
     */
    public String getDisplayNameType() {
        return displayNameType;
    }

    /**
     * Sets the display name type of this record.
     *
     * @param displayNameType the display name type
     *                        <ul>
     *                        <li>A - Abberviation</li>
     *                        <li>F - Former Ban</li>
     *                        <li>O - Other name</li>
     *                        <li>V - VTM </li>
     *                        <li>N - Trade Name Alias </li>
     *                        </ul>
     */
    public void setDisplayNameType(String displayNameType) {
        this.displayNameType = displayNameType;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getDisplayString() {
        return displayString;
    }

    public void setDisplayString(String displayString) {
        this.displayString = displayString;
    }

    public String getReplacementString() {
        return toString();
    }

    public void setReplacementString(String replacementString) {
        this.replacementString = replacementString;
    }

    public boolean isDidYouMean() {
        return isDidYouMean;
    }

    public void setDidYouMean(boolean isDidYouMean) {
        this.isDidYouMean = isDidYouMean;
    }

    public boolean isFreeText() {
        return isFreeText;
    }

    public void setFreeText(boolean isFreeText) {
        this.isFreeText = isFreeText;
    }

    public boolean isParent() {
        return parent;
    }

    public void setParent(boolean parent) {
        this.parent = parent;
    }

    public boolean isSwapDisplayFormat() {
        return swapDisplayFormat;
    }

    public void setSwapDisplayFormat(boolean swapDisplayFormat) {
        this.swapDisplayFormat = swapDisplayFormat;
    }
}
