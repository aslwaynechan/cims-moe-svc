package hk.health.moe.pojo.dto.inner;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/9/4
 */
public class InnerMaxDosDto {
    private Long multDoseNo;
    private Long maxDosage;

    public Long getMultDoseNo() {
        return multDoseNo;
    }

    public void setMultDoseNo(Long multDoseNo) {
        this.multDoseNo = multDoseNo;
    }

    public Long getMaxDosage() {
        return maxDosage;
    }

    public void setMaxDosage(Long maxDosage) {
        this.maxDosage = maxDosage;
    }
}
