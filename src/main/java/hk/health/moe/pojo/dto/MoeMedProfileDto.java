/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeMedProfileDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.rpc.IsSerializable;*/

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.Valid;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MoeMedProfileDto implements /*IsSerializable,*/ Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5793317271627670569L;

    private String hospcode;
    private Long cmsItemNo;
    private Long ordNo;
    private MoeEhrMedProfileDto moeEhrMedProfile = new MoeEhrMedProfileDto();
    private String patHospcode;
    private String caseNo;
    private String regimen = "D";
    private String multDose;
    private BigDecimal volValue;
    private String volUnit;
    private String volText;
    private String itemcode;
    private String firstDisplayname;
    private String tradename;
    private String strength;
    private String baseunit;
    private String formcode;
    private String routeCode;
    private String routeDesc;
    private String formDesc;
    private String salt;
    private String extraInfo;
    private String dangerdrug = "N";
    private String externalUse;
    private String nameType = "V";
    private String secondDisplayname;
    //private String dosage;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal dosage;
    private String modu;
    private String freqCode;
    private Long freq1;
    private String supFreqCode;
    private Long supFreq1;
    private Long supFreq2;
    private String dayOfWeek;
    private String adminTimeCode;
    private String prn = "N";
    private BigDecimal prnPercent;
    private String siteCode;
    private String supSiteDesc;
    private Long duration;
    private String durationUnit;
    private Date startDate;
    private Date endDate;
    private Long moQty;
    private String moQtyUnit;
    private String actionStatus;
    private String formulStatus;
    private String formulStatus2;
    private String patType;
    private String itemStatus = "A";
    private String specInstruct;
    private String specNote;
    private String translate;
    private String fixPeriod;
    private String restricted;
    private String singleUse;
    private String moCreate;
    private String displayRoutedesc;
    private String trueDisplayname;
    private Long orgItemNo;
    private String allowRepeat;
    private String freqText;
    private String supFreqText;
    private String trueAliasname;
    private String capdSystem;
    private String capdCalcium;
    private String capdConc;
    private String durationInputType;
    private String capdBaseunit;
    private String siteDesc;
    private String remarkCreateBy;
    private Date remarkCreateDate;
    private String remarkText;
    private String remarkConfirmBy;
    private Date remarkConfirmDate;
    private String trueConc;
    private String mdsInfo;
    private String adminTimeDesc;
    private String commentCreateBy;
    private Date commentCreateDate;
    private String updateBy;
    private Date updateTime;
    private List<MoeMedMultDoseDto> moeMedMultDoses = new ArrayList<MoeMedMultDoseDto>();
    @Valid
    private List<MoeEhrMedAllergenDto> moeEhrMedAllergens = new ArrayList<MoeEhrMedAllergenDto>();
    private String currentMoQtyUnit;
    private boolean validateCombo = true;
    private String currentFormulStatus;
    private String currentFreqText;
    private String currentSupFreqText;
    // Simon 20190719 start-- add
    private String myFavouriteId;
    private Long itemNo;
    // Simon 20190719 end-- add

    // Moe drug strength, not in DB
    private List<MoeDrugStrengthDto> strengths = new ArrayList<MoeDrugStrengthDto>();

    private Long version;

    //Chris 20190905 For getOrder API return generateSpecialIntervalText result --Start
    private List<String> specialIntervalText;
    //private String displayWithFreq;
    private List<String> displayWithFreq;
    //Chris 20190905 For getOrder API return generateSpecialIntervalText result --End

    //Chris 20191009 Max duration  -Start
    private Long maxDuration;
    //Chris 20191009 Max duration  -End

    //Chris 20191010 Min dosage  -Start
    private List<Double> minDosages;
    //Chris 20191010 Min dosage  -End

    //Chris 20191014 Min Dosage String  -Start
    private String minDosagesMessage;
    //Chris 20191014 Min Dosage String  -End

    //Simon 20190813 start-- add
    List<PreparationDto> prep;

    public List<PreparationDto> getPrep() {
        return prep;
    }

    public void setPrep(List<PreparationDto> prep) {
        this.prep = prep;
    }
    //Simon 20190813 end-- add

    public String getMyFavouriteId() {
        return myFavouriteId;
    }

    public void setMyFavouriteId(String myFavouriteId) {
        this.myFavouriteId = myFavouriteId;
    }

    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    public Long getCmsItemNo() {
        return cmsItemNo;
    }

    public void setCmsItemNo(Long cmsItemNo) {
        this.cmsItemNo = cmsItemNo;
    }

    public MoeEhrMedProfileDto getMoeEhrMedProfile() {
        if (this.moeEhrMedProfile == null) {
            this.moeEhrMedProfile = new MoeEhrMedProfileDto();
        }
        return moeEhrMedProfile;
    }

    public void setMoeEhrMedProfile(MoeEhrMedProfileDto moeEhrMedProfile) {
        this.moeEhrMedProfile = moeEhrMedProfile;
    }

    public String getPatHospcode() {
        return patHospcode;
    }

    public void setPatHospcode(String patHospcode) {
        this.patHospcode = patHospcode;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getRegimen() {
        return regimen;
    }

    public void setRegimen(String regimen) {
        this.regimen = regimen;
    }

    public String getMultDose() {
        return multDose;
    }

    public void setMultDose(String multDose) {
        this.multDose = multDose;
    }

    public BigDecimal getVolValue() {
        return volValue;
    }

    public void setVolValue(BigDecimal volValue) {
        this.volValue = volValue;
    }

    public String getVolUnit() {
        return volUnit;
    }

    public void setVolUnit(String volUnit) {
        this.volUnit = volUnit;
    }

    public String getVolText() {
        return volText;
    }

    public void setVolText(String volText) {
        this.volText = volText;
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public String getFirstDisplayname() {
        return firstDisplayname;
    }

    public void setFirstDisplayname(String firstDisplayname) {
        this.firstDisplayname = firstDisplayname;
    }

    public String getTradename() {
        return tradename;
    }

    public void setTradename(String tradename) {
        this.tradename = tradename;
    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
        if (strength != null && strength.indexOf("+") > -1) {
            String[] strengths = strength.split("\\+");
            this.strengths = new ArrayList<MoeDrugStrengthDto>();
            for (int i = 0; i < strengths.length; i++) {
                this.strengths.add(new MoeDrugStrengthDto(strengths[i].trim()));
            }
        }
    }

    public String getBaseunit() {
        return baseunit;
    }

    public void setBaseunit(String baseunit) {
        this.baseunit = baseunit;
    }

    public String getFormcode() {
        return formcode;
    }

    public void setFormcode(String formcode) {
        this.formcode = formcode;
    }

    public String getRouteCode() {
        return routeCode;
    }

    public void setRouteCode(String routeCode) {
        this.routeCode = routeCode;
    }

    public String getRouteDesc() {
        return routeDesc;
    }

    public void setRouteDesc(String routeDesc) {
        this.routeDesc = routeDesc;
    }

    public String getFormDesc() {
        return formDesc;
    }

    public void setFormDesc(String formDesc) {
        this.formDesc = formDesc;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }

    public String getDangerdrug() {
        return dangerdrug;
    }

    public void setDangerdrug(String dangerdrug) {
        this.dangerdrug = dangerdrug;
    }

    public String getExternalUse() {
        return externalUse;
    }

    public void setExternalUse(String externalUse) {
        this.externalUse = externalUse;
    }

    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    public String getSecondDisplayname() {
        return secondDisplayname;
    }

    public void setSecondDisplayname(String secondDisplayname) {
        this.secondDisplayname = secondDisplayname;
    }

/*    public String getDosageDisplay() {
        return (dosage == null) ? null : NumberFormat.getDecimalFormat().format(dosage.doubleValue());
    }*/

    public BigDecimal getDosage() {
        return dosage;
    }

    public void setDosage(BigDecimal dosage) {
        this.dosage = dosage;
    }

    public String getModu() {
        return modu;
    }

    public void setModu(String modu) {
        this.modu = modu;
    }

    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    public Long getFreq1() {
        return freq1;
    }

    public void setFreq1(Long freq1) {
        this.freq1 = freq1;
    }

    public String getSupFreqCode() {
        return supFreqCode;
    }

    public void setSupFreqCode(String supFreqCode) {
        this.supFreqCode = supFreqCode;
    }

    public Long getSupFreq1() {
        return supFreq1;
    }

    public void setSupFreq1(Long supFreq1) {
        this.supFreq1 = supFreq1;
    }

    public Long getSupFreq2() {
        return supFreq2;
    }

    public void setSupFreq2(Long supFreq2) {
        this.supFreq2 = supFreq2;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getAdminTimeCode() {
        return adminTimeCode;
    }

    public void setAdminTimeCode(String adminTimeCode) {
        this.adminTimeCode = adminTimeCode;
    }

    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    public BigDecimal getPrnPercent() {
        return prnPercent;
    }

    public void setPrnPercent(BigDecimal prnPercent) {
        this.prnPercent = prnPercent;
    }

    public String getSiteCode() {
        return siteCode;
    }

    public void setSiteCode(String siteCode) {
        this.siteCode = siteCode;
    }

    public String getSupSiteDesc() {
        return supSiteDesc;
    }

    public void setSupSiteDesc(String supSiteDesc) {
        this.supSiteDesc = supSiteDesc;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getMoQty() {
        return moQty;
    }

    public void setMoQty(Long moQty) {
        this.moQty = moQty;
    }

    public String getMoQtyUnit() {
        return moQtyUnit;
    }

    public void setMoQtyUnit(String moQtyUnit) {
        this.moQtyUnit = moQtyUnit;
    }

    public String getActionStatus() {
        return actionStatus;
    }

    public void setActionStatus(String actionStatus) {
        this.actionStatus = actionStatus;
    }

    public String getFormulStatus() {
        return formulStatus;
    }

    public void setFormulStatus(String formulStatus) {
        this.formulStatus = formulStatus;
    }

    public String getFormulStatus2() {
        return formulStatus2;
    }

    public void setFormulStatus2(String formulStatus2) {
        this.formulStatus2 = formulStatus2;
    }

    public String getPatType() {
        return patType;
    }

    public void setPatType(String patType) {
        this.patType = patType;
    }

    public String getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(String itemStatus) {
        this.itemStatus = itemStatus;
    }

    public String getSpecInstruct() {
        return specInstruct;
    }

    public void setSpecInstruct(String specInstruct) {
        this.specInstruct = specInstruct;
    }

    public String getSpecNote() {
        return specNote;
    }

    public void setSpecNote(String specNote) {
        this.specNote = specNote;
    }

    public String getTranslate() {
        return translate;
    }

    public void setTranslate(String translate) {
        this.translate = translate;
    }

    public String getFixPeriod() {
        return fixPeriod;
    }

    public void setFixPeriod(String fixPeriod) {
        this.fixPeriod = fixPeriod;
    }

    public String getRestricted() {
        return restricted;
    }

    public void setRestricted(String restricted) {
        this.restricted = restricted;
    }

    public String getSingleUse() {
        return singleUse;
    }

    public void setSingleUse(String singleUse) {
        this.singleUse = singleUse;
    }

    public String getMoCreate() {
        return moCreate;
    }

    public void setMoCreate(String moCreate) {
        this.moCreate = moCreate;
    }

    public String getDisplayRoutedesc() {
        return displayRoutedesc;
    }

    public void setDisplayRoutedesc(String displayRoutedesc) {
        this.displayRoutedesc = displayRoutedesc;
    }

    public String getTrueDisplayname() {
        return trueDisplayname;
    }

    public void setTrueDisplayname(String trueDisplayname) {
        this.trueDisplayname = trueDisplayname;
    }

    public Long getOrgItemNo() {
        return orgItemNo;
    }

    public void setOrgItemNo(Long orgItemNo) {
        this.orgItemNo = orgItemNo;
    }

    public String getAllowRepeat() {
        return allowRepeat;
    }

    public void setAllowRepeat(String allowRepeat) {
        this.allowRepeat = allowRepeat;
    }

    public String getFreqText() {
        return freqText;
    }

    public void setFreqText(String freqText) {
        this.freqText = freqText;
    }

    public String getSupFreqText() {
        return supFreqText;
    }

    public void setSupFreqText(String supFreqText) {
        this.supFreqText = supFreqText;
    }

    public String getTrueAliasname() {
        return trueAliasname;
    }

    public void setTrueAliasname(String trueAliasname) {
        this.trueAliasname = trueAliasname;
    }

    public String getCapdSystem() {
        return capdSystem;
    }

    public void setCapdSystem(String capdSystem) {
        this.capdSystem = capdSystem;
    }

    public String getCapdCalcium() {
        return capdCalcium;
    }

    public void setCapdCalcium(String capdCalcium) {
        this.capdCalcium = capdCalcium;
    }

    public String getCapdConc() {
        return capdConc;
    }

    public void setCapdConc(String capdConc) {
        this.capdConc = capdConc;
    }

    public String getDurationInputType() {
        return durationInputType;
    }

    public void setDurationInputType(String durationInputType) {
        this.durationInputType = durationInputType;
    }

    public String getCapdBaseunit() {
        return capdBaseunit;
    }

    public void setCapdBaseunit(String capdBaseunit) {
        this.capdBaseunit = capdBaseunit;
    }

    public String getSiteDesc() {
        return siteDesc;
    }

    public void setSiteDesc(String siteDesc) {
        this.siteDesc = siteDesc;
    }

    public String getRemarkCreateBy() {
        return remarkCreateBy;
    }

    public void setRemarkCreateBy(String remarkCreateBy) {
        this.remarkCreateBy = remarkCreateBy;
    }

    public Date getRemarkCreateDate() {
        return remarkCreateDate;
    }

    public void setRemarkCreateDate(Date remarkCreateDate) {
        this.remarkCreateDate = remarkCreateDate;
    }

    public String getRemarkText() {
        return remarkText;
    }

    public void setRemarkText(String remarkText) {
        this.remarkText = remarkText;
    }

    public String getRemarkConfirmBy() {
        return remarkConfirmBy;
    }

    public void setRemarkConfirmBy(String remarkConfirmBy) {
        this.remarkConfirmBy = remarkConfirmBy;
    }

    public Date getRemarkConfirmDate() {
        return remarkConfirmDate;
    }

    public void setRemarkConfirmDate(Date remarkConfirmDate) {
        this.remarkConfirmDate = remarkConfirmDate;
    }

    public String getTrueConc() {
        return trueConc;
    }

    public void setTrueConc(String trueConc) {
        this.trueConc = trueConc;
    }

    public String getMdsInfo() {
        return mdsInfo;
    }

    public void setMdsInfo(String mdsInfo) {
        this.mdsInfo = mdsInfo;
    }

    public String getAdminTimeDesc() {
        return adminTimeDesc;
    }

    public void setAdminTimeDesc(String adminTimeDesc) {
        this.adminTimeDesc = adminTimeDesc;
    }

    public String getCommentCreateBy() {
        return commentCreateBy;
    }

    public void setCommentCreateBy(String commentCreateBy) {
        this.commentCreateBy = commentCreateBy;
    }

    public Date getCommentCreateDate() {
        return commentCreateDate;
    }

    public void setCommentCreateDate(Date commentCreateDate) {
        this.commentCreateDate = commentCreateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public List<MoeMedMultDoseDto> getMoeMedMultDoses() {
        return moeMedMultDoses;
    }

    public void addMoeMedMultDose(MoeMedMultDoseDto moeMedMultDoseDto) {
        this.moeMedMultDoses.add(moeMedMultDoseDto);
    }

    public void setMoeMedMultDoses(List<MoeMedMultDoseDto> moeMedMultDoses) {
        if (moeMedMultDoses != null)
            this.moeMedMultDoses = moeMedMultDoses;
    }

    public void setOrdNo(Long ordNo) {
        this.ordNo = ordNo;
    }

    public Long getOrdNo() {
        return ordNo;
    }

    public void setMoeEhrMedAllergens(List<MoeEhrMedAllergenDto> moeEhrMedAllergens) {
        if (moeEhrMedAllergens != null)
            this.moeEhrMedAllergens = moeEhrMedAllergens;
    }

    public List<MoeEhrMedAllergenDto> getMoeEhrMedAllergens() {
        return moeEhrMedAllergens;
    }

    public void addMoeEhrMedAllergens(MoeEhrMedAllergenDto moeEhrMedAllergenDto) {
        this.moeEhrMedAllergens.add(moeEhrMedAllergenDto);
    }

    public List<MoeDrugStrengthDto> getStrengths() {
        return strengths;
    }

    public void setStrengths(List<MoeDrugStrengthDto> strengths) {
        this.strengths = strengths;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getVersion() {
        return version;
    }

    public void setCurrentMoQtyUnit(String currentMoQtyUnit) {
        this.currentMoQtyUnit = currentMoQtyUnit;
    }

    public String getCurrentMoQtyUnit() {
        return currentMoQtyUnit;
    }

    public String getCurrentFormulStatus() {
        return currentFormulStatus;
    }

    public void setCurrentFormulStatus(String currentFormulStatus) {
        this.currentFormulStatus = currentFormulStatus;
    }

    public String getCurrentFreqText() {
        return currentFreqText;
    }

    public void setCurrentFreqText(String currentFreqText) {
        this.currentFreqText = currentFreqText;
    }

    public String getCurrentSupFreqText() {
        return currentSupFreqText;
    }

    public void setCurrentSupFreqText(String currentSupFreqText) {
        this.currentSupFreqText = currentSupFreqText;
    }

    public boolean isValidateCombo() {
        return validateCombo;
    }

    public void setValidateCombo(boolean validateCombo) {
        this.validateCombo = validateCombo;
    }

    public Long getItemNo() {
        return itemNo;
    }

    public void setItemNo(Long itemNo) {
        this.itemNo = itemNo;
    }

    public List<String> getSpecialIntervalText() {
        return specialIntervalText;
    }

    public void setSpecialIntervalText(List<String> specialIntervalText) {
        this.specialIntervalText = specialIntervalText;
    }

    public List<String> getDisplayWithFreq() {
        return displayWithFreq;
    }

    public void setDisplayWithFreq(List<String> displayWithFreq) {
        this.displayWithFreq = displayWithFreq;
    }

    public Long getMaxDuration() {
        return maxDuration;
    }

    public void setMaxDuration(Long maxDuration) {
        this.maxDuration = maxDuration;
    }

    public List<Double> getMinDosages() {
        return minDosages;
    }

    public void setMinDosages(List<Double> minDosages) {
        this.minDosages = minDosages;
    }

    public String getMinDosagesMessage() {
        return minDosagesMessage;
    }

    public void setMinDosagesMessage(String minDosagesMessage) {
        this.minDosagesMessage = minDosagesMessage;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MoeMedProfileDTO [actionStatus=");
        builder.append(actionStatus);
        builder.append(", adminTimeCode=");
        builder.append(adminTimeCode);
        builder.append(", adminTimeDesc=");
        builder.append(adminTimeDesc);
        builder.append(", allowRepeat=");
        builder.append(allowRepeat);
        builder.append(", baseunit=");
        builder.append(baseunit);
        builder.append(", capdBaseunit=");
        builder.append(capdBaseunit);
        builder.append(", capdCalcium=");
        builder.append(capdCalcium);
        builder.append(", capdConc=");
        builder.append(capdConc);
        builder.append(", capdSystem=");
        builder.append(capdSystem);
        builder.append(", caseNo=");
        builder.append(caseNo);
        builder.append(", cmsItemNo=");
        builder.append(cmsItemNo);
        builder.append(", commentCreateBy=");
        builder.append(commentCreateBy);
        builder.append(", commentCreateDate=");
        builder.append(commentCreateDate);
        builder.append(", dangerdrug=");
        builder.append(dangerdrug);
        builder.append(", dayOfWeek=");
        builder.append(dayOfWeek);
        builder.append(", displayRoutedesc=");
        builder.append(displayRoutedesc);
        builder.append(", dosage=");
        builder.append(dosage);
        builder.append(", duration=");
        builder.append(duration);
        builder.append(", durationInputType=");
        builder.append(durationInputType);
        builder.append(", durationUnit=");
        builder.append(durationUnit);
        builder.append(", endDate=");
        builder.append(endDate);
        builder.append(", externalUse=");
        builder.append(externalUse);
        builder.append(", extraInfo=");
        builder.append(extraInfo);
        builder.append(", firstDisplayname=");
        builder.append(firstDisplayname);
        builder.append(", fixPeriod=");
        builder.append(fixPeriod);
        builder.append(", formDesc=");
        builder.append(formDesc);
        builder.append(", formcode=");
        builder.append(formcode);
        builder.append(", formulStatus=");
        builder.append(formulStatus);
        builder.append(", formulStatus2=");
        builder.append(formulStatus2);
        builder.append(", freq1=");
        builder.append(freq1);
        builder.append(", freqCode=");
        builder.append(freqCode);
        builder.append(", freqText=");
        builder.append(freqText);
        builder.append(", hospcode=");
        builder.append(hospcode);
        builder.append(", itemStatus=");
        builder.append(itemStatus);
        builder.append(", itemcode=");
        builder.append(itemcode);
        builder.append(", mdsInfo=");
        builder.append(mdsInfo);
        builder.append(", moCreate=");
        builder.append(moCreate);
        builder.append(", moQty=");
        builder.append(moQty);
        builder.append(", moQtyUnit=");
        builder.append(moQtyUnit);
        builder.append(", modu=");
        builder.append(modu);
        builder.append(", moeEhrMedAllergens=");
        builder.append(moeEhrMedAllergens);
        builder.append(", moeEhrMedProfile=");
        builder.append(moeEhrMedProfile);
        builder.append(", moeMedMultDoses=");
        builder.append(moeMedMultDoses);
        builder.append(", multDose=");
        builder.append(multDose);
        builder.append(", nameType=");
        builder.append(nameType);
        builder.append(", ordNo=");
        builder.append(ordNo);
        builder.append(", orgItemNo=");
        builder.append(orgItemNo);
        builder.append(", patHospcode=");
        builder.append(patHospcode);
        builder.append(", patType=");
        builder.append(patType);
        builder.append(", prn=");
        builder.append(prn);
        builder.append(", prnPercent=");
        builder.append(prnPercent);
        builder.append(", regimen=");
        builder.append(regimen);
        builder.append(", remarkConfirmBy=");
        builder.append(remarkConfirmBy);
        builder.append(", remarkConfirmDate=");
        builder.append(remarkConfirmDate);
        builder.append(", remarkCreateBy=");
        builder.append(remarkCreateBy);
        builder.append(", remarkCreateDate=");
        builder.append(remarkCreateDate);
        builder.append(", remarkText=");
        builder.append(remarkText);
        builder.append(", restricted=");
        builder.append(restricted);
        builder.append(", routeCode=");
        builder.append(routeCode);
        builder.append(", routeDesc=");
        builder.append(routeDesc);
        builder.append(", salt=");
        builder.append(salt);
        builder.append(", secondDisplayname=");
        builder.append(secondDisplayname);
        builder.append(", singleUse=");
        builder.append(singleUse);
        builder.append(", siteCode=");
        builder.append(siteCode);
        builder.append(", siteDesc=");
        builder.append(siteDesc);
        builder.append(", specInstruct=");
        builder.append(specInstruct);
        builder.append(", specNote=");
        builder.append(specNote);
        builder.append(", startDate=");
        builder.append(startDate);
        builder.append(", strength=");
        builder.append(strength);
        builder.append(", supFreq1=");
        builder.append(supFreq1);
        builder.append(", supFreq2=");
        builder.append(supFreq2);
        builder.append(", supFreqCode=");
        builder.append(supFreqCode);
        builder.append(", supFreqText=");
        builder.append(supFreqText);
        builder.append(", supSiteDesc=");
        builder.append(supSiteDesc);
        builder.append(", tradename=");
        builder.append(tradename);
        builder.append(", translate=");
        builder.append(translate);
        builder.append(", trueAliasname=");
        builder.append(trueAliasname);
        builder.append(", trueConc=");
        builder.append(trueConc);
        builder.append(", trueDisplayname=");
        builder.append(trueDisplayname);
        builder.append(", updateBy=");
        builder.append(updateBy);
        builder.append(", updateTime=");
        builder.append(updateTime);
        builder.append(", version=");
        builder.append(version);
        builder.append(", volText=");
        builder.append(volText);
        builder.append(", volUnit=");
        builder.append(volUnit);
        builder.append(", volValue=");
        builder.append(volValue);
        builder.append(", currentMoQtyUnit=");
        builder.append(currentMoQtyUnit);
        builder.append(", currentFormulStatus=");
        builder.append(currentFormulStatus);
        builder.append(", currentFreqText=");
        builder.append(currentFreqText);
        builder.append(", currentSupFreqText=");
        builder.append(currentSupFreqText);
        builder.append("]");
        return builder.toString();
    }

}
