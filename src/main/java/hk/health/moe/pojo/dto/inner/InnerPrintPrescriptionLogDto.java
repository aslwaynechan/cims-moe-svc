package hk.health.moe.pojo.dto.inner;

import com.fasterxml.jackson.annotation.JsonInclude;
import hk.health.moe.pojo.dto.MoeEhrRxImageLogDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class InnerPrintPrescriptionLogDto {

    private MoeEhrRxImageLogDto dischargeReportLog;
    private MoeEhrRxImageLogDto purchaseReportLog;

    public MoeEhrRxImageLogDto getDischargeReportLog() {
        return dischargeReportLog;
    }

    public void setDischargeReportLog(MoeEhrRxImageLogDto dischargeReportLog) {
        this.dischargeReportLog = dischargeReportLog;
    }

    public MoeEhrRxImageLogDto getPurchaseReportLog() {
        return purchaseReportLog;
    }

    public void setPurchaseReportLog(MoeEhrRxImageLogDto purchaseReportLog) {
        this.purchaseReportLog = purchaseReportLog;
    }

}
