/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  MOE Drug Maintenance
 *
 * PROGRAM NAME    :  MoeDrugStrengthLocalDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;

public class MoeDrugStrengthLocalDto implements java.io.Serializable {

    private String localDrugId;
    private Long version;
    private String strength;
    private String strengthLevelExtraInfo;
    private String amp;
    private Integer ampId;
    private String vmp;
    private Integer vmpId;
    private String createUserId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private Date createDtm;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private Date updateDtm;

    public MoeDrugStrengthLocalDto() {
        this.localDrugId = "";
        this.amp = "";
        this.vmp = "";
        this.createHosp = "";
        this.createDtm = new Date();
        this.updateDtm = new Date();
    }

    public MoeDrugStrengthLocalDto(String createUserId, String createUser, String createHosp,
                                   String createRank, String createRankDesc, Date createDtm) {
        setCreateUser(createUser);
        setCreateUserId(createUserId);
        setCreateHosp(createHosp);
        setCreateRank(createRank);
        setCreateRankDesc(createRankDesc);
        setCreateDtm(createDtm);
    }

    public String getLocalDrugId() {
        return this.localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        // Ricci 20191009 start --
        this.localDrugId = StringUtils.isNotBlank(localDrugId) ? localDrugId.toUpperCase() : localDrugId;
        // Ricci 20191009 end --
    }

    public Long getVersion() {
        return this.version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getStrength() {
        return this.strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public String getStrengthLevelExtraInfo() {
        return this.strengthLevelExtraInfo;
    }

    public void setStrengthLevelExtraInfo(String strengthLevelExtraInfo) {
        this.strengthLevelExtraInfo = strengthLevelExtraInfo;
    }

    public String getAmp() {
        return this.amp;
    }

    public void setAmp(String amp) {
        this.amp = amp;
    }

    public Integer getAmpId() {
        return this.ampId;
    }

    public void setAmpId(Integer ampId) {
        this.ampId = ampId;
    }

    public String getVmp() {
        return this.vmp;
    }

    public void setVmp(String vmp) {
        this.vmp = vmp;
    }

    public Integer getVmpId() {
        return this.vmpId;
    }

    public void setVmpId(Integer vmpId) {
        this.vmpId = vmpId;
    }

    public String getCreateUserId() {
        return this.createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUser() {
        return this.createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateHosp() {
        return this.createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    public String getCreateRank() {
        return this.createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    public String getCreateRankDesc() {
        return this.createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    public Date getCreateDtm() {
        return this.createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    public String getUpdateUserId() {
        return this.updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getUpdateUser() {
        return this.updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateHosp() {
        return this.updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    public String getUpdateRank() {
        return this.updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    public String getUpdateRankDesc() {
        return this.updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    public Date getUpdateDtm() {
        return this.updateDtm;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }

}
