package hk.health.moe.pojo.dto;

import org.apache.commons.lang3.StringUtils;

public class MoeDrugBySpecialtyDto implements java.io.Serializable {

    private static final long serialVersionUID = -1820351486191381727L;
    private String hospcode;
    private String userSpecialty;
    private String localDrugId;
    private Long version;
/*    private String createUserId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;*/

    public String getHospcode() {
        return this.hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    public String getUserSpecialty() {
        return this.userSpecialty;
    }

    public void setUserSpecialty(String userSpecialty) {
        this.userSpecialty = userSpecialty;
    }

    public String getLocalDrugId() {
        return this.localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        // Ricci 20191009 start --
        this.localDrugId = StringUtils.isNotBlank(localDrugId) ? localDrugId.toUpperCase() : localDrugId;
        // Ricci 20191009 end --
    }

    public Long getVersion() {
        return this.version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

/*    public String getCreateUserId() {
        return this.createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUser() {
        return this.createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateHosp() {
        return this.createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    public String getCreateRank() {
        return this.createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    public String getCreateRankDesc() {
        return this.createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }*/

}
