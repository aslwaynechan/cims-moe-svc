/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeCommonDosageTypeDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;

public class MoeCommonDosageTypeDto implements Serializable/*, IsSerializable*/ {

    /**
     *
     */
    private static final long serialVersionUID = 3041932269138986262L;

    private String commonDosageType;
    private String commonDosageTypeDesc;
    private String display;
    private Long rank;

    public String getCommonDosageType() {
        return commonDosageType;
    }

    public void setCommonDosageType(String commonDosageType) {
        this.commonDosageType = commonDosageType;
    }

    public String getCommonDosageTypeDesc() {
        return commonDosageTypeDesc;
    }

    public void setCommonDosageTypeDesc(String commonDosageTypeDesc) {
        this.commonDosageTypeDesc = commonDosageTypeDesc;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public Long getRank() {
        return this.rank;
    }

    public void setRank(Long rank) {
        this.rank = rank;
    }

}
