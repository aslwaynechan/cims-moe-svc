
package hk.health.moe.pojo.dto.webservice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 *
 *
 * <pre>
 * &lt;complexType name="MedProfileDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="itemNo" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="doseInstruction" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="doseInstructionWithStyle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="doseInstructionPrinting" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="vtm" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="vtmId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="dangerousDrug" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="aliasName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="aliasNameType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tradeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="manufacturer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="genericIndicator" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="drugName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="drugNameType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="drugTermId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="drugRoute" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="screenDisplay" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="localDrugKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dosage" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="dosageUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dosageUnitId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="dosageUnitDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="freqCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="freqText" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="freqValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="freqId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="freqDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="supplFreqCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="supplFreqText" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="supplFreqValue1" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="supplFreqValue2" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="supplFreqId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="supplFreqDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dayOfWeek" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cycleMultiplier" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="prn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="form" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="formExtraInfo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="route" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="routeId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="routeDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="duration" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="durationUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="durationUnitId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="durationUnitDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totalQuantity" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="totalQuantityUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="totalQuantityUnitId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="totalQuantityUnitDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="strengthCompulsory" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="strength" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="strengthExtraInfo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="actionStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="actionStatusDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="site" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="siteId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="siteDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="specialInstruction" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="orderLineType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MedMultDoseDto" type="{http://ehr.gov.hk/moe/ws/beans}MedMultDoseDto" maxOccurs="1000" minOccurs="0"/>
 *         &lt;element name="MedAllergenDto" type="{http://ehr.gov.hk/moe/ws/beans}MedAllergenDto" maxOccurs="1000" minOccurs="0"/>
 *         &lt;element name="createBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="createDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="createHosp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="createRank" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="createRankDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="createUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="updateBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="updateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="updateHosp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="updateRank" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="updateRankDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="updateUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="trxType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prescriptionSortingSeq" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MedProfileDto",  propOrder = {
    "itemNo",
    "doseInstruction",
    "doseInstructionWithStyle",
    "doseInstructionPrinting",
    "vtm",
    "vtmId",
    "dangerousDrug",
    "aliasName",
    "aliasNameType",
    "tradeName",
    "manufacturer",
    "genericIndicator",
    "drugName",
    "drugNameType",
    "drugTermId",
    "drugRoute",
    "screenDisplay",
    "localDrugKey",
    "dosage",
    "dosageUnit",
    "dosageUnitId",
    "dosageUnitDesc",
    "freqCode",
    "freqText",
    "freqValue",
    "freqId",
    "freqDesc",
    "supplFreqCode",
    "supplFreqText",
    "supplFreqValue1",
    "supplFreqValue2",
    "supplFreqId",
    "supplFreqDesc",
    "dayOfWeek",
    "cycleMultiplier",
    "prn",
    "form",
    "formExtraInfo",
    "route",
    "routeId",
    "routeDesc",
    "duration",
    "durationUnit",
    "durationUnitId",
    "durationUnitDesc",
    "totalQuantity",
    "totalQuantityUnit",
    "totalQuantityUnitId",
    "totalQuantityUnitDesc",
    "strengthCompulsory",
    "strength",
    "strengthExtraInfo",
    "actionStatus",
    "actionStatusDesc",
    "site",
    "siteId",
    "siteDesc",
    "startDate",
    "endDate",
    "specialInstruction",
    "orderLineType",
    "medMultDoseDto",
    "medAllergenDto",
    "createBy",
    "createDate",
    "createHosp",
    "createRank",
    "createRankDesc",
    "createUser",
    "updateBy",
    "updateDate",
    "updateHosp",
    "updateRank",
    "updateRankDesc",
    "updateUser",
    "trxType",
    "prescriptionSortingSeq"
})
public class MedProfileDto {

    @XmlElement
    protected long itemNo;
    @XmlElement( required = true)
    protected String doseInstruction;
    @XmlElement( required = true)
    protected String doseInstructionWithStyle;
    @XmlElement( required = true)
    protected String doseInstructionPrinting;
    @XmlElement( required = true)
    protected String vtm;
    @XmlElement
    protected long vtmId;
    @XmlElement( required = true)
    protected String dangerousDrug;
    @XmlElement( required = true)
    protected String aliasName;
    @XmlElement( required = true)
    protected String aliasNameType;
    @XmlElement( required = true)
    protected String tradeName;
    @XmlElement( required = true)
    protected String manufacturer;
    @XmlElement( required = true)
    protected String genericIndicator;
    @XmlElement( required = true)
    protected String drugName;
    @XmlElement( required = true)
    protected String drugNameType;
    @XmlElement
    protected long drugTermId;
    @XmlElement( required = true)
    protected String drugRoute;
    @XmlElement
    protected String screenDisplay;
    @XmlElement( required = true)
    protected String localDrugKey;
    @XmlElement( required = true)
    protected BigDecimal dosage;
    @XmlElement( required = true)
    protected String dosageUnit;
    @XmlElement
    protected Long dosageUnitId;
    @XmlElement
    protected String dosageUnitDesc;
    @XmlElement( required = true)
    protected String freqCode;
    @XmlElement( required = true)
    protected String freqText;
    @XmlElement
    protected long freqValue;
    @XmlElement
    protected Long freqId;
    @XmlElement
    protected String freqDesc;
    @XmlElement( required = true)
    protected String supplFreqCode;
    @XmlElement( required = true)
    protected String supplFreqText;
    @XmlElement
    protected long supplFreqValue1;
    @XmlElement
    protected long supplFreqValue2;
    @XmlElement
    protected Long supplFreqId;
    @XmlElement
    protected String supplFreqDesc;
    @XmlElement( required = true)
    protected String dayOfWeek;
    @XmlElement
    protected long cycleMultiplier;
    @XmlElement
    protected String prn;
    @XmlElement( required = true)
    protected String form;
    @XmlElement( required = true)
    protected String formExtraInfo;
    @XmlElement( required = true)
    protected String route;
    @XmlElement
    protected Long routeId;
    @XmlElement
    protected String routeDesc;
    @XmlElement
    protected long duration;
    @XmlElement( required = true)
    protected String durationUnit;
    @XmlElement
    protected Long durationUnitId;
    @XmlElement
    protected String durationUnitDesc;
    @XmlElement
    protected long totalQuantity;
    @XmlElement( required = true)
    protected String totalQuantityUnit;
    @XmlElement
    protected Long totalQuantityUnitId;
    @XmlElement
    protected String totalQuantityUnitDesc;
    @XmlElement( required = true)
    protected String strengthCompulsory;
    @XmlElement( required = true)
    protected String strength;
    @XmlElement( required = true)
    protected String strengthExtraInfo;
    @XmlElement( required = true)
    protected String actionStatus;
    @XmlElement( required = true)
    protected String actionStatusDesc;
    @XmlElement( required = true)
    protected String site;
    @XmlElement
    protected Long siteId;
    @XmlElement
    protected String siteDesc;
    @XmlElement( required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDate;
    @XmlElement( required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endDate;
    @XmlElement( required = true)
    protected String specialInstruction;
    @XmlElement( required = true)
    protected String orderLineType;
    @XmlElement(name = "MedMultDoseDto")
    protected List<MedMultDoseDto> medMultDoseDto = new ArrayList<>();
    @XmlElement(name = "MedAllergenDto")
    protected List<MedAllergenDto> medAllergenDto = new ArrayList<>();
    @XmlElement
    protected String createBy;
    @XmlElement
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createDate;
    @XmlElement
    protected String createHosp;
    @XmlElement
    protected String createRank;
    @XmlElement
    protected String createRankDesc;
    @XmlElement
    protected String createUser;
    @XmlElement
    protected String updateBy;
    @XmlElement
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar updateDate;
    @XmlElement
    protected String updateHosp;
    @XmlElement
    protected String updateRank;
    @XmlElement
    protected String updateRankDesc;
    @XmlElement
    protected String updateUser;
    @XmlElement
    protected String trxType;
    @XmlElement
    protected Long prescriptionSortingSeq;

    public long getItemNo() {
        return itemNo;
    }

    public void setItemNo(long itemNo) {
        this.itemNo = itemNo;
    }

    public String getDoseInstruction() {
        return doseInstruction;
    }

    public void setDoseInstruction(String doseInstruction) {
        this.doseInstruction = doseInstruction;
    }

    public String getDoseInstructionWithStyle() {
        return doseInstructionWithStyle;
    }

    public void setDoseInstructionWithStyle(String doseInstructionWithStyle) {
        this.doseInstructionWithStyle = doseInstructionWithStyle;
    }

    public String getDoseInstructionPrinting() {
        return doseInstructionPrinting;
    }

    public void setDoseInstructionPrinting(String doseInstructionPrinting) {
        this.doseInstructionPrinting = doseInstructionPrinting;
    }

    public String getVtm() {
        return vtm;
    }

    public void setVtm(String vtm) {
        this.vtm = vtm;
    }

    public long getVtmId() {
        return vtmId;
    }

    public void setVtmId(long vtmId) {
        this.vtmId = vtmId;
    }

    public String getDangerousDrug() {
        return dangerousDrug;
    }

    public void setDangerousDrug(String dangerousDrug) {
        this.dangerousDrug = dangerousDrug;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getAliasNameType() {
        return aliasNameType;
    }

    public void setAliasNameType(String aliasNameType) {
        this.aliasNameType = aliasNameType;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getGenericIndicator() {
        return genericIndicator;
    }

    public void setGenericIndicator(String genericIndicator) {
        this.genericIndicator = genericIndicator;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getDrugNameType() {
        return drugNameType;
    }

    public void setDrugNameType(String drugNameType) {
        this.drugNameType = drugNameType;
    }

    public long getDrugTermId() {
        return drugTermId;
    }

    public void setDrugTermId(long drugTermId) {
        this.drugTermId = drugTermId;
    }

    public String getDrugRoute() {
        return drugRoute;
    }

    public void setDrugRoute(String drugRoute) {
        this.drugRoute = drugRoute;
    }

    public String getScreenDisplay() {
        return screenDisplay;
    }

    public void setScreenDisplay(String screenDisplay) {
        this.screenDisplay = screenDisplay;
    }

    public String getLocalDrugKey() {
        return localDrugKey;
    }

    public void setLocalDrugKey(String localDrugKey) {
        this.localDrugKey = localDrugKey;
    }

    public BigDecimal getDosage() {
        return dosage;
    }

    public void setDosage(BigDecimal dosage) {
        this.dosage = dosage;
    }

    public String getDosageUnit() {
        return dosageUnit;
    }

    public void setDosageUnit(String dosageUnit) {
        this.dosageUnit = dosageUnit;
    }

    public Long getDosageUnitId() {
        return dosageUnitId;
    }

    public void setDosageUnitId(Long dosageUnitId) {
        this.dosageUnitId = dosageUnitId;
    }

    public String getDosageUnitDesc() {
        return dosageUnitDesc;
    }

    public void setDosageUnitDesc(String dosageUnitDesc) {
        this.dosageUnitDesc = dosageUnitDesc;
    }

    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    public String getFreqText() {
        return freqText;
    }

    public void setFreqText(String freqText) {
        this.freqText = freqText;
    }

    public long getFreqValue() {
        return freqValue;
    }

    public void setFreqValue(long freqValue) {
        this.freqValue = freqValue;
    }

    public Long getFreqId() {
        return freqId;
    }

    public void setFreqId(Long freqId) {
        this.freqId = freqId;
    }

    public String getFreqDesc() {
        return freqDesc;
    }

    public void setFreqDesc(String freqDesc) {
        this.freqDesc = freqDesc;
    }

    public String getSupplFreqCode() {
        return supplFreqCode;
    }

    public void setSupplFreqCode(String supplFreqCode) {
        this.supplFreqCode = supplFreqCode;
    }

    public String getSupplFreqText() {
        return supplFreqText;
    }

    public void setSupplFreqText(String supplFreqText) {
        this.supplFreqText = supplFreqText;
    }

    public long getSupplFreqValue1() {
        return supplFreqValue1;
    }

    public void setSupplFreqValue1(long supplFreqValue1) {
        this.supplFreqValue1 = supplFreqValue1;
    }

    public long getSupplFreqValue2() {
        return supplFreqValue2;
    }

    public void setSupplFreqValue2(long supplFreqValue2) {
        this.supplFreqValue2 = supplFreqValue2;
    }

    public Long getSupplFreqId() {
        return supplFreqId;
    }

    public void setSupplFreqId(Long supplFreqId) {
        this.supplFreqId = supplFreqId;
    }

    public String getSupplFreqDesc() {
        return supplFreqDesc;
    }

    public void setSupplFreqDesc(String supplFreqDesc) {
        this.supplFreqDesc = supplFreqDesc;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public long getCycleMultiplier() {
        return cycleMultiplier;
    }

    public void setCycleMultiplier(long cycleMultiplier) {
        this.cycleMultiplier = cycleMultiplier;
    }

    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public String getFormExtraInfo() {
        return formExtraInfo;
    }

    public void setFormExtraInfo(String formExtraInfo) {
        this.formExtraInfo = formExtraInfo;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public String getRouteDesc() {
        return routeDesc;
    }

    public void setRouteDesc(String routeDesc) {
        this.routeDesc = routeDesc;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    public Long getDurationUnitId() {
        return durationUnitId;
    }

    public void setDurationUnitId(Long durationUnitId) {
        this.durationUnitId = durationUnitId;
    }

    public String getDurationUnitDesc() {
        return durationUnitDesc;
    }

    public void setDurationUnitDesc(String durationUnitDesc) {
        this.durationUnitDesc = durationUnitDesc;
    }

    public long getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getTotalQuantityUnit() {
        return totalQuantityUnit;
    }

    public void setTotalQuantityUnit(String totalQuantityUnit) {
        this.totalQuantityUnit = totalQuantityUnit;
    }

    public Long getTotalQuantityUnitId() {
        return totalQuantityUnitId;
    }

    public void setTotalQuantityUnitId(Long totalQuantityUnitId) {
        this.totalQuantityUnitId = totalQuantityUnitId;
    }

    public String getTotalQuantityUnitDesc() {
        return totalQuantityUnitDesc;
    }

    public void setTotalQuantityUnitDesc(String totalQuantityUnitDesc) {
        this.totalQuantityUnitDesc = totalQuantityUnitDesc;
    }

    public String getStrengthCompulsory() {
        return strengthCompulsory;
    }

    public void setStrengthCompulsory(String strengthCompulsory) {
        this.strengthCompulsory = strengthCompulsory;
    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public String getStrengthExtraInfo() {
        return strengthExtraInfo;
    }

    public void setStrengthExtraInfo(String strengthExtraInfo) {
        this.strengthExtraInfo = strengthExtraInfo;
    }

    public String getActionStatus() {
        return actionStatus;
    }

    public void setActionStatus(String actionStatus) {
        this.actionStatus = actionStatus;
    }

    public String getActionStatusDesc() {
        return actionStatusDesc;
    }

    public void setActionStatusDesc(String actionStatusDesc) {
        this.actionStatusDesc = actionStatusDesc;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public String getSiteDesc() {
        return siteDesc;
    }

    public void setSiteDesc(String siteDesc) {
        this.siteDesc = siteDesc;
    }

    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    public void setStartDate(XMLGregorianCalendar startDate) {
        this.startDate = startDate;
    }

    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    public void setEndDate(XMLGregorianCalendar endDate) {
        this.endDate = endDate;
    }

    public String getSpecialInstruction() {
        return specialInstruction;
    }

    public void setSpecialInstruction(String specialInstruction) {
        this.specialInstruction = specialInstruction;
    }

    public String getOrderLineType() {
        return orderLineType;
    }

    public void setOrderLineType(String orderLineType) {
        this.orderLineType = orderLineType;
    }

    public List<MedMultDoseDto> getMedMultDoseDto() {
        return medMultDoseDto;
    }

    public void setMedMultDoseDto(List<MedMultDoseDto> medMultDoseDto) {
        this.medMultDoseDto = medMultDoseDto;
    }

    public List<MedAllergenDto> getMedAllergenDto() {
        return medAllergenDto;
    }

    public void setMedAllergenDto(List<MedAllergenDto> medAllergenDto) {
        this.medAllergenDto = medAllergenDto;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public XMLGregorianCalendar getCreateDate() {
        return createDate;
    }

    public void setCreateDate(XMLGregorianCalendar createDate) {
        this.createDate = createDate;
    }

    public String getCreateHosp() {
        return createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    public String getCreateRank() {
        return createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    public String getCreateRankDesc() {
        return createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public XMLGregorianCalendar getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(XMLGregorianCalendar updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateHosp() {
        return updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    public String getUpdateRank() {
        return updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    public Long getPrescriptionSortingSeq() {
        return prescriptionSortingSeq;
    }

    public void setPrescriptionSortingSeq(Long prescriptionSortingSeq) {
        this.prescriptionSortingSeq = prescriptionSortingSeq;
    }
}
