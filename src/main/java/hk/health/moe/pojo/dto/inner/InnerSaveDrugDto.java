package hk.health.moe.pojo.dto.inner;

import hk.health.moe.pojo.dto.MoeDrugAliasNameLocalDto;
import hk.health.moe.pojo.dto.MoeDrugLocalDto;
import hk.health.moe.pojo.dto.MoeDrugOrdPropertyLocalDto;
import hk.health.moe.pojo.dto.MoeDrugStrengthLocalDto;
import hk.health.moe.pojo.dto.MoeDrugTherapeuticLocalDto;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class InnerSaveDrugDto implements java.io.Serializable {

    private static final long serialVersionUID = -8043400752559232469L;
    //--- drug info ---
    private String localDrugId;
    private String hkRegNo;
    private boolean localDrugOnly;
    private boolean newDrugRecord;
    private boolean addToSAAM;
    //--- drug detail ---
    private MoeDrugLocalDto moeDrugLocal;
    private List<MoeDrugAliasNameLocalDto> moeDrugAliasNameLocalList;
    private MoeDrugStrengthLocalDto moeDrugStrengthLocal;
    private MoeDrugOrdPropertyLocalDto moeDrugOrdPropertyLocal;
    //--- predefine dosage ---
    private List<InnerSaveCommonDosageDto> predefineDosageItems;
    private List<InnerRelatedDrugDosageLocalDrug> relatedDrugDosageLocalDrugs;
    //--- drug by specialty ---
    private List<InnerSaveDrugBySpecialtyDto> moeDrugBySpecialtyItems;
    //--- local therapeutic class ---
    private List<MoeDrugTherapeuticLocalDto> moeTherapeuticLocals;

/*    public InnerSaveDrugDto() {
        moeDrugLocal = new MoeDrugLocalDto();
        moeDrugAliasNameLocalList = new ArrayList<MoeDrugAliasNameLocalDto>();
        moeDrugStrengthLocal = new MoeDrugStrengthLocalDto();
        moeDrugOrdPropertyLocal = new MoeDrugOrdPropertyLocalDto();
        //predefineDosageActionTypes = new ArrayList<String>();
        predefineDosageItems = new ArrayList<MoeCommonDosageDto>();
        moeDrugBySpecialtys = new ArrayList<MoeDrugBySpecialtyDto>();
        moeTherapeuticLocals = new ArrayList<MoeDrugTherapeuticLocalDto>();
    }*/

    public String getLocalDrugId() {
        return localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        // Ricci 20191009 start --
        this.localDrugId = StringUtils.isNotBlank(localDrugId) ? localDrugId.toUpperCase() : localDrugId;
        // Ricci 20191009 end --
    }

    public String getHkRegNo() {
        return hkRegNo;
    }

    public void setHkRegNo(String hkRegNo) {
        this.hkRegNo = hkRegNo;
    }

    public boolean isLocalDrugOnly() {
        return localDrugOnly;
    }

    public void setLocalDrugOnly(boolean localDrugOnly) {
        this.localDrugOnly = localDrugOnly;
    }

    public boolean isNewDrugRecord() {
        return newDrugRecord;
    }

    public void setNewDrugRecord(boolean newDrugRecord) {
        this.newDrugRecord = newDrugRecord;
    }

    public boolean isAddToSAAM() {
        return addToSAAM;
    }

    public void setAddToSAAM(boolean addToSAAM) {
        this.addToSAAM = addToSAAM;
    }

    public MoeDrugLocalDto getMoeDrugLocal() {
        return moeDrugLocal;
    }

    public void setMoeDrugLocal(MoeDrugLocalDto moeDrugLocal) {
        this.moeDrugLocal = moeDrugLocal;
    }

    public List<MoeDrugAliasNameLocalDto> getMoeDrugAliasNameLocalList() {
        return moeDrugAliasNameLocalList;
    }

    public void setMoeDrugAliasNameLocalList(List<MoeDrugAliasNameLocalDto> moeDrugAliasNameLocalList) {
        this.moeDrugAliasNameLocalList = moeDrugAliasNameLocalList;
    }

    public MoeDrugStrengthLocalDto getMoeDrugStrengthLocal() {
        return moeDrugStrengthLocal;
    }

    public void setMoeDrugStrengthLocal(MoeDrugStrengthLocalDto moeDrugStrengthLocal) {
        this.moeDrugStrengthLocal = moeDrugStrengthLocal;
    }

    public MoeDrugOrdPropertyLocalDto getMoeDrugOrdPropertyLocal() {
        return moeDrugOrdPropertyLocal;
    }

    public void setMoeDrugOrdPropertyLocal(MoeDrugOrdPropertyLocalDto moeDrugOrdPropertyLocal) {
        this.moeDrugOrdPropertyLocal = moeDrugOrdPropertyLocal;
    }

    public List<InnerSaveCommonDosageDto> getPredefineDosageItems() {
        return predefineDosageItems;
    }

    public void setPredefineDosageItems(List<InnerSaveCommonDosageDto> predefineDosageItems) {
        this.predefineDosageItems = predefineDosageItems;
    }

    public List<InnerRelatedDrugDosageLocalDrug> getRelatedDrugDosageLocalDrugs() {
        return relatedDrugDosageLocalDrugs;
    }

    public void setRelatedDrugDosageLocalDrugs(List<InnerRelatedDrugDosageLocalDrug> relatedDrugDosageLocalDrugs) {
        this.relatedDrugDosageLocalDrugs = relatedDrugDosageLocalDrugs;
    }

    public List<InnerSaveDrugBySpecialtyDto> getMoeDrugBySpecialtyItems() {
        return moeDrugBySpecialtyItems;
    }

    public void setMoeDrugBySpecialtyItems(List<InnerSaveDrugBySpecialtyDto> moeDrugBySpecialtyItems) {
        this.moeDrugBySpecialtyItems = moeDrugBySpecialtyItems;
    }

    public List<MoeDrugTherapeuticLocalDto> getMoeTherapeuticLocals() {
        return moeTherapeuticLocals;
    }

    public void setMoeTherapeuticLocals(List<MoeDrugTherapeuticLocalDto> moeTherapeuticLocals) {
        this.moeTherapeuticLocals = moeTherapeuticLocals;
    }

}
