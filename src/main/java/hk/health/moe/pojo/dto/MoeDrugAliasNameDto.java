/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeDrugAliasNameDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;

public class MoeDrugAliasNameDto implements Serializable/*, IsSerializable*/ {

    /**
     *
     */
    private static final long serialVersionUID = 6353115238011203174L;

    private String aliasNameType;
    private String aliasName;
    private String aliasNameDesc;

    public String getAliasNameType() {
        return aliasNameType;
    }

    public void setAliasNameType(String aliasNameType) {
        this.aliasNameType = aliasNameType;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getAliasNameDesc() {
        return aliasNameDesc;
    }

    public void setAliasNameDesc(String aliasNameDesc) {
        this.aliasNameDesc = aliasNameDesc;
    }
}
