/*********************************************************************
* Copyright (c) eHR HK Limited 2013. 
* All Rights Reserved.
* 
*********************************************************************
* PROJECT NAME    :  eHR Adaptation
*
* MODULE NAME     :  Medication Order Entry (MOE)
*
* PROGRAM NAME    :  ResponseBean.java
*
* PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
*
**********************************************************************
* VERSION         :   1.01.001
* REF NO          :  
* DATE CREATED    :   7 June 2013
* 
*********************************************************************
*
* CHANGES         :
* REF NO           DATE        DETAIL
* ------           ----        ------
*
*********************************************************************
*/
package hk.health.moe.pojo.dto.webservice;


import hk.health.moe.pojo.dto.MoeEhrOrderDto;
import net.sf.jasperreports.engine.ReturnValue;

import java.io.Serializable;


/**
 * The Class ResponseBean.
 */
public class ResponseBean implements Serializable/*, IsSerializable*/{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4823800415030609092L;
	private String responseErrorCode = "";
	private String responseMessage = "";
	private int orderNo;
	private MoeEhrOrderDto order;
	private ReturnValue printValue;

	/**
	 * Instantiates a new response bean.
	 */
	public ResponseBean() {
	}
	
	public ResponseBean(String code) {
		this.responseErrorCode = code;
	}

	/**
	 * Gets the response error code of this record.
	 *
	 * @return the response error code
	 * <ul>
	 *        <li>Success code: 00001-I-001</li>
	 *        <li>Fail code: other than 00001-I-001, e.g. 00001-N-003</li>
	 * </ul>
	 */
	public String getResponseErrorCode() {
		return responseErrorCode;
	}

	/**
	 * Gets the response message of this record.
	 *
	 * @return the response message
	 */
	public String getResponseMessage() {
		return responseMessage;
	}

	/**
	 * Sets the response error code of this record.
	 *
	 * @param responseErrorCode the response error code
	 */
	public void setResponseErrorCode(String responseErrorCode) {
		this.responseErrorCode = responseErrorCode;
	}

	/**
	 * Sets the response message of this record.
	 *
	 * @param responseMessage the response message
	 */
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public int getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}

	public MoeEhrOrderDto getOrder() {
		return order;
	}

	public void setOrder(MoeEhrOrderDto order) {
		this.order = order;
	}
	
	public ReturnValue getPrintValue() {
		return printValue;
	}

	public void setPrintValue(ReturnValue printValue) {
		this.printValue = printValue;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder message = new StringBuilder();
		message.append("ResponseBean [");
		message.append("ResponseCode=" + getResponseErrorCode());
		message.append("]");
		return message.toString();
	}
}
