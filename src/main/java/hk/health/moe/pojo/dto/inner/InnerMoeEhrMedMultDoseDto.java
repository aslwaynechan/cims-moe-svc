package hk.health.moe.pojo.dto.inner;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/8/1
 */
public class InnerMoeEhrMedMultDoseDto {
    private Long supplFreqId;

    public Long getSupplFreqId() {
        return supplFreqId;
    }

    public void setSupplFreqId(Long supplFreqId) {
        this.supplFreqId = supplFreqId;
    }
}
