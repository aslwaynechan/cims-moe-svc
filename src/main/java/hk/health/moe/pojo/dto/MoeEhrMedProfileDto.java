/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeEhrMedProfileDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MoeEhrMedProfileDto implements /*IsSerializable,*/ Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -9177115124174331608L;

    private Long drugId;
    private String vtm;
    private Long vtmId;
    private String tradeName;
    private Long tradeNameVtmId;
    private String aliasName;
    private String aliasNameType;
    private String manufacturer;
    private String screenDisplay;
    private String orderLineType;
    private String genericIndicator;
    private String strengthCompulsory;
    private Long cycleMultiplier;
    private Long formId;
    private Long doseFormExtraInfoId;
    private String doseFormExtraInfo;
    private Long routeId;
    private Long freqId;
    private Long supplFreqId;
    private Long moQtyUnitId;
    private Long moduId;
    private Long siteId;
    private String drugRouteEng;
    private Long drugRouteId;
    private String strengthLevelExtraInfo;
    private String createUserId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private Date createDtm;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private Date updateDtm;
    private String medDiscontFlag;
    private String medDiscontReasonCode;
    private String medDiscontInfo;
    private String medDiscontUserId;
    private String medDiscontUser;
    private String medDiscontHosp;
    private Date medDiscontDtm;
    private Long version;
    private boolean currentExist = true;
    private boolean isNotSuspend = true;

    private boolean checkAllergy = true;
    private Long currentMoQtyUnitId;
    private Date hkmttUpdateDate;

    // Ingredient list, not in DB
    private List<String> ingredientList = null;

    public String getVtm() {
        return vtm;
    }

    public void setVtm(String vtm) {
        this.vtm = vtm;
        if (vtm != null && vtm.indexOf("+") > -1) {
            String[] ingredients = vtm.split("\\+");
            ingredientList = new ArrayList<String>();
            for (int i = 0; i < ingredients.length; i++) {
                ingredientList.add(ingredients[i].trim());
            }
        }
    }

    public Long getVtmId() {
        return vtmId;
    }

    public void setVtmId(Long vtmId) {
        this.vtmId = vtmId;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public Long getTradeNameVtmId() {
        return tradeNameVtmId;
    }

    public void setTradeNameVtmId(Long tradeNameVtmId) {
        this.tradeNameVtmId = tradeNameVtmId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getAliasNameType() {
        return aliasNameType;
    }

    public void setAliasNameType(String aliasNameType) {
        this.aliasNameType = aliasNameType;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getScreenDisplay() {
        return screenDisplay;
    }

    public void setScreenDisplay(String screenDisplay) {
        this.screenDisplay = screenDisplay;
    }

    public void setOrderLineType(String orderLineType) {
        this.orderLineType = orderLineType;
    }

    public String getOrderLineType() {
        return orderLineType;
    }

    public String getGenericIndicator() {
        return genericIndicator;
    }

    public void setGenericIndicator(String genericIndicator) {
        this.genericIndicator = genericIndicator;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateHosp() {
        return createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    public String getCreateRank() {
        return createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    public String getCreateRankDesc() {
        return createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    public Date getCreateDtm() {
        return createDtm;
    }

    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateHosp() {
        return updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    public String getUpdateRank() {
        return updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }

    public Date getUpdateDtm() {
        return updateDtm;
    }

    public void setCycleMultiplier(Long cycleMultiplier) {
        this.cycleMultiplier = cycleMultiplier;
    }

    public Long getCycleMultiplier() {
        return cycleMultiplier;
    }

    public Long getDrugId() {
        return drugId;
    }

    public void setDrugId(Long drugId) {
        this.drugId = drugId;
    }

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public void setDoseFormExtraInfoId(Long doseFormExtraInfoId) {
        this.doseFormExtraInfoId = doseFormExtraInfoId;
    }

    public Long getDoseFormExtraInfoId() {
        return doseFormExtraInfoId;
    }

    public void setDoseFormExtraInfo(String doseFormExtraInfo) {
        this.doseFormExtraInfo = doseFormExtraInfo;
    }

    public String getDoseFormExtraInfo() {
        return doseFormExtraInfo;
    }

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public Long getFreqId() {
        return freqId;
    }

    public void setFreqId(Long freqId) {
        this.freqId = freqId;
    }

    public Long getSupplFreqId() {
        return supplFreqId;
    }

    public void setSupplFreqId(Long supplFreqId) {
        this.supplFreqId = supplFreqId;
    }

    public Long getMoQtyUnitId() {
        return moQtyUnitId;
    }

    public void setMoQtyUnitId(Long moQtyUnitId) {
        this.moQtyUnitId = moQtyUnitId;
    }

    public Long getModuId() {
        return moduId;
    }

    public void setModuId(Long moduId) {
        this.moduId = moduId;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getVersion() {
        return version;
    }

    public List<String> getIngredientList() {
        return ingredientList;
    }

    public void setIngredientList(List<String> ingredientList) {
        this.ingredientList = ingredientList;
    }

    public String getStrengthCompulsory() {
        return strengthCompulsory;
    }

    public void setStrengthCompulsory(String strengthCompulsory) {
        this.strengthCompulsory = strengthCompulsory;
    }

    public String getDrugRouteEng() {
        return drugRouteEng;
    }

    public void setDrugRouteEng(String drugRouteEng) {
        this.drugRouteEng = drugRouteEng;
    }

    public Long getDrugRouteId() {
        return drugRouteId;
    }

    public void setDrugRouteId(Long drugRouteId) {
        this.drugRouteId = drugRouteId;
    }

    public void setStrengthLevelExtraInfo(String strengthLevelExtraInfo) {
        this.strengthLevelExtraInfo = strengthLevelExtraInfo;
    }

    public String getStrengthLevelExtraInfo() {
        return strengthLevelExtraInfo;
    }

    public boolean isCurrentExist() {
        return currentExist;
    }

    public void setCurrentExist(boolean currentExist) {
        this.currentExist = currentExist;
    }

    public boolean isNotSuspend() {
        return isNotSuspend;
    }

    public void setNotSuspend(boolean isNotSuspend) {
        this.isNotSuspend = isNotSuspend;
    }

    public void setCheckAllergy(boolean checkAllergy) {
        this.checkAllergy = checkAllergy;
    }

    public boolean isCheckAllergy() {
        return checkAllergy;
    }

    public Long getCurrentMoQtyUnitId() {
        return currentMoQtyUnitId;
    }

    public void setCurrentMoQtyUnitId(Long currentMoQtyUnitId) {
        this.currentMoQtyUnitId = currentMoQtyUnitId;
    }

    public Date getHkmttUpdateDate() {
        return hkmttUpdateDate;
    }

    public void setHkmttUpdateDate(Date hkmttUpdateDate) {
        this.hkmttUpdateDate = hkmttUpdateDate;
    }

    public String getMedDiscontFlag() {
        return medDiscontFlag;
    }

    public void setMedDiscontFlag(String medDiscontFlag) {
        this.medDiscontFlag = medDiscontFlag;
    }

    public String getMedDiscontReasonCode() {
        return medDiscontReasonCode;
    }

    public void setMedDiscontReasonCode(String medDiscontReasonCode) {
        this.medDiscontReasonCode = medDiscontReasonCode;
    }

    public String getMedDiscontInfo() {
        return medDiscontInfo;
    }

    public void setMedDiscontInfo(String medDiscontInfo) {
        this.medDiscontInfo = medDiscontInfo;
    }

    public String getMedDiscontUserId() {
        return medDiscontUserId;
    }

    public void setMedDiscontUserId(String medDiscontUserId) {
        this.medDiscontUserId = medDiscontUserId;
    }

    public String getMedDiscontUser() {
        return medDiscontUser;
    }

    public void setMedDiscontUser(String medDiscontUser) {
        this.medDiscontUser = medDiscontUser;
    }

    public String getMedDiscontHosp() {
        return medDiscontHosp;
    }

    public void setMedDiscontHosp(String medDiscontHosp) {
        this.medDiscontHosp = medDiscontHosp;
    }

    public Date getMedDiscontDtm() {
        return medDiscontDtm;
    }

    public void setMedDiscontDtm(Date medDiscontDtm) {
        this.medDiscontDtm = medDiscontDtm;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MoeEhrMedProfileDTO [aliasName=");
        builder.append(aliasName);
        builder.append(", aliasNameType=");
        builder.append(aliasNameType);
        builder.append(", checkAllergy=");
        builder.append(checkAllergy);
        builder.append(", createDtm=");
        builder.append(createDtm);
        builder.append(", createHosp=");
        builder.append(createHosp);
        builder.append(", createRank=");
        builder.append(createRank);
        builder.append(", createRankDesc=");
        builder.append(createRankDesc);
        builder.append(", createUser=");
        builder.append(createUser);
        builder.append(", createUserId=");
        builder.append(createUserId);
        builder.append(", currentExist=");
        builder.append(currentExist);
        builder.append(", cycleMultiplier=");
        builder.append(cycleMultiplier);
        builder.append(", doseFormExtraInfo=");
        builder.append(doseFormExtraInfo);
        builder.append(", doseFormExtraInfoId=");
        builder.append(doseFormExtraInfoId);
        builder.append(", drugId=");
        builder.append(drugId);
        builder.append(", drugRouteEng=");
        builder.append(drugRouteEng);
        builder.append(", drugRouteId=");
        builder.append(drugRouteId);
        builder.append(", formId=");
        builder.append(formId);
        builder.append(", freqId=");
        builder.append(freqId);
        builder.append(", genericIndicator=");
        builder.append(genericIndicator);
        builder.append(", ingredientList=");
        builder.append(ingredientList);
        builder.append(", manufacturer=");
        builder.append(manufacturer);
        builder.append(", moQtyUnitId=");
        builder.append(moQtyUnitId);
        builder.append(", moduId=");
        builder.append(moduId);
        builder.append(", orderLineType=");
        builder.append(orderLineType);
        builder.append(", routeId=");
        builder.append(routeId);
        builder.append(", screenDisplay=");
        builder.append(screenDisplay);
        builder.append(", siteId=");
        builder.append(siteId);
        builder.append(", strengthCompulsory=");
        builder.append(strengthCompulsory);
        builder.append(", strengthLevelExtraInfo=");
        builder.append(strengthLevelExtraInfo);
        builder.append(", supplFreqId=");
        builder.append(supplFreqId);
        builder.append(", tradeName=");
        builder.append(tradeName);
        builder.append(", tradeNameVtmId=");
        builder.append(tradeNameVtmId);
        builder.append(", updateDtm=");
        builder.append(updateDtm);
        builder.append(", updateHosp=");
        builder.append(updateHosp);
        builder.append(", updateRank=");
        builder.append(updateRank);
        builder.append(", updateRankDesc=");
        builder.append(updateRankDesc);
        builder.append(", updateUser=");
        builder.append(updateUser);
        builder.append(", updateUserId=");
        builder.append(updateUserId);
        builder.append(", medDiscontFlag=");
        builder.append(medDiscontFlag);
        builder.append(", medDiscontReasonCode=");
        builder.append(medDiscontReasonCode);
        builder.append(", medDiscontInfo=");
        builder.append(medDiscontInfo);
        builder.append(", medDiscontUserId=");
        builder.append(medDiscontUserId);
        builder.append(", medDiscontUser=");
        builder.append(medDiscontUser);
        builder.append(", medDiscontHosp=");
        builder.append(medDiscontHosp);
        builder.append(", medDiscontDtm=");
        builder.append(medDiscontDtm);
        builder.append(", version=");
        builder.append(version);
        builder.append(", vtm=");
        builder.append(vtm);
        builder.append(", vtmId=");
        builder.append(vtmId);
        builder.append(", currentMoQtyUnitId=");
        builder.append(currentMoQtyUnitId);
        builder.append("]");
        return builder.toString();
    }
}
