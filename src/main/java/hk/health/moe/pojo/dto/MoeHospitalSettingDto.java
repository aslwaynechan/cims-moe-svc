/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeHospitalSettingDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MoeHospitalSettingDto implements Serializable/*, IsSerializable*/ {

    /**
     *
     */
    private static final long serialVersionUID = 3099216011058448321L;

    private String paramName;
    private String paramNameDesc;
    private String paramValue;
    private String paramValueType;
    private String paramPossibleValue;
    private String paramLevel;
    private String paramDisplay;
    // Ricci 20190819 start --
    @NotBlank
    private String hospcode;
    @NotBlank
    private String userSpecialty;
    // Ricci 20190819 end --

    // Ricci 20190819 start --
    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    public String getUserSpecialty() {
        return userSpecialty;
    }

    public void setUserSpecialty(String userSpecialty) {
        this.userSpecialty = userSpecialty;
    }
    // Ricci 20190819 end --

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamNameDesc() {
        return paramNameDesc;
    }

    public void setParamNameDesc(String paramNameDesc) {
        this.paramNameDesc = paramNameDesc;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public String getParamValueType() {
        return paramValueType;
    }

    public void setParamValueType(String paramValueType) {
        this.paramValueType = paramValueType;
    }

    public String getParamPossibleValue() {
        return paramPossibleValue;
    }

    public void setParamPossibleValue(String paramPossibleValue) {
        this.paramPossibleValue = paramPossibleValue;
    }

    public String getParamLevel() {
        return paramLevel;
    }

    public void setParamLevel(String paramLevel) {
        this.paramLevel = paramLevel;
    }

    public String getParamDisplay() {
        return paramDisplay;
    }

    public void setParamDisplay(String paramDisplay) {
        this.paramDisplay = paramDisplay;
    }

}
