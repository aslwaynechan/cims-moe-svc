package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;
import hk.gov.ehr.model.moe.webservice.dto.ManifestBean;*/

import hk.health.moe.restservice.bean.saam.MoeAllergyReactionDto;

import java.io.Serializable;
import java.util.List;

public class InnerMoePatientAllergyDto implements /*IsSerializable,*/ Serializable {

    private static final long serialVersionUID = 6455544196991210432L;
    private String allergen;
    private String allergenTermID;
    private String allergySeqNo;
    private String allergenType;
    private String certainty;
    private String displayName;
    private String additionalInfo;
    private String reactionLevel;
    private List<MoeAllergyReactionDto> allergyReactions;

    public List<MoeAllergyReactionDto> getAllergyReactions() {
        return allergyReactions;
    }

    public void setAllergyReactions(List<MoeAllergyReactionDto> allergyReactions) {
        this.allergyReactions = allergyReactions;
    }


    public String getReactionLevel() {
        return reactionLevel;
    }

    public void setReactionLevel(String reactionLevel) {
        this.reactionLevel = reactionLevel;
    }


    public String getAllergen() {
        return allergen;
    }

    public void setAllergen(String allergen) {
        this.allergen = allergen;
    }

    public String getAllergenTermID() {
        return allergenTermID;
    }

    public void setAllergenTermID(String allergenTermID) {
        this.allergenTermID = allergenTermID;
    }

    public String getAllergySeqNo() {
        return allergySeqNo;
    }

    public void setAllergySeqNo(String allergySeqNo) {
        this.allergySeqNo = allergySeqNo;
    }

    public String getAllergenType() {
        return allergenType;
    }

    public void setAllergenType(String allergenType) {
        this.allergenType = allergenType;
    }

    public String getCertainty() {
        return certainty;
    }

    public void setCertainty(String certainty) {
        this.certainty = certainty;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

}
