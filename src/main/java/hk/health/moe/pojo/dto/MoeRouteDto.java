/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  MoeRouteDto.java
 *
 * PURPOSE         :  Provides the classes necessary to create a data transfer object(DTO) for data transfer
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.pojo.dto;

/*import com.google.gwt.user.client.rpc.IsSerializable;*/

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class MoeRouteDto implements Serializable/*, IsSerializable*/ {

    private static final long serialVersionUID = -3481264041949172635L;

    private Long routeId;
    private String routeEng;
    private String routeChi;
    private String routeOtherName;
    private BigDecimal routeMultiplier;
    private String enableInRegimen;
    private List<MoeSiteDto> sites = new LinkedList<>();

    public Long getRouteId() {
        return this.routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public String getRouteEng() {
        return routeEng;
    }

    public void setRouteEng(String routeEng) {
        this.routeEng = routeEng;
    }

    public String getRouteChi() {
        return routeChi;
    }

    public void setRouteChi(String routeChi) {
        this.routeChi = routeChi;
    }

    public BigDecimal getRouteMultiplier() {
        return routeMultiplier;
    }

    public void setRouteMultiplier(BigDecimal routeMultiplier) {
        this.routeMultiplier = routeMultiplier;
    }

    public void setRouteOtherName(String routeOtherName) {
        this.routeOtherName = routeOtherName;
    }

    public String getRouteOtherName() {
        return routeOtherName;
    }

    public String getEnableInRegimen() {
        return enableInRegimen;
    }

    public void setEnableInRegimen(String enableInRegimen) {
        this.enableInRegimen = enableInRegimen;
    }

    public List<MoeSiteDto> getSites() {
        return sites;
    }

    public void setSites(List<MoeSiteDto> sites) {
        this.sites = sites;
    }
}
