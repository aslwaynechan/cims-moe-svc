package hk.health.moe.pojo.po;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

@Entity
@Table(name = "MOE_EHR_RX_IMAGE_LOG")
public class MoeEhrRxImageLogPo implements Serializable {
    private static final long serialVersionUID = 3048457297916366745L;
    private String reportId;
    private String hospcode;
    private long ordNo;
    private String directPrint;
    private String reportType;
    private byte[] rxImage;
    private String rxXml;
    private String refNo;
    private Date createDtm;
    private Date lastPrintDtm;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "REPORT_ID")
    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    @Basic
    @Column(name = "HOSPCODE")
    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    @Basic
    @Column(name = "ORD_NO")
    public long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(long ordNo) {
        this.ordNo = ordNo;
    }

    @Basic
    @Column(name = "DIRECT_PRINT")
    public String getDirectPrint() {
        return directPrint;
    }

    public void setDirectPrint(String directPrint) {
        this.directPrint = directPrint;
    }

    @Basic
    @Column(name = "REPORT_TYPE")
    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    @Basic
    @Column(name = "RX_IMAGE")
    public byte[] getRxImage() {
        return rxImage;
    }

    public void setRxImage(byte[] rxImage) {
        this.rxImage = rxImage;
    }

    @Basic
    @Column(name = "RX_XML")
    public String getRxXml() {
        return rxXml;
    }

    public void setRxXml(String rxXml) {
        this.rxXml = rxXml;
    }

    @Basic
    @Column(name = "REF_NO")
    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    @Basic
    @Column(name = "CREATE_DTM")
    public Date getCreateDtm() {
        return createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    @Basic
    @Column(name = "LAST_PRINT_DTM")
    public Date getLastPrintDtm() {
        return lastPrintDtm;
    }

    public void setLastPrintDtm(Date lastPrintDtm) {
        this.lastPrintDtm = lastPrintDtm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeEhrRxImageLogPo that = (MoeEhrRxImageLogPo) o;

        if (ordNo != that.ordNo) return false;
        if (reportId != null ? !reportId.equals(that.reportId) : that.reportId != null) return false;
        if (hospcode != null ? !hospcode.equals(that.hospcode) : that.hospcode != null) return false;
        if (directPrint != null ? !directPrint.equals(that.directPrint) : that.directPrint != null) return false;
        if (reportType != null ? !reportType.equals(that.reportType) : that.reportType != null) return false;
        if (!Arrays.equals(rxImage, that.rxImage)) return false;
        if (rxXml != null ? !rxXml.equals(that.rxXml) : that.rxXml != null) return false;
        if (refNo != null ? !refNo.equals(that.refNo) : that.refNo != null) return false;
        if (createDtm != null ? !createDtm.equals(that.createDtm) : that.createDtm != null) return false;
        if (lastPrintDtm != null ? !lastPrintDtm.equals(that.lastPrintDtm) : that.lastPrintDtm != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = reportId != null ? reportId.hashCode() : 0;
        result = 31 * result + (hospcode != null ? hospcode.hashCode() : 0);
        result = 31 * result + (int) (ordNo ^ (ordNo >>> 32));
        result = 31 * result + (directPrint != null ? directPrint.hashCode() : 0);
        result = 31 * result + (reportType != null ? reportType.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(rxImage);
        result = 31 * result + (rxXml != null ? rxXml.hashCode() : 0);
        result = 31 * result + (refNo != null ? refNo.hashCode() : 0);
        result = 31 * result + (createDtm != null ? createDtm.hashCode() : 0);
        result = 31 * result + (lastPrintDtm != null ? lastPrintDtm.hashCode() : 0);
        return result;
    }
}
