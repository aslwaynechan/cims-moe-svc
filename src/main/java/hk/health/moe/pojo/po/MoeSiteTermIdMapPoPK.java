package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeSiteTermIdMapPoPK implements Serializable {
    private static final long serialVersionUID = -1668753862062442342L;
    private String siteEng;
    private long ehrTermId;

    @Column(name = "SITE_ENG")
    @Id
    public String getSiteEng() {
        return siteEng;
    }

    public void setSiteEng(String siteEng) {
        this.siteEng = siteEng;
    }

    @Column(name = "EHR_TERM_ID")
    @Id
    public long getEhrTermId() {
        return ehrTermId;
    }

    public void setEhrTermId(long ehrTermId) {
        this.ehrTermId = ehrTermId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeSiteTermIdMapPoPK that = (MoeSiteTermIdMapPoPK) o;

        if (ehrTermId != that.ehrTermId) return false;
        if (siteEng != null ? !siteEng.equals(that.siteEng) : that.siteEng != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = siteEng != null ? siteEng.hashCode() : 0;
        result = 31 * result + (int) (ehrTermId ^ (ehrTermId >>> 32));
        return result;
    }
}
