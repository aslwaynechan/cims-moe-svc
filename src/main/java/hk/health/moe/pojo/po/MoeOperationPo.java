package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MOE_OPERATION")
public class MoeOperationPo implements Serializable {
    private static final long serialVersionUID = -994261411448151962L;
    private String operationId;
    private String operationName;
    private Set<MoeAppPo> moeApps = new HashSet<>();
    private Set<MoeMethodPo> moeMethods = new HashSet<>();

    @Id
    @Column(name = "OPERATION_ID")
    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    @Basic
    @Column(name = "OPERATION_NAME")
    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "MOE_APP_OPERATION",
            joinColumns = { @JoinColumn(name = "OPERATION_ID", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "APP_ID", nullable = false, updatable = false) })
    public Set<MoeAppPo> getMoeApps() {
        return this.moeApps;
    }

    public void setMoeApps(Set<MoeAppPo> moeApps) {
        this.moeApps = moeApps;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "MOE_OPERATION_METHOD",
            joinColumns = { @JoinColumn(name = "OPERATION_ID", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "METHOD_ID", nullable = false, updatable = false) })
    public Set<MoeMethodPo> getMoeMethods() {
        return this.moeMethods;
    }

    public void setMoeMethods(Set<MoeMethodPo> moeMethods) {
        this.moeMethods = moeMethods;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeOperationPo that = (MoeOperationPo) o;

        if (operationId != null ? !operationId.equals(that.operationId) : that.operationId != null) return false;
        if (operationName != null ? !operationName.equals(that.operationName) : that.operationName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = operationId != null ? operationId.hashCode() : 0;
        result = 31 * result + (operationName != null ? operationName.hashCode() : 0);
        return result;
    }
}
