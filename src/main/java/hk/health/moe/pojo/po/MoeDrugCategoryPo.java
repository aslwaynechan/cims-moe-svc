package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Arrays;

@Entity
@Table(name = "MOE_DRUG_CATEGORY")
public class MoeDrugCategoryPo implements Serializable {
    private static final long serialVersionUID = 3222656659523896427L;
    private String drugCategoryId;
    private String drugCategoryDesc;
    private byte[] image;
    private long rank;

    @Id
    @Column(name = "DRUG_CATEGORY_ID")
    public String getDrugCategoryId() {
        return drugCategoryId;
    }

    public void setDrugCategoryId(String drugCategoryId) {
        this.drugCategoryId = drugCategoryId;
    }

    @Basic
    @Column(name = "DRUG_CATEGORY_DESC")
    public String getDrugCategoryDesc() {
        return drugCategoryDesc;
    }

    public void setDrugCategoryDesc(String drugCategoryDesc) {
        this.drugCategoryDesc = drugCategoryDesc;
    }

    @Basic
    @Column(name = "IMAGE")
    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Basic
    @Column(name = "RANK")
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDrugCategoryPo that = (MoeDrugCategoryPo) o;

        if (rank != that.rank) return false;
        if (drugCategoryId != null ? !drugCategoryId.equals(that.drugCategoryId) : that.drugCategoryId != null)
            return false;
        if (drugCategoryDesc != null ? !drugCategoryDesc.equals(that.drugCategoryDesc) : that.drugCategoryDesc != null)
            return false;
        if (!Arrays.equals(image, that.image)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = drugCategoryId != null ? drugCategoryId.hashCode() : 0;
        result = 31 * result + (drugCategoryDesc != null ? drugCategoryDesc.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(image);
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        return result;
    }
}
