package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

public class MoeEhrAcceptDrugCheckingPoPK implements Serializable {
    private static final long serialVersionUID = -6112175967001096070L;
    private String acceptId;
    private String buttonLabel;
    private Date recordDtm;

    @Column(name = "ACCEPT_ID")
    @Id
    public String getAcceptId() {
        return acceptId;
    }

    public void setAcceptId(String acceptId) {
        this.acceptId = acceptId;
    }

    @Column(name = "BUTTON_LABEL")
    @Id
    public String getButtonLabel() {
        return buttonLabel;
    }

    public void setButtonLabel(String buttonLabel) {
        this.buttonLabel = buttonLabel;
    }

    @Column(name = "RECORD_DTM")
    @Id
    public Date getRecordDtm() {
        return recordDtm;
    }

    public void setRecordDtm(Date recordDtm) {
        this.recordDtm = recordDtm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeEhrAcceptDrugCheckingPoPK that = (MoeEhrAcceptDrugCheckingPoPK) o;

        if (acceptId != null ? !acceptId.equals(that.acceptId) : that.acceptId != null) return false;
        if (buttonLabel != null ? !buttonLabel.equals(that.buttonLabel) : that.buttonLabel != null) return false;
        if (recordDtm != null ? !recordDtm.equals(that.recordDtm) : that.recordDtm != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = acceptId != null ? acceptId.hashCode() : 0;
        result = 31 * result + (buttonLabel != null ? buttonLabel.hashCode() : 0);
        result = 31 * result + (recordDtm != null ? recordDtm.hashCode() : 0);
        return result;
    }
}
