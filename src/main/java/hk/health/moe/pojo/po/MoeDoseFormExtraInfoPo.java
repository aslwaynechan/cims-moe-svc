package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MOE_DOSE_FORM_EXTRA_INFO")
public class MoeDoseFormExtraInfoPo  implements Serializable {
    private static final long serialVersionUID = 1626624010270909854L;
    private long doseFormExtraInfoId;
    private String doseFormExtraInfo;
    private Long doseFormExtraInfoTermId;
    private long rank;
    private Set<MoeDrugPo> moeDrugs = new HashSet<>();

    @Id
    @Column(name = "DOSE_FORM_EXTRA_INFO_ID")
    public long getDoseFormExtraInfoId() {
        return doseFormExtraInfoId;
    }

    public void setDoseFormExtraInfoId(long doseFormExtraInfoId) {
        this.doseFormExtraInfoId = doseFormExtraInfoId;
    }

    @Basic
    @Column(name = "DOSE_FORM_EXTRA_INFO")
    public String getDoseFormExtraInfo() {
        return doseFormExtraInfo;
    }

    public void setDoseFormExtraInfo(String doseFormExtraInfo) {
        this.doseFormExtraInfo = doseFormExtraInfo;
    }

    @Basic
    @Column(name = "DOSE_FORM_EXTRA_INFO_TERM_ID")
    public Long getDoseFormExtraInfoTermId() {
        return doseFormExtraInfoTermId;
    }

    public void setDoseFormExtraInfoTermId(Long doseFormExtraInfoTermId) {
        this.doseFormExtraInfoTermId = doseFormExtraInfoTermId;
    }

    @Basic
    @Column(name = "RANK")
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moeDoseFormExtraInfo")
    public Set<MoeDrugPo> getMoeDrugs() {
        return this.moeDrugs;
    }

    public void setMoeDrugs(Set<MoeDrugPo> moeDrugs) {
        this.moeDrugs = moeDrugs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDoseFormExtraInfoPo that = (MoeDoseFormExtraInfoPo) o;

        if (doseFormExtraInfoId != that.doseFormExtraInfoId) return false;
        if (rank != that.rank) return false;
        if (doseFormExtraInfo != null ? !doseFormExtraInfo.equals(that.doseFormExtraInfo) : that.doseFormExtraInfo != null)
            return false;
        if (doseFormExtraInfoTermId != null ? !doseFormExtraInfoTermId.equals(that.doseFormExtraInfoTermId) : that.doseFormExtraInfoTermId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (doseFormExtraInfoId ^ (doseFormExtraInfoId >>> 32));
        result = 31 * result + (doseFormExtraInfo != null ? doseFormExtraInfo.hashCode() : 0);
        result = 31 * result + (doseFormExtraInfoTermId != null ? doseFormExtraInfoTermId.hashCode() : 0);
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        return result;
    }
}
