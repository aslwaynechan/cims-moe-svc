package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeSupplFreqTermIdMapPoPK implements Serializable {
    private static final long serialVersionUID = 6101049009214303994L;
    private String supplFreqCode;
    private String supplFreqDescEng;
    private long ehrTermId;

    @Column(name = "SUPPL_FREQ_CODE")
    @Id
    public String getSupplFreqCode() {
        return supplFreqCode;
    }

    public void setSupplFreqCode(String supplFreqCode) {
        this.supplFreqCode = supplFreqCode;
    }

    @Column(name = "SUPPL_FREQ_DESC_ENG")
    @Id
    public String getSupplFreqDescEng() {
        return supplFreqDescEng;
    }

    public void setSupplFreqDescEng(String supplFreqDescEng) {
        this.supplFreqDescEng = supplFreqDescEng;
    }

    @Column(name = "EHR_TERM_ID")
    @Id
    public long getEhrTermId() {
        return ehrTermId;
    }

    public void setEhrTermId(long ehrTermId) {
        this.ehrTermId = ehrTermId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeSupplFreqTermIdMapPoPK that = (MoeSupplFreqTermIdMapPoPK) o;

        if (ehrTermId != that.ehrTermId) return false;
        if (supplFreqCode != null ? !supplFreqCode.equals(that.supplFreqCode) : that.supplFreqCode != null)
            return false;
        if (supplFreqDescEng != null ? !supplFreqDescEng.equals(that.supplFreqDescEng) : that.supplFreqDescEng != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = supplFreqCode != null ? supplFreqCode.hashCode() : 0;
        result = 31 * result + (supplFreqDescEng != null ? supplFreqDescEng.hashCode() : 0);
        result = 31 * result + (int) (ehrTermId ^ (ehrTermId >>> 32));
        return result;
    }
}
