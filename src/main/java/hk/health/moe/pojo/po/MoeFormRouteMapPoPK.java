package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeFormRouteMapPoPK implements Serializable {
    private static final long serialVersionUID = -1081498073650680454L;
    private long formId;
    private long routeId;

    @Column(name = "FORM_ID")
    @Id
    public long getFormId() {
        return formId;
    }

    public void setFormId(long formId) {
        this.formId = formId;
    }

    @Column(name = "ROUTE_ID")
    @Id
    public long getRouteId() {
        return routeId;
    }

    public void setRouteId(long routeId) {
        this.routeId = routeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeFormRouteMapPoPK that = (MoeFormRouteMapPoPK) o;

        if (formId != that.formId) return false;
        if (routeId != that.routeId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (formId ^ (formId >>> 32));
        result = 31 * result + (int) (routeId ^ (routeId >>> 32));
        return result;
    }
}
