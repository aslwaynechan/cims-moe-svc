/*
package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "MOE_MSG")
@IdClass(MoeMsgPoPK.class)
public class MoeMsgPo {
    private String functionId;
    private String severityCode;
    private String messageCode;
    private long messageLevel;
    private String engMessageHeader;
    private String engDescription;
    private String engCause;
    private String engAction;
    private String engNoOfButton;
    private String chiMessageHeader;
    private String chiDescription;
    private String chiCause;
    private String chiAction;
    private String chiNoOfButton;
    private String defaultButton;
    private Date effectiveDtm;
    private Date expiryDtm;
    private Date createDtm;
    private String createBy;
    private Date updateDtm;
    private String updateBy;

    @Id
    @Column(name = "FUNCTION_ID")
    public String getFunctionId() {
        return functionId;
    }

    public void setFunctionId(String functionId) {
        this.functionId = functionId;
    }

    @Id
    @Column(name = "SEVERITY_CODE")
    public String getSeverityCode() {
        return severityCode;
    }

    public void setSeverityCode(String severityCode) {
        this.severityCode = severityCode;
    }

    @Id
    @Column(name = "MESSAGE_CODE")
    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    @Basic
    @Column(name = "MESSAGE_LEVEL")
    public long getMessageLevel() {
        return messageLevel;
    }

    public void setMessageLevel(long messageLevel) {
        this.messageLevel = messageLevel;
    }

    @Basic
    @Column(name = "ENG_MESSAGE_HEADER")
    public String getEngMessageHeader() {
        return engMessageHeader;
    }

    public void setEngMessageHeader(String engMessageHeader) {
        this.engMessageHeader = engMessageHeader;
    }

    @Basic
    @Column(name = "ENG_DESCRIPTION")
    public String getEngDescription() {
        return engDescription;
    }

    public void setEngDescription(String engDescription) {
        this.engDescription = engDescription;
    }

    @Basic
    @Column(name = "ENG_CAUSE")
    public String getEngCause() {
        return engCause;
    }

    public void setEngCause(String engCause) {
        this.engCause = engCause;
    }

    @Basic
    @Column(name = "ENG_ACTION")
    public String getEngAction() {
        return engAction;
    }

    public void setEngAction(String engAction) {
        this.engAction = engAction;
    }

    @Basic
    @Column(name = "ENG_NO_OF_BUTTON")
    public String getEngNoOfButton() {
        return engNoOfButton;
    }

    public void setEngNoOfButton(String engNoOfButton) {
        this.engNoOfButton = engNoOfButton;
    }

    @Basic
    @Column(name = "CHI_MESSAGE_HEADER")
    public String getChiMessageHeader() {
        return chiMessageHeader;
    }

    public void setChiMessageHeader(String chiMessageHeader) {
        this.chiMessageHeader = chiMessageHeader;
    }

    @Basic
    @Column(name = "CHI_DESCRIPTION")
    public String getChiDescription() {
        return chiDescription;
    }

    public void setChiDescription(String chiDescription) {
        this.chiDescription = chiDescription;
    }

    @Basic
    @Column(name = "CHI_CAUSE")
    public String getChiCause() {
        return chiCause;
    }

    public void setChiCause(String chiCause) {
        this.chiCause = chiCause;
    }

    @Basic
    @Column(name = "CHI_ACTION")
    public String getChiAction() {
        return chiAction;
    }

    public void setChiAction(String chiAction) {
        this.chiAction = chiAction;
    }

    @Basic
    @Column(name = "CHI_NO_OF_BUTTON")
    public String getChiNoOfButton() {
        return chiNoOfButton;
    }

    public void setChiNoOfButton(String chiNoOfButton) {
        this.chiNoOfButton = chiNoOfButton;
    }

    @Basic
    @Column(name = "DEFAULT_BUTTON")
    public String getDefaultButton() {
        return defaultButton;
    }

    public void setDefaultButton(String defaultButton) {
        this.defaultButton = defaultButton;
    }

    @Basic
    @Column(name = "EFFECTIVE_DTM")
    public Date getEffectiveDtm() {
        return effectiveDtm;
    }

    public void setEffectiveDtm(Date effectiveDtm) {
        this.effectiveDtm = effectiveDtm;
    }

    @Basic
    @Column(name = "EXPIRY_DTM")
    public Date getExpiryDtm() {
        return expiryDtm;
    }

    public void setExpiryDtm(Date expiryDtm) {
        this.expiryDtm = expiryDtm;
    }

    @Basic
    @Column(name = "CREATE_DTM")
    public Date getCreateDtm() {
        return createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    @Basic
    @Column(name = "CREATE_BY")
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    @Basic
    @Column(name = "UPDATE_DTM")
    public Date getUpdateDtm() {
        return updateDtm;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }

    @Basic
    @Column(name = "UPDATE_BY")
    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeMsgPo moeMsgPo = (MoeMsgPo) o;

        if (messageLevel != moeMsgPo.messageLevel) return false;
        if (functionId != null ? !functionId.equals(moeMsgPo.functionId) : moeMsgPo.functionId != null) return false;
        if (severityCode != null ? !severityCode.equals(moeMsgPo.severityCode) : moeMsgPo.severityCode != null)
            return false;
        if (messageCode != null ? !messageCode.equals(moeMsgPo.messageCode) : moeMsgPo.messageCode != null)
            return false;
        if (engMessageHeader != null ? !engMessageHeader.equals(moeMsgPo.engMessageHeader) : moeMsgPo.engMessageHeader != null)
            return false;
        if (engDescription != null ? !engDescription.equals(moeMsgPo.engDescription) : moeMsgPo.engDescription != null)
            return false;
        if (engCause != null ? !engCause.equals(moeMsgPo.engCause) : moeMsgPo.engCause != null) return false;
        if (engAction != null ? !engAction.equals(moeMsgPo.engAction) : moeMsgPo.engAction != null) return false;
        if (engNoOfButton != null ? !engNoOfButton.equals(moeMsgPo.engNoOfButton) : moeMsgPo.engNoOfButton != null)
            return false;
        if (chiMessageHeader != null ? !chiMessageHeader.equals(moeMsgPo.chiMessageHeader) : moeMsgPo.chiMessageHeader != null)
            return false;
        if (chiDescription != null ? !chiDescription.equals(moeMsgPo.chiDescription) : moeMsgPo.chiDescription != null)
            return false;
        if (chiCause != null ? !chiCause.equals(moeMsgPo.chiCause) : moeMsgPo.chiCause != null) return false;
        if (chiAction != null ? !chiAction.equals(moeMsgPo.chiAction) : moeMsgPo.chiAction != null) return false;
        if (chiNoOfButton != null ? !chiNoOfButton.equals(moeMsgPo.chiNoOfButton) : moeMsgPo.chiNoOfButton != null)
            return false;
        if (defaultButton != null ? !defaultButton.equals(moeMsgPo.defaultButton) : moeMsgPo.defaultButton != null)
            return false;
        if (effectiveDtm != null ? !effectiveDtm.equals(moeMsgPo.effectiveDtm) : moeMsgPo.effectiveDtm != null)
            return false;
        if (expiryDtm != null ? !expiryDtm.equals(moeMsgPo.expiryDtm) : moeMsgPo.expiryDtm != null) return false;
        if (createDtm != null ? !createDtm.equals(moeMsgPo.createDtm) : moeMsgPo.createDtm != null) return false;
        if (createBy != null ? !createBy.equals(moeMsgPo.createBy) : moeMsgPo.createBy != null) return false;
        if (updateDtm != null ? !updateDtm.equals(moeMsgPo.updateDtm) : moeMsgPo.updateDtm != null) return false;
        if (updateBy != null ? !updateBy.equals(moeMsgPo.updateBy) : moeMsgPo.updateBy != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = functionId != null ? functionId.hashCode() : 0;
        result = 31 * result + (severityCode != null ? severityCode.hashCode() : 0);
        result = 31 * result + (messageCode != null ? messageCode.hashCode() : 0);
        result = 31 * result + (int) (messageLevel ^ (messageLevel >>> 32));
        result = 31 * result + (engMessageHeader != null ? engMessageHeader.hashCode() : 0);
        result = 31 * result + (engDescription != null ? engDescription.hashCode() : 0);
        result = 31 * result + (engCause != null ? engCause.hashCode() : 0);
        result = 31 * result + (engAction != null ? engAction.hashCode() : 0);
        result = 31 * result + (engNoOfButton != null ? engNoOfButton.hashCode() : 0);
        result = 31 * result + (chiMessageHeader != null ? chiMessageHeader.hashCode() : 0);
        result = 31 * result + (chiDescription != null ? chiDescription.hashCode() : 0);
        result = 31 * result + (chiCause != null ? chiCause.hashCode() : 0);
        result = 31 * result + (chiAction != null ? chiAction.hashCode() : 0);
        result = 31 * result + (chiNoOfButton != null ? chiNoOfButton.hashCode() : 0);
        result = 31 * result + (defaultButton != null ? defaultButton.hashCode() : 0);
        result = 31 * result + (effectiveDtm != null ? effectiveDtm.hashCode() : 0);
        result = 31 * result + (expiryDtm != null ? expiryDtm.hashCode() : 0);
        result = 31 * result + (createDtm != null ? createDtm.hashCode() : 0);
        result = 31 * result + (createBy != null ? createBy.hashCode() : 0);
        result = 31 * result + (updateDtm != null ? updateDtm.hashCode() : 0);
        result = 31 * result + (updateBy != null ? updateBy.hashCode() : 0);
        return result;
    }
}
*/
