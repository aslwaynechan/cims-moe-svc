package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

public class MoeMyFavouriteDetailPoPK implements Serializable {
    private static final long serialVersionUID = 3442461869655954199L;
    //private String myFavouriteId;
    private long itemNo;
    private MoeMyFavouriteHdrPo moeMyFavouriteHdr;

/*    @Column(name = "MY_FAVOURITE_ID")
    @Id
    public String getMyFavouriteId() {
        return myFavouriteId;
    }

    public void setMyFavouriteId(String myFavouriteId) {
        this.myFavouriteId = myFavouriteId;
    }*/

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "my_favourite_id", nullable = false, insertable = false, updatable = false)
    public MoeMyFavouriteHdrPo getMoeMyFavouriteHdr() {
        return this.moeMyFavouriteHdr;
    }

    public void setMoeMyFavouriteHdr(MoeMyFavouriteHdrPo moeMyFavouriteHdr) {
        this.moeMyFavouriteHdr = moeMyFavouriteHdr;
    }

    @Column(name = "ITEM_NO")
    @Id
    public long getItemNo() {
        return itemNo;
    }

    public void setItemNo(long itemNo) {
        this.itemNo = itemNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeMyFavouriteDetailPoPK that = (MoeMyFavouriteDetailPoPK) o;

        if (itemNo != that.itemNo) return false;
        //if (myFavouriteId != null ? !myFavouriteId.equals(that.myFavouriteId) : that.myFavouriteId != null)
        //return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        //int result = myFavouriteId != null ? myFavouriteId.hashCode() : 0;
        result = 31 * result + (int) (itemNo ^ (itemNo >>> 32));
        return result;
    }
}
