/*
package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeOperationMethodPoPK implements Serializable {
    private String operationId;
    private String methodId;

    @Column(name = "OPERATION_ID")
    @Id
    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    @Column(name = "METHOD_ID")
    @Id
    public String getMethodId() {
        return methodId;
    }

    public void setMethodId(String methodId) {
        this.methodId = methodId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeOperationMethodPoPK that = (MoeOperationMethodPoPK) o;

        if (operationId != null ? !operationId.equals(that.operationId) : that.operationId != null) return false;
        if (methodId != null ? !methodId.equals(that.methodId) : that.methodId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = operationId != null ? operationId.hashCode() : 0;
        result = 31 * result + (methodId != null ? methodId.hashCode() : 0);
        return result;
    }
}
*/
