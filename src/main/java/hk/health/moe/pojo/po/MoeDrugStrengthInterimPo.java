package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "MOE_DRUG_STRENGTH_INTERIM")
@IdClass(MoeDrugStrengthInterimPoPK.class)
public class MoeDrugStrengthInterimPo implements Serializable {
    private static final long serialVersionUID = 5322055833686035283L;
    private String hkRegNo;
    private String localDrugId;
    private String strength;
    private String strengthLevelExtraInfo;
    private String amp;
    private Long ampId;
    private String vmp;
    private Long vmpId;
    private String createUserId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private Date createDtm;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private Date updateDtm;
    private long version;
    private String tradeNameAlias;
    private String specifyQuantityFlag;
    private String drugCategoryId;

    @Id
    @Column(name = "HK_REG_NO")
    public String getHkRegNo() {
        return hkRegNo;
    }

    public void setHkRegNo(String hkRegNo) {
        this.hkRegNo = hkRegNo;
    }

    @Id
    @Column(name = "LOCAL_DRUG_ID")
    public String getLocalDrugId() {
        return localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        this.localDrugId = localDrugId;
    }

    @Id
    @Column(name = "STRENGTH")
    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    @Id
    @Column(name = "STRENGTH_LEVEL_EXTRA_INFO")
    public String getStrengthLevelExtraInfo() {
        return strengthLevelExtraInfo;
    }

    public void setStrengthLevelExtraInfo(String strengthLevelExtraInfo) {
        this.strengthLevelExtraInfo = strengthLevelExtraInfo;
    }

    @Id
    @Column(name = "AMP")
    public String getAmp() {
        return amp;
    }

    public void setAmp(String amp) {
        this.amp = amp;
    }

    @Id
    @Column(name = "AMP_ID")
    public Long getAmpId() {
        return ampId;
    }

    public void setAmpId(Long ampId) {
        this.ampId = ampId;
    }

    @Id
    @Column(name = "VMP")
    public String getVmp() {
        return vmp;
    }

    public void setVmp(String vmp) {
        this.vmp = vmp;
    }

    @Id
    @Column(name = "VMP_ID")
    public Long getVmpId() {
        return vmpId;
    }

    public void setVmpId(Long vmpId) {
        this.vmpId = vmpId;
    }

    @Id
    @Column(name = "CREATE_USER_ID")
    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    @Id
    @Column(name = "CREATE_USER")
    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    @Id
    @Column(name = "CREATE_HOSP")
    public String getCreateHosp() {
        return createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    @Id
    @Column(name = "CREATE_RANK")
    public String getCreateRank() {
        return createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    @Id
    @Column(name = "CREATE_RANK_DESC")
    public String getCreateRankDesc() {
        return createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    @Id
    @Column(name = "CREATE_DTM")
    public Date getCreateDtm() {
        return createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    @Id
    @Column(name = "UPDATE_USER_ID")
    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Id
    @Column(name = "UPDATE_USER")
    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Id
    @Column(name = "UPDATE_HOSP")
    public String getUpdateHosp() {
        return updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    @Id
    @Column(name = "UPDATE_RANK")
    public String getUpdateRank() {
        return updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    @Id
    @Column(name = "UPDATE_RANK_DESC")
    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    @Id
    @Column(name = "UPDATE_DTM")
    public Date getUpdateDtm() {
        return updateDtm;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }

    @Id
    @Column(name = "VERSION")
    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    @Id
    @Column(name = "TRADE_NAME_ALIAS")
    public String getTradeNameAlias() {
        return tradeNameAlias;
    }

    public void setTradeNameAlias(String tradeNameAlias) {
        this.tradeNameAlias = tradeNameAlias;
    }

    @Id
    @Column(name = "SPECIFY_QUANTITY_FLAG")
    public String getSpecifyQuantityFlag() {
        return specifyQuantityFlag;
    }

    public void setSpecifyQuantityFlag(String specifyQuantityFlag) {
        this.specifyQuantityFlag = specifyQuantityFlag;
    }

    @Id
    @Column(name = "DRUG_CATEGORY_ID")
    public String getDrugCategoryId() {
        return drugCategoryId;
    }

    public void setDrugCategoryId(String drugCategoryId) {
        this.drugCategoryId = drugCategoryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDrugStrengthInterimPo that = (MoeDrugStrengthInterimPo) o;

        if (version != that.version) return false;
        if (hkRegNo != null ? !hkRegNo.equals(that.hkRegNo) : that.hkRegNo != null) return false;
        if (localDrugId != null ? !localDrugId.equals(that.localDrugId) : that.localDrugId != null) return false;
        if (strength != null ? !strength.equals(that.strength) : that.strength != null) return false;
        if (strengthLevelExtraInfo != null ? !strengthLevelExtraInfo.equals(that.strengthLevelExtraInfo) : that.strengthLevelExtraInfo != null)
            return false;
        if (amp != null ? !amp.equals(that.amp) : that.amp != null) return false;
        if (ampId != null ? !ampId.equals(that.ampId) : that.ampId != null) return false;
        if (vmp != null ? !vmp.equals(that.vmp) : that.vmp != null) return false;
        if (vmpId != null ? !vmpId.equals(that.vmpId) : that.vmpId != null) return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null) return false;
        if (createUser != null ? !createUser.equals(that.createUser) : that.createUser != null) return false;
        if (createHosp != null ? !createHosp.equals(that.createHosp) : that.createHosp != null) return false;
        if (createRank != null ? !createRank.equals(that.createRank) : that.createRank != null) return false;
        if (createRankDesc != null ? !createRankDesc.equals(that.createRankDesc) : that.createRankDesc != null)
            return false;
        if (createDtm != null ? !createDtm.equals(that.createDtm) : that.createDtm != null) return false;
        if (updateUserId != null ? !updateUserId.equals(that.updateUserId) : that.updateUserId != null) return false;
        if (updateUser != null ? !updateUser.equals(that.updateUser) : that.updateUser != null) return false;
        if (updateHosp != null ? !updateHosp.equals(that.updateHosp) : that.updateHosp != null) return false;
        if (updateRank != null ? !updateRank.equals(that.updateRank) : that.updateRank != null) return false;
        if (updateRankDesc != null ? !updateRankDesc.equals(that.updateRankDesc) : that.updateRankDesc != null)
            return false;
        if (updateDtm != null ? !updateDtm.equals(that.updateDtm) : that.updateDtm != null) return false;
        if (tradeNameAlias != null ? !tradeNameAlias.equals(that.tradeNameAlias) : that.tradeNameAlias != null)
            return false;
        if (specifyQuantityFlag != null ? !specifyQuantityFlag.equals(that.specifyQuantityFlag) : that.specifyQuantityFlag != null)
            return false;
        if (drugCategoryId != null ? !drugCategoryId.equals(that.drugCategoryId) : that.drugCategoryId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hkRegNo != null ? hkRegNo.hashCode() : 0;
        result = 31 * result + (localDrugId != null ? localDrugId.hashCode() : 0);
        result = 31 * result + (strength != null ? strength.hashCode() : 0);
        result = 31 * result + (strengthLevelExtraInfo != null ? strengthLevelExtraInfo.hashCode() : 0);
        result = 31 * result + (amp != null ? amp.hashCode() : 0);
        result = 31 * result + (ampId != null ? ampId.hashCode() : 0);
        result = 31 * result + (vmp != null ? vmp.hashCode() : 0);
        result = 31 * result + (vmpId != null ? vmpId.hashCode() : 0);
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (createUser != null ? createUser.hashCode() : 0);
        result = 31 * result + (createHosp != null ? createHosp.hashCode() : 0);
        result = 31 * result + (createRank != null ? createRank.hashCode() : 0);
        result = 31 * result + (createRankDesc != null ? createRankDesc.hashCode() : 0);
        result = 31 * result + (createDtm != null ? createDtm.hashCode() : 0);
        result = 31 * result + (updateUserId != null ? updateUserId.hashCode() : 0);
        result = 31 * result + (updateUser != null ? updateUser.hashCode() : 0);
        result = 31 * result + (updateHosp != null ? updateHosp.hashCode() : 0);
        result = 31 * result + (updateRank != null ? updateRank.hashCode() : 0);
        result = 31 * result + (updateRankDesc != null ? updateRankDesc.hashCode() : 0);
        result = 31 * result + (updateDtm != null ? updateDtm.hashCode() : 0);
        result = 31 * result + (int) (version ^ (version >>> 32));
        result = 31 * result + (tradeNameAlias != null ? tradeNameAlias.hashCode() : 0);
        result = 31 * result + (specifyQuantityFlag != null ? specifyQuantityFlag.hashCode() : 0);
        result = 31 * result + (drugCategoryId != null ? drugCategoryId.hashCode() : 0);
        return result;
    }
}
