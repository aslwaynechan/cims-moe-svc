package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeHospitalSettingPoPK implements Serializable {
    private String hospcode;
    private String userSpecialty;
    private String paramName;

    @Column(name = "HOSPCODE")
    @Id
    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    @Column(name = "USER_SPECIALTY")
    @Id
    public String getUserSpecialty() {
        return userSpecialty;
    }

    public void setUserSpecialty(String userSpecialty) {
        this.userSpecialty = userSpecialty;
    }

    @Column(name = "PARAM_NAME")
    @Id
    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeHospitalSettingPoPK that = (MoeHospitalSettingPoPK) o;

        if (hospcode != null ? !hospcode.equals(that.hospcode) : that.hospcode != null) return false;
        if (userSpecialty != null ? !userSpecialty.equals(that.userSpecialty) : that.userSpecialty != null)
            return false;
        if (paramName != null ? !paramName.equals(that.paramName) : that.paramName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hospcode != null ? hospcode.hashCode() : 0;
        result = 31 * result + (userSpecialty != null ? userSpecialty.hashCode() : 0);
        result = 31 * result + (paramName != null ? paramName.hashCode() : 0);
        return result;
    }
}
