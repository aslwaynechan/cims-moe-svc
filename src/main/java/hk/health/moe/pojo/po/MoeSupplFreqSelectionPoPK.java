package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeSupplFreqSelectionPoPK implements Serializable {
    private static final long serialVersionUID = -7021581553641743770L;
    private long supplFreqId;
    private String supplFreqDisplay;

    @Column(name = "SUPPL_FREQ_ID")
    @Id
    public long getSupplFreqId() {
        return supplFreqId;
    }

    public void setSupplFreqId(long supplFreqId) {
        this.supplFreqId = supplFreqId;
    }

    @Column(name = "SUPPL_FREQ_DISPLAY")
    @Id
    public String getSupplFreqDisplay() {
        return supplFreqDisplay;
    }

    public void setSupplFreqDisplay(String supplFreqDisplay) {
        this.supplFreqDisplay = supplFreqDisplay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeSupplFreqSelectionPoPK that = (MoeSupplFreqSelectionPoPK) o;

        if (supplFreqId != that.supplFreqId) return false;
        if (supplFreqDisplay != null ? !supplFreqDisplay.equals(that.supplFreqDisplay) : that.supplFreqDisplay != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (supplFreqId ^ (supplFreqId >>> 32));
        result = 31 * result + (supplFreqDisplay != null ? supplFreqDisplay.hashCode() : 0);
        return result;
    }
}
