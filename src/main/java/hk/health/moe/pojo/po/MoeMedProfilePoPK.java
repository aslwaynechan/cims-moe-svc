package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeMedProfilePoPK implements Serializable {
    private static final long serialVersionUID = 4311672665999492592L;
    private String hospcode;
    private long ordNo;
    private long cmsItemNo;

    @Column(name = "HOSPCODE")
    @Id
    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    @Column(name = "ORD_NO")
    @Id
    public long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(long ordNo) {
        this.ordNo = ordNo;
    }

    @Column(name = "CMS_ITEM_NO")
    @Id
    public long getCmsItemNo() {
        return cmsItemNo;
    }

    public void setCmsItemNo(long cmsItemNo) {
        this.cmsItemNo = cmsItemNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeMedProfilePoPK that = (MoeMedProfilePoPK) o;

        if (ordNo != that.ordNo) return false;
        if (cmsItemNo != that.cmsItemNo) return false;
        if (hospcode != null ? !hospcode.equals(that.hospcode) : that.hospcode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hospcode != null ? hospcode.hashCode() : 0;
        result = 31 * result + (int) (ordNo ^ (ordNo >>> 32));
        result = 31 * result + (int) (cmsItemNo ^ (cmsItemNo >>> 32));
        return result;
    }
}
