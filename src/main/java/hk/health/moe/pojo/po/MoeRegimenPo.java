package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MOE_REGIMEN")
public class MoeRegimenPo implements Serializable {
    private static final long serialVersionUID = -7897937349012248274L;
    private String regimenType;
    private String regimen;
    private long rank;
    private Set<MoeCommonDosagePo> moeCommonDosages = new HashSet<>();
    private Set<MoeSupplFreqPo> moeSupplFreqs = new HashSet<>();
    private Set<MoeRegimenUnitPo> moeRegimenUnits = new HashSet<>();


    @Id
    @Column(name = "REGIMEN_TYPE")
    public String getRegimenType() {
        return regimenType;
    }

    public void setRegimenType(String regimenType) {
        this.regimenType = regimenType;
    }

    @Basic
    @Column(name = "REGIMEN")
    public String getRegimen() {
        return regimen;
    }

    public void setRegimen(String regimen) {
        this.regimen = regimen;
    }

    @Basic
    @Column(name = "RANK")
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moeRegimen")
    public Set<MoeCommonDosagePo> getMoeCommonDosages() {
        return this.moeCommonDosages;
    }

    public void setMoeCommonDosages(Set<MoeCommonDosagePo> moeCommonDosages) {
        this.moeCommonDosages = moeCommonDosages;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moeRegimen")
    public Set<MoeSupplFreqPo> getMoeSupplFreqs() {
        return this.moeSupplFreqs;
    }

    public void setMoeSupplFreqs(Set<MoeSupplFreqPo> moeSupplFreqs) {
        this.moeSupplFreqs = moeSupplFreqs;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moeRegimen")
    public Set<MoeRegimenUnitPo> getMoeRegimenUnits() {
        return this.moeRegimenUnits;
    }

    public void setMoeRegimenUnits(Set<MoeRegimenUnitPo> moeRegimenUnits) {
        this.moeRegimenUnits = moeRegimenUnits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeRegimenPo that = (MoeRegimenPo) o;

        if (rank != that.rank) return false;
        if (regimenType != null ? !regimenType.equals(that.regimenType) : that.regimenType != null) return false;
        if (regimen != null ? !regimen.equals(that.regimen) : that.regimen != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = regimenType != null ? regimenType.hashCode() : 0;
        result = 31 * result + (regimen != null ? regimen.hashCode() : 0);
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        return result;
    }
}
