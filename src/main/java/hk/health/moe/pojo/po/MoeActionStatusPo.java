package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_ACTION_STATUS")
public class MoeActionStatusPo  implements Serializable {
    private static final long serialVersionUID = -4252616227569638455L;
    private String actionStatusType;
    private String actionStatus;
    private long rank;

    @Id
    @Column(name = "ACTION_STATUS_TYPE")
    public String getActionStatusType() {
        return actionStatusType;
    }

    public void setActionStatusType(String actionStatusType) {
        this.actionStatusType = actionStatusType;
    }

    @Basic
    @Column(name = "ACTION_STATUS")
    public String getActionStatus() {
        return actionStatus;
    }

    public void setActionStatus(String actionStatus) {
        this.actionStatus = actionStatus;
    }

    @Basic
    @Column(name = "RANK")
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeActionStatusPo that = (MoeActionStatusPo) o;

        if (rank != that.rank) return false;
        if (actionStatusType != null ? !actionStatusType.equals(that.actionStatusType) : that.actionStatusType != null)
            return false;
        if (actionStatus != null ? !actionStatus.equals(that.actionStatus) : that.actionStatus != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = actionStatusType != null ? actionStatusType.hashCode() : 0;
        result = 31 * result + (actionStatus != null ? actionStatus.hashCode() : 0);
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        return result;
    }
}
