package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_SUPPL_FREQ_SELECTION")
@IdClass(MoeSupplFreqSelectionPoPK.class)
public class MoeSupplFreqSelectionPo implements Serializable, Comparable<MoeSupplFreqSelectionPo> {
    private static final long serialVersionUID = 4239122064719865213L;
    private long supplFreqSelectionId;
    private long supplFreqId;
    private String supplFreqDisplay;
    private long rank;
    private MoeSupplFreqPo moeSupplFreq;

    @Basic
    @Column(name = "SUPPL_FREQ_SELECTION_ID")
    public long getSupplFreqSelectionId() {
        return supplFreqSelectionId;
    }

    public void setSupplFreqSelectionId(long supplFreqSelectionId) {
        this.supplFreqSelectionId = supplFreqSelectionId;
    }

    @Id
    @Column(name = "SUPPL_FREQ_ID")
    public long getSupplFreqId() {
        return supplFreqId;
    }

    public void setSupplFreqId(long supplFreqId) {
        this.supplFreqId = supplFreqId;
    }

    @Id
    @Column(name = "SUPPL_FREQ_DISPLAY")
    public String getSupplFreqDisplay() {
        return supplFreqDisplay;
    }

    public void setSupplFreqDisplay(String supplFreqDisplay) {
        this.supplFreqDisplay = supplFreqDisplay;
    }

    @Basic
    @Column(name = "RANK")
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "suppl_freq_id", nullable = false, insertable = false, updatable = false)
    public MoeSupplFreqPo getMoeSupplFreq() {
        return this.moeSupplFreq;
    }

    public void setMoeSupplFreq(MoeSupplFreqPo moeSupplFreq) {
        this.moeSupplFreq = moeSupplFreq;
    }

    @Override
    public int compareTo(MoeSupplFreqSelectionPo o) {
        long thisVal = this.rank;
        long anotherVal = o.getRank();
        return (thisVal<anotherVal ? -1 : (thisVal==anotherVal ? 0 : 1));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeSupplFreqSelectionPo that = (MoeSupplFreqSelectionPo) o;

        if (supplFreqSelectionId != that.supplFreqSelectionId) return false;
        if (supplFreqId != that.supplFreqId) return false;
        if (rank != that.rank) return false;
        if (supplFreqDisplay != null ? !supplFreqDisplay.equals(that.supplFreqDisplay) : that.supplFreqDisplay != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (supplFreqSelectionId ^ (supplFreqSelectionId >>> 32));
        result = 31 * result + (int) (supplFreqId ^ (supplFreqId >>> 32));
        result = 31 * result + (supplFreqDisplay != null ? supplFreqDisplay.hashCode() : 0);
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        return result;
    }
}
