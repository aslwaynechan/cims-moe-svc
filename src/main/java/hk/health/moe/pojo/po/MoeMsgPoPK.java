/*
package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeMsgPoPK implements Serializable {
    private String functionId;
    private String severityCode;
    private String messageCode;

    @Column(name = "FUNCTION_ID")
    @Id
    public String getFunctionId() {
        return functionId;
    }

    public void setFunctionId(String functionId) {
        this.functionId = functionId;
    }

    @Column(name = "SEVERITY_CODE")
    @Id
    public String getSeverityCode() {
        return severityCode;
    }

    public void setSeverityCode(String severityCode) {
        this.severityCode = severityCode;
    }

    @Column(name = "MESSAGE_CODE")
    @Id
    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeMsgPoPK that = (MoeMsgPoPK) o;

        if (functionId != null ? !functionId.equals(that.functionId) : that.functionId != null) return false;
        if (severityCode != null ? !severityCode.equals(that.severityCode) : that.severityCode != null) return false;
        if (messageCode != null ? !messageCode.equals(that.messageCode) : that.messageCode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = functionId != null ? functionId.hashCode() : 0;
        result = 31 * result + (severityCode != null ? severityCode.hashCode() : 0);
        result = 31 * result + (messageCode != null ? messageCode.hashCode() : 0);
        return result;
    }
}
*/
