package hk.health.moe.pojo.po;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MOE_PATIENT")
public class MoePatientPo implements Serializable {
    private static final long serialVersionUID = -6004345705740299093L;
    private String moePatientKey;
    private String patientRefKey;
    private Set<MoeEhrOrderPo> moeEhrOrders = new HashSet<MoeEhrOrderPo>();

    @GeneratedValue(generator="moePatientKeyGenerator")
    @GenericGenerator(name="moePatientKeyGenerator", strategy = "uuid")
    @Id
    @Column(name = "MOE_PATIENT_KEY")
    public String getMoePatientKey() {
        return moePatientKey;
    }

    public void setMoePatientKey(String moePatientKey) {
        this.moePatientKey = moePatientKey;
    }

    @Basic
    @Column(name = "PATIENT_REF_KEY")
    public String getPatientRefKey() {
        return patientRefKey;
    }

    public void setPatientRefKey(String patientRefKey) {
        this.patientRefKey = patientRefKey;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moePatient")
    public Set<MoeEhrOrderPo> getMoeEhrOrders() {
        return this.moeEhrOrders;
    }

    public void setMoeEhrOrders(Set<MoeEhrOrderPo> moeEhrOrders) {
        this.moeEhrOrders = moeEhrOrders;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoePatientPo that = (MoePatientPo) o;

        if (moePatientKey != null ? !moePatientKey.equals(that.moePatientKey) : that.moePatientKey != null)
            return false;
        if (patientRefKey != null ? !patientRefKey.equals(that.patientRefKey) : that.patientRefKey != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = moePatientKey != null ? moePatientKey.hashCode() : 0;
        result = 31 * result + (patientRefKey != null ? patientRefKey.hashCode() : 0);
        return result;
    }
}
