package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MOE_METHOD")
public class MoeMethodPo implements Serializable {
    private static final long serialVersionUID = -4137395672345232463L;
    private String methodId;
    private String methodName;
    private Set<MoeOperationPo> operations = new HashSet<>();

    @Id
    @Column(name = "METHOD_ID")
    public String getMethodId() {
        return methodId;
    }

    public void setMethodId(String methodId) {
        this.methodId = methodId;
    }

    @Basic
    @Column(name = "METHOD_NAME")
    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "MOE_OPERATION_METHOD",
            joinColumns = { @JoinColumn(name = "METHOD_ID", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "OPERATION_ID", nullable = false, updatable = false) })
    public Set<MoeOperationPo> getOperations() {
        return this.operations;
    }

    public void setOperations(Set<MoeOperationPo> operations) {
        this.operations = operations;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeMethodPo that = (MoeMethodPo) o;

        if (methodId != null ? !methodId.equals(that.methodId) : that.methodId != null) return false;
        if (methodName != null ? !methodName.equals(that.methodName) : that.methodName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = methodId != null ? methodId.hashCode() : 0;
        result = 31 * result + (methodName != null ? methodName.hashCode() : 0);
        return result;
    }
}
