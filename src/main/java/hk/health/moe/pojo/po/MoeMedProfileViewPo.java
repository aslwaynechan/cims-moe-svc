/*
package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "MOE_MED_PROFILE_VIEW")
public class MoeMedProfileViewPo {
    private String hospcode;
    private Long ordNo;
    private String patientKey;
    private String episodeNo;
    private String createUserId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private Date createDtm;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private Date lastUpdDate;
    private String refNo;
    private Date ordDate;
    private String episodeType;
    private String ipasWard;
    private String bedNo;
    private String specialty;
    private String tradeName;
    private String vtm;
    private String aliasName;
    private String aliasNameType;
    private String manufacturer;
    private String drugRoute;
    private String regimenRoute;
    private String formDesc;
    private String doseFormExtraInfo;
    private String genericIndicator;
    private String strength;
    private String strengthLevelExtraInfo;
    private String strengthCompulsory;
    private String dangerdrug;
    private String nameDesc;
    private Long cmsItemNo;
    private Long orgItemNo;
    private Long stepNo;
    private Long multDoseNo;
    private String orderLineType;
    private Long dosage;
    private String modu;
    private String prn;
    private Date startDate;
    private Date endDate;
    private Long moQty;
    private String moQtyUnit;
    private String actionStatus;
    private String drugDesc;
    private String drugDescLong;
    private String freqCode;
    private Long freq1;
    private String freqText;
    private String supFreqCode;
    private Long supFreq1;
    private Long supFreq2;
    private String supFreqText;
    private Long duration;
    private String durationUnitDesc;
    private Long durationMultiplier;
    private String siteDesc;
    private String specInstruct;
    private String itemcode;
    private Date itemUpdateTime;
    private Long overridingRowNo;
    private String allergen;
    private String matchReactionType;
    private String overrideReason;

    @Basic
    @Column(name = "HOSPCODE")
    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    @Basic
    @Column(name = "ORD_NO")
    public Long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(Long ordNo) {
        this.ordNo = ordNo;
    }

    @Basic
    @Column(name = "PATIENT_KEY")
    public String getPatientKey() {
        return patientKey;
    }

    public void setPatientKey(String patientKey) {
        this.patientKey = patientKey;
    }

    @Basic
    @Column(name = "EPISODE_NO")
    public String getEpisodeNo() {
        return episodeNo;
    }

    public void setEpisodeNo(String episodeNo) {
        this.episodeNo = episodeNo;
    }

    @Basic
    @Column(name = "CREATE_USER_ID")
    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    @Basic
    @Column(name = "CREATE_USER")
    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    @Basic
    @Column(name = "CREATE_HOSP")
    public String getCreateHosp() {
        return createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    @Basic
    @Column(name = "CREATE_RANK")
    public String getCreateRank() {
        return createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    @Basic
    @Column(name = "CREATE_RANK_DESC")
    public String getCreateRankDesc() {
        return createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    @Basic
    @Column(name = "CREATE_DTM")
    public Date getCreateDtm() {
        return createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    @Basic
    @Column(name = "UPDATE_USER_ID")
    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Basic
    @Column(name = "UPDATE_USER")
    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Basic
    @Column(name = "UPDATE_HOSP")
    public String getUpdateHosp() {
        return updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    @Basic
    @Column(name = "UPDATE_RANK")
    public String getUpdateRank() {
        return updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    @Basic
    @Column(name = "UPDATE_RANK_DESC")
    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    @Basic
    @Column(name = "LAST_UPD_DATE")
    public Date getLastUpdDate() {
        return lastUpdDate;
    }

    public void setLastUpdDate(Date lastUpdDate) {
        this.lastUpdDate = lastUpdDate;
    }

    @Basic
    @Column(name = "REF_NO")
    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    @Basic
    @Column(name = "ORD_DATE")
    public Date getOrdDate() {
        return ordDate;
    }

    public void setOrdDate(Date ordDate) {
        this.ordDate = ordDate;
    }

    @Basic
    @Column(name = "EPISODE_TYPE")
    public String getEpisodeType() {
        return episodeType;
    }

    public void setEpisodeType(String episodeType) {
        this.episodeType = episodeType;
    }

    @Basic
    @Column(name = "IPAS_WARD")
    public String getIpasWard() {
        return ipasWard;
    }

    public void setIpasWard(String ipasWard) {
        this.ipasWard = ipasWard;
    }

    @Basic
    @Column(name = "BED_NO")
    public String getBedNo() {
        return bedNo;
    }

    public void setBedNo(String bedNo) {
        this.bedNo = bedNo;
    }

    @Basic
    @Column(name = "SPECIALTY")
    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    @Basic
    @Column(name = "TRADE_NAME")
    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    @Basic
    @Column(name = "VTM")
    public String getVtm() {
        return vtm;
    }

    public void setVtm(String vtm) {
        this.vtm = vtm;
    }

    @Basic
    @Column(name = "ALIAS_NAME")
    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    @Basic
    @Column(name = "ALIAS_NAME_TYPE")
    public String getAliasNameType() {
        return aliasNameType;
    }

    public void setAliasNameType(String aliasNameType) {
        this.aliasNameType = aliasNameType;
    }

    @Basic
    @Column(name = "MANUFACTURER")
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Basic
    @Column(name = "DRUG_ROUTE")
    public String getDrugRoute() {
        return drugRoute;
    }

    public void setDrugRoute(String drugRoute) {
        this.drugRoute = drugRoute;
    }

    @Basic
    @Column(name = "REGIMEN_ROUTE")
    public String getRegimenRoute() {
        return regimenRoute;
    }

    public void setRegimenRoute(String regimenRoute) {
        this.regimenRoute = regimenRoute;
    }

    @Basic
    @Column(name = "FORM_DESC")
    public String getFormDesc() {
        return formDesc;
    }

    public void setFormDesc(String formDesc) {
        this.formDesc = formDesc;
    }

    @Basic
    @Column(name = "DOSE_FORM_EXTRA_INFO")
    public String getDoseFormExtraInfo() {
        return doseFormExtraInfo;
    }

    public void setDoseFormExtraInfo(String doseFormExtraInfo) {
        this.doseFormExtraInfo = doseFormExtraInfo;
    }

    @Basic
    @Column(name = "GENERIC_INDICATOR")
    public String getGenericIndicator() {
        return genericIndicator;
    }

    public void setGenericIndicator(String genericIndicator) {
        this.genericIndicator = genericIndicator;
    }

    @Basic
    @Column(name = "STRENGTH")
    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    @Basic
    @Column(name = "STRENGTH_LEVEL_EXTRA_INFO")
    public String getStrengthLevelExtraInfo() {
        return strengthLevelExtraInfo;
    }

    public void setStrengthLevelExtraInfo(String strengthLevelExtraInfo) {
        this.strengthLevelExtraInfo = strengthLevelExtraInfo;
    }

    @Basic
    @Column(name = "STRENGTH_COMPULSORY")
    public String getStrengthCompulsory() {
        return strengthCompulsory;
    }

    public void setStrengthCompulsory(String strengthCompulsory) {
        this.strengthCompulsory = strengthCompulsory;
    }

    @Basic
    @Column(name = "DANGERDRUG")
    public String getDangerdrug() {
        return dangerdrug;
    }

    public void setDangerdrug(String dangerdrug) {
        this.dangerdrug = dangerdrug;
    }

    @Basic
    @Column(name = "NAME_DESC")
    public String getNameDesc() {
        return nameDesc;
    }

    public void setNameDesc(String nameDesc) {
        this.nameDesc = nameDesc;
    }

    @Basic
    @Column(name = "CMS_ITEM_NO")
    public Long getCmsItemNo() {
        return cmsItemNo;
    }

    public void setCmsItemNo(Long cmsItemNo) {
        this.cmsItemNo = cmsItemNo;
    }

    @Basic
    @Column(name = "ORG_ITEM_NO")
    public Long getOrgItemNo() {
        return orgItemNo;
    }

    public void setOrgItemNo(Long orgItemNo) {
        this.orgItemNo = orgItemNo;
    }

    @Basic
    @Column(name = "STEP_NO")
    public Long getStepNo() {
        return stepNo;
    }

    public void setStepNo(Long stepNo) {
        this.stepNo = stepNo;
    }

    @Basic
    @Column(name = "MULT_DOSE_NO")
    public Long getMultDoseNo() {
        return multDoseNo;
    }

    public void setMultDoseNo(Long multDoseNo) {
        this.multDoseNo = multDoseNo;
    }

    @Basic
    @Column(name = "ORDER_LINE_TYPE")
    public String getOrderLineType() {
        return orderLineType;
    }

    public void setOrderLineType(String orderLineType) {
        this.orderLineType = orderLineType;
    }

    @Basic
    @Column(name = "DOSAGE")
    public Long getDosage() {
        return dosage;
    }

    public void setDosage(Long dosage) {
        this.dosage = dosage;
    }

    @Basic
    @Column(name = "MODU")
    public String getModu() {
        return modu;
    }

    public void setModu(String modu) {
        this.modu = modu;
    }

    @Basic
    @Column(name = "PRN")
    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    @Basic
    @Column(name = "START_DATE")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "END_DATE")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "MO_QTY")
    public Long getMoQty() {
        return moQty;
    }

    public void setMoQty(Long moQty) {
        this.moQty = moQty;
    }

    @Basic
    @Column(name = "MO_QTY_UNIT")
    public String getMoQtyUnit() {
        return moQtyUnit;
    }

    public void setMoQtyUnit(String moQtyUnit) {
        this.moQtyUnit = moQtyUnit;
    }

    @Basic
    @Column(name = "ACTION_STATUS")
    public String getActionStatus() {
        return actionStatus;
    }

    public void setActionStatus(String actionStatus) {
        this.actionStatus = actionStatus;
    }

    @Basic
    @Column(name = "DRUG_DESC")
    public String getDrugDesc() {
        return drugDesc;
    }

    public void setDrugDesc(String drugDesc) {
        this.drugDesc = drugDesc;
    }

    @Basic
    @Column(name = "DRUG_DESC_LONG")
    public String getDrugDescLong() {
        return drugDescLong;
    }

    public void setDrugDescLong(String drugDescLong) {
        this.drugDescLong = drugDescLong;
    }

    @Basic
    @Column(name = "FREQ_CODE")
    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    @Basic
    @Column(name = "FREQ1")
    public Long getFreq1() {
        return freq1;
    }

    public void setFreq1(Long freq1) {
        this.freq1 = freq1;
    }

    @Basic
    @Column(name = "FREQ_TEXT")
    public String getFreqText() {
        return freqText;
    }

    public void setFreqText(String freqText) {
        this.freqText = freqText;
    }

    @Basic
    @Column(name = "SUP_FREQ_CODE")
    public String getSupFreqCode() {
        return supFreqCode;
    }

    public void setSupFreqCode(String supFreqCode) {
        this.supFreqCode = supFreqCode;
    }

    @Basic
    @Column(name = "SUP_FREQ1")
    public Long getSupFreq1() {
        return supFreq1;
    }

    public void setSupFreq1(Long supFreq1) {
        this.supFreq1 = supFreq1;
    }

    @Basic
    @Column(name = "SUP_FREQ2")
    public Long getSupFreq2() {
        return supFreq2;
    }

    public void setSupFreq2(Long supFreq2) {
        this.supFreq2 = supFreq2;
    }

    @Basic
    @Column(name = "SUP_FREQ_TEXT")
    public String getSupFreqText() {
        return supFreqText;
    }

    public void setSupFreqText(String supFreqText) {
        this.supFreqText = supFreqText;
    }

    @Basic
    @Column(name = "DURATION")
    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    @Basic
    @Column(name = "DURATION_UNIT_DESC")
    public String getDurationUnitDesc() {
        return durationUnitDesc;
    }

    public void setDurationUnitDesc(String durationUnitDesc) {
        this.durationUnitDesc = durationUnitDesc;
    }

    @Basic
    @Column(name = "DURATION_MULTIPLIER")
    public Long getDurationMultiplier() {
        return durationMultiplier;
    }

    public void setDurationMultiplier(Long durationMultiplier) {
        this.durationMultiplier = durationMultiplier;
    }

    @Basic
    @Column(name = "SITE_DESC")
    public String getSiteDesc() {
        return siteDesc;
    }

    public void setSiteDesc(String siteDesc) {
        this.siteDesc = siteDesc;
    }

    @Basic
    @Column(name = "SPEC_INSTRUCT")
    public String getSpecInstruct() {
        return specInstruct;
    }

    public void setSpecInstruct(String specInstruct) {
        this.specInstruct = specInstruct;
    }

    @Basic
    @Column(name = "ITEMCODE")
    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    @Basic
    @Column(name = "ITEM_UPDATE_TIME")
    public Date getItemUpdateTime() {
        return itemUpdateTime;
    }

    public void setItemUpdateTime(Date itemUpdateTime) {
        this.itemUpdateTime = itemUpdateTime;
    }

    @Basic
    @Column(name = "OVERRIDING_ROW_NO")
    public Long getOverridingRowNo() {
        return overridingRowNo;
    }

    public void setOverridingRowNo(Long overridingRowNo) {
        this.overridingRowNo = overridingRowNo;
    }

    @Basic
    @Column(name = "ALLERGEN")
    public String getAllergen() {
        return allergen;
    }

    public void setAllergen(String allergen) {
        this.allergen = allergen;
    }

    @Basic
    @Column(name = "MATCH_REACTION_TYPE")
    public String getMatchReactionType() {
        return matchReactionType;
    }

    public void setMatchReactionType(String matchReactionType) {
        this.matchReactionType = matchReactionType;
    }

    @Basic
    @Column(name = "OVERRIDE_REASON")
    public String getOverrideReason() {
        return overrideReason;
    }

    public void setOverrideReason(String overrideReason) {
        this.overrideReason = overrideReason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeMedProfileViewPo that = (MoeMedProfileViewPo) o;

        if (hospcode != null ? !hospcode.equals(that.hospcode) : that.hospcode != null) return false;
        if (ordNo != null ? !ordNo.equals(that.ordNo) : that.ordNo != null) return false;
        if (patientKey != null ? !patientKey.equals(that.patientKey) : that.patientKey != null) return false;
        if (episodeNo != null ? !episodeNo.equals(that.episodeNo) : that.episodeNo != null) return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null) return false;
        if (createUser != null ? !createUser.equals(that.createUser) : that.createUser != null) return false;
        if (createHosp != null ? !createHosp.equals(that.createHosp) : that.createHosp != null) return false;
        if (createRank != null ? !createRank.equals(that.createRank) : that.createRank != null) return false;
        if (createRankDesc != null ? !createRankDesc.equals(that.createRankDesc) : that.createRankDesc != null)
            return false;
        if (createDtm != null ? !createDtm.equals(that.createDtm) : that.createDtm != null) return false;
        if (updateUserId != null ? !updateUserId.equals(that.updateUserId) : that.updateUserId != null) return false;
        if (updateUser != null ? !updateUser.equals(that.updateUser) : that.updateUser != null) return false;
        if (updateHosp != null ? !updateHosp.equals(that.updateHosp) : that.updateHosp != null) return false;
        if (updateRank != null ? !updateRank.equals(that.updateRank) : that.updateRank != null) return false;
        if (updateRankDesc != null ? !updateRankDesc.equals(that.updateRankDesc) : that.updateRankDesc != null)
            return false;
        if (lastUpdDate != null ? !lastUpdDate.equals(that.lastUpdDate) : that.lastUpdDate != null) return false;
        if (refNo != null ? !refNo.equals(that.refNo) : that.refNo != null) return false;
        if (ordDate != null ? !ordDate.equals(that.ordDate) : that.ordDate != null) return false;
        if (episodeType != null ? !episodeType.equals(that.episodeType) : that.episodeType != null) return false;
        if (ipasWard != null ? !ipasWard.equals(that.ipasWard) : that.ipasWard != null) return false;
        if (bedNo != null ? !bedNo.equals(that.bedNo) : that.bedNo != null) return false;
        if (specialty != null ? !specialty.equals(that.specialty) : that.specialty != null) return false;
        if (tradeName != null ? !tradeName.equals(that.tradeName) : that.tradeName != null) return false;
        if (vtm != null ? !vtm.equals(that.vtm) : that.vtm != null) return false;
        if (aliasName != null ? !aliasName.equals(that.aliasName) : that.aliasName != null) return false;
        if (aliasNameType != null ? !aliasNameType.equals(that.aliasNameType) : that.aliasNameType != null)
            return false;
        if (manufacturer != null ? !manufacturer.equals(that.manufacturer) : that.manufacturer != null) return false;
        if (drugRoute != null ? !drugRoute.equals(that.drugRoute) : that.drugRoute != null) return false;
        if (regimenRoute != null ? !regimenRoute.equals(that.regimenRoute) : that.regimenRoute != null) return false;
        if (formDesc != null ? !formDesc.equals(that.formDesc) : that.formDesc != null) return false;
        if (doseFormExtraInfo != null ? !doseFormExtraInfo.equals(that.doseFormExtraInfo) : that.doseFormExtraInfo != null)
            return false;
        if (genericIndicator != null ? !genericIndicator.equals(that.genericIndicator) : that.genericIndicator != null)
            return false;
        if (strength != null ? !strength.equals(that.strength) : that.strength != null) return false;
        if (strengthLevelExtraInfo != null ? !strengthLevelExtraInfo.equals(that.strengthLevelExtraInfo) : that.strengthLevelExtraInfo != null)
            return false;
        if (strengthCompulsory != null ? !strengthCompulsory.equals(that.strengthCompulsory) : that.strengthCompulsory != null)
            return false;
        if (dangerdrug != null ? !dangerdrug.equals(that.dangerdrug) : that.dangerdrug != null) return false;
        if (nameDesc != null ? !nameDesc.equals(that.nameDesc) : that.nameDesc != null) return false;
        if (cmsItemNo != null ? !cmsItemNo.equals(that.cmsItemNo) : that.cmsItemNo != null) return false;
        if (orgItemNo != null ? !orgItemNo.equals(that.orgItemNo) : that.orgItemNo != null) return false;
        if (stepNo != null ? !stepNo.equals(that.stepNo) : that.stepNo != null) return false;
        if (multDoseNo != null ? !multDoseNo.equals(that.multDoseNo) : that.multDoseNo != null) return false;
        if (orderLineType != null ? !orderLineType.equals(that.orderLineType) : that.orderLineType != null)
            return false;
        if (dosage != null ? !dosage.equals(that.dosage) : that.dosage != null) return false;
        if (modu != null ? !modu.equals(that.modu) : that.modu != null) return false;
        if (prn != null ? !prn.equals(that.prn) : that.prn != null) return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;
        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
        if (moQty != null ? !moQty.equals(that.moQty) : that.moQty != null) return false;
        if (moQtyUnit != null ? !moQtyUnit.equals(that.moQtyUnit) : that.moQtyUnit != null) return false;
        if (actionStatus != null ? !actionStatus.equals(that.actionStatus) : that.actionStatus != null) return false;
        if (drugDesc != null ? !drugDesc.equals(that.drugDesc) : that.drugDesc != null) return false;
        if (drugDescLong != null ? !drugDescLong.equals(that.drugDescLong) : that.drugDescLong != null) return false;
        if (freqCode != null ? !freqCode.equals(that.freqCode) : that.freqCode != null) return false;
        if (freq1 != null ? !freq1.equals(that.freq1) : that.freq1 != null) return false;
        if (freqText != null ? !freqText.equals(that.freqText) : that.freqText != null) return false;
        if (supFreqCode != null ? !supFreqCode.equals(that.supFreqCode) : that.supFreqCode != null) return false;
        if (supFreq1 != null ? !supFreq1.equals(that.supFreq1) : that.supFreq1 != null) return false;
        if (supFreq2 != null ? !supFreq2.equals(that.supFreq2) : that.supFreq2 != null) return false;
        if (supFreqText != null ? !supFreqText.equals(that.supFreqText) : that.supFreqText != null) return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;
        if (durationUnitDesc != null ? !durationUnitDesc.equals(that.durationUnitDesc) : that.durationUnitDesc != null)
            return false;
        if (durationMultiplier != null ? !durationMultiplier.equals(that.durationMultiplier) : that.durationMultiplier != null)
            return false;
        if (siteDesc != null ? !siteDesc.equals(that.siteDesc) : that.siteDesc != null) return false;
        if (specInstruct != null ? !specInstruct.equals(that.specInstruct) : that.specInstruct != null) return false;
        if (itemcode != null ? !itemcode.equals(that.itemcode) : that.itemcode != null) return false;
        if (itemUpdateTime != null ? !itemUpdateTime.equals(that.itemUpdateTime) : that.itemUpdateTime != null)
            return false;
        if (overridingRowNo != null ? !overridingRowNo.equals(that.overridingRowNo) : that.overridingRowNo != null)
            return false;
        if (allergen != null ? !allergen.equals(that.allergen) : that.allergen != null) return false;
        if (matchReactionType != null ? !matchReactionType.equals(that.matchReactionType) : that.matchReactionType != null)
            return false;
        if (overrideReason != null ? !overrideReason.equals(that.overrideReason) : that.overrideReason != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hospcode != null ? hospcode.hashCode() : 0;
        result = 31 * result + (ordNo != null ? ordNo.hashCode() : 0);
        result = 31 * result + (patientKey != null ? patientKey.hashCode() : 0);
        result = 31 * result + (episodeNo != null ? episodeNo.hashCode() : 0);
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (createUser != null ? createUser.hashCode() : 0);
        result = 31 * result + (createHosp != null ? createHosp.hashCode() : 0);
        result = 31 * result + (createRank != null ? createRank.hashCode() : 0);
        result = 31 * result + (createRankDesc != null ? createRankDesc.hashCode() : 0);
        result = 31 * result + (createDtm != null ? createDtm.hashCode() : 0);
        result = 31 * result + (updateUserId != null ? updateUserId.hashCode() : 0);
        result = 31 * result + (updateUser != null ? updateUser.hashCode() : 0);
        result = 31 * result + (updateHosp != null ? updateHosp.hashCode() : 0);
        result = 31 * result + (updateRank != null ? updateRank.hashCode() : 0);
        result = 31 * result + (updateRankDesc != null ? updateRankDesc.hashCode() : 0);
        result = 31 * result + (lastUpdDate != null ? lastUpdDate.hashCode() : 0);
        result = 31 * result + (refNo != null ? refNo.hashCode() : 0);
        result = 31 * result + (ordDate != null ? ordDate.hashCode() : 0);
        result = 31 * result + (episodeType != null ? episodeType.hashCode() : 0);
        result = 31 * result + (ipasWard != null ? ipasWard.hashCode() : 0);
        result = 31 * result + (bedNo != null ? bedNo.hashCode() : 0);
        result = 31 * result + (specialty != null ? specialty.hashCode() : 0);
        result = 31 * result + (tradeName != null ? tradeName.hashCode() : 0);
        result = 31 * result + (vtm != null ? vtm.hashCode() : 0);
        result = 31 * result + (aliasName != null ? aliasName.hashCode() : 0);
        result = 31 * result + (aliasNameType != null ? aliasNameType.hashCode() : 0);
        result = 31 * result + (manufacturer != null ? manufacturer.hashCode() : 0);
        result = 31 * result + (drugRoute != null ? drugRoute.hashCode() : 0);
        result = 31 * result + (regimenRoute != null ? regimenRoute.hashCode() : 0);
        result = 31 * result + (formDesc != null ? formDesc.hashCode() : 0);
        result = 31 * result + (doseFormExtraInfo != null ? doseFormExtraInfo.hashCode() : 0);
        result = 31 * result + (genericIndicator != null ? genericIndicator.hashCode() : 0);
        result = 31 * result + (strength != null ? strength.hashCode() : 0);
        result = 31 * result + (strengthLevelExtraInfo != null ? strengthLevelExtraInfo.hashCode() : 0);
        result = 31 * result + (strengthCompulsory != null ? strengthCompulsory.hashCode() : 0);
        result = 31 * result + (dangerdrug != null ? dangerdrug.hashCode() : 0);
        result = 31 * result + (nameDesc != null ? nameDesc.hashCode() : 0);
        result = 31 * result + (cmsItemNo != null ? cmsItemNo.hashCode() : 0);
        result = 31 * result + (orgItemNo != null ? orgItemNo.hashCode() : 0);
        result = 31 * result + (stepNo != null ? stepNo.hashCode() : 0);
        result = 31 * result + (multDoseNo != null ? multDoseNo.hashCode() : 0);
        result = 31 * result + (orderLineType != null ? orderLineType.hashCode() : 0);
        result = 31 * result + (dosage != null ? dosage.hashCode() : 0);
        result = 31 * result + (modu != null ? modu.hashCode() : 0);
        result = 31 * result + (prn != null ? prn.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (moQty != null ? moQty.hashCode() : 0);
        result = 31 * result + (moQtyUnit != null ? moQtyUnit.hashCode() : 0);
        result = 31 * result + (actionStatus != null ? actionStatus.hashCode() : 0);
        result = 31 * result + (drugDesc != null ? drugDesc.hashCode() : 0);
        result = 31 * result + (drugDescLong != null ? drugDescLong.hashCode() : 0);
        result = 31 * result + (freqCode != null ? freqCode.hashCode() : 0);
        result = 31 * result + (freq1 != null ? freq1.hashCode() : 0);
        result = 31 * result + (freqText != null ? freqText.hashCode() : 0);
        result = 31 * result + (supFreqCode != null ? supFreqCode.hashCode() : 0);
        result = 31 * result + (supFreq1 != null ? supFreq1.hashCode() : 0);
        result = 31 * result + (supFreq2 != null ? supFreq2.hashCode() : 0);
        result = 31 * result + (supFreqText != null ? supFreqText.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (durationUnitDesc != null ? durationUnitDesc.hashCode() : 0);
        result = 31 * result + (durationMultiplier != null ? durationMultiplier.hashCode() : 0);
        result = 31 * result + (siteDesc != null ? siteDesc.hashCode() : 0);
        result = 31 * result + (specInstruct != null ? specInstruct.hashCode() : 0);
        result = 31 * result + (itemcode != null ? itemcode.hashCode() : 0);
        result = 31 * result + (itemUpdateTime != null ? itemUpdateTime.hashCode() : 0);
        result = 31 * result + (overridingRowNo != null ? overridingRowNo.hashCode() : 0);
        result = 31 * result + (allergen != null ? allergen.hashCode() : 0);
        result = 31 * result + (matchReactionType != null ? matchReactionType.hashCode() : 0);
        result = 31 * result + (overrideReason != null ? overrideReason.hashCode() : 0);
        return result;
    }
}
*/
