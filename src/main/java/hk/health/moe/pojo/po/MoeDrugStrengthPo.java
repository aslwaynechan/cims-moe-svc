package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_DRUG_STRENGTH")
@IdClass(MoeDrugStrengthPoPK.class)
public class MoeDrugStrengthPo implements Serializable, Comparable<MoeDrugStrengthPo> {
    private static final long serialVersionUID = -1089404929912225511L;
    private String hkRegNo;
    private String strength;
    private String strengthLevelExtraInfo;
    private long rank;
    private String amp;
    private Long ampId;
    private String vmp;
    private Long vmpId;
    //private MoeDrugPo moeDrug;

    @Id
    @Column(name = "HK_REG_NO")
    public String getHkRegNo() {
        return hkRegNo;
    }

    public void setHkRegNo(String hkRegNo) {
        this.hkRegNo = hkRegNo;
    }

    @Basic
    @Column(name = "STRENGTH")
    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    @Basic
    @Column(name = "STRENGTH_LEVEL_EXTRA_INFO")
    public String getStrengthLevelExtraInfo() {
        return strengthLevelExtraInfo;
    }

    public void setStrengthLevelExtraInfo(String strengthLevelExtraInfo) {
        this.strengthLevelExtraInfo = strengthLevelExtraInfo;
    }

    @Id
    @Column(name = "RANK")
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @Basic
    @Column(name = "AMP")
    public String getAmp() {
        return amp;
    }

    public void setAmp(String amp) {
        this.amp = amp;
    }

    @Basic
    @Column(name = "AMP_ID")
    public Long getAmpId() {
        return ampId;
    }

    public void setAmpId(Long ampId) {
        this.ampId = ampId;
    }

    @Basic
    @Column(name = "VMP")
    public String getVmp() {
        return vmp;
    }

    public void setVmp(String vmp) {
        this.vmp = vmp;
    }

    @Basic
    @Column(name = "VMP_ID")
    public Long getVmpId() {
        return vmpId;
    }

/*    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hk_reg_no", nullable = false, insertable = false, updatable = false)
    public MoeDrugPo getMoeDrug() {
        return this.moeDrug;
    }

    public void setMoeDrug(MoeDrugPo moeDrug) {
        this.moeDrug = moeDrug;
    }*/

    public void setVmpId(Long vmpId) {
        this.vmpId = vmpId;
    }

    @Override
    public int compareTo(MoeDrugStrengthPo o) {
        long thisVal = this.getRank();
        long anotherVal = o.getRank();
        return (thisVal<anotherVal ? -1 : (thisVal==anotherVal ? 0 : 1));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDrugStrengthPo that = (MoeDrugStrengthPo) o;

        if (rank != that.rank) return false;
        if (hkRegNo != null ? !hkRegNo.equals(that.hkRegNo) : that.hkRegNo != null) return false;
        if (strength != null ? !strength.equals(that.strength) : that.strength != null) return false;
        if (strengthLevelExtraInfo != null ? !strengthLevelExtraInfo.equals(that.strengthLevelExtraInfo) : that.strengthLevelExtraInfo != null)
            return false;
        if (amp != null ? !amp.equals(that.amp) : that.amp != null) return false;
        if (ampId != null ? !ampId.equals(that.ampId) : that.ampId != null) return false;
        if (vmp != null ? !vmp.equals(that.vmp) : that.vmp != null) return false;
        if (vmpId != null ? !vmpId.equals(that.vmpId) : that.vmpId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hkRegNo != null ? hkRegNo.hashCode() : 0;
        result = 31 * result + (strength != null ? strength.hashCode() : 0);
        result = 31 * result + (strengthLevelExtraInfo != null ? strengthLevelExtraInfo.hashCode() : 0);
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        result = 31 * result + (amp != null ? amp.hashCode() : 0);
        result = 31 * result + (ampId != null ? ampId.hashCode() : 0);
        result = 31 * result + (vmp != null ? vmp.hashCode() : 0);
        result = 31 * result + (vmpId != null ? vmpId.hashCode() : 0);
        return result;
    }
}
