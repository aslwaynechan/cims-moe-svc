package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name = "MOE_MY_FAVOURITE_DETAIL")
@IdClass(MoeMyFavouriteDetailPoPK.class)
public class MoeMyFavouriteDetailPo implements Serializable, Comparable<MoeMyFavouriteDetailPo> {
    private static final long serialVersionUID = -1331392683305375133L;
    //private String myFavouriteId;
    private long itemNo;
    private String regimen;
    private String vtm;
    private String tradeName;
    private String genericIndicator;
    private String manufacturer;
    private String aliasName;
    private String nameType;
    private String orderLineType;
    private String dangerDrug;
    private Long formId;
    private String formEng;
    private String doseFormExtraInfo;
    private Long doseFormExtraInfoId;
    private Long routeId;
    private String routeEng;
    private Long freqId;
    private String freqCode;
    private String freqEng;
    private Long freqValue1;
    private String freqText;
    private Long siteId;
    private String siteEng;
    private String prn;
    private Long duration;
    private String durationUnit;
    private String screenDisplay;
    private BigDecimal dosage;
    private Long dosageUnitId;
    private String dosageUnit;
    private Long supplFreqId;
    private String supplFreqEng;
    private Long supplFreqValue1;
    private Long supplFreqValue2;
    private String supplFreqText;
    private String dayOfWeek;
    private Long moQty;
    private Long moQtyUnitId;
    private String moQtyUnit;
    private String specInstruct;
    private String actionStatus;
    private String preparation;
    private String drugRouteEng;
    private Long drugRouteId;
    private String strengthLevelExtraInfo;
    private String strengthCompulsory;
    private Long cycleMultiplier;
    private String capdSystem;
    private String capdCalcium;
    private String capdConc;
    private String capdBaseunit;
    private String capdTrueConc;
    private String createUserId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private Date hkmttUpdateDate;
    private MoeMyFavouriteHdrPo moeMyFavouriteHdr;
    private Set<MoeMyFavouriteMultDosePo> moeMyFavouriteMultDoses = new TreeSet<>();

    public MoeMyFavouriteDetailPo() {
    }

    public MoeMyFavouriteDetailPo(long itemNo,
                                  String createUserId,
                                  String createUser,
                                  String createHosp,
                                  String createRank,
                                  String createRankDesc) {
        this.itemNo = itemNo;
        this.createUserId = createUserId;
        this.createUser = createUser;
        this.createHosp = createHosp;
        this.createRank = createRank;
        this.createRankDesc = createRankDesc;
    }

/*    @Id
    @Column(name = "MY_FAVOURITE_ID")
    public String getMyFavouriteId() {
        return myFavouriteId;
    }

    public void setMyFavouriteId(String myFavouriteId) {
        this.myFavouriteId = myFavouriteId;
    }*/

    @Id
    @Column(name = "ITEM_NO")
    public long getItemNo() {
        return itemNo;
    }

    public void setItemNo(long itemNo) {
        this.itemNo = itemNo;
    }

    @Basic
    @Column(name = "REGIMEN")
    public String getRegimen() {
        return regimen;
    }

    public void setRegimen(String regimen) {
        this.regimen = regimen;
    }

    @Basic
    @Column(name = "VTM")
    public String getVtm() {
        return vtm;
    }

    public void setVtm(String vtm) {
        this.vtm = vtm;
    }

    @Basic
    @Column(name = "TRADE_NAME")
    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    @Basic
    @Column(name = "GENERIC_INDICATOR")
    public String getGenericIndicator() {
        return genericIndicator;
    }

    public void setGenericIndicator(String genericIndicator) {
        this.genericIndicator = genericIndicator;
    }

    @Basic
    @Column(name = "MANUFACTURER")
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Basic
    @Column(name = "ALIAS_NAME")
    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    @Basic
    @Column(name = "NAME_TYPE")
    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    @Basic
    @Column(name = "ORDER_LINE_TYPE")
    public String getOrderLineType() {
        return orderLineType;
    }

    public void setOrderLineType(String orderLineType) {
        this.orderLineType = orderLineType;
    }

    @Basic
    @Column(name = "DANGER_DRUG")
    public String getDangerDrug() {
        return dangerDrug;
    }

    public void setDangerDrug(String dangerDrug) {
        this.dangerDrug = dangerDrug;
    }

    @Basic
    @Column(name = "FORM_ID")
    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    @Basic
    @Column(name = "FORM_ENG")
    public String getFormEng() {
        return formEng;
    }

    public void setFormEng(String formEng) {
        this.formEng = formEng;
    }

    @Basic
    @Column(name = "DOSE_FORM_EXTRA_INFO")
    public String getDoseFormExtraInfo() {
        return doseFormExtraInfo;
    }

    public void setDoseFormExtraInfo(String doseFormExtraInfo) {
        this.doseFormExtraInfo = doseFormExtraInfo;
    }

    @Basic
    @Column(name = "DOSE_FORM_EXTRA_INFO_ID")
    public Long getDoseFormExtraInfoId() {
        return doseFormExtraInfoId;
    }

    public void setDoseFormExtraInfoId(Long doseFormExtraInfoId) {
        this.doseFormExtraInfoId = doseFormExtraInfoId;
    }

    @Basic
    @Column(name = "ROUTE_ID")
    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    @Basic
    @Column(name = "ROUTE_ENG")
    public String getRouteEng() {
        return routeEng;
    }

    public void setRouteEng(String routeEng) {
        this.routeEng = routeEng;
    }

    @Basic
    @Column(name = "FREQ_ID")
    public Long getFreqId() {
        return freqId;
    }

    public void setFreqId(Long freqId) {
        this.freqId = freqId;
    }

    @Basic
    @Column(name = "FREQ_CODE")
    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    @Basic
    @Column(name = "FREQ_ENG")
    public String getFreqEng() {
        return freqEng;
    }

    public void setFreqEng(String freqEng) {
        this.freqEng = freqEng;
    }

    @Basic
    @Column(name = "FREQ_VALUE_1")
    public Long getFreqValue1() {
        return freqValue1;
    }

    public void setFreqValue1(Long freqValue1) {
        this.freqValue1 = freqValue1;
    }

    @Basic
    @Column(name = "FREQ_TEXT")
    public String getFreqText() {
        return freqText;
    }

    public void setFreqText(String freqText) {
        this.freqText = freqText;
    }

    @Basic
    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    @Basic
    @Column(name = "SITE_ENG")
    public String getSiteEng() {
        return siteEng;
    }

    public void setSiteEng(String siteEng) {
        this.siteEng = siteEng;
    }

    @Basic
    @Column(name = "PRN")
    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    @Basic
    @Column(name = "DURATION")
    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    @Basic
    @Column(name = "DURATION_UNIT")
    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    @Basic
    @Column(name = "SCREEN_DISPLAY")
    public String getScreenDisplay() {
        return screenDisplay;
    }

    public void setScreenDisplay(String screenDisplay) {
        this.screenDisplay = screenDisplay;
    }

    @Basic
    @Column(name = "DOSAGE")
    public BigDecimal getDosage() {
        return dosage;
    }

    public void setDosage(BigDecimal dosage) {
        this.dosage = dosage;
    }

    @Basic
    @Column(name = "DOSAGE_UNIT_ID")
    public Long getDosageUnitId() {
        return dosageUnitId;
    }

    public void setDosageUnitId(Long dosageUnitId) {
        this.dosageUnitId = dosageUnitId;
    }

    @Basic
    @Column(name = "DOSAGE_UNIT")
    public String getDosageUnit() {
        return dosageUnit;
    }

    public void setDosageUnit(String dosageUnit) {
        this.dosageUnit = dosageUnit;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_ID")
    public Long getSupplFreqId() {
        return supplFreqId;
    }

    public void setSupplFreqId(Long supplFreqId) {
        this.supplFreqId = supplFreqId;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_ENG")
    public String getSupplFreqEng() {
        return supplFreqEng;
    }

    public void setSupplFreqEng(String supplFreqEng) {
        this.supplFreqEng = supplFreqEng;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_VALUE_1")
    public Long getSupplFreqValue1() {
        return supplFreqValue1;
    }

    public void setSupplFreqValue1(Long supplFreqValue1) {
        this.supplFreqValue1 = supplFreqValue1;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_VALUE_2")
    public Long getSupplFreqValue2() {
        return supplFreqValue2;
    }

    public void setSupplFreqValue2(Long supplFreqValue2) {
        this.supplFreqValue2 = supplFreqValue2;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_TEXT")
    public String getSupplFreqText() {
        return supplFreqText;
    }

    public void setSupplFreqText(String supplFreqText) {
        this.supplFreqText = supplFreqText;
    }

    @Basic
    @Column(name = "DAY_OF_WEEK")
    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    @Basic
    @Column(name = "MO_QTY")
    public Long getMoQty() {
        return moQty;
    }

    public void setMoQty(Long moQty) {
        this.moQty = moQty;
    }

    @Basic
    @Column(name = "MO_QTY_UNIT_ID")
    public Long getMoQtyUnitId() {
        return moQtyUnitId;
    }

    public void setMoQtyUnitId(Long moQtyUnitId) {
        this.moQtyUnitId = moQtyUnitId;
    }

    @Basic
    @Column(name = "MO_QTY_UNIT")
    public String getMoQtyUnit() {
        return moQtyUnit;
    }

    public void setMoQtyUnit(String moQtyUnit) {
        this.moQtyUnit = moQtyUnit;
    }

    @Basic
    @Column(name = "SPEC_INSTRUCT")
    public String getSpecInstruct() {
        return specInstruct;
    }

    public void setSpecInstruct(String specInstruct) {
        this.specInstruct = specInstruct;
    }

    @Basic
    @Column(name = "ACTION_STATUS")
    public String getActionStatus() {
        return actionStatus;
    }

    public void setActionStatus(String actionStatus) {
        this.actionStatus = actionStatus;
    }

    @Basic
    @Column(name = "PREPARATION")
    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }

    @Basic
    @Column(name = "DRUG_ROUTE_ENG")
    public String getDrugRouteEng() {
        return drugRouteEng;
    }

    public void setDrugRouteEng(String drugRouteEng) {
        this.drugRouteEng = drugRouteEng;
    }

    @Basic
    @Column(name = "DRUG_ROUTE_ID")
    public Long getDrugRouteId() {
        return drugRouteId;
    }

    public void setDrugRouteId(Long drugRouteId) {
        this.drugRouteId = drugRouteId;
    }

    @Basic
    @Column(name = "STRENGTH_LEVEL_EXTRA_INFO")
    public String getStrengthLevelExtraInfo() {
        return strengthLevelExtraInfo;
    }

    public void setStrengthLevelExtraInfo(String strengthLevelExtraInfo) {
        this.strengthLevelExtraInfo = strengthLevelExtraInfo;
    }

    @Basic
    @Column(name = "STRENGTH_COMPULSORY")
    public String getStrengthCompulsory() {
        return strengthCompulsory;
    }

    public void setStrengthCompulsory(String strengthCompulsory) {
        this.strengthCompulsory = strengthCompulsory;
    }

    @Basic
    @Column(name = "CYCLE_MULTIPLIER")
    public Long getCycleMultiplier() {
        return cycleMultiplier;
    }

    public void setCycleMultiplier(Long cycleMultiplier) {
        this.cycleMultiplier = cycleMultiplier;
    }

    @Basic
    @Column(name = "CAPD_SYSTEM")
    public String getCapdSystem() {
        return capdSystem;
    }

    public void setCapdSystem(String capdSystem) {
        this.capdSystem = capdSystem;
    }

    @Basic
    @Column(name = "CAPD_CALCIUM")
    public String getCapdCalcium() {
        return capdCalcium;
    }

    public void setCapdCalcium(String capdCalcium) {
        this.capdCalcium = capdCalcium;
    }

    @Basic
    @Column(name = "CAPD_CONC")
    public String getCapdConc() {
        return capdConc;
    }

    public void setCapdConc(String capdConc) {
        this.capdConc = capdConc;
    }

    @Basic
    @Column(name = "CAPD_BASEUNIT")
    public String getCapdBaseunit() {
        return capdBaseunit;
    }

    public void setCapdBaseunit(String capdBaseunit) {
        this.capdBaseunit = capdBaseunit;
    }

    @Basic
    @Column(name = "CAPD_TRUE_CONC")
    public String getCapdTrueConc() {
        return capdTrueConc;
    }

    public void setCapdTrueConc(String capdTrueConc) {
        this.capdTrueConc = capdTrueConc;
    }

    @Basic
    @Column(name = "CREATE_USER_ID")
    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    @Basic
    @Column(name = "CREATE_USER")
    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    @Basic
    @Column(name = "CREATE_HOSP")
    public String getCreateHosp() {
        return createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    @Basic
    @Column(name = "CREATE_RANK")
    public String getCreateRank() {
        return createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    @Basic
    @Column(name = "CREATE_RANK_DESC")
    public String getCreateRankDesc() {
        return createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    @Basic
    @Column(name = "UPDATE_USER_ID")
    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Basic
    @Column(name = "UPDATE_USER")
    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Basic
    @Column(name = "UPDATE_HOSP")
    public String getUpdateHosp() {
        return updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    @Basic
    @Column(name = "UPDATE_RANK")
    public String getUpdateRank() {
        return updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    @Basic
    @Column(name = "UPDATE_RANK_DESC")
    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    @Basic
    @Column(name = "HKMTT_UPDATE_DATE")
    public Date getHkmttUpdateDate() {
        return hkmttUpdateDate;
    }

    public void setHkmttUpdateDate(Date hkmttUpdateDate) {
        this.hkmttUpdateDate = hkmttUpdateDate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "my_favourite_id", nullable = false, insertable = false, updatable = false)
    public MoeMyFavouriteHdrPo getMoeMyFavouriteHdr() {
        return this.moeMyFavouriteHdr;
    }

    public void setMoeMyFavouriteHdr(MoeMyFavouriteHdrPo moeMyFavouriteHdr) {
        this.moeMyFavouriteHdr = moeMyFavouriteHdr;
    }


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moeMyFavouriteDetail",
            cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("stepNo asc")
    public Set<MoeMyFavouriteMultDosePo> getMoeMyFavouriteMultDoses() {
        return this.moeMyFavouriteMultDoses;
    }

    public void setMoeMyFavouriteMultDoses(Set<MoeMyFavouriteMultDosePo> moeMyFavouriteMultDoses) {
        this.moeMyFavouriteMultDoses = moeMyFavouriteMultDoses;
    }

    public void addMoeMyFavouriteMultDose(MoeMyFavouriteMultDosePo dose) {
        if (this.moeMyFavouriteMultDoses != null) {
            // Simon 20190722 start-- comment
            //dose.setMyFavouriteId(this.getMyFavouriteId());
            //dose.setItemNo(this.getItemNo());
            // Simon 20190722 end-- comment
            dose.setStepNo(this.moeMyFavouriteMultDoses.size() + 1L);
            dose.setMultDoseNo(this.moeMyFavouriteMultDoses.size() + 1L);
            dose.setMoeMyFavouriteDetail(this);
            this.moeMyFavouriteMultDoses.add(dose);
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeMyFavouriteDetailPo that = (MoeMyFavouriteDetailPo) o;

        if (itemNo != that.itemNo) return false;
        //if (myFavouriteId != null ? !myFavouriteId.equals(that.myFavouriteId) : that.myFavouriteId != null)
        //return false;
        if (regimen != null ? !regimen.equals(that.regimen) : that.regimen != null) return false;
        if (vtm != null ? !vtm.equals(that.vtm) : that.vtm != null) return false;
        if (tradeName != null ? !tradeName.equals(that.tradeName) : that.tradeName != null) return false;
        if (genericIndicator != null ? !genericIndicator.equals(that.genericIndicator) : that.genericIndicator != null)
            return false;
        if (manufacturer != null ? !manufacturer.equals(that.manufacturer) : that.manufacturer != null) return false;
        if (aliasName != null ? !aliasName.equals(that.aliasName) : that.aliasName != null) return false;
        if (nameType != null ? !nameType.equals(that.nameType) : that.nameType != null) return false;
        if (orderLineType != null ? !orderLineType.equals(that.orderLineType) : that.orderLineType != null)
            return false;
        if (dangerDrug != null ? !dangerDrug.equals(that.dangerDrug) : that.dangerDrug != null) return false;
        if (formId != null ? !formId.equals(that.formId) : that.formId != null) return false;
        if (formEng != null ? !formEng.equals(that.formEng) : that.formEng != null) return false;
        if (doseFormExtraInfo != null ? !doseFormExtraInfo.equals(that.doseFormExtraInfo) : that.doseFormExtraInfo != null)
            return false;
        if (doseFormExtraInfoId != null ? !doseFormExtraInfoId.equals(that.doseFormExtraInfoId) : that.doseFormExtraInfoId != null)
            return false;
        if (routeId != null ? !routeId.equals(that.routeId) : that.routeId != null) return false;
        if (routeEng != null ? !routeEng.equals(that.routeEng) : that.routeEng != null) return false;
        if (freqId != null ? !freqId.equals(that.freqId) : that.freqId != null) return false;
        if (freqCode != null ? !freqCode.equals(that.freqCode) : that.freqCode != null) return false;
        if (freqEng != null ? !freqEng.equals(that.freqEng) : that.freqEng != null) return false;
        if (freqValue1 != null ? !freqValue1.equals(that.freqValue1) : that.freqValue1 != null) return false;
        if (freqText != null ? !freqText.equals(that.freqText) : that.freqText != null) return false;
        if (siteId != null ? !siteId.equals(that.siteId) : that.siteId != null) return false;
        if (siteEng != null ? !siteEng.equals(that.siteEng) : that.siteEng != null) return false;
        if (prn != null ? !prn.equals(that.prn) : that.prn != null) return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;
        if (durationUnit != null ? !durationUnit.equals(that.durationUnit) : that.durationUnit != null) return false;
        if (screenDisplay != null ? !screenDisplay.equals(that.screenDisplay) : that.screenDisplay != null)
            return false;
        if (dosage != null ? !dosage.equals(that.dosage) : that.dosage != null) return false;
        if (dosageUnitId != null ? !dosageUnitId.equals(that.dosageUnitId) : that.dosageUnitId != null) return false;
        if (dosageUnit != null ? !dosageUnit.equals(that.dosageUnit) : that.dosageUnit != null) return false;
        if (supplFreqId != null ? !supplFreqId.equals(that.supplFreqId) : that.supplFreqId != null) return false;
        if (supplFreqEng != null ? !supplFreqEng.equals(that.supplFreqEng) : that.supplFreqEng != null) return false;
        if (supplFreqValue1 != null ? !supplFreqValue1.equals(that.supplFreqValue1) : that.supplFreqValue1 != null)
            return false;
        if (supplFreqValue2 != null ? !supplFreqValue2.equals(that.supplFreqValue2) : that.supplFreqValue2 != null)
            return false;
        if (supplFreqText != null ? !supplFreqText.equals(that.supplFreqText) : that.supplFreqText != null)
            return false;
        if (dayOfWeek != null ? !dayOfWeek.equals(that.dayOfWeek) : that.dayOfWeek != null) return false;
        if (moQty != null ? !moQty.equals(that.moQty) : that.moQty != null) return false;
        if (moQtyUnitId != null ? !moQtyUnitId.equals(that.moQtyUnitId) : that.moQtyUnitId != null) return false;
        if (moQtyUnit != null ? !moQtyUnit.equals(that.moQtyUnit) : that.moQtyUnit != null) return false;
        if (specInstruct != null ? !specInstruct.equals(that.specInstruct) : that.specInstruct != null) return false;
        if (actionStatus != null ? !actionStatus.equals(that.actionStatus) : that.actionStatus != null) return false;
        if (preparation != null ? !preparation.equals(that.preparation) : that.preparation != null) return false;
        if (drugRouteEng != null ? !drugRouteEng.equals(that.drugRouteEng) : that.drugRouteEng != null) return false;
        if (drugRouteId != null ? !drugRouteId.equals(that.drugRouteId) : that.drugRouteId != null) return false;
        if (strengthLevelExtraInfo != null ? !strengthLevelExtraInfo.equals(that.strengthLevelExtraInfo) : that.strengthLevelExtraInfo != null)
            return false;
        if (strengthCompulsory != null ? !strengthCompulsory.equals(that.strengthCompulsory) : that.strengthCompulsory != null)
            return false;
        if (cycleMultiplier != null ? !cycleMultiplier.equals(that.cycleMultiplier) : that.cycleMultiplier != null)
            return false;
        if (capdSystem != null ? !capdSystem.equals(that.capdSystem) : that.capdSystem != null) return false;
        if (capdCalcium != null ? !capdCalcium.equals(that.capdCalcium) : that.capdCalcium != null) return false;
        if (capdConc != null ? !capdConc.equals(that.capdConc) : that.capdConc != null) return false;
        if (capdBaseunit != null ? !capdBaseunit.equals(that.capdBaseunit) : that.capdBaseunit != null) return false;
        if (capdTrueConc != null ? !capdTrueConc.equals(that.capdTrueConc) : that.capdTrueConc != null) return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null) return false;
        if (createUser != null ? !createUser.equals(that.createUser) : that.createUser != null) return false;
        if (createHosp != null ? !createHosp.equals(that.createHosp) : that.createHosp != null) return false;
        if (createRank != null ? !createRank.equals(that.createRank) : that.createRank != null) return false;
        if (createRankDesc != null ? !createRankDesc.equals(that.createRankDesc) : that.createRankDesc != null)
            return false;
        if (updateUserId != null ? !updateUserId.equals(that.updateUserId) : that.updateUserId != null) return false;
        if (updateUser != null ? !updateUser.equals(that.updateUser) : that.updateUser != null) return false;
        if (updateHosp != null ? !updateHosp.equals(that.updateHosp) : that.updateHosp != null) return false;
        if (updateRank != null ? !updateRank.equals(that.updateRank) : that.updateRank != null) return false;
        if (updateRankDesc != null ? !updateRankDesc.equals(that.updateRankDesc) : that.updateRankDesc != null)
            return false;
        if (hkmttUpdateDate != null ? !hkmttUpdateDate.equals(that.hkmttUpdateDate) : that.hkmttUpdateDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        //int result = myFavouriteId != null ? myFavouriteId.hashCode() : 0;
        int result = 0;
        result = 31 * result + (int) (itemNo ^ (itemNo >>> 32));
        result = 31 * result + (regimen != null ? regimen.hashCode() : 0);
        result = 31 * result + (vtm != null ? vtm.hashCode() : 0);
        result = 31 * result + (tradeName != null ? tradeName.hashCode() : 0);
        result = 31 * result + (genericIndicator != null ? genericIndicator.hashCode() : 0);
        result = 31 * result + (manufacturer != null ? manufacturer.hashCode() : 0);
        result = 31 * result + (aliasName != null ? aliasName.hashCode() : 0);
        result = 31 * result + (nameType != null ? nameType.hashCode() : 0);
        result = 31 * result + (orderLineType != null ? orderLineType.hashCode() : 0);
        result = 31 * result + (dangerDrug != null ? dangerDrug.hashCode() : 0);
        result = 31 * result + (formId != null ? formId.hashCode() : 0);
        result = 31 * result + (formEng != null ? formEng.hashCode() : 0);
        result = 31 * result + (doseFormExtraInfo != null ? doseFormExtraInfo.hashCode() : 0);
        result = 31 * result + (doseFormExtraInfoId != null ? doseFormExtraInfoId.hashCode() : 0);
        result = 31 * result + (routeId != null ? routeId.hashCode() : 0);
        result = 31 * result + (routeEng != null ? routeEng.hashCode() : 0);
        result = 31 * result + (freqId != null ? freqId.hashCode() : 0);
        result = 31 * result + (freqCode != null ? freqCode.hashCode() : 0);
        result = 31 * result + (freqEng != null ? freqEng.hashCode() : 0);
        result = 31 * result + (freqValue1 != null ? freqValue1.hashCode() : 0);
        result = 31 * result + (freqText != null ? freqText.hashCode() : 0);
        result = 31 * result + (siteId != null ? siteId.hashCode() : 0);
        result = 31 * result + (siteEng != null ? siteEng.hashCode() : 0);
        result = 31 * result + (prn != null ? prn.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (durationUnit != null ? durationUnit.hashCode() : 0);
        result = 31 * result + (screenDisplay != null ? screenDisplay.hashCode() : 0);
        result = 31 * result + (dosage != null ? dosage.hashCode() : 0);
        result = 31 * result + (dosageUnitId != null ? dosageUnitId.hashCode() : 0);
        result = 31 * result + (dosageUnit != null ? dosageUnit.hashCode() : 0);
        result = 31 * result + (supplFreqId != null ? supplFreqId.hashCode() : 0);
        result = 31 * result + (supplFreqEng != null ? supplFreqEng.hashCode() : 0);
        result = 31 * result + (supplFreqValue1 != null ? supplFreqValue1.hashCode() : 0);
        result = 31 * result + (supplFreqValue2 != null ? supplFreqValue2.hashCode() : 0);
        result = 31 * result + (supplFreqText != null ? supplFreqText.hashCode() : 0);
        result = 31 * result + (dayOfWeek != null ? dayOfWeek.hashCode() : 0);
        result = 31 * result + (moQty != null ? moQty.hashCode() : 0);
        result = 31 * result + (moQtyUnitId != null ? moQtyUnitId.hashCode() : 0);
        result = 31 * result + (moQtyUnit != null ? moQtyUnit.hashCode() : 0);
        result = 31 * result + (specInstruct != null ? specInstruct.hashCode() : 0);
        result = 31 * result + (actionStatus != null ? actionStatus.hashCode() : 0);
        result = 31 * result + (preparation != null ? preparation.hashCode() : 0);
        result = 31 * result + (drugRouteEng != null ? drugRouteEng.hashCode() : 0);
        result = 31 * result + (drugRouteId != null ? drugRouteId.hashCode() : 0);
        result = 31 * result + (strengthLevelExtraInfo != null ? strengthLevelExtraInfo.hashCode() : 0);
        result = 31 * result + (strengthCompulsory != null ? strengthCompulsory.hashCode() : 0);
        result = 31 * result + (cycleMultiplier != null ? cycleMultiplier.hashCode() : 0);
        result = 31 * result + (capdSystem != null ? capdSystem.hashCode() : 0);
        result = 31 * result + (capdCalcium != null ? capdCalcium.hashCode() : 0);
        result = 31 * result + (capdConc != null ? capdConc.hashCode() : 0);
        result = 31 * result + (capdBaseunit != null ? capdBaseunit.hashCode() : 0);
        result = 31 * result + (capdTrueConc != null ? capdTrueConc.hashCode() : 0);
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (createUser != null ? createUser.hashCode() : 0);
        result = 31 * result + (createHosp != null ? createHosp.hashCode() : 0);
        result = 31 * result + (createRank != null ? createRank.hashCode() : 0);
        result = 31 * result + (createRankDesc != null ? createRankDesc.hashCode() : 0);
        result = 31 * result + (updateUserId != null ? updateUserId.hashCode() : 0);
        result = 31 * result + (updateUser != null ? updateUser.hashCode() : 0);
        result = 31 * result + (updateHosp != null ? updateHosp.hashCode() : 0);
        result = 31 * result + (updateRank != null ? updateRank.hashCode() : 0);
        result = 31 * result + (updateRankDesc != null ? updateRankDesc.hashCode() : 0);
        result = 31 * result + (hkmttUpdateDate != null ? hkmttUpdateDate.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(MoeMyFavouriteDetailPo o) {
        Long thisVal = this.getItemNo();
        Long anotherVal = o.getItemNo();
        if (thisVal == 0L) {
            return 1;
        }
        return (thisVal < anotherVal ? -1 : (thisVal.equals(anotherVal) ? 0 : 1));
    }
}
