package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import java.io.Serializable;

public class MoeMyFavouriteMultDosePoPK implements Serializable {
    private static final long serialVersionUID = -1295862428196106870L;
/*    private String myFavouriteId;
    private long itemNo;*/
    private long stepNo;
    private long multDoseNo;
    private MoeMyFavouriteDetailPo moeMyFavouriteDetail;

/*    @Column(name = "MY_FAVOURITE_ID")
    @Id
    public String getMyFavouriteId() {
        return myFavouriteId;
    }

    public void setMyFavouriteId(String myFavouriteId) {
        this.myFavouriteId = myFavouriteId;
    }

    @Column(name = "ITEM_NO")
    @Id
    public long getItemNo() {
        return itemNo;
    }

    public void setItemNo(long itemNo) {
        this.itemNo = itemNo;
    }*/

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "my_favourite_id", referencedColumnName = "my_favourite_id", nullable = false, insertable = false, updatable = false),
            @JoinColumn(name = "item_no", referencedColumnName = "item_no", nullable = false, insertable = false, updatable = false)})
    public MoeMyFavouriteDetailPo getMoeMyFavouriteDetail() {
        return this.moeMyFavouriteDetail;
    }

    public void setMoeMyFavouriteDetail(MoeMyFavouriteDetailPo moeMyFavouriteDetail) {
        this.moeMyFavouriteDetail = moeMyFavouriteDetail;
    }

    @Column(name = "STEP_NO")
    @Id
    public long getStepNo() {
        return stepNo;
    }

    public void setStepNo(long stepNo) {
        this.stepNo = stepNo;
    }

    @Column(name = "MULT_DOSE_NO")
    @Id
    public long getMultDoseNo() {
        return multDoseNo;
    }

    public void setMultDoseNo(long multDoseNo) {
        this.multDoseNo = multDoseNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeMyFavouriteMultDosePoPK that = (MoeMyFavouriteMultDosePoPK) o;

        //if (itemNo != that.itemNo) return false;
        if (stepNo != that.stepNo) return false;
        if (multDoseNo != that.multDoseNo) return false;
        //if (myFavouriteId != null ? !myFavouriteId.equals(that.myFavouriteId) : that.myFavouriteId != null)
            //return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        //int result = myFavouriteId != null ? myFavouriteId.hashCode() : 0;
        //result = 31 * result + (int) (itemNo ^ (itemNo >>> 32));
        result = 31 * result + (int) (stepNo ^ (stepNo >>> 32));
        result = 31 * result + (int) (multDoseNo ^ (multDoseNo >>> 32));
        return result;
    }
}
