package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_BASE_UNIT_TERM_ID_MAP")
@IdClass(MoeBaseUnitTermIdMapPoPK.class)
public class MoeBaseUnitTermIdMapPo  implements Serializable {
    private static final long serialVersionUID = -8635345798551261903L;
    private String baseUnit;
    private String ehrBaseUnitDesc;
    private long ehrTermId;
    private String defaultMap;
    private String ehrStatus;

    @Id
    @Column(name = "BASE_UNIT")
    public String getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(String baseUnit) {
        this.baseUnit = baseUnit;
    }

    @Basic
    @Column(name = "EHR_BASE_UNIT_DESC")
    public String getEhrBaseUnitDesc() {
        return ehrBaseUnitDesc;
    }

    public void setEhrBaseUnitDesc(String ehrBaseUnitDesc) {
        this.ehrBaseUnitDesc = ehrBaseUnitDesc;
    }

    @Id
    @Column(name = "EHR_TERM_ID")
    public long getEhrTermId() {
        return ehrTermId;
    }

    public void setEhrTermId(long ehrTermId) {
        this.ehrTermId = ehrTermId;
    }

    @Basic
    @Column(name = "DEFAULT_MAP")
    public String getDefaultMap() {
        return defaultMap;
    }

    public void setDefaultMap(String defaultMap) {
        this.defaultMap = defaultMap;
    }

    @Basic
    @Column(name = "EHR_STATUS")
    public String getEhrStatus() {
        return ehrStatus;
    }

    public void setEhrStatus(String ehrStatus) {
        this.ehrStatus = ehrStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeBaseUnitTermIdMapPo that = (MoeBaseUnitTermIdMapPo) o;

        if (ehrTermId != that.ehrTermId) return false;
        if (baseUnit != null ? !baseUnit.equals(that.baseUnit) : that.baseUnit != null) return false;
        if (ehrBaseUnitDesc != null ? !ehrBaseUnitDesc.equals(that.ehrBaseUnitDesc) : that.ehrBaseUnitDesc != null)
            return false;
        if (defaultMap != null ? !defaultMap.equals(that.defaultMap) : that.defaultMap != null) return false;
        if (ehrStatus != null ? !ehrStatus.equals(that.ehrStatus) : that.ehrStatus != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = baseUnit != null ? baseUnit.hashCode() : 0;
        result = 31 * result + (ehrBaseUnitDesc != null ? ehrBaseUnitDesc.hashCode() : 0);
        result = 31 * result + (int) (ehrTermId ^ (ehrTermId >>> 32));
        result = 31 * result + (defaultMap != null ? defaultMap.hashCode() : 0);
        result = 31 * result + (ehrStatus != null ? ehrStatus.hashCode() : 0);
        return result;
    }
}
