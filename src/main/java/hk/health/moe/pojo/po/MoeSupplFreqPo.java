package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name = "MOE_SUPPL_FREQ")
public class MoeSupplFreqPo implements Serializable {
    private static final long serialVersionUID = -3818620603669760771L;
    private long supplFreqId;
    private String supplFreqEng;
    private String supplFreqChi;
    private String prefixSupplFreqEng;
    private String prefixSupplFreqChi;
    //private String regimenType;
    private long upperLimit;
    private long lowerLimit;
    private BigDecimal supplFreqMultiplier;
    private String useInputValue;
    private String useInputMethod;
    private String perDurationMultiplier;
    private long rank;
    private MoeRegimenPo moeRegimen;
    private Set<MoeSupplFreqDescPo> moeSupplFreqDescs = new TreeSet<>();
    private Set<MoeSupplFreqSelectionPo> moeSupplFreqSelections = new TreeSet<>();

    @Id
    @Column(name = "SUPPL_FREQ_ID")
    public long getSupplFreqId() {
        return supplFreqId;
    }

    public void setSupplFreqId(long supplFreqId) {
        this.supplFreqId = supplFreqId;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_ENG")
    public String getSupplFreqEng() {
        return supplFreqEng;
    }

    public void setSupplFreqEng(String supplFreqEng) {
        this.supplFreqEng = supplFreqEng;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_CHI")
    public String getSupplFreqChi() {
        return supplFreqChi;
    }

    public void setSupplFreqChi(String supplFreqChi) {
        this.supplFreqChi = supplFreqChi;
    }

    @Basic
    @Column(name = "PREFIX_SUPPL_FREQ_ENG")
    public String getPrefixSupplFreqEng() {
        return prefixSupplFreqEng;
    }

    public void setPrefixSupplFreqEng(String prefixSupplFreqEng) {
        this.prefixSupplFreqEng = prefixSupplFreqEng;
    }

    @Basic
    @Column(name = "PREFIX_SUPPL_FREQ_CHI")
    public String getPrefixSupplFreqChi() {
        return prefixSupplFreqChi;
    }

    public void setPrefixSupplFreqChi(String prefixSupplFreqChi) {
        this.prefixSupplFreqChi = prefixSupplFreqChi;
    }

/*    @Basic
    @Column(name = "REGIMEN_TYPE")
    public String getRegimenType() {
        return regimenType;
    }

    public void setRegimenType(String regimenType) {
        this.regimenType = regimenType;
    }*/

    @Basic
    @Column(name = "UPPER_LIMIT")
    public long getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(long upperLimit) {
        this.upperLimit = upperLimit;
    }

    @Basic
    @Column(name = "LOWER_LIMIT")
    public long getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(long lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_MULTIPLIER")
    public BigDecimal getSupplFreqMultiplier() {
        return supplFreqMultiplier;
    }

    public void setSupplFreqMultiplier(BigDecimal supplFreqMultiplier) {
        this.supplFreqMultiplier = supplFreqMultiplier;
    }

    @Basic
    @Column(name = "USE_INPUT_VALUE")
    public String getUseInputValue() {
        return useInputValue;
    }

    public void setUseInputValue(String useInputValue) {
        this.useInputValue = useInputValue;
    }

    @Basic
    @Column(name = "USE_INPUT_METHOD")
    public String getUseInputMethod() {
        return useInputMethod;
    }

    public void setUseInputMethod(String useInputMethod) {
        this.useInputMethod = useInputMethod;
    }

    @Basic
    @Column(name = "PER_DURATION_MULTIPLIER")
    public String getPerDurationMultiplier() {
        return perDurationMultiplier;
    }

    public void setPerDurationMultiplier(String perDurationMultiplier) {
        this.perDurationMultiplier = perDurationMultiplier;
    }

    @Basic
    @Column(name = "RANK")
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "regimen_type", nullable = false)
    public MoeRegimenPo getMoeRegimen() {
        return this.moeRegimen;
    }

    public void setMoeRegimen(MoeRegimenPo moeRegimen) {
        this.moeRegimen = moeRegimen;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moeSupplFreq")
    public Set<MoeSupplFreqDescPo> getMoeSupplFreqDescs() {
        return this.moeSupplFreqDescs;
    }

    public void setMoeSupplFreqDescs(Set<MoeSupplFreqDescPo> moeSupplFreqDescs) {
        this.moeSupplFreqDescs = moeSupplFreqDescs;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moeSupplFreq")
    public Set<MoeSupplFreqSelectionPo> getMoeSupplFreqSelections() {
        return this.moeSupplFreqSelections;
    }

    public void setMoeSupplFreqSelections(
            Set<MoeSupplFreqSelectionPo> moeSupplFreqSelections) {
        this.moeSupplFreqSelections = moeSupplFreqSelections;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeSupplFreqPo that = (MoeSupplFreqPo) o;

        if (supplFreqId != that.supplFreqId) return false;
        if (upperLimit != that.upperLimit) return false;
        if (lowerLimit != that.lowerLimit) return false;
        if (supplFreqMultiplier != that.supplFreqMultiplier) return false;
        if (rank != that.rank) return false;
        if (supplFreqEng != null ? !supplFreqEng.equals(that.supplFreqEng) : that.supplFreqEng != null) return false;
        if (supplFreqChi != null ? !supplFreqChi.equals(that.supplFreqChi) : that.supplFreqChi != null) return false;
        if (prefixSupplFreqEng != null ? !prefixSupplFreqEng.equals(that.prefixSupplFreqEng) : that.prefixSupplFreqEng != null)
            return false;
        if (prefixSupplFreqChi != null ? !prefixSupplFreqChi.equals(that.prefixSupplFreqChi) : that.prefixSupplFreqChi != null)
            return false;
        //if (regimenType != null ? !regimenType.equals(that.regimenType) : that.regimenType != null) return false;
        if (useInputValue != null ? !useInputValue.equals(that.useInputValue) : that.useInputValue != null)
            return false;
        if (useInputMethod != null ? !useInputMethod.equals(that.useInputMethod) : that.useInputMethod != null)
            return false;
        if (perDurationMultiplier != null ? !perDurationMultiplier.equals(that.perDurationMultiplier) : that.perDurationMultiplier != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (supplFreqId ^ (supplFreqId >>> 32));
        result = 31 * result + (supplFreqEng != null ? supplFreqEng.hashCode() : 0);
        result = 31 * result + (supplFreqChi != null ? supplFreqChi.hashCode() : 0);
        result = 31 * result + (prefixSupplFreqEng != null ? prefixSupplFreqEng.hashCode() : 0);
        result = 31 * result + (prefixSupplFreqChi != null ? prefixSupplFreqChi.hashCode() : 0);
        //result = 31 * result + (regimenType != null ? regimenType.hashCode() : 0);
        result = 31 * result + (int) (upperLimit ^ (upperLimit >>> 32));
        result = 31 * result + (int) (lowerLimit ^ (lowerLimit >>> 32));
        result = 31 * result + (supplFreqMultiplier != null ? supplFreqMultiplier.hashCode() : 0);
        result = 31 * result + (useInputValue != null ? useInputValue.hashCode() : 0);
        result = 31 * result + (useInputMethod != null ? useInputMethod.hashCode() : 0);
        result = 31 * result + (perDurationMultiplier != null ? perDurationMultiplier.hashCode() : 0);
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        return result;
    }
}
