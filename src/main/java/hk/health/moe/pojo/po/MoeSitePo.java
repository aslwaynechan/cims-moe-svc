package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MOE_SITE")
public class MoeSitePo implements Serializable {
    private static final long serialVersionUID = -8726901073817013596L;
    private long siteId;
    private String siteEng;
    private String siteChi;
    private Long siteMultiplier;
    private long rank;
    private Set<MoeRouteSiteMapPo> moeRouteSiteMaps = new HashSet<>();

    @Id
    @Column(name = "SITE_ID")
    public long getSiteId() {
        return siteId;
    }

    public void setSiteId(long siteId) {
        this.siteId = siteId;
    }

    @Basic
    @Column(name = "SITE_ENG")
    public String getSiteEng() {
        return siteEng;
    }

    public void setSiteEng(String siteEng) {
        this.siteEng = siteEng;
    }

    @Basic
    @Column(name = "SITE_CHI")
    public String getSiteChi() {
        return siteChi;
    }

    public void setSiteChi(String siteChi) {
        this.siteChi = siteChi;
    }

    @Basic
    @Column(name = "SITE_MULTIPLIER")
    public Long getSiteMultiplier() {
        return siteMultiplier;
    }

    public void setSiteMultiplier(Long siteMultiplier) {
        this.siteMultiplier = siteMultiplier;
    }

    @Basic
    @Column(name = "RANK")
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moeSite")
    public Set<MoeRouteSiteMapPo> getMoeRouteSiteMaps() {
        return this.moeRouteSiteMaps;
    }

    public void setMoeRouteSiteMaps(Set<MoeRouteSiteMapPo> moeRouteSiteMaps) {
        this.moeRouteSiteMaps = moeRouteSiteMaps;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeSitePo moeSitePo = (MoeSitePo) o;

        if (siteId != moeSitePo.siteId) return false;
        if (rank != moeSitePo.rank) return false;
        if (siteEng != null ? !siteEng.equals(moeSitePo.siteEng) : moeSitePo.siteEng != null) return false;
        if (siteChi != null ? !siteChi.equals(moeSitePo.siteChi) : moeSitePo.siteChi != null) return false;
        if (siteMultiplier != null ? !siteMultiplier.equals(moeSitePo.siteMultiplier) : moeSitePo.siteMultiplier != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (siteId ^ (siteId >>> 32));
        result = 31 * result + (siteEng != null ? siteEng.hashCode() : 0);
        result = 31 * result + (siteChi != null ? siteChi.hashCode() : 0);
        result = 31 * result + (siteMultiplier != null ? siteMultiplier.hashCode() : 0);
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        return result;
    }
}
