package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_DRUG_DISPLAY_NAME_TYPE")
public class MoeDrugDisplayNameTypePo implements Serializable {
    private static final long serialVersionUID = -8478274419508023141L;
    private String nameDesc;
    private String nameType;

    @Basic
    @Column(name = "NAME_DESC")
    public String getNameDesc() {
        return nameDesc;
    }

    public void setNameDesc(String nameDesc) {
        this.nameDesc = nameDesc;
    }

    @Id
    @Column(name = "NAME_TYPE")
    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDrugDisplayNameTypePo that = (MoeDrugDisplayNameTypePo) o;

        if (nameDesc != null ? !nameDesc.equals(that.nameDesc) : that.nameDesc != null) return false;
        if (nameType != null ? !nameType.equals(that.nameType) : that.nameType != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nameDesc != null ? nameDesc.hashCode() : 0;
        result = 31 * result + (nameType != null ? nameType.hashCode() : 0);
        return result;
    }
}
