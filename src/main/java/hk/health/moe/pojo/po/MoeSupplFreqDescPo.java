package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_SUPPL_FREQ_DESC")
public class MoeSupplFreqDescPo implements Serializable, Comparable<MoeSupplFreqDescPo> {
    private static final long serialVersionUID = -199829469164056879L;
    private long supplFreqDescId;
    //private long supplFreqId;
    private Long supplFreqSelectionValue;
    private String freqCode;
    private Long supplFreq1;
    private String displayWithFreq;
    private String supplFreqDescEng;
    private String supplFreqDescChi;
    private long rank;
    private MoeSupplFreqPo moeSupplFreq;

    @Id
    @Column(name = "SUPPL_FREQ_DESC_ID")
    public long getSupplFreqDescId() {
        return supplFreqDescId;
    }

    public void setSupplFreqDescId(long supplFreqDescId) {
        this.supplFreqDescId = supplFreqDescId;
    }

/*    @Basic
    @Column(name = "SUPPL_FREQ_ID")
    public long getSupplFreqId() {
        return supplFreqId;
    }

    public void setSupplFreqId(long supplFreqId) {
        this.supplFreqId = supplFreqId;
    }*/

    @Basic
    @Column(name = "SUPPL_FREQ_SELECTION_VALUE")
    public Long getSupplFreqSelectionValue() {
        return supplFreqSelectionValue;
    }

    public void setSupplFreqSelectionValue(Long supplFreqSelectionValue) {
        this.supplFreqSelectionValue = supplFreqSelectionValue;
    }

    @Basic
    @Column(name = "FREQ_CODE")
    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    @Basic
    @Column(name = "SUPPL_FREQ1")
    public Long getSupplFreq1() {
        return supplFreq1;
    }

    public void setSupplFreq1(Long supplFreq1) {
        this.supplFreq1 = supplFreq1;
    }

    @Basic
    @Column(name = "DISPLAY_WITH_FREQ")
    public String getDisplayWithFreq() {
        return displayWithFreq;
    }

    public void setDisplayWithFreq(String displayWithFreq) {
        this.displayWithFreq = displayWithFreq;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_DESC_ENG")
    public String getSupplFreqDescEng() {
        return supplFreqDescEng;
    }

    public void setSupplFreqDescEng(String supplFreqDescEng) {
        this.supplFreqDescEng = supplFreqDescEng;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_DESC_CHI")
    public String getSupplFreqDescChi() {
        return supplFreqDescChi;
    }

    public void setSupplFreqDescChi(String supplFreqDescChi) {
        this.supplFreqDescChi = supplFreqDescChi;
    }

    @Basic
    @Column(name = "RANK")
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "suppl_freq_id", nullable = false)
    public MoeSupplFreqPo getMoeSupplFreq() {
        return this.moeSupplFreq;
    }

    public void setMoeSupplFreq(MoeSupplFreqPo moeSupplFreq) {
        this.moeSupplFreq = moeSupplFreq;
    }

    @Override
    public int compareTo(MoeSupplFreqDescPo o) {
        long thisVal = this.rank;
        long anotherVal = o.getRank();
        return (thisVal<anotherVal ? -1 : (thisVal==anotherVal ? 0 : 1));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeSupplFreqDescPo that = (MoeSupplFreqDescPo) o;

        if (supplFreqDescId != that.supplFreqDescId) return false;
        //if (supplFreqId != that.supplFreqId) return false;
        if (rank != that.rank) return false;
        if (supplFreqSelectionValue != null ? !supplFreqSelectionValue.equals(that.supplFreqSelectionValue) : that.supplFreqSelectionValue != null)
            return false;
        if (freqCode != null ? !freqCode.equals(that.freqCode) : that.freqCode != null) return false;
        if (supplFreq1 != null ? !supplFreq1.equals(that.supplFreq1) : that.supplFreq1 != null) return false;
        if (displayWithFreq != null ? !displayWithFreq.equals(that.displayWithFreq) : that.displayWithFreq != null)
            return false;
        if (supplFreqDescEng != null ? !supplFreqDescEng.equals(that.supplFreqDescEng) : that.supplFreqDescEng != null)
            return false;
        if (supplFreqDescChi != null ? !supplFreqDescChi.equals(that.supplFreqDescChi) : that.supplFreqDescChi != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (supplFreqDescId ^ (supplFreqDescId >>> 32));
        //result = 31 * result + (int) (supplFreqId ^ (supplFreqId >>> 32));
        result = 31 * result + (supplFreqSelectionValue != null ? supplFreqSelectionValue.hashCode() : 0);
        result = 31 * result + (freqCode != null ? freqCode.hashCode() : 0);
        result = 31 * result + (supplFreq1 != null ? supplFreq1.hashCode() : 0);
        result = 31 * result + (displayWithFreq != null ? displayWithFreq.hashCode() : 0);
        result = 31 * result + (supplFreqDescEng != null ? supplFreqDescEng.hashCode() : 0);
        result = 31 * result + (supplFreqDescChi != null ? supplFreqDescChi.hashCode() : 0);
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        return result;
    }
}
