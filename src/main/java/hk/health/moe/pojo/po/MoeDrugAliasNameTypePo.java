package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MOE_DRUG_ALIAS_NAME_TYPE")
public class MoeDrugAliasNameTypePo implements Serializable {
    private static final long serialVersionUID = -5772635421883589463L;
    private String aliasNameType;
    private String aliasNameDesc;
    private Set<MoeDrugAliasNamePo> moeDrugAliasNames = new HashSet<>();

    @Id
    @Column(name = "ALIAS_NAME_TYPE")
    public String getAliasNameType() {
        return aliasNameType;
    }

    public void setAliasNameType(String aliasNameType) {
        this.aliasNameType = aliasNameType;
    }

    @Basic
    @Column(name = "ALIAS_NAME_DESC")
    public String getAliasNameDesc() {
        return aliasNameDesc;
    }

    public void setAliasNameDesc(String aliasNameDesc) {
        this.aliasNameDesc = aliasNameDesc;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moeDrugAliasNameType")
    public Set<MoeDrugAliasNamePo> getMoeDrugAliasNames() {
        return this.moeDrugAliasNames;
    }

    public void setMoeDrugAliasNames(Set<MoeDrugAliasNamePo> moeDrugAliasNames) {
        this.moeDrugAliasNames = moeDrugAliasNames;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDrugAliasNameTypePo that = (MoeDrugAliasNameTypePo) o;

        if (aliasNameType != null ? !aliasNameType.equals(that.aliasNameType) : that.aliasNameType != null)
            return false;
        if (aliasNameDesc != null ? !aliasNameDesc.equals(that.aliasNameDesc) : that.aliasNameDesc != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = aliasNameType != null ? aliasNameType.hashCode() : 0;
        result = 31 * result + (aliasNameDesc != null ? aliasNameDesc.hashCode() : 0);
        return result;
    }
}
