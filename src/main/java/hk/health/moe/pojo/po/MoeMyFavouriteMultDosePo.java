package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "MOE_MY_FAVOURITE_MULT_DOSE")
@IdClass(MoeMyFavouriteMultDosePoPK.class)
public class MoeMyFavouriteMultDosePo implements Serializable, Comparable<MoeMyFavouriteMultDosePo> {
    private static final long serialVersionUID = 1281299220147956366L;
    /*    private String myFavouriteId;
        private long itemNo;*/
    private long stepNo;
    private long multDoseNo;
    private BigDecimal dosage;
    private String dosageUnit;
    private Long freqId;
    private String freqCode;
    private String freqEng;
    private Long freqValue1;
    private String freqText;
    private Long duration;
    private String durationUnit;
    private String prn;
    private String dayOfWeek;
    private String siteEng;
    private String supSiteEng;
    private Date startDate;
    private Date endDate;
    private Long moQty;
    private String moQtyUnit;
    private Long supplFreqId;
    private String supplFreqEng;
    private Long supplFreqValue1;
    private Long supplFreqValue2;
    private String supplFreqText;
    private String capdSystem;
    private String capdCalcium;
    private String capdConc;
    private String capdBaseunit;
    private String trueConc;
    private String capdChargeInd;
    private Long capdChargeableUnit;
    private BigDecimal capdChargeableAmount;
    private String confirmCapdChargeInd;
    private Long confirmCapdChargeUnit;
    private MoeMyFavouriteDetailPo moeMyFavouriteDetail;

/*    @Id
    @Column(name = "MY_FAVOURITE_ID")
    public String getMyFavouriteId() {
        return myFavouriteId;
    }

    public void setMyFavouriteId(String myFavouriteId) {
        this.myFavouriteId = myFavouriteId;
    }

    @Id
    @Column(name = "ITEM_NO")
    public long getItemNo() {
        return itemNo;
    }

    public void setItemNo(long itemNo) {
        this.itemNo = itemNo;
    }*/

    @Id
    @Column(name = "STEP_NO")
    public long getStepNo() {
        return stepNo;
    }

    public void setStepNo(long stepNo) {
        this.stepNo = stepNo;
    }

    @Id
    @Column(name = "MULT_DOSE_NO")
    public long getMultDoseNo() {
        return multDoseNo;
    }

    public void setMultDoseNo(long multDoseNo) {
        this.multDoseNo = multDoseNo;
    }

    @Basic
    @Column(name = "DOSAGE")
    public BigDecimal getDosage() {
        return dosage;
    }

    public void setDosage(BigDecimal dosage) {
        this.dosage = dosage;
    }

    @Basic
    @Column(name = "DOSAGE_UNIT")
    public String getDosageUnit() {
        return dosageUnit;
    }

    public void setDosageUnit(String dosageUnit) {
        this.dosageUnit = dosageUnit;
    }

    @Basic
    @Column(name = "FREQ_ID")
    public Long getFreqId() {
        return freqId;
    }

    public void setFreqId(Long freqId) {
        this.freqId = freqId;
    }

    @Basic
    @Column(name = "FREQ_CODE")
    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    @Basic
    @Column(name = "FREQ_ENG")
    public String getFreqEng() {
        return freqEng;
    }

    public void setFreqEng(String freqEng) {
        this.freqEng = freqEng;
    }

    @Basic
    @Column(name = "FREQ_VALUE_1")
    public Long getFreqValue1() {
        return freqValue1;
    }

    public void setFreqValue1(Long freqValue1) {
        this.freqValue1 = freqValue1;
    }

    @Basic
    @Column(name = "FREQ_TEXT")
    public String getFreqText() {
        return freqText;
    }

    public void setFreqText(String freqText) {
        this.freqText = freqText;
    }

    @Basic
    @Column(name = "DURATION")
    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    @Basic
    @Column(name = "DURATION_UNIT")
    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    @Basic
    @Column(name = "PRN")
    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    @Basic
    @Column(name = "DAY_OF_WEEK")
    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    @Basic
    @Column(name = "SITE_ENG")
    public String getSiteEng() {
        return siteEng;
    }

    public void setSiteEng(String siteEng) {
        this.siteEng = siteEng;
    }

    @Basic
    @Column(name = "SUP_SITE_ENG")
    public String getSupSiteEng() {
        return supSiteEng;
    }

    public void setSupSiteEng(String supSiteEng) {
        this.supSiteEng = supSiteEng;
    }

    @Basic
    @Column(name = "START_DATE")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "END_DATE")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "MO_QTY")
    public Long getMoQty() {
        return moQty;
    }

    public void setMoQty(Long moQty) {
        this.moQty = moQty;
    }

    @Basic
    @Column(name = "MO_QTY_UNIT")
    public String getMoQtyUnit() {
        return moQtyUnit;
    }

    public void setMoQtyUnit(String moQtyUnit) {
        this.moQtyUnit = moQtyUnit;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_ID")
    public Long getSupplFreqId() {
        return supplFreqId;
    }

    public void setSupplFreqId(Long supplFreqId) {
        this.supplFreqId = supplFreqId;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_ENG")
    public String getSupplFreqEng() {
        return supplFreqEng;
    }

    public void setSupplFreqEng(String supplFreqEng) {
        this.supplFreqEng = supplFreqEng;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_VALUE_1")
    public Long getSupplFreqValue1() {
        return supplFreqValue1;
    }

    public void setSupplFreqValue1(Long supplFreqValue1) {
        this.supplFreqValue1 = supplFreqValue1;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_VALUE_2")
    public Long getSupplFreqValue2() {
        return supplFreqValue2;
    }

    public void setSupplFreqValue2(Long supplFreqValue2) {
        this.supplFreqValue2 = supplFreqValue2;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_TEXT")
    public String getSupplFreqText() {
        return supplFreqText;
    }

    public void setSupplFreqText(String supplFreqText) {
        this.supplFreqText = supplFreqText;
    }

    @Basic
    @Column(name = "CAPD_SYSTEM")
    public String getCapdSystem() {
        return capdSystem;
    }

    public void setCapdSystem(String capdSystem) {
        this.capdSystem = capdSystem;
    }

    @Basic
    @Column(name = "CAPD_CALCIUM")
    public String getCapdCalcium() {
        return capdCalcium;
    }

    public void setCapdCalcium(String capdCalcium) {
        this.capdCalcium = capdCalcium;
    }

    @Basic
    @Column(name = "CAPD_CONC")
    public String getCapdConc() {
        return capdConc;
    }

    public void setCapdConc(String capdConc) {
        this.capdConc = capdConc;
    }

    @Basic
    @Column(name = "CAPD_BASEUNIT")
    public String getCapdBaseunit() {
        return capdBaseunit;
    }

    public void setCapdBaseunit(String capdBaseunit) {
        this.capdBaseunit = capdBaseunit;
    }

    @Basic
    @Column(name = "TRUE_CONC")
    public String getTrueConc() {
        return trueConc;
    }

    public void setTrueConc(String trueConc) {
        this.trueConc = trueConc;
    }

    @Basic
    @Column(name = "CAPD_CHARGE_IND")
    public String getCapdChargeInd() {
        return capdChargeInd;
    }

    public void setCapdChargeInd(String capdChargeInd) {
        this.capdChargeInd = capdChargeInd;
    }

    @Basic
    @Column(name = "CAPD_CHARGEABLE_UNIT")
    public Long getCapdChargeableUnit() {
        return capdChargeableUnit;
    }

    public void setCapdChargeableUnit(Long capdChargeableUnit) {
        this.capdChargeableUnit = capdChargeableUnit;
    }

    @Basic
    @Column(name = "CAPD_CHARGEABLE_AMOUNT")
    public BigDecimal getCapdChargeableAmount() {
        return capdChargeableAmount;
    }

    public void setCapdChargeableAmount(BigDecimal capdChargeableAmount) {
        this.capdChargeableAmount = capdChargeableAmount;
    }

    @Basic
    @Column(name = "CONFIRM_CAPD_CHARGE_IND")
    public String getConfirmCapdChargeInd() {
        return confirmCapdChargeInd;
    }

    public void setConfirmCapdChargeInd(String confirmCapdChargeInd) {
        this.confirmCapdChargeInd = confirmCapdChargeInd;
    }

    @Basic
    @Column(name = "CONFIRM_CAPD_CHARGE_UNIT")
    public Long getConfirmCapdChargeUnit() {
        return confirmCapdChargeUnit;
    }

    public void setConfirmCapdChargeUnit(Long confirmCapdChargeUnit) {
        this.confirmCapdChargeUnit = confirmCapdChargeUnit;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "my_favourite_id", referencedColumnName = "my_favourite_id", nullable = false, insertable = false, updatable = false),
            @JoinColumn(name = "item_no", referencedColumnName = "item_no", nullable = false, insertable = false, updatable = false)})
    public MoeMyFavouriteDetailPo getMoeMyFavouriteDetail() {
        return this.moeMyFavouriteDetail;
    }

    public void setMoeMyFavouriteDetail(MoeMyFavouriteDetailPo moeMyFavouriteDetail) {
        this.moeMyFavouriteDetail = moeMyFavouriteDetail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeMyFavouriteMultDosePo that = (MoeMyFavouriteMultDosePo) o;

        //if (itemNo != that.itemNo) return false;
        if (stepNo != that.stepNo) return false;
        if (multDoseNo != that.multDoseNo) return false;
        //if (myFavouriteId != null ? !myFavouriteId.equals(that.myFavouriteId) : that.myFavouriteId != null)
        //return false;
        if (dosage != null ? !dosage.equals(that.dosage) : that.dosage != null) return false;
        if (dosageUnit != null ? !dosageUnit.equals(that.dosageUnit) : that.dosageUnit != null) return false;
        if (freqId != null ? !freqId.equals(that.freqId) : that.freqId != null) return false;
        if (freqCode != null ? !freqCode.equals(that.freqCode) : that.freqCode != null) return false;
        if (freqEng != null ? !freqEng.equals(that.freqEng) : that.freqEng != null) return false;
        if (freqValue1 != null ? !freqValue1.equals(that.freqValue1) : that.freqValue1 != null) return false;
        if (freqText != null ? !freqText.equals(that.freqText) : that.freqText != null) return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;
        if (durationUnit != null ? !durationUnit.equals(that.durationUnit) : that.durationUnit != null) return false;
        if (prn != null ? !prn.equals(that.prn) : that.prn != null) return false;
        if (dayOfWeek != null ? !dayOfWeek.equals(that.dayOfWeek) : that.dayOfWeek != null) return false;
        if (siteEng != null ? !siteEng.equals(that.siteEng) : that.siteEng != null) return false;
        if (supSiteEng != null ? !supSiteEng.equals(that.supSiteEng) : that.supSiteEng != null) return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;
        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
        if (moQty != null ? !moQty.equals(that.moQty) : that.moQty != null) return false;
        if (moQtyUnit != null ? !moQtyUnit.equals(that.moQtyUnit) : that.moQtyUnit != null) return false;
        if (supplFreqId != null ? !supplFreqId.equals(that.supplFreqId) : that.supplFreqId != null) return false;
        if (supplFreqEng != null ? !supplFreqEng.equals(that.supplFreqEng) : that.supplFreqEng != null) return false;
        if (supplFreqValue1 != null ? !supplFreqValue1.equals(that.supplFreqValue1) : that.supplFreqValue1 != null)
            return false;
        if (supplFreqValue2 != null ? !supplFreqValue2.equals(that.supplFreqValue2) : that.supplFreqValue2 != null)
            return false;
        if (supplFreqText != null ? !supplFreqText.equals(that.supplFreqText) : that.supplFreqText != null)
            return false;
        if (capdSystem != null ? !capdSystem.equals(that.capdSystem) : that.capdSystem != null) return false;
        if (capdCalcium != null ? !capdCalcium.equals(that.capdCalcium) : that.capdCalcium != null) return false;
        if (capdConc != null ? !capdConc.equals(that.capdConc) : that.capdConc != null) return false;
        if (capdBaseunit != null ? !capdBaseunit.equals(that.capdBaseunit) : that.capdBaseunit != null) return false;
        if (trueConc != null ? !trueConc.equals(that.trueConc) : that.trueConc != null) return false;
        if (capdChargeInd != null ? !capdChargeInd.equals(that.capdChargeInd) : that.capdChargeInd != null)
            return false;
        if (capdChargeableUnit != null ? !capdChargeableUnit.equals(that.capdChargeableUnit) : that.capdChargeableUnit != null)
            return false;
        if (capdChargeableAmount != null ? !capdChargeableAmount.equals(that.capdChargeableAmount) : that.capdChargeableAmount != null)
            return false;
        if (confirmCapdChargeInd != null ? !confirmCapdChargeInd.equals(that.confirmCapdChargeInd) : that.confirmCapdChargeInd != null)
            return false;
        if (confirmCapdChargeUnit != null ? !confirmCapdChargeUnit.equals(that.confirmCapdChargeUnit) : that.confirmCapdChargeUnit != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        //int result = myFavouriteId != null ? myFavouriteId.hashCode() : 0;
        //result = 31 * result + (int) (itemNo ^ (itemNo >>> 32));
        result = 31 * result + (int) (stepNo ^ (stepNo >>> 32));
        result = 31 * result + (int) (multDoseNo ^ (multDoseNo >>> 32));
        result = 31 * result + (dosage != null ? dosage.hashCode() : 0);
        result = 31 * result + (dosageUnit != null ? dosageUnit.hashCode() : 0);
        result = 31 * result + (freqId != null ? freqId.hashCode() : 0);
        result = 31 * result + (freqCode != null ? freqCode.hashCode() : 0);
        result = 31 * result + (freqEng != null ? freqEng.hashCode() : 0);
        result = 31 * result + (freqValue1 != null ? freqValue1.hashCode() : 0);
        result = 31 * result + (freqText != null ? freqText.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (durationUnit != null ? durationUnit.hashCode() : 0);
        result = 31 * result + (prn != null ? prn.hashCode() : 0);
        result = 31 * result + (dayOfWeek != null ? dayOfWeek.hashCode() : 0);
        result = 31 * result + (siteEng != null ? siteEng.hashCode() : 0);
        result = 31 * result + (supSiteEng != null ? supSiteEng.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (moQty != null ? moQty.hashCode() : 0);
        result = 31 * result + (moQtyUnit != null ? moQtyUnit.hashCode() : 0);
        result = 31 * result + (supplFreqId != null ? supplFreqId.hashCode() : 0);
        result = 31 * result + (supplFreqEng != null ? supplFreqEng.hashCode() : 0);
        result = 31 * result + (supplFreqValue1 != null ? supplFreqValue1.hashCode() : 0);
        result = 31 * result + (supplFreqValue2 != null ? supplFreqValue2.hashCode() : 0);
        result = 31 * result + (supplFreqText != null ? supplFreqText.hashCode() : 0);
        result = 31 * result + (capdSystem != null ? capdSystem.hashCode() : 0);
        result = 31 * result + (capdCalcium != null ? capdCalcium.hashCode() : 0);
        result = 31 * result + (capdConc != null ? capdConc.hashCode() : 0);
        result = 31 * result + (capdBaseunit != null ? capdBaseunit.hashCode() : 0);
        result = 31 * result + (trueConc != null ? trueConc.hashCode() : 0);
        result = 31 * result + (capdChargeInd != null ? capdChargeInd.hashCode() : 0);
        result = 31 * result + (capdChargeableUnit != null ? capdChargeableUnit.hashCode() : 0);
        result = 31 * result + (capdChargeableAmount != null ? capdChargeableAmount.hashCode() : 0);
        result = 31 * result + (confirmCapdChargeInd != null ? confirmCapdChargeInd.hashCode() : 0);
        result = 31 * result + (confirmCapdChargeUnit != null ? confirmCapdChargeUnit.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(MoeMyFavouriteMultDosePo o) {
        long thisVal = this.getStepNo();
        long anotherVal = o.getStepNo();
        if (thisVal == 0L) {
            return -1;
        }
        return (thisVal < anotherVal ? -1 : (thisVal == anotherVal ? 0 : 1));
    }

}
