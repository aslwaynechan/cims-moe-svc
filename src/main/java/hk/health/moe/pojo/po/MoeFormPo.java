package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MOE_FORM")
public class MoeFormPo implements Serializable {
    private static final long serialVersionUID = -5072161126290139849L;
    private long formId;
    private String formEng;
    private String formChi;
    private BigDecimal formMultiplier;
    private Set<MoeFormRouteMapPo> moeFormRouteMaps = new HashSet<>();

    @Id
    @Column(name = "FORM_ID")
    public long getFormId() {
        return formId;
    }

    public void setFormId(long formId) {
        this.formId = formId;
    }

    @Basic
    @Column(name = "FORM_ENG")
    public String getFormEng() {
        return formEng;
    }

    public void setFormEng(String formEng) {
        this.formEng = formEng;
    }

    @Basic
    @Column(name = "FORM_CHI")
    public String getFormChi() {
        return formChi;
    }

    public void setFormChi(String formChi) {
        this.formChi = formChi;
    }

    @Basic
    @Column(name = "FORM_MULTIPLIER")
    public BigDecimal getFormMultiplier() {
        return formMultiplier;
    }

    public void setFormMultiplier(BigDecimal formMultiplier) {
        this.formMultiplier = formMultiplier;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moeForm")
    public Set<MoeFormRouteMapPo> getMoeFormRouteMaps() {
        return this.moeFormRouteMaps;
    }

    public void setMoeFormRouteMaps(Set<MoeFormRouteMapPo> moeFormRouteMaps) {
        this.moeFormRouteMaps = moeFormRouteMaps;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeFormPo moeFormPo = (MoeFormPo) o;

        if (formId != moeFormPo.formId) return false;
        if (formEng != null ? !formEng.equals(moeFormPo.formEng) : moeFormPo.formEng != null) return false;
        if (formChi != null ? !formChi.equals(moeFormPo.formChi) : moeFormPo.formChi != null) return false;
        if (formMultiplier != null ? !formMultiplier.equals(moeFormPo.formMultiplier) : moeFormPo.formMultiplier != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (formId ^ (formId >>> 32));
        result = 31 * result + (formEng != null ? formEng.hashCode() : 0);
        result = 31 * result + (formChi != null ? formChi.hashCode() : 0);
        result = 31 * result + (formMultiplier != null ? formMultiplier.hashCode() : 0);
        return result;
    }
}
