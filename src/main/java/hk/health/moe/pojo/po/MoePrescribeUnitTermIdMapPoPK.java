package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoePrescribeUnitTermIdMapPoPK implements Serializable {
    private static final long serialVersionUID = -6994248352048404603L;
    private String prescribeUnit;
    private long ehrTermId;

    @Column(name = "PRESCRIBE_UNIT")
    @Id
    public String getPrescribeUnit() {
        return prescribeUnit;
    }

    public void setPrescribeUnit(String prescribeUnit) {
        this.prescribeUnit = prescribeUnit;
    }

    @Column(name = "EHR_TERM_ID")
    @Id
    public long getEhrTermId() {
        return ehrTermId;
    }

    public void setEhrTermId(long ehrTermId) {
        this.ehrTermId = ehrTermId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoePrescribeUnitTermIdMapPoPK that = (MoePrescribeUnitTermIdMapPoPK) o;

        if (ehrTermId != that.ehrTermId) return false;
        if (prescribeUnit != null ? !prescribeUnit.equals(that.prescribeUnit) : that.prescribeUnit != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = prescribeUnit != null ? prescribeUnit.hashCode() : 0;
        result = 31 * result + (int) (ehrTermId ^ (ehrTermId >>> 32));
        return result;
    }
}
