package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_DRUG_TERM_DESC_ID_MAP")
public class MoeDrugTermDescIdMapPo implements Serializable {
    private static final long serialVersionUID = 1682275921479157384L;
    private String termDesc;
    private long termId;
    private String termStatus;
    private String descType;

    @Basic
    @Column(name = "TERM_DESC")
    public String getTermDesc() {
        return termDesc;
    }

    public void setTermDesc(String termDesc) {
        this.termDesc = termDesc;
    }

    @Id
    @Column(name = "TERM_ID")
    public long getTermId() {
        return termId;
    }

    public void setTermId(long termId) {
        this.termId = termId;
    }

    @Basic
    @Column(name = "TERM_STATUS")
    public String getTermStatus() {
        return termStatus;
    }

    public void setTermStatus(String termStatus) {
        this.termStatus = termStatus;
    }

    @Basic
    @Column(name = "DESC_TYPE")
    public String getDescType() {
        return descType;
    }

    public void setDescType(String descType) {
        this.descType = descType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDrugTermDescIdMapPo that = (MoeDrugTermDescIdMapPo) o;

        if (termId != that.termId) return false;
        if (termDesc != null ? !termDesc.equals(that.termDesc) : that.termDesc != null) return false;
        if (termStatus != null ? !termStatus.equals(that.termStatus) : that.termStatus != null) return false;
        if (descType != null ? !descType.equals(that.descType) : that.descType != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = termDesc != null ? termDesc.hashCode() : 0;
        result = 31 * result + (int) (termId ^ (termId >>> 32));
        result = 31 * result + (termStatus != null ? termStatus.hashCode() : 0);
        result = 31 * result + (descType != null ? descType.hashCode() : 0);
        return result;
    }
}
