package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_DRUG_ALIAS_NAME")
public class MoeDrugAliasNamePo implements Serializable {
    private static final long serialVersionUID = -7344162842978925558L;
    private String aliasNameId;
    private String hkRegNo;
    private String aliasName;
    //private String aliasNameType;
    private MoeDrugAliasNameTypePo moeDrugAliasNameType;
    //private MoeDrugPo moeDrug;

    @Id
    @Column(name = "ALIAS_NAME_ID")
    public String getAliasNameId() {
        return aliasNameId;
    }

    public void setAliasNameId(String aliasNameId) {
        this.aliasNameId = aliasNameId;
    }

    @Basic
    @Column(name = "HK_REG_NO")
    public String getHkRegNo() {
        return hkRegNo;
    }

    public void setHkRegNo(String hkRegNo) {
        this.hkRegNo = hkRegNo;
    }

    @Basic
    @Column(name = "ALIAS_NAME")
    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

/*    @Basic
    @Column(name = "ALIAS_NAME_TYPE")
    public String getAliasNameType() {
        return aliasNameType;
    }

    public void setAliasNameType(String aliasNameType) {
        this.aliasNameType = aliasNameType;
    }*/

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "alias_name_type", nullable = false)
    public MoeDrugAliasNameTypePo getMoeDrugAliasNameType() {
        return this.moeDrugAliasNameType;
    }

    public void setMoeDrugAliasNameType(MoeDrugAliasNameTypePo moeDrugAliasNameType) {
        this.moeDrugAliasNameType = moeDrugAliasNameType;
    }

/*    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hk_reg_no", nullable = false)
    public MoeDrugPo getMoeDrug() {
        return this.moeDrug;
    }

    public void setMoeDrug(MoeDrugPo moeDrug) {
        this.moeDrug = moeDrug;
    }*/


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDrugAliasNamePo that = (MoeDrugAliasNamePo) o;

        if (aliasNameId != null ? !aliasNameId.equals(that.aliasNameId) : that.aliasNameId != null) return false;
        if (hkRegNo != null ? !hkRegNo.equals(that.hkRegNo) : that.hkRegNo != null) return false;
        if (aliasName != null ? !aliasName.equals(that.aliasName) : that.aliasName != null) return false;
        //if (aliasNameType != null ? !aliasNameType.equals(that.aliasNameType) : that.aliasNameType != null)
            //return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = aliasNameId != null ? aliasNameId.hashCode() : 0;
        result = 31 * result + (hkRegNo != null ? hkRegNo.hashCode() : 0);
        result = 31 * result + (aliasName != null ? aliasName.hashCode() : 0);
        //result = 31 * result + (aliasNameType != null ? aliasNameType.hashCode() : 0);
        return result;
    }
}
