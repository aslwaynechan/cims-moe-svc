package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_PRN_TERM_ID_MAP")
@IdClass(MoePrnTermIdMapPoPK.class)
public class MoePrnTermIdMapPo implements Serializable {
    private static final long serialVersionUID = -2080374057800476971L;
    private String prn;
    private String ehrPrnDesc;
    private long ehrTermId;
    private String defaultMap;
    private String ehrStatus;

    @Id
    @Column(name = "PRN")
    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    @Basic
    @Column(name = "EHR_PRN_DESC")
    public String getEhrPrnDesc() {
        return ehrPrnDesc;
    }

    public void setEhrPrnDesc(String ehrPrnDesc) {
        this.ehrPrnDesc = ehrPrnDesc;
    }

    @Id
    @Column(name = "EHR_TERM_ID")
    public long getEhrTermId() {
        return ehrTermId;
    }

    public void setEhrTermId(long ehrTermId) {
        this.ehrTermId = ehrTermId;
    }

    @Basic
    @Column(name = "DEFAULT_MAP")
    public String getDefaultMap() {
        return defaultMap;
    }

    public void setDefaultMap(String defaultMap) {
        this.defaultMap = defaultMap;
    }

    @Basic
    @Column(name = "EHR_STATUS")
    public String getEhrStatus() {
        return ehrStatus;
    }

    public void setEhrStatus(String ehrStatus) {
        this.ehrStatus = ehrStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoePrnTermIdMapPo that = (MoePrnTermIdMapPo) o;

        if (ehrTermId != that.ehrTermId) return false;
        if (prn != null ? !prn.equals(that.prn) : that.prn != null) return false;
        if (ehrPrnDesc != null ? !ehrPrnDesc.equals(that.ehrPrnDesc) : that.ehrPrnDesc != null) return false;
        if (defaultMap != null ? !defaultMap.equals(that.defaultMap) : that.defaultMap != null) return false;
        if (ehrStatus != null ? !ehrStatus.equals(that.ehrStatus) : that.ehrStatus != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = prn != null ? prn.hashCode() : 0;
        result = 31 * result + (ehrPrnDesc != null ? ehrPrnDesc.hashCode() : 0);
        result = 31 * result + (int) (ehrTermId ^ (ehrTermId >>> 32));
        result = 31 * result + (defaultMap != null ? defaultMap.hashCode() : 0);
        result = 31 * result + (ehrStatus != null ? ehrStatus.hashCode() : 0);
        return result;
    }
}
