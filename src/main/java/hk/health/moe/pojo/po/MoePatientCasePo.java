package hk.health.moe.pojo.po;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MOE_PATIENT_CASE")
public class MoePatientCasePo implements Serializable {
    private static final long serialVersionUID = -4140736581903967344L;
    private String moeCaseNo;
    private String caseRefNo;
    private Set<MoeEhrOrderPo> moeEhrOrders = new HashSet<>();

    @GeneratedValue(generator="moeCaseNoGenerator")
    @GenericGenerator(name="moeCaseNoGenerator", strategy = "uuid")
    @Id
    @Column(name = "MOE_CASE_NO")
    public String getMoeCaseNo() {
        return moeCaseNo;
    }

    public void setMoeCaseNo(String moeCaseNo) {
        this.moeCaseNo = moeCaseNo;
    }

    @Basic
    @Column(name = "CASE_REF_NO")
    public String getCaseRefNo() {
        return caseRefNo;
    }

    public void setCaseRefNo(String caseRefNo) {
        this.caseRefNo = caseRefNo;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moePatientCase")
    public Set<MoeEhrOrderPo> getMoeEhrOrders() {
        return this.moeEhrOrders;
    }

    public void setMoeEhrOrders(Set<MoeEhrOrderPo> moeEhrOrders) {
        this.moeEhrOrders = moeEhrOrders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoePatientCasePo that = (MoePatientCasePo) o;

        if (moeCaseNo != null ? !moeCaseNo.equals(that.moeCaseNo) : that.moeCaseNo != null) return false;
        if (caseRefNo != null ? !caseRefNo.equals(that.caseRefNo) : that.caseRefNo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = moeCaseNo != null ? moeCaseNo.hashCode() : 0;
        result = 31 * result + (caseRefNo != null ? caseRefNo.hashCode() : 0);
        return result;
    }
}
