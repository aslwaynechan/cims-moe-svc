/*
package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "MOE_AUDIT_LOG")
public class MoeAuditLogPo {
    private String actionLogId;
    private Long logStatus;
    private String logTypeId;
    private String recordId;
    private String loginId;
    private String patientId;
    private Date actionDatetime;
    private String logonTrailId;
    private String workstationId;
    private String workstationIp;
    private String logDescription;
    private String caseNo;

    @Id
    @Column(name = "ACTION_LOG_ID")
    public String getActionLogId() {
        return actionLogId;
    }

    public void setActionLogId(String actionLogId) {
        this.actionLogId = actionLogId;
    }

    @Basic
    @Column(name = "LOG_STATUS")
    public Long getLogStatus() {
        return logStatus;
    }

    public void setLogStatus(Long logStatus) {
        this.logStatus = logStatus;
    }

    @Basic
    @Column(name = "LOG_TYPE_ID")
    public String getLogTypeId() {
        return logTypeId;
    }

    public void setLogTypeId(String logTypeId) {
        this.logTypeId = logTypeId;
    }

    @Basic
    @Column(name = "RECORD_ID")
    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    @Basic
    @Column(name = "LOGIN_ID")
    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    @Basic
    @Column(name = "PATIENT_ID")
    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    @Basic
    @Column(name = "ACTION_DATETIME")
    public Date getActionDatetime() {
        return actionDatetime;
    }

    public void setActionDatetime(Date actionDatetime) {
        this.actionDatetime = actionDatetime;
    }

    @Basic
    @Column(name = "LOGON_TRAIL_ID")
    public String getLogonTrailId() {
        return logonTrailId;
    }

    public void setLogonTrailId(String logonTrailId) {
        this.logonTrailId = logonTrailId;
    }

    @Basic
    @Column(name = "WORKSTATION_ID")
    public String getWorkstationId() {
        return workstationId;
    }

    public void setWorkstationId(String workstationId) {
        this.workstationId = workstationId;
    }

    @Basic
    @Column(name = "WORKSTATION_IP")
    public String getWorkstationIp() {
        return workstationIp;
    }

    public void setWorkstationIp(String workstationIp) {
        this.workstationIp = workstationIp;
    }

    @Basic
    @Column(name = "LOG_DESCRIPTION")
    public String getLogDescription() {
        return logDescription;
    }

    public void setLogDescription(String logDescription) {
        this.logDescription = logDescription;
    }

    @Basic
    @Column(name = "CASE_NO")
    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeAuditLogPo that = (MoeAuditLogPo) o;

        if (actionLogId != null ? !actionLogId.equals(that.actionLogId) : that.actionLogId != null) return false;
        if (logStatus != null ? !logStatus.equals(that.logStatus) : that.logStatus != null) return false;
        if (logTypeId != null ? !logTypeId.equals(that.logTypeId) : that.logTypeId != null) return false;
        if (recordId != null ? !recordId.equals(that.recordId) : that.recordId != null) return false;
        if (loginId != null ? !loginId.equals(that.loginId) : that.loginId != null) return false;
        if (patientId != null ? !patientId.equals(that.patientId) : that.patientId != null) return false;
        if (actionDatetime != null ? !actionDatetime.equals(that.actionDatetime) : that.actionDatetime != null)
            return false;
        if (logonTrailId != null ? !logonTrailId.equals(that.logonTrailId) : that.logonTrailId != null) return false;
        if (workstationId != null ? !workstationId.equals(that.workstationId) : that.workstationId != null)
            return false;
        if (workstationIp != null ? !workstationIp.equals(that.workstationIp) : that.workstationIp != null)
            return false;
        if (logDescription != null ? !logDescription.equals(that.logDescription) : that.logDescription != null)
            return false;
        if (caseNo != null ? !caseNo.equals(that.caseNo) : that.caseNo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = actionLogId != null ? actionLogId.hashCode() : 0;
        result = 31 * result + (logStatus != null ? logStatus.hashCode() : 0);
        result = 31 * result + (logTypeId != null ? logTypeId.hashCode() : 0);
        result = 31 * result + (recordId != null ? recordId.hashCode() : 0);
        result = 31 * result + (loginId != null ? loginId.hashCode() : 0);
        result = 31 * result + (patientId != null ? patientId.hashCode() : 0);
        result = 31 * result + (actionDatetime != null ? actionDatetime.hashCode() : 0);
        result = 31 * result + (logonTrailId != null ? logonTrailId.hashCode() : 0);
        result = 31 * result + (workstationId != null ? workstationId.hashCode() : 0);
        result = 31 * result + (workstationIp != null ? workstationIp.hashCode() : 0);
        result = 31 * result + (logDescription != null ? logDescription.hashCode() : 0);
        result = 31 * result + (caseNo != null ? caseNo.hashCode() : 0);
        return result;
    }
}
*/
