package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "MOE_COMMON_DOSAGE")
public class MoeCommonDosagePo implements Serializable {
    private static final long serialVersionUID = 2110976506857097524L;
    private long commonDosageId;
    private String hkRegNo;
    private String localDrugId;
    //private String dosageType;
    //private String regimenType;
    private BigDecimal dosage;
    private Long dosageUnitId;
    private String dosageUnit;
    //private long freqId;
    private String freqCode;
    private String freqEng;
    private Long freq1;
    private String prn;
    private Long supplFreqId;
    private String supplFreqEng;
    private Long supplFreq1;
    private Long supplFreq2;
    private String owner;
    private String suspend;
    private String createUserId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private Date createDtm;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private Date updateDtm;
    private long version;
    private Long routeId;
    private String routeEng;
    private MoeRegimenPo moeRegimen;
    private MoeFreqPo moeFreq;
    private MoeCommonDosageTypePo moeCommonDosageType;

    public MoeCommonDosagePo() {
    }

    public MoeCommonDosagePo(MoeCommonDosagePo record) {
        this.commonDosageId = record.getCommonDosageId();
        this.moeRegimen = record.getMoeRegimen();
        this.moeFreq = record.getMoeFreq();
        this.hkRegNo = record.getHkRegNo();
        this.moeCommonDosageType = record.getMoeCommonDosageType();
        this.localDrugId = record.getLocalDrugId();
        this.dosage = record.getDosage();
        this.dosageUnitId = record.getDosageUnitId();
        this.dosageUnit = record.getDosageUnit();
        this.freqCode = record.getFreqCode();
        this.freqEng = record.getFreqEng();
        this.freq1 = record.getFreq1();
        this.prn = record.getPrn();
        this.supplFreqId = record.getSupplFreqId();
        this.supplFreqEng = record.getSupplFreqEng();
        this.supplFreq1 = record.getSupplFreq1();
        this.supplFreq2 = record.getSupplFreq2();
        this.owner = record.getOwner();
        this.suspend = record.getSuspend();
        this.createUserId = record.getCreateUserId();
        this.createUser = record.getCreateUser();
        this.createHosp = record.getCreateHosp();
        this.createRank = record.getCreateRank();
        this.createRankDesc = record.getCreateRankDesc();
        this.createDtm = record.getCreateDtm();
        this.updateUserId = record.getUpdateUserId();
        this.updateUser = record.getUpdateUser();
        this.updateHosp = record.getUpdateHosp();
        this.updateRank = record.getUpdateRank();
        this.updateRankDesc = record.getUpdateRankDesc();
        this.updateDtm = record.getUpdateDtm();
        this.routeId = record.getRouteId();
        this.routeEng = record.getRouteEng();
    }

    @Id
    @Column(name = "COMMON_DOSAGE_ID")
    @SequenceGenerator(name = "SEQ_COMMON_DOSAGE_ID", sequenceName = "SEQ_COMMON_DOSAGE_ID", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_COMMON_DOSAGE_ID")
    public long getCommonDosageId() {
        return commonDosageId;
    }

    public void setCommonDosageId(long commonDosageId) {
        this.commonDosageId = commonDosageId;
    }

    @Basic
    @Column(name = "HK_REG_NO")
    public String getHkRegNo() {
        return hkRegNo;
    }

    public void setHkRegNo(String hkRegNo) {
        this.hkRegNo = hkRegNo;
    }

    @Basic
    @Column(name = "LOCAL_DRUG_ID")
    public String getLocalDrugId() {
        return localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        this.localDrugId = localDrugId;
    }

/*    @Basic
    @Column(name = "DOSAGE_TYPE")
    public String getDosageType() {
        return dosageType;
    }

    public void setDosageType(String dosageType) {
        this.dosageType = dosageType;
    }*/

/*    @Basic
    @Column(name = "REGIMEN_TYPE")
    public String getRegimenType() {
        return regimenType;
    }

    public void setRegimenType(String regimenType) {
        this.regimenType = regimenType;
    }*/

    @Basic
    @Column(name = "DOSAGE")
    public BigDecimal getDosage() {
        return dosage;
    }

    public void setDosage(BigDecimal dosage) {
        this.dosage = dosage;
    }

    @Basic
    @Column(name = "DOSAGE_UNIT_ID")
    public Long getDosageUnitId() {
        return dosageUnitId;
    }

    public void setDosageUnitId(Long dosageUnitId) {
        this.dosageUnitId = dosageUnitId;
    }

    @Basic
    @Column(name = "DOSAGE_UNIT")
    public String getDosageUnit() {
        return dosageUnit;
    }

    public void setDosageUnit(String dosageUnit) {
        this.dosageUnit = dosageUnit;
    }

/*    @Basic
    @Column(name = "FREQ_ID")
    public long getFreqId() {
        return freqId;
    }

    public void setFreqId(long freqId) {
        this.freqId = freqId;
    }*/

    @Basic
    @Column(name = "FREQ_CODE")
    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    @Basic
    @Column(name = "FREQ_ENG")
    public String getFreqEng() {
        return freqEng;
    }

    public void setFreqEng(String freqEng) {
        this.freqEng = freqEng;
    }

    @Basic
    @Column(name = "FREQ1")
    public Long getFreq1() {
        return freq1;
    }

    public void setFreq1(Long freq1) {
        this.freq1 = freq1;
    }

    @Basic
    @Column(name = "PRN")
    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_ID")
    public Long getSupplFreqId() {
        return supplFreqId;
    }

    public void setSupplFreqId(Long supplFreqId) {
        this.supplFreqId = supplFreqId;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_ENG")
    public String getSupplFreqEng() {
        return supplFreqEng;
    }

    public void setSupplFreqEng(String supplFreqEng) {
        this.supplFreqEng = supplFreqEng;
    }

    @Basic
    @Column(name = "SUPPL_FREQ1")
    public Long getSupplFreq1() {
        return supplFreq1;
    }

    public void setSupplFreq1(Long supplFreq1) {
        this.supplFreq1 = supplFreq1;
    }

    @Basic
    @Column(name = "SUPPL_FREQ2")
    public Long getSupplFreq2() {
        return supplFreq2;
    }

    public void setSupplFreq2(Long supplFreq2) {
        this.supplFreq2 = supplFreq2;
    }

    @Basic
    @Column(name = "OWNER")
    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Basic
    @Column(name = "SUSPEND")
    public String getSuspend() {
        return suspend;
    }

    public void setSuspend(String suspend) {
        this.suspend = suspend;
    }

    @Basic
    @Column(name = "CREATE_USER_ID")
    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    @Basic
    @Column(name = "CREATE_USER")
    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    @Basic
    @Column(name = "CREATE_HOSP")
    public String getCreateHosp() {
        return createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    @Basic
    @Column(name = "CREATE_RANK")
    public String getCreateRank() {
        return createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    @Basic
    @Column(name = "CREATE_RANK_DESC")
    public String getCreateRankDesc() {
        return createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    @Basic
    @Column(name = "CREATE_DTM")
    public Date getCreateDtm() {
        return createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    @Basic
    @Column(name = "UPDATE_USER_ID")
    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Basic
    @Column(name = "UPDATE_USER")
    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Basic
    @Column(name = "UPDATE_HOSP")
    public String getUpdateHosp() {
        return updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    @Basic
    @Column(name = "UPDATE_RANK")
    public String getUpdateRank() {
        return updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    @Basic
    @Column(name = "UPDATE_RANK_DESC")
    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    @Basic
    @Column(name = "UPDATE_DTM")
    public Date getUpdateDtm() {
        return updateDtm;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }

    @Version
    @Column(name = "VERSION")
    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    @Basic
    @Column(name = "ROUTE_ID")
    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    @Basic
    @Column(name = "ROUTE_ENG")
    public String getRouteEng() {
        return routeEng;
    }

    public void setRouteEng(String routeEng) {
        this.routeEng = routeEng;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "regimen_type", nullable = false)
    public MoeRegimenPo getMoeRegimen() {
        return this.moeRegimen;
    }

    public void setMoeRegimen(MoeRegimenPo moeRegimen) {
        this.moeRegimen = moeRegimen;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "freq_id", nullable = false)
    public MoeFreqPo getMoeFreq() {
        return this.moeFreq;
    }

    public void setMoeFreq(MoeFreqPo moeFreq) {
        this.moeFreq = moeFreq;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dosage_type", nullable = false)
    public MoeCommonDosageTypePo getMoeCommonDosageType() {
        return this.moeCommonDosageType;
    }

    public void setMoeCommonDosageType(MoeCommonDosageTypePo moeCommonDosageType) {
        this.moeCommonDosageType = moeCommonDosageType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeCommonDosagePo that = (MoeCommonDosagePo) o;

        if (commonDosageId != that.commonDosageId) return false;
        //if (freqId != that.freqId) return false;
        if (version != that.version) return false;
        if (hkRegNo != null ? !hkRegNo.equals(that.hkRegNo) : that.hkRegNo != null) return false;
        if (localDrugId != null ? !localDrugId.equals(that.localDrugId) : that.localDrugId != null) return false;
        //if (dosageType != null ? !dosageType.equals(that.dosageType) : that.dosageType != null) return false;
        //if (regimenType != null ? !regimenType.equals(that.regimenType) : that.regimenType != null) return false;
        if (dosage != null ? !dosage.equals(that.dosage) : that.dosage != null) return false;
        if (dosageUnitId != null ? !dosageUnitId.equals(that.dosageUnitId) : that.dosageUnitId != null) return false;
        if (dosageUnit != null ? !dosageUnit.equals(that.dosageUnit) : that.dosageUnit != null) return false;
        if (freqCode != null ? !freqCode.equals(that.freqCode) : that.freqCode != null) return false;
        if (freqEng != null ? !freqEng.equals(that.freqEng) : that.freqEng != null) return false;
        if (freq1 != null ? !freq1.equals(that.freq1) : that.freq1 != null) return false;
        if (prn != null ? !prn.equals(that.prn) : that.prn != null) return false;
        if (supplFreqId != null ? !supplFreqId.equals(that.supplFreqId) : that.supplFreqId != null) return false;
        if (supplFreqEng != null ? !supplFreqEng.equals(that.supplFreqEng) : that.supplFreqEng != null) return false;
        if (supplFreq1 != null ? !supplFreq1.equals(that.supplFreq1) : that.supplFreq1 != null) return false;
        if (supplFreq2 != null ? !supplFreq2.equals(that.supplFreq2) : that.supplFreq2 != null) return false;
        if (owner != null ? !owner.equals(that.owner) : that.owner != null) return false;
        if (suspend != null ? !suspend.equals(that.suspend) : that.suspend != null) return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null) return false;
        if (createUser != null ? !createUser.equals(that.createUser) : that.createUser != null) return false;
        if (createHosp != null ? !createHosp.equals(that.createHosp) : that.createHosp != null) return false;
        if (createRank != null ? !createRank.equals(that.createRank) : that.createRank != null) return false;
        if (createRankDesc != null ? !createRankDesc.equals(that.createRankDesc) : that.createRankDesc != null)
            return false;
        if (createDtm != null ? !createDtm.equals(that.createDtm) : that.createDtm != null) return false;
        if (updateUserId != null ? !updateUserId.equals(that.updateUserId) : that.updateUserId != null) return false;
        if (updateUser != null ? !updateUser.equals(that.updateUser) : that.updateUser != null) return false;
        if (updateHosp != null ? !updateHosp.equals(that.updateHosp) : that.updateHosp != null) return false;
        if (updateRank != null ? !updateRank.equals(that.updateRank) : that.updateRank != null) return false;
        if (updateRankDesc != null ? !updateRankDesc.equals(that.updateRankDesc) : that.updateRankDesc != null)
            return false;
        if (updateDtm != null ? !updateDtm.equals(that.updateDtm) : that.updateDtm != null) return false;
        if (routeId != null ? !routeId.equals(that.routeId) : that.routeId != null) return false;
        if (routeEng != null ? !routeEng.equals(that.routeEng) : that.routeEng != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (commonDosageId ^ (commonDosageId >>> 32));
        result = 31 * result + (hkRegNo != null ? hkRegNo.hashCode() : 0);
        result = 31 * result + (localDrugId != null ? localDrugId.hashCode() : 0);
        //result = 31 * result + (dosageType != null ? dosageType.hashCode() : 0);
        //result = 31 * result + (regimenType != null ? regimenType.hashCode() : 0);
        result = 31 * result + (dosage != null ? dosage.hashCode() : 0);
        result = 31 * result + (dosageUnitId != null ? dosageUnitId.hashCode() : 0);
        result = 31 * result + (dosageUnit != null ? dosageUnit.hashCode() : 0);
        //result = 31 * result + (int) (freqId ^ (freqId >>> 32));
        result = 31 * result + (freqCode != null ? freqCode.hashCode() : 0);
        result = 31 * result + (freqEng != null ? freqEng.hashCode() : 0);
        result = 31 * result + (freq1 != null ? freq1.hashCode() : 0);
        result = 31 * result + (prn != null ? prn.hashCode() : 0);
        result = 31 * result + (supplFreqId != null ? supplFreqId.hashCode() : 0);
        result = 31 * result + (supplFreqEng != null ? supplFreqEng.hashCode() : 0);
        result = 31 * result + (supplFreq1 != null ? supplFreq1.hashCode() : 0);
        result = 31 * result + (supplFreq2 != null ? supplFreq2.hashCode() : 0);
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (suspend != null ? suspend.hashCode() : 0);
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (createUser != null ? createUser.hashCode() : 0);
        result = 31 * result + (createHosp != null ? createHosp.hashCode() : 0);
        result = 31 * result + (createRank != null ? createRank.hashCode() : 0);
        result = 31 * result + (createRankDesc != null ? createRankDesc.hashCode() : 0);
        result = 31 * result + (createDtm != null ? createDtm.hashCode() : 0);
        result = 31 * result + (updateUserId != null ? updateUserId.hashCode() : 0);
        result = 31 * result + (updateUser != null ? updateUser.hashCode() : 0);
        result = 31 * result + (updateHosp != null ? updateHosp.hashCode() : 0);
        result = 31 * result + (updateRank != null ? updateRank.hashCode() : 0);
        result = 31 * result + (updateRankDesc != null ? updateRankDesc.hashCode() : 0);
        result = 31 * result + (updateDtm != null ? updateDtm.hashCode() : 0);
        result = 31 * result + (int) (version ^ (version >>> 32));
        result = 31 * result + (routeId != null ? routeId.hashCode() : 0);
        result = 31 * result + (routeEng != null ? routeEng.hashCode() : 0);
        return result;
    }
}
