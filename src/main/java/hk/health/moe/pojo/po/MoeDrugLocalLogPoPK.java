package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

public class MoeDrugLocalLogPoPK implements Serializable {
    private static final long serialVersionUID = 1873741499475414752L;
    private String localDrugId;
    private String hkRegNo;
    private String terminologyName;
    private String tradeName;
    private String vtm;
    private Long formId;
    private String formEng;
    private Long doseFormExtraInfoId;
    private String doseFormExtraInfo;
    private Long routeId;
    private String routeEng;
    private Long vtmId;
    private Long tradeVtmId;
    private Long vtmRouteId;
    private Long tradeVtmRouteId;
    private Long vtmRouteFormId;
    private Long tradeVtmRouteFormId;
    private Long baseUnitId;
    private String baseUnit;
    private Long prescribeUnitId;
    private String prescribeUnit;
    private Long dispenseUnitId;
    private String dispenseUnit;
    private Long legalClassId;
    private String legalClass;
    private String manufacturer;
    private String genericIndicator;
    private String allergyCheckFlag;
    private String strengthCompulsory;
    private String createUserId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private Date createDtm;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private Date updateDtm;
    private long version;
    private String actionType;
    private Date actionTime;

    @Id
    @Column(name = "LOCAL_DRUG_ID")
    public String getLocalDrugId() {
        return localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        this.localDrugId = localDrugId;
    }

    @Id
    @Column(name = "HK_REG_NO")
    public String getHkRegNo() {
        return hkRegNo;
    }

    public void setHkRegNo(String hkRegNo) {
        this.hkRegNo = hkRegNo;
    }

    @Id
    @Column(name = "TERMINOLOGY_NAME")
    public String getTerminologyName() {
        return terminologyName;
    }

    public void setTerminologyName(String terminologyName) {
        this.terminologyName = terminologyName;
    }

    @Id
    @Column(name = "TRADE_NAME")
    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    @Id
    @Column(name = "VTM")
    public String getVtm() {
        return vtm;
    }

    public void setVtm(String vtm) {
        this.vtm = vtm;
    }

    @Id
    @Column(name = "FORM_ID")
    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    @Id
    @Column(name = "FORM_ENG")
    public String getFormEng() {
        return formEng;
    }

    public void setFormEng(String formEng) {
        this.formEng = formEng;
    }

    @Id
    @Column(name = "DOSE_FORM_EXTRA_INFO_ID")
    public Long getDoseFormExtraInfoId() {
        return doseFormExtraInfoId;
    }

    public void setDoseFormExtraInfoId(Long doseFormExtraInfoId) {
        this.doseFormExtraInfoId = doseFormExtraInfoId;
    }

    @Id
    @Column(name = "DOSE_FORM_EXTRA_INFO")
    public String getDoseFormExtraInfo() {
        return doseFormExtraInfo;
    }

    public void setDoseFormExtraInfo(String doseFormExtraInfo) {
        this.doseFormExtraInfo = doseFormExtraInfo;
    }

    @Id
    @Column(name = "ROUTE_ID")
    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    @Id
    @Column(name = "ROUTE_ENG")
    public String getRouteEng() {
        return routeEng;
    }

    public void setRouteEng(String routeEng) {
        this.routeEng = routeEng;
    }

    @Id
    @Column(name = "VTM_ID")
    public Long getVtmId() {
        return vtmId;
    }

    public void setVtmId(Long vtmId) {
        this.vtmId = vtmId;
    }

    @Id
    @Column(name = "TRADE_VTM_ID")
    public Long getTradeVtmId() {
        return tradeVtmId;
    }

    public void setTradeVtmId(Long tradeVtmId) {
        this.tradeVtmId = tradeVtmId;
    }

    @Id
    @Column(name = "VTM_ROUTE_ID")
    public Long getVtmRouteId() {
        return vtmRouteId;
    }

    public void setVtmRouteId(Long vtmRouteId) {
        this.vtmRouteId = vtmRouteId;
    }

    @Id
    @Column(name = "TRADE_VTM_ROUTE_ID")
    public Long getTradeVtmRouteId() {
        return tradeVtmRouteId;
    }

    public void setTradeVtmRouteId(Long tradeVtmRouteId) {
        this.tradeVtmRouteId = tradeVtmRouteId;
    }

    @Id
    @Column(name = "VTM_ROUTE_FORM_ID")
    public Long getVtmRouteFormId() {
        return vtmRouteFormId;
    }

    public void setVtmRouteFormId(Long vtmRouteFormId) {
        this.vtmRouteFormId = vtmRouteFormId;
    }

    @Id
    @Column(name = "TRADE_VTM_ROUTE_FORM_ID")
    public Long getTradeVtmRouteFormId() {
        return tradeVtmRouteFormId;
    }

    public void setTradeVtmRouteFormId(Long tradeVtmRouteFormId) {
        this.tradeVtmRouteFormId = tradeVtmRouteFormId;
    }

    @Id
    @Column(name = "BASE_UNIT_ID")
    public Long getBaseUnitId() {
        return baseUnitId;
    }

    public void setBaseUnitId(Long baseUnitId) {
        this.baseUnitId = baseUnitId;
    }

    @Id
    @Column(name = "BASE_UNIT")
    public String getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(String baseUnit) {
        this.baseUnit = baseUnit;
    }

    @Id
    @Column(name = "PRESCRIBE_UNIT_ID")
    public Long getPrescribeUnitId() {
        return prescribeUnitId;
    }

    public void setPrescribeUnitId(Long prescribeUnitId) {
        this.prescribeUnitId = prescribeUnitId;
    }

    @Id
    @Column(name = "PRESCRIBE_UNIT")
    public String getPrescribeUnit() {
        return prescribeUnit;
    }

    public void setPrescribeUnit(String prescribeUnit) {
        this.prescribeUnit = prescribeUnit;
    }

    @Id
    @Column(name = "DISPENSE_UNIT_ID")
    public Long getDispenseUnitId() {
        return dispenseUnitId;
    }

    public void setDispenseUnitId(Long dispenseUnitId) {
        this.dispenseUnitId = dispenseUnitId;
    }

    @Id
    @Column(name = "DISPENSE_UNIT")
    public String getDispenseUnit() {
        return dispenseUnit;
    }

    public void setDispenseUnit(String dispenseUnit) {
        this.dispenseUnit = dispenseUnit;
    }

    @Id
    @Column(name = "LEGAL_CLASS_ID")
    public Long getLegalClassId() {
        return legalClassId;
    }

    public void setLegalClassId(Long legalClassId) {
        this.legalClassId = legalClassId;
    }

    @Id
    @Column(name = "LEGAL_CLASS")
    public String getLegalClass() {
        return legalClass;
    }

    public void setLegalClass(String legalClass) {
        this.legalClass = legalClass;
    }

    @Id
    @Column(name = "MANUFACTURER")
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Id
    @Column(name = "GENERIC_INDICATOR")
    public String getGenericIndicator() {
        return genericIndicator;
    }

    public void setGenericIndicator(String genericIndicator) {
        this.genericIndicator = genericIndicator;
    }

    @Id
    @Column(name = "ALLERGY_CHECK_FLAG")
    public String getAllergyCheckFlag() {
        return allergyCheckFlag;
    }

    public void setAllergyCheckFlag(String allergyCheckFlag) {
        this.allergyCheckFlag = allergyCheckFlag;
    }

    @Id
    @Column(name = "STRENGTH_COMPULSORY")
    public String getStrengthCompulsory() {
        return strengthCompulsory;
    }

    public void setStrengthCompulsory(String strengthCompulsory) {
        this.strengthCompulsory = strengthCompulsory;
    }

    @Id
    @Column(name = "CREATE_USER_ID")
    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    @Id
    @Column(name = "CREATE_USER")
    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    @Id
    @Column(name = "CREATE_HOSP")
    public String getCreateHosp() {
        return createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    @Id
    @Column(name = "CREATE_RANK")
    public String getCreateRank() {
        return createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    @Id
    @Column(name = "CREATE_RANK_DESC")
    public String getCreateRankDesc() {
        return createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    @Id
    @Column(name = "CREATE_DTM")
    public Date getCreateDtm() {
        return createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    @Id
    @Column(name = "UPDATE_USER_ID")
    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Id
    @Column(name = "UPDATE_USER")
    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Id
    @Column(name = "UPDATE_HOSP")
    public String getUpdateHosp() {
        return updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    @Id
    @Column(name = "UPDATE_RANK")
    public String getUpdateRank() {
        return updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    @Id
    @Column(name = "UPDATE_RANK_DESC")
    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    @Id
    @Column(name = "UPDATE_DTM")
    public Date getUpdateDtm() {
        return updateDtm;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }

    @Id
    @Column(name = "VERSION")
    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    @Id
    @Column(name = "ACTION_TYPE")
    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    @Id
    @Column(name = "ACTION_TIME")
    public Date getActionTime() {
        return actionTime;
    }

    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDrugLocalLogPoPK that = (MoeDrugLocalLogPoPK) o;

        if (version != that.version) return false;
        if (localDrugId != null ? !localDrugId.equals(that.localDrugId) : that.localDrugId != null) return false;
        if (hkRegNo != null ? !hkRegNo.equals(that.hkRegNo) : that.hkRegNo != null) return false;
        if (terminologyName != null ? !terminologyName.equals(that.terminologyName) : that.terminologyName != null)
            return false;
        if (tradeName != null ? !tradeName.equals(that.tradeName) : that.tradeName != null) return false;
        if (vtm != null ? !vtm.equals(that.vtm) : that.vtm != null) return false;
        if (formId != null ? !formId.equals(that.formId) : that.formId != null) return false;
        if (formEng != null ? !formEng.equals(that.formEng) : that.formEng != null) return false;
        if (doseFormExtraInfoId != null ? !doseFormExtraInfoId.equals(that.doseFormExtraInfoId) : that.doseFormExtraInfoId != null)
            return false;
        if (doseFormExtraInfo != null ? !doseFormExtraInfo.equals(that.doseFormExtraInfo) : that.doseFormExtraInfo != null)
            return false;
        if (routeId != null ? !routeId.equals(that.routeId) : that.routeId != null) return false;
        if (routeEng != null ? !routeEng.equals(that.routeEng) : that.routeEng != null) return false;
        if (vtmId != null ? !vtmId.equals(that.vtmId) : that.vtmId != null) return false;
        if (tradeVtmId != null ? !tradeVtmId.equals(that.tradeVtmId) : that.tradeVtmId != null) return false;
        if (vtmRouteId != null ? !vtmRouteId.equals(that.vtmRouteId) : that.vtmRouteId != null) return false;
        if (tradeVtmRouteId != null ? !tradeVtmRouteId.equals(that.tradeVtmRouteId) : that.tradeVtmRouteId != null)
            return false;
        if (vtmRouteFormId != null ? !vtmRouteFormId.equals(that.vtmRouteFormId) : that.vtmRouteFormId != null)
            return false;
        if (tradeVtmRouteFormId != null ? !tradeVtmRouteFormId.equals(that.tradeVtmRouteFormId) : that.tradeVtmRouteFormId != null)
            return false;
        if (baseUnitId != null ? !baseUnitId.equals(that.baseUnitId) : that.baseUnitId != null) return false;
        if (baseUnit != null ? !baseUnit.equals(that.baseUnit) : that.baseUnit != null) return false;
        if (prescribeUnitId != null ? !prescribeUnitId.equals(that.prescribeUnitId) : that.prescribeUnitId != null)
            return false;
        if (prescribeUnit != null ? !prescribeUnit.equals(that.prescribeUnit) : that.prescribeUnit != null)
            return false;
        if (dispenseUnitId != null ? !dispenseUnitId.equals(that.dispenseUnitId) : that.dispenseUnitId != null)
            return false;
        if (dispenseUnit != null ? !dispenseUnit.equals(that.dispenseUnit) : that.dispenseUnit != null) return false;
        if (legalClassId != null ? !legalClassId.equals(that.legalClassId) : that.legalClassId != null) return false;
        if (legalClass != null ? !legalClass.equals(that.legalClass) : that.legalClass != null) return false;
        if (manufacturer != null ? !manufacturer.equals(that.manufacturer) : that.manufacturer != null) return false;
        if (genericIndicator != null ? !genericIndicator.equals(that.genericIndicator) : that.genericIndicator != null)
            return false;
        if (allergyCheckFlag != null ? !allergyCheckFlag.equals(that.allergyCheckFlag) : that.allergyCheckFlag != null)
            return false;
        if (strengthCompulsory != null ? !strengthCompulsory.equals(that.strengthCompulsory) : that.strengthCompulsory != null)
            return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null) return false;
        if (createUser != null ? !createUser.equals(that.createUser) : that.createUser != null) return false;
        if (createHosp != null ? !createHosp.equals(that.createHosp) : that.createHosp != null) return false;
        if (createRank != null ? !createRank.equals(that.createRank) : that.createRank != null) return false;
        if (createRankDesc != null ? !createRankDesc.equals(that.createRankDesc) : that.createRankDesc != null)
            return false;
        if (createDtm != null ? !createDtm.equals(that.createDtm) : that.createDtm != null) return false;
        if (updateUserId != null ? !updateUserId.equals(that.updateUserId) : that.updateUserId != null) return false;
        if (updateUser != null ? !updateUser.equals(that.updateUser) : that.updateUser != null) return false;
        if (updateHosp != null ? !updateHosp.equals(that.updateHosp) : that.updateHosp != null) return false;
        if (updateRank != null ? !updateRank.equals(that.updateRank) : that.updateRank != null) return false;
        if (updateRankDesc != null ? !updateRankDesc.equals(that.updateRankDesc) : that.updateRankDesc != null)
            return false;
        if (updateDtm != null ? !updateDtm.equals(that.updateDtm) : that.updateDtm != null) return false;
        if (actionType != null ? !actionType.equals(that.actionType) : that.actionType != null) return false;
        if (actionTime != null ? !actionTime.equals(that.actionTime) : that.actionTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = localDrugId != null ? localDrugId.hashCode() : 0;
        result = 31 * result + (hkRegNo != null ? hkRegNo.hashCode() : 0);
        result = 31 * result + (terminologyName != null ? terminologyName.hashCode() : 0);
        result = 31 * result + (tradeName != null ? tradeName.hashCode() : 0);
        result = 31 * result + (vtm != null ? vtm.hashCode() : 0);
        result = 31 * result + (formId != null ? formId.hashCode() : 0);
        result = 31 * result + (formEng != null ? formEng.hashCode() : 0);
        result = 31 * result + (doseFormExtraInfoId != null ? doseFormExtraInfoId.hashCode() : 0);
        result = 31 * result + (doseFormExtraInfo != null ? doseFormExtraInfo.hashCode() : 0);
        result = 31 * result + (routeId != null ? routeId.hashCode() : 0);
        result = 31 * result + (routeEng != null ? routeEng.hashCode() : 0);
        result = 31 * result + (vtmId != null ? vtmId.hashCode() : 0);
        result = 31 * result + (tradeVtmId != null ? tradeVtmId.hashCode() : 0);
        result = 31 * result + (vtmRouteId != null ? vtmRouteId.hashCode() : 0);
        result = 31 * result + (tradeVtmRouteId != null ? tradeVtmRouteId.hashCode() : 0);
        result = 31 * result + (vtmRouteFormId != null ? vtmRouteFormId.hashCode() : 0);
        result = 31 * result + (tradeVtmRouteFormId != null ? tradeVtmRouteFormId.hashCode() : 0);
        result = 31 * result + (baseUnitId != null ? baseUnitId.hashCode() : 0);
        result = 31 * result + (baseUnit != null ? baseUnit.hashCode() : 0);
        result = 31 * result + (prescribeUnitId != null ? prescribeUnitId.hashCode() : 0);
        result = 31 * result + (prescribeUnit != null ? prescribeUnit.hashCode() : 0);
        result = 31 * result + (dispenseUnitId != null ? dispenseUnitId.hashCode() : 0);
        result = 31 * result + (dispenseUnit != null ? dispenseUnit.hashCode() : 0);
        result = 31 * result + (legalClassId != null ? legalClassId.hashCode() : 0);
        result = 31 * result + (legalClass != null ? legalClass.hashCode() : 0);
        result = 31 * result + (manufacturer != null ? manufacturer.hashCode() : 0);
        result = 31 * result + (genericIndicator != null ? genericIndicator.hashCode() : 0);
        result = 31 * result + (allergyCheckFlag != null ? allergyCheckFlag.hashCode() : 0);
        result = 31 * result + (strengthCompulsory != null ? strengthCompulsory.hashCode() : 0);
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (createUser != null ? createUser.hashCode() : 0);
        result = 31 * result + (createHosp != null ? createHosp.hashCode() : 0);
        result = 31 * result + (createRank != null ? createRank.hashCode() : 0);
        result = 31 * result + (createRankDesc != null ? createRankDesc.hashCode() : 0);
        result = 31 * result + (createDtm != null ? createDtm.hashCode() : 0);
        result = 31 * result + (updateUserId != null ? updateUserId.hashCode() : 0);
        result = 31 * result + (updateUser != null ? updateUser.hashCode() : 0);
        result = 31 * result + (updateHosp != null ? updateHosp.hashCode() : 0);
        result = 31 * result + (updateRank != null ? updateRank.hashCode() : 0);
        result = 31 * result + (updateRankDesc != null ? updateRankDesc.hashCode() : 0);
        result = 31 * result + (updateDtm != null ? updateDtm.hashCode() : 0);
        result = 31 * result + (int) (version ^ (version >>> 32));
        result = 31 * result + (actionType != null ? actionType.hashCode() : 0);
        result = 31 * result + (actionTime != null ? actionTime.hashCode() : 0);
        return result;
    }
}
