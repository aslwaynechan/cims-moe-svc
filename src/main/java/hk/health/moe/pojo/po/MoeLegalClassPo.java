package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_LEGAL_CLASS")
public class MoeLegalClassPo implements Serializable {
    private static final long serialVersionUID = -3597863087722996715L;
    private long legalClassId;
    private String legalClass;
    private String dangerousDrug;

    @Id
    @Column(name = "LEGAL_CLASS_ID")
    public long getLegalClassId() {
        return legalClassId;
    }

    public void setLegalClassId(long legalClassId) {
        this.legalClassId = legalClassId;
    }

    @Basic
    @Column(name = "LEGAL_CLASS")
    public String getLegalClass() {
        return legalClass;
    }

    public void setLegalClass(String legalClass) {
        this.legalClass = legalClass;
    }

    @Basic
    @Column(name = "DANGEROUS_DRUG")
    public String getDangerousDrug() {
        return dangerousDrug;
    }

    public void setDangerousDrug(String dangerousDrug) {
        this.dangerousDrug = dangerousDrug;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeLegalClassPo that = (MoeLegalClassPo) o;

        if (legalClassId != that.legalClassId) return false;
        if (legalClass != null ? !legalClass.equals(that.legalClass) : that.legalClass != null) return false;
        if (dangerousDrug != null ? !dangerousDrug.equals(that.dangerousDrug) : that.dangerousDrug != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (legalClassId ^ (legalClassId >>> 32));
        result = 31 * result + (legalClass != null ? legalClass.hashCode() : 0);
        result = 31 * result + (dangerousDrug != null ? dangerousDrug.hashCode() : 0);
        return result;
    }
}
