package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name = "MOE_MED_PROFILE")
@IdClass(MoeMedProfilePoPK.class)
public class MoeMedProfilePo implements Serializable, Comparable<MoeMedProfilePo>{
    private static final long serialVersionUID = -7831456632716133699L;
    private String hospcode;
    private long ordNo;
    private long cmsItemNo;
    private String patHospcode;
    private String caseNo;
    private String regimen;
    private String multDose;
    private BigDecimal volValue;
    private String volUnit;
    private String volText;
    private String itemcode;
    private String firstDisplayname;
    private String tradename;
    private String strength;
    private String baseunit;
    private String formcode;
    private String routeCode;
    private String routeDesc;
    private String formDesc;
    private String salt;
    private String extraInfo;
    private String dangerdrug;
    private String externalUse;
    private String nameType;
    private String secondDisplayname;
    private BigDecimal dosage;
    private String modu;
    private String freqCode;
    private Long freq1;
    private String supFreqCode;
    private Long supFreq1;
    private Long supFreq2;
    private String dayOfWeek;
    private String adminTimeCode;
    private String prn;
    private BigDecimal prnPercent;
    private String siteCode;
    private String supSiteDesc;
    private Long duration;
    private String durationUnit;
    private Date startDate;
    private Date endDate;
    private Long moQty;
    private String moQtyUnit;
    private String actionStatus;
    private String formulStatus;
    private String formulStatus2;
    private String patType;
    private String itemStatus;
    private String specInstruct;
    private String specNote;
    private String translate;
    private String fixPeriod;
    private String restricted;
    private String singleUse;
    private String moCreate;
    private String displayRoutedesc;
    private String trueDisplayname;
    private long orgItemNo;
    private String allowRepeat;
    private String freqText;
    private String supFreqText;
    private String trueAliasname;
    private String capdSystem;
    private String capdCalcium;
    private String capdConc;
    private String durationInputType;
    private String capdBaseunit;
    private String siteDesc;
    private String remarkCreateBy;
    private Date remarkCreateDate;
    private String remarkText;
    private String remarkConfirmBy;
    private Date remarkConfirmDate;
    private String trueConc;
    private String mdsInfo;
    private String adminTimeDesc;
    private String commentCreateBy;
    private Date commentCreateDate;
    private String updateBy;
    private Date updateTime;
    private MoeOrderPo moeOrder;
    private Set<MoeMedMultDosePo> moeMedMultDoses = new TreeSet<>();
    private MoeEhrMedProfilePo moeEhrMedProfile;
    private Set<MoeEhrMedAllergenPo> moeEhrMedAllergens = new TreeSet<>();

    @Id
    @Column(name = "HOSPCODE")
    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    @Id
    @Column(name = "ORD_NO")
    public long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(long ordNo) {
        this.ordNo = ordNo;
    }

    @Id
    @Column(name = "CMS_ITEM_NO")
    public long getCmsItemNo() {
        return cmsItemNo;
    }

    public void setCmsItemNo(long cmsItemNo) {
        this.cmsItemNo = cmsItemNo;
    }

    @Basic
    @Column(name = "PAT_HOSPCODE")
    public String getPatHospcode() {
        return patHospcode;
    }

    public void setPatHospcode(String patHospcode) {
        this.patHospcode = patHospcode;
    }

    @Basic
    @Column(name = "CASE_NO")
    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    @Basic
    @Column(name = "REGIMEN")
    public String getRegimen() {
        return regimen;
    }

    public void setRegimen(String regimen) {
        this.regimen = regimen;
    }

    @Basic
    @Column(name = "MULT_DOSE")
    public String getMultDose() {
        return multDose;
    }

    public void setMultDose(String multDose) {
        this.multDose = multDose;
    }

    @Basic
    @Column(name = "VOL_VALUE")
    public BigDecimal getVolValue() {
        return volValue;
    }

    public void setVolValue(BigDecimal volValue) {
        this.volValue = volValue;
    }

    @Basic
    @Column(name = "VOL_UNIT")
    public String getVolUnit() {
        return volUnit;
    }

    public void setVolUnit(String volUnit) {
        this.volUnit = volUnit;
    }

    @Basic
    @Column(name = "VOL_TEXT")
    public String getVolText() {
        return volText;
    }

    public void setVolText(String volText) {
        this.volText = volText;
    }

    @Basic
    @Column(name = "ITEMCODE")
    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    @Basic
    @Column(name = "FIRST_DISPLAYNAME")
    public String getFirstDisplayname() {
        return firstDisplayname;
    }

    public void setFirstDisplayname(String firstDisplayname) {
        this.firstDisplayname = firstDisplayname;
    }

    @Basic
    @Column(name = "TRADENAME")
    public String getTradename() {
        return tradename;
    }

    public void setTradename(String tradename) {
        this.tradename = tradename;
    }

    @Basic
    @Column(name = "STRENGTH")
    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    @Basic
    @Column(name = "BASEUNIT")
    public String getBaseunit() {
        return baseunit;
    }

    public void setBaseunit(String baseunit) {
        this.baseunit = baseunit;
    }

    @Basic
    @Column(name = "FORMCODE")
    public String getFormcode() {
        return formcode;
    }

    public void setFormcode(String formcode) {
        this.formcode = formcode;
    }

    @Basic
    @Column(name = "ROUTE_CODE")
    public String getRouteCode() {
        return routeCode;
    }

    public void setRouteCode(String routeCode) {
        this.routeCode = routeCode;
    }

    @Basic
    @Column(name = "ROUTE_DESC")
    public String getRouteDesc() {
        return routeDesc;
    }

    public void setRouteDesc(String routeDesc) {
        this.routeDesc = routeDesc;
    }

    @Basic
    @Column(name = "FORM_DESC")
    public String getFormDesc() {
        return formDesc;
    }

    public void setFormDesc(String formDesc) {
        this.formDesc = formDesc;
    }

    @Basic
    @Column(name = "SALT")
    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Basic
    @Column(name = "EXTRA_INFO")
    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }

    @Basic
    @Column(name = "DANGERDRUG")
    public String getDangerdrug() {
        return dangerdrug;
    }

    public void setDangerdrug(String dangerdrug) {
        this.dangerdrug = dangerdrug;
    }

    @Basic
    @Column(name = "EXTERNAL_USE")
    public String getExternalUse() {
        return externalUse;
    }

    public void setExternalUse(String externalUse) {
        this.externalUse = externalUse;
    }

    @Basic
    @Column(name = "NAME_TYPE")
    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    @Basic
    @Column(name = "SECOND_DISPLAYNAME")
    public String getSecondDisplayname() {
        return secondDisplayname;
    }

    public void setSecondDisplayname(String secondDisplayname) {
        this.secondDisplayname = secondDisplayname;
    }

    @Basic
    @Column(name = "DOSAGE")
    public BigDecimal getDosage() {
        return dosage;
    }

    public void setDosage(BigDecimal dosage) {
        this.dosage = dosage;
    }

    @Basic
    @Column(name = "MODU")
    public String getModu() {
        return modu;
    }

    public void setModu(String modu) {
        this.modu = modu;
    }

    @Basic
    @Column(name = "FREQ_CODE")
    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    @Basic
    @Column(name = "FREQ1")
    public Long getFreq1() {
        return freq1;
    }

    public void setFreq1(Long freq1) {
        this.freq1 = freq1;
    }

    @Basic
    @Column(name = "SUP_FREQ_CODE")
    public String getSupFreqCode() {
        return supFreqCode;
    }

    public void setSupFreqCode(String supFreqCode) {
        this.supFreqCode = supFreqCode;
    }

    @Basic
    @Column(name = "SUP_FREQ1")
    public Long getSupFreq1() {
        return supFreq1;
    }

    public void setSupFreq1(Long supFreq1) {
        this.supFreq1 = supFreq1;
    }

    @Basic
    @Column(name = "SUP_FREQ2")
    public Long getSupFreq2() {
        return supFreq2;
    }

    public void setSupFreq2(Long supFreq2) {
        this.supFreq2 = supFreq2;
    }

    @Basic
    @Column(name = "DAY_OF_WEEK")
    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    @Basic
    @Column(name = "ADMIN_TIME_CODE")
    public String getAdminTimeCode() {
        return adminTimeCode;
    }

    public void setAdminTimeCode(String adminTimeCode) {
        this.adminTimeCode = adminTimeCode;
    }

    @Basic
    @Column(name = "PRN")
    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    @Basic
    @Column(name = "PRN_PERCENT")
    public BigDecimal getPrnPercent() {
        return prnPercent;
    }

    public void setPrnPercent(BigDecimal prnPercent) {
        this.prnPercent = prnPercent;
    }

    @Basic
    @Column(name = "SITE_CODE")
    public String getSiteCode() {
        return siteCode;
    }

    public void setSiteCode(String siteCode) {
        this.siteCode = siteCode;
    }

    @Basic
    @Column(name = "SUP_SITE_DESC")
    public String getSupSiteDesc() {
        return supSiteDesc;
    }

    public void setSupSiteDesc(String supSiteDesc) {
        this.supSiteDesc = supSiteDesc;
    }

    @Basic
    @Column(name = "DURATION")
    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    @Basic
    @Column(name = "DURATION_UNIT")
    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    @Basic
    @Column(name = "START_DATE")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "END_DATE")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "MO_QTY")
    public Long getMoQty() {
        return moQty;
    }

    public void setMoQty(Long moQty) {
        this.moQty = moQty;
    }

    @Basic
    @Column(name = "MO_QTY_UNIT")
    public String getMoQtyUnit() {
        return moQtyUnit;
    }

    public void setMoQtyUnit(String moQtyUnit) {
        this.moQtyUnit = moQtyUnit;
    }

    @Basic
    @Column(name = "ACTION_STATUS")
    public String getActionStatus() {
        return actionStatus;
    }

    public void setActionStatus(String actionStatus) {
        this.actionStatus = actionStatus;
    }

    @Basic
    @Column(name = "FORMUL_STATUS")
    public String getFormulStatus() {
        return formulStatus;
    }

    public void setFormulStatus(String formulStatus) {
        this.formulStatus = formulStatus;
    }

    @Basic
    @Column(name = "FORMUL_STATUS2")
    public String getFormulStatus2() {
        return formulStatus2;
    }

    public void setFormulStatus2(String formulStatus2) {
        this.formulStatus2 = formulStatus2;
    }

    @Basic
    @Column(name = "PAT_TYPE")
    public String getPatType() {
        return patType;
    }

    public void setPatType(String patType) {
        this.patType = patType;
    }

    @Basic
    @Column(name = "ITEM_STATUS")
    public String getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(String itemStatus) {
        this.itemStatus = itemStatus;
    }

    @Basic
    @Column(name = "SPEC_INSTRUCT")
    public String getSpecInstruct() {
        return specInstruct;
    }

    public void setSpecInstruct(String specInstruct) {
        this.specInstruct = specInstruct;
    }

    @Basic
    @Column(name = "SPEC_NOTE")
    public String getSpecNote() {
        return specNote;
    }

    public void setSpecNote(String specNote) {
        this.specNote = specNote;
    }

    @Basic
    @Column(name = "TRANSLATE")
    public String getTranslate() {
        return translate;
    }

    public void setTranslate(String translate) {
        this.translate = translate;
    }

    @Basic
    @Column(name = "FIX_PERIOD")
    public String getFixPeriod() {
        return fixPeriod;
    }

    public void setFixPeriod(String fixPeriod) {
        this.fixPeriod = fixPeriod;
    }

    @Basic
    @Column(name = "RESTRICTED")
    public String getRestricted() {
        return restricted;
    }

    public void setRestricted(String restricted) {
        this.restricted = restricted;
    }

    @Basic
    @Column(name = "SINGLE_USE")
    public String getSingleUse() {
        return singleUse;
    }

    public void setSingleUse(String singleUse) {
        this.singleUse = singleUse;
    }

    @Basic
    @Column(name = "MO_CREATE")
    public String getMoCreate() {
        return moCreate;
    }

    public void setMoCreate(String moCreate) {
        this.moCreate = moCreate;
    }

    @Basic
    @Column(name = "DISPLAY_ROUTEDESC")
    public String getDisplayRoutedesc() {
        return displayRoutedesc;
    }

    public void setDisplayRoutedesc(String displayRoutedesc) {
        this.displayRoutedesc = displayRoutedesc;
    }

    @Basic
    @Column(name = "TRUE_DISPLAYNAME")
    public String getTrueDisplayname() {
        return trueDisplayname;
    }

    public void setTrueDisplayname(String trueDisplayname) {
        this.trueDisplayname = trueDisplayname;
    }

    @Basic
    @Column(name = "ORG_ITEM_NO")
    public long getOrgItemNo() {
        return orgItemNo;
    }

    public void setOrgItemNo(long orgItemNo) {
        this.orgItemNo = orgItemNo;
    }

    @Basic
    @Column(name = "ALLOW_REPEAT")
    public String getAllowRepeat() {
        return allowRepeat;
    }

    public void setAllowRepeat(String allowRepeat) {
        this.allowRepeat = allowRepeat;
    }

    @Basic
    @Column(name = "FREQ_TEXT")
    public String getFreqText() {
        return freqText;
    }

    public void setFreqText(String freqText) {
        this.freqText = freqText;
    }

    @Basic
    @Column(name = "SUP_FREQ_TEXT")
    public String getSupFreqText() {
        return supFreqText;
    }

    public void setSupFreqText(String supFreqText) {
        this.supFreqText = supFreqText;
    }

    @Basic
    @Column(name = "TRUE_ALIASNAME")
    public String getTrueAliasname() {
        return trueAliasname;
    }

    public void setTrueAliasname(String trueAliasname) {
        this.trueAliasname = trueAliasname;
    }

    @Basic
    @Column(name = "CAPD_SYSTEM")
    public String getCapdSystem() {
        return capdSystem;
    }

    public void setCapdSystem(String capdSystem) {
        this.capdSystem = capdSystem;
    }

    @Basic
    @Column(name = "CAPD_CALCIUM")
    public String getCapdCalcium() {
        return capdCalcium;
    }

    public void setCapdCalcium(String capdCalcium) {
        this.capdCalcium = capdCalcium;
    }

    @Basic
    @Column(name = "CAPD_CONC")
    public String getCapdConc() {
        return capdConc;
    }

    public void setCapdConc(String capdConc) {
        this.capdConc = capdConc;
    }

    @Basic
    @Column(name = "DURATION_INPUT_TYPE")
    public String getDurationInputType() {
        return durationInputType;
    }

    public void setDurationInputType(String durationInputType) {
        this.durationInputType = durationInputType;
    }

    @Basic
    @Column(name = "CAPD_BASEUNIT")
    public String getCapdBaseunit() {
        return capdBaseunit;
    }

    public void setCapdBaseunit(String capdBaseunit) {
        this.capdBaseunit = capdBaseunit;
    }

    @Basic
    @Column(name = "SITE_DESC")
    public String getSiteDesc() {
        return siteDesc;
    }

    public void setSiteDesc(String siteDesc) {
        this.siteDesc = siteDesc;
    }

    @Basic
    @Column(name = "REMARK_CREATE_BY")
    public String getRemarkCreateBy() {
        return remarkCreateBy;
    }

    public void setRemarkCreateBy(String remarkCreateBy) {
        this.remarkCreateBy = remarkCreateBy;
    }

    @Basic
    @Column(name = "REMARK_CREATE_DATE")
    public Date getRemarkCreateDate() {
        return remarkCreateDate;
    }

    public void setRemarkCreateDate(Date remarkCreateDate) {
        this.remarkCreateDate = remarkCreateDate;
    }

    @Basic
    @Column(name = "REMARK_TEXT")
    public String getRemarkText() {
        return remarkText;
    }

    public void setRemarkText(String remarkText) {
        this.remarkText = remarkText;
    }

    @Basic
    @Column(name = "REMARK_CONFIRM_BY")
    public String getRemarkConfirmBy() {
        return remarkConfirmBy;
    }

    public void setRemarkConfirmBy(String remarkConfirmBy) {
        this.remarkConfirmBy = remarkConfirmBy;
    }

    @Basic
    @Column(name = "REMARK_CONFIRM_DATE")
    public Date getRemarkConfirmDate() {
        return remarkConfirmDate;
    }

    public void setRemarkConfirmDate(Date remarkConfirmDate) {
        this.remarkConfirmDate = remarkConfirmDate;
    }

    @Basic
    @Column(name = "TRUE_CONC")
    public String getTrueConc() {
        return trueConc;
    }

    public void setTrueConc(String trueConc) {
        this.trueConc = trueConc;
    }

    @Basic
    @Column(name = "MDS_INFO")
    public String getMdsInfo() {
        return mdsInfo;
    }

    public void setMdsInfo(String mdsInfo) {
        this.mdsInfo = mdsInfo;
    }

    @Basic
    @Column(name = "ADMIN_TIME_DESC")
    public String getAdminTimeDesc() {
        return adminTimeDesc;
    }

    public void setAdminTimeDesc(String adminTimeDesc) {
        this.adminTimeDesc = adminTimeDesc;
    }

    @Basic
    @Column(name = "COMMENT_CREATE_BY")
    public String getCommentCreateBy() {
        return commentCreateBy;
    }

    public void setCommentCreateBy(String commentCreateBy) {
        this.commentCreateBy = commentCreateBy;
    }

    @Basic
    @Column(name = "COMMENT_CREATE_DATE")
    public Date getCommentCreateDate() {
        return commentCreateDate;
    }

    public void setCommentCreateDate(Date commentCreateDate) {
        this.commentCreateDate = commentCreateDate;
    }

    @Basic
    @Column(name = "UPDATE_BY")
    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    @Basic
    @Column(name = "UPDATE_TIME")
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @MapsId("ordNo")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ord_no", nullable = false, insertable = false, updatable = false)
    public MoeOrderPo getMoeOrder() {
        return this.moeOrder;
    }

    public void setMoeOrder(MoeOrderPo moeOrder) {
        this.moeOrder = moeOrder;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moeMedProfile",
            cascade= CascadeType.ALL, orphanRemoval=true)
    @OrderBy("stepNo asc")
    public Set<MoeMedMultDosePo> getMoeMedMultDoses() {
        return this.moeMedMultDoses;
    }

    public void setMoeMedMultDoses(Set<MoeMedMultDosePo> moeMedMultDoses) {
        this.moeMedMultDoses = moeMedMultDoses;
    }

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "moeMedProfile",
            cascade=CascadeType.ALL, orphanRemoval=true)
    public MoeEhrMedProfilePo getMoeEhrMedProfile() {
        return this.moeEhrMedProfile;
    }

    public void setMoeEhrMedProfile(MoeEhrMedProfilePo moeEhrMedProfile) {
        this.moeEhrMedProfile = moeEhrMedProfile;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moeMedProfile",
            cascade=CascadeType.ALL, orphanRemoval=true)
    @OrderBy("rowNo asc")
    public Set<MoeEhrMedAllergenPo> getMoeEhrMedAllergens() {
        return this.moeEhrMedAllergens;
    }

    public void setMoeEhrMedAllergens(Set<MoeEhrMedAllergenPo> moeEhrMedAllergens) {
        this.moeEhrMedAllergens = moeEhrMedAllergens;
    }

    @Override
    public int compareTo(MoeMedProfilePo o) {
        long thisVal = this.getOrgItemNo();
        long anotherVal = o.getOrgItemNo();
        return (thisVal<anotherVal ? -1 : (thisVal==anotherVal ? 0 : 1));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeMedProfilePo that = (MoeMedProfilePo) o;

        if (ordNo != that.ordNo) return false;
        if (cmsItemNo != that.cmsItemNo) return false;
        if (orgItemNo != that.orgItemNo) return false;
        if (hospcode != null ? !hospcode.equals(that.hospcode) : that.hospcode != null) return false;
        if (patHospcode != null ? !patHospcode.equals(that.patHospcode) : that.patHospcode != null) return false;
        if (caseNo != null ? !caseNo.equals(that.caseNo) : that.caseNo != null) return false;
        if (regimen != null ? !regimen.equals(that.regimen) : that.regimen != null) return false;
        if (multDose != null ? !multDose.equals(that.multDose) : that.multDose != null) return false;
        if (volValue != null ? !volValue.equals(that.volValue) : that.volValue != null) return false;
        if (volUnit != null ? !volUnit.equals(that.volUnit) : that.volUnit != null) return false;
        if (volText != null ? !volText.equals(that.volText) : that.volText != null) return false;
        if (itemcode != null ? !itemcode.equals(that.itemcode) : that.itemcode != null) return false;
        if (firstDisplayname != null ? !firstDisplayname.equals(that.firstDisplayname) : that.firstDisplayname != null)
            return false;
        if (tradename != null ? !tradename.equals(that.tradename) : that.tradename != null) return false;
        if (strength != null ? !strength.equals(that.strength) : that.strength != null) return false;
        if (baseunit != null ? !baseunit.equals(that.baseunit) : that.baseunit != null) return false;
        if (formcode != null ? !formcode.equals(that.formcode) : that.formcode != null) return false;
        if (routeCode != null ? !routeCode.equals(that.routeCode) : that.routeCode != null) return false;
        if (routeDesc != null ? !routeDesc.equals(that.routeDesc) : that.routeDesc != null) return false;
        if (formDesc != null ? !formDesc.equals(that.formDesc) : that.formDesc != null) return false;
        if (salt != null ? !salt.equals(that.salt) : that.salt != null) return false;
        if (extraInfo != null ? !extraInfo.equals(that.extraInfo) : that.extraInfo != null) return false;
        if (dangerdrug != null ? !dangerdrug.equals(that.dangerdrug) : that.dangerdrug != null) return false;
        if (externalUse != null ? !externalUse.equals(that.externalUse) : that.externalUse != null) return false;
        if (nameType != null ? !nameType.equals(that.nameType) : that.nameType != null) return false;
        if (secondDisplayname != null ? !secondDisplayname.equals(that.secondDisplayname) : that.secondDisplayname != null)
            return false;
        if (dosage != null ? !dosage.equals(that.dosage) : that.dosage != null) return false;
        if (modu != null ? !modu.equals(that.modu) : that.modu != null) return false;
        if (freqCode != null ? !freqCode.equals(that.freqCode) : that.freqCode != null) return false;
        if (freq1 != null ? !freq1.equals(that.freq1) : that.freq1 != null) return false;
        if (supFreqCode != null ? !supFreqCode.equals(that.supFreqCode) : that.supFreqCode != null) return false;
        if (supFreq1 != null ? !supFreq1.equals(that.supFreq1) : that.supFreq1 != null) return false;
        if (supFreq2 != null ? !supFreq2.equals(that.supFreq2) : that.supFreq2 != null) return false;
        if (dayOfWeek != null ? !dayOfWeek.equals(that.dayOfWeek) : that.dayOfWeek != null) return false;
        if (adminTimeCode != null ? !adminTimeCode.equals(that.adminTimeCode) : that.adminTimeCode != null)
            return false;
        if (prn != null ? !prn.equals(that.prn) : that.prn != null) return false;
        if (prnPercent != null ? !prnPercent.equals(that.prnPercent) : that.prnPercent != null) return false;
        if (siteCode != null ? !siteCode.equals(that.siteCode) : that.siteCode != null) return false;
        if (supSiteDesc != null ? !supSiteDesc.equals(that.supSiteDesc) : that.supSiteDesc != null) return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;
        if (durationUnit != null ? !durationUnit.equals(that.durationUnit) : that.durationUnit != null) return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;
        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
        if (moQty != null ? !moQty.equals(that.moQty) : that.moQty != null) return false;
        if (moQtyUnit != null ? !moQtyUnit.equals(that.moQtyUnit) : that.moQtyUnit != null) return false;
        if (actionStatus != null ? !actionStatus.equals(that.actionStatus) : that.actionStatus != null) return false;
        if (formulStatus != null ? !formulStatus.equals(that.formulStatus) : that.formulStatus != null) return false;
        if (formulStatus2 != null ? !formulStatus2.equals(that.formulStatus2) : that.formulStatus2 != null)
            return false;
        if (patType != null ? !patType.equals(that.patType) : that.patType != null) return false;
        if (itemStatus != null ? !itemStatus.equals(that.itemStatus) : that.itemStatus != null) return false;
        if (specInstruct != null ? !specInstruct.equals(that.specInstruct) : that.specInstruct != null) return false;
        if (specNote != null ? !specNote.equals(that.specNote) : that.specNote != null) return false;
        if (translate != null ? !translate.equals(that.translate) : that.translate != null) return false;
        if (fixPeriod != null ? !fixPeriod.equals(that.fixPeriod) : that.fixPeriod != null) return false;
        if (restricted != null ? !restricted.equals(that.restricted) : that.restricted != null) return false;
        if (singleUse != null ? !singleUse.equals(that.singleUse) : that.singleUse != null) return false;
        if (moCreate != null ? !moCreate.equals(that.moCreate) : that.moCreate != null) return false;
        if (displayRoutedesc != null ? !displayRoutedesc.equals(that.displayRoutedesc) : that.displayRoutedesc != null)
            return false;
        if (trueDisplayname != null ? !trueDisplayname.equals(that.trueDisplayname) : that.trueDisplayname != null)
            return false;
        if (allowRepeat != null ? !allowRepeat.equals(that.allowRepeat) : that.allowRepeat != null) return false;
        if (freqText != null ? !freqText.equals(that.freqText) : that.freqText != null) return false;
        if (supFreqText != null ? !supFreqText.equals(that.supFreqText) : that.supFreqText != null) return false;
        if (trueAliasname != null ? !trueAliasname.equals(that.trueAliasname) : that.trueAliasname != null)
            return false;
        if (capdSystem != null ? !capdSystem.equals(that.capdSystem) : that.capdSystem != null) return false;
        if (capdCalcium != null ? !capdCalcium.equals(that.capdCalcium) : that.capdCalcium != null) return false;
        if (capdConc != null ? !capdConc.equals(that.capdConc) : that.capdConc != null) return false;
        if (durationInputType != null ? !durationInputType.equals(that.durationInputType) : that.durationInputType != null)
            return false;
        if (capdBaseunit != null ? !capdBaseunit.equals(that.capdBaseunit) : that.capdBaseunit != null) return false;
        if (siteDesc != null ? !siteDesc.equals(that.siteDesc) : that.siteDesc != null) return false;
        if (remarkCreateBy != null ? !remarkCreateBy.equals(that.remarkCreateBy) : that.remarkCreateBy != null)
            return false;
        if (remarkCreateDate != null ? !remarkCreateDate.equals(that.remarkCreateDate) : that.remarkCreateDate != null)
            return false;
        if (remarkText != null ? !remarkText.equals(that.remarkText) : that.remarkText != null) return false;
        if (remarkConfirmBy != null ? !remarkConfirmBy.equals(that.remarkConfirmBy) : that.remarkConfirmBy != null)
            return false;
        if (remarkConfirmDate != null ? !remarkConfirmDate.equals(that.remarkConfirmDate) : that.remarkConfirmDate != null)
            return false;
        if (trueConc != null ? !trueConc.equals(that.trueConc) : that.trueConc != null) return false;
        if (mdsInfo != null ? !mdsInfo.equals(that.mdsInfo) : that.mdsInfo != null) return false;
        if (adminTimeDesc != null ? !adminTimeDesc.equals(that.adminTimeDesc) : that.adminTimeDesc != null)
            return false;
        if (commentCreateBy != null ? !commentCreateBy.equals(that.commentCreateBy) : that.commentCreateBy != null)
            return false;
        if (commentCreateDate != null ? !commentCreateDate.equals(that.commentCreateDate) : that.commentCreateDate != null)
            return false;
        if (updateBy != null ? !updateBy.equals(that.updateBy) : that.updateBy != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hospcode != null ? hospcode.hashCode() : 0;
        result = 31 * result + (int) (ordNo ^ (ordNo >>> 32));
        result = 31 * result + (int) (cmsItemNo ^ (cmsItemNo >>> 32));
        result = 31 * result + (patHospcode != null ? patHospcode.hashCode() : 0);
        result = 31 * result + (caseNo != null ? caseNo.hashCode() : 0);
        result = 31 * result + (regimen != null ? regimen.hashCode() : 0);
        result = 31 * result + (multDose != null ? multDose.hashCode() : 0);
        result = 31 * result + (volValue != null ? volValue.hashCode() : 0);
        result = 31 * result + (volUnit != null ? volUnit.hashCode() : 0);
        result = 31 * result + (volText != null ? volText.hashCode() : 0);
        result = 31 * result + (itemcode != null ? itemcode.hashCode() : 0);
        result = 31 * result + (firstDisplayname != null ? firstDisplayname.hashCode() : 0);
        result = 31 * result + (tradename != null ? tradename.hashCode() : 0);
        result = 31 * result + (strength != null ? strength.hashCode() : 0);
        result = 31 * result + (baseunit != null ? baseunit.hashCode() : 0);
        result = 31 * result + (formcode != null ? formcode.hashCode() : 0);
        result = 31 * result + (routeCode != null ? routeCode.hashCode() : 0);
        result = 31 * result + (routeDesc != null ? routeDesc.hashCode() : 0);
        result = 31 * result + (formDesc != null ? formDesc.hashCode() : 0);
        result = 31 * result + (salt != null ? salt.hashCode() : 0);
        result = 31 * result + (extraInfo != null ? extraInfo.hashCode() : 0);
        result = 31 * result + (dangerdrug != null ? dangerdrug.hashCode() : 0);
        result = 31 * result + (externalUse != null ? externalUse.hashCode() : 0);
        result = 31 * result + (nameType != null ? nameType.hashCode() : 0);
        result = 31 * result + (secondDisplayname != null ? secondDisplayname.hashCode() : 0);
        result = 31 * result + (dosage != null ? dosage.hashCode() : 0);
        result = 31 * result + (modu != null ? modu.hashCode() : 0);
        result = 31 * result + (freqCode != null ? freqCode.hashCode() : 0);
        result = 31 * result + (freq1 != null ? freq1.hashCode() : 0);
        result = 31 * result + (supFreqCode != null ? supFreqCode.hashCode() : 0);
        result = 31 * result + (supFreq1 != null ? supFreq1.hashCode() : 0);
        result = 31 * result + (supFreq2 != null ? supFreq2.hashCode() : 0);
        result = 31 * result + (dayOfWeek != null ? dayOfWeek.hashCode() : 0);
        result = 31 * result + (adminTimeCode != null ? adminTimeCode.hashCode() : 0);
        result = 31 * result + (prn != null ? prn.hashCode() : 0);
        result = 31 * result + (prnPercent != null ? prnPercent.hashCode() : 0);
        result = 31 * result + (siteCode != null ? siteCode.hashCode() : 0);
        result = 31 * result + (supSiteDesc != null ? supSiteDesc.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (durationUnit != null ? durationUnit.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (moQty != null ? moQty.hashCode() : 0);
        result = 31 * result + (moQtyUnit != null ? moQtyUnit.hashCode() : 0);
        result = 31 * result + (actionStatus != null ? actionStatus.hashCode() : 0);
        result = 31 * result + (formulStatus != null ? formulStatus.hashCode() : 0);
        result = 31 * result + (formulStatus2 != null ? formulStatus2.hashCode() : 0);
        result = 31 * result + (patType != null ? patType.hashCode() : 0);
        result = 31 * result + (itemStatus != null ? itemStatus.hashCode() : 0);
        result = 31 * result + (specInstruct != null ? specInstruct.hashCode() : 0);
        result = 31 * result + (specNote != null ? specNote.hashCode() : 0);
        result = 31 * result + (translate != null ? translate.hashCode() : 0);
        result = 31 * result + (fixPeriod != null ? fixPeriod.hashCode() : 0);
        result = 31 * result + (restricted != null ? restricted.hashCode() : 0);
        result = 31 * result + (singleUse != null ? singleUse.hashCode() : 0);
        result = 31 * result + (moCreate != null ? moCreate.hashCode() : 0);
        result = 31 * result + (displayRoutedesc != null ? displayRoutedesc.hashCode() : 0);
        result = 31 * result + (trueDisplayname != null ? trueDisplayname.hashCode() : 0);
        result = 31 * result + (int) (orgItemNo ^ (orgItemNo >>> 32));
        result = 31 * result + (allowRepeat != null ? allowRepeat.hashCode() : 0);
        result = 31 * result + (freqText != null ? freqText.hashCode() : 0);
        result = 31 * result + (supFreqText != null ? supFreqText.hashCode() : 0);
        result = 31 * result + (trueAliasname != null ? trueAliasname.hashCode() : 0);
        result = 31 * result + (capdSystem != null ? capdSystem.hashCode() : 0);
        result = 31 * result + (capdCalcium != null ? capdCalcium.hashCode() : 0);
        result = 31 * result + (capdConc != null ? capdConc.hashCode() : 0);
        result = 31 * result + (durationInputType != null ? durationInputType.hashCode() : 0);
        result = 31 * result + (capdBaseunit != null ? capdBaseunit.hashCode() : 0);
        result = 31 * result + (siteDesc != null ? siteDesc.hashCode() : 0);
        result = 31 * result + (remarkCreateBy != null ? remarkCreateBy.hashCode() : 0);
        result = 31 * result + (remarkCreateDate != null ? remarkCreateDate.hashCode() : 0);
        result = 31 * result + (remarkText != null ? remarkText.hashCode() : 0);
        result = 31 * result + (remarkConfirmBy != null ? remarkConfirmBy.hashCode() : 0);
        result = 31 * result + (remarkConfirmDate != null ? remarkConfirmDate.hashCode() : 0);
        result = 31 * result + (trueConc != null ? trueConc.hashCode() : 0);
        result = 31 * result + (mdsInfo != null ? mdsInfo.hashCode() : 0);
        result = 31 * result + (adminTimeDesc != null ? adminTimeDesc.hashCode() : 0);
        result = 31 * result + (commentCreateBy != null ? commentCreateBy.hashCode() : 0);
        result = 31 * result + (commentCreateDate != null ? commentCreateDate.hashCode() : 0);
        result = 31 * result + (updateBy != null ? updateBy.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        return result;
    }
}
