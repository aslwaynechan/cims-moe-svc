package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeEhrMedProfileLogPoPK implements Serializable {
    private static final long serialVersionUID = -4914156019302298430L;
    private String hospcode;
    private long ordNo;
    private long cmsItemNo;
    private String refNo;

    @Column(name = "HOSPCODE")
    @Id
    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    @Column(name = "ORD_NO")
    @Id
    public long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(long ordNo) {
        this.ordNo = ordNo;
    }

    @Column(name = "CMS_ITEM_NO")
    @Id
    public long getCmsItemNo() {
        return cmsItemNo;
    }

    public void setCmsItemNo(long cmsItemNo) {
        this.cmsItemNo = cmsItemNo;
    }

    @Column(name = "REF_NO")
    @Id
    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeEhrMedProfileLogPoPK that = (MoeEhrMedProfileLogPoPK) o;

        if (ordNo != that.ordNo) return false;
        if (cmsItemNo != that.cmsItemNo) return false;
        if (hospcode != null ? !hospcode.equals(that.hospcode) : that.hospcode != null) return false;
        if (refNo != null ? !refNo.equals(that.refNo) : that.refNo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hospcode != null ? hospcode.hashCode() : 0;
        result = 31 * result + (int) (ordNo ^ (ordNo >>> 32));
        result = 31 * result + (int) (cmsItemNo ^ (cmsItemNo >>> 32));
        result = 31 * result + (refNo != null ? refNo.hashCode() : 0);
        return result;
    }
}
