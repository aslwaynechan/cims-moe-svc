package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeDrugStrengthPoPK implements Serializable {
    private static final long serialVersionUID = 7708328659970637759L;
    private String hkRegNo;
    private long rank;

    @Column(name = "HK_REG_NO")
    @Id
    public String getHkRegNo() {
        return hkRegNo;
    }

    public void setHkRegNo(String hkRegNo) {
        this.hkRegNo = hkRegNo;
    }

    @Column(name = "RANK")
    @Id
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDrugStrengthPoPK that = (MoeDrugStrengthPoPK) o;

        if (rank != that.rank) return false;
        if (hkRegNo != null ? !hkRegNo.equals(that.hkRegNo) : that.hkRegNo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hkRegNo != null ? hkRegNo.hashCode() : 0;
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        return result;
    }
}
