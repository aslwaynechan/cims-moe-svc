package hk.health.moe.pojo.po;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import java.io.Serializable;
import java.util.Objects;

public class MoeEhrOrderLogPoPK implements Serializable {

    private static final long serialVersionUID = 6642926320177269470L;
    private MoeOrderLogPo moeOrderLog;

    @Id
    @OneToOne(fetch = FetchType.LAZY, cascade= CascadeType.ALL, orphanRemoval=true)
    @JoinColumns({
            @JoinColumn(name = "ORD_NO", referencedColumnName = "ORD_NO"),
            @JoinColumn(name = "REF_NO", referencedColumnName = "REF_NO")
    })
    public MoeOrderLogPo getMoeOrderLog() {
        return this.moeOrderLog;
    }

    public void setMoeOrderLog(MoeOrderLogPo moeOrderLog) {
        this.moeOrderLog = moeOrderLog;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MoeEhrOrderLogPoPK)) return false;
        MoeEhrOrderLogPoPK that = (MoeEhrOrderLogPoPK) o;
        return moeOrderLog.equals(that.moeOrderLog);
    }

    @Override
    public int hashCode() {
        return Objects.hash(moeOrderLog);
    }

/*    private long ordNo;
    private String refNo;

    @Column(name = "ORD_NO")
    @Id
    public long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(long ordNo) {
        this.ordNo = ordNo;
    }

    @Column(name = "REF_NO")
    @Id
    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }*/

/*    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeEhrOrderLogPoPK that = (MoeEhrOrderLogPoPK) o;

        if (ordNo != that.ordNo) return false;
        if (refNo != null ? !refNo.equals(that.refNo) : that.refNo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (ordNo ^ (ordNo >>> 32));
        result = 31 * result + (refNo != null ? refNo.hashCode() : 0);
        return result;
    }*/

}
