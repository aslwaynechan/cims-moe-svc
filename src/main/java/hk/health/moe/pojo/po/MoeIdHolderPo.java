package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_ID_HOLDER")
public class MoeIdHolderPo implements Serializable {
    private static final long serialVersionUID = 1642747513098231471L;
    private String idName;
    private long nextId;

    @Id
    @Column(name = "ID_NAME")
    public String getIdName() {
        return idName;
    }

    public void setIdName(String idName) {
        this.idName = idName;
    }

    @Basic
    @Column(name = "NEXT_ID")
    public long getNextId() {
        return nextId;
    }

    public void setNextId(long nextId) {
        this.nextId = nextId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeIdHolderPo that = (MoeIdHolderPo) o;

        if (nextId != that.nextId) return false;
        if (idName != null ? !idName.equals(that.idName) : that.idName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idName != null ? idName.hashCode() : 0;
        result = 31 * result + (int) (nextId ^ (nextId >>> 32));
        return result;
    }
}
