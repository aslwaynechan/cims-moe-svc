package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeUserSettingPoPK implements Serializable {
    private static final long serialVersionUID = -975155277736875029L;
    private String loginId;
    private String paramName;

    @Column(name = "LOGIN_ID")
    @Id
    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    @Column(name = "PARAM_NAME")
    @Id
    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeUserSettingPoPK that = (MoeUserSettingPoPK) o;

        if (loginId != null ? !loginId.equals(that.loginId) : that.loginId != null) return false;
        if (paramName != null ? !paramName.equals(that.paramName) : that.paramName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = loginId != null ? loginId.hashCode() : 0;
        result = 31 * result + (paramName != null ? paramName.hashCode() : 0);
        return result;
    }
}
