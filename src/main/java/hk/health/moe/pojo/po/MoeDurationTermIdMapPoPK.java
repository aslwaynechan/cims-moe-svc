package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeDurationTermIdMapPoPK implements Serializable {
    private static final long serialVersionUID = -3475026196576469414L;
    private String durationUnit;
    private long ehrTermId;

    @Column(name = "DURATION_UNIT")
    @Id
    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    @Column(name = "EHR_TERM_ID")
    @Id
    public long getEhrTermId() {
        return ehrTermId;
    }

    public void setEhrTermId(long ehrTermId) {
        this.ehrTermId = ehrTermId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDurationTermIdMapPoPK that = (MoeDurationTermIdMapPoPK) o;

        if (ehrTermId != that.ehrTermId) return false;
        if (durationUnit != null ? !durationUnit.equals(that.durationUnit) : that.durationUnit != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = durationUnit != null ? durationUnit.hashCode() : 0;
        result = 31 * result + (int) (ehrTermId ^ (ehrTermId >>> 32));
        return result;
    }
}
