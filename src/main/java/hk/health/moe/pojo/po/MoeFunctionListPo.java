package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_FUNCTION_LIST")
public class MoeFunctionListPo implements Serializable {
    private static final long serialVersionUID = -9196140084278251812L;
    private String functionId;
    private String functionName;
    private String systemMaintenance;

    @Id
    @Column(name = "FUNCTION_ID")
    public String getFunctionId() {
        return functionId;
    }

    public void setFunctionId(String functionId) {
        this.functionId = functionId;
    }

    @Basic
    @Column(name = "FUNCTION_NAME")
    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    @Basic
    @Column(name = "SYSTEM_MAINTENANCE")
    public String getSystemMaintenance() {
        return systemMaintenance;
    }

    public void setSystemMaintenance(String systemMaintenance) {
        this.systemMaintenance = systemMaintenance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeFunctionListPo that = (MoeFunctionListPo) o;

        if (functionId != null ? !functionId.equals(that.functionId) : that.functionId != null) return false;
        if (functionName != null ? !functionName.equals(that.functionName) : that.functionName != null) return false;
        if (systemMaintenance != null ? !systemMaintenance.equals(that.systemMaintenance) : that.systemMaintenance != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = functionId != null ? functionId.hashCode() : 0;
        result = 31 * result + (functionName != null ? functionName.hashCode() : 0);
        result = 31 * result + (systemMaintenance != null ? systemMaintenance.hashCode() : 0);
        return result;
    }
}
