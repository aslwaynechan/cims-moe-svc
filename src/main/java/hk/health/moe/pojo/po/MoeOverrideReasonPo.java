package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_OVERRIDE_REASON")
@IdClass(MoeOverrideReasonPoPK.class)
public class MoeOverrideReasonPo implements Serializable {
    private static final long serialVersionUID = 5600424088572611406L;
    private String reasonType;
    private String reasonCode;
    private String reasonCodeType;
    private String reasonDesc;
    private long rank;

    @Id
    @Column(name = "REASON_TYPE")
    public String getReasonType() {
        return reasonType;
    }

    public void setReasonType(String reasonType) {
        this.reasonType = reasonType;
    }

    @Id
    @Column(name = "REASON_CODE")
    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    @Basic
    @Column(name = "REASON_CODE_TYPE")
    public String getReasonCodeType() {
        return reasonCodeType;
    }

    public void setReasonCodeType(String reasonCodeType) {
        this.reasonCodeType = reasonCodeType;
    }

    @Basic
    @Column(name = "REASON_DESC")
    public String getReasonDesc() {
        return reasonDesc;
    }

    public void setReasonDesc(String reasonDesc) {
        this.reasonDesc = reasonDesc;
    }

    @Basic
    @Column(name = "RANK")
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeOverrideReasonPo that = (MoeOverrideReasonPo) o;

        if (rank != that.rank) return false;
        if (reasonType != null ? !reasonType.equals(that.reasonType) : that.reasonType != null) return false;
        if (reasonCode != null ? !reasonCode.equals(that.reasonCode) : that.reasonCode != null) return false;
        if (reasonCodeType != null ? !reasonCodeType.equals(that.reasonCodeType) : that.reasonCodeType != null)
            return false;
        if (reasonDesc != null ? !reasonDesc.equals(that.reasonDesc) : that.reasonDesc != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = reasonType != null ? reasonType.hashCode() : 0;
        result = 31 * result + (reasonCode != null ? reasonCode.hashCode() : 0);
        result = 31 * result + (reasonCodeType != null ? reasonCodeType.hashCode() : 0);
        result = 31 * result + (reasonDesc != null ? reasonDesc.hashCode() : 0);
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        return result;
    }
}
