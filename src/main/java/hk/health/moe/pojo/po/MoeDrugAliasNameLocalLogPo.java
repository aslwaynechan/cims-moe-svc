package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "MOE_DRUG_ALIAS_NAME_LOCAL_LOG")
@IdClass(MoeDrugAliasNameLocalLogPoPK.class)
public class MoeDrugAliasNameLocalLogPo implements Serializable {
    private static final long serialVersionUID = -4870571896783223408L;
    private String aliasNameId;
    private String hkRegNo;
    private String aliasName;
    private String aliasNameType;
    private String createUserId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private Date createDtm;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private Date updateDtm;
    private long version;
    private String actionType;
    private Date actionTime;

    public MoeDrugAliasNameLocalLogPo() {
    }

    public MoeDrugAliasNameLocalLogPo(String actionType, MoeDrugAliasNameLocalPo record) {
        aliasNameId = record.getAliasNameId();
        hkRegNo = record.getHkRegNo();
        aliasName = record.getAliasName();
        aliasNameType = record.getAliasNameType();
        createUserId = record.getCreateUserId();
        createUser = record.getCreateUser();
        createHosp = record.getCreateHosp();
        createRank = record.getCreateRank();
        createRankDesc = record.getCreateRankDesc();
        createDtm = record.getCreateDtm();
        updateUserId = record.getUpdateUserId();
        updateUser = record.getUpdateUser();
        updateHosp = record.getUpdateHosp();
        updateRank = record.getUpdateRank();
        updateRankDesc = record.getUpdateRankDesc();
        updateDtm = record.getUpdateDtm();
        version = record.getVersion();
        this.actionType = actionType;
        actionTime = new Date();
    }

    @Id
    @Column(name = "ALIAS_NAME_ID")
    public String getAliasNameId() {
        return aliasNameId;
    }

    public void setAliasNameId(String aliasNameId) {
        this.aliasNameId = aliasNameId;
    }

    @Id
    @Column(name = "HK_REG_NO")
    public String getHkRegNo() {
        return hkRegNo;
    }

    public void setHkRegNo(String hkRegNo) {
        this.hkRegNo = hkRegNo;
    }

    @Id
    @Column(name = "ALIAS_NAME")
    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    @Id
    @Column(name = "ALIAS_NAME_TYPE")
    public String getAliasNameType() {
        return aliasNameType;
    }

    public void setAliasNameType(String aliasNameType) {
        this.aliasNameType = aliasNameType;
    }

    @Id
    @Column(name = "CREATE_USER_ID")
    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    @Id
    @Column(name = "CREATE_USER")
    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    @Id
    @Column(name = "CREATE_HOSP")
    public String getCreateHosp() {
        return createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    @Id
    @Column(name = "CREATE_RANK")
    public String getCreateRank() {
        return createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    @Id
    @Column(name = "CREATE_RANK_DESC")
    public String getCreateRankDesc() {
        return createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    @Id
    @Column(name = "CREATE_DTM")
    public Date getCreateDtm() {
        return createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    @Id
    @Column(name = "UPDATE_USER_ID")
    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Id
    @Column(name = "UPDATE_USER")
    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Id
    @Column(name = "UPDATE_HOSP")
    public String getUpdateHosp() {
        return updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    @Id
    @Column(name = "UPDATE_RANK")
    public String getUpdateRank() {
        return updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    @Id
    @Column(name = "UPDATE_RANK_DESC")
    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    @Id
    @Column(name = "UPDATE_DTM")
    public Date getUpdateDtm() {
        return updateDtm;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }

    @Id
    @Column(name = "VERSION")
    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    @Id
    @Column(name = "ACTION_TYPE")
    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    @Id
    @Column(name = "ACTION_TIME")
    public Date getActionTime() {
        return actionTime;
    }

    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDrugAliasNameLocalLogPo that = (MoeDrugAliasNameLocalLogPo) o;

        if (version != that.version) return false;
        if (aliasNameId != null ? !aliasNameId.equals(that.aliasNameId) : that.aliasNameId != null) return false;
        if (hkRegNo != null ? !hkRegNo.equals(that.hkRegNo) : that.hkRegNo != null) return false;
        if (aliasName != null ? !aliasName.equals(that.aliasName) : that.aliasName != null) return false;
        if (aliasNameType != null ? !aliasNameType.equals(that.aliasNameType) : that.aliasNameType != null)
            return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null) return false;
        if (createUser != null ? !createUser.equals(that.createUser) : that.createUser != null) return false;
        if (createHosp != null ? !createHosp.equals(that.createHosp) : that.createHosp != null) return false;
        if (createRank != null ? !createRank.equals(that.createRank) : that.createRank != null) return false;
        if (createRankDesc != null ? !createRankDesc.equals(that.createRankDesc) : that.createRankDesc != null)
            return false;
        if (createDtm != null ? !createDtm.equals(that.createDtm) : that.createDtm != null) return false;
        if (updateUserId != null ? !updateUserId.equals(that.updateUserId) : that.updateUserId != null) return false;
        if (updateUser != null ? !updateUser.equals(that.updateUser) : that.updateUser != null) return false;
        if (updateHosp != null ? !updateHosp.equals(that.updateHosp) : that.updateHosp != null) return false;
        if (updateRank != null ? !updateRank.equals(that.updateRank) : that.updateRank != null) return false;
        if (updateRankDesc != null ? !updateRankDesc.equals(that.updateRankDesc) : that.updateRankDesc != null)
            return false;
        if (updateDtm != null ? !updateDtm.equals(that.updateDtm) : that.updateDtm != null) return false;
        if (actionType != null ? !actionType.equals(that.actionType) : that.actionType != null) return false;
        if (actionTime != null ? !actionTime.equals(that.actionTime) : that.actionTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = aliasNameId != null ? aliasNameId.hashCode() : 0;
        result = 31 * result + (hkRegNo != null ? hkRegNo.hashCode() : 0);
        result = 31 * result + (aliasName != null ? aliasName.hashCode() : 0);
        result = 31 * result + (aliasNameType != null ? aliasNameType.hashCode() : 0);
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (createUser != null ? createUser.hashCode() : 0);
        result = 31 * result + (createHosp != null ? createHosp.hashCode() : 0);
        result = 31 * result + (createRank != null ? createRank.hashCode() : 0);
        result = 31 * result + (createRankDesc != null ? createRankDesc.hashCode() : 0);
        result = 31 * result + (createDtm != null ? createDtm.hashCode() : 0);
        result = 31 * result + (updateUserId != null ? updateUserId.hashCode() : 0);
        result = 31 * result + (updateUser != null ? updateUser.hashCode() : 0);
        result = 31 * result + (updateHosp != null ? updateHosp.hashCode() : 0);
        result = 31 * result + (updateRank != null ? updateRank.hashCode() : 0);
        result = 31 * result + (updateRankDesc != null ? updateRankDesc.hashCode() : 0);
        result = 31 * result + (updateDtm != null ? updateDtm.hashCode() : 0);
        result = 31 * result + (int) (version ^ (version >>> 32));
        result = 31 * result + (actionType != null ? actionType.hashCode() : 0);
        result = 31 * result + (actionTime != null ? actionTime.hashCode() : 0);
        return result;
    }
}
