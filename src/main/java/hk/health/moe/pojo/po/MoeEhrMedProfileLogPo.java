package hk.health.moe.pojo.po;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "MOE_EHR_MED_PROFILE_LOG")
// Chris 20190808  -Start
//@IdClass(MoeEhrMedProfileLogPoPK.class)
@IdClass(MoeMedProfileLogPoPK.class)
// Chris 20190808  -End
public class MoeEhrMedProfileLogPo implements Serializable {
    private static final long serialVersionUID = 4951577260949592636L;
    private String hospcode;
    private long ordNo;
    private long cmsItemNo;
    private Long drugId;
    private String vtm;
    private Long vtmId;
    private String tradeName;
    private Long tradeNameVtmId;
    private String aliasName;
    private String aliasNameType;
    private String manufacturer;
    private String screenDisplay;
    private String orderLineType;
    private String genericIndicator;
    private Long cycleMultiplier;
    private Long formId;
    private Long doseFormExtraInfoId;
    private String doseFormExtraInfo;
    private Long routeId;
    private Long freqId;
    private Long supplFreqId;
    private Long moQtyUnitId;
    private Long moduId;
    private Long siteId;
    private Long drugRouteId;
    private String strengthLevelExtraInfo;
    private String strengthCompulsory;
    private String drugRouteEng;
    private String createUserId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private Date createDtm;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private Date updateDtm;
    private String refNo;
    private String trxType;
    private String medDiscontFlag;
    private String medDiscontReasonCode;
    private String medDiscontInformation;
    private String medDiscontUserId;
    private String medDiscontUser;
    private String medDiscontHosp;
    private Date medDiscontDtm;
    private MoeMedProfileLogPo moeMedProfileLog;
    private MoeDrugTermDescIdMapPo moeDrugTermDescIdMap;

    @Id
    @Column(name = "HOSPCODE")
    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    @Id
    @Column(name = "ORD_NO")
    public long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(long ordNo) {
        this.ordNo = ordNo;
    }

    @Id
    @Column(name = "CMS_ITEM_NO")
    public long getCmsItemNo() {
        return cmsItemNo;
    }

    public void setCmsItemNo(long cmsItemNo) {
        this.cmsItemNo = cmsItemNo;
    }

    @Basic
    @Column(name = "DRUG_ID")
    public Long getDrugId() {
        return drugId;
    }

    public void setDrugId(Long drugId) {
        this.drugId = drugId;
    }

    @Basic
    @Column(name = "VTM")
    public String getVtm() {
        return vtm;
    }

    public void setVtm(String vtm) {
        this.vtm = vtm;
    }

    @Basic
    @Column(name = "VTM_ID")
    public Long getVtmId() {
        return vtmId;
    }

    public void setVtmId(Long vtmId) {
        this.vtmId = vtmId;
    }

    @Basic
    @Column(name = "TRADE_NAME")
    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    @Basic
    @Column(name = "TRADE_NAME_VTM_ID")
    public Long getTradeNameVtmId() {
        return tradeNameVtmId;
    }

    public void setTradeNameVtmId(Long tradeNameVtmId) {
        this.tradeNameVtmId = tradeNameVtmId;
    }

    @Basic
    @Column(name = "ALIAS_NAME")
    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    @Basic
    @Column(name = "ALIAS_NAME_TYPE")
    public String getAliasNameType() {
        return aliasNameType;
    }

    public void setAliasNameType(String aliasNameType) {
        this.aliasNameType = aliasNameType;
    }

    @Basic
    @Column(name = "MANUFACTURER")
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Basic
    @Column(name = "SCREEN_DISPLAY")
    public String getScreenDisplay() {
        return screenDisplay;
    }

    public void setScreenDisplay(String screenDisplay) {
        this.screenDisplay = screenDisplay;
    }

    @Basic
    @Column(name = "ORDER_LINE_TYPE")
    public String getOrderLineType() {
        return orderLineType;
    }

    public void setOrderLineType(String orderLineType) {
        this.orderLineType = orderLineType;
    }

    @Basic
    @Column(name = "GENERIC_INDICATOR")
    public String getGenericIndicator() {
        return genericIndicator;
    }

    public void setGenericIndicator(String genericIndicator) {
        this.genericIndicator = genericIndicator;
    }

    @Basic
    @Column(name = "CYCLE_MULTIPLIER")
    public Long getCycleMultiplier() {
        return cycleMultiplier;
    }

    public void setCycleMultiplier(Long cycleMultiplier) {
        this.cycleMultiplier = cycleMultiplier;
    }

    @Basic
    @Column(name = "FORM_ID")
    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    @Basic
    @Column(name = "DOSE_FORM_EXTRA_INFO_ID")
    public Long getDoseFormExtraInfoId() {
        return doseFormExtraInfoId;
    }

    public void setDoseFormExtraInfoId(Long doseFormExtraInfoId) {
        this.doseFormExtraInfoId = doseFormExtraInfoId;
    }

    @Basic
    @Column(name = "DOSE_FORM_EXTRA_INFO")
    public String getDoseFormExtraInfo() {
        return doseFormExtraInfo;
    }

    public void setDoseFormExtraInfo(String doseFormExtraInfo) {
        this.doseFormExtraInfo = doseFormExtraInfo;
    }

    @Basic
    @Column(name = "ROUTE_ID")
    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    @Basic
    @Column(name = "FREQ_ID")
    public Long getFreqId() {
        return freqId;
    }

    public void setFreqId(Long freqId) {
        this.freqId = freqId;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_ID")
    public Long getSupplFreqId() {
        return supplFreqId;
    }

    public void setSupplFreqId(Long supplFreqId) {
        this.supplFreqId = supplFreqId;
    }

    @Basic
    @Column(name = "MO_QTY_UNIT_ID")
    public Long getMoQtyUnitId() {
        return moQtyUnitId;
    }

    public void setMoQtyUnitId(Long moQtyUnitId) {
        this.moQtyUnitId = moQtyUnitId;
    }

    @Basic
    @Column(name = "MODU_ID")
    public Long getModuId() {
        return moduId;
    }

    public void setModuId(Long moduId) {
        this.moduId = moduId;
    }

    @Basic
    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    @Basic
    @Column(name = "DRUG_ROUTE_ID")
    public Long getDrugRouteId() {
        return drugRouteId;
    }

    public void setDrugRouteId(Long drugRouteId) {
        this.drugRouteId = drugRouteId;
    }

    @Basic
    @Column(name = "STRENGTH_LEVEL_EXTRA_INFO")
    public String getStrengthLevelExtraInfo() {
        return strengthLevelExtraInfo;
    }

    public void setStrengthLevelExtraInfo(String strengthLevelExtraInfo) {
        this.strengthLevelExtraInfo = strengthLevelExtraInfo;
    }

    @Basic
    @Column(name = "STRENGTH_COMPULSORY")
    public String getStrengthCompulsory() {
        return strengthCompulsory;
    }

    public void setStrengthCompulsory(String strengthCompulsory) {
        this.strengthCompulsory = strengthCompulsory;
    }

    @Basic
    @Column(name = "DRUG_ROUTE_ENG")
    public String getDrugRouteEng() {
        return drugRouteEng;
    }

    public void setDrugRouteEng(String drugRouteEng) {
        this.drugRouteEng = drugRouteEng;
    }

    @Basic
    @Column(name = "CREATE_USER_ID")
    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    @Basic
    @Column(name = "CREATE_USER")
    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    @Basic
    @Column(name = "CREATE_HOSP")
    public String getCreateHosp() {
        return createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    @Basic
    @Column(name = "CREATE_RANK")
    public String getCreateRank() {
        return createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    @Basic
    @Column(name = "CREATE_RANK_DESC")
    public String getCreateRankDesc() {
        return createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    @Basic
    @Column(name = "CREATE_DTM")
    public Date getCreateDtm() {
        return createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    @Basic
    @Column(name = "UPDATE_USER_ID")
    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Basic
    @Column(name = "UPDATE_USER")
    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Basic
    @Column(name = "UPDATE_HOSP")
    public String getUpdateHosp() {
        return updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    @Basic
    @Column(name = "UPDATE_RANK")
    public String getUpdateRank() {
        return updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    @Basic
    @Column(name = "UPDATE_RANK_DESC")
    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    @Basic
    @Column(name = "UPDATE_DTM")
    public Date getUpdateDtm() {
        return updateDtm;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }

    @Id
    @Column(name = "REF_NO")
    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    @Basic
    @Column(name = "TRX_TYPE")
    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    @Basic
    @Column(name = "MED_DISCONT_FLAG")
    public String getMedDiscontFlag() {
        return medDiscontFlag;
    }

    public void setMedDiscontFlag(String medDiscontFlag) {
        this.medDiscontFlag = medDiscontFlag;
    }

    @Basic
    @Column(name = "MED_DISCONT_REASON_CODE")
    public String getMedDiscontReasonCode() {
        return medDiscontReasonCode;
    }

    public void setMedDiscontReasonCode(String medDiscontReasonCode) {
        this.medDiscontReasonCode = medDiscontReasonCode;
    }

    @Basic
    @Column(name = "MED_DISCONT_INFORMATION")
    public String getMedDiscontInformation() {
        return medDiscontInformation;
    }

    public void setMedDiscontInformation(String medDiscontInformation) {
        this.medDiscontInformation = medDiscontInformation;
    }

    @Basic
    @Column(name = "MED_DISCONT_USER_ID")
    public String getMedDiscontUserId() {
        return medDiscontUserId;
    }

    public void setMedDiscontUserId(String medDiscontUserId) {
        this.medDiscontUserId = medDiscontUserId;
    }

    @Basic
    @Column(name = "MED_DISCONT_USER")
    public String getMedDiscontUser() {
        return medDiscontUser;
    }

    public void setMedDiscontUser(String medDiscontUser) {
        this.medDiscontUser = medDiscontUser;
    }

    @Basic
    @Column(name = "MED_DISCONT_HOSP")
    public String getMedDiscontHosp() {
        return medDiscontHosp;
    }

    public void setMedDiscontHosp(String medDiscontHosp) {
        this.medDiscontHosp = medDiscontHosp;
    }

    @Basic
    @Column(name = "MED_DISCONT_DTM")
    public Date getMedDiscontDtm() {
        return medDiscontDtm;
    }

    public void setMedDiscontDtm(Date medDiscontDtm) {
        this.medDiscontDtm = medDiscontDtm;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public MoeMedProfileLogPo getMoeMedProfileLog() {
        return this.moeMedProfileLog;
    }

    public void setMoeMedProfileLog(MoeMedProfileLogPo moeMedProfileLog) {
        this.moeMedProfileLog = moeMedProfileLog;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "drug_id", referencedColumnName = "term_id", nullable = false, insertable = false, updatable = false)
    @NotFound(action= NotFoundAction.IGNORE)
    public MoeDrugTermDescIdMapPo getMoeDrugTermDescIdMap(){
        return moeDrugTermDescIdMap;
    }

    public void setMoeDrugTermDescIdMap(MoeDrugTermDescIdMapPo moeDrugTermDescIdMap){
        this.moeDrugTermDescIdMap = moeDrugTermDescIdMap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeEhrMedProfileLogPo that = (MoeEhrMedProfileLogPo) o;

        if (ordNo != that.ordNo) return false;
        if (cmsItemNo != that.cmsItemNo) return false;
        if (hospcode != null ? !hospcode.equals(that.hospcode) : that.hospcode != null) return false;
        if (drugId != null ? !drugId.equals(that.drugId) : that.drugId != null) return false;
        if (vtm != null ? !vtm.equals(that.vtm) : that.vtm != null) return false;
        if (vtmId != null ? !vtmId.equals(that.vtmId) : that.vtmId != null) return false;
        if (tradeName != null ? !tradeName.equals(that.tradeName) : that.tradeName != null) return false;
        if (tradeNameVtmId != null ? !tradeNameVtmId.equals(that.tradeNameVtmId) : that.tradeNameVtmId != null)
            return false;
        if (aliasName != null ? !aliasName.equals(that.aliasName) : that.aliasName != null) return false;
        if (aliasNameType != null ? !aliasNameType.equals(that.aliasNameType) : that.aliasNameType != null)
            return false;
        if (manufacturer != null ? !manufacturer.equals(that.manufacturer) : that.manufacturer != null) return false;
        if (screenDisplay != null ? !screenDisplay.equals(that.screenDisplay) : that.screenDisplay != null)
            return false;
        if (orderLineType != null ? !orderLineType.equals(that.orderLineType) : that.orderLineType != null)
            return false;
        if (genericIndicator != null ? !genericIndicator.equals(that.genericIndicator) : that.genericIndicator != null)
            return false;
        if (cycleMultiplier != null ? !cycleMultiplier.equals(that.cycleMultiplier) : that.cycleMultiplier != null)
            return false;
        if (formId != null ? !formId.equals(that.formId) : that.formId != null) return false;
        if (doseFormExtraInfoId != null ? !doseFormExtraInfoId.equals(that.doseFormExtraInfoId) : that.doseFormExtraInfoId != null)
            return false;
        if (doseFormExtraInfo != null ? !doseFormExtraInfo.equals(that.doseFormExtraInfo) : that.doseFormExtraInfo != null)
            return false;
        if (routeId != null ? !routeId.equals(that.routeId) : that.routeId != null) return false;
        if (freqId != null ? !freqId.equals(that.freqId) : that.freqId != null) return false;
        if (supplFreqId != null ? !supplFreqId.equals(that.supplFreqId) : that.supplFreqId != null) return false;
        if (moQtyUnitId != null ? !moQtyUnitId.equals(that.moQtyUnitId) : that.moQtyUnitId != null) return false;
        if (moduId != null ? !moduId.equals(that.moduId) : that.moduId != null) return false;
        if (siteId != null ? !siteId.equals(that.siteId) : that.siteId != null) return false;
        if (drugRouteId != null ? !drugRouteId.equals(that.drugRouteId) : that.drugRouteId != null) return false;
        if (strengthLevelExtraInfo != null ? !strengthLevelExtraInfo.equals(that.strengthLevelExtraInfo) : that.strengthLevelExtraInfo != null)
            return false;
        if (strengthCompulsory != null ? !strengthCompulsory.equals(that.strengthCompulsory) : that.strengthCompulsory != null)
            return false;
        if (drugRouteEng != null ? !drugRouteEng.equals(that.drugRouteEng) : that.drugRouteEng != null) return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null) return false;
        if (createUser != null ? !createUser.equals(that.createUser) : that.createUser != null) return false;
        if (createHosp != null ? !createHosp.equals(that.createHosp) : that.createHosp != null) return false;
        if (createRank != null ? !createRank.equals(that.createRank) : that.createRank != null) return false;
        if (createRankDesc != null ? !createRankDesc.equals(that.createRankDesc) : that.createRankDesc != null)
            return false;
        if (createDtm != null ? !createDtm.equals(that.createDtm) : that.createDtm != null) return false;
        if (updateUserId != null ? !updateUserId.equals(that.updateUserId) : that.updateUserId != null) return false;
        if (updateUser != null ? !updateUser.equals(that.updateUser) : that.updateUser != null) return false;
        if (updateHosp != null ? !updateHosp.equals(that.updateHosp) : that.updateHosp != null) return false;
        if (updateRank != null ? !updateRank.equals(that.updateRank) : that.updateRank != null) return false;
        if (updateRankDesc != null ? !updateRankDesc.equals(that.updateRankDesc) : that.updateRankDesc != null)
            return false;
        if (updateDtm != null ? !updateDtm.equals(that.updateDtm) : that.updateDtm != null) return false;
        if (refNo != null ? !refNo.equals(that.refNo) : that.refNo != null) return false;
        if (trxType != null ? !trxType.equals(that.trxType) : that.trxType != null) return false;
        if (medDiscontFlag != null ? !medDiscontFlag.equals(that.medDiscontFlag) : that.medDiscontFlag != null)
            return false;
        if (medDiscontReasonCode != null ? !medDiscontReasonCode.equals(that.medDiscontReasonCode) : that.medDiscontReasonCode != null)
            return false;
        if (medDiscontInformation != null ? !medDiscontInformation.equals(that.medDiscontInformation) : that.medDiscontInformation != null)
            return false;
        if (medDiscontUserId != null ? !medDiscontUserId.equals(that.medDiscontUserId) : that.medDiscontUserId != null)
            return false;
        if (medDiscontUser != null ? !medDiscontUser.equals(that.medDiscontUser) : that.medDiscontUser != null)
            return false;
        if (medDiscontHosp != null ? !medDiscontHosp.equals(that.medDiscontHosp) : that.medDiscontHosp != null)
            return false;
        if (medDiscontDtm != null ? !medDiscontDtm.equals(that.medDiscontDtm) : that.medDiscontDtm != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hospcode != null ? hospcode.hashCode() : 0;
        result = 31 * result + (int) (ordNo ^ (ordNo >>> 32));
        result = 31 * result + (int) (cmsItemNo ^ (cmsItemNo >>> 32));
        result = 31 * result + (drugId != null ? drugId.hashCode() : 0);
        result = 31 * result + (vtm != null ? vtm.hashCode() : 0);
        result = 31 * result + (vtmId != null ? vtmId.hashCode() : 0);
        result = 31 * result + (tradeName != null ? tradeName.hashCode() : 0);
        result = 31 * result + (tradeNameVtmId != null ? tradeNameVtmId.hashCode() : 0);
        result = 31 * result + (aliasName != null ? aliasName.hashCode() : 0);
        result = 31 * result + (aliasNameType != null ? aliasNameType.hashCode() : 0);
        result = 31 * result + (manufacturer != null ? manufacturer.hashCode() : 0);
        result = 31 * result + (screenDisplay != null ? screenDisplay.hashCode() : 0);
        result = 31 * result + (orderLineType != null ? orderLineType.hashCode() : 0);
        result = 31 * result + (genericIndicator != null ? genericIndicator.hashCode() : 0);
        result = 31 * result + (cycleMultiplier != null ? cycleMultiplier.hashCode() : 0);
        result = 31 * result + (formId != null ? formId.hashCode() : 0);
        result = 31 * result + (doseFormExtraInfoId != null ? doseFormExtraInfoId.hashCode() : 0);
        result = 31 * result + (doseFormExtraInfo != null ? doseFormExtraInfo.hashCode() : 0);
        result = 31 * result + (routeId != null ? routeId.hashCode() : 0);
        result = 31 * result + (freqId != null ? freqId.hashCode() : 0);
        result = 31 * result + (supplFreqId != null ? supplFreqId.hashCode() : 0);
        result = 31 * result + (moQtyUnitId != null ? moQtyUnitId.hashCode() : 0);
        result = 31 * result + (moduId != null ? moduId.hashCode() : 0);
        result = 31 * result + (siteId != null ? siteId.hashCode() : 0);
        result = 31 * result + (drugRouteId != null ? drugRouteId.hashCode() : 0);
        result = 31 * result + (strengthLevelExtraInfo != null ? strengthLevelExtraInfo.hashCode() : 0);
        result = 31 * result + (strengthCompulsory != null ? strengthCompulsory.hashCode() : 0);
        result = 31 * result + (drugRouteEng != null ? drugRouteEng.hashCode() : 0);
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (createUser != null ? createUser.hashCode() : 0);
        result = 31 * result + (createHosp != null ? createHosp.hashCode() : 0);
        result = 31 * result + (createRank != null ? createRank.hashCode() : 0);
        result = 31 * result + (createRankDesc != null ? createRankDesc.hashCode() : 0);
        result = 31 * result + (createDtm != null ? createDtm.hashCode() : 0);
        result = 31 * result + (updateUserId != null ? updateUserId.hashCode() : 0);
        result = 31 * result + (updateUser != null ? updateUser.hashCode() : 0);
        result = 31 * result + (updateHosp != null ? updateHosp.hashCode() : 0);
        result = 31 * result + (updateRank != null ? updateRank.hashCode() : 0);
        result = 31 * result + (updateRankDesc != null ? updateRankDesc.hashCode() : 0);
        result = 31 * result + (updateDtm != null ? updateDtm.hashCode() : 0);
        result = 31 * result + (refNo != null ? refNo.hashCode() : 0);
        result = 31 * result + (trxType != null ? trxType.hashCode() : 0);
        result = 31 * result + (medDiscontFlag != null ? medDiscontFlag.hashCode() : 0);
        result = 31 * result + (medDiscontReasonCode != null ? medDiscontReasonCode.hashCode() : 0);
        result = 31 * result + (medDiscontInformation != null ? medDiscontInformation.hashCode() : 0);
        result = 31 * result + (medDiscontUserId != null ? medDiscontUserId.hashCode() : 0);
        result = 31 * result + (medDiscontUser != null ? medDiscontUser.hashCode() : 0);
        result = 31 * result + (medDiscontHosp != null ? medDiscontHosp.hashCode() : 0);
        result = 31 * result + (medDiscontDtm != null ? medDiscontDtm.hashCode() : 0);
        return result;
    }
}
