/*
package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MOE_AUDIT_LOG_TYPE")
public class MoeAuditLogTypePo {
    private String logTypeId;
    private String logDescription;
    private String logComponent;
    private String successActionType;
    private String failActionType;
    private Long severity;

    @Id
    @Column(name = "LOG_TYPE_ID")
    public String getLogTypeId() {
        return logTypeId;
    }

    public void setLogTypeId(String logTypeId) {
        this.logTypeId = logTypeId;
    }

    @Basic
    @Column(name = "LOG_DESCRIPTION")
    public String getLogDescription() {
        return logDescription;
    }

    public void setLogDescription(String logDescription) {
        this.logDescription = logDescription;
    }

    @Basic
    @Column(name = "LOG_COMPONENT")
    public String getLogComponent() {
        return logComponent;
    }

    public void setLogComponent(String logComponent) {
        this.logComponent = logComponent;
    }

    @Basic
    @Column(name = "SUCCESS_ACTION_TYPE")
    public String getSuccessActionType() {
        return successActionType;
    }

    public void setSuccessActionType(String successActionType) {
        this.successActionType = successActionType;
    }

    @Basic
    @Column(name = "FAIL_ACTION_TYPE")
    public String getFailActionType() {
        return failActionType;
    }

    public void setFailActionType(String failActionType) {
        this.failActionType = failActionType;
    }

    @Basic
    @Column(name = "SEVERITY")
    public Long getSeverity() {
        return severity;
    }

    public void setSeverity(Long severity) {
        this.severity = severity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeAuditLogTypePo that = (MoeAuditLogTypePo) o;

        if (logTypeId != null ? !logTypeId.equals(that.logTypeId) : that.logTypeId != null) return false;
        if (logDescription != null ? !logDescription.equals(that.logDescription) : that.logDescription != null)
            return false;
        if (logComponent != null ? !logComponent.equals(that.logComponent) : that.logComponent != null) return false;
        if (successActionType != null ? !successActionType.equals(that.successActionType) : that.successActionType != null)
            return false;
        if (failActionType != null ? !failActionType.equals(that.failActionType) : that.failActionType != null)
            return false;
        if (severity != null ? !severity.equals(that.severity) : that.severity != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = logTypeId != null ? logTypeId.hashCode() : 0;
        result = 31 * result + (logDescription != null ? logDescription.hashCode() : 0);
        result = 31 * result + (logComponent != null ? logComponent.hashCode() : 0);
        result = 31 * result + (successActionType != null ? successActionType.hashCode() : 0);
        result = 31 * result + (failActionType != null ? failActionType.hashCode() : 0);
        result = 31 * result + (severity != null ? severity.hashCode() : 0);
        return result;
    }
}
*/
