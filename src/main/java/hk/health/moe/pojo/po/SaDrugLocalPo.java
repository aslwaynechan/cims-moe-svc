package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "SA_DRUG_LOCAL")
public class SaDrugLocalPo {
    private String allergenId;
    private String hkRegNo;
    private String vtm;
    private String tradeName;
    private String formerBan;
    private String abbreviation;
    private String otherName;
    private Date createDtm;
    private Date updateDtm;
    private String genericProductInd;

    @Id
    @Column(name = "ALLERGEN_ID")
    public String getAllergenId() {
        return allergenId;
    }

    public void setAllergenId(String allergenId) {
        this.allergenId = allergenId;
    }

    @Basic
    @Column(name = "HK_REG_NO")
    public String getHkRegNo() {
        return hkRegNo;
    }

    public void setHkRegNo(String hkRegNo) {
        this.hkRegNo = hkRegNo;
    }

    @Basic
    @Column(name = "VTM")
    public String getVtm() {
        return vtm;
    }

    public void setVtm(String vtm) {
        this.vtm = vtm;
    }

    @Basic
    @Column(name = "TRADE_NAME")
    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    @Basic
    @Column(name = "FORMER_BAN")
    public String getFormerBan() {
        return formerBan;
    }

    public void setFormerBan(String formerBan) {
        this.formerBan = formerBan;
    }

    @Basic
    @Column(name = "ABBREVIATION")
    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    @Basic
    @Column(name = "OTHER_NAME")
    public String getOtherName() {
        return otherName;
    }

    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }

    @Basic
    @Column(name = "CREATE_DTM")
    public Date getCreateDtm() {
        return createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    @Basic
    @Column(name = "UPDATE_DTM")
    public Date getUpdateDtm() {
        return updateDtm;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }

    @Basic
    @Column(name = "GENERIC_PRODUCT_IND")
    public String getGenericProductInd() {
        return genericProductInd;
    }

    public void setGenericProductInd(String genericProductInd) {
        this.genericProductInd = genericProductInd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SaDrugLocalPo that = (SaDrugLocalPo) o;

        if (allergenId != null ? !allergenId.equals(that.allergenId) : that.allergenId != null) return false;
        if (hkRegNo != null ? !hkRegNo.equals(that.hkRegNo) : that.hkRegNo != null) return false;
        if (vtm != null ? !vtm.equals(that.vtm) : that.vtm != null) return false;
        if (tradeName != null ? !tradeName.equals(that.tradeName) : that.tradeName != null) return false;
        if (formerBan != null ? !formerBan.equals(that.formerBan) : that.formerBan != null) return false;
        if (abbreviation != null ? !abbreviation.equals(that.abbreviation) : that.abbreviation != null) return false;
        if (otherName != null ? !otherName.equals(that.otherName) : that.otherName != null) return false;
        if (createDtm != null ? !createDtm.equals(that.createDtm) : that.createDtm != null) return false;
        if (updateDtm != null ? !updateDtm.equals(that.updateDtm) : that.updateDtm != null) return false;
        if (genericProductInd != null ? !genericProductInd.equals(that.genericProductInd) : that.genericProductInd != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = allergenId != null ? allergenId.hashCode() : 0;
        result = 31 * result + (hkRegNo != null ? hkRegNo.hashCode() : 0);
        result = 31 * result + (vtm != null ? vtm.hashCode() : 0);
        result = 31 * result + (tradeName != null ? tradeName.hashCode() : 0);
        result = 31 * result + (formerBan != null ? formerBan.hashCode() : 0);
        result = 31 * result + (abbreviation != null ? abbreviation.hashCode() : 0);
        result = 31 * result + (otherName != null ? otherName.hashCode() : 0);
        result = 31 * result + (createDtm != null ? createDtm.hashCode() : 0);
        result = 31 * result + (updateDtm != null ? updateDtm.hashCode() : 0);
        result = 31 * result + (genericProductInd != null ? genericProductInd.hashCode() : 0);
        return result;
    }
}
