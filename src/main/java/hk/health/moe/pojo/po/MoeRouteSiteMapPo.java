package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_ROUTE_SITE_MAP")
@IdClass(MoeRouteSiteMapPoPK.class)
public class MoeRouteSiteMapPo implements Serializable {
    private static final long serialVersionUID = 7714554920621468208L;
    private long routeId;
    private long siteId;
    private long rank;
    private String displayRoute;
    private MoeSitePo moeSite;
    private MoeRoutePo moeRoute;

    @Id
    @Column(name = "ROUTE_ID")
    public long getRouteId() {
        return routeId;
    }

    public void setRouteId(long routeId) {
        this.routeId = routeId;
    }

    @Id
    @Column(name = "SITE_ID")
    public long getSiteId() {
        return siteId;
    }

    public void setSiteId(long siteId) {
        this.siteId = siteId;
    }

    @Basic
    @Column(name = "RANK")
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @Basic
    @Column(name = "DISPLAY_ROUTE")
    public String getDisplayRoute() {
        return displayRoute;
    }

    public void setDisplayRoute(String displayRoute) {
        this.displayRoute = displayRoute;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "site_id", nullable = false, insertable = false, updatable = false)
    public MoeSitePo getMoeSite() {
        return this.moeSite;
    }

    public void setMoeSite(MoeSitePo moeSite) {
        this.moeSite = moeSite;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "route_id", nullable = false, insertable = false, updatable = false)
    public MoeRoutePo getMoeRoute() {
        return this.moeRoute;
    }

    public void setMoeRoute(MoeRoutePo moeRoute) {
        this.moeRoute = moeRoute;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeRouteSiteMapPo that = (MoeRouteSiteMapPo) o;

        if (routeId != that.routeId) return false;
        if (siteId != that.siteId) return false;
        if (rank != that.rank) return false;
        if (displayRoute != null ? !displayRoute.equals(that.displayRoute) : that.displayRoute != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (routeId ^ (routeId >>> 32));
        result = 31 * result + (int) (siteId ^ (siteId >>> 32));
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        result = 31 * result + (displayRoute != null ? displayRoute.hashCode() : 0);
        return result;
    }
}
