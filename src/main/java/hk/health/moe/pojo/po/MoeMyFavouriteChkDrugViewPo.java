/*
package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "MOE_MY_FAVOURITE_CHK_DRUG_VIEW")
public class MoeMyFavouriteChkDrugViewPo {
    private String myFavouriteId;
    private String myFavouriteName;
    private String favouriteType;
    private Long itemNo;
    private String vtm;
    private String tradeName;
    private String aliasName;
    private String nameType;
    private String dangerDrug;
    private String formEng;
    private String doseFormExtraInfo;
    private String drugRouteEng;
    private String screenDisplay;
    private String strength;
    private String strengthLevelExtraInfo;
    private String strengthCompulsory;
    private String freqText;
    private String supplFreqText;
    private Long duration;
    private String durationUnit;
    private String specInstruct;
    private Long dosage;
    private String dosageUnit;
    private String orderLineType;
    private Long stepNo;
    private String prn;
    private String availableInDruglist;
    private String createUserId;
    private String userSpecialty;

    @Basic
    @Column(name = "MY_FAVOURITE_ID")
    public String getMyFavouriteId() {
        return myFavouriteId;
    }

    public void setMyFavouriteId(String myFavouriteId) {
        this.myFavouriteId = myFavouriteId;
    }

    @Basic
    @Column(name = "MY_FAVOURITE_NAME")
    public String getMyFavouriteName() {
        return myFavouriteName;
    }

    public void setMyFavouriteName(String myFavouriteName) {
        this.myFavouriteName = myFavouriteName;
    }

    @Basic
    @Column(name = "FAVOURITE_TYPE")
    public String getFavouriteType() {
        return favouriteType;
    }

    public void setFavouriteType(String favouriteType) {
        this.favouriteType = favouriteType;
    }

    @Basic
    @Column(name = "ITEM_NO")
    public Long getItemNo() {
        return itemNo;
    }

    public void setItemNo(Long itemNo) {
        this.itemNo = itemNo;
    }

    @Basic
    @Column(name = "VTM")
    public String getVtm() {
        return vtm;
    }

    public void setVtm(String vtm) {
        this.vtm = vtm;
    }

    @Basic
    @Column(name = "TRADE_NAME")
    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    @Basic
    @Column(name = "ALIAS_NAME")
    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    @Basic
    @Column(name = "NAME_TYPE")
    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    @Basic
    @Column(name = "DANGER_DRUG")
    public String getDangerDrug() {
        return dangerDrug;
    }

    public void setDangerDrug(String dangerDrug) {
        this.dangerDrug = dangerDrug;
    }

    @Basic
    @Column(name = "FORM_ENG")
    public String getFormEng() {
        return formEng;
    }

    public void setFormEng(String formEng) {
        this.formEng = formEng;
    }

    @Basic
    @Column(name = "DOSE_FORM_EXTRA_INFO")
    public String getDoseFormExtraInfo() {
        return doseFormExtraInfo;
    }

    public void setDoseFormExtraInfo(String doseFormExtraInfo) {
        this.doseFormExtraInfo = doseFormExtraInfo;
    }

    @Basic
    @Column(name = "DRUG_ROUTE_ENG")
    public String getDrugRouteEng() {
        return drugRouteEng;
    }

    public void setDrugRouteEng(String drugRouteEng) {
        this.drugRouteEng = drugRouteEng;
    }

    @Basic
    @Column(name = "SCREEN_DISPLAY")
    public String getScreenDisplay() {
        return screenDisplay;
    }

    public void setScreenDisplay(String screenDisplay) {
        this.screenDisplay = screenDisplay;
    }

    @Basic
    @Column(name = "STRENGTH")
    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    @Basic
    @Column(name = "STRENGTH_LEVEL_EXTRA_INFO")
    public String getStrengthLevelExtraInfo() {
        return strengthLevelExtraInfo;
    }

    public void setStrengthLevelExtraInfo(String strengthLevelExtraInfo) {
        this.strengthLevelExtraInfo = strengthLevelExtraInfo;
    }

    @Basic
    @Column(name = "STRENGTH_COMPULSORY")
    public String getStrengthCompulsory() {
        return strengthCompulsory;
    }

    public void setStrengthCompulsory(String strengthCompulsory) {
        this.strengthCompulsory = strengthCompulsory;
    }

    @Basic
    @Column(name = "FREQ_TEXT")
    public String getFreqText() {
        return freqText;
    }

    public void setFreqText(String freqText) {
        this.freqText = freqText;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_TEXT")
    public String getSupplFreqText() {
        return supplFreqText;
    }

    public void setSupplFreqText(String supplFreqText) {
        this.supplFreqText = supplFreqText;
    }

    @Basic
    @Column(name = "DURATION")
    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    @Basic
    @Column(name = "DURATION_UNIT")
    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    @Basic
    @Column(name = "SPEC_INSTRUCT")
    public String getSpecInstruct() {
        return specInstruct;
    }

    public void setSpecInstruct(String specInstruct) {
        this.specInstruct = specInstruct;
    }

    @Basic
    @Column(name = "DOSAGE")
    public Long getDosage() {
        return dosage;
    }

    public void setDosage(Long dosage) {
        this.dosage = dosage;
    }

    @Basic
    @Column(name = "DOSAGE_UNIT")
    public String getDosageUnit() {
        return dosageUnit;
    }

    public void setDosageUnit(String dosageUnit) {
        this.dosageUnit = dosageUnit;
    }

    @Basic
    @Column(name = "ORDER_LINE_TYPE")
    public String getOrderLineType() {
        return orderLineType;
    }

    public void setOrderLineType(String orderLineType) {
        this.orderLineType = orderLineType;
    }

    @Basic
    @Column(name = "STEP_NO")
    public Long getStepNo() {
        return stepNo;
    }

    public void setStepNo(Long stepNo) {
        this.stepNo = stepNo;
    }

    @Basic
    @Column(name = "PRN")
    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    @Basic
    @Column(name = "AVAILABLE_IN_DRUGLIST")
    public String getAvailableInDruglist() {
        return availableInDruglist;
    }

    public void setAvailableInDruglist(String availableInDruglist) {
        this.availableInDruglist = availableInDruglist;
    }

    @Basic
    @Column(name = "CREATE_USER_ID")
    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    @Basic
    @Column(name = "USER_SPECIALTY")
    public String getUserSpecialty() {
        return userSpecialty;
    }

    public void setUserSpecialty(String userSpecialty) {
        this.userSpecialty = userSpecialty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeMyFavouriteChkDrugViewPo that = (MoeMyFavouriteChkDrugViewPo) o;

        if (myFavouriteId != null ? !myFavouriteId.equals(that.myFavouriteId) : that.myFavouriteId != null)
            return false;
        if (myFavouriteName != null ? !myFavouriteName.equals(that.myFavouriteName) : that.myFavouriteName != null)
            return false;
        if (favouriteType != null ? !favouriteType.equals(that.favouriteType) : that.favouriteType != null)
            return false;
        if (itemNo != null ? !itemNo.equals(that.itemNo) : that.itemNo != null) return false;
        if (vtm != null ? !vtm.equals(that.vtm) : that.vtm != null) return false;
        if (tradeName != null ? !tradeName.equals(that.tradeName) : that.tradeName != null) return false;
        if (aliasName != null ? !aliasName.equals(that.aliasName) : that.aliasName != null) return false;
        if (nameType != null ? !nameType.equals(that.nameType) : that.nameType != null) return false;
        if (dangerDrug != null ? !dangerDrug.equals(that.dangerDrug) : that.dangerDrug != null) return false;
        if (formEng != null ? !formEng.equals(that.formEng) : that.formEng != null) return false;
        if (doseFormExtraInfo != null ? !doseFormExtraInfo.equals(that.doseFormExtraInfo) : that.doseFormExtraInfo != null)
            return false;
        if (drugRouteEng != null ? !drugRouteEng.equals(that.drugRouteEng) : that.drugRouteEng != null) return false;
        if (screenDisplay != null ? !screenDisplay.equals(that.screenDisplay) : that.screenDisplay != null)
            return false;
        if (strength != null ? !strength.equals(that.strength) : that.strength != null) return false;
        if (strengthLevelExtraInfo != null ? !strengthLevelExtraInfo.equals(that.strengthLevelExtraInfo) : that.strengthLevelExtraInfo != null)
            return false;
        if (strengthCompulsory != null ? !strengthCompulsory.equals(that.strengthCompulsory) : that.strengthCompulsory != null)
            return false;
        if (freqText != null ? !freqText.equals(that.freqText) : that.freqText != null) return false;
        if (supplFreqText != null ? !supplFreqText.equals(that.supplFreqText) : that.supplFreqText != null)
            return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;
        if (durationUnit != null ? !durationUnit.equals(that.durationUnit) : that.durationUnit != null) return false;
        if (specInstruct != null ? !specInstruct.equals(that.specInstruct) : that.specInstruct != null) return false;
        if (dosage != null ? !dosage.equals(that.dosage) : that.dosage != null) return false;
        if (dosageUnit != null ? !dosageUnit.equals(that.dosageUnit) : that.dosageUnit != null) return false;
        if (orderLineType != null ? !orderLineType.equals(that.orderLineType) : that.orderLineType != null)
            return false;
        if (stepNo != null ? !stepNo.equals(that.stepNo) : that.stepNo != null) return false;
        if (prn != null ? !prn.equals(that.prn) : that.prn != null) return false;
        if (availableInDruglist != null ? !availableInDruglist.equals(that.availableInDruglist) : that.availableInDruglist != null)
            return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null) return false;
        if (userSpecialty != null ? !userSpecialty.equals(that.userSpecialty) : that.userSpecialty != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = myFavouriteId != null ? myFavouriteId.hashCode() : 0;
        result = 31 * result + (myFavouriteName != null ? myFavouriteName.hashCode() : 0);
        result = 31 * result + (favouriteType != null ? favouriteType.hashCode() : 0);
        result = 31 * result + (itemNo != null ? itemNo.hashCode() : 0);
        result = 31 * result + (vtm != null ? vtm.hashCode() : 0);
        result = 31 * result + (tradeName != null ? tradeName.hashCode() : 0);
        result = 31 * result + (aliasName != null ? aliasName.hashCode() : 0);
        result = 31 * result + (nameType != null ? nameType.hashCode() : 0);
        result = 31 * result + (dangerDrug != null ? dangerDrug.hashCode() : 0);
        result = 31 * result + (formEng != null ? formEng.hashCode() : 0);
        result = 31 * result + (doseFormExtraInfo != null ? doseFormExtraInfo.hashCode() : 0);
        result = 31 * result + (drugRouteEng != null ? drugRouteEng.hashCode() : 0);
        result = 31 * result + (screenDisplay != null ? screenDisplay.hashCode() : 0);
        result = 31 * result + (strength != null ? strength.hashCode() : 0);
        result = 31 * result + (strengthLevelExtraInfo != null ? strengthLevelExtraInfo.hashCode() : 0);
        result = 31 * result + (strengthCompulsory != null ? strengthCompulsory.hashCode() : 0);
        result = 31 * result + (freqText != null ? freqText.hashCode() : 0);
        result = 31 * result + (supplFreqText != null ? supplFreqText.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (durationUnit != null ? durationUnit.hashCode() : 0);
        result = 31 * result + (specInstruct != null ? specInstruct.hashCode() : 0);
        result = 31 * result + (dosage != null ? dosage.hashCode() : 0);
        result = 31 * result + (dosageUnit != null ? dosageUnit.hashCode() : 0);
        result = 31 * result + (orderLineType != null ? orderLineType.hashCode() : 0);
        result = 31 * result + (stepNo != null ? stepNo.hashCode() : 0);
        result = 31 * result + (prn != null ? prn.hashCode() : 0);
        result = 31 * result + (availableInDruglist != null ? availableInDruglist.hashCode() : 0);
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (userSpecialty != null ? userSpecialty.hashCode() : 0);
        return result;
    }
}
*/
