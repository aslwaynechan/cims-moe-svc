package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_PRESCRIBE_UNIT_TERM_ID_MAP")
@IdClass(MoePrescribeUnitTermIdMapPoPK.class)
public class MoePrescribeUnitTermIdMapPo implements Serializable {
    private static final long serialVersionUID = 7385637260927800546L;
    private String prescribeUnit;
    private String ehrPrescribeUnitDesc;
    private long ehrTermId;
    private String defaultMap;
    private String ehrStatus;

    @Id
    @Column(name = "PRESCRIBE_UNIT")
    public String getPrescribeUnit() {
        return prescribeUnit;
    }

    public void setPrescribeUnit(String prescribeUnit) {
        this.prescribeUnit = prescribeUnit;
    }

    @Basic
    @Column(name = "EHR_PRESCRIBE_UNIT_DESC")
    public String getEhrPrescribeUnitDesc() {
        return ehrPrescribeUnitDesc;
    }

    public void setEhrPrescribeUnitDesc(String ehrPrescribeUnitDesc) {
        this.ehrPrescribeUnitDesc = ehrPrescribeUnitDesc;
    }

    @Id
    @Column(name = "EHR_TERM_ID")
    public long getEhrTermId() {
        return ehrTermId;
    }

    public void setEhrTermId(long ehrTermId) {
        this.ehrTermId = ehrTermId;
    }

    @Basic
    @Column(name = "DEFAULT_MAP")
    public String getDefaultMap() {
        return defaultMap;
    }

    public void setDefaultMap(String defaultMap) {
        this.defaultMap = defaultMap;
    }

    @Basic
    @Column(name = "EHR_STATUS")
    public String getEhrStatus() {
        return ehrStatus;
    }

    public void setEhrStatus(String ehrStatus) {
        this.ehrStatus = ehrStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoePrescribeUnitTermIdMapPo that = (MoePrescribeUnitTermIdMapPo) o;

        if (ehrTermId != that.ehrTermId) return false;
        if (prescribeUnit != null ? !prescribeUnit.equals(that.prescribeUnit) : that.prescribeUnit != null)
            return false;
        if (ehrPrescribeUnitDesc != null ? !ehrPrescribeUnitDesc.equals(that.ehrPrescribeUnitDesc) : that.ehrPrescribeUnitDesc != null)
            return false;
        if (defaultMap != null ? !defaultMap.equals(that.defaultMap) : that.defaultMap != null) return false;
        if (ehrStatus != null ? !ehrStatus.equals(that.ehrStatus) : that.ehrStatus != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = prescribeUnit != null ? prescribeUnit.hashCode() : 0;
        result = 31 * result + (ehrPrescribeUnitDesc != null ? ehrPrescribeUnitDesc.hashCode() : 0);
        result = 31 * result + (int) (ehrTermId ^ (ehrTermId >>> 32));
        result = 31 * result + (defaultMap != null ? defaultMap.hashCode() : 0);
        result = 31 * result + (ehrStatus != null ? ehrStatus.hashCode() : 0);
        return result;
    }
}
