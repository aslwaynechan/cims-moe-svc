package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeDrugTherapeuticLocalPoPK implements Serializable {
    private String localDrugId;
    private long rank;

    @Column(name = "LOCAL_DRUG_ID")
    @Id
    public String getLocalDrugId() {
        return localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        this.localDrugId = localDrugId;
    }

    @Column(name = "RANK")
    @Id
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDrugTherapeuticLocalPoPK that = (MoeDrugTherapeuticLocalPoPK) o;

        if (rank != that.rank) return false;
        if (localDrugId != null ? !localDrugId.equals(that.localDrugId) : that.localDrugId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = localDrugId != null ? localDrugId.hashCode() : 0;
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        return result;
    }
}
