package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import java.io.Serializable;

public class MoeOverrideReasonPoPK implements Serializable {
    private static final long serialVersionUID = -813551015966482568L;
    private String reasonType;
    private String reasonCode;

    @Basic
    @Column(name = "REASON_TYPE")
    public String getReasonType() {
        return reasonType;
    }

    public void setReasonType(String reasonType) {
        this.reasonType = reasonType;
    }

    @Basic
    @Column(name = "REASON_CODE")
    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeOverrideReasonPoPK that = (MoeOverrideReasonPoPK) o;

        if (reasonType != null ? !reasonType.equals(that.reasonType) : that.reasonType != null) return false;
        if (reasonCode != null ? !reasonCode.equals(that.reasonCode) : that.reasonCode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = reasonType != null ? reasonType.hashCode() : 0;
        result = 31 * result + (reasonCode != null ? reasonCode.hashCode() : 0);

        return result;
    }
}
