package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Arrays;

@Entity
@Table(name = "MOE_HOSP_IMAGE")
public class MoeHospImagePo implements Serializable {
    private static final long serialVersionUID = 7682700604787440308L;
    private String imageName;
    private byte[] image;

    @Id
    @Column(name = "IMAGE_NAME")
    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    @Basic
    @Column(name = "IMAGE")
    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeHospImagePo that = (MoeHospImagePo) o;

        if (imageName != null ? !imageName.equals(that.imageName) : that.imageName != null) return false;
        if (!Arrays.equals(image, that.image)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = imageName != null ? imageName.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(image);
        return result;
    }
}
