package hk.health.moe.pojo.po;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name = "MOE_MY_FAVOURITE_HDR")
public class MoeMyFavouriteHdrPo implements Serializable {
    private static final long serialVersionUID = -965022698827533873L;
    private String myFavouriteId;
    private String createUserId;
    private String userSpecialty;
    private String myFavouriteName;
    private String frontMyFavouriteId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private Date createDate;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private Date updateDate;
    private Long version;
    private String specialtyForMyFavourite;
    private Set<MoeMyFavouriteDetailPo> moeMyFavouriteDetails = new TreeSet<>();

    // Transient start --
    private String cacheId;
    private String isCache;
    // Transient end --

    public MoeMyFavouriteHdrPo() {
    }

    public MoeMyFavouriteHdrPo(String createUser,
                               String createHosp,
                               String createRank,
                               String createRankDesc,
                               Date createDate,
                               String createUserId) {
        this.createUser = createUser;
        this.createHosp = createHosp;
        this.createRank = createRank;
        this.createRankDesc = createRankDesc;
        this.createDate = createDate;
        this.createUserId = createUserId;
    }

    @Id
    @GeneratedValue(generator = "myFavouriteIdGenerator")
    @GenericGenerator(name = "myFavouriteIdGenerator", strategy = "uuid")
    @Column(name = "MY_FAVOURITE_ID")
    public String getMyFavouriteId() {
        return myFavouriteId;
    }

    public void setMyFavouriteId(String myFavouriteId) {
        this.myFavouriteId = myFavouriteId;
    }

    @Basic
    @Column(name = "CREATE_USER_ID")
    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    @Basic
    @Column(name = "USER_SPECIALTY")
    public String getUserSpecialty() {
        return userSpecialty;
    }

    public void setUserSpecialty(String userSpecialty) {
        this.userSpecialty = userSpecialty;
    }

    @Basic
    @Column(name = "MY_FAVOURITE_NAME")
    public String getMyFavouriteName() {
        return myFavouriteName;
    }

    public void setMyFavouriteName(String myFavouriteName) {
        this.myFavouriteName = myFavouriteName;
    }

    @Basic
    @Column(name = "FRONT_MY_FAVOURITE_ID")
    public String getFrontMyFavouriteId() {
        return frontMyFavouriteId;
    }

    public void setFrontMyFavouriteId(String frontMyFavouriteId) {
        this.frontMyFavouriteId = frontMyFavouriteId;
    }

    @Basic
    @Column(name = "CREATE_USER")
    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    @Basic
    @Column(name = "CREATE_HOSP")
    public String getCreateHosp() {
        return createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    @Basic
    @Column(name = "CREATE_RANK")
    public String getCreateRank() {
        return createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    @Basic
    @Column(name = "CREATE_RANK_DESC")
    public String getCreateRankDesc() {
        return createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    @Basic
    @Column(name = "CREATE_DATE")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Basic
    @Column(name = "UPDATE_USER_ID")
    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Basic
    @Column(name = "UPDATE_USER")
    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Basic
    @Column(name = "UPDATE_HOSP")
    public String getUpdateHosp() {
        return updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    @Basic
    @Column(name = "UPDATE_RANK")
    public String getUpdateRank() {
        return updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    @Basic
    @Column(name = "UPDATE_RANK_DESC")
    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    @Basic
    @Column(name = "UPDATE_DATE")
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Version
    @Column(name = "VERSION")
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Basic
    @Column(name = "SPECIALTY_FOR_MY_FAVOURITE")
    public String getSpecialtyForMyFavourite() {
        return specialtyForMyFavourite;
    }

    public void setSpecialtyForMyFavourite(String specialtyForMyFavourite) {
        this.specialtyForMyFavourite = specialtyForMyFavourite;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moeMyFavouriteHdr",
            cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("itemNo asc")
    public Set<MoeMyFavouriteDetailPo> getMoeMyFavouriteDetails() {
        return this.moeMyFavouriteDetails;
    }

    @Transient
    public String getCacheId() {
        return cacheId;
    }

    public void setCacheId(String cacheId) {
        this.cacheId = cacheId;
    }

    @Transient
    public String getIsCache() {
        return isCache;
    }

    public void setIsCache(String isCache) {
        this.isCache = isCache;
    }

    public void setMoeMyFavouriteDetails(Set<MoeMyFavouriteDetailPo> moeMyFavouriteDetails) {
        this.moeMyFavouriteDetails = moeMyFavouriteDetails;
    }

    public void addMoeMyFavouriteDetail(MoeMyFavouriteDetailPo detail) {
        if (this.moeMyFavouriteDetails != null) {
            //detail.setMyFavouriteId(myFavouriteId);
            detail.setItemNo(moeMyFavouriteDetails.size() + 1L);
            detail.setMoeMyFavouriteHdr(this);
            this.moeMyFavouriteDetails.add(detail);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeMyFavouriteHdrPo that = (MoeMyFavouriteHdrPo) o;

        if (version != that.version) return false;
        if (myFavouriteId != null ? !myFavouriteId.equals(that.myFavouriteId) : that.myFavouriteId != null)
            return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null) return false;
        if (userSpecialty != null ? !userSpecialty.equals(that.userSpecialty) : that.userSpecialty != null)
            return false;
        if (myFavouriteName != null ? !myFavouriteName.equals(that.myFavouriteName) : that.myFavouriteName != null)
            return false;
        if (frontMyFavouriteId != null ? !frontMyFavouriteId.equals(that.frontMyFavouriteId) : that.frontMyFavouriteId != null)
            return false;
        if (createUser != null ? !createUser.equals(that.createUser) : that.createUser != null) return false;
        if (createHosp != null ? !createHosp.equals(that.createHosp) : that.createHosp != null) return false;
        if (createRank != null ? !createRank.equals(that.createRank) : that.createRank != null) return false;
        if (createRankDesc != null ? !createRankDesc.equals(that.createRankDesc) : that.createRankDesc != null)
            return false;
        if (createDate != null ? !createDate.equals(that.createDate) : that.createDate != null) return false;
        if (updateUserId != null ? !updateUserId.equals(that.updateUserId) : that.updateUserId != null) return false;
        if (updateUser != null ? !updateUser.equals(that.updateUser) : that.updateUser != null) return false;
        if (updateHosp != null ? !updateHosp.equals(that.updateHosp) : that.updateHosp != null) return false;
        if (updateRank != null ? !updateRank.equals(that.updateRank) : that.updateRank != null) return false;
        if (updateRankDesc != null ? !updateRankDesc.equals(that.updateRankDesc) : that.updateRankDesc != null)
            return false;
        if (updateDate != null ? !updateDate.equals(that.updateDate) : that.updateDate != null) return false;
        if (specialtyForMyFavourite != null ? !specialtyForMyFavourite.equals(that.specialtyForMyFavourite) : that.specialtyForMyFavourite != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = myFavouriteId != null ? myFavouriteId.hashCode() : 0;
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (userSpecialty != null ? userSpecialty.hashCode() : 0);
        result = 31 * result + (myFavouriteName != null ? myFavouriteName.hashCode() : 0);
        result = 31 * result + (frontMyFavouriteId != null ? frontMyFavouriteId.hashCode() : 0);
        result = 31 * result + (createUser != null ? createUser.hashCode() : 0);
        result = 31 * result + (createHosp != null ? createHosp.hashCode() : 0);
        result = 31 * result + (createRank != null ? createRank.hashCode() : 0);
        result = 31 * result + (createRankDesc != null ? createRankDesc.hashCode() : 0);
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        result = 31 * result + (updateUserId != null ? updateUserId.hashCode() : 0);
        result = 31 * result + (updateUser != null ? updateUser.hashCode() : 0);
        result = 31 * result + (updateHosp != null ? updateHosp.hashCode() : 0);
        result = 31 * result + (updateRank != null ? updateRank.hashCode() : 0);
        result = 31 * result + (updateRankDesc != null ? updateRankDesc.hashCode() : 0);
        result = 31 * result + (updateDate != null ? updateDate.hashCode() : 0);
        result = 31 * result + (int) (version ^ (version >>> 32));
        result = 31 * result + (specialtyForMyFavourite != null ? specialtyForMyFavourite.hashCode() : 0);
        return result;
    }
}
