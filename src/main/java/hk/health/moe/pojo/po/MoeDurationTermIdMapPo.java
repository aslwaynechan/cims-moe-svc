package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_DURATION_TERM_ID_MAP")
@IdClass(MoeDurationTermIdMapPoPK.class)
public class MoeDurationTermIdMapPo implements Serializable {
    private static final long serialVersionUID = 2659781195339115632L;
    private String durationUnit;
    private String ehrDurationUnitDesc;
    private long ehrTermId;
    private String defaultMap;
    private String ehrStatus;

    @Id
    @Column(name = "DURATION_UNIT")
    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    @Basic
    @Column(name = "EHR_DURATION_UNIT_DESC")
    public String getEhrDurationUnitDesc() {
        return ehrDurationUnitDesc;
    }

    public void setEhrDurationUnitDesc(String ehrDurationUnitDesc) {
        this.ehrDurationUnitDesc = ehrDurationUnitDesc;
    }

    @Id
    @Column(name = "EHR_TERM_ID")
    public long getEhrTermId() {
        return ehrTermId;
    }

    public void setEhrTermId(long ehrTermId) {
        this.ehrTermId = ehrTermId;
    }

    @Basic
    @Column(name = "DEFAULT_MAP")
    public String getDefaultMap() {
        return defaultMap;
    }

    public void setDefaultMap(String defaultMap) {
        this.defaultMap = defaultMap;
    }

    @Basic
    @Column(name = "EHR_STATUS")
    public String getEhrStatus() {
        return ehrStatus;
    }

    public void setEhrStatus(String ehrStatus) {
        this.ehrStatus = ehrStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDurationTermIdMapPo that = (MoeDurationTermIdMapPo) o;

        if (ehrTermId != that.ehrTermId) return false;
        if (durationUnit != null ? !durationUnit.equals(that.durationUnit) : that.durationUnit != null) return false;
        if (ehrDurationUnitDesc != null ? !ehrDurationUnitDesc.equals(that.ehrDurationUnitDesc) : that.ehrDurationUnitDesc != null)
            return false;
        if (defaultMap != null ? !defaultMap.equals(that.defaultMap) : that.defaultMap != null) return false;
        if (ehrStatus != null ? !ehrStatus.equals(that.ehrStatus) : that.ehrStatus != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = durationUnit != null ? durationUnit.hashCode() : 0;
        result = 31 * result + (ehrDurationUnitDesc != null ? ehrDurationUnitDesc.hashCode() : 0);
        result = 31 * result + (int) (ehrTermId ^ (ehrTermId >>> 32));
        result = 31 * result + (defaultMap != null ? defaultMap.hashCode() : 0);
        result = 31 * result + (ehrStatus != null ? ehrStatus.hashCode() : 0);
        return result;
    }
}
