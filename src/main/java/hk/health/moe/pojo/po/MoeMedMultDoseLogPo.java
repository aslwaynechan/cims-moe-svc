package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "MOE_MED_MULT_DOSE_LOG")
@IdClass(MoeMedMultDoseLogPoPK.class)
public class MoeMedMultDoseLogPo implements Serializable, Comparable<MoeMedMultDoseLogPo> {
    private static final long serialVersionUID = -8145570834448027295L;
    private String hospcode;
    private long ordNo;
    private long cmsItemNo;
    private long stepNo;
    private long multDoseNo;
    private String refNo;
    private String patHospcode;
    private BigDecimal dosage;
    private String freqCode;
    private Long freq1;
    private String supFreqCode;
    private Long supFreq1;
    private Long supFreq2;
    private String dayOfWeek;
    private String adminTimeCode;
    private String prn;
    private BigDecimal prnPercent;
    private String siteCode;
    private String supSiteDesc;
    private Long duration;
    private String durationUnit;
    private Date startDate;
    private Date endDate;
    private Long moQty;
    private String moQtyUnit;
    private String updateBy;
    private Date updateTime;
    private String freqText;
    private String supFreqText;
    private String capdSystem;
    private String capdCalcium;
    private String capdConc;
    private String itemcode;
    private String capdBaseunit;
    private String siteDesc;
    private String trueConc;
    private String adminTimeDesc;
    private String capdChargeInd;
    private Long capdChargeableUnit;
    private BigDecimal capdChargeableAmount;
    private String confirmCapdChargeInd;
    private Long confirmCapdChargeUnit;
    private MoeMedProfileLogPo moeMedProfileLog;
    private MoeEhrMedMultDoseLogPo moeEhrMedMultDoseLog;

    @Id
    @Column(name = "HOSPCODE")
    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    @Id
    @Column(name = "ORD_NO")
    public long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(long ordNo) {
        this.ordNo = ordNo;
    }

    @Id
    @Column(name = "CMS_ITEM_NO")
    public long getCmsItemNo() {
        return cmsItemNo;
    }

    public void setCmsItemNo(long cmsItemNo) {
        this.cmsItemNo = cmsItemNo;
    }

    @Id
    @Column(name = "STEP_NO")
    public long getStepNo() {
        return stepNo;
    }

    public void setStepNo(long stepNo) {
        this.stepNo = stepNo;
    }

    @Id
    @Column(name = "MULT_DOSE_NO")
    public long getMultDoseNo() {
        return multDoseNo;
    }

    public void setMultDoseNo(long multDoseNo) {
        this.multDoseNo = multDoseNo;
    }

    @Id
    @Column(name = "REF_NO")
    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    @Basic
    @Column(name = "PAT_HOSPCODE")
    public String getPatHospcode() {
        return patHospcode;
    }

    public void setPatHospcode(String patHospcode) {
        this.patHospcode = patHospcode;
    }

    @Basic
    @Column(name = "DOSAGE")
    public BigDecimal getDosage() {
        return dosage;
    }

    public void setDosage(BigDecimal dosage) {
        this.dosage = dosage;
    }

    @Basic
    @Column(name = "FREQ_CODE")
    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    @Basic
    @Column(name = "FREQ1")
    public Long getFreq1() {
        return freq1;
    }

    public void setFreq1(Long freq1) {
        this.freq1 = freq1;
    }

    @Basic
    @Column(name = "SUP_FREQ_CODE")
    public String getSupFreqCode() {
        return supFreqCode;
    }

    public void setSupFreqCode(String supFreqCode) {
        this.supFreqCode = supFreqCode;
    }

    @Basic
    @Column(name = "SUP_FREQ1")
    public Long getSupFreq1() {
        return supFreq1;
    }

    public void setSupFreq1(Long supFreq1) {
        this.supFreq1 = supFreq1;
    }

    @Basic
    @Column(name = "SUP_FREQ2")
    public Long getSupFreq2() {
        return supFreq2;
    }

    public void setSupFreq2(Long supFreq2) {
        this.supFreq2 = supFreq2;
    }

    @Basic
    @Column(name = "DAY_OF_WEEK")
    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    @Basic
    @Column(name = "ADMIN_TIME_CODE")
    public String getAdminTimeCode() {
        return adminTimeCode;
    }

    public void setAdminTimeCode(String adminTimeCode) {
        this.adminTimeCode = adminTimeCode;
    }

    @Basic
    @Column(name = "PRN")
    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    @Basic
    @Column(name = "PRN_PERCENT")
    public BigDecimal getPrnPercent() {
        return prnPercent;
    }

    public void setPrnPercent(BigDecimal prnPercent) {
        this.prnPercent = prnPercent;
    }

    @Basic
    @Column(name = "SITE_CODE")
    public String getSiteCode() {
        return siteCode;
    }

    public void setSiteCode(String siteCode) {
        this.siteCode = siteCode;
    }

    @Basic
    @Column(name = "SUP_SITE_DESC")
    public String getSupSiteDesc() {
        return supSiteDesc;
    }

    public void setSupSiteDesc(String supSiteDesc) {
        this.supSiteDesc = supSiteDesc;
    }

    @Basic
    @Column(name = "DURATION")
    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    @Basic
    @Column(name = "DURATION_UNIT")
    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    @Basic
    @Column(name = "START_DATE")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "END_DATE")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "MO_QTY")
    public Long getMoQty() {
        return moQty;
    }

    public void setMoQty(Long moQty) {
        this.moQty = moQty;
    }

    @Basic
    @Column(name = "MO_QTY_UNIT")
    public String getMoQtyUnit() {
        return moQtyUnit;
    }

    public void setMoQtyUnit(String moQtyUnit) {
        this.moQtyUnit = moQtyUnit;
    }

    @Basic
    @Column(name = "UPDATE_BY")
    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    @Basic
    @Column(name = "UPDATE_TIME")
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "FREQ_TEXT")
    public String getFreqText() {
        return freqText;
    }

    public void setFreqText(String freqText) {
        this.freqText = freqText;
    }

    @Basic
    @Column(name = "SUP_FREQ_TEXT")
    public String getSupFreqText() {
        return supFreqText;
    }

    public void setSupFreqText(String supFreqText) {
        this.supFreqText = supFreqText;
    }

    @Basic
    @Column(name = "CAPD_SYSTEM")
    public String getCapdSystem() {
        return capdSystem;
    }

    public void setCapdSystem(String capdSystem) {
        this.capdSystem = capdSystem;
    }

    @Basic
    @Column(name = "CAPD_CALCIUM")
    public String getCapdCalcium() {
        return capdCalcium;
    }

    public void setCapdCalcium(String capdCalcium) {
        this.capdCalcium = capdCalcium;
    }

    @Basic
    @Column(name = "CAPD_CONC")
    public String getCapdConc() {
        return capdConc;
    }

    public void setCapdConc(String capdConc) {
        this.capdConc = capdConc;
    }

    @Basic
    @Column(name = "ITEMCODE")
    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    @Basic
    @Column(name = "CAPD_BASEUNIT")
    public String getCapdBaseunit() {
        return capdBaseunit;
    }

    public void setCapdBaseunit(String capdBaseunit) {
        this.capdBaseunit = capdBaseunit;
    }

    @Basic
    @Column(name = "SITE_DESC")
    public String getSiteDesc() {
        return siteDesc;
    }

    public void setSiteDesc(String siteDesc) {
        this.siteDesc = siteDesc;
    }

    @Basic
    @Column(name = "TRUE_CONC")
    public String getTrueConc() {
        return trueConc;
    }

    public void setTrueConc(String trueConc) {
        this.trueConc = trueConc;
    }

    @Basic
    @Column(name = "ADMIN_TIME_DESC")
    public String getAdminTimeDesc() {
        return adminTimeDesc;
    }

    public void setAdminTimeDesc(String adminTimeDesc) {
        this.adminTimeDesc = adminTimeDesc;
    }

    @Basic
    @Column(name = "CAPD_CHARGE_IND")
    public String getCapdChargeInd() {
        return capdChargeInd;
    }

    public void setCapdChargeInd(String capdChargeInd) {
        this.capdChargeInd = capdChargeInd;
    }

    @Basic
    @Column(name = "CAPD_CHARGEABLE_UNIT")
    public Long getCapdChargeableUnit() {
        return capdChargeableUnit;
    }

    public void setCapdChargeableUnit(Long capdChargeableUnit) {
        this.capdChargeableUnit = capdChargeableUnit;
    }

    @Basic
    @Column(name = "CAPD_CHARGEABLE_AMOUNT")
    public BigDecimal getCapdChargeableAmount() {
        return capdChargeableAmount;
    }

    public void setCapdChargeableAmount(BigDecimal capdChargeableAmount) {
        this.capdChargeableAmount = capdChargeableAmount;
    }

    @Basic
    @Column(name = "CONFIRM_CAPD_CHARGE_IND")
    public String getConfirmCapdChargeInd() {
        return confirmCapdChargeInd;
    }

    public void setConfirmCapdChargeInd(String confirmCapdChargeInd) {
        this.confirmCapdChargeInd = confirmCapdChargeInd;
    }

    @Basic
    @Column(name = "CONFIRM_CAPD_CHARGE_UNIT")
    public Long getConfirmCapdChargeUnit() {
        return confirmCapdChargeUnit;
    }

    public void setConfirmCapdChargeUnit(Long confirmCapdChargeUnit) {
        this.confirmCapdChargeUnit = confirmCapdChargeUnit;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns( {
            @JoinColumn(name = "hospcode", referencedColumnName = "hospcode", nullable = false, insertable = false, updatable = false),
            @JoinColumn(name = "ord_no", referencedColumnName = "ord_no", nullable = false, insertable = false, updatable = false),
            @JoinColumn(name = "cms_item_no", referencedColumnName = "cms_item_no", nullable = false, insertable = false, updatable = false),
            @JoinColumn(name = "ref_no", referencedColumnName = "ref_no", nullable = false, insertable = false, updatable = false) })
    public MoeMedProfileLogPo getMoeMedProfileLog() {
        return this.moeMedProfileLog;
    }

    public void setMoeMedProfileLog(MoeMedProfileLogPo moeMedProfileLog) {
        this.moeMedProfileLog = moeMedProfileLog;
    }

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "moeMedMultDoseLog",
            cascade= CascadeType.ALL, orphanRemoval=true)
    public MoeEhrMedMultDoseLogPo getMoeEhrMedMultDoseLog() {
        return this.moeEhrMedMultDoseLog;
    }

    public void setMoeEhrMedMultDoseLog(MoeEhrMedMultDoseLogPo moeEhrMedMultDoseLog) {
        this.moeEhrMedMultDoseLog = moeEhrMedMultDoseLog;
    }


    @Override
    public int compareTo(MoeMedMultDoseLogPo o) {
        long thisVal = this.getMultDoseNo();
        long anotherVal = o.getMultDoseNo();
        return (thisVal<anotherVal ? -1 : (thisVal==anotherVal ? 0 : 1));
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeMedMultDoseLogPo that = (MoeMedMultDoseLogPo) o;

        if (ordNo != that.ordNo) return false;
        if (cmsItemNo != that.cmsItemNo) return false;
        if (stepNo != that.stepNo) return false;
        if (multDoseNo != that.multDoseNo) return false;
        if (hospcode != null ? !hospcode.equals(that.hospcode) : that.hospcode != null) return false;
        if (refNo != null ? !refNo.equals(that.refNo) : that.refNo != null) return false;
        if (patHospcode != null ? !patHospcode.equals(that.patHospcode) : that.patHospcode != null) return false;
        if (dosage != null ? !dosage.equals(that.dosage) : that.dosage != null) return false;
        if (freqCode != null ? !freqCode.equals(that.freqCode) : that.freqCode != null) return false;
        if (freq1 != null ? !freq1.equals(that.freq1) : that.freq1 != null) return false;
        if (supFreqCode != null ? !supFreqCode.equals(that.supFreqCode) : that.supFreqCode != null) return false;
        if (supFreq1 != null ? !supFreq1.equals(that.supFreq1) : that.supFreq1 != null) return false;
        if (supFreq2 != null ? !supFreq2.equals(that.supFreq2) : that.supFreq2 != null) return false;
        if (dayOfWeek != null ? !dayOfWeek.equals(that.dayOfWeek) : that.dayOfWeek != null) return false;
        if (adminTimeCode != null ? !adminTimeCode.equals(that.adminTimeCode) : that.adminTimeCode != null)
            return false;
        if (prn != null ? !prn.equals(that.prn) : that.prn != null) return false;
        if (prnPercent != null ? !prnPercent.equals(that.prnPercent) : that.prnPercent != null) return false;
        if (siteCode != null ? !siteCode.equals(that.siteCode) : that.siteCode != null) return false;
        if (supSiteDesc != null ? !supSiteDesc.equals(that.supSiteDesc) : that.supSiteDesc != null) return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;
        if (durationUnit != null ? !durationUnit.equals(that.durationUnit) : that.durationUnit != null) return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;
        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
        if (moQty != null ? !moQty.equals(that.moQty) : that.moQty != null) return false;
        if (moQtyUnit != null ? !moQtyUnit.equals(that.moQtyUnit) : that.moQtyUnit != null) return false;
        if (updateBy != null ? !updateBy.equals(that.updateBy) : that.updateBy != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
        if (freqText != null ? !freqText.equals(that.freqText) : that.freqText != null) return false;
        if (supFreqText != null ? !supFreqText.equals(that.supFreqText) : that.supFreqText != null) return false;
        if (capdSystem != null ? !capdSystem.equals(that.capdSystem) : that.capdSystem != null) return false;
        if (capdCalcium != null ? !capdCalcium.equals(that.capdCalcium) : that.capdCalcium != null) return false;
        if (capdConc != null ? !capdConc.equals(that.capdConc) : that.capdConc != null) return false;
        if (itemcode != null ? !itemcode.equals(that.itemcode) : that.itemcode != null) return false;
        if (capdBaseunit != null ? !capdBaseunit.equals(that.capdBaseunit) : that.capdBaseunit != null) return false;
        if (siteDesc != null ? !siteDesc.equals(that.siteDesc) : that.siteDesc != null) return false;
        if (trueConc != null ? !trueConc.equals(that.trueConc) : that.trueConc != null) return false;
        if (adminTimeDesc != null ? !adminTimeDesc.equals(that.adminTimeDesc) : that.adminTimeDesc != null)
            return false;
        if (capdChargeInd != null ? !capdChargeInd.equals(that.capdChargeInd) : that.capdChargeInd != null)
            return false;
        if (capdChargeableUnit != null ? !capdChargeableUnit.equals(that.capdChargeableUnit) : that.capdChargeableUnit != null)
            return false;
        if (capdChargeableAmount != null ? !capdChargeableAmount.equals(that.capdChargeableAmount) : that.capdChargeableAmount != null)
            return false;
        if (confirmCapdChargeInd != null ? !confirmCapdChargeInd.equals(that.confirmCapdChargeInd) : that.confirmCapdChargeInd != null)
            return false;
        if (confirmCapdChargeUnit != null ? !confirmCapdChargeUnit.equals(that.confirmCapdChargeUnit) : that.confirmCapdChargeUnit != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hospcode != null ? hospcode.hashCode() : 0;
        result = 31 * result + (int) (ordNo ^ (ordNo >>> 32));
        result = 31 * result + (int) (cmsItemNo ^ (cmsItemNo >>> 32));
        result = 31 * result + (int) (stepNo ^ (stepNo >>> 32));
        result = 31 * result + (int) (multDoseNo ^ (multDoseNo >>> 32));
        result = 31 * result + (refNo != null ? refNo.hashCode() : 0);
        result = 31 * result + (patHospcode != null ? patHospcode.hashCode() : 0);
        result = 31 * result + (dosage != null ? dosage.hashCode() : 0);
        result = 31 * result + (freqCode != null ? freqCode.hashCode() : 0);
        result = 31 * result + (freq1 != null ? freq1.hashCode() : 0);
        result = 31 * result + (supFreqCode != null ? supFreqCode.hashCode() : 0);
        result = 31 * result + (supFreq1 != null ? supFreq1.hashCode() : 0);
        result = 31 * result + (supFreq2 != null ? supFreq2.hashCode() : 0);
        result = 31 * result + (dayOfWeek != null ? dayOfWeek.hashCode() : 0);
        result = 31 * result + (adminTimeCode != null ? adminTimeCode.hashCode() : 0);
        result = 31 * result + (prn != null ? prn.hashCode() : 0);
        result = 31 * result + (prnPercent != null ? prnPercent.hashCode() : 0);
        result = 31 * result + (siteCode != null ? siteCode.hashCode() : 0);
        result = 31 * result + (supSiteDesc != null ? supSiteDesc.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (durationUnit != null ? durationUnit.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (moQty != null ? moQty.hashCode() : 0);
        result = 31 * result + (moQtyUnit != null ? moQtyUnit.hashCode() : 0);
        result = 31 * result + (updateBy != null ? updateBy.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + (freqText != null ? freqText.hashCode() : 0);
        result = 31 * result + (supFreqText != null ? supFreqText.hashCode() : 0);
        result = 31 * result + (capdSystem != null ? capdSystem.hashCode() : 0);
        result = 31 * result + (capdCalcium != null ? capdCalcium.hashCode() : 0);
        result = 31 * result + (capdConc != null ? capdConc.hashCode() : 0);
        result = 31 * result + (itemcode != null ? itemcode.hashCode() : 0);
        result = 31 * result + (capdBaseunit != null ? capdBaseunit.hashCode() : 0);
        result = 31 * result + (siteDesc != null ? siteDesc.hashCode() : 0);
        result = 31 * result + (trueConc != null ? trueConc.hashCode() : 0);
        result = 31 * result + (adminTimeDesc != null ? adminTimeDesc.hashCode() : 0);
        result = 31 * result + (capdChargeInd != null ? capdChargeInd.hashCode() : 0);
        result = 31 * result + (capdChargeableUnit != null ? capdChargeableUnit.hashCode() : 0);
        result = 31 * result + (capdChargeableAmount != null ? capdChargeableAmount.hashCode() : 0);
        result = 31 * result + (confirmCapdChargeInd != null ? confirmCapdChargeInd.hashCode() : 0);
        result = 31 * result + (confirmCapdChargeUnit != null ? confirmCapdChargeUnit.hashCode() : 0);
        return result;
    }
}
