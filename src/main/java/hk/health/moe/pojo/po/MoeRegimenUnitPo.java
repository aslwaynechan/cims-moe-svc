package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_REGIMEN_UNIT")
@IdClass(MoeRegimenUnitPoPK.class)
public class MoeRegimenUnitPo implements Serializable {
    private static final long serialVersionUID = -9211581527052079717L;
    private String regimenType;
    private String durationUnit;
    private long rank;
    private MoeRegimenPo moeRegimen;

    @Id
    @Column(name = "REGIMEN_TYPE")
    public String getRegimenType() {
        return regimenType;
    }

    public void setRegimenType(String regimenType) {
        this.regimenType = regimenType;
    }

    @Id
    @Column(name = "DURATION_UNIT")
    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    @Basic
    @Column(name = "RANK")
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "regimen_type", nullable = false, insertable = false, updatable = false)
    public MoeRegimenPo getMoeRegimen() {
        return this.moeRegimen;
    }

    public void setMoeRegimen(MoeRegimenPo moeRegimen) {
        this.moeRegimen = moeRegimen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeRegimenUnitPo that = (MoeRegimenUnitPo) o;

        if (rank != that.rank) return false;
        if (regimenType != null ? !regimenType.equals(that.regimenType) : that.regimenType != null) return false;
        if (durationUnit != null ? !durationUnit.equals(that.durationUnit) : that.durationUnit != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = regimenType != null ? regimenType.hashCode() : 0;
        result = 31 * result + (durationUnit != null ? durationUnit.hashCode() : 0);
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        return result;
    }
}
