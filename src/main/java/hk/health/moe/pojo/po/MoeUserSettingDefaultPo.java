package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_USER_SETTING_DEFAULT")
public class MoeUserSettingDefaultPo implements Serializable {
    private static final long serialVersionUID = 3163904978851000710L;
    private String paramName;
    private String paramNameDesc;
    private String defaultParamValue;
    private String paramValueType;
    private String paramPossibleValue;
    private String paramLevel;
    private String paramDisplay;

    @Id
    @Column(name = "PARAM_NAME")
    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    @Basic
    @Column(name = "PARAM_NAME_DESC")
    public String getParamNameDesc() {
        return paramNameDesc;
    }

    public void setParamNameDesc(String paramNameDesc) {
        this.paramNameDesc = paramNameDesc;
    }

    @Basic
    @Column(name = "DEFAULT_PARAM_VALUE")
    public String getDefaultParamValue() {
        return defaultParamValue;
    }

    public void setDefaultParamValue(String defaultParamValue) {
        this.defaultParamValue = defaultParamValue;
    }

    @Basic
    @Column(name = "PARAM_VALUE_TYPE")
    public String getParamValueType() {
        return paramValueType;
    }

    public void setParamValueType(String paramValueType) {
        this.paramValueType = paramValueType;
    }

    @Basic
    @Column(name = "PARAM_POSSIBLE_VALUE")
    public String getParamPossibleValue() {
        return paramPossibleValue;
    }

    public void setParamPossibleValue(String paramPossibleValue) {
        this.paramPossibleValue = paramPossibleValue;
    }

    @Basic
    @Column(name = "PARAM_LEVEL")
    public String getParamLevel() {
        return paramLevel;
    }

    public void setParamLevel(String paramLevel) {
        this.paramLevel = paramLevel;
    }

    @Basic
    @Column(name = "PARAM_DISPLAY")
    public String getParamDisplay() {
        return paramDisplay;
    }

    public void setParamDisplay(String paramDisplay) {
        this.paramDisplay = paramDisplay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeUserSettingDefaultPo that = (MoeUserSettingDefaultPo) o;

        if (paramName != null ? !paramName.equals(that.paramName) : that.paramName != null) return false;
        if (paramNameDesc != null ? !paramNameDesc.equals(that.paramNameDesc) : that.paramNameDesc != null)
            return false;
        if (defaultParamValue != null ? !defaultParamValue.equals(that.defaultParamValue) : that.defaultParamValue != null)
            return false;
        if (paramValueType != null ? !paramValueType.equals(that.paramValueType) : that.paramValueType != null)
            return false;
        if (paramPossibleValue != null ? !paramPossibleValue.equals(that.paramPossibleValue) : that.paramPossibleValue != null)
            return false;
        if (paramLevel != null ? !paramLevel.equals(that.paramLevel) : that.paramLevel != null) return false;
        if (paramDisplay != null ? !paramDisplay.equals(that.paramDisplay) : that.paramDisplay != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = paramName != null ? paramName.hashCode() : 0;
        result = 31 * result + (paramNameDesc != null ? paramNameDesc.hashCode() : 0);
        result = 31 * result + (defaultParamValue != null ? defaultParamValue.hashCode() : 0);
        result = 31 * result + (paramValueType != null ? paramValueType.hashCode() : 0);
        result = 31 * result + (paramPossibleValue != null ? paramPossibleValue.hashCode() : 0);
        result = 31 * result + (paramLevel != null ? paramLevel.hashCode() : 0);
        result = 31 * result + (paramDisplay != null ? paramDisplay.hashCode() : 0);
        return result;
    }
}
