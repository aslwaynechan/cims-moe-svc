package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeOrderLogPoPK implements Serializable {
    private static final long serialVersionUID = 9183577544322265538L;
    private long ordNo;
    private String refNo;

    @Column(name = "ORD_NO")
    @Id
    public long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(long ordNo) {
        this.ordNo = ordNo;
    }

    @Column(name = "REF_NO")
    @Id
    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeOrderLogPoPK that = (MoeOrderLogPoPK) o;

        if (ordNo != that.ordNo) return false;
        if (refNo != null ? !refNo.equals(that.refNo) : that.refNo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (ordNo ^ (ordNo >>> 32));
        result = 31 * result + (refNo != null ? refNo.hashCode() : 0);
        return result;
    }
}
