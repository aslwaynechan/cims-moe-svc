package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "MOE_EHR_MED_ALLERGEN_LOG")
@IdClass(MoeEhrMedAllergenLogPoPK.class)
public class MoeEhrMedAllergenLogPo implements Serializable, Comparable<MoeEhrMedAllergenLogPo> {
    private static final long serialVersionUID = 760483147963949721L;
    private String hospcode;
    private long ordNo;
    private long cmsItemNo;
    private String allergen;
    private String matchType;
    private String screenMsg;
    private String certainty;
    private String additionInfo;
    private String overrideReason;
    private String ackBy;
    private Date ackDate;
    private String manifestation;
    private long rowNo;
    private String overrideStatus;
    private String refNo;
    private MoeMedProfileLogPo moeMedProfileLog;

    @Id
    @Column(name = "HOSPCODE")
    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    @Id
    @Column(name = "ORD_NO")
    public long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(long ordNo) {
        this.ordNo = ordNo;
    }

    @Id
    @Column(name = "CMS_ITEM_NO")
    public long getCmsItemNo() {
        return cmsItemNo;
    }

    public void setCmsItemNo(long cmsItemNo) {
        this.cmsItemNo = cmsItemNo;
    }

    @Basic
    @Column(name = "ALLERGEN")
    public String getAllergen() {
        return allergen;
    }

    public void setAllergen(String allergen) {
        this.allergen = allergen;
    }

    @Basic
    @Column(name = "MATCH_TYPE")
    public String getMatchType() {
        return matchType;
    }

    public void setMatchType(String matchType) {
        this.matchType = matchType;
    }

    @Basic
    @Column(name = "SCREEN_MSG")
    public String getScreenMsg() {
        return screenMsg;
    }

    public void setScreenMsg(String screenMsg) {
        this.screenMsg = screenMsg;
    }

    @Basic
    @Column(name = "CERTAINTY")
    public String getCertainty() {
        return certainty;
    }

    public void setCertainty(String certainty) {
        this.certainty = certainty;
    }

    @Basic
    @Column(name = "ADDITION_INFO")
    public String getAdditionInfo() {
        return additionInfo;
    }

    public void setAdditionInfo(String additionInfo) {
        this.additionInfo = additionInfo;
    }

    @Basic
    @Column(name = "OVERRIDE_REASON")
    public String getOverrideReason() {
        return overrideReason;
    }

    public void setOverrideReason(String overrideReason) {
        this.overrideReason = overrideReason;
    }

    @Basic
    @Column(name = "ACK_BY")
    public String getAckBy() {
        return ackBy;
    }

    public void setAckBy(String ackBy) {
        this.ackBy = ackBy;
    }

    @Basic
    @Column(name = "ACK_DATE")
    public Date getAckDate() {
        return ackDate;
    }

    public void setAckDate(Date ackDate) {
        this.ackDate = ackDate;
    }

    @Basic
    @Column(name = "MANIFESTATION")
    public String getManifestation() {
        return manifestation;
    }

    public void setManifestation(String manifestation) {
        this.manifestation = manifestation;
    }

    //Chris 20190927 Fix id about uncomment @Id  -Start
    //Chris 20190926 Fix id  -Start
    @Id
    //@Basic
    //Chris 20190926 Fix id  -End
    //Chris 20190927 Fix id about uncomment @Id  -End
    @Column(name = "ROW_NO")
    public long getRowNo() {
        return rowNo;
    }

    public void setRowNo(long rowNo) {
        this.rowNo = rowNo;
    }

    @Basic
    @Column(name = "OVERRIDE_STATUS")
    public String getOverrideStatus() {
        return overrideStatus;
    }

    public void setOverrideStatus(String overrideStatus) {
        this.overrideStatus = overrideStatus;
    }

    @Id
    @Column(name = "REF_NO")
    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns( {
            @JoinColumn(name = "hospcode", referencedColumnName = "hospcode", nullable = false, insertable = false, updatable = false),
            @JoinColumn(name = "ord_no", referencedColumnName = "ord_no", nullable = false, insertable = false, updatable = false),
            @JoinColumn(name = "cms_item_no", referencedColumnName = "cms_item_no", nullable = false, insertable = false, updatable = false),
            @JoinColumn(name = "ref_no", referencedColumnName = "ref_no", nullable = false, insertable = false, updatable = false) })
    public MoeMedProfileLogPo getMoeMedProfileLog() {
        return this.moeMedProfileLog;
    }

    public void setMoeMedProfileLog(MoeMedProfileLogPo moeMedProfileLog) {
        this.moeMedProfileLog = moeMedProfileLog;
    }

    @Override
    public int compareTo(MoeEhrMedAllergenLogPo o) {
        long thisVal = this.getRowNo();
        long anotherVal = o.getRowNo();
        return (thisVal<anotherVal ? -1 : (thisVal==anotherVal ? 0 : 1));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeEhrMedAllergenLogPo that = (MoeEhrMedAllergenLogPo) o;

        if (ordNo != that.ordNo) return false;
        if (cmsItemNo != that.cmsItemNo) return false;
        if (rowNo != that.rowNo) return false;
        if (hospcode != null ? !hospcode.equals(that.hospcode) : that.hospcode != null) return false;
        if (allergen != null ? !allergen.equals(that.allergen) : that.allergen != null) return false;
        if (matchType != null ? !matchType.equals(that.matchType) : that.matchType != null) return false;
        if (screenMsg != null ? !screenMsg.equals(that.screenMsg) : that.screenMsg != null) return false;
        if (certainty != null ? !certainty.equals(that.certainty) : that.certainty != null) return false;
        if (additionInfo != null ? !additionInfo.equals(that.additionInfo) : that.additionInfo != null) return false;
        if (overrideReason != null ? !overrideReason.equals(that.overrideReason) : that.overrideReason != null)
            return false;
        if (ackBy != null ? !ackBy.equals(that.ackBy) : that.ackBy != null) return false;
        if (ackDate != null ? !ackDate.equals(that.ackDate) : that.ackDate != null) return false;
        if (manifestation != null ? !manifestation.equals(that.manifestation) : that.manifestation != null)
            return false;
        if (overrideStatus != null ? !overrideStatus.equals(that.overrideStatus) : that.overrideStatus != null)
            return false;
        if (refNo != null ? !refNo.equals(that.refNo) : that.refNo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hospcode != null ? hospcode.hashCode() : 0;
        result = 31 * result + (int) (ordNo ^ (ordNo >>> 32));
        result = 31 * result + (int) (cmsItemNo ^ (cmsItemNo >>> 32));
        result = 31 * result + (allergen != null ? allergen.hashCode() : 0);
        result = 31 * result + (matchType != null ? matchType.hashCode() : 0);
        result = 31 * result + (screenMsg != null ? screenMsg.hashCode() : 0);
        result = 31 * result + (certainty != null ? certainty.hashCode() : 0);
        result = 31 * result + (additionInfo != null ? additionInfo.hashCode() : 0);
        result = 31 * result + (overrideReason != null ? overrideReason.hashCode() : 0);
        result = 31 * result + (ackBy != null ? ackBy.hashCode() : 0);
        result = 31 * result + (ackDate != null ? ackDate.hashCode() : 0);
        result = 31 * result + (manifestation != null ? manifestation.hashCode() : 0);
        result = 31 * result + (int) (rowNo ^ (rowNo >>> 32));
        result = 31 * result + (overrideStatus != null ? overrideStatus.hashCode() : 0);
        result = 31 * result + (refNo != null ? refNo.hashCode() : 0);
        return result;
    }
}
