package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeFreqTermIdMapPoPK implements Serializable {
    private static final long serialVersionUID = 8033528586243281236L;
    private String freqCode;
    private long ehrTermId;

    @Column(name = "FREQ_CODE")
    @Id
    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    @Column(name = "EHR_TERM_ID")
    @Id
    public long getEhrTermId() {
        return ehrTermId;
    }

    public void setEhrTermId(long ehrTermId) {
        this.ehrTermId = ehrTermId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeFreqTermIdMapPoPK that = (MoeFreqTermIdMapPoPK) o;

        if (ehrTermId != that.ehrTermId) return false;
        if (freqCode != null ? !freqCode.equals(that.freqCode) : that.freqCode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = freqCode != null ? freqCode.hashCode() : 0;
        result = 31 * result + (int) (ehrTermId ^ (ehrTermId >>> 32));
        return result;
    }
}
