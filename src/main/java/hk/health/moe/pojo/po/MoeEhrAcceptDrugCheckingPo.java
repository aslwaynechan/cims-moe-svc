package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "MOE_EHR_ACCEPT_DRUG_CHECKING")
@IdClass(MoeEhrAcceptDrugCheckingPoPK.class)
public class MoeEhrAcceptDrugCheckingPo implements Serializable {
    private static final long serialVersionUID = 5002314543237728860L;
    //private Long ordNo;
    private String acceptId;
    private String allergen;
    private String drugVtm;
    private String screenMsg;
    private String buttonLabel;
    private Date recordDtm;
    private MoeOrderPo moeOrder;

/*    @Basic
    @Column(name = "ORD_NO")
    public Long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(Long ordNo) {
        this.ordNo = ordNo;
    }*/

    @Id
    @Column(name = "ACCEPT_ID")
    public String getAcceptId() {
        return acceptId;
    }

    public void setAcceptId(String acceptId) {
        this.acceptId = acceptId;
    }

    @Basic
    @Column(name = "ALLERGEN")
    public String getAllergen() {
        return allergen;
    }

    public void setAllergen(String allergen) {
        this.allergen = allergen;
    }

    @Basic
    @Column(name = "DRUG_VTM")
    public String getDrugVtm() {
        return drugVtm;
    }

    public void setDrugVtm(String drugVtm) {
        this.drugVtm = drugVtm;
    }

    @Basic
    @Column(name = "SCREEN_MSG")
    public String getScreenMsg() {
        return screenMsg;
    }

    public void setScreenMsg(String screenMsg) {
        this.screenMsg = screenMsg;
    }

    @Id
    @Column(name = "BUTTON_LABEL")
    public String getButtonLabel() {
        return buttonLabel;
    }

    public void setButtonLabel(String buttonLabel) {
        this.buttonLabel = buttonLabel;
    }

    @Id
    @Column(name = "RECORD_DTM")
    public Date getRecordDtm() {
        return recordDtm;
    }

    public void setRecordDtm(Date recordDtm) {
        this.recordDtm = recordDtm;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORD_NO")
    public MoeOrderPo getMoeOrder() {
        return this.moeOrder;
    }

    public void setMoeOrder(MoeOrderPo moeOrder) {
        this.moeOrder = moeOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeEhrAcceptDrugCheckingPo that = (MoeEhrAcceptDrugCheckingPo) o;

        //if (ordNo != null ? !ordNo.equals(that.ordNo) : that.ordNo != null) return false;
        if (acceptId != null ? !acceptId.equals(that.acceptId) : that.acceptId != null) return false;
        if (allergen != null ? !allergen.equals(that.allergen) : that.allergen != null) return false;
        if (drugVtm != null ? !drugVtm.equals(that.drugVtm) : that.drugVtm != null) return false;
        if (screenMsg != null ? !screenMsg.equals(that.screenMsg) : that.screenMsg != null) return false;
        if (buttonLabel != null ? !buttonLabel.equals(that.buttonLabel) : that.buttonLabel != null) return false;
        if (recordDtm != null ? !recordDtm.equals(that.recordDtm) : that.recordDtm != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        //int result = ordNo != null ? ordNo.hashCode() : 0;
        int result = 0;
        result = 31 * result + (acceptId != null ? acceptId.hashCode() : 0);
        result = 31 * result + (allergen != null ? allergen.hashCode() : 0);
        result = 31 * result + (drugVtm != null ? drugVtm.hashCode() : 0);
        result = 31 * result + (screenMsg != null ? screenMsg.hashCode() : 0);
        result = 31 * result + (buttonLabel != null ? buttonLabel.hashCode() : 0);
        result = 31 * result + (recordDtm != null ? recordDtm.hashCode() : 0);
        return result;
    }
}
