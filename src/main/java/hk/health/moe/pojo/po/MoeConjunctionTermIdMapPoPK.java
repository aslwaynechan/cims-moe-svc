package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeConjunctionTermIdMapPoPK implements Serializable {
    private static final long serialVersionUID = 5414438968589777728L;
    private String ehrConjunctionDesc;
    private long ehrTermId;

    @Column(name = "EHR_CONJUNCTION_DESC")
    @Id
    public String getEhrConjunctionDesc() {
        return ehrConjunctionDesc;
    }

    public void setEhrConjunctionDesc(String ehrConjunctionDesc) {
        this.ehrConjunctionDesc = ehrConjunctionDesc;
    }

    @Column(name = "EHR_TERM_ID")
    @Id
    public long getEhrTermId() {
        return ehrTermId;
    }

    public void setEhrTermId(long ehrTermId) {
        this.ehrTermId = ehrTermId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeConjunctionTermIdMapPoPK that = (MoeConjunctionTermIdMapPoPK) o;

        if (ehrTermId != that.ehrTermId) return false;
        if (ehrConjunctionDesc != null ? !ehrConjunctionDesc.equals(that.ehrConjunctionDesc) : that.ehrConjunctionDesc != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ehrConjunctionDesc != null ? ehrConjunctionDesc.hashCode() : 0;
        result = 31 * result + (int) (ehrTermId ^ (ehrTermId >>> 32));
        return result;
    }
}
