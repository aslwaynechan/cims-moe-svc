package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_BASE_UNIT")
public class MoeBaseUnitPo  implements Serializable {
    private static final long serialVersionUID = 541901385934950584L;
    private long baseUnitId;
    private String baseUnit;
    private String forStrengthUnit;
    private String forUnitOfMeasure;
    private String forBaseUnit;
    private String forPrescribeUnit;
    private String forDispenseUnit;

    @Id
    @Column(name = "BASE_UNIT_ID")
    public long getBaseUnitId() {
        return baseUnitId;
    }

    public void setBaseUnitId(long baseUnitId) {
        this.baseUnitId = baseUnitId;
    }

    @Basic
    @Column(name = "BASE_UNIT")
    public String getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(String baseUnit) {
        this.baseUnit = baseUnit;
    }

    @Basic
    @Column(name = "FOR_STRENGTH_UNIT")
    public String getForStrengthUnit() {
        return forStrengthUnit;
    }

    public void setForStrengthUnit(String forStrengthUnit) {
        this.forStrengthUnit = forStrengthUnit;
    }

    @Basic
    @Column(name = "FOR_UNIT_OF_MEASURE")
    public String getForUnitOfMeasure() {
        return forUnitOfMeasure;
    }

    public void setForUnitOfMeasure(String forUnitOfMeasure) {
        this.forUnitOfMeasure = forUnitOfMeasure;
    }

    @Basic
    @Column(name = "FOR_BASE_UNIT")
    public String getForBaseUnit() {
        return forBaseUnit;
    }

    public void setForBaseUnit(String forBaseUnit) {
        this.forBaseUnit = forBaseUnit;
    }

    @Basic
    @Column(name = "FOR_PRESCRIBE_UNIT")
    public String getForPrescribeUnit() {
        return forPrescribeUnit;
    }

    public void setForPrescribeUnit(String forPrescribeUnit) {
        this.forPrescribeUnit = forPrescribeUnit;
    }

    @Basic
    @Column(name = "FOR_DISPENSE_UNIT")
    public String getForDispenseUnit() {
        return forDispenseUnit;
    }

    public void setForDispenseUnit(String forDispenseUnit) {
        this.forDispenseUnit = forDispenseUnit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeBaseUnitPo that = (MoeBaseUnitPo) o;

        if (baseUnitId != that.baseUnitId) return false;
        if (baseUnit != null ? !baseUnit.equals(that.baseUnit) : that.baseUnit != null) return false;
        if (forStrengthUnit != null ? !forStrengthUnit.equals(that.forStrengthUnit) : that.forStrengthUnit != null)
            return false;
        if (forUnitOfMeasure != null ? !forUnitOfMeasure.equals(that.forUnitOfMeasure) : that.forUnitOfMeasure != null)
            return false;
        if (forBaseUnit != null ? !forBaseUnit.equals(that.forBaseUnit) : that.forBaseUnit != null) return false;
        if (forPrescribeUnit != null ? !forPrescribeUnit.equals(that.forPrescribeUnit) : that.forPrescribeUnit != null)
            return false;
        if (forDispenseUnit != null ? !forDispenseUnit.equals(that.forDispenseUnit) : that.forDispenseUnit != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (baseUnitId ^ (baseUnitId >>> 32));
        result = 31 * result + (baseUnit != null ? baseUnit.hashCode() : 0);
        result = 31 * result + (forStrengthUnit != null ? forStrengthUnit.hashCode() : 0);
        result = 31 * result + (forUnitOfMeasure != null ? forUnitOfMeasure.hashCode() : 0);
        result = 31 * result + (forBaseUnit != null ? forBaseUnit.hashCode() : 0);
        result = 31 * result + (forPrescribeUnit != null ? forPrescribeUnit.hashCode() : 0);
        result = 31 * result + (forDispenseUnit != null ? forDispenseUnit.hashCode() : 0);
        return result;
    }
}
