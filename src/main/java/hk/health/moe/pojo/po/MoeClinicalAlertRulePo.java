package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_CLINICAL_ALERT_RULE")
public class MoeClinicalAlertRulePo implements Serializable {
    private static final long serialVersionUID = -9063372025468390379L;
    private long ruleId;
    private String vtm;
    private String matchType;
    private String alertDesc;

    @Id
    @Column(name = "RULE_ID")
    public long getRuleId() {
        return ruleId;
    }

    public void setRuleId(long ruleId) {
        this.ruleId = ruleId;
    }

    @Basic
    @Column(name = "VTM")
    public String getVtm() {
        return vtm;
    }

    public void setVtm(String vtm) {
        this.vtm = vtm;
    }

    @Basic
    @Column(name = "MATCH_TYPE")
    public String getMatchType() {
        return matchType;
    }

    public void setMatchType(String matchType) {
        this.matchType = matchType;
    }

    @Basic
    @Column(name = "ALERT_DESC")
    public String getAlertDesc() {
        return alertDesc;
    }

    public void setAlertDesc(String alertDesc) {
        this.alertDesc = alertDesc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeClinicalAlertRulePo that = (MoeClinicalAlertRulePo) o;

        if (ruleId != that.ruleId) return false;
        if (vtm != null ? !vtm.equals(that.vtm) : that.vtm != null) return false;
        if (matchType != null ? !matchType.equals(that.matchType) : that.matchType != null) return false;
        if (alertDesc != null ? !alertDesc.equals(that.alertDesc) : that.alertDesc != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (ruleId ^ (ruleId >>> 32));
        result = 31 * result + (vtm != null ? vtm.hashCode() : 0);
        result = 31 * result + (matchType != null ? matchType.hashCode() : 0);
        result = 31 * result + (alertDesc != null ? alertDesc.hashCode() : 0);
        return result;
    }
}
