package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_SUPPL_FREQ_TERM_ID_MAP")
@IdClass(MoeSupplFreqTermIdMapPoPK.class)
public class MoeSupplFreqTermIdMapPo implements Serializable {
    private static final long serialVersionUID = 2470227843048674244L;
    private String freqCode;
    private Long freq1;
    private String supplFreqCode;
    private String supplFreqDescEng;
    private Long supFreq1;
    private Long supFreq2;
    private String dayOfWeek;
    private String ehrSupplFreqDesc;
    private long ehrTermId;
    private String defaultMap;
    private String ehrStatus;

    @Basic
    @Column(name = "FREQ_CODE")
    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    @Basic
    @Column(name = "FREQ1")
    public Long getFreq1() {
        return freq1;
    }

    public void setFreq1(Long freq1) {
        this.freq1 = freq1;
    }

    @Id
    @Column(name = "SUPPL_FREQ_CODE")
    public String getSupplFreqCode() {
        return supplFreqCode;
    }

    public void setSupplFreqCode(String supplFreqCode) {
        this.supplFreqCode = supplFreqCode;
    }

    @Id
    @Column(name = "SUPPL_FREQ_DESC_ENG")
    public String getSupplFreqDescEng() {
        return supplFreqDescEng;
    }

    public void setSupplFreqDescEng(String supplFreqDescEng) {
        this.supplFreqDescEng = supplFreqDescEng;
    }

    @Basic
    @Column(name = "SUP_FREQ1")
    public Long getSupFreq1() {
        return supFreq1;
    }

    public void setSupFreq1(Long supFreq1) {
        this.supFreq1 = supFreq1;
    }

    @Basic
    @Column(name = "SUP_FREQ2")
    public Long getSupFreq2() {
        return supFreq2;
    }

    public void setSupFreq2(Long supFreq2) {
        this.supFreq2 = supFreq2;
    }

    @Basic
    @Column(name = "DAY_OF_WEEK")
    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    @Basic
    @Column(name = "EHR_SUPPL_FREQ_DESC")
    public String getEhrSupplFreqDesc() {
        return ehrSupplFreqDesc;
    }

    public void setEhrSupplFreqDesc(String ehrSupplFreqDesc) {
        this.ehrSupplFreqDesc = ehrSupplFreqDesc;
    }

    @Id
    @Column(name = "EHR_TERM_ID")
    public long getEhrTermId() {
        return ehrTermId;
    }

    public void setEhrTermId(long ehrTermId) {
        this.ehrTermId = ehrTermId;
    }

    @Basic
    @Column(name = "DEFAULT_MAP")
    public String getDefaultMap() {
        return defaultMap;
    }

    public void setDefaultMap(String defaultMap) {
        this.defaultMap = defaultMap;
    }

    @Basic
    @Column(name = "EHR_STATUS")
    public String getEhrStatus() {
        return ehrStatus;
    }

    public void setEhrStatus(String ehrStatus) {
        this.ehrStatus = ehrStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeSupplFreqTermIdMapPo that = (MoeSupplFreqTermIdMapPo) o;

        if (ehrTermId != that.ehrTermId) return false;
        if (freqCode != null ? !freqCode.equals(that.freqCode) : that.freqCode != null) return false;
        if (freq1 != null ? !freq1.equals(that.freq1) : that.freq1 != null) return false;
        if (supplFreqCode != null ? !supplFreqCode.equals(that.supplFreqCode) : that.supplFreqCode != null)
            return false;
        if (supplFreqDescEng != null ? !supplFreqDescEng.equals(that.supplFreqDescEng) : that.supplFreqDescEng != null)
            return false;
        if (supFreq1 != null ? !supFreq1.equals(that.supFreq1) : that.supFreq1 != null) return false;
        if (supFreq2 != null ? !supFreq2.equals(that.supFreq2) : that.supFreq2 != null) return false;
        if (dayOfWeek != null ? !dayOfWeek.equals(that.dayOfWeek) : that.dayOfWeek != null) return false;
        if (ehrSupplFreqDesc != null ? !ehrSupplFreqDesc.equals(that.ehrSupplFreqDesc) : that.ehrSupplFreqDesc != null)
            return false;
        if (defaultMap != null ? !defaultMap.equals(that.defaultMap) : that.defaultMap != null) return false;
        if (ehrStatus != null ? !ehrStatus.equals(that.ehrStatus) : that.ehrStatus != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = freqCode != null ? freqCode.hashCode() : 0;
        result = 31 * result + (freq1 != null ? freq1.hashCode() : 0);
        result = 31 * result + (supplFreqCode != null ? supplFreqCode.hashCode() : 0);
        result = 31 * result + (supplFreqDescEng != null ? supplFreqDescEng.hashCode() : 0);
        result = 31 * result + (supFreq1 != null ? supFreq1.hashCode() : 0);
        result = 31 * result + (supFreq2 != null ? supFreq2.hashCode() : 0);
        result = 31 * result + (dayOfWeek != null ? dayOfWeek.hashCode() : 0);
        result = 31 * result + (ehrSupplFreqDesc != null ? ehrSupplFreqDesc.hashCode() : 0);
        result = 31 * result + (int) (ehrTermId ^ (ehrTermId >>> 32));
        result = 31 * result + (defaultMap != null ? defaultMap.hashCode() : 0);
        result = 31 * result + (ehrStatus != null ? ehrStatus.hashCode() : 0);
        return result;
    }
}
