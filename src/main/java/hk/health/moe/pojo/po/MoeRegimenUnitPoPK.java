package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeRegimenUnitPoPK implements Serializable {
    private static final long serialVersionUID = 3308898927147979326L;
    private String regimenType;
    private String durationUnit;

    @Column(name = "REGIMEN_TYPE")
    @Id
    public String getRegimenType() {
        return regimenType;
    }

    public void setRegimenType(String regimenType) {
        this.regimenType = regimenType;
    }

    @Column(name = "DURATION_UNIT")
    @Id
    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeRegimenUnitPoPK that = (MoeRegimenUnitPoPK) o;

        if (regimenType != null ? !regimenType.equals(that.regimenType) : that.regimenType != null) return false;
        if (durationUnit != null ? !durationUnit.equals(that.durationUnit) : that.durationUnit != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = regimenType != null ? regimenType.hashCode() : 0;
        result = 31 * result + (durationUnit != null ? durationUnit.hashCode() : 0);
        return result;
    }
}
