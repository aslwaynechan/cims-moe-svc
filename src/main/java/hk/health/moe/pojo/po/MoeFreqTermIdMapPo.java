package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_FREQ_TERM_ID_MAP")
@IdClass(MoeFreqTermIdMapPoPK.class)
public class MoeFreqTermIdMapPo implements Serializable {
    private static final long serialVersionUID = 203390323701442697L;
    private String freqCode;
    private Long freq1;
    private String ehrFreqDesc;
    private long ehrTermId;
    private String defaultMap;
    private String mapToSupplFreq;
    private String localFreqDesc;
    private String supplFreqDescEng;
    private String ehrSupplFreqDesc;
    private Long ehrSupplFreqTermId;
    private String ehrStatus;

    @Id
    @Column(name = "FREQ_CODE")
    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    @Basic
    @Column(name = "FREQ1")
    public Long getFreq1() {
        return freq1;
    }

    public void setFreq1(Long freq1) {
        this.freq1 = freq1;
    }

    @Basic
    @Column(name = "EHR_FREQ_DESC")
    public String getEhrFreqDesc() {
        return ehrFreqDesc;
    }

    public void setEhrFreqDesc(String ehrFreqDesc) {
        this.ehrFreqDesc = ehrFreqDesc;
    }

    @Id
    @Column(name = "EHR_TERM_ID")
    public long getEhrTermId() {
        return ehrTermId;
    }

    public void setEhrTermId(long ehrTermId) {
        this.ehrTermId = ehrTermId;
    }

    @Basic
    @Column(name = "DEFAULT_MAP")
    public String getDefaultMap() {
        return defaultMap;
    }

    public void setDefaultMap(String defaultMap) {
        this.defaultMap = defaultMap;
    }

    @Basic
    @Column(name = "MAP_TO_SUPPL_FREQ")
    public String getMapToSupplFreq() {
        return mapToSupplFreq;
    }

    public void setMapToSupplFreq(String mapToSupplFreq) {
        this.mapToSupplFreq = mapToSupplFreq;
    }

    @Basic
    @Column(name = "LOCAL_FREQ_DESC")
    public String getLocalFreqDesc() {
        return localFreqDesc;
    }

    public void setLocalFreqDesc(String localFreqDesc) {
        this.localFreqDesc = localFreqDesc;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_DESC_ENG")
    public String getSupplFreqDescEng() {
        return supplFreqDescEng;
    }

    public void setSupplFreqDescEng(String supplFreqDescEng) {
        this.supplFreqDescEng = supplFreqDescEng;
    }

    @Basic
    @Column(name = "EHR_SUPPL_FREQ_DESC")
    public String getEhrSupplFreqDesc() {
        return ehrSupplFreqDesc;
    }

    public void setEhrSupplFreqDesc(String ehrSupplFreqDesc) {
        this.ehrSupplFreqDesc = ehrSupplFreqDesc;
    }

    @Basic
    @Column(name = "EHR_SUPPL_FREQ_TERM_ID")
    public Long getEhrSupplFreqTermId() {
        return ehrSupplFreqTermId;
    }

    public void setEhrSupplFreqTermId(Long ehrSupplFreqTermId) {
        this.ehrSupplFreqTermId = ehrSupplFreqTermId;
    }

    @Basic
    @Column(name = "EHR_STATUS")
    public String getEhrStatus() {
        return ehrStatus;
    }

    public void setEhrStatus(String ehrStatus) {
        this.ehrStatus = ehrStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeFreqTermIdMapPo that = (MoeFreqTermIdMapPo) o;

        if (ehrTermId != that.ehrTermId) return false;
        if (freqCode != null ? !freqCode.equals(that.freqCode) : that.freqCode != null) return false;
        if (freq1 != null ? !freq1.equals(that.freq1) : that.freq1 != null) return false;
        if (ehrFreqDesc != null ? !ehrFreqDesc.equals(that.ehrFreqDesc) : that.ehrFreqDesc != null) return false;
        if (defaultMap != null ? !defaultMap.equals(that.defaultMap) : that.defaultMap != null) return false;
        if (mapToSupplFreq != null ? !mapToSupplFreq.equals(that.mapToSupplFreq) : that.mapToSupplFreq != null)
            return false;
        if (localFreqDesc != null ? !localFreqDesc.equals(that.localFreqDesc) : that.localFreqDesc != null)
            return false;
        if (supplFreqDescEng != null ? !supplFreqDescEng.equals(that.supplFreqDescEng) : that.supplFreqDescEng != null)
            return false;
        if (ehrSupplFreqDesc != null ? !ehrSupplFreqDesc.equals(that.ehrSupplFreqDesc) : that.ehrSupplFreqDesc != null)
            return false;
        if (ehrSupplFreqTermId != null ? !ehrSupplFreqTermId.equals(that.ehrSupplFreqTermId) : that.ehrSupplFreqTermId != null)
            return false;
        if (ehrStatus != null ? !ehrStatus.equals(that.ehrStatus) : that.ehrStatus != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = freqCode != null ? freqCode.hashCode() : 0;
        result = 31 * result + (freq1 != null ? freq1.hashCode() : 0);
        result = 31 * result + (ehrFreqDesc != null ? ehrFreqDesc.hashCode() : 0);
        result = 31 * result + (int) (ehrTermId ^ (ehrTermId >>> 32));
        result = 31 * result + (defaultMap != null ? defaultMap.hashCode() : 0);
        result = 31 * result + (mapToSupplFreq != null ? mapToSupplFreq.hashCode() : 0);
        result = 31 * result + (localFreqDesc != null ? localFreqDesc.hashCode() : 0);
        result = 31 * result + (supplFreqDescEng != null ? supplFreqDescEng.hashCode() : 0);
        result = 31 * result + (ehrSupplFreqDesc != null ? ehrSupplFreqDesc.hashCode() : 0);
        result = 31 * result + (ehrSupplFreqTermId != null ? ehrSupplFreqTermId.hashCode() : 0);
        result = 31 * result + (ehrStatus != null ? ehrStatus.hashCode() : 0);
        return result;
    }
}
