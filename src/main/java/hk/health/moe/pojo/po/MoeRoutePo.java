package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MOE_ROUTE")
public class MoeRoutePo implements Serializable {
    private static final long serialVersionUID = -3046591229217706774L;
    private long routeId;
    private String routeEng;
    private String routeChi;
    private String routeOtherName;
    private BigDecimal routeMultiplier;
    private long rank;
    private String enableInRegimen;
    private Set<MoeFormRouteMapPo> moeFormRouteMaps = new HashSet<>();
    private Set<MoeRouteSiteMapPo> moeRouteSiteMaps = new HashSet<>();

    @Id
    @Column(name = "ROUTE_ID")
    public long getRouteId() {
        return routeId;
    }

    public void setRouteId(long routeId) {
        this.routeId = routeId;
    }

    @Basic
    @Column(name = "ROUTE_ENG")
    public String getRouteEng() {
        return routeEng;
    }

    public void setRouteEng(String routeEng) {
        this.routeEng = routeEng;
    }

    @Basic
    @Column(name = "ROUTE_CHI")
    public String getRouteChi() {
        return routeChi;
    }

    public void setRouteChi(String routeChi) {
        this.routeChi = routeChi;
    }

    @Basic
    @Column(name = "ROUTE_OTHER_NAME")
    public String getRouteOtherName() {
        return routeOtherName;
    }

    public void setRouteOtherName(String routeOtherName) {
        this.routeOtherName = routeOtherName;
    }

    @Basic
    @Column(name = "ROUTE_MULTIPLIER")
    public BigDecimal getRouteMultiplier() {
        return routeMultiplier;
    }

    public void setRouteMultiplier(BigDecimal routeMultiplier) {
        this.routeMultiplier = routeMultiplier;
    }

    @Basic
    @Column(name = "RANK")
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @Basic
    @Column(name = "ENABLE_IN_REGIMEN")
    public String getEnableInRegimen() {
        return enableInRegimen;
    }

    public void setEnableInRegimen(String enableInRegimen) {
        this.enableInRegimen = enableInRegimen;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moeRoute")
    public Set<MoeFormRouteMapPo> getMoeFormRouteMaps() {
        return this.moeFormRouteMaps;
    }

    public void setMoeFormRouteMaps(Set<MoeFormRouteMapPo> moeFormRouteMaps) {
        this.moeFormRouteMaps = moeFormRouteMaps;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moeRoute")
    public Set<MoeRouteSiteMapPo> getMoeRouteSiteMaps() {
        return this.moeRouteSiteMaps;
    }

    public void setMoeRouteSiteMaps(Set<MoeRouteSiteMapPo> moeRouteSiteMaps) {
        this.moeRouteSiteMaps = moeRouteSiteMaps;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeRoutePo that = (MoeRoutePo) o;

        if (routeId != that.routeId) return false;
        if (rank != that.rank) return false;
        if (routeEng != null ? !routeEng.equals(that.routeEng) : that.routeEng != null) return false;
        if (routeChi != null ? !routeChi.equals(that.routeChi) : that.routeChi != null) return false;
        if (routeOtherName != null ? !routeOtherName.equals(that.routeOtherName) : that.routeOtherName != null)
            return false;
        if (routeMultiplier != null ? !routeMultiplier.equals(that.routeMultiplier) : that.routeMultiplier != null)
            return false;
        if (enableInRegimen != null ? !enableInRegimen.equals(that.enableInRegimen) : that.enableInRegimen != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (routeId ^ (routeId >>> 32));
        result = 31 * result + (routeEng != null ? routeEng.hashCode() : 0);
        result = 31 * result + (routeChi != null ? routeChi.hashCode() : 0);
        result = 31 * result + (routeOtherName != null ? routeOtherName.hashCode() : 0);
        result = 31 * result + (routeMultiplier != null ? routeMultiplier.hashCode() : 0);
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        result = 31 * result + (enableInRegimen != null ? enableInRegimen.hashCode() : 0);
        return result;
    }
}
