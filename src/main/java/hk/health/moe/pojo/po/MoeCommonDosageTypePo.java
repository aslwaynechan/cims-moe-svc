package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MOE_COMMON_DOSAGE_TYPE")
public class MoeCommonDosageTypePo implements Serializable {
    private static final long serialVersionUID = 4173274314087460075L;
    private String commonDosageType;
    private String commonDosageTypeDesc;
    private String display;
    private long rank;
    private Set<MoeCommonDosagePo> moeCommonDosages = new HashSet<>();

    @Id
    @Column(name = "COMMON_DOSAGE_TYPE")
    public String getCommonDosageType() {
        return commonDosageType;
    }

    public void setCommonDosageType(String commonDosageType) {
        this.commonDosageType = commonDosageType;
    }

    @Basic
    @Column(name = "COMMON_DOSAGE_TYPE_DESC")
    public String getCommonDosageTypeDesc() {
        return commonDosageTypeDesc;
    }

    public void setCommonDosageTypeDesc(String commonDosageTypeDesc) {
        this.commonDosageTypeDesc = commonDosageTypeDesc;
    }

    @Basic
    @Column(name = "DISPLAY")
    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    @Basic
    @Column(name = "RANK")
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moeCommonDosageType")
    public Set<MoeCommonDosagePo> getMoeCommonDosages() {
        return this.moeCommonDosages;
    }

    public void setMoeCommonDosages(Set<MoeCommonDosagePo> moeCommonDosages) {
        this.moeCommonDosages = moeCommonDosages;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeCommonDosageTypePo that = (MoeCommonDosageTypePo) o;

        if (rank != that.rank) return false;
        if (commonDosageType != null ? !commonDosageType.equals(that.commonDosageType) : that.commonDosageType != null)
            return false;
        if (commonDosageTypeDesc != null ? !commonDosageTypeDesc.equals(that.commonDosageTypeDesc) : that.commonDosageTypeDesc != null)
            return false;
        if (display != null ? !display.equals(that.display) : that.display != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = commonDosageType != null ? commonDosageType.hashCode() : 0;
        result = 31 * result + (commonDosageTypeDesc != null ? commonDosageTypeDesc.hashCode() : 0);
        result = 31 * result + (display != null ? display.hashCode() : 0);
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        return result;
    }
}
