package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "MOE_FREQ")
public class MoeFreqPo implements Serializable {
    private static final long serialVersionUID = 2814966335744462455L;
    private long freqId;
    private String freqCode;
    private String freqEng;
    private String freqChi;
    private long upperLimit;
    private long lowerLimit;
    private BigDecimal freqMultiplier;
    private Long durationValue;
    private String durationUnit;
    private String useInputValue;
    private String useInputMethod;
    private long rank;
    private Long freqMultiplierDenominator;
    private String freqDescEng;

    @Id
    @Column(name = "FREQ_ID")
    public long getFreqId() {
        return freqId;
    }

    public void setFreqId(long freqId) {
        this.freqId = freqId;
    }

    @Basic
    @Column(name = "FREQ_CODE")
    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    @Basic
    @Column(name = "FREQ_ENG")
    public String getFreqEng() {
        return freqEng;
    }

    public void setFreqEng(String freqEng) {
        this.freqEng = freqEng;
    }

    @Basic
    @Column(name = "FREQ_CHI")
    public String getFreqChi() {
        return freqChi;
    }

    public void setFreqChi(String freqChi) {
        this.freqChi = freqChi;
    }

    @Basic
    @Column(name = "UPPER_LIMIT")
    public long getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(long upperLimit) {
        this.upperLimit = upperLimit;
    }

    @Basic
    @Column(name = "LOWER_LIMIT")
    public long getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(long lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    @Basic
    @Column(name = "FREQ_MULTIPLIER")
    public BigDecimal getFreqMultiplier() {
        return freqMultiplier;
    }

    public void setFreqMultiplier(BigDecimal freqMultiplier) {
        this.freqMultiplier = freqMultiplier;
    }

    @Basic
    @Column(name = "DURATION_VALUE")
    public Long getDurationValue() {
        return durationValue;
    }

    public void setDurationValue(Long durationValue) {
        this.durationValue = durationValue;
    }

    @Basic
    @Column(name = "DURATION_UNIT")
    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    @Basic
    @Column(name = "USE_INPUT_VALUE")
    public String getUseInputValue() {
        return useInputValue;
    }

    public void setUseInputValue(String useInputValue) {
        this.useInputValue = useInputValue;
    }

    @Basic
    @Column(name = "USE_INPUT_METHOD")
    public String getUseInputMethod() {
        return useInputMethod;
    }

    public void setUseInputMethod(String useInputMethod) {
        this.useInputMethod = useInputMethod;
    }

    @Basic
    @Column(name = "RANK")
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @Basic
    @Column(name = "FREQ_MULTIPLIER_DENOMINATOR")
    public Long getFreqMultiplierDenominator() {
        return freqMultiplierDenominator;
    }

    public void setFreqMultiplierDenominator(Long freqMultiplierDenominator) {
        this.freqMultiplierDenominator = freqMultiplierDenominator;
    }

    @Basic
    @Column(name = "FREQ_DESC_ENG")
    public String getFreqDescEng() {
        return freqDescEng;
    }

    public void setFreqDescEng(String freqDescEng) {
        this.freqDescEng = freqDescEng;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeFreqPo moeFreqPo = (MoeFreqPo) o;

        if (freqId != moeFreqPo.freqId) return false;
        if (upperLimit != moeFreqPo.upperLimit) return false;
        if (lowerLimit != moeFreqPo.lowerLimit) return false;
        if (freqMultiplier != moeFreqPo.freqMultiplier) return false;
        if (rank != moeFreqPo.rank) return false;
        if (freqCode != null ? !freqCode.equals(moeFreqPo.freqCode) : moeFreqPo.freqCode != null) return false;
        if (freqEng != null ? !freqEng.equals(moeFreqPo.freqEng) : moeFreqPo.freqEng != null) return false;
        if (freqChi != null ? !freqChi.equals(moeFreqPo.freqChi) : moeFreqPo.freqChi != null) return false;
        if (durationValue != null ? !durationValue.equals(moeFreqPo.durationValue) : moeFreqPo.durationValue != null)
            return false;
        if (durationUnit != null ? !durationUnit.equals(moeFreqPo.durationUnit) : moeFreqPo.durationUnit != null)
            return false;
        if (useInputValue != null ? !useInputValue.equals(moeFreqPo.useInputValue) : moeFreqPo.useInputValue != null)
            return false;
        if (useInputMethod != null ? !useInputMethod.equals(moeFreqPo.useInputMethod) : moeFreqPo.useInputMethod != null)
            return false;
        if (freqMultiplierDenominator != null ? !freqMultiplierDenominator.equals(moeFreqPo.freqMultiplierDenominator) : moeFreqPo.freqMultiplierDenominator != null)
            return false;
        if (freqDescEng != null ? !freqDescEng.equals(moeFreqPo.freqDescEng) : moeFreqPo.freqDescEng != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (freqId ^ (freqId >>> 32));
        result = 31 * result + (freqCode != null ? freqCode.hashCode() : 0);
        result = 31 * result + (freqEng != null ? freqEng.hashCode() : 0);
        result = 31 * result + (freqChi != null ? freqChi.hashCode() : 0);
        result = 31 * result + (int) (upperLimit ^ (upperLimit >>> 32));
        result = 31 * result + (int) (lowerLimit ^ (lowerLimit >>> 32));
        result = 31 * result + (freqMultiplier != null ? freqMultiplier.hashCode() : 0);
        result = 31 * result + (durationValue != null ? durationValue.hashCode() : 0);
        result = 31 * result + (durationUnit != null ? durationUnit.hashCode() : 0);
        result = 31 * result + (useInputValue != null ? useInputValue.hashCode() : 0);
        result = 31 * result + (useInputMethod != null ? useInputMethod.hashCode() : 0);
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        result = 31 * result + (freqMultiplierDenominator != null ? freqMultiplierDenominator.hashCode() : 0);
        result = 31 * result + (freqDescEng != null ? freqDescEng.hashCode() : 0);
        return result;
    }
}
