package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_DURATION_UNIT_MULTIPLIER")
public class MoeDurationUnitMultiplierPo implements Serializable {
    private static final long serialVersionUID = -9217888739349151817L;
    private String durationUnit;
    private String durationUnitDesc;
    private long durationMultiplier;

    @Id
    @Column(name = "DURATION_UNIT")
    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    @Basic
    @Column(name = "DURATION_UNIT_DESC")
    public String getDurationUnitDesc() {
        return durationUnitDesc;
    }

    public void setDurationUnitDesc(String durationUnitDesc) {
        this.durationUnitDesc = durationUnitDesc;
    }

    @Basic
    @Column(name = "DURATION_MULTIPLIER")
    public long getDurationMultiplier() {
        return durationMultiplier;
    }

    public void setDurationMultiplier(long durationMultiplier) {
        this.durationMultiplier = durationMultiplier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDurationUnitMultiplierPo that = (MoeDurationUnitMultiplierPo) o;

        if (durationMultiplier != that.durationMultiplier) return false;
        if (durationUnit != null ? !durationUnit.equals(that.durationUnit) : that.durationUnit != null) return false;
        if (durationUnitDesc != null ? !durationUnitDesc.equals(that.durationUnitDesc) : that.durationUnitDesc != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = durationUnit != null ? durationUnit.hashCode() : 0;
        result = 31 * result + (durationUnitDesc != null ? durationUnitDesc.hashCode() : 0);
        result = 31 * result + (int) (durationMultiplier ^ (durationMultiplier >>> 32));
        return result;
    }
}
