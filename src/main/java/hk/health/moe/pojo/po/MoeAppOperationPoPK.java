/*
package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeAppOperationPoPK implements Serializable {
    private String appId;
    private String operationId;

    @Column(name = "APP_ID")
    @Id
    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    @Column(name = "OPERATION_ID")
    @Id
    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeAppOperationPoPK that = (MoeAppOperationPoPK) o;

        if (appId != null ? !appId.equals(that.appId) : that.appId != null) return false;
        if (operationId != null ? !operationId.equals(that.operationId) : that.operationId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = appId != null ? appId.hashCode() : 0;
        result = 31 * result + (operationId != null ? operationId.hashCode() : 0);
        return result;
    }
}
*/
