package hk.health.moe.pojo.po;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name = "MOE_ORDER")
@DynamicUpdate
public class MoeOrderPo implements Serializable {
    private static final long serialVersionUID = 1065061190859892276L;
    private long ordNo;
    private String hospcode;
    private String workstore;
    private String patHospcode;
    private Date ordDate;
    private String ordType;
    private String ordSubtype;
    private String ordStatus;
    private String refNo;
    private String moCode;
    private Long prevOrdNo;
    private Long maxItemNo;
    private Date endDate;
    private String suspend;
    private String privatePatient;
    private String transferPatient;
    private String allowModify;
    private String prescType;
    private String moeOrder;
    private String caseNo;
    private String srcSpecialty;
    private String srcSubspecialty;
    private String ipasWard;
    private String bedNo;
    private String specialty;
    private String subspecialty;
    private String phsWard;
    private String eisSpecCode;
    private String dispHospcode;
    private String dispWorkstore;
    private String ticknum;
    private Date tickdate;
    private String wkstatcode;
    private Date lastUpdDate;
    private String lastUpdBy;
    private Long dispOrdNo;
    private String dischargeReason;
    private String remarkCreateBy;
    private Date remarkCreateDate;
    private String remarkText;
    private String remarkConfirmBy;
    private Date remarkConfirmDate;
    private String remarkStatus;
    private String lastPrescType;
    private String uncollect;
    private String allowPostComment;
    private String printType;
    private String prevHospcode;
    private String prevWorkstore;
    private Date prevDispDate;
    private String prevTicknum;
    private long version;
    private Set<MoeMedProfilePo> moeMedProfiles = new TreeSet<>();
    private MoeEhrOrderPo moeEhrOrder;
    private String backDate;


/*    @SequenceGenerator(name = "SEQ_ORDER_ID", sequenceName = "SEQ_ORDER_ID", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ORDER_ID")*/
/*    @GenericGenerator(name="ordIdGenerator", strategy = "org.hibernate.id.enhanced.TableGenerator",
            parameters = {
                    @Parameter(name="table_name", value = "moe_id_holder"),
                    @Parameter(name="value_column_name", value = "next_id"),
                    @Parameter(name="segment_column_name", value = "id_name"),
                    @Parameter(name="segment_value", value = "ord_no")
            }
    )
    @GeneratedValue(generator="ordIdGenerator")*/
    @Id
    @Column(name = "ORD_NO", unique = true, nullable = false)
    public long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(long ordNo) {
        this.ordNo = ordNo;
    }

    @Basic
    @Column(name = "HOSPCODE")
    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    @Basic
    @Column(name = "WORKSTORE")
    public String getWorkstore() {
        return workstore;
    }

    public void setWorkstore(String workstore) {
        this.workstore = workstore;
    }

    @Basic
    @Column(name = "PAT_HOSPCODE")
    public String getPatHospcode() {
        return patHospcode;
    }

    public void setPatHospcode(String patHospcode) {
        this.patHospcode = patHospcode;
    }

    @Basic
    @Column(name = "ORD_DATE")
    public Date getOrdDate() {
        return ordDate;
    }

    public void setOrdDate(Date ordDate) {
        this.ordDate = ordDate;
    }

    @Basic
    @Column(name = "ORD_TYPE")
    public String getOrdType() {
        return ordType;
    }

    public void setOrdType(String ordType) {
        this.ordType = ordType;
    }

    @Basic
    @Column(name = "ORD_SUBTYPE")
    public String getOrdSubtype() {
        return ordSubtype;
    }

    public void setOrdSubtype(String ordSubtype) {
        this.ordSubtype = ordSubtype;
    }

    @Basic
    @Column(name = "ORD_STATUS")
    public String getOrdStatus() {
        return ordStatus;
    }

    public void setOrdStatus(String ordStatus) {
        this.ordStatus = ordStatus;
    }

    @Basic
    @Column(name = "REF_NO")
    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    @Basic
    @Column(name = "MO_CODE")
    public String getMoCode() {
        return moCode;
    }

    public void setMoCode(String moCode) {
        this.moCode = moCode;
    }

    @Basic
    @Column(name = "PREV_ORD_NO")
    public Long getPrevOrdNo() {
        return prevOrdNo;
    }

    public void setPrevOrdNo(Long prevOrdNo) {
        this.prevOrdNo = prevOrdNo;
    }

    @Basic
    @Column(name = "MAX_ITEM_NO")
    public Long getMaxItemNo() {
        return maxItemNo;
    }

    public void setMaxItemNo(Long maxItemNo) {
        this.maxItemNo = maxItemNo;
    }

    @Basic
    @Column(name = "END_DATE")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "SUSPEND")
    public String getSuspend() {
        return suspend;
    }

    public void setSuspend(String suspend) {
        this.suspend = suspend;
    }

    @Basic
    @Column(name = "PRIVATE_PATIENT")
    public String getPrivatePatient() {
        return privatePatient;
    }

    public void setPrivatePatient(String privatePatient) {
        this.privatePatient = privatePatient;
    }

    @Basic
    @Column(name = "TRANSFER_PATIENT")
    public String getTransferPatient() {
        return transferPatient;
    }

    public void setTransferPatient(String transferPatient) {
        this.transferPatient = transferPatient;
    }

    @Basic
    @Column(name = "ALLOW_MODIFY")
    public String getAllowModify() {
        return allowModify;
    }

    public void setAllowModify(String allowModify) {
        this.allowModify = allowModify;
    }

    @Basic
    @Column(name = "PRESC_TYPE")
    public String getPrescType() {
        return prescType;
    }

    public void setPrescType(String prescType) {
        this.prescType = prescType;
    }

    @Basic
    @Column(name = "MOE_ORDER")
    public String getMoeOrder() {
        return moeOrder;
    }

    public void setMoeOrder(String moeOrder) {
        this.moeOrder = moeOrder;
    }

    @Basic
    @Column(name = "CASE_NO")
    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    @Basic
    @Column(name = "SRC_SPECIALTY")
    public String getSrcSpecialty() {
        return srcSpecialty;
    }

    public void setSrcSpecialty(String srcSpecialty) {
        this.srcSpecialty = srcSpecialty;
    }

    @Basic
    @Column(name = "SRC_SUBSPECIALTY")
    public String getSrcSubspecialty() {
        return srcSubspecialty;
    }

    public void setSrcSubspecialty(String srcSubspecialty) {
        this.srcSubspecialty = srcSubspecialty;
    }

    @Basic
    @Column(name = "IPAS_WARD")
    public String getIpasWard() {
        return ipasWard;
    }

    public void setIpasWard(String ipasWard) {
        this.ipasWard = ipasWard;
    }

    @Basic
    @Column(name = "BED_NO")
    public String getBedNo() {
        return bedNo;
    }

    public void setBedNo(String bedNo) {
        this.bedNo = bedNo;
    }

    @Basic
    @Column(name = "SPECIALTY")
    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    @Basic
    @Column(name = "SUBSPECIALTY")
    public String getSubspecialty() {
        return subspecialty;
    }

    public void setSubspecialty(String subspecialty) {
        this.subspecialty = subspecialty;
    }

    @Basic
    @Column(name = "PHS_WARD")
    public String getPhsWard() {
        return phsWard;
    }

    public void setPhsWard(String phsWard) {
        this.phsWard = phsWard;
    }

    @Basic
    @Column(name = "EIS_SPEC_CODE")
    public String getEisSpecCode() {
        return eisSpecCode;
    }

    public void setEisSpecCode(String eisSpecCode) {
        this.eisSpecCode = eisSpecCode;
    }

    @Basic
    @Column(name = "DISP_HOSPCODE")
    public String getDispHospcode() {
        return dispHospcode;
    }

    public void setDispHospcode(String dispHospcode) {
        this.dispHospcode = dispHospcode;
    }

    @Basic
    @Column(name = "DISP_WORKSTORE")
    public String getDispWorkstore() {
        return dispWorkstore;
    }

    public void setDispWorkstore(String dispWorkstore) {
        this.dispWorkstore = dispWorkstore;
    }

    @Basic
    @Column(name = "TICKNUM")
    public String getTicknum() {
        return ticknum;
    }

    public void setTicknum(String ticknum) {
        this.ticknum = ticknum;
    }

    @Basic
    @Column(name = "TICKDATE")
    public Date getTickdate() {
        return tickdate;
    }

    public void setTickdate(Date tickdate) {
        this.tickdate = tickdate;
    }

    @Basic
    @Column(name = "WKSTATCODE")
    public String getWkstatcode() {
        return wkstatcode;
    }

    public void setWkstatcode(String wkstatcode) {
        this.wkstatcode = wkstatcode;
    }

    @Basic
    @Column(name = "LAST_UPD_DATE")
    public Date getLastUpdDate() {
        return lastUpdDate;
    }

    public void setLastUpdDate(Date lastUpdDate) {
        this.lastUpdDate = lastUpdDate;
    }

    @Basic
    @Column(name = "LAST_UPD_BY")
    public String getLastUpdBy() {
        return lastUpdBy;
    }

    public void setLastUpdBy(String lastUpdBy) {
        this.lastUpdBy = lastUpdBy;
    }

    @Basic
    @Column(name = "DISP_ORD_NO")
    public Long getDispOrdNo() {
        return dispOrdNo;
    }

    public void setDispOrdNo(Long dispOrdNo) {
        this.dispOrdNo = dispOrdNo;
    }

    @Basic
    @Column(name = "DISCHARGE_REASON")
    public String getDischargeReason() {
        return dischargeReason;
    }

    public void setDischargeReason(String dischargeReason) {
        this.dischargeReason = dischargeReason;
    }

    @Basic
    @Column(name = "REMARK_CREATE_BY")
    public String getRemarkCreateBy() {
        return remarkCreateBy;
    }

    public void setRemarkCreateBy(String remarkCreateBy) {
        this.remarkCreateBy = remarkCreateBy;
    }

    @Basic
    @Column(name = "REMARK_CREATE_DATE")
    public Date getRemarkCreateDate() {
        return remarkCreateDate;
    }

    public void setRemarkCreateDate(Date remarkCreateDate) {
        this.remarkCreateDate = remarkCreateDate;
    }

    @Basic
    @Column(name = "REMARK_TEXT")
    public String getRemarkText() {
        return remarkText;
    }

    public void setRemarkText(String remarkText) {
        this.remarkText = remarkText;
    }

    @Basic
    @Column(name = "REMARK_CONFIRM_BY")
    public String getRemarkConfirmBy() {
        return remarkConfirmBy;
    }

    public void setRemarkConfirmBy(String remarkConfirmBy) {
        this.remarkConfirmBy = remarkConfirmBy;
    }

    @Basic
    @Column(name = "REMARK_CONFIRM_DATE")
    public Date getRemarkConfirmDate() {
        return remarkConfirmDate;
    }

    public void setRemarkConfirmDate(Date remarkConfirmDate) {
        this.remarkConfirmDate = remarkConfirmDate;
    }

    @Basic
    @Column(name = "REMARK_STATUS")
    public String getRemarkStatus() {
        return remarkStatus;
    }

    public void setRemarkStatus(String remarkStatus) {
        this.remarkStatus = remarkStatus;
    }

    @Basic
    @Column(name = "LAST_PRESC_TYPE")
    public String getLastPrescType() {
        return lastPrescType;
    }

    public void setLastPrescType(String lastPrescType) {
        this.lastPrescType = lastPrescType;
    }

    @Basic
    @Column(name = "UNCOLLECT")
    public String getUncollect() {
        return uncollect;
    }

    public void setUncollect(String uncollect) {
        this.uncollect = uncollect;
    }

    @Basic
    @Column(name = "ALLOW_POST_COMMENT")
    public String getAllowPostComment() {
        return allowPostComment;
    }

    public void setAllowPostComment(String allowPostComment) {
        this.allowPostComment = allowPostComment;
    }

    @Basic
    @Column(name = "PRINT_TYPE")
    public String getPrintType() {
        return printType;
    }

    public void setPrintType(String printType) {
        this.printType = printType;
    }

    @Basic
    @Column(name = "PREV_HOSPCODE")
    public String getPrevHospcode() {
        return prevHospcode;
    }

    public void setPrevHospcode(String prevHospcode) {
        this.prevHospcode = prevHospcode;
    }

    @Basic
    @Column(name = "PREV_WORKSTORE")
    public String getPrevWorkstore() {
        return prevWorkstore;
    }

    public void setPrevWorkstore(String prevWorkstore) {
        this.prevWorkstore = prevWorkstore;
    }

    @Basic
    @Column(name = "PREV_DISP_DATE")
    public Date getPrevDispDate() {
        return prevDispDate;
    }

    public void setPrevDispDate(Date prevDispDate) {
        this.prevDispDate = prevDispDate;
    }

    @Basic
    @Column(name = "PREV_TICKNUM")
    public String getPrevTicknum() {
        return prevTicknum;
    }

    public void setPrevTicknum(String prevTicknum) {
        this.prevTicknum = prevTicknum;
    }

    @Version
    @Column(name = "VERSION")
    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "moeOrder",
            cascade= CascadeType.ALL, orphanRemoval=true)
    @OrderBy("cmsItemNo asc")
    public Set<MoeMedProfilePo> getMoeMedProfiles() {
        return this.moeMedProfiles;
    }

    public void setMoeMedProfiles(Set<MoeMedProfilePo> moeMedProfiles) {
        this.moeMedProfiles = moeMedProfiles;
    }

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "moeOrder")
    public MoeEhrOrderPo getMoeEhrOrder() {
        return this.moeEhrOrder;
    }

    public void setMoeEhrOrder(MoeEhrOrderPo moeEhrOrder) {
        this.moeEhrOrder = moeEhrOrder;
    }

    @Transient
    public String getBackDate() {
        return backDate;
    }

    public void setBackDate(String backDate) {
        this.backDate = backDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeOrderPo that = (MoeOrderPo) o;

        if (ordNo != that.ordNo) return false;
        if (version != that.version) return false;
        if (hospcode != null ? !hospcode.equals(that.hospcode) : that.hospcode != null) return false;
        if (workstore != null ? !workstore.equals(that.workstore) : that.workstore != null) return false;
        if (patHospcode != null ? !patHospcode.equals(that.patHospcode) : that.patHospcode != null) return false;
        if (ordDate != null ? !ordDate.equals(that.ordDate) : that.ordDate != null) return false;
        if (ordType != null ? !ordType.equals(that.ordType) : that.ordType != null) return false;
        if (ordSubtype != null ? !ordSubtype.equals(that.ordSubtype) : that.ordSubtype != null) return false;
        if (ordStatus != null ? !ordStatus.equals(that.ordStatus) : that.ordStatus != null) return false;
        if (refNo != null ? !refNo.equals(that.refNo) : that.refNo != null) return false;
        if (moCode != null ? !moCode.equals(that.moCode) : that.moCode != null) return false;
        if (prevOrdNo != null ? !prevOrdNo.equals(that.prevOrdNo) : that.prevOrdNo != null) return false;
        if (maxItemNo != null ? !maxItemNo.equals(that.maxItemNo) : that.maxItemNo != null) return false;
        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
        if (suspend != null ? !suspend.equals(that.suspend) : that.suspend != null) return false;
        if (privatePatient != null ? !privatePatient.equals(that.privatePatient) : that.privatePatient != null)
            return false;
        if (transferPatient != null ? !transferPatient.equals(that.transferPatient) : that.transferPatient != null)
            return false;
        if (allowModify != null ? !allowModify.equals(that.allowModify) : that.allowModify != null) return false;
        if (prescType != null ? !prescType.equals(that.prescType) : that.prescType != null) return false;
        if (moeOrder != null ? !moeOrder.equals(that.moeOrder) : that.moeOrder != null) return false;
        if (caseNo != null ? !caseNo.equals(that.caseNo) : that.caseNo != null) return false;
        if (srcSpecialty != null ? !srcSpecialty.equals(that.srcSpecialty) : that.srcSpecialty != null) return false;
        if (srcSubspecialty != null ? !srcSubspecialty.equals(that.srcSubspecialty) : that.srcSubspecialty != null)
            return false;
        if (ipasWard != null ? !ipasWard.equals(that.ipasWard) : that.ipasWard != null) return false;
        if (bedNo != null ? !bedNo.equals(that.bedNo) : that.bedNo != null) return false;
        if (specialty != null ? !specialty.equals(that.specialty) : that.specialty != null) return false;
        if (subspecialty != null ? !subspecialty.equals(that.subspecialty) : that.subspecialty != null) return false;
        if (phsWard != null ? !phsWard.equals(that.phsWard) : that.phsWard != null) return false;
        if (eisSpecCode != null ? !eisSpecCode.equals(that.eisSpecCode) : that.eisSpecCode != null) return false;
        if (dispHospcode != null ? !dispHospcode.equals(that.dispHospcode) : that.dispHospcode != null) return false;
        if (dispWorkstore != null ? !dispWorkstore.equals(that.dispWorkstore) : that.dispWorkstore != null)
            return false;
        if (ticknum != null ? !ticknum.equals(that.ticknum) : that.ticknum != null) return false;
        if (tickdate != null ? !tickdate.equals(that.tickdate) : that.tickdate != null) return false;
        if (wkstatcode != null ? !wkstatcode.equals(that.wkstatcode) : that.wkstatcode != null) return false;
        if (lastUpdDate != null ? !lastUpdDate.equals(that.lastUpdDate) : that.lastUpdDate != null) return false;
        if (lastUpdBy != null ? !lastUpdBy.equals(that.lastUpdBy) : that.lastUpdBy != null) return false;
        if (dispOrdNo != null ? !dispOrdNo.equals(that.dispOrdNo) : that.dispOrdNo != null) return false;
        if (dischargeReason != null ? !dischargeReason.equals(that.dischargeReason) : that.dischargeReason != null)
            return false;
        if (remarkCreateBy != null ? !remarkCreateBy.equals(that.remarkCreateBy) : that.remarkCreateBy != null)
            return false;
        if (remarkCreateDate != null ? !remarkCreateDate.equals(that.remarkCreateDate) : that.remarkCreateDate != null)
            return false;
        if (remarkText != null ? !remarkText.equals(that.remarkText) : that.remarkText != null) return false;
        if (remarkConfirmBy != null ? !remarkConfirmBy.equals(that.remarkConfirmBy) : that.remarkConfirmBy != null)
            return false;
        if (remarkConfirmDate != null ? !remarkConfirmDate.equals(that.remarkConfirmDate) : that.remarkConfirmDate != null)
            return false;
        if (remarkStatus != null ? !remarkStatus.equals(that.remarkStatus) : that.remarkStatus != null) return false;
        if (lastPrescType != null ? !lastPrescType.equals(that.lastPrescType) : that.lastPrescType != null)
            return false;
        if (uncollect != null ? !uncollect.equals(that.uncollect) : that.uncollect != null) return false;
        if (allowPostComment != null ? !allowPostComment.equals(that.allowPostComment) : that.allowPostComment != null)
            return false;
        if (printType != null ? !printType.equals(that.printType) : that.printType != null) return false;
        if (prevHospcode != null ? !prevHospcode.equals(that.prevHospcode) : that.prevHospcode != null) return false;
        if (prevWorkstore != null ? !prevWorkstore.equals(that.prevWorkstore) : that.prevWorkstore != null)
            return false;
        if (prevDispDate != null ? !prevDispDate.equals(that.prevDispDate) : that.prevDispDate != null) return false;
        if (prevTicknum != null ? !prevTicknum.equals(that.prevTicknum) : that.prevTicknum != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (ordNo ^ (ordNo >>> 32));
        result = 31 * result + (hospcode != null ? hospcode.hashCode() : 0);
        result = 31 * result + (workstore != null ? workstore.hashCode() : 0);
        result = 31 * result + (patHospcode != null ? patHospcode.hashCode() : 0);
        result = 31 * result + (ordDate != null ? ordDate.hashCode() : 0);
        result = 31 * result + (ordType != null ? ordType.hashCode() : 0);
        result = 31 * result + (ordSubtype != null ? ordSubtype.hashCode() : 0);
        result = 31 * result + (ordStatus != null ? ordStatus.hashCode() : 0);
        result = 31 * result + (refNo != null ? refNo.hashCode() : 0);
        result = 31 * result + (moCode != null ? moCode.hashCode() : 0);
        result = 31 * result + (prevOrdNo != null ? prevOrdNo.hashCode() : 0);
        result = 31 * result + (maxItemNo != null ? maxItemNo.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (suspend != null ? suspend.hashCode() : 0);
        result = 31 * result + (privatePatient != null ? privatePatient.hashCode() : 0);
        result = 31 * result + (transferPatient != null ? transferPatient.hashCode() : 0);
        result = 31 * result + (allowModify != null ? allowModify.hashCode() : 0);
        result = 31 * result + (prescType != null ? prescType.hashCode() : 0);
        result = 31 * result + (moeOrder != null ? moeOrder.hashCode() : 0);
        result = 31 * result + (caseNo != null ? caseNo.hashCode() : 0);
        result = 31 * result + (srcSpecialty != null ? srcSpecialty.hashCode() : 0);
        result = 31 * result + (srcSubspecialty != null ? srcSubspecialty.hashCode() : 0);
        result = 31 * result + (ipasWard != null ? ipasWard.hashCode() : 0);
        result = 31 * result + (bedNo != null ? bedNo.hashCode() : 0);
        result = 31 * result + (specialty != null ? specialty.hashCode() : 0);
        result = 31 * result + (subspecialty != null ? subspecialty.hashCode() : 0);
        result = 31 * result + (phsWard != null ? phsWard.hashCode() : 0);
        result = 31 * result + (eisSpecCode != null ? eisSpecCode.hashCode() : 0);
        result = 31 * result + (dispHospcode != null ? dispHospcode.hashCode() : 0);
        result = 31 * result + (dispWorkstore != null ? dispWorkstore.hashCode() : 0);
        result = 31 * result + (ticknum != null ? ticknum.hashCode() : 0);
        result = 31 * result + (tickdate != null ? tickdate.hashCode() : 0);
        result = 31 * result + (wkstatcode != null ? wkstatcode.hashCode() : 0);
        result = 31 * result + (lastUpdDate != null ? lastUpdDate.hashCode() : 0);
        result = 31 * result + (lastUpdBy != null ? lastUpdBy.hashCode() : 0);
        result = 31 * result + (dispOrdNo != null ? dispOrdNo.hashCode() : 0);
        result = 31 * result + (dischargeReason != null ? dischargeReason.hashCode() : 0);
        result = 31 * result + (remarkCreateBy != null ? remarkCreateBy.hashCode() : 0);
        result = 31 * result + (remarkCreateDate != null ? remarkCreateDate.hashCode() : 0);
        result = 31 * result + (remarkText != null ? remarkText.hashCode() : 0);
        result = 31 * result + (remarkConfirmBy != null ? remarkConfirmBy.hashCode() : 0);
        result = 31 * result + (remarkConfirmDate != null ? remarkConfirmDate.hashCode() : 0);
        result = 31 * result + (remarkStatus != null ? remarkStatus.hashCode() : 0);
        result = 31 * result + (lastPrescType != null ? lastPrescType.hashCode() : 0);
        result = 31 * result + (uncollect != null ? uncollect.hashCode() : 0);
        result = 31 * result + (allowPostComment != null ? allowPostComment.hashCode() : 0);
        result = 31 * result + (printType != null ? printType.hashCode() : 0);
        result = 31 * result + (prevHospcode != null ? prevHospcode.hashCode() : 0);
        result = 31 * result + (prevWorkstore != null ? prevWorkstore.hashCode() : 0);
        result = 31 * result + (prevDispDate != null ? prevDispDate.hashCode() : 0);
        result = 31 * result + (prevTicknum != null ? prevTicknum.hashCode() : 0);
        result = 31 * result + (int) (version ^ (version >>> 32));
        return result;
    }
}
