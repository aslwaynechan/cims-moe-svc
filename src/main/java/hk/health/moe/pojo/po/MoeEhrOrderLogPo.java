package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_EHR_ORDER_LOG")
// Chris 20190808  -Start
//@IdClass(MoeEhrOrderLogPoPK.class)
@IdClass(MoeOrderLogPoPK.class)
// Chris 20190808  -End
public class MoeEhrOrderLogPo implements Serializable {
    private static final long serialVersionUID = 4024936251436229978L;
    private String hospcode;
    private long ordNo;
    //private String moePatientKey;
    //private String moeCaseNo;
    private String createUserId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private String refNo;
    private String remarkUserId;
    private String remarkUser;
    private String remarkHosp;
    private String remarkRank;
    private String remarkRankDesc;
    private MoeOrderLogPo moeOrderLog;
    private MoePatientPo moePatient;
    private MoePatientCasePo moePatientCase;

    @Basic
    @Column(name = "HOSPCODE")
    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    // Chris 20190808  Uncomment  -Start
    @Id
    @Column(name = "ORD_NO")
    public long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(long ordNo) {
        this.ordNo = ordNo;
    }
    // Chris 20190808 Uncomment  -End

/*    @Basic
    @Column(name = "MOE_PATIENT_KEY")
    public String getMoePatientKey() {
        return moePatientKey;
    }

    public void setMoePatientKey(String moePatientKey) {
        this.moePatientKey = moePatientKey;
    }*/

/*    @Basic
    @Column(name = "MOE_CASE_NO")
    public String getMoeCaseNo() {
        return moeCaseNo;
    }

    public void setMoeCaseNo(String moeCaseNo) {
        this.moeCaseNo = moeCaseNo;
    }*/

    @Basic
    @Column(name = "CREATE_USER_ID")
    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    @Basic
    @Column(name = "CREATE_USER")
    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    @Basic
    @Column(name = "CREATE_HOSP")
    public String getCreateHosp() {
        return createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    @Basic
    @Column(name = "CREATE_RANK")
    public String getCreateRank() {
        return createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    @Basic
    @Column(name = "CREATE_RANK_DESC")
    public String getCreateRankDesc() {
        return createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    @Basic
    @Column(name = "UPDATE_USER_ID")
    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Basic
    @Column(name = "UPDATE_USER")
    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Basic
    @Column(name = "UPDATE_HOSP")
    public String getUpdateHosp() {
        return updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    @Basic
    @Column(name = "UPDATE_RANK")
    public String getUpdateRank() {
        return updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    @Basic
    @Column(name = "UPDATE_RANK_DESC")
    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    // Chris 20190808 Uncomment  -Start
    @Id
    @Column(name = "REF_NO")
    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }
    // Chris 20190808 Uncomment  -End

    @Basic
    @Column(name = "REMARK_USER_ID")
    public String getRemarkUserId() {
        return remarkUserId;
    }

    public void setRemarkUserId(String remarkUserId) {
        this.remarkUserId = remarkUserId;
    }

    @Basic
    @Column(name = "REMARK_USER")
    public String getRemarkUser() {
        return remarkUser;
    }

    public void setRemarkUser(String remarkUser) {
        this.remarkUser = remarkUser;
    }

    @Basic
    @Column(name = "REMARK_HOSP")
    public String getRemarkHosp() {
        return remarkHosp;
    }

    public void setRemarkHosp(String remarkHosp) {
        this.remarkHosp = remarkHosp;
    }

    @Basic
    @Column(name = "REMARK_RANK")
    public String getRemarkRank() {
        return remarkRank;
    }

    public void setRemarkRank(String remarkRank) {
        this.remarkRank = remarkRank;
    }

    @Basic
    @Column(name = "REMARK_RANK_DESC")
    public String getRemarkRankDesc() {
        return remarkRankDesc;
    }

    public void setRemarkRankDesc(String remarkRankDesc) {
        this.remarkRankDesc = remarkRankDesc;
    }

    @Id
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "ORD_NO", referencedColumnName = "ORD_NO", insertable = false, updatable = false),
            @JoinColumn(name = "REF_NO", referencedColumnName = "REF_NO", insertable = false, updatable = false)
    })
    public MoeOrderLogPo getMoeOrderLog() {
        return this.moeOrderLog;
    }

    public void setMoeOrderLog(MoeOrderLogPo moeOrderLog) {
        this.moeOrderLog = moeOrderLog;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "moe_patient_key", referencedColumnName = "moe_patient_key", nullable = false)
    public MoePatientPo getMoePatient() {
        return this.moePatient;
    }

    public void setMoePatient(MoePatientPo moePatient) {
        this.moePatient = moePatient;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "moe_case_no", referencedColumnName = "moe_case_no", nullable = false)
    public MoePatientCasePo getMoePatientCase() {
        return this.moePatientCase;
    }

    public void setMoePatientCase(MoePatientCasePo moePatientCase) {
        this.moePatientCase = moePatientCase;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeEhrOrderLogPo that = (MoeEhrOrderLogPo) o;

        if (ordNo != that.ordNo) return false;
        if (hospcode != null ? !hospcode.equals(that.hospcode) : that.hospcode != null) return false;
        //if (moePatientKey != null ? !moePatientKey.equals(that.moePatientKey) : that.moePatientKey != null)
        //return false;
        //if (moeCaseNo != null ? !moeCaseNo.equals(that.moeCaseNo) : that.moeCaseNo != null) return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null) return false;
        if (createUser != null ? !createUser.equals(that.createUser) : that.createUser != null) return false;
        if (createHosp != null ? !createHosp.equals(that.createHosp) : that.createHosp != null) return false;
        if (createRank != null ? !createRank.equals(that.createRank) : that.createRank != null) return false;
        if (createRankDesc != null ? !createRankDesc.equals(that.createRankDesc) : that.createRankDesc != null)
            return false;
        if (updateUserId != null ? !updateUserId.equals(that.updateUserId) : that.updateUserId != null) return false;
        if (updateUser != null ? !updateUser.equals(that.updateUser) : that.updateUser != null) return false;
        if (updateHosp != null ? !updateHosp.equals(that.updateHosp) : that.updateHosp != null) return false;
        if (updateRank != null ? !updateRank.equals(that.updateRank) : that.updateRank != null) return false;
        if (updateRankDesc != null ? !updateRankDesc.equals(that.updateRankDesc) : that.updateRankDesc != null)
            return false;
        if (refNo != null ? !refNo.equals(that.refNo) : that.refNo != null) return false;
        if (remarkUserId != null ? !remarkUserId.equals(that.remarkUserId) : that.remarkUserId != null) return false;
        if (remarkUser != null ? !remarkUser.equals(that.remarkUser) : that.remarkUser != null) return false;
        if (remarkHosp != null ? !remarkHosp.equals(that.remarkHosp) : that.remarkHosp != null) return false;
        if (remarkRank != null ? !remarkRank.equals(that.remarkRank) : that.remarkRank != null) return false;
        if (remarkRankDesc != null ? !remarkRankDesc.equals(that.remarkRankDesc) : that.remarkRankDesc != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hospcode != null ? hospcode.hashCode() : 0;
        result = 31 * result + (int) (ordNo ^ (ordNo >>> 32));
        //result = 31 * result + (moePatientKey != null ? moePatientKey.hashCode() : 0);
        //result = 31 * result + (moeCaseNo != null ? moeCaseNo.hashCode() : 0);
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (createUser != null ? createUser.hashCode() : 0);
        result = 31 * result + (createHosp != null ? createHosp.hashCode() : 0);
        result = 31 * result + (createRank != null ? createRank.hashCode() : 0);
        result = 31 * result + (createRankDesc != null ? createRankDesc.hashCode() : 0);
        result = 31 * result + (updateUserId != null ? updateUserId.hashCode() : 0);
        result = 31 * result + (updateUser != null ? updateUser.hashCode() : 0);
        result = 31 * result + (updateHosp != null ? updateHosp.hashCode() : 0);
        result = 31 * result + (updateRank != null ? updateRank.hashCode() : 0);
        result = 31 * result + (updateRankDesc != null ? updateRankDesc.hashCode() : 0);
        result = 31 * result + (refNo != null ? refNo.hashCode() : 0);
        result = 31 * result + (remarkUserId != null ? remarkUserId.hashCode() : 0);
        result = 31 * result + (remarkUser != null ? remarkUser.hashCode() : 0);
        result = 31 * result + (remarkHosp != null ? remarkHosp.hashCode() : 0);
        result = 31 * result + (remarkRank != null ? remarkRank.hashCode() : 0);
        result = 31 * result + (remarkRankDesc != null ? remarkRankDesc.hashCode() : 0);
        return result;
    }
}
