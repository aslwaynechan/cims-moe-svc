package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_CONJUNCTION_TERM_ID_MAP")
@IdClass(MoeConjunctionTermIdMapPoPK.class)
public class MoeConjunctionTermIdMapPo implements Serializable {
    private static final long serialVersionUID = 2050344997078811705L;
    private String orderLineType;
    private String ehrConjunctionDesc;
    private long ehrTermId;
    private String defaultMap;
    private String ehrStatus;

    @Basic
    @Column(name = "ORDER_LINE_TYPE")
    public String getOrderLineType() {
        return orderLineType;
    }

    public void setOrderLineType(String orderLineType) {
        this.orderLineType = orderLineType;
    }

    @Id
    @Column(name = "EHR_CONJUNCTION_DESC")
    public String getEhrConjunctionDesc() {
        return ehrConjunctionDesc;
    }

    public void setEhrConjunctionDesc(String ehrConjunctionDesc) {
        this.ehrConjunctionDesc = ehrConjunctionDesc;
    }

    @Id
    @Column(name = "EHR_TERM_ID")
    public long getEhrTermId() {
        return ehrTermId;
    }

    public void setEhrTermId(long ehrTermId) {
        this.ehrTermId = ehrTermId;
    }

    @Basic
    @Column(name = "DEFAULT_MAP")
    public String getDefaultMap() {
        return defaultMap;
    }

    public void setDefaultMap(String defaultMap) {
        this.defaultMap = defaultMap;
    }

    @Basic
    @Column(name = "EHR_STATUS")
    public String getEhrStatus() {
        return ehrStatus;
    }

    public void setEhrStatus(String ehrStatus) {
        this.ehrStatus = ehrStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeConjunctionTermIdMapPo that = (MoeConjunctionTermIdMapPo) o;

        if (ehrTermId != that.ehrTermId) return false;
        if (orderLineType != null ? !orderLineType.equals(that.orderLineType) : that.orderLineType != null)
            return false;
        if (ehrConjunctionDesc != null ? !ehrConjunctionDesc.equals(that.ehrConjunctionDesc) : that.ehrConjunctionDesc != null)
            return false;
        if (defaultMap != null ? !defaultMap.equals(that.defaultMap) : that.defaultMap != null) return false;
        if (ehrStatus != null ? !ehrStatus.equals(that.ehrStatus) : that.ehrStatus != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = orderLineType != null ? orderLineType.hashCode() : 0;
        result = 31 * result + (ehrConjunctionDesc != null ? ehrConjunctionDesc.hashCode() : 0);
        result = 31 * result + (int) (ehrTermId ^ (ehrTermId >>> 32));
        result = 31 * result + (defaultMap != null ? defaultMap.hashCode() : 0);
        result = 31 * result + (ehrStatus != null ? ehrStatus.hashCode() : 0);
        return result;
    }
}
