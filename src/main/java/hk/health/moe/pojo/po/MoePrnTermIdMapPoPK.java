package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoePrnTermIdMapPoPK implements Serializable {
    private static final long serialVersionUID = 1254698082173753371L;
    private String prn;
    private long ehrTermId;

    @Column(name = "PRN")
    @Id
    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    @Column(name = "EHR_TERM_ID")
    @Id
    public long getEhrTermId() {
        return ehrTermId;
    }

    public void setEhrTermId(long ehrTermId) {
        this.ehrTermId = ehrTermId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoePrnTermIdMapPoPK that = (MoePrnTermIdMapPoPK) o;

        if (ehrTermId != that.ehrTermId) return false;
        if (prn != null ? !prn.equals(that.prn) : that.prn != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = prn != null ? prn.hashCode() : 0;
        result = 31 * result + (int) (ehrTermId ^ (ehrTermId >>> 32));
        return result;
    }
}
