package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MOE_APP")
public class MoeAppPo  implements Serializable {
    private static final long serialVersionUID = -6428728888507232179L;
    private String id;
    private String appName;
    private String password;
    private Set<MoeOperationPo> moeOperations = new HashSet<>();

    @Id
    @Column(name = "ID")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "APP_NAME")
    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    @Basic
    @Column(name = "PASSWORD")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "MOE_APP_OPERATION",
            joinColumns = { @JoinColumn(name = "APP_ID", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "OPERATION_ID", nullable = false, updatable = false) })
    public Set<MoeOperationPo> getMoeOperations() {
        return this.moeOperations;
    }

    public void setMoeOperations(Set<MoeOperationPo> moeOperations) {
        this.moeOperations = moeOperations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeAppPo moeAppPo = (MoeAppPo) o;

        if (id != null ? !id.equals(moeAppPo.id) : moeAppPo.id != null) return false;
        if (appName != null ? !appName.equals(moeAppPo.appName) : moeAppPo.appName != null) return false;
        if (password != null ? !password.equals(moeAppPo.password) : moeAppPo.password != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (appName != null ? appName.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
