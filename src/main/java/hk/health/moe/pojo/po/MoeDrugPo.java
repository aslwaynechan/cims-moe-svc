package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MOE_DRUG")
@IdClass(MoeDrugPoPK.class)
public class MoeDrugPo implements Serializable {
    private static final long serialVersionUID = 1143469718081773548L;
    private String hkRegNo;
    private String tradeName;
    private String vtm;
    //private Long formId;
    private String formEng;
    private Long doseFormExtraInfoId;
    private String doseFormExtraInfo;
    //private Long routeId;
    private String routeEng;
    private Long vtmId;
    private Long tradeVtmId;
    private Long vtmRouteId;
    private Long tradeVtmRouteId;
    private Long vtmRouteFormId;
    private Long tradeVtmRouteFormId;
    //private Long baseUnitId;
    private String baseUnit;
    //private Long prescribeUnitId;
    private String prescribeUnit;
    //private Long dispenseUnitId;
    private String dispenseUnit;
    private Long legalClassId;
    private String legalClass;
    private String manufacturer;
    private String genericIndicator;
    private String allergyCheckFlag;
    private String strengthCompulsory;
    private long rank;
    private MoeBaseUnitPo moeBaseUnitByPrescribeUnitId;
    private MoeBaseUnitPo moeBaseUnitByBaseUnitId;
    private MoeBaseUnitPo moeBaseUnitByDispenseUnitId;
    private MoeFormPo moeForm;
    private MoeRoutePo moeRoute;
    private MoeDoseFormExtraInfoPo moeDoseFormExtraInfo;
    private Set<MoeCommonDosagePo> moeCommonDosages = new HashSet<>();
    private Set<MoeDrugAliasNamePo> moeDrugAliasNames = new HashSet<>();
    private MoeDrugStrengthPo moeDrugStrength;

    @Id
    @Column(name = "HK_REG_NO")
    public String getHkRegNo() {
        return hkRegNo;
    }

    public void setHkRegNo(String hkRegNo) {
        this.hkRegNo = hkRegNo;
    }

    @Basic
    @Column(name = "TRADE_NAME")
    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    @Basic
    @Column(name = "VTM")
    public String getVtm() {
        return vtm;
    }

    public void setVtm(String vtm) {
        this.vtm = vtm;
    }

/*    @Basic
    @Column(name = "FORM_ID")
    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }*/

    @Basic
    @Column(name = "FORM_ENG")
    public String getFormEng() {
        return formEng;
    }

    public void setFormEng(String formEng) {
        this.formEng = formEng;
    }

    @Basic
    @Column(name = "DOSE_FORM_EXTRA_INFO_ID")
    public Long getDoseFormExtraInfoId() {
        return doseFormExtraInfoId;
    }

    public void setDoseFormExtraInfoId(Long doseFormExtraInfoId) {
        this.doseFormExtraInfoId = doseFormExtraInfoId;
    }

    @Basic
    @Column(name = "DOSE_FORM_EXTRA_INFO")
    public String getDoseFormExtraInfo() {
        return doseFormExtraInfo;
    }

    public void setDoseFormExtraInfo(String doseFormExtraInfo) {
        this.doseFormExtraInfo = doseFormExtraInfo;
    }

/*    @Basic
    @Column(name = "ROUTE_ID")
    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }*/

    @Basic
    @Column(name = "ROUTE_ENG")
    public String getRouteEng() {
        return routeEng;
    }

    public void setRouteEng(String routeEng) {
        this.routeEng = routeEng;
    }

    @Basic
    @Column(name = "VTM_ID")
    public Long getVtmId() {
        return vtmId;
    }

    public void setVtmId(Long vtmId) {
        this.vtmId = vtmId;
    }

    @Basic
    @Column(name = "TRADE_VTM_ID")
    public Long getTradeVtmId() {
        return tradeVtmId;
    }

    public void setTradeVtmId(Long tradeVtmId) {
        this.tradeVtmId = tradeVtmId;
    }

    @Basic
    @Column(name = "VTM_ROUTE_ID")
    public Long getVtmRouteId() {
        return vtmRouteId;
    }

    public void setVtmRouteId(Long vtmRouteId) {
        this.vtmRouteId = vtmRouteId;
    }

    @Basic
    @Column(name = "TRADE_VTM_ROUTE_ID")
    public Long getTradeVtmRouteId() {
        return tradeVtmRouteId;
    }

    public void setTradeVtmRouteId(Long tradeVtmRouteId) {
        this.tradeVtmRouteId = tradeVtmRouteId;
    }

    @Basic
    @Column(name = "VTM_ROUTE_FORM_ID")
    public Long getVtmRouteFormId() {
        return vtmRouteFormId;
    }

    public void setVtmRouteFormId(Long vtmRouteFormId) {
        this.vtmRouteFormId = vtmRouteFormId;
    }

    @Basic
    @Column(name = "TRADE_VTM_ROUTE_FORM_ID")
    public Long getTradeVtmRouteFormId() {
        return tradeVtmRouteFormId;
    }

    public void setTradeVtmRouteFormId(Long tradeVtmRouteFormId) {
        this.tradeVtmRouteFormId = tradeVtmRouteFormId;
    }

/*    @Basic
    @Column(name = "BASE_UNIT_ID")
    public Long getBaseUnitId() {
        return baseUnitId;
    }

    public void setBaseUnitId(Long baseUnitId) {
        this.baseUnitId = baseUnitId;
    }*/

    @Basic
    @Column(name = "BASE_UNIT")
    public String getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(String baseUnit) {
        this.baseUnit = baseUnit;
    }

/*    @Basic
    @Column(name = "PRESCRIBE_UNIT_ID")
    public Long getPrescribeUnitId() {
        return prescribeUnitId;
    }

    public void setPrescribeUnitId(Long prescribeUnitId) {
        this.prescribeUnitId = prescribeUnitId;
    }*/

    @Basic
    @Column(name = "PRESCRIBE_UNIT")
    public String getPrescribeUnit() {
        return prescribeUnit;
    }

    public void setPrescribeUnit(String prescribeUnit) {
        this.prescribeUnit = prescribeUnit;
    }

/*    @Basic
    @Column(name = "DISPENSE_UNIT_ID")
    public Long getDispenseUnitId() {
        return dispenseUnitId;
    }

    public void setDispenseUnitId(Long dispenseUnitId) {
        this.dispenseUnitId = dispenseUnitId;
    }*/

    @Basic
    @Column(name = "DISPENSE_UNIT")
    public String getDispenseUnit() {
        return dispenseUnit;
    }

    public void setDispenseUnit(String dispenseUnit) {
        this.dispenseUnit = dispenseUnit;
    }

    @Basic
    @Column(name = "LEGAL_CLASS_ID")
    public Long getLegalClassId() {
        return legalClassId;
    }

    public void setLegalClassId(Long legalClassId) {
        this.legalClassId = legalClassId;
    }

    @Basic
    @Column(name = "LEGAL_CLASS")
    public String getLegalClass() {
        return legalClass;
    }

    public void setLegalClass(String legalClass) {
        this.legalClass = legalClass;
    }

    @Basic
    @Column(name = "MANUFACTURER")
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Basic
    @Column(name = "GENERIC_INDICATOR")
    public String getGenericIndicator() {
        return genericIndicator;
    }

    public void setGenericIndicator(String genericIndicator) {
        this.genericIndicator = genericIndicator;
    }

    @Basic
    @Column(name = "ALLERGY_CHECK_FLAG")
    public String getAllergyCheckFlag() {
        return allergyCheckFlag;
    }

    public void setAllergyCheckFlag(String allergyCheckFlag) {
        this.allergyCheckFlag = allergyCheckFlag;
    }

    @Basic
    @Column(name = "STRENGTH_COMPULSORY")
    public String getStrengthCompulsory() {
        return strengthCompulsory;
    }

    public void setStrengthCompulsory(String strengthCompulsory) {
        this.strengthCompulsory = strengthCompulsory;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "prescribe_unit_id")
    public MoeBaseUnitPo getMoeBaseUnitByPrescribeUnitId() {
        return this.moeBaseUnitByPrescribeUnitId;
    }

    public void setMoeBaseUnitByPrescribeUnitId(
            MoeBaseUnitPo moeBaseUnitByPrescribeUnitId) {
        this.moeBaseUnitByPrescribeUnitId = moeBaseUnitByPrescribeUnitId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dose_form_extra_info_id", insertable = false, updatable = false)
    public MoeDoseFormExtraInfoPo getMoeDoseFormExtraInfo() {
        return this.moeDoseFormExtraInfo;
    }

    public void setMoeDoseFormExtraInfo(MoeDoseFormExtraInfoPo moeDoseFormExtraInfo) {
        this.moeDoseFormExtraInfo = moeDoseFormExtraInfo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "base_unit_id")
    public MoeBaseUnitPo getMoeBaseUnitByBaseUnitId() {
        return this.moeBaseUnitByBaseUnitId;
    }

    public void setMoeBaseUnitByBaseUnitId(MoeBaseUnitPo moeBaseUnitByBaseUnitId) {
        this.moeBaseUnitByBaseUnitId = moeBaseUnitByBaseUnitId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dispense_unit_id")
    public MoeBaseUnitPo getMoeBaseUnitByDispenseUnitId() {
        return this.moeBaseUnitByDispenseUnitId;
    }

    public void setMoeBaseUnitByDispenseUnitId(MoeBaseUnitPo moeBaseUnitByDispenseUnitId) {
        this.moeBaseUnitByDispenseUnitId = moeBaseUnitByDispenseUnitId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "route_id")
    public MoeRoutePo getMoeRoute() {
        return this.moeRoute;
    }

    public void setMoeRoute(MoeRoutePo moeRoute) {
        this.moeRoute = moeRoute;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "form_id")
    public MoeFormPo getMoeForm() {
        return this.moeForm;
    }

    public void setMoeForm(MoeFormPo moeForm) {
        this.moeForm = moeForm;
    }

    //@OneToMany(fetch = FetchType.LAZY, mappedBy = "moeDrug")
/*    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "hk_reg_no", referencedColumnName = "hk_reg_no", insertable = false, updatable = false)*/
    @Transient
    public Set<MoeDrugAliasNamePo> getMoeDrugAliasNames() {
        return this.moeDrugAliasNames;
    }

    public void setMoeDrugAliasNames(Set<MoeDrugAliasNamePo> moeDrugAliasNames) {
        this.moeDrugAliasNames = moeDrugAliasNames;
    }

    //@OneToMany(fetch = FetchType.LAZY, mappedBy = "moeDrug")
    /*@OneToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "hk_reg_no", referencedColumnName = "hk_reg_no", insertable = false, updatable = false),
            @JoinColumn(name = "rank", referencedColumnName = "rank", insertable = false, updatable = false)
    })*/
    @Transient
    public MoeDrugStrengthPo getMoeDrugStrength() {
        return this.moeDrugStrength;
    }

    public void setMoeDrugStrength(MoeDrugStrengthPo moeDrugStrength) {
        this.moeDrugStrength = moeDrugStrength;
    }

/*    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "hk_reg_no", referencedColumnName = "hk_reg_no", insertable = false, updatable = false)*/
    @Transient
    public Set<MoeCommonDosagePo> getMoeCommonDosages() {
        return this.moeCommonDosages;
    }

    public void setMoeCommonDosages(Set<MoeCommonDosagePo> moeCommonDosages) {
        this.moeCommonDosages = moeCommonDosages;
    }


    @Id
    @Column(name = "RANK")
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDrugPo moeDrugPo = (MoeDrugPo) o;

        if (rank != moeDrugPo.rank) return false;
        if (hkRegNo != null ? !hkRegNo.equals(moeDrugPo.hkRegNo) : moeDrugPo.hkRegNo != null) return false;
        if (tradeName != null ? !tradeName.equals(moeDrugPo.tradeName) : moeDrugPo.tradeName != null) return false;
        if (vtm != null ? !vtm.equals(moeDrugPo.vtm) : moeDrugPo.vtm != null) return false;
        //if (formId != null ? !formId.equals(moeDrugPo.formId) : moeDrugPo.formId != null) return false;
        if (formEng != null ? !formEng.equals(moeDrugPo.formEng) : moeDrugPo.formEng != null) return false;
        if (doseFormExtraInfoId != null ? !doseFormExtraInfoId.equals(moeDrugPo.doseFormExtraInfoId) : moeDrugPo.doseFormExtraInfoId != null)
            //return false;
            if (doseFormExtraInfo != null ? !doseFormExtraInfo.equals(moeDrugPo.doseFormExtraInfo) : moeDrugPo.doseFormExtraInfo != null)
                return false;
        //if (routeId != null ? !routeId.equals(moeDrugPo.routeId) : moeDrugPo.routeId != null) return false;
        if (routeEng != null ? !routeEng.equals(moeDrugPo.routeEng) : moeDrugPo.routeEng != null) return false;
        if (vtmId != null ? !vtmId.equals(moeDrugPo.vtmId) : moeDrugPo.vtmId != null) return false;
        if (tradeVtmId != null ? !tradeVtmId.equals(moeDrugPo.tradeVtmId) : moeDrugPo.tradeVtmId != null) return false;
        if (vtmRouteId != null ? !vtmRouteId.equals(moeDrugPo.vtmRouteId) : moeDrugPo.vtmRouteId != null) return false;
        if (tradeVtmRouteId != null ? !tradeVtmRouteId.equals(moeDrugPo.tradeVtmRouteId) : moeDrugPo.tradeVtmRouteId != null)
            return false;
        if (vtmRouteFormId != null ? !vtmRouteFormId.equals(moeDrugPo.vtmRouteFormId) : moeDrugPo.vtmRouteFormId != null)
            return false;
        if (tradeVtmRouteFormId != null ? !tradeVtmRouteFormId.equals(moeDrugPo.tradeVtmRouteFormId) : moeDrugPo.tradeVtmRouteFormId != null)
            return false;
        //if (baseUnitId != null ? !baseUnitId.equals(moeDrugPo.baseUnitId) : moeDrugPo.baseUnitId != null) return false;
        if (baseUnit != null ? !baseUnit.equals(moeDrugPo.baseUnit) : moeDrugPo.baseUnit != null) return false;
        //if (prescribeUnitId != null ? !prescribeUnitId.equals(moeDrugPo.prescribeUnitId) : moeDrugPo.prescribeUnitId != null)
        //return false;
        if (prescribeUnit != null ? !prescribeUnit.equals(moeDrugPo.prescribeUnit) : moeDrugPo.prescribeUnit != null)
            return false;
        //if (dispenseUnitId != null ? !dispenseUnitId.equals(moeDrugPo.dispenseUnitId) : moeDrugPo.dispenseUnitId != null)
        //return false;
        if (dispenseUnit != null ? !dispenseUnit.equals(moeDrugPo.dispenseUnit) : moeDrugPo.dispenseUnit != null)
            return false;
        if (legalClassId != null ? !legalClassId.equals(moeDrugPo.legalClassId) : moeDrugPo.legalClassId != null)
            return false;
        if (legalClass != null ? !legalClass.equals(moeDrugPo.legalClass) : moeDrugPo.legalClass != null) return false;
        if (manufacturer != null ? !manufacturer.equals(moeDrugPo.manufacturer) : moeDrugPo.manufacturer != null)
            return false;
        if (genericIndicator != null ? !genericIndicator.equals(moeDrugPo.genericIndicator) : moeDrugPo.genericIndicator != null)
            return false;
        if (allergyCheckFlag != null ? !allergyCheckFlag.equals(moeDrugPo.allergyCheckFlag) : moeDrugPo.allergyCheckFlag != null)
            return false;
        if (strengthCompulsory != null ? !strengthCompulsory.equals(moeDrugPo.strengthCompulsory) : moeDrugPo.strengthCompulsory != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hkRegNo != null ? hkRegNo.hashCode() : 0;
        result = 31 * result + (tradeName != null ? tradeName.hashCode() : 0);
        result = 31 * result + (vtm != null ? vtm.hashCode() : 0);
        //result = 31 * result + (formId != null ? formId.hashCode() : 0);
        result = 31 * result + (formEng != null ? formEng.hashCode() : 0);
        result = 31 * result + (doseFormExtraInfoId != null ? doseFormExtraInfoId.hashCode() : 0);
        result = 31 * result + (doseFormExtraInfo != null ? doseFormExtraInfo.hashCode() : 0);
        //result = 31 * result + (routeId != null ? routeId.hashCode() : 0);
        result = 31 * result + (routeEng != null ? routeEng.hashCode() : 0);
        result = 31 * result + (vtmId != null ? vtmId.hashCode() : 0);
        result = 31 * result + (tradeVtmId != null ? tradeVtmId.hashCode() : 0);
        result = 31 * result + (vtmRouteId != null ? vtmRouteId.hashCode() : 0);
        result = 31 * result + (tradeVtmRouteId != null ? tradeVtmRouteId.hashCode() : 0);
        result = 31 * result + (vtmRouteFormId != null ? vtmRouteFormId.hashCode() : 0);
        result = 31 * result + (tradeVtmRouteFormId != null ? tradeVtmRouteFormId.hashCode() : 0);
        //result = 31 * result + (baseUnitId != null ? baseUnitId.hashCode() : 0);
        result = 31 * result + (baseUnit != null ? baseUnit.hashCode() : 0);
        //result = 31 * result + (prescribeUnitId != null ? prescribeUnitId.hashCode() : 0);
        result = 31 * result + (prescribeUnit != null ? prescribeUnit.hashCode() : 0);
        //result = 31 * result + (dispenseUnitId != null ? dispenseUnitId.hashCode() : 0);
        result = 31 * result + (dispenseUnit != null ? dispenseUnit.hashCode() : 0);
        result = 31 * result + (legalClassId != null ? legalClassId.hashCode() : 0);
        result = 31 * result + (legalClass != null ? legalClass.hashCode() : 0);
        result = 31 * result + (manufacturer != null ? manufacturer.hashCode() : 0);
        result = 31 * result + (genericIndicator != null ? genericIndicator.hashCode() : 0);
        result = 31 * result + (allergyCheckFlag != null ? allergyCheckFlag.hashCode() : 0);
        result = 31 * result + (strengthCompulsory != null ? strengthCompulsory.hashCode() : 0);
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        return result;
    }
}
