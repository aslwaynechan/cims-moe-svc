package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "MOE_USER_SETTING_LOG")
@IdClass(MoeUserSettingLogPoPK.class)
public class MoeUserSettingLogPo implements Serializable {
    private static final long serialVersionUID = 4826823159275497120L;
    private String loginId;
    private String loginName;
    private String paramName;
    private String paramNameDesc;
    private String paramValue;
    private String paramValueType;
    private String paramPossibleValue;
    private String paramLevel;
    private String paramDisplay;
    private String createBy;
    private Date createDtm;
    private String updateBy;
    private Date updateDtm;
    private String actionType;
    private Date actionDate;

    @Id
    @Column(name = "LOGIN_ID")
    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    @Basic
    @Column(name = "LOGIN_NAME")
    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    @Id
    @Column(name = "PARAM_NAME")
    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    @Basic
    @Column(name = "PARAM_NAME_DESC")
    public String getParamNameDesc() {
        return paramNameDesc;
    }

    public void setParamNameDesc(String paramNameDesc) {
        this.paramNameDesc = paramNameDesc;
    }

    @Basic
    @Column(name = "PARAM_VALUE")
    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    @Basic
    @Column(name = "PARAM_VALUE_TYPE")
    public String getParamValueType() {
        return paramValueType;
    }

    public void setParamValueType(String paramValueType) {
        this.paramValueType = paramValueType;
    }

    @Basic
    @Column(name = "PARAM_POSSIBLE_VALUE")
    public String getParamPossibleValue() {
        return paramPossibleValue;
    }

    public void setParamPossibleValue(String paramPossibleValue) {
        this.paramPossibleValue = paramPossibleValue;
    }

    @Basic
    @Column(name = "PARAM_LEVEL")
    public String getParamLevel() {
        return paramLevel;
    }

    public void setParamLevel(String paramLevel) {
        this.paramLevel = paramLevel;
    }

    @Basic
    @Column(name = "PARAM_DISPLAY")
    public String getParamDisplay() {
        return paramDisplay;
    }

    public void setParamDisplay(String paramDisplay) {
        this.paramDisplay = paramDisplay;
    }

    @Basic
    @Column(name = "CREATE_BY")
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    @Basic
    @Column(name = "CREATE_DTM")
    public Date getCreateDtm() {
        return createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    @Basic
    @Column(name = "UPDATE_BY")
    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    @Basic
    @Column(name = "UPDATE_DTM")
    public Date getUpdateDtm() {
        return updateDtm;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }

    @Id
    @Column(name = "ACTION_TYPE")
    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    @Id
    @Column(name = "ACTION_DATE")
    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeUserSettingLogPo that = (MoeUserSettingLogPo) o;

        if (loginId != null ? !loginId.equals(that.loginId) : that.loginId != null) return false;
        if (loginName != null ? !loginName.equals(that.loginName) : that.loginName != null) return false;
        if (paramName != null ? !paramName.equals(that.paramName) : that.paramName != null) return false;
        if (paramNameDesc != null ? !paramNameDesc.equals(that.paramNameDesc) : that.paramNameDesc != null)
            return false;
        if (paramValue != null ? !paramValue.equals(that.paramValue) : that.paramValue != null) return false;
        if (paramValueType != null ? !paramValueType.equals(that.paramValueType) : that.paramValueType != null)
            return false;
        if (paramPossibleValue != null ? !paramPossibleValue.equals(that.paramPossibleValue) : that.paramPossibleValue != null)
            return false;
        if (paramLevel != null ? !paramLevel.equals(that.paramLevel) : that.paramLevel != null) return false;
        if (paramDisplay != null ? !paramDisplay.equals(that.paramDisplay) : that.paramDisplay != null) return false;
        if (createBy != null ? !createBy.equals(that.createBy) : that.createBy != null) return false;
        if (createDtm != null ? !createDtm.equals(that.createDtm) : that.createDtm != null) return false;
        if (updateBy != null ? !updateBy.equals(that.updateBy) : that.updateBy != null) return false;
        if (updateDtm != null ? !updateDtm.equals(that.updateDtm) : that.updateDtm != null) return false;
        if (actionType != null ? !actionType.equals(that.actionType) : that.actionType != null) return false;
        if (actionDate != null ? !actionDate.equals(that.actionDate) : that.actionDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = loginId != null ? loginId.hashCode() : 0;
        result = 31 * result + (loginName != null ? loginName.hashCode() : 0);
        result = 31 * result + (paramName != null ? paramName.hashCode() : 0);
        result = 31 * result + (paramNameDesc != null ? paramNameDesc.hashCode() : 0);
        result = 31 * result + (paramValue != null ? paramValue.hashCode() : 0);
        result = 31 * result + (paramValueType != null ? paramValueType.hashCode() : 0);
        result = 31 * result + (paramPossibleValue != null ? paramPossibleValue.hashCode() : 0);
        result = 31 * result + (paramLevel != null ? paramLevel.hashCode() : 0);
        result = 31 * result + (paramDisplay != null ? paramDisplay.hashCode() : 0);
        result = 31 * result + (createBy != null ? createBy.hashCode() : 0);
        result = 31 * result + (createDtm != null ? createDtm.hashCode() : 0);
        result = 31 * result + (updateBy != null ? updateBy.hashCode() : 0);
        result = 31 * result + (updateDtm != null ? updateDtm.hashCode() : 0);
        result = 31 * result + (actionType != null ? actionType.hashCode() : 0);
        result = 31 * result + (actionDate != null ? actionDate.hashCode() : 0);
        return result;
    }
}
