package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_SITE_TERM_ID_MAP")
@IdClass(MoeSiteTermIdMapPoPK.class)
public class MoeSiteTermIdMapPo implements Serializable {
    private static final long serialVersionUID = -8322124736148168664L;
    private String siteEng;
    private String ehrSiteDesc;
    private long ehrTermId;
    private String defaultMap;
    private String ehrStatus;

    @Id
    @Column(name = "SITE_ENG")
    public String getSiteEng() {
        return siteEng;
    }

    public void setSiteEng(String siteEng) {
        this.siteEng = siteEng;
    }

    @Basic
    @Column(name = "EHR_SITE_DESC")
    public String getEhrSiteDesc() {
        return ehrSiteDesc;
    }

    public void setEhrSiteDesc(String ehrSiteDesc) {
        this.ehrSiteDesc = ehrSiteDesc;
    }

    @Id
    @Column(name = "EHR_TERM_ID")
    public long getEhrTermId() {
        return ehrTermId;
    }

    public void setEhrTermId(long ehrTermId) {
        this.ehrTermId = ehrTermId;
    }

    @Basic
    @Column(name = "DEFAULT_MAP")
    public String getDefaultMap() {
        return defaultMap;
    }

    public void setDefaultMap(String defaultMap) {
        this.defaultMap = defaultMap;
    }

    @Basic
    @Column(name = "EHR_STATUS")
    public String getEhrStatus() {
        return ehrStatus;
    }

    public void setEhrStatus(String ehrStatus) {
        this.ehrStatus = ehrStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeSiteTermIdMapPo that = (MoeSiteTermIdMapPo) o;

        if (ehrTermId != that.ehrTermId) return false;
        if (siteEng != null ? !siteEng.equals(that.siteEng) : that.siteEng != null) return false;
        if (ehrSiteDesc != null ? !ehrSiteDesc.equals(that.ehrSiteDesc) : that.ehrSiteDesc != null) return false;
        if (defaultMap != null ? !defaultMap.equals(that.defaultMap) : that.defaultMap != null) return false;
        if (ehrStatus != null ? !ehrStatus.equals(that.ehrStatus) : that.ehrStatus != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = siteEng != null ? siteEng.hashCode() : 0;
        result = 31 * result + (ehrSiteDesc != null ? ehrSiteDesc.hashCode() : 0);
        result = 31 * result + (int) (ehrTermId ^ (ehrTermId >>> 32));
        result = 31 * result + (defaultMap != null ? defaultMap.hashCode() : 0);
        result = 31 * result + (ehrStatus != null ? ehrStatus.hashCode() : 0);
        return result;
    }
}
