package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_FORM_ROUTE_MAP")
@IdClass(MoeFormRouteMapPoPK.class)
public class MoeFormRouteMapPo implements Serializable {
    private static final long serialVersionUID = 1742237682926381736L;
    private long formId;
    private long routeId;
    private String displayRoute;
    private MoeRoutePo moeRoute;
    private MoeFormPo moeForm;

    @Id
    @Column(name = "FORM_ID")
    public long getFormId() {
        return formId;
    }

    public void setFormId(long formId) {
        this.formId = formId;
    }

    @Id
    @Column(name = "ROUTE_ID")
    public long getRouteId() {
        return routeId;
    }

    public void setRouteId(long routeId) {
        this.routeId = routeId;
    }

    @Basic
    @Column(name = "DISPLAY_ROUTE")
    public String getDisplayRoute() {
        return displayRoute;
    }

    public void setDisplayRoute(String displayRoute) {
        this.displayRoute = displayRoute;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "route_id", nullable = false, insertable = false, updatable = false)
    public MoeRoutePo getMoeRoute() {
        return this.moeRoute;
    }

    public void setMoeRoute(MoeRoutePo moeRoute) {
        this.moeRoute = moeRoute;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "form_id", nullable = false, insertable = false, updatable = false)
    public MoeFormPo getMoeForm() {
        return this.moeForm;
    }

    public void setMoeForm(MoeFormPo moeForm) {
        this.moeForm = moeForm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeFormRouteMapPo that = (MoeFormRouteMapPo) o;

        if (formId != that.formId) return false;
        if (routeId != that.routeId) return false;
        if (displayRoute != null ? !displayRoute.equals(that.displayRoute) : that.displayRoute != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (formId ^ (formId >>> 32));
        result = 31 * result + (int) (routeId ^ (routeId >>> 32));
        result = 31 * result + (displayRoute != null ? displayRoute.hashCode() : 0);
        return result;
    }
}
