package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeRouteTermIdMapPoPK implements Serializable {
    private static final long serialVersionUID = 8162170339400512960L;
    private String routeEng;
    private long ehrTermId;

    @Column(name = "ROUTE_ENG")
    @Id
    public String getRouteEng() {
        return routeEng;
    }

    public void setRouteEng(String routeEng) {
        this.routeEng = routeEng;
    }

    @Column(name = "EHR_TERM_ID")
    @Id
    public long getEhrTermId() {
        return ehrTermId;
    }

    public void setEhrTermId(long ehrTermId) {
        this.ehrTermId = ehrTermId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeRouteTermIdMapPoPK that = (MoeRouteTermIdMapPoPK) o;

        if (ehrTermId != that.ehrTermId) return false;
        if (routeEng != null ? !routeEng.equals(that.routeEng) : that.routeEng != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = routeEng != null ? routeEng.hashCode() : 0;
        result = 31 * result + (int) (ehrTermId ^ (ehrTermId >>> 32));
        return result;
    }
}
