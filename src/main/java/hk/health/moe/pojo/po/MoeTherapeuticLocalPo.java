/*
package hk.health.moe.pojo.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MOE_THERAPEUTIC_LOCAL")
public class MoeTherapeuticLocalPo {
    private long therapeuticLocalId;
    private String therapeuticClassCode;
    private String therapeuticClassDescription;

    @Id
    @Column(name = "THERAPEUTIC_LOCAL_ID")
    public long getTherapeuticLocalId() {
        return therapeuticLocalId;
    }

    public void setTherapeuticLocalId(long therapeuticLocalId) {
        this.therapeuticLocalId = therapeuticLocalId;
    }

    @Basic
    @Column(name = "THERAPEUTIC_CLASS_CODE")
    public String getTherapeuticClassCode() {
        return therapeuticClassCode;
    }

    public void setTherapeuticClassCode(String therapeuticClassCode) {
        this.therapeuticClassCode = therapeuticClassCode;
    }

    @Basic
    @Column(name = "THERAPEUTIC_CLASS_DESCRIPTION")
    public String getTherapeuticClassDescription() {
        return therapeuticClassDescription;
    }

    public void setTherapeuticClassDescription(String therapeuticClassDescription) {
        this.therapeuticClassDescription = therapeuticClassDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeTherapeuticLocalPo that = (MoeTherapeuticLocalPo) o;

        if (therapeuticLocalId != that.therapeuticLocalId) return false;
        if (therapeuticClassCode != null ? !therapeuticClassCode.equals(that.therapeuticClassCode) : that.therapeuticClassCode != null)
            return false;
        if (therapeuticClassDescription != null ? !therapeuticClassDescription.equals(that.therapeuticClassDescription) : that.therapeuticClassDescription != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (therapeuticLocalId ^ (therapeuticLocalId >>> 32));
        result = 31 * result + (therapeuticClassCode != null ? therapeuticClassCode.hashCode() : 0);
        result = 31 * result + (therapeuticClassDescription != null ? therapeuticClassDescription.hashCode() : 0);
        return result;
    }
}
*/
