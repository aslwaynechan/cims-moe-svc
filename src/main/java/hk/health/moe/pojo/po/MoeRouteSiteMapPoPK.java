package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeRouteSiteMapPoPK implements Serializable {
    private static final long serialVersionUID = 4221808333136510235L;
    private long routeId;
    private long siteId;

    @Column(name = "ROUTE_ID")
    @Id
    public long getRouteId() {
        return routeId;
    }

    public void setRouteId(long routeId) {
        this.routeId = routeId;
    }

    @Column(name = "SITE_ID")
    @Id
    public long getSiteId() {
        return siteId;
    }

    public void setSiteId(long siteId) {
        this.siteId = siteId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeRouteSiteMapPoPK that = (MoeRouteSiteMapPoPK) o;

        if (routeId != that.routeId) return false;
        if (siteId != that.siteId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (routeId ^ (routeId >>> 32));
        result = 31 * result + (int) (siteId ^ (siteId >>> 32));
        return result;
    }
}
