package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeLocationUserGroupMapPoPK implements Serializable {
    private static final long serialVersionUID = 7739714378455678793L;
    private String hospcode;
    private String userSpecialty;

    @Column(name = "HOSPCODE")
    @Id
    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    @Column(name = "USER_SPECIALTY")
    @Id
    public String getUserSpecialty() {
        return userSpecialty;
    }

    public void setUserSpecialty(String userSpecialty) {
        this.userSpecialty = userSpecialty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeLocationUserGroupMapPoPK that = (MoeLocationUserGroupMapPoPK) o;

        if (hospcode != null ? !hospcode.equals(that.hospcode) : that.hospcode != null) return false;
        if (userSpecialty != null ? !userSpecialty.equals(that.userSpecialty) : that.userSpecialty != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hospcode != null ? hospcode.hashCode() : 0;
        result = 31 * result + (userSpecialty != null ? userSpecialty.hashCode() : 0);
        return result;
    }
}
