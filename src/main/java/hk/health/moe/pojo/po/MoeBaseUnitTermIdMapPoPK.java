package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MoeBaseUnitTermIdMapPoPK implements Serializable {
    private static final long serialVersionUID = 7031087884672026145L;
    private String baseUnit;
    private long ehrTermId;

    @Column(name = "BASE_UNIT")
    @Id
    public String getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(String baseUnit) {
        this.baseUnit = baseUnit;
    }

    @Column(name = "EHR_TERM_ID")
    @Id
    public long getEhrTermId() {
        return ehrTermId;
    }

    public void setEhrTermId(long ehrTermId) {
        this.ehrTermId = ehrTermId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeBaseUnitTermIdMapPoPK that = (MoeBaseUnitTermIdMapPoPK) o;

        if (ehrTermId != that.ehrTermId) return false;
        if (baseUnit != null ? !baseUnit.equals(that.baseUnit) : that.baseUnit != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = baseUnit != null ? baseUnit.hashCode() : 0;
        result = 31 * result + (int) (ehrTermId ^ (ehrTermId >>> 32));
        return result;
    }
}
