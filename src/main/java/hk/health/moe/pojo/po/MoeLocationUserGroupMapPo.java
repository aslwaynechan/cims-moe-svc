package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MOE_LOCATION_USER_GROUP_MAP")
@IdClass(MoeLocationUserGroupMapPoPK.class)
public class MoeLocationUserGroupMapPo implements Serializable {
    private static final long serialVersionUID = 2561029137430455310L;
    private String hospcode;
    private String userSpecialty;

    @Id
    @Column(name = "HOSPCODE")
    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    @Id
    @Column(name = "USER_SPECIALTY")
    public String getUserSpecialty() {
        return userSpecialty;
    }

    public void setUserSpecialty(String userSpecialty) {
        this.userSpecialty = userSpecialty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeLocationUserGroupMapPo that = (MoeLocationUserGroupMapPo) o;

        if (hospcode != null ? !hospcode.equals(that.hospcode) : that.hospcode != null) return false;
        if (userSpecialty != null ? !userSpecialty.equals(that.userSpecialty) : that.userSpecialty != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hospcode != null ? hospcode.hashCode() : 0;
        result = 31 * result + (userSpecialty != null ? userSpecialty.hashCode() : 0);
        return result;
    }
}
