package hk.health.moe.pojo.po;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "MOE_EHR_MED_MULT_DOSE")
@IdClass(MoeMedMultDosePoPK.class)
@DynamicUpdate
//@IdClass(MoeEhrMedMultDosePoPK.class)
public class MoeEhrMedMultDosePo {
    private String hospcode;
    private long ordNo;
    private long cmsItemNo;
    private long stepNo;
    private long multDoseNo;
    private Long freqId;
    private Long supplFreqId;
    private Long moQtyUnitId;
    private MoeMedMultDosePo moeMedMultDose;

    @Id
    @Column(name = "HOSPCODE")
    public String getHospcode() {
        return hospcode;
    }

    public void setHospcode(String hospcode) {
        this.hospcode = hospcode;
    }

    @Id
    @Column(name = "ORD_NO")
    public long getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(long ordNo) {
        this.ordNo = ordNo;
    }

    @Id
    @Column(name = "CMS_ITEM_NO")
    public long getCmsItemNo() {
        return cmsItemNo;
    }

    public void setCmsItemNo(long cmsItemNo) {
        this.cmsItemNo = cmsItemNo;
    }

    @Id
    @Column(name = "STEP_NO")
    public long getStepNo() {
        return stepNo;
    }

    public void setStepNo(long stepNo) {
        this.stepNo = stepNo;
    }

    @Id
    @Column(name = "MULT_DOSE_NO")
    public long getMultDoseNo() {
        return multDoseNo;
    }

    public void setMultDoseNo(long multDoseNo) {
        this.multDoseNo = multDoseNo;
    }

    @Basic
    @Column(name = "FREQ_ID")
    public Long getFreqId() {
        return freqId;
    }

    public void setFreqId(Long freqId) {
        this.freqId = freqId;
    }

    @Basic
    @Column(name = "SUPPL_FREQ_ID")
    public Long getSupplFreqId() {
        return supplFreqId;
    }

    public void setSupplFreqId(Long supplFreqId) {
        this.supplFreqId = supplFreqId;
    }

    @Basic
    @Column(name = "MO_QTY_UNIT_ID")
    public Long getMoQtyUnitId() {
        return moQtyUnitId;
    }

    public void setMoQtyUnitId(Long moQtyUnitId) {
        this.moQtyUnitId = moQtyUnitId;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public MoeMedMultDosePo getMoeMedMultDose() {
        return this.moeMedMultDose;
    }

    public void setMoeMedMultDose(MoeMedMultDosePo moeMedMultDose) {
        this.moeMedMultDose = moeMedMultDose;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeEhrMedMultDosePo that = (MoeEhrMedMultDosePo) o;

        if (ordNo != that.ordNo) return false;
        if (cmsItemNo != that.cmsItemNo) return false;
        if (stepNo != that.stepNo) return false;
        if (multDoseNo != that.multDoseNo) return false;
        if (hospcode != null ? !hospcode.equals(that.hospcode) : that.hospcode != null) return false;
        if (freqId != null ? !freqId.equals(that.freqId) : that.freqId != null) return false;
        if (supplFreqId != null ? !supplFreqId.equals(that.supplFreqId) : that.supplFreqId != null) return false;
        if (moQtyUnitId != null ? !moQtyUnitId.equals(that.moQtyUnitId) : that.moQtyUnitId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hospcode != null ? hospcode.hashCode() : 0;
        result = 31 * result + (int) (ordNo ^ (ordNo >>> 32));
        result = 31 * result + (int) (cmsItemNo ^ (cmsItemNo >>> 32));
        result = 31 * result + (int) (stepNo ^ (stepNo >>> 32));
        result = 31 * result + (int) (multDoseNo ^ (multDoseNo >>> 32));
        result = 31 * result + (freqId != null ? freqId.hashCode() : 0);
        result = 31 * result + (supplFreqId != null ? supplFreqId.hashCode() : 0);
        result = 31 * result + (moQtyUnitId != null ? moQtyUnitId.hashCode() : 0);
        return result;
    }
}
