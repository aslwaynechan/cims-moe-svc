package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "MOE_FREQ_LOCAL_LOG")
@IdClass(MoeFreqLocalLogPoPK.class)
public class MoeFreqLocalLogPo implements Serializable {
    private static final long serialVersionUID = 6282745228958097752L;
    private long freqId;
    private String freqCode;
    private String freqEng;
    private String freqChi;
    private long upperLimit;
    private long lowerLimit;
    private long freqMultiplier;
    private Long durationValue;
    private String durationUnit;
    private String useInputValue;
    private String useInputMethod;
    private long rank;
    private String createUserId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private Date createDtm;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private Date updateDtm;
    private long version;
    private String actionType;
    private Date actionDate;
    private Long freqMultiplierDenominator;
    private String freqDescEng;

    @Id
    @Column(name = "FREQ_ID")
    public long getFreqId() {
        return freqId;
    }

    public void setFreqId(long freqId) {
        this.freqId = freqId;
    }

    @Id
    @Column(name = "FREQ_CODE")
    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    @Id
    @Column(name = "FREQ_ENG")
    public String getFreqEng() {
        return freqEng;
    }

    public void setFreqEng(String freqEng) {
        this.freqEng = freqEng;
    }

    @Id
    @Column(name = "FREQ_CHI")
    public String getFreqChi() {
        return freqChi;
    }

    public void setFreqChi(String freqChi) {
        this.freqChi = freqChi;
    }

    @Id
    @Column(name = "UPPER_LIMIT")
    public long getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(long upperLimit) {
        this.upperLimit = upperLimit;
    }

    @Id
    @Column(name = "LOWER_LIMIT")
    public long getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(long lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    @Id
    @Column(name = "FREQ_MULTIPLIER")
    public long getFreqMultiplier() {
        return freqMultiplier;
    }

    public void setFreqMultiplier(long freqMultiplier) {
        this.freqMultiplier = freqMultiplier;
    }

    @Id
    @Column(name = "DURATION_VALUE")
    public Long getDurationValue() {
        return durationValue;
    }

    public void setDurationValue(Long durationValue) {
        this.durationValue = durationValue;
    }

    @Id
    @Column(name = "DURATION_UNIT")
    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    @Id
    @Column(name = "USE_INPUT_VALUE")
    public String getUseInputValue() {
        return useInputValue;
    }

    public void setUseInputValue(String useInputValue) {
        this.useInputValue = useInputValue;
    }

    @Id
    @Column(name = "USE_INPUT_METHOD")
    public String getUseInputMethod() {
        return useInputMethod;
    }

    public void setUseInputMethod(String useInputMethod) {
        this.useInputMethod = useInputMethod;
    }

    @Id
    @Column(name = "RANK")
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    @Id
    @Column(name = "CREATE_USER_ID")
    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    @Id
    @Column(name = "CREATE_USER")
    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    @Id
    @Column(name = "CREATE_HOSP")
    public String getCreateHosp() {
        return createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    @Id
    @Column(name = "CREATE_RANK")
    public String getCreateRank() {
        return createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    @Id
    @Column(name = "CREATE_RANK_DESC")
    public String getCreateRankDesc() {
        return createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    @Id
    @Column(name = "CREATE_DTM")
    public Date getCreateDtm() {
        return createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    @Id
    @Column(name = "UPDATE_USER_ID")
    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Id
    @Column(name = "UPDATE_USER")
    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Id
    @Column(name = "UPDATE_HOSP")
    public String getUpdateHosp() {
        return updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    @Id
    @Column(name = "UPDATE_RANK")
    public String getUpdateRank() {
        return updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    @Id
    @Column(name = "UPDATE_RANK_DESC")
    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    @Id
    @Column(name = "UPDATE_DTM")
    public Date getUpdateDtm() {
        return updateDtm;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }

    @Id
    @Column(name = "VERSION")
    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    @Id
    @Column(name = "ACTION_TYPE")
    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    @Id
    @Column(name = "ACTION_DATE")
    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    @Id
    @Column(name = "FREQ_MULTIPLIER_DENOMINATOR")
    public Long getFreqMultiplierDenominator() {
        return freqMultiplierDenominator;
    }

    public void setFreqMultiplierDenominator(Long freqMultiplierDenominator) {
        this.freqMultiplierDenominator = freqMultiplierDenominator;
    }

    @Id
    @Column(name = "FREQ_DESC_ENG")
    public String getFreqDescEng() {
        return freqDescEng;
    }

    public void setFreqDescEng(String freqDescEng) {
        this.freqDescEng = freqDescEng;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeFreqLocalLogPo that = (MoeFreqLocalLogPo) o;

        if (freqId != that.freqId) return false;
        if (upperLimit != that.upperLimit) return false;
        if (lowerLimit != that.lowerLimit) return false;
        if (freqMultiplier != that.freqMultiplier) return false;
        if (rank != that.rank) return false;
        if (version != that.version) return false;
        if (freqCode != null ? !freqCode.equals(that.freqCode) : that.freqCode != null) return false;
        if (freqEng != null ? !freqEng.equals(that.freqEng) : that.freqEng != null) return false;
        if (freqChi != null ? !freqChi.equals(that.freqChi) : that.freqChi != null) return false;
        if (durationValue != null ? !durationValue.equals(that.durationValue) : that.durationValue != null)
            return false;
        if (durationUnit != null ? !durationUnit.equals(that.durationUnit) : that.durationUnit != null) return false;
        if (useInputValue != null ? !useInputValue.equals(that.useInputValue) : that.useInputValue != null)
            return false;
        if (useInputMethod != null ? !useInputMethod.equals(that.useInputMethod) : that.useInputMethod != null)
            return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null) return false;
        if (createUser != null ? !createUser.equals(that.createUser) : that.createUser != null) return false;
        if (createHosp != null ? !createHosp.equals(that.createHosp) : that.createHosp != null) return false;
        if (createRank != null ? !createRank.equals(that.createRank) : that.createRank != null) return false;
        if (createRankDesc != null ? !createRankDesc.equals(that.createRankDesc) : that.createRankDesc != null)
            return false;
        if (createDtm != null ? !createDtm.equals(that.createDtm) : that.createDtm != null) return false;
        if (updateUserId != null ? !updateUserId.equals(that.updateUserId) : that.updateUserId != null) return false;
        if (updateUser != null ? !updateUser.equals(that.updateUser) : that.updateUser != null) return false;
        if (updateHosp != null ? !updateHosp.equals(that.updateHosp) : that.updateHosp != null) return false;
        if (updateRank != null ? !updateRank.equals(that.updateRank) : that.updateRank != null) return false;
        if (updateRankDesc != null ? !updateRankDesc.equals(that.updateRankDesc) : that.updateRankDesc != null)
            return false;
        if (updateDtm != null ? !updateDtm.equals(that.updateDtm) : that.updateDtm != null) return false;
        if (actionType != null ? !actionType.equals(that.actionType) : that.actionType != null) return false;
        if (actionDate != null ? !actionDate.equals(that.actionDate) : that.actionDate != null) return false;
        if (freqMultiplierDenominator != null ? !freqMultiplierDenominator.equals(that.freqMultiplierDenominator) : that.freqMultiplierDenominator != null)
            return false;
        if (freqDescEng != null ? !freqDescEng.equals(that.freqDescEng) : that.freqDescEng != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (freqId ^ (freqId >>> 32));
        result = 31 * result + (freqCode != null ? freqCode.hashCode() : 0);
        result = 31 * result + (freqEng != null ? freqEng.hashCode() : 0);
        result = 31 * result + (freqChi != null ? freqChi.hashCode() : 0);
        result = 31 * result + (int) (upperLimit ^ (upperLimit >>> 32));
        result = 31 * result + (int) (lowerLimit ^ (lowerLimit >>> 32));
        result = 31 * result + (int) (freqMultiplier ^ (freqMultiplier >>> 32));
        result = 31 * result + (durationValue != null ? durationValue.hashCode() : 0);
        result = 31 * result + (durationUnit != null ? durationUnit.hashCode() : 0);
        result = 31 * result + (useInputValue != null ? useInputValue.hashCode() : 0);
        result = 31 * result + (useInputMethod != null ? useInputMethod.hashCode() : 0);
        result = 31 * result + (int) (rank ^ (rank >>> 32));
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (createUser != null ? createUser.hashCode() : 0);
        result = 31 * result + (createHosp != null ? createHosp.hashCode() : 0);
        result = 31 * result + (createRank != null ? createRank.hashCode() : 0);
        result = 31 * result + (createRankDesc != null ? createRankDesc.hashCode() : 0);
        result = 31 * result + (createDtm != null ? createDtm.hashCode() : 0);
        result = 31 * result + (updateUserId != null ? updateUserId.hashCode() : 0);
        result = 31 * result + (updateUser != null ? updateUser.hashCode() : 0);
        result = 31 * result + (updateHosp != null ? updateHosp.hashCode() : 0);
        result = 31 * result + (updateRank != null ? updateRank.hashCode() : 0);
        result = 31 * result + (updateRankDesc != null ? updateRankDesc.hashCode() : 0);
        result = 31 * result + (updateDtm != null ? updateDtm.hashCode() : 0);
        result = 31 * result + (int) (version ^ (version >>> 32));
        result = 31 * result + (actionType != null ? actionType.hashCode() : 0);
        result = 31 * result + (actionDate != null ? actionDate.hashCode() : 0);
        result = 31 * result + (freqMultiplierDenominator != null ? freqMultiplierDenominator.hashCode() : 0);
        result = 31 * result + (freqDescEng != null ? freqDescEng.hashCode() : 0);
        return result;
    }
}
