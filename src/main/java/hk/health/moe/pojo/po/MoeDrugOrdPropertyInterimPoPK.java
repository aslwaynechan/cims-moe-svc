package hk.health.moe.pojo.po;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class MoeDrugOrdPropertyInterimPoPK implements Serializable {
    private static final long serialVersionUID = 7505265773278323931L;
    private String localDrugId;
    private String suspend;
    private String outOfStock;
    private BigDecimal price;
    private BigDecimal minimumDosage;
    private Long maximumDuration;
    private String createUserId;
    private String createUser;
    private String createHosp;
    private String createRank;
    private String createRankDesc;
    private Date createDtm;
    private String updateUserId;
    private String updateUser;
    private String updateHosp;
    private String updateRank;
    private String updateRankDesc;
    private Date updateDtm;
    private long version;
    private String tradeNameAlias;
    private String specifyQuantityFlag;

    @Id
    @Column(name = "LOCAL_DRUG_ID")
    public String getLocalDrugId() {
        return localDrugId;
    }

    public void setLocalDrugId(String localDrugId) {
        this.localDrugId = localDrugId;
    }

    @Id
    @Column(name = "SUSPEND")
    public String getSuspend() {
        return suspend;
    }

    public void setSuspend(String suspend) {
        this.suspend = suspend;
    }

    @Id
    @Column(name = "OUT_OF_STOCK")
    public String getOutOfStock() {
        return outOfStock;
    }

    public void setOutOfStock(String outOfStock) {
        this.outOfStock = outOfStock;
    }

    @Id
    @Column(name = "PRICE")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Id
    @Column(name = "MINIMUM_DOSAGE")
    public BigDecimal getMinimumDosage() {
        return minimumDosage;
    }

    public void setMinimumDosage(BigDecimal minimumDosage) {
        this.minimumDosage = minimumDosage;
    }

    @Id
    @Column(name = "MAXIMUM_DURATION")
    public Long getMaximumDuration() {
        return maximumDuration;
    }

    public void setMaximumDuration(Long maximumDuration) {
        this.maximumDuration = maximumDuration;
    }

    @Id
    @Column(name = "CREATE_USER_ID")
    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    @Id
    @Column(name = "CREATE_USER")
    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    @Id
    @Column(name = "CREATE_HOSP")
    public String getCreateHosp() {
        return createHosp;
    }

    public void setCreateHosp(String createHosp) {
        this.createHosp = createHosp;
    }

    @Id
    @Column(name = "CREATE_RANK")
    public String getCreateRank() {
        return createRank;
    }

    public void setCreateRank(String createRank) {
        this.createRank = createRank;
    }

    @Id
    @Column(name = "CREATE_RANK_DESC")
    public String getCreateRankDesc() {
        return createRankDesc;
    }

    public void setCreateRankDesc(String createRankDesc) {
        this.createRankDesc = createRankDesc;
    }

    @Id
    @Column(name = "CREATE_DTM")
    public Date getCreateDtm() {
        return createDtm;
    }

    public void setCreateDtm(Date createDtm) {
        this.createDtm = createDtm;
    }

    @Id
    @Column(name = "UPDATE_USER_ID")
    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Id
    @Column(name = "UPDATE_USER")
    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Id
    @Column(name = "UPDATE_HOSP")
    public String getUpdateHosp() {
        return updateHosp;
    }

    public void setUpdateHosp(String updateHosp) {
        this.updateHosp = updateHosp;
    }

    @Id
    @Column(name = "UPDATE_RANK")
    public String getUpdateRank() {
        return updateRank;
    }

    public void setUpdateRank(String updateRank) {
        this.updateRank = updateRank;
    }

    @Id
    @Column(name = "UPDATE_RANK_DESC")
    public String getUpdateRankDesc() {
        return updateRankDesc;
    }

    public void setUpdateRankDesc(String updateRankDesc) {
        this.updateRankDesc = updateRankDesc;
    }

    @Id
    @Column(name = "UPDATE_DTM")
    public Date getUpdateDtm() {
        return updateDtm;
    }

    public void setUpdateDtm(Date updateDtm) {
        this.updateDtm = updateDtm;
    }

    @Id
    @Column(name = "VERSION")
    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    @Id
    @Column(name = "TRADE_NAME_ALIAS")
    public String getTradeNameAlias() {
        return tradeNameAlias;
    }

    public void setTradeNameAlias(String tradeNameAlias) {
        this.tradeNameAlias = tradeNameAlias;
    }

    @Id
    @Column(name = "SPECIFY_QUANTITY_FLAG")
    public String getSpecifyQuantityFlag() {
        return specifyQuantityFlag;
    }

    public void setSpecifyQuantityFlag(String specifyQuantityFlag) {
        this.specifyQuantityFlag = specifyQuantityFlag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeDrugOrdPropertyInterimPoPK that = (MoeDrugOrdPropertyInterimPoPK) o;

        if (version != that.version) return false;
        if (localDrugId != null ? !localDrugId.equals(that.localDrugId) : that.localDrugId != null) return false;
        if (suspend != null ? !suspend.equals(that.suspend) : that.suspend != null) return false;
        if (outOfStock != null ? !outOfStock.equals(that.outOfStock) : that.outOfStock != null) return false;
        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        if (minimumDosage != null ? !minimumDosage.equals(that.minimumDosage) : that.minimumDosage != null)
            return false;
        if (maximumDuration != null ? !maximumDuration.equals(that.maximumDuration) : that.maximumDuration != null)
            return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null) return false;
        if (createUser != null ? !createUser.equals(that.createUser) : that.createUser != null) return false;
        if (createHosp != null ? !createHosp.equals(that.createHosp) : that.createHosp != null) return false;
        if (createRank != null ? !createRank.equals(that.createRank) : that.createRank != null) return false;
        if (createRankDesc != null ? !createRankDesc.equals(that.createRankDesc) : that.createRankDesc != null)
            return false;
        if (createDtm != null ? !createDtm.equals(that.createDtm) : that.createDtm != null) return false;
        if (updateUserId != null ? !updateUserId.equals(that.updateUserId) : that.updateUserId != null) return false;
        if (updateUser != null ? !updateUser.equals(that.updateUser) : that.updateUser != null) return false;
        if (updateHosp != null ? !updateHosp.equals(that.updateHosp) : that.updateHosp != null) return false;
        if (updateRank != null ? !updateRank.equals(that.updateRank) : that.updateRank != null) return false;
        if (updateRankDesc != null ? !updateRankDesc.equals(that.updateRankDesc) : that.updateRankDesc != null)
            return false;
        if (updateDtm != null ? !updateDtm.equals(that.updateDtm) : that.updateDtm != null) return false;
        if (tradeNameAlias != null ? !tradeNameAlias.equals(that.tradeNameAlias) : that.tradeNameAlias != null)
            return false;
        if (specifyQuantityFlag != null ? !specifyQuantityFlag.equals(that.specifyQuantityFlag) : that.specifyQuantityFlag != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = localDrugId != null ? localDrugId.hashCode() : 0;
        result = 31 * result + (suspend != null ? suspend.hashCode() : 0);
        result = 31 * result + (outOfStock != null ? outOfStock.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (minimumDosage != null ? minimumDosage.hashCode() : 0);
        result = 31 * result + (maximumDuration != null ? maximumDuration.hashCode() : 0);
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (createUser != null ? createUser.hashCode() : 0);
        result = 31 * result + (createHosp != null ? createHosp.hashCode() : 0);
        result = 31 * result + (createRank != null ? createRank.hashCode() : 0);
        result = 31 * result + (createRankDesc != null ? createRankDesc.hashCode() : 0);
        result = 31 * result + (createDtm != null ? createDtm.hashCode() : 0);
        result = 31 * result + (updateUserId != null ? updateUserId.hashCode() : 0);
        result = 31 * result + (updateUser != null ? updateUser.hashCode() : 0);
        result = 31 * result + (updateHosp != null ? updateHosp.hashCode() : 0);
        result = 31 * result + (updateRank != null ? updateRank.hashCode() : 0);
        result = 31 * result + (updateRankDesc != null ? updateRankDesc.hashCode() : 0);
        result = 31 * result + (updateDtm != null ? updateDtm.hashCode() : 0);
        result = 31 * result + (int) (version ^ (version >>> 32));
        result = 31 * result + (tradeNameAlias != null ? tradeNameAlias.hashCode() : 0);
        result = 31 * result + (specifyQuantityFlag != null ? specifyQuantityFlag.hashCode() : 0);
        return result;
    }
}
