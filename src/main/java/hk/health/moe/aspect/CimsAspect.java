package hk.health.moe.aspect;

/**************************************************************************
 * NAME        : CimsAspect.java
 * VERSION     : 1.0.0
 * DATE        : 03-JUL-2019
 * DESCRIPTION : AOP for MOE
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       03-JUL-2019  Initial Version
 **************************************************************************/

import hk.health.moe.common.ResponseCode;
import hk.health.moe.exception.ConcurrentException;
import hk.health.moe.exception.DacServiceException;
import hk.health.moe.exception.DrugServiceException;
import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.exception.ParamException;
import hk.health.moe.exception.RecordException;
import hk.health.moe.exception.SaamServiceException;
import hk.health.moe.exception.SecurityException;
import hk.health.moe.pojo.bo.AspectLogBo;
import hk.health.moe.pojo.vo.CimsResponseVo;
import hk.health.moe.util.HttpContextUtil;
import hk.health.moe.util.LocaleMessageSourceUtil;
import hk.health.moe.util.ResponseUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@Aspect
public class CimsAspect {

    private final static Logger logger = LoggerFactory.getLogger(CimsAspect.class);

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    @Value("${restTemplate.systemLog.turn-on}")
    boolean turnOn;

    @Pointcut("execution(public * hk.health.moe.controller.*.*(..))")
    public void pointCut() {
    }

    @Around(value = "pointCut()")
    public CimsResponseVo around(ProceedingJoinPoint joinPoint) throws Throwable {
        AspectLogBo aspectLogBo = null;
        try {
            aspectLogBo = (AspectLogBo) HttpContextUtil.getHttpServletRequest().getAttribute("ASPECT_LOG");
            if (aspectLogBo != null) {
                aspectLogBo.setFunctionName(joinPoint.getSignature().toString());
            }
        } catch (Exception e) {
            logger.error("Can not initialize log object");
        }

        CimsResponseVo responseVo = null;
        Throwable exception = null;

        try {
            responseVo = (CimsResponseVo) joinPoint.proceed();
        } catch (ParamException e) {
            if (e.isFieldError()) {
                //eric 20191227 start--
//                responseVo = ResponseUtil.initErrorResultVO(e.getFieldErrorDtos(),e.getMessage());
                responseVo = ResponseUtil.initErrorResultVO(e.getFieldErrorDtos(),e.getResponseCode(),e.getMessage());
                //eric 20191227 end--
            } else {
                //responseVo = ResponseUtil.initErrorResultVO(e.getMessage());
                //eric 20191223 start--
//                responseVo = ResponseUtil.initErrorResultVO(ResponseCode.CommonMessage.PARAMETER_ERROR.getResponseCode(), e.getMessage());
                responseVo = ResponseUtil.initErrorResultVO(e.getResponseCode(),e.getMessage());
                //eric 20191223 end--
            }
        } catch (ObjectOptimisticLockingFailureException | ConcurrentException e) {
            //eric 20191223 start--
//            responseVo = ResponseUtil.initErrorResultVO(localeMessageSourceUtil.getMessage("records.concurrentError"));
            responseVo = ResponseUtil.initErrorResultVO(ResponseCode.SpecialMessage.RECORDS_CONCURRENT_ERRORS.getResponseCode(),
                    ResponseCode.SpecialMessage.RECORDS_CONCURRENT_ERRORS.getResponseMessage());
            //eric 20191223 end--
        } catch (SecurityException | RecordException e) {
            responseVo = ResponseUtil.initErrorResultVO(e.getMessage());
        } catch (MoeServiceException e) {
            //eric 20191223 start--
//            responseVo = ResponseUtil.initErrorResultVO(e.getMessage());
            responseVo = ResponseUtil.initErrorResultVO(e.getResponseCode(),e.getMessage());
            //eric 20191223 end--
            exception = e.getLogThrowable();
        } catch (DacServiceException e) {
            responseVo = ResponseUtil.initErrorResultVO(e.getResponseCode(), e.getMessage());
        } catch (SaamServiceException e) {
            //eric 20191223 start--
//            responseVo = ResponseUtil.initErrorResultVO(e.getMessage());
            responseVo = ResponseUtil.initErrorResultVO(e.getResponseCode(),e.getMessage());
            //eric 20191223 end--
        } catch (DrugServiceException e) {
            responseVo = ResponseUtil.initErrorResultVO(e.getMessage());
            exception = e.getLogThrowable();
        } catch (Exception e) {
            //eric 20191223 start--
//            responseVo = ResponseUtil.initErrorResultVO(localeMessageSourceUtil.getMessage("system.error"));
            responseVo = ResponseUtil.initErrorResultVO(ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseCode(),ResponseCode.CommonMessage.SYSTEM_ERROR.getResponseMessage());
            //eric 20191223 end--
            exception = e;
        } finally {
            if (exception != null) {
                if (aspectLogBo != null) {
                    String uuid = UUID.randomUUID().toString();
                    aspectLogBo.setErrorId(uuid);
                    logger.error(uuid, exception);
                } else {
                    logger.error("Exception", exception);
                }
            }
        }

        return responseVo;
    }

}
