package hk.health.moe.exception;

public class SaamServiceException extends Exception {

    //eric 20191223 start--
    private int responseCode;
    //eric 20191223 end--

    public SaamServiceException() {
        super();
    }

    public SaamServiceException(String message) {
        super(message);
    }

    //eric 20191223 start--
    public SaamServiceException(int responseCode,String message) {
        super(message);
        this.responseCode=responseCode;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
    //eric 20191223 end--
}
