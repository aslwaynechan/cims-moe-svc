package hk.health.moe.exception;

public class DacServiceException extends Exception {

    //Chris  20191205  -Start
    private Integer responseCode;
    private String responseMessage;
    //Chris  20191205  -End

    public DacServiceException() {
        super();
    }

    public DacServiceException(String responseMessage) {
        super(responseMessage);
    }

    //Chris  20191205  -Start
    public DacServiceException(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public DacServiceException(Integer responseCode, String responseMessage) {
        super(responseMessage);
        this.responseCode = responseCode;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }
    //Chris  20191205  -End

}
