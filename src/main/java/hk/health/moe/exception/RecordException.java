package hk.health.moe.exception;

import hk.health.moe.pojo.dto.FieldErrorDto;

import java.util.List;

/**************************************************************************
 * NAME        : ParamException.java
 * VERSION     : 1.0.0
 * DATE        : 03-JUL-2019
 * DESCRIPTION : Custom Exception for MOE, work for illegal or not exist record from database ...etc
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       03-JUL-2019  Initial Version
 **************************************************************************/

public class RecordException extends Exception {

    //eric  20191219  -Start
    private int responseCode;
    private String responseMessage;
    //eric  20191219  -End

    public RecordException() {
        super();
    }

    public RecordException(String message) {
        super(message);
    }

    //eric  20191219  -Start
    public RecordException(int responseCode, String responseMessage) {
        super(responseMessage);
        this.responseCode = responseCode;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }
    //eric  20191219  -End
}
