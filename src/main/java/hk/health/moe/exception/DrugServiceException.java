/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  MOE Drug Maintenance
 *
 * PROGRAM NAME    :  DrugException.java
 *
 * PURPOSE         :  Exception class for error handling
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.exception;


/**
 * @author Simon
 * @version 1.0
 * @date 2019/8/22
 * @Description for moe_drug_server
 */

public class DrugServiceException extends Exception {

    private static final long serialVersionUID = 2463511675399396570L;

    private Throwable logThrowable;

    public Throwable getLogThrowable() {
        return logThrowable;
    }

    public void setLogThrowable(Throwable logThrowable) {
        this.logThrowable = logThrowable;
    }


    public DrugServiceException() {
        super();
    }

    public DrugServiceException(String message) {
        super(message);
    }

    public DrugServiceException(Exception e) {
        super(e);
    }

    public DrugServiceException(String message, Exception e) {
        super(message, e);
        logThrowable = e;
    }
}
