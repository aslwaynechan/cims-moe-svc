package hk.health.moe.exception;

/**************************************************************************
 * NAME        : ConcurrentException.java
 * VERSION     : 1.0.0
 * DATE        : 03-JUL-2019
 * DESCRIPTION : Custom Exception for MOE, work for ObjectOptimisticLockingFailureException
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       03-JUL-2019  Initial Version
 **************************************************************************/

public class ConcurrentException extends Exception {

    public ConcurrentException() {
        super();
    }

    //eric  20191219  -Start
    private int responseCode;
    private String responseMessage;
    //eric  20191219  -End

    public ConcurrentException(String message) {
        super(message);
    }

    //eric  20191219  -Start
    public ConcurrentException(int responseCode, String responseMessage) {
        super(responseMessage);
        this.responseCode = responseCode;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }
    //eric  20191219  -End

}
