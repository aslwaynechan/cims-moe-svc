package hk.health.moe.exception;

public class SecurityException extends Exception {

    //eric 20191223 start--
    private int responseCode;
    //eric 20191223 end--
    public SecurityException() {
        super();
    }

    public SecurityException(String message) {
        super(message);
    }
    //eric 20191223 start--
    public SecurityException(int responseCode,String message) {
        super(message);
        this.responseCode=responseCode;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
    //eric 20191223 end--

}
