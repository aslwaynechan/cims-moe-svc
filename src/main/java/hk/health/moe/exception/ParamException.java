package hk.health.moe.exception;

/**************************************************************************
 * NAME        : ParamException.java
 * VERSION     : 1.0.0
 * DATE        : 03-JUL-2019
 * DESCRIPTION : Custom Exception for MOE, work for illegal parameter
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       03-JUL-2019  Initial Version
 **************************************************************************/

import hk.health.moe.pojo.dto.FieldErrorDto;

import java.util.List;

public class ParamException extends Exception {

    private boolean isFieldError;
    private List<FieldErrorDto> fieldErrorDtos;

    //eric  20191219  -Start
    private int responseCode;
    private String responseMessage;
    //eric  20191219  -End

    public ParamException() {
        super();
    }

    public ParamException(String message) {
        super(message);
    }

    public ParamException(String message, boolean isFieldError, List<FieldErrorDto> fieldErrorDtos) {
        super(message);
        this.isFieldError = isFieldError;
        this.fieldErrorDtos = fieldErrorDtos;
    }
    //eric 20191219 start--
    public ParamException(int responseCode,String message, boolean isFieldError, List<FieldErrorDto> fieldErrorDtos) {
        super(message);
        this.isFieldError = isFieldError;
        this.fieldErrorDtos = fieldErrorDtos;
        this.responseCode = responseCode;
    }
    //eric 20191219 end--
    public boolean isFieldError() {
        return isFieldError;
    }

    public void setFieldError(boolean fieldError) {
        isFieldError = fieldError;
    }

    public List<FieldErrorDto> getFieldErrorDtos() {
        return fieldErrorDtos;
    }

    public void setFieldErrorDtos(List<FieldErrorDto> fieldErrorDtos) {
        this.fieldErrorDtos = fieldErrorDtos;
    }

    //eric  20191219  -Start
    public ParamException(int responseCode, String responseMessage) {
        super(responseMessage);
        this.responseCode = responseCode;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }
    //eric  20191219  -End
}
