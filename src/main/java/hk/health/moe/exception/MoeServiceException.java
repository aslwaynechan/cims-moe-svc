package hk.health.moe.exception;

public class MoeServiceException extends Exception {

    private Throwable logThrowable;

    //eric  20191219  -Start
    private int responseCode;
    private String responseMessage;
    //eric  20191219  -End

    public Throwable getLogThrowable() {
        return logThrowable;
    }

    public void setLogThrowable(Throwable logThrowable) {
        this.logThrowable = logThrowable;
    }

    public MoeServiceException() {
        super();
    }

    public MoeServiceException(String message) {
        super(message);
    }

    public MoeServiceException(String message, Throwable cause) {
        super(message, cause);
        logThrowable = cause;
    }
    //eric  20191219  -Start
    public MoeServiceException(int responseCode, String responseMessage) {
        super(responseMessage);
        this.responseCode = responseCode;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }
    //eric  20191219  -End

}
