package hk.health.moe.common;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/7/31
 */
public class ModuleConstant {
    public static final String ON_EVEN_ODD = "on odd / even days";
    public static final String USE_INPUT_VALUE_N = "N";
    public static final String USE_INPUT_METHOD_DEVIDE = "/";
    public static final String USE_INPUT_METHOD_MULTIPLE = "*";
    public static final String USE_INPUT_METHOD_SUM = "+";
    public static final String USE_INPUT_METHOD_SUBTRAHEND = "-";
    public static final String DAY_OF_WEEK_SELECTED = "1";
    public static final String PER_DURATION_MULTIPLIER_Y = "Y";
    public static final String REGIMENT_TYPE_CYCLE = "C";

}
