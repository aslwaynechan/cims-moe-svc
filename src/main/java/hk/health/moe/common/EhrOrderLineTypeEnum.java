package hk.health.moe.common;

/**
 * @author Simon
 * @version 1.0
 * @date 2019/7/31
 */
public class EhrOrderLineTypeEnum {

    public static final String RECURRENT = "R";
    public static final String STEP_UP_DOWN = "S";
    public static final String NORMAL = "N";
    public static final String ADVANCED = "A";
    public static final String MULTIPLE_LINE = "M";
}
