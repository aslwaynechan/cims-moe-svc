package hk.health.moe.common;

public class ResponseCode {

    public enum CommonMessage {
        //Success
        SUCCESS(0, "Success."),
        //Fail
        TOKEN_EXPIRE_ERROR(1, "Token has expired, please log in again."),
        TOKEN_INVALID_ERROR(2, "User or password is invalid!"),
        PARAMETER_ERROR(3, "Invalid parameter!"),
        VERSION_ERROR(4, "Updated by others."),
        SYSTEM_ERROR(5,"System error.")
        ;

        private int responseCode;
        private String responseMessage;


        CommonMessage(int responseCode, String description) {
            this.responseCode = responseCode;
            this.responseMessage = description;
        }

        public int getResponseCode() {
            return responseCode;
        }

        public String getResponseMessage() {
            return responseMessage;
        }

    }

    //DAC  /listMDSEnquiries
    public enum ListMDSEnquiries {
        //Success
        SUCCESS(0, "Success."),
        //Fail
        TOKEN_EXPIRE_ERROR(1, "Token has expired, please log in again."),
        TOKEN_INVALID_ERROR(2, "User or password is invalid!"),
        PARAMETER_ERROR(3, "Invalid parameter!"),
        VERSION_ERROR(4, "Updated by others."),
        SYSTEM_ERROR(5,"System error."),

        GET_MDS_ERROR_WITHOUT_CAUSE(33, "Fail to call MDS enquiry."),
        GET_MDS_ERROR_WITH_CAUSE(34, "Fail to call MDS enquiry. Cause by: {0}"),

        RESOURCE_ACCESS_DENIED_ERROR(6, "Resource access denied.")
        ;

        private int responseCode;
        private String responseMessage;

        ListMDSEnquiries(int responseCode, String description) {
            this.responseCode = responseCode;
            this.responseMessage = description;
        }

        public int getResponseCode() {
            return responseCode;
        }

        public String getResponseMessage() {
            return responseMessage;
        }

    }


    //DAC  /medicationDecision
    public enum AllergyChecking {
        //Success
        SUCCESS(0, "Success."),
        //Fail
        TOKEN_EXPIRE_ERROR(1, "Token has expired, please log in again."),
        TOKEN_INVALID_ERROR(2, "User or password is invalid!"),
        PARAMETER_ERROR(3, "Invalid parameter!"),
        VERSION_ERROR(4, "Updated by others."),
        SYSTEM_ERROR(5,"System error."),

        ALLERGY_CHECKING_ERROR_WITHOUT_CAUSE(31, "Fail to call drug checking."),
        ALLERGY_CHECKING_ERROR_WITH_CAUSE(32, "Fail to call drug checking.Cause by: {0}"),

        RESOURCE_ACCESS_DENIED_ERROR(6, "Resource access denied.")
        ;

        private int responseCode;
        private String responseMessage;

        AllergyChecking(int responseCode, String description) {
            this.responseCode = responseCode;
            this.responseMessage = description;
        }

        public int getResponseCode() {
            return responseCode;
        }

        public String getResponseMessage() {
            return responseMessage;
        }

    }

    //eric 20191219 start--
    //moe orderMyFavouriteDetail
    public enum SpecialMessage {

        RECORDS_CONCURRENT_ERRORS(7,"Server Busy, Please Try Again Later."),
        RECORDS_NOT_EXISTED(8,"Records not existed"),
        MY_FAVOURITE_DELETED_BY_OTHERS(39,"My Favourite Deleted By Others."),
        MY_FAVOURITE_UPDATED_BY_OTHERS(40,"My Favourite Updated By Others."),

        MY_FAVOURITE_SORT_ERROR(42,"myfavourite sort error."),

        MOEORDER_DUPLICATE_MULTDOSENO(44,"MultDoseNo can not be duplicate!"),
        RECORDS_PRESCRIPTION_NOTEXISTED(45,"Prescription number not found."),
        MOEORDER_UPDATE_BY_OTHERS(46,"Moe Order Updated By Others"),
        SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALERT_POST_WITHCAUSE(47,"Fail to update patient summary. Case By: {0}{1}{2}{3}"),
        SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALERT_POST_NOCAUSE(48,"Fail to update patient summary."),
        SYSTEM_RESTSERVICE_SAAM_ERROR_ALLERGY_ADD(49,"Fail to save record."),
        SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_NOEXIST(50,"Patient's record dose not exist!"),
        SYSTEM_RESTSERVICE_SAAM_ERROR_ALLERGY_DUPLICATE(51,"An Allergy Record {1} for this Allergen already exists."),
        RECORDS_ALREADY_EXIST(52,"Records already exist!"),
        DATA_PROBLEM(53,"Data Problem.Case By: {0}"),
        MOEORDER_PATIENT_NOT_EXIST(54,"Patient Does Not Exist."),
        SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_SUMMARY_GET_NOCAUSE(55,"Fail to get patient summary."),
        SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_SUMMARY_GET_WITHCAUSE(56,"Fail to get patient summary.Cause by: {0}"),
        SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALLERGY_PUT_WITHCAUSE(57,"Fali to update patient allergy in bulk. Case By: {0}{1}{2}{3}"),
        SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALLERGY_PUT_NOCAUSE(58,"Fali to update patient allergy in bulk."),
        SYSTEM_RESTSERVICE_SAAM_ERROR_PATIENT_ALERT_DELETE_NOCAUSE(59,"Fail to delete patient alert.")
        ;

        private int responseCode;
        private String responseMessage;

        SpecialMessage(int responseCode, String description) {
            this.responseCode = responseCode;
            this.responseMessage = description;
        }

        public int getResponseCode() {
            return responseCode;
        }

        public String getResponseMessage() {
            return responseMessage;
        }

    }

    //eric 20191219 start--


}
