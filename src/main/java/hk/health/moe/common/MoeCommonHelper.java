package hk.health.moe.common;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import hk.health.moe.pojo.bo.AspectLogBo;
import hk.health.moe.pojo.dto.UserDto;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

public class MoeCommonHelper {

    public final static Logger logger = LoggerFactory.getLogger(MoeCommonHelper.class);

    public static boolean isFlagTrue(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.DB_FLAG_TRUE.equals(character.toUpperCase());
        }
    }

    public static boolean isFlagFalse(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.DB_FLAG_FALSE.equals(character.toUpperCase());
        }
    }

    public static boolean isAliasNameTypeVtm(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.ALIAS_NAME_TYPE_VTM.equals(character.toUpperCase());
        }
    }

    public static boolean isAliasNameTypeOther(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.ALIAS_NAME_TYPE_OTHER.equals(character.toUpperCase());
        }
    }

    public static boolean isAliasNameTypeBan(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.ALIAS_NAME_TYPE_BAN.equals(character.toUpperCase());
        }
    }

    public static boolean isAliasNameTypeAbb(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.ALIAS_NAME_TYPE_ABB.equals(character.toUpperCase());
        }
    }

    public static boolean isAliasNameTypeFreeText(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.ALIAS_NAME_TYPE_FREE_TEXT.equals(character.toUpperCase());
        }
    }

    public static boolean isAliasNameTypeLocalDrug(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.ALIAS_NAME_TYPE_LOCAL_DRUG.equals(character.toUpperCase());
        }
    }

    public static boolean isAliasNameTypeTradeNameAlias(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.ALIAS_NAME_TYPE_TRADE_NAME_ALIAS.equals(character.toUpperCase());
        }
    }

    public static boolean isDrugGenericIndYes(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.DRUG_GENERIC_IND_YES.equals(character.toUpperCase());
        }
    }

    public static boolean isStrengthCompulsoryYes(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.STRENGTH_COMPULSORY_YES.equals(character.toUpperCase());
        }
    }

    public static boolean isStrengthCompulsoryNo(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.STRENGTH_COMPULSORY_NO.equals(character.toUpperCase());
        }
    }

    public static boolean isDrugDosagePrnYes(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.DRUG_DOSAGE_PRN_YES.equals(character.toUpperCase());
        }
    }

    public static boolean isOrderLineTypeRecurrent(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.ORDER_LINE_TYPE_RECURRENT.equals(character.toUpperCase());
        }
    }

    public static boolean isOrderLineTypeStepUpDown(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.ORDER_LINE_TYPE_STEP_UP_DOWN.equals(character.toUpperCase());
        }
    }

    public static boolean isOrderLineTypeNormal(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.ORDER_LINE_TYPE_NORMAL.equals(character.toUpperCase());
        }
    }

    public static boolean isOrderLineTypeAdvanced(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.ORDER_LINE_TYPE_ADVANCED.equals(character.toUpperCase());
        }
    }

    public static boolean isOrderLineTypeMultipleLine(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.ORDER_LINE_TYPE_MULTIPLE_LINE.equals(character.toUpperCase());
        }
    }

    public static boolean isFormRouteDisplay(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.FORM_ROUTE_DISPLAY_YES.equals(character.toUpperCase());
        }
    }

    public static boolean isPrescTypeHomeLeave(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.PRESC_TYPE_HOME_LEAVE.equals(character.toUpperCase());
        }
    }

    public static boolean isPrescTypeDischarge(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.PRESC_TYPE_DISCHARGE.equals(character.toUpperCase());
        }
    }

    public static boolean isPrescTypeOutpatient(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.PRESC_TYPE_OUTPATIENT.equals(character.toUpperCase());
        }
    }

    public static boolean isActionTypePurchaseInCommunity(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.ACTION_TYPE_PURCHASE_IN_COMMUNITY.equals(character.toUpperCase());
        }
    }

    public static boolean isRegimentTypeWeekly(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.REGIMENT_TYPE_WEEKLY.equals(character.toUpperCase());
        }
    }

    public static boolean isRegimentTypeCycle(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.REGIMENT_TYPE_CYCLE.equals(character.toUpperCase());
        }
    }

    public static boolean isRegimentTypeMonthly(String character) {
        if (StringUtils.isBlank(character)) {
            return false;
        } else {
            return ServerConstant.REGIMENT_TYPE_MONTHLY.equals(character.toUpperCase());
        }
    }

    public static String stringToHex(byte[] contentInByte) {
        char[] hexChars = "0123456789abcdef".toCharArray();
        char[] chars = new char[2 * contentInByte.length];
        for (int i = 0; i < contentInByte.length; ++i) {
            chars[2 * i] = hexChars[(contentInByte[i] & 0xF0) >>> 4];
            chars[2 * i + 1] = hexChars[contentInByte[i] & 0x0F];
        }
        return new String(chars);
    }

    public final static boolean isNumber(String number) {
        if (!StringUtils.isBlank(number)) {
            return number.matches(ServerConstant.REGEX_NUMBER);
        }
        return false;
    }

    // Ricci 20190823 moe_drug_server start --
    public static boolean isSame(String s1, String s2) {
        if (s1 != null && s2 != null) {
            return s1.equals(s2);
        } else if (s1 == null && s2 == null) {
            return true;
        } else {
            return false;
        }
    }
    // Ricci 20190823 moe_drug_server end --

    // Ricci 20191021 start --
    public static String readBytes(byte[] buf, String characterEncoding) {
        if (buf.length > 0) {
            String payload;
            try {
                payload = new String(buf, 0, buf.length, characterEncoding);
            } catch (UnsupportedEncodingException e) {
                payload = "[unknown]";
            }
            return payload.replaceAll("\\n", "");
        } else {
            return "";
        }
    }

    public static AspectLogBo buildAspectLog(AspectLogBo aspectLogBo, ContentCachingRequestWrapper requestWrapper) {

        StringBuffer sbBrowser = new StringBuffer();

        // Requester Browser and OSaspectLogBo
        String ua = requestWrapper.getHeader("user-agent");
        UserAgent userAgent = UserAgent.parseUserAgentString(ua);
        Browser browser = userAgent.getBrowser();
        OperatingSystem os = userAgent.getOperatingSystem();
        String strBrowser = browser != null ?
                sbBrowser.append(browser.getName()).append(" ").append(browser.getVersion(ua)).toString() : sbBrowser.toString();
        String strOs = os != null ? os.getName() : "";
        aspectLogBo.setRemoteBrowser(strBrowser);
        aspectLogBo.setRemoteOs(strOs);

        // Service hostname
        InetAddress inetAddress = null;
        try {
            inetAddress = InetAddress.getLocalHost();
            aspectLogBo.setServiceHostname(inetAddress.getHostName());
        } catch (UnknownHostException e) {
        }

        // Requester IP
        aspectLogBo.setRemoteIp(requestWrapper.getRemoteAddr());

        // Requester Http Method
        aspectLogBo.setHttpMethod(requestWrapper.getMethod());

        // Request URL
        aspectLogBo.setRequestUrl(requestWrapper.getRequestURL().toString());

        // Requester Body
        aspectLogBo.setRequestBody(MoeCommonHelper.readBytes(requestWrapper.getContentAsByteArray(), requestWrapper.getCharacterEncoding()));

        // Requester encoding
        aspectLogBo.setCharacterEncoding(requestWrapper.getCharacterEncoding());

        return aspectLogBo;
    }

    public static void buildAspectLog(AspectLogBo aspectLogBo, StopWatch timer, ContentCachingResponseWrapper responseWrapper) {

        // Response Body
        aspectLogBo.setResponseBody(readBytes(responseWrapper.getContentAsByteArray(), responseWrapper.getCharacterEncoding()));

        // Response Status Code
        aspectLogBo.setResponseStatusCode(responseWrapper.getStatusCode());

        // Timer
        aspectLogBo.setStartDtm(new Date(timer.getStartTime()));
        aspectLogBo.setEndDtm(new Date(timer.getStartTime() + timer.getTime()));
        Float getTime = Float.valueOf(timer.getTime());
        aspectLogBo.setDuration(getTime / 1000 + " s");
    }
    // Ricci 20191021 end --

    // Ricci 20191108 start --
    public static void buildUserInfo(Object targetData, Object createInfo, Object upSerInfo, Date now) /*throws Exception*/ {
        Date createDate = null;
        Class dataClazz = targetData.getClass();

        Method createUserIdSetter = getSetMethod("createUserId", dataClazz, false);
        Method createUserSetter = getSetMethod("createUser", dataClazz, false);
        Method createHospSetter = getSetMethod("createHosp", dataClazz, false);
        Method createRankSetter = getSetMethod("createRank", dataClazz, false);
        Method createRankDescSetter = getSetMethod("createRankDesc", dataClazz, false);
        Method createDateSetter = getSetMethod("createDate", dataClazz, false);

        Method updateUserIdSetter = getSetMethod("updateUserId", dataClazz, false);
        Method updateUserSetter = getSetMethod("updateUser", dataClazz, false);
        Method updateHospSetter = getSetMethod("updateHosp", dataClazz, false);
        Method updateRankSetter = getSetMethod("updateRank", dataClazz, false);
        Method updateRankDescSetter = getSetMethod("updateRankDesc", dataClazz, false);
        Method updateDateSetter = getSetMethod("updateDate", dataClazz, false);

        createInfo = createInfo != null ? createInfo : upSerInfo;
        Class createInfoClazz = createInfo.getClass();
        Method createUserIdGetter = null;
        Method createUserGetter = null;
        Method createHospGetter = null;
        Method createRankGetter = null;
        Method createRankDescGetter = null;
        if (createInfo instanceof UserDto) {
            createUserIdGetter = getSetMethod("loginId", createInfoClazz, true);
            createUserGetter = getSetMethod("loginName", createInfoClazz, true);
            createHospGetter = getSetMethod("hospitalCd", createInfoClazz, true);
            createRankGetter = getSetMethod("userRankCd", createInfoClazz, true);
            createRankDescGetter = getSetMethod("userRankDesc", createInfoClazz, true);
            createDate = now;
        } else {
            createUserIdGetter = getSetMethod("createUserId", createInfoClazz, true);
            createUserGetter = getSetMethod("createUser", createInfoClazz, true);
            createHospGetter = getSetMethod("createHosp", createInfoClazz, true);
            createRankGetter = getSetMethod("createRank", createInfoClazz, true);
            createRankDescGetter = getSetMethod("createRankDesc", createInfoClazz, true);
            createDate = getterMethodInvoke(getSetMethod("createDate", createInfoClazz, true), createInfo, Date.class);
        }

        Class upSerInfoClazz = upSerInfo.getClass();
        Method updateUserIdGetter = null;
        Method updateUserGetter = null;
        Method updateHospGetter = null;
        Method updateRankGetter = null;
        Method updateRankDescGetter = null;
        if (upSerInfo instanceof UserDto) {
            updateUserIdGetter = getSetMethod("loginId", upSerInfoClazz, true);
            updateUserGetter = getSetMethod("loginName", upSerInfoClazz, true);
            updateHospGetter = getSetMethod("hospitalCd", upSerInfoClazz, true);
            updateRankGetter = getSetMethod("userRankCd", upSerInfoClazz, true);
            updateRankDescGetter = getSetMethod("userRankDesc", upSerInfoClazz, true);

        } else {
            updateUserIdGetter = getSetMethod("updateUserId", upSerInfoClazz, true);
            updateUserGetter = getSetMethod("updateUser", upSerInfoClazz, true);
            updateHospGetter = getSetMethod("updateHosp", upSerInfoClazz, true);
            updateRankGetter = getSetMethod("updateRank", upSerInfoClazz, true);
            updateRankDescGetter = getSetMethod("updateRankDesc", upSerInfoClazz, true);
        }

        getSetMethodInvoke(createUserIdSetter, targetData, createUserIdGetter, createInfo);
        getSetMethodInvoke(createUserSetter, targetData, createUserGetter, createInfo);
        getSetMethodInvoke(createHospSetter, targetData, createHospGetter, createInfo);
        getSetMethodInvoke(createRankSetter, targetData, createRankGetter, createInfo);
        getSetMethodInvoke(createRankDescSetter, targetData, createRankDescGetter, createInfo);
        setterMethodInvoke(createDateSetter, targetData, createDate);

        getSetMethodInvoke(updateUserIdSetter, targetData, updateUserIdGetter, upSerInfo);
        getSetMethodInvoke(updateUserSetter, targetData, updateUserGetter, upSerInfo);
        getSetMethodInvoke(updateHospSetter, targetData, updateHospGetter, upSerInfo);
        getSetMethodInvoke(updateRankSetter, targetData, updateRankGetter, upSerInfo);
        getSetMethodInvoke(updateRankDescSetter, targetData, updateRankDescGetter, upSerInfo);
        setterMethodInvoke(updateDateSetter, targetData, now);
    }
    // Ricci 20191108 end --

    // Ricci 20191111 start -- handle IntrospectionException, Solution is skip...
    private static Method getSetMethod(String propertyName, Class<?> beanClass, boolean isGetter) {

        Method method = null;
        PropertyDescriptor pd = null;
        try {
            pd = new PropertyDescriptor(propertyName, beanClass);
            if (isGetter) {
                method = pd.getReadMethod();
            } else {
                method = pd.getWriteMethod();
            }
        } catch (IntrospectionException ignore) {
            logger.warn(ignore.getMessage());
        }

        return method;
    }

    private static <T> T getterMethodInvoke(Method getter, Object getterTarget, Class<T> argumentClazz) {
        if (null != getter) {
            T result = null;
            try {
                Object value = getter.invoke(getterTarget);
                if (value.getClass().equals(argumentClazz)) {
                    result = (T) value;
                } else {
                    Class<?> superclass = value.getClass().getSuperclass();
                    while (superclass != null) {
                        if (superclass.isAssignableFrom(Object.class)) {
                            break;
                        } else if (superclass.equals(argumentClazz)) {
                            result = (T) value;
                            break;
                        } else {
                            superclass = superclass.getSuperclass();
                        }
                    }
                }
            } catch (IllegalAccessException | InvocationTargetException ignore) {
                logger.warn(ignore.getMessage());
            }

            return result;
        } else {
            return null;
        }
    }

    private static void setterMethodInvoke(Method setter, Object setterTarget, Object argument) {
        if (null != setter) {
            try {
                setter.invoke(setterTarget, argument);
            } catch (IllegalAccessException | InvocationTargetException ignore) {
                logger.warn(ignore.getMessage());
            }
        }
    }

    private static void getSetMethodInvoke(Method setter, Object setterTarget, Method getter, Object getterTarget) {
        if (null != setter && null != getter) {
            Object value = getterMethodInvoke(getter, getterTarget, getter.getReturnType());
            setterMethodInvoke(setter, setterTarget, value);
        }
    }
    // Ricci 20191111 end --


}
