/*********************************************************************
* Copyright (c) eHR HK Limited 2013. 
* All Rights Reserved.
* 
*********************************************************************
* PROJECT NAME    :  eHR Adaptation
*
* MODULE NAME     :  Medication Order Entry (MOE)
*
* PROGRAM NAME    :  WebServiceConstant.java
*
* PURPOSE         :  Represents the variable that never changes in the application
*
**********************************************************************
* VERSION         :   1.01.001
* REF NO          :  
* DATE CREATED    :   7 June 2013
* 
*********************************************************************
*
* CHANGES         :
* REF NO           DATE        DETAIL
* ------           ----        ------
*
*********************************************************************
*/
package hk.health.moe.common;

public class WebServiceConstant {
	
	public static final String ACTION_INSERT = "I";
	public static final String ACTION_DELETE = "D";
	public static final String ACTION_UPDATE = "U";
	
	public static final String WS_MISSING_HOSPCODE_ERR_CODE = "1";
	public static final String WS_MISSING_HOSPCODE_ERR_MSG = "Hospital Code is null or empty";	
	public static final String WS_MISSING_ORDER_NUMBER_ERR_CODE = "2";
	public static final String WS_MISSING_ORDER_NUMBER_ERR_MSG = "Order Number is null or empty";	
	public static final String WS_SYSTEM_ERR_CODE = "3";
	public static final String WS_SYSTEM_ERR_MSG = "Internal server error.";	
	public static final String WS_AUTHENTICATION_ERR_CODE = "4";
	public static final String WS_AUTHENTICATION_ERR_MSG = "Web service authentication fail. Password does not match.";	
	public static final String WS_MISSING_PATIENT_ALLERGY_ERR_CODE = "5";
	public static final String WS_MISSING_PATIENT_ALLERGY_ERR_MSG = "Patient Allergy is null or empty";
	public static final String WS_MISSING_PATIENT_PRESCRIPTION_ERR_CODE = "6";
	public static final String WS_MISSING_PATIENT_PRESCRIPTION_ERR_MSG = "Patient Prescription is null or empty";
	public static final String WS_MISSING_PATIENT_KEY_ERR_CODE = "7";
	public static final String WS_MISSING_PATIENT_KEY_ERR_MSG = "Patient Key is null or empty";
	public static final String WS_MISSING_EPISODE_NO_ERR_CODE = "8";
	public static final String WS_MISSING_EPISODE_NO_ERR_MSG = "Episode No is null or empty";
	public static final String WS_MISSING_EPISODE_TYPE_ERR_CODE = "9";
	public static final String WS_MISSING_EPISODE_TYPE_ERR_MSG = "Episode Type is null or empty";
	public static final String WS_REQUEST_PARAM_ERR_CODE = "10";
	public static final String WS_REQUEST_PARAM_ERR_MSG = "Invalid parameter";
	public static final String WS_ACCESS_DENIED_ERR_CODE = "11";
	public static final String WS_ACCESS_DENIED_ERR_MSG = "You are not authorized to access this method.";
	public static final String WS_APPLICATION_EXPIRED_ERR_CODE = "12";
	public static final String WS_APPLICATION_EXPIRED_ERR_MSG = "Application has been expired.";	
	public static final String WS_MISSING_DRUG_ERR_CODE = "13";
	public static final String WS_MISSING_DRUG_ERR_MSG = "Drug attribute not match with HKCTT.";
	public static final String WS_MISSING_DATE_ERR_CODE = "14";
	public static final String WS_MISSING_DATE_ERR_MSG = "Start and End Date not correct / empty";
	public static final String WS_ALIAS_NAME_ERR_CODE = "15";
	public static final String WS_ALIAS_NAME_ERR_MSG = "Not allow add alias name in local drug";
	public static final String WS_INVALID_DRUG_KEY_ERR_CODE = "16";
	public static final String WS_INVALID_DRUG_KEY_ERR_MSG = "Local drug key not found";

	// Simon 20191119 start--
    public static final String WS_MISSING_LOGIN_ID_ERR_CODE = "17";
    public static final String WS_MISSING_LOGIN_ID_ERR_MSG = "Login id is null or empty";
    public static final String WS_MISSING_USER_RANK_ERR_CODE = "18";
    public static final String WS_MISSING_USER_RANK_ERR_MSG = "User rank is null or empty";
    //Simon 20191119 end--

	public static final String MESSAGES_AND = "and";
	public static final String MESSAGES_THEN = "then";
	public static final String ON_EVEN_ODD = "on odd / even days";
	public static final String BLANKS_IND = "__";
	public static final String DOSAGE_FORMAT = "#,##0.####";
	public static final String DOSAGE_FORMAT2 = "#0.####";
}
