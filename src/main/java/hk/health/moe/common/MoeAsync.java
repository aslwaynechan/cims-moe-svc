package hk.health.moe.common;

/**************************************************************************
 * NAME        : MoeAsync.java
 * VERSION     : 1.0.0
 * DATE        : 03-JUL-2019
 * DESCRIPTION : Async function for MOE
 * MODIFICATION HISTORY
 * Name             Date         Description
 * ===============  ===========  =======================================
 * Ricci Liao       03-JUL-2019  Initial Version
 **************************************************************************/

import hk.health.moe.exception.MoeServiceException;
import hk.health.moe.pojo.bo.AspectLogBo;
import hk.health.moe.service.MoeContentService;
import hk.health.moe.service.RestTemplateService;
import hk.health.moe.util.LocaleMessageSourceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.concurrent.locks.ReentrantLock;

@Component
public class MoeAsync {
    public final static Logger logger = LoggerFactory.getLogger(MoeAsync.class);

    @Autowired
    private RestTemplateService restTemplateService;

    @Autowired
    private MoeContentService moeContentService;

    @Autowired
    private ReentrantLock refreshDrugListLock;

    @Autowired
    private LocaleMessageSourceUtil localeMessageSourceUtil;

    /*@Async
    public void logTask(Map<String, Object> logMap) {

        try {
            SystemLogDto systemLogDto = new SystemLogDto();
            systemLogDto.setDate((Date) logMap.get("date"));
            systemLogDto.setFunctionName((String) logMap.get("functionName"));
            systemLogDto.setIp((String) logMap.get("ip"));
            systemLogDto.setSid((String) logMap.get("sid"));
            systemLogDto.setBrowser((String) logMap.get("browser"));
            systemLogDto.setOs((String) logMap.get("os"));
            systemLogDto.setServerHostName((String) logMap.get("serverHostName"));
            systemLogDto.setException((Exception) logMap.get("exception"));
            systemLogDto.setType(null == logMap.get("exception") ? "S" : "F");
            systemLogDto.setClinicCd((String) logMap.get("clinicCd"));
            systemLogDto.setUserId((String) logMap.get("userId"));

            restTemplateService.writeSystemLog(systemLogDto);
        } catch (Exception e) {
            logger.error("Writing Exception", e);
        }
    }*/

    @Async
    public void refreshDrugListTask() throws Exception {

        try {
            refreshDrugListLock.lock();
            moeContentService.refreshDrugList();
        } catch (Exception e) {
            throw new MoeServiceException(localeMessageSourceUtil.getMessage("system.refresh.drugList.error"));
        } finally {
            refreshDrugListLock.unlock();
        }
    }

    @Async
    public void buildAspectLog(AspectLogBo aspectLogBo) {
        logger.info(aspectLogBo.toLogString());
        //System.out.println(aspectLogBo.toLogString());
    }

}
