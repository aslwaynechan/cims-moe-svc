/*********************************************************************
 * Copyright (c) eHR HK Limited 2013.
 * All Rights Reserved.
 *
 *********************************************************************
 * PROJECT NAME    :  eHR Adaptation
 *
 * MODULE NAME     :  Medication Order Entry (MOE)
 *
 * PROGRAM NAME    :  ServerConstant.java
 *
 * PURPOSE         :  Represents the variable that never changes in the application
 *
 **********************************************************************
 * VERSION         :   1.01.001
 * REF NO          :
 * DATE CREATED    :   7 June 2013
 *
 *********************************************************************
 *
 * CHANGES         :
 * REF NO           DATE        DETAIL
 * ------           ----        ------
 *
 *********************************************************************
 */
package hk.health.moe.common;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ServerConstant {

    public static final String APP_MY_FAV = "MOE_MY_FAV_MAIN";

    public static final String ROLE_ALL = "ROLE_ALL";

    public static final String FUNCTION_ID_MOE_MODULE = "00004";
    public static final String FUNCTION_ID_ALERT_MODULE = "00001";

    public static final String SUCCESS_ERRCODE = FUNCTION_ID_MOE_MODULE + "-I-001";
    public static final String ERROR_DEFAULT_ERRCODE = FUNCTION_ID_MOE_MODULE + "-N-001";
    public static final String PATIENT_NOT_FOUND_ERRCODE = FUNCTION_ID_MOE_MODULE + "-I-0101";
    public static final String MOE_ALERT_RECORD_UPDATED_BY_OTHERS_ERRCODE = FUNCTION_ID_MOE_MODULE + "-W-057";
    public static final String MOE_ALERT_RECORD_DELETED_BY_OTHERS_ERRCODE = FUNCTION_ID_MOE_MODULE + "-W-058";
    public static final String ALERT_SUCCESS_ERRCODE = FUNCTION_ID_ALERT_MODULE + "-I-001";
    public static final String ALERT_RECORD_UPDATED_BY_OTHERS_ERRCODE = FUNCTION_ID_ALERT_MODULE + "-W-015";
    public static final String ALERT_RECORD_DELETED_BY_OTHERS_ERRCODE = FUNCTION_ID_ALERT_MODULE + "-W-020";

    public static final String DB_FAIL_TO_RETRIEVE_RECORD_ERRCODE = FUNCTION_ID_MOE_MODULE + "-D-001";
    public static final String DB_FAIL_TO_SAVE_RECORD_ERRCODE = FUNCTION_ID_MOE_MODULE + "-D-002";
    public static final String DB_FAIL_TO_DELETE_RECORD_ERRCODE = FUNCTION_ID_MOE_MODULE + "-D-003";
    public static final String DB_FAIL_TO_PRINT_RECORD_ERRCODE = FUNCTION_ID_MOE_MODULE + "-D-004";
    public static final String DB_FAIL_RECORD_UPDATED_BY_OTHERS = FUNCTION_ID_MOE_MODULE + "-D-005";
    public static final String DB_FAIL_RECORD_DELETED_BY_OTHERS = FUNCTION_ID_MOE_MODULE + "-D-006";

    public static final float SPELL_CHECKER_ACCURACY = .7f;

    public static final String QUESTION_UNSAVE_ERRCODE = FUNCTION_ID_MOE_MODULE + "-Q-001";
    public static final String QUESTION_CHECK_NO_KNOWN_DRUG_ALLERGY_CHECKBOX_ERRCODE = FUNCTION_ID_MOE_MODULE + "-Q-002";

    public static final String WARN_MISS_INPUT_ALLERGY = FUNCTION_ID_MOE_MODULE + "-W-001";
    public static final String WARN_MISS_ALLERGY_MANIFEST = FUNCTION_ID_MOE_MODULE + "-W-002";
    public static final String WARN_MISS_ALLERGY_CERTAINTY = FUNCTION_ID_MOE_MODULE + "-W-003";
    public static final String WARN_MISS_INPUT_ADR = FUNCTION_ID_MOE_MODULE + "-W-004";
    public static final String WARN_MISS_SPECIFY_REACTION = FUNCTION_ID_MOE_MODULE + "-W-005";
    public static final String WARN_MISS_SPECIFY_LVL_SERVERITY = FUNCTION_ID_MOE_MODULE + "-W-006";
    public static final String WARN_MISS_SPECIFY_REMARKS_REASON = FUNCTION_ID_MOE_MODULE + "-W-007";
    public static final String WARN_MISS_SPECIFY_REASON = FUNCTION_ID_MOE_MODULE + "-W-008";
    public static final String PATIENT_ALLERGY_ADD_DUPLICATE_ERRCODE = FUNCTION_ID_MOE_MODULE + "-W-009";
    public static final String PATIENT_ADR_ADD_DUPLICATE_ERRCODE = FUNCTION_ID_MOE_MODULE + "-W-010";
    public static final String WARN_MISS_INPUT_ALERT = FUNCTION_ID_MOE_MODULE + "-W-011";
    public static final String PATIENT_ALERT_ADD_DUPLICATE_ERRCODE = FUNCTION_ID_MOE_MODULE + "-W-012";
    public static final String WARN_DATE_RANGE_ERRCODE = FUNCTION_ID_MOE_MODULE + "-W-013";
    public static final String WARN_MISS_SPECIFY_ADR_INFO = FUNCTION_ID_MOE_MODULE + "-W-014";
    public static final String PATIENT_ALLERGY_UPDATE_LOCK_ERRCODE = FUNCTION_ID_MOE_MODULE + "-W-015";
    public static final String PATIENT_ADR_UPDATE_LOCK_ERRCODE = FUNCTION_ID_MOE_MODULE + "-W-015";
    public static final String PATIENT_ALERT_UPDATE_LOCK_ERRCODE = FUNCTION_ID_MOE_MODULE + "-W-015";
    public static final String WARN_INVALID_DATE_ERRCODE = FUNCTION_ID_MOE_MODULE + "-W-016";
    public static final String WARN_EXCEED_MAX_ADD_INFO_LENGTH = FUNCTION_ID_MOE_MODULE + "-W-017";
    public static final String WARN_EXCEED_MAX_REASON_LENGTH = FUNCTION_ID_MOE_MODULE + "-W-018";
    public static final String WARN_MISS_SPECIFY_CM_INFO = FUNCTION_ID_MOE_MODULE + "-W-019";
    public static final String RECORD_NOT_FOUND = FUNCTION_ID_ALERT_MODULE + "-W-020";
    public static final String WARN_INVALID_ALERT_END_DATE = FUNCTION_ID_MOE_MODULE + "-W-021";
    public static final String WARN_INVALID_ALERT_START_DATE = FUNCTION_ID_MOE_MODULE + "-W-022";
    public static final String WARN_FALL_CALL_DRUG_CHECKING = FUNCTION_ID_MOE_MODULE + "-W-023";
    //	public static final String WARN_INVALID_INPUT_CHAR = FUNCTION_ID_MOE_MODULE + "-W-023";
    public static final String WARN_EXCEED_MAX_SEARCH_LENGTH = FUNCTION_ID_MOE_MODULE + "-W-024";
    public static final String WARN_NO_KNOWN_DRUG_RESERVED_WORD = FUNCTION_ID_MOE_MODULE + "-W-025";
    public static final String WARN_HAS_DRUG_ALLERGY_RECORD = FUNCTION_ID_MOE_MODULE + "-W-026";
    public static final String WARN_PATIENT_NOT_FOUND = FUNCTION_ID_MOE_MODULE + "-W-041";
    public static final String WARN_HOSPITAL_SETTING_NOT_FOUND = FUNCTION_ID_MOE_MODULE + "-W-042";
    public static final String WARN_FAIL_CALL_SERVICE = FUNCTION_ID_MOE_MODULE + "-W-045";

    public static final String APP_INIT_ERRCODE = FUNCTION_ID_MOE_MODULE + "-A-001";
    public static final String APP_INIT_ERRMSG = "Initialization Error.";

    public static final String WARN_INVALID_PARAM = FUNCTION_ID_MOE_MODULE + "-W-001";

    public static final String APP_VERSION_ERRCODE = FUNCTION_ID_MOE_MODULE + "-A-004";
    public static final String APP_EPISODE_DISCHARGED = FUNCTION_ID_MOE_MODULE + "-A-005";
    public static final String APP_EPISODE_TYPE_CHANGED = FUNCTION_ID_MOE_MODULE + "-A-002";
    public static final String APP_SERVICE_UNAUTHORIZED = FUNCTION_ID_MOE_MODULE + "-A-006";
    public static final String APP_EXPIRY_ERRCODE = FUNCTION_ID_MOE_MODULE + "-A-009";

    public static final String APP_AUTHENTICATION_ERRCODE = FUNCTION_ID_MOE_MODULE + "-N-003";

    public static final String DB_FLAG_ACTIVE = "A";
    public static final String DB_FLAG_REASONT_TYPE_ALLERGY = "G";
    public static final String DB_FLAG_REASONT_TYPE_ADR = "D";
    public static final String DB_FLAG_TRUE = "Y";
    public static final String DB_FLAG_FALSE = "N";

    public static final String PRODUCTION = "PRODUCTION";
    public final static byte[] AES_KEY = new byte[]{'H', 'o', 'S', 'P', 'i', 'T', 'a', 'L', 'A', 'u', 'T', 'h', 'O', 'r', 'I', 't'};
    //public static final String JASYPT_ALOGORITHM = "PBEWithMD5AndDES";
    public static final String JASYPT_ALOGORITHM = "uDAwfCK4OVl1NCqmov-bYpk-7zkZ1-t7ykX3r_sBycw";
    //public static final String JASYPT_PASSWORD = "HaITsCe3";
    public static final String JASYPT_KEY = "tO1ear62JFUnA8R2ws8BOw";
    //public static final String JASYPT_SALT = "s@1tV@!ue";
    public static final String JASYPT_SALT = "D85cEWnP9u8WV-uv9ho8Dg";

    public static final String ALIAS_NAME_TYPE_VTM = "V";
    public static final String ALIAS_NAME_TYPE_OTHER = "O";
    public static final String ALIAS_NAME_TYPE_BAN = "B";
    public static final String ALIAS_NAME_TYPE_ABB = "A";
    public static final String ALIAS_NAME_TYPE_FREE_TEXT = "F";
    public static final String ALIAS_NAME_TYPE_LOCAL_DRUG = "L";
    public static final String ALIAS_NAME_TYPE_TRADE_NAME_ALIAS = "N";

    public static final String DRUG_GENERIC_IND_YES = "Y";
    public static final String STRENGTH_COMPULSORY_YES = "Y";
    public static final String STRENGTH_COMPULSORY_NO = "N";
    public static final String DRUG_FREQ_PARAM_REGEX = "__";

    public static final String DRUG_DOSAGE_PRN_YES = "Y";
    public static final String DRUG_DOSAGE_PRN_STRING = "as required";

    public static final String ORDER_LINE_TYPE_RECURRENT = "R";
    public static final String ORDER_LINE_TYPE_STEP_UP_DOWN = "S";
    public static final String ORDER_LINE_TYPE_NORMAL = "N";
    public static final String ORDER_LINE_TYPE_ADVANCED = "A";
    public static final String ORDER_LINE_TYPE_MULTIPLE_LINE = "M";

    /**
     * Alert Allergy
     **/
    public static final String CLASSIFIED_INDICATOR_Y = "Y";
    public static final String CLASSIFIED_INDICATOR_N = "N";
    public static final String CLASSIFIED_INDICATOR_NULL = "-";

    // Data Import Type
    public static final String IMPORT_ALLERGY = "allergy";
    public static final String IMPORT_ADR = "adr";
    public static final String IMPORT_ALERT = "alert";
    public static final String IMPORT_FREE_TEXT = "C";
    public static final String IMPORT_STRUCTURED_DATA = "S";
    public static final Integer INSERT_RECORD_ITEM_TOTAL = 7;
    public static final String CERTAINTY_CERTAIN_CODE = "C";
    public static final String CERTAINTY_SUSPECTED_CODE = "S";
    public static final String SEVERITY_MILD_CODE = "M";
    public static final String SEVERITY_SEVERE_CODE = "S";

    public static final String WS_ACTION_TYPE_I = "I";
    public static final String WS_ACTION_TYPE_D = "D";

    /**
     * Web Service Exception Error Code
     **/
    public static final String WS_PATIENT_EMPTY_ERR_CODE = "1";
    public static final String WS_PATIENT_EMPTY_ERR_MSG = "Patient reference key is null or empty.";
    public static final String WS_SYSTEM_ERR_CODE = "2";
    public static final String WS_SYSTEM_ERR_MSG = "Internal server error.";
    public static final String WS_AUTHENTICATION_ERR_CODE = "3";
    public static final String WS_AUTHENTICATION_ERR_MSG = "Web service authentication fail. Password not match.";
    // Simon 20191115 start--
    public static final String WS_AUTHENTICATION_ERR_NOT_EXIST_MSG = "Web service authentication fail. User not exist.";
    //Simon 20191115 end--
    // log4j file appender
    public static final String LOG4J_PARAM_FILE_PATH = "log_path";
    public static final String LOG4J_PARAM_LOG_LEVEL_HA = "log_level_ha";
    public static final String LOG4J_PARAM_LOG_LEVEL_SPRING = "log_level_spring";

    // Allergen Parameter
    public static final String DANGER_DRUG = "DDI";
    public static final String CARBAMAZEPINE = "carbamazepine";
    public static final String MATCH_TYPE_ALLERGEN = "A";
    public static final String MATCH_TYPE_CARBAMAZEPINE = "C";
    public static final String MATCH_MANIFEST_OTHERS = "others";

    // Display Name Type
    public static final String DISPLAY_NAME_TYPE_VTM = "V";
    public static final String DISPLAY_NAME_TYPE_TRADENAME = "T";
    public static final String DISPLAY_NAME_TYPE_OTHER = "O";
    public static final String DISPLAY_NAME_TYPE_BAN = "B";
    public static final String DISPLAY_NAME_TYPE_ABB = "A";
    public static final String DISPLAY_NAME_TYPE_ALLERGENGROUP = "G";
    public static final String DISPLAY_NAME_TYPE_TRADENAMEALIAS = "N";

    // Allergen type
    public static final String ALLERGEN_TYPE_FREE_TEXT_DRUG = "O";
    public static final String ALLERGEN_TYPE_FREE_TEXT_NON_DRUG = "X";
    public static final String ALLERGEN_TYPE_FREE_TEXT_IMPORT = "C";
    public static final String ALLERGEN_TYPE_STRUCTURE_IMPORT = "S";


    // TRX Type
    public static final String TRX_TYPE_INSERT = "I";
    public static final String TRX_TYPE_UPDATE = "U";
    public static final String TRX_TYPE_DELETE = "D";

    // Parameter Length
    public static final Integer ENCOUNTER_NO_MAX_LENGTH = 20;
    public static final Integer MRN_MAX_LENGTH = 50;
    public static final Integer SYSTEM_LOGIN_MAX_LENGTH = 100;
    public static final Integer LOGIN_MAX_LENGTH = 50;
    public static final Integer USER_RIGHT_MAX_LENGTH = 1;
    public static final Integer HOSPITAL_CODE_MAX_LENGTH = 20;
    public static final Integer SOURCE_SYSTEM_MAX_LENGTH = 5;
    public static final Integer USER_RANK_MAX_LENGTH = 10;
    public static final Integer USER_RANK_DESC_MAX_LENGTH = 48;
    public static final Integer LOGIN_NAME_MAX_LENGTH = 255;

    // Setting System Parameter Name
    public static final String SETTING_ENABLED = "Y";
    public static final String SETTING_DISABLED = "N";
    public static final String PARAM_NAME_SAAM_WS_ADDR = "saam_ws_address";
    public static final String PARAM_NAME_SAAM_WS_USERNAME = "saam_ws_username";
    public static final String PARAM_NAME_SAAM_WS_PWKEY = "saam_ws_password";
    public static final String PARAM_NAME_SAAM_SOURCE_SYSTEM = "saam_ws_source_system";
    // public static final String PARAM_NAME_ENABLE_DID_YOU_MEAN 		= "enable_did_you_mean";
    public static final String PARAM_NAME_ENABLE_REPORT_PRINT = "enable_report_print";
    public static final String PARAM_NAME_ENABLE_RX_PREVIEW = "enable_rx_preview";
    public static final String PARAM_NAME_ENABLE_AUDIT_LOG = "enable_audit_log";
    public static final String PARAM_NAME_HOSPITAL_NAME_ENG = "hospital_name_eng";
    public static final String PARAM_NAME_ENABLE_DEPARTMENTAL_FAV = "enable_departmental_my_favourite";
    public static final String PARAM_NAME_ENABLE_RX_SHARE_GROUP = "enable_rx_share_group";
    public static final String PARAM_NAME_ENABLE_MULTIPLE_DISCHARGE = "enable_multiple_discharge";
    public static final String TRIAL_EXPIRY_DATE = "trial_expiry_date";
    public static final String PARAM_NAME_ENCRYPT_AUDIT_LOG = "encrypt_audit_log";
    public static final String PARAM_NAME_ORDER_NUMBER_PREFIX = "order_number_prefix";
    public static final String PARAM_NAME_DEFAULT_DRUG_HISTORY_PERIOD = "default_drug_history_period";
    public static final String PARAM_NAME_MDS_WS_ADDR = "mds_ws_address";
    public static final String PARAM_NAME_MDS_WS_USERNAME = "mds_ws_username";
    public static final String PARAM_NAME_MDS_WS_PWKEY = "mds_ws_password";
    public static final String PARAM_NAME_ENABLE_MDS = "enable_mds";
    public static final String PARAM_NAME_ENABLE_PROFILING = "enable_profiling";
    public static final String PARAM_NAME_PRINT_DOC_VIEWER_PATH = "print_doc_viewer_path";
    public static final String PARAM_NAME_PRINT_DOC_VIEWER_LOCAL_PATH = "print_doc_viewer_local_path";
    public static final String PARAM_NAME_PRINT_OUTPUT_FORMAT = "print_output_format";

    public static final String PARAM_NAME_CS_SOURCE_SYSTEM = "cs_ws_source_system";
    public static final String PARAM_NAME_ENABLE_SAME_ROUTE_SAME_DRUG_CHECKING = "enable_same_route_site_same_drug_checking";

    public static final String PARAM_NAME_ENABLE_CARRY_FORWARD_QTY = "enable_carry_forward_qty";

    public static final String PARAM_NAME_ENABLE_COMMON_DOSAGE = "enable_common_dosage";

    public static final String ENABLE_ASSIGN_SPECIALTY_INTO_MY_FAVOURITE = "enable_assign_specialty_into_my_favourite";
    // Session Attributes
    public static final String SESS_ATTR_USER_INFO = "userDTO";
    public static final String SESS_ATTR_AUDIT_INFO = "auditDTO";
    public static final String SESS_ATTR_ALLERGY_INFO = "patientSummaryDTO";
    public static final String SESS_ATTR_REPORT_DISCHARGE = "reportDischarge";
    public static final String SESS_ATTR_REPORT_PURCHASE = "reportPurchase";

    // My Favourite
    public static final String MY_FAVOURITED_HEAD_ID = "0";

    // Base Unit
    public static final String BASE_UNIT_TRUE = "Y";

    // Presc Type
    public static final String PRESC_TYPE_HOME_LEAVE = "H";
    public static final String PRESC_TYPE_DISCHARGE = "D";
    public static final String PRESC_TYPE_OUTPATIENT = "O";
    // Simon 20191028 add  start--
    public static final String PRES_TYPE_ALL = "A";
    //Simon 20191028 add  end--

    public static final String DEFAULT_DATE_FORMAT = "dd-MMM-yyyy";
    public static final String DATE_TIME_FORMAT = "yyyyMMddHHmmss";

    public static final String FIVE_ML_SPOONFUL = "5mL spoonful";

    // Ricci 20190731 start --
    // Action Status
    public static final String ACTION_TYPE_ADMINSTER_IN_CLINIC = "C";
    public static final String ACTION_TYPE_PURCHASE_IN_COMMUNITY = "P";
    public static final String ACTION_TYPE_DISPENSE_IN_PHARMACY = "Y";
    public static final String ACTION_TYPE_RECORD_ONLY = "R";

    // Regiment Type
    public static final String REGIMENT_TYPE_DAILY = "D";
    public static final String REGIMENT_TYPE_WEEKLY = "W";
    public static final String REGIMENT_TYPE_MONTHLY = "M";
    public static final String REGIMENT_TYPE_CYCLE = "C";
    public static final String REGIMENT_TYPE_STEP_UP_DOWN = "S";
    // Ricci 20190731 end --

    public static final String OVERRIDDEN_MSG = "(!) Allergy prompt is overridden.";
    public static final String INGTEDIENT_MORE_THAN_THREE_MSG = "* This drug contains > 3 ingredients";
    public static final String LOCAL_DENTAL_TREATMENT_IND = "^";
    public static final String LOCAL_DENTAL_TREATMENT_MSG = "For local dental treatment only(僅供本地牙治療之用)";
    public static final String DENTAL_TREATMENT_IND = "#";
    public static final String DENTAL_TREATMENT_MSG = "For dental treatment only (祇限牙科醫療用)";

    public static final Map<String, String> SOURCE_SYSTEM_ORD_SUBTYPE_MAP = Collections
            .unmodifiableMap(new HashMap<String, String>() {
                /**
                 *
                 */
                private static final long serialVersionUID = 1L;

                {
                    put("CS", "C");
                    put("DS", "D");
                }
            });

    public static final String APP_ID = "MOE";

    public static final String PARAM_NAME_REFRESH_MOE_IP_ADDR = "drugmaintenance_refresh_moe_ip_address";
    public static final String PARAM_NAME_ENABLE_REFRESH_MOE = "enable_drugmaintenance_refresh_MOE";
    public static final String REFRESH_DRUG_URL = "/moe/moe/refreshDrugList";

    public static final String SETTING_ENABLE_SAME_ROUTE_SITE_DRUG_CHECKING = "enable_same_route_site_same_drug_checking";
    public static final String ENABLE_HLA_B_1502_REMINDER = "enable_hla-b*1502_reminder";

    public static final String BLANKS_IND = "__";
    public static final String FREQ_CHANGE_NUM_TO_WORD = "__times/day";
    public static final String ON_EVEN_ODD = "on odd / even days";
    public static final String ON_WEEK_DAYS = "mon, tue, wed, thu, fri, sat, sun";

    public static final String PRINT_DOC_VIEWER_PATH = "print_doc_viewer_path";
    public static final String PRINT_DOC_VIEWER_PROXY = "dvProxy.html";
    public static final String PRINT_AGENT_CLIENT_URL = "print_agent_client_url";
    public static final String PRINT_AGENT_CLIENT_VER = "print_agent_client_ver";
    public static final String PRINT_AGENT_LOCAL = "print_agent_local";

    public static final String MED_DISC_INFO_PREFIX = "Reason for discontinuation: ";

    public static final String FORM_ROUTE_DISPLAY_YES = "Y";


    // Ricci 20190704 start --
    public final static String CODE_LIST_TYPE_FREQ_CODE = "freq_code";
    public final static String CODE_LIST_TYPE_ROUTE = "route";
    public final static String CODE_LIST_TYPE_DURATION_UNIT = "duration_unit";
    public final static String CODE_LIST_TYPE_BASE_UNIT = "base_unit";
    public final static String CODE_LIST_TYPE_ACTION_STATUS = "action_status";
    public final static String CODE_LIST_TYPE_SITE = "site";
    // Chris 20190805  -Start
    public final static String CODE_LIST_TYPE_REGIMEN_TYPE = "regimen_type";
    // Chris 20190805  -End

    /// Simon 20190917 start--
    public final static String CODE_LIST_TYPE_LEGAL_CLASS = "legal_class";
    public final static String CODE_LIST_TYPE_FORM = "form";
    public final static String CODE_LIST_TYPE_DOSE_FORM_EXTRA_INFO = "dose_form_extra_info";

    //Simon 20190917 end--
    // Ricci 20191012 start --
    public final static String CODE_LIST_TYPE_COMMON_DOSAGE_TYPE = "common_dosage_type";
    // Ricci 20191012 end --
    // Ricci 20191014 start --
    public final static String CODE_LIST_TYPE_ALIAS_NAME_TYPE = "alias_name_type";
    // Ricci 20191014 end --
    // Chris 20191014 Start --
    public final static String CODE_LIST_TYPE_DURATION_UNIT_MAP = "duration_unit_map";
    // Chris 20191014 End --

    public final static String SYSTEM_LOGIN = "SYSTEM_LOGIN";
    public final static String LOGIN_ID = "LOGIN_ID";
    public final static String HOSPITAL_CD = "HOSPITAL_CD";
    public final static String MRN_PATIENT_IDENTITY = "MRN_PATIENT_IDENTITY";
    public final static String MRN_PATIENT_ENCOUNTER_NUM = "MRN_PATIENT_ENCOUNTER_NUM";
    public final static String PATIENT_TYPE_CD = "PATIENT_TYPE_CD";
    public final static String PRESC_TYPE_CD = "PRESC_TYPE_CD";

    public final static Integer ERROR_RESULT_CODE = 1;
    public final static Integer SUCCESS_RESULT_CODE = 0;
    // JWT start --
    public static final String JWT_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkngXDsTy7Vuo1CcDu0mUP+3lN2JarCYJa6lDJhbBwUstEE/HCTtZwsgoWFJFb2T3WAxCsXlxYKo9DiWJbBeMV7TRTzKbAz0Wp4M6ygAWo3NLlzeOa+4cMOVQ9kq0doHormzxzA/tHXTx9BYBtZBJryuXL3+qxWsb0CLtMa/W6Gs1+zKxo3FGvW0t1QnoZfoJV1VF8kvSEsDpsmV3kqerE0cVPkn/BOZ22x32F8Q3k3g0+g9npWIUZ3zVn/pemuG7vYTtPjBKAJCKeFF86gW2Fqb0EjrGCZx7BCO4gV3Gb6vng2mLEb3DkIcGBelmE0rnYAmlYAtJnSxWVI8gNUhwlwIDAQAB";
    public static final String JWT_PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCSeBcOxPLtW6jUJwO7SZQ/7eU3YlqsJglrqUMmFsHBSy0QT8cJO1nCyChYUkVvZPdYDEKxeXFgqj0OJYlsF4xXtNFPMpsDPRangzrKABajc0uXN45r7hww5VD2SrR2geiubPHMD+0ddPH0FgG1kEmvK5cvf6rFaxvQIu0xr9boazX7MrGjcUa9bS3VCehl+glXVUXyS9ISwOmyZXeSp6sTRxU+Sf8E5nbbHfYXxDeTeDT6D2elYhRnfNWf+l6a4bu9hO0+MEoAkIp4UXzqBbYWpvQSOsYJnHsEI7iBXcZvq+eDaYsRvcOQhwYF6WYTSudgCaVgC0mdLFZUjyA1SHCXAgMBAAECggEATH1chUEDIEOa0lghkcAmJJCnAJpjgb1HdCrrv0tpGfmZaghPiBmvkZIXx7AHRNo0dY3Jkoc6V2HwCqA9uO4/Q1pRaIuj+jf/ybE1lWIgcHF4i1rfDKxHmp1W/5gZ0D2dduG8EHZMvPJinLaVY+0bHdik1pBUbU63MqXauvT6RVtUgZZP1hoD6O23ZYwMiCtib9gkisniaoLBn1CIZ4MDcWbn6dxne1yJLDeoVpP4ZFU3sVlmnekh5KcUhklNkdMvLOSeuchP0dFXDgQtwUDfJNd5JWFOVCo1f4yl3pAYZTvQp/+v4+6LDff4z4ft5HPjPrktGT85GRQDdJM6pX6u4QKBgQDWfyYj5UbiJxpVD+M4SChY/WIY8mSkYIEH5Tj/KfKddCOUdx4kBacMl47Q/131c1y/ztasVwVPA1CObNQthVcIftyuqhdp06VvO2GmiMlaBMAG0vB4mZ7Y47Tr5vW4atpV/G7iNpCQ5GriTLzg9VFAWzRT0/tNfOjmeUtBqukz5wKBgQCuz0WbdyMwjq6T9d83gFKbrNhupNMOVmjuSO9oDNQOyvnQrDYZsghOfEAs4VCoEwkI01bkE6eIxKn93xlEfG/ztqqcZ0y40LCbYZgbdB4s2966lj2n+IZp22wLIjZGuLlRxRb6uBuD+aGsLt9NO1fN6OwtsVcE3ie2X2pDFIBH0QKBgBf8tiYzgL7Pwqkakr17jE+PXoYNrWWYiTV50+rtJP2ovEXhIFTE8Q5+cHE87aQCV6/3kJhLTwOsCjkzQHMqwzXnAgTibqXeDlUnHd+C0omnmFXrBwtlmpcEt/ndpUMPaAVPqpLPqVEayj0uebzqkkCRMoPhY+wheOY1UE4yr8unAoGBAKawBlPEOs3mgBmebgbiWeMP4iE/EfGt+8iby42QDfU9HxEX5U6DMOCGIjupbduWqxq7SzOYejw0K1RlFBb2AVqHaBBTgBkLBhxj50Lmao+j6LJK6OfuV11BWbkUGmU9Z5jNMqhIPhSOSNeCqnNejs9Nt0eqvqtpmryQJydnuRrBAoGART1cwZj3Z3yLAqbWaxoOkCCoT0EGPTKi760R2eNaKwHn+lP9kO8y2DsvmtkwmANUoBOEW4tyqRjFxAAK+U5K6pkM9PHKq/q8+HbIUbCY1gg3KINHProsbFv3VCX+dcjuwMxtEPXna9GSTyn2ycMsiKLnVAuMBVG1antVQKrfGJ8=";
    public static final String JWT_ISSUER = "Moe";
    // JWT end --
    // Ricci 20190704 end --

    // Ricci 20190731 start --
    public static final String CLASS_PATH = "classpath:";
    public static final String REPORT_PATH = "reports/";
    public static final String REPORT_PATH_FOR_PRESCRIPTION = "prescription/";
    public static final String REPORT_NAME_FOR_DISCHARGE_PRESCRIPTION = "Discharge_Prescription.jasper";
    public static final String REPORT_NAME_FOR_PURCHASE_PRESCRIPTION = "Purchase_Prescription.jasper";
    // Ricci 20190731 end --

    // Chris 20190814 start --
    public static final String ORDER_PRINT_TYPE_PRINT = "P";
    // Chris 20190814 end --
    // Ricci 20190814 start --
    public static final String ORDER_PRINT_TYPE_RE_PRINT = "R";
    // Ricci 20190814 end --
    // Ricci 20190820 start --
    public static final String PRESCRIPTION_EDITABLE_DURATION = "prescription_editable_duration";
    public final static String REGEX_NUMBER = "[0-9]+$";
    public static final String MODE_CREATE_NEW = "create";
    //public static final String MODE_EDIT_EXISTING_PRESCRIPTION = "edit";
    public static final String MODE_ENQUIRY = "enquiry";
    public static final String MODE_EDIT_WITH_REMARK = "editWithRemark";
    public static final String MODE_BACKDATE = "backdate";
    public static final String MODE_LOCK = "lock";
    // Ricci 20190820 end --

    // Simon 20190822 for moe_drug_server start--
    public static final String TERMINOLOGY_NAME = "HKCTT";
    // Simon 20190822 for moe_drug_server end--

    // Ricci 20190822 for moe_drug_server start--
    public static final String PARAM_NAME_ENABLE_LOCAL_THERAPEUTIC_CLASS = "enable_local_therapeutic_class";
    // Ricci 20190822 for moe_drug_server end--

    // Chris 20190827 For Special Interval Start--
    public static final String SPECIAL_INTERVAL_ODD_EVEN_DAYS = "on odd / even days";
    public static final String SPECIAL_INTERVAL_DAY_OF_WEEK = "mon, tue, wed, thu, fri, sat, sun";
    // Chris 20190827 For Special Interval End--

    // Chris 20190827 For Input Box Display Special Interval in "on odd / even days" Case  Start--
    public static final String INPUT_BOX_SPECIAL_INTERVAL_ON_ODD_DAYS = "on odd days";
    public static final String INPUT_BOX_SPECIAL_INTERVAL_ON_EVEN_DAYS = "on even days";
    // Chris 20190827 For Input Box Display Special Interval in "on odd / even days" Case  End--

    // Chris 20190906 For Special Interval Start--
    public static final String SPECIAL_INTERVAL_FREQ_CODE_DAILY = "daily";
    // Chris 20190906 For Special Interval End--

    // Ricci 20190925 start --
    public static final String SYSTEM_LOGIN_FOR_MOE_SVC = "MOE";
    public static final String SYSTEM_LOGIN_FOR_DRUG_MAINTENANCE_SVC = "DMS";
    // Ricci 20190925 end --

    // Ricci 20190926 start --
    public static final String APP_VERSION = "1.0.0";
    public static final String DAC_PAWD = "123456"; // TODO
    // Ricci 20190926 end --

    // Ricci 20191009 start --
    public static final String RECORD_INSERT = "I";
    public static final String RECORD_UPDATE = "U";
    public static final String RECORD_DELETE = "D";
    // Ricci 20191009 end --

    public static final String FIELD_ERROR_NOT_NULL = "NOT_NULL";

    // Simon 20191016 add for system setting start--
    public final static String CODE_LIST_TYPE_SYSTEM_SETTING = "system_setting";
    //Simon 20191016 add for system setting end--
    public final static String CODE_LIST_TYPE_FORM_ROUTE_MAP="form_route_map";
    // Ricci 20191018 start -- for HLA-B
    public static final String SYSTEM_SETTING_PARAM_NAME_HLAB1502_NEG = "saam_alert_desc_hlab1502_negative";
    public static final String SYSTEM_SETTING_PARAM_NAME_HLAB1502_NEG_CODE = "saam_alert_desc_hlab1502_negative_code";
    // Ricci 20191018 end --

    // Simon 20191025 saam constants start--
    public static final String PATIENT_ALLERGY_ADD_FAIL_ERRCODE = FUNCTION_ID_ALERT_MODULE + "-D-002";
    public static final String PATIENT_ALERT_DELETE_FAIL_ERRCODE = FUNCTION_ID_ALERT_MODULE + "-D-003";
    //Simon 20191025 saam constants end--
    // Simon 20191028 start--
    public static final String ORDER_STATUS_CREATE = "O";
    public static final String ORDER_STATUS_DELETE = "CO";
    public static final String DRUG_HISTORY_WITHIN_MONTH_ALL = "A";
    public static final String DRUG_HISTORY_WITHIN_MONTH_ONE_MONTH = "1";
    public static final String DRUG_HISTORY_WITHIN_MONTH_THREE_MONTH = "3";
    public static final String DRUG_HISTORY_WITHIN_MONTH_SIX_MONTH = "6";
    public static final String DANGEROUS_DRUG_LEGAL_CLASS_Y = "Y";
    public static final String DANGEROUS_DRUG_LEGAL_CLASS_N = "N";
    public static final String DOSAGE_TYPE_ADULT = "A";
    public static final String DOSAGE_TYPE_PAEDIATRIC = "P";
    public static final String DOSAGE_TYPE_ALL = "C";
    //Simon 20191028 end--

    //Chris 20191028  -Start
    //Order
    public static final String ORDER_TRANSACTION_TYPE_INSERT = "I";
    public static final String ORDER_TRANSACTION_TYPE_UPDATE = "U";
    public static final String ORDER_TRANSACTION_TYPE_DELETE = "D";
    public static final String ORDER_TRANSACTION_TYPE_N_A = "N";     // N/A

    public static final String ORDER_REMARK_STATUS_Y = "Y";

    //Profile
    public static final String PROFILE_ITEM_STATUS_ACTIVE = "A";
    public static final String PROFILE_ITEM_STATUS_DELETE = "D";

    public static final int LENGTH_100 = 100;
    //Chris 20191028  -End

    public static final String DAC_LOGIN_USER = "CIMS-MOE-SVC";

    // Ricci 20191105 start --
    public static final String MOE_CACHE_ORDER_PREFIX = "MOE:ORDER:MRN:%S:MRN_E_NUM:%S";
    public static final String MOE_CACHE_DEPT_FAV_MAINTENANCE_PREFIX = "MOE:DEPT_FAV:SPEC:%S:HDR_CACHE_ID:%S";
    public static final String MOE_CACHE_DEPT_FAV_MAINTENANCE_PREFIX_WITH_FAV_ID = "MOE:DEPT_FAV:SPEC:%S:HDR_CACHE_ID:%S:FAV_ID:%S";
    // Ricci 20191105 end --


    public static final String STEP_UP_DOWN = "S";

    public static final String MOE_CACHE_LOCK_ORDER_PREFIX = "MOE:ORDER_LOCK:MRN:%S:MRN_E_NUM:%S";
    public static final String MOE_CACHE_TOKEN_BLACKLIST_PREFIX = "MOE:TOKEN:SYSTEM_LOGIN:%S:SPEC:%S:HOSP_CD:%S:LOGIN_ID:%S:BL_ID:%S";



    //Chris 20191112  -Start

    //Template of Redis Key
    //public static final String DRUG_SEARCH_BASIC_CACHE_PREFIX = "DRUG:DRUG_CATEGORY:%S:DRUG_TYPE:%S:DRUG_NAME:%S:CACHE_ID:%S";
    public static final String DRUG_SEARCH_CACHE_PREFIX_BASIC = "DRUG:DRUG_CATEGORY:%S:DRUG_TYPE:%S:MATCH_FIELD_REPLACEMENT_NAME:%S:CACHE_ID:%S";
    //public static final String DRUG_SEARCH_BASIC_CACHE_PREFIX = "DRUG:IS_DOSAGE:%S:DRUG_TYPE:%S:DRUG_NAME:%S:CACHE_ID:%S";
    public static final String DRUG_SEARCH_CACHE_PREFIX_MULTI_INGR = "DRUG:DRUG_CATEGORY:%S:DRUG_TYPE:BAN:MATCH_FIELD_ALIAS_NAME:%S:CACHE_ID:%S";

    //Template of keyword
    public static final String KEY_WORD_TEMPLATE_BASIC = "DRUG:DRUG_CATEGORY:*:DRUG_TYPE:*:MATCH_FIELD_REPLACEMENT_NAME:*%S*:CACHE_ID:*";
    public static final String KEY_WORD_TEMPLATE_MULTI_INGR = "DRUG:DRUG_CATEGORY:*:DRUG_TYPE:BAN:MATCH_FIELD_ALIAS_NAME:%S*:CACHE_ID:*";

    //Template of Redis key prefix (For delete all drugs)
    public static final String KEY_WORD_PREFIX = "DRUG:*";
    //Template of Redis Key (Simple)
    public static final String DRUGS_BY_TYPE_SIMPLE_REDIS_KEY =  "DRUG:DRUGS_BY_TYPE:DRUG_TYPE:%S:REPLACEMENT_NAME:%S:CACHE_ID:%S";
    public static final String DOSAGE_DRUGS_BY_TYPE_SIMPLE_REDIS_KEY = "DRUG:DOSAGE_DRUGS_BY_TYPE:DRUG_TYPE:%S:REPLACEMENT_NAME:%S:CACHE_ID:%S";
    //Template of Redis Key (search Multi Ingr By Ban Vtm)
    public static final String DRUGS_BY_TYPE_OF_BAN_REDIS_KEY = "DRUG:DRUGS_BY_TYPE:DRUG_TYPE:BAN:ALIAS_NAME:%S:CACHE_ID:%S";
    public static final String DOSAGE_DRUGS_BY_TYPE_OF_BAN_REDIS_KEY = "DRUG:DOSAGE_DRUGS_BY_TYPE:DRUG_TYPE:BAN:ALIAS_NAME:%S:CACHE_ID:%S";
    //Template of Generic Name Drug Redis key (For cache data and search data)
    public static final String GENERIC_NAME_LEVEL_DRUGS_REDIS_KEY = "DRUG:GENERIC_NAME_LEVEL:LOCAL_DRUG_ID:%S";

    //Type of List
    public static final String DRUG_LIST_TYPE = "DRUGS_BY_TYPE";
    public static final String DOSAGE_DRUG_LIST_TYPE = "DOSAGE_DRUGS_BY_TYPE";

    public static final String DRUG_CATEGORY = "DRUG_CATEGORY:";
    public static final String DRUG_TYPE = "DRUG_TYPE:";
    public static final String DRUG_NAME = "DRUG_NAME:";

    //Chris 20191112  -End

    //Chris 20200106  -Start
    public static final String PARAM_NAME_ENABLE_MOE_REDIS = "enable_moe_redis";
    public static final String PARAM_NAME_ENABLE_DEPT_FAV_REDIS = "enable_dept_fav_redis";
    //Chris 20200106  -End

    //Chris 20200109  -Start
    public static final boolean DEFAULT_VALUE_TRUE = true;
    public static final boolean DEFAULT_VALUE_FALSE = false;
    //Chris 20200109  -End

}
