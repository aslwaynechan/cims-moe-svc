package hk.health.moe.common;

public enum DrugSearchType {
    TRADENAME(DrugType.TRADENAME),
    GENERIC(DrugType.GENERIC),
    ASSORTED("ASSORTED"),
    NONMTT(DrugType.NONMTT),
    MULTIINGR(DrugType.MULTIINGR);

    public final String s;

    private DrugSearchType(String s) {
        this.s = s;
    }

    private DrugSearchType(DrugType t) {
        this.s = t.toString();
    }

    public String toString() {
        return s;
    }
}
