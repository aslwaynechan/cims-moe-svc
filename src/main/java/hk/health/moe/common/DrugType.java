package hk.health.moe.common;

public enum DrugType {
    TRADENAMEALIAS, TRADENAME, GENERIC, VTM, BAN, OTHER, ABB, NONMTT, MULTIINGR;
}
