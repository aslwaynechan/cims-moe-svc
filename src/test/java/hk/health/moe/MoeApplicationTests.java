package hk.health.moe;

import hk.health.moe.pojo.po.MoeActionStatusPo;
import hk.health.moe.repository.MoeActionStatusRepository;
import hk.health.moe.service.impl.MoeActionStatusServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MoeApplicationTests {

    @Mock
    MoeActionStatusRepository moeActionStatusRepository;

    @InjectMocks
    MoeActionStatusServiceImpl moeActionStatusServiceImpl;

    @Test
    public void test() throws Exception {
        MoeActionStatusPo moeActionStatus = new MoeActionStatusPo();
        moeActionStatus.setActionStatusType("ACTION_STATUS_TYPE");
        moeActionStatus.setActionStatus("ACTION_STATUS");
        moeActionStatus.setRank(1L);
        when(moeActionStatusRepository.findByActionStatusType(anyString())).thenReturn(moeActionStatus);
        assertEquals("ACTION_STATUS_TYPE", moeActionStatusServiceImpl.getActionStatus(anyString()).getActionStatusType());
        assertEquals("ACTION_STATUS", moeActionStatusServiceImpl.getActionStatus(anyString()).getActionStatus());
        assertEquals(1L, moeActionStatusServiceImpl.getActionStatus(anyString()).getRank());

    }


}
